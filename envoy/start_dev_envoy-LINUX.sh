#!/bin/bash
#
# This script starts a local "dev" instance of Envoy which is useful for development work

getenvoy run standard:1.11.0 -- --config-path ./mobi-devops-portal-envoy-DEV-LINUX.yaml
