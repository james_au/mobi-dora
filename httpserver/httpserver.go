package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//
// Starts a GoLang based static HTTP file server.  Using Gorilla Mux package.
//
// Reference:
// https://github.com/gorilla/mux/blob/master/README.md#static-files
//
func main() {
	// TODO: Default values need to be overridable via command line arguments
	const DEFAULT_SOURCE_DIR = "client"
	const DEFAULT_URL_PATH = "/html/"
	const DEFAULT_HTTP_PORT = 5000
	const DEFAULT_WRITE_TIME_SEC = 30
	const DEFAULT_READ_TIME_SEC = 30

	// The main Gorilla Mux object is the router
	var router = mux.NewRouter()

	// Add the HTTP handler
	router.PathPrefix(DEFAULT_URL_PATH).Handler(http.StripPrefix(DEFAULT_URL_PATH, http.FileServer(http.Dir(DEFAULT_SOURCE_DIR))))

	// Add access logging "middleware" handler
	router.Use(accessLogHandler)

	// Add custom headers to help with tracing & debugging
	router.Use(headerHandler)

	var server = &http.Server{
		Handler:      router,
		Addr:         "0.0.0.0" + ":" + strconv.Itoa(DEFAULT_HTTP_PORT),
		WriteTimeout: DEFAULT_WRITE_TIME_SEC * time.Second,
		ReadTimeout:  DEFAULT_READ_TIME_SEC * time.Second,
	}

	log.Println(fmt.Sprintf("Starting HTTP server.  Root path = http://localhost:%d%s", DEFAULT_HTTP_PORT, DEFAULT_URL_PATH))
	log.Println(fmt.Sprintf("Serving files from local directory: %s", DEFAULT_SOURCE_DIR))

	// Block and listen on port here
	log.Fatal(server.ListenAndServe())
}

// Logs requests.  Uses log output format similiar to Flask
//
func accessLogHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		// Log the access request
		log.Println(fmt.Sprintf("\"%s %s %s\"", request.Method, request.RequestURI, request.Proto))

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(response, request)
	})
}

// Adds custom response headers
//
func headerHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(response http.ResponseWriter, request *http.Request) {
		response.Header().Add("Server", "gorilla/mux")

		// Manually set HTML "content-type" for certain paths
		var urlLower = strings.ToLower(request.URL.Path)
		if strings.HasSuffix(urlLower, ".json") {
			response.Header().Add("content-type", "application/json")
		}

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(response, request)
	})
}
