package main

import (
	"dora/prometheus/config"
	"dora/prometheus/metrics"
	"dora/prometheus/util"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"sync"
	"time"
)

// This is a custom Prometheus "node exporter" for Dora metrics reporting
//
// Core handling logic adapted from this example: https://sysdig.com/blog/prometheus-metrics/
//

//
// Program constants
//

//
// Program parameters that will be read from command-line or config file at runtime
//
var (
	// Actual data listen TCP port
	paramListenPort *string

	// URL source of the data from Dora
	paramSourceUrl *string
)

//
// Program globals
//
var (
	urlDoraGrpc     string     // Will be populated with the URL to the required source Dora status feed
	httpServerMutex sync.Mutex // Used to force HTTP server access to one thread at a time
	//errorLog          error_log.ErrorLog  // Output error logs (low volume logging only)
)

//
// Declare/define the Prometheus gauges used for this exporter
//
var (
	gaugeTotalLogins = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: config.DEFAULT_NAMESPACE,
			Subsystem: "auth",
			Name:      "total_logins",
			Help:      "Current number of logins both non-expired and expired",
		})
	gaugeActiveLogins = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: config.DEFAULT_NAMESPACE,
			Subsystem: "auth",
			Name:      "active_logins",
			Help:      "Current number of non-expired logins",
		})
)

var (
	configRec *config.ConfigStruct
)

// Implements the http.Handler interface for /metrics endpoint
type MetricsHttpHandler struct {
	http.Handler
}

func (o *MetricsHttpHandler) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	// SINGLE ACCESS ONLY:
	// Use a mutex to force serial single-threaded access to the server handler at any given time as the contents
	// might not be thread safe.  This is not expected to be a performance problem since this endpoint is only meant to
	// be polled by a single Prometheus instance.
	httpServerMutex.Lock()
	defer httpServerMutex.Unlock()

	fmt.Println("Handling /metrics endpoint...")

	// Get Dora raw metrics from its gRPC endpoint
	var metricsRec = metrics.MetricsStruct{}
	if err := util.RetrieveJsonData(urlDoraGrpc, &metricsRec); err == nil {
		gaugeTotalLogins.Set(float64(util.Atoi(metricsRec.TotalLogins)))
		gaugeActiveLogins.Set(float64(util.Atoi(metricsRec.ActiveLogins)))

		// Call next handler (Prometheus) after performing stats generation above
		promhttp.Handler().ServeHTTP(responseWriter, request)
	} else {
		// Got fatal error while retrieving telemetry data so return an HTTP 500 response
		http.Error(responseWriter, fmt.Sprintf("Error: %v\n", err), 500)

		// Add additional information to the error
		responseWriter.Write([]byte(fmt.Sprintf("\n")))
		responseWriter.Write([]byte(fmt.Sprintf("\n")))
		responseWriter.Write([]byte(fmt.Sprintf("You can call the the root (/) endpoint for more diagnostic information.\n")))

		// Return early -- do not call Prometheus handler since no data could be retrieved from the source
		return
	}
}

// Implements the http.Handler interface for / (root) endpoint
type RootHttpHandler struct {
	http.Handler
}

func (o *RootHttpHandler) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	fmt.Println("Handling / endpoint...")

	//
	// This handler outputs status information and does a test run showing failure/success as necessary to help admins
	// debug configuration when there are suspected problems.
	//

	var startTime = time.Now()
	var metricsRec = metrics.MetricsStruct{}
	if err := util.RetrieveJsonData(urlDoraGrpc, &metricsRec); err == nil {
		//
		// Successful status health check
		//
		var duration = time.Now().Sub(startTime)
		responseWriter.Write([]byte(fmt.Sprintf("Successful call to Dora metrics source data at %s (%0.3f secs)\n", urlDoraGrpc, duration.Seconds())))
		responseWriter.Write([]byte(fmt.Sprintf("  Total logins: %s\n", metricsRec.TotalLogins)))
		responseWriter.Write([]byte(fmt.Sprintf("  Active logins: %s\n", metricsRec.ActiveLogins)))
		responseWriter.Write([]byte(fmt.Sprintf("\n")))
		responseWriter.Write([]byte(fmt.Sprintf("You can now call the /metrics endpoint to get the raw node exporter Prometheus data\n")))
	} else {
		//
		// Error detected
		//
		http.Error(responseWriter, "", 500)

		responseWriter.Write([]byte(fmt.Sprintf("********************************************\n")))
		responseWriter.Write([]byte(fmt.Sprintf("** RUNTIME OR CONFIG PROBLEM ENCOUNTERED! **\n")))
		responseWriter.Write([]byte(fmt.Sprintf("********************************************\n")))
		responseWriter.Write([]byte(fmt.Sprintf("FAILED to call Dora source specified at: %s\n", urlDoraGrpc)))
		responseWriter.Write([]byte(fmt.Sprintf("ERROR ENCOUNTERED: %v\n", err)))
		responseWriter.Write([]byte(fmt.Sprintf("\n")))
		responseWriter.Write([]byte(fmt.Sprintf("Is Dora URL pointed at the correct location that returns expected JSON formatted data?  Check the source URL!\n")))
		responseWriter.Write([]byte(fmt.Sprintf("Is Dora URL correctly formatted?\n")))
		responseWriter.Write([]byte(fmt.Sprintf("Is Dora instance up and running?\n")))
		responseWriter.Write([]byte(fmt.Sprintf("Is network accessible?\n")))
		responseWriter.Write([]byte(fmt.Sprintf("\n")))
		responseWriter.Write([]byte(fmt.Sprintf("Correct the problems above and restart this exporter.\n")))
	}
}

func main() {
	if configRecTemp, errConfig := config.New(); errConfig != nil {
		// Fatal

	} else {
		// Config read is good so use it as global config
		configRec = configRecTemp
	}

	// Set source of metric data
	urlDoraGrpc = configRec.DatasourceUrl

	fmt.Printf("Will retrieve Dora stats from URL: %s\n", urlDoraGrpc)

	// Register HTTP handlers for all required endpoints
	var mux = http.NewServeMux()
	mux.Handle("/metrics", &MetricsHttpHandler{})
	mux.Handle("/", &RootHttpHandler{})

	// All gauge instances must be registered with the Prometheus library
	prometheus.MustRegister(gaugeActiveLogins)
	prometheus.MustRegister(gaugeTotalLogins)

	// Start the HTTP server on given listening port.  It will block until terminated or errors out.
	//
	fmt.Printf("Starting HTTP server listening on port: %d\n", configRec.HttpListenPort)
	if errServer := http.ListenAndServe(fmt.Sprintf(":%d", configRec.HttpListenPort), mux); errServer != nil {
		var errMsg = fmt.Sprintf("Server could not start or errored out with fatal runtime error: %v", errServer)
		panic(errMsg) // Panic to return error code to OS and message to console if running in a terminal
	} else {
		fmt.Sprintf("Server terminated normally")
	}
}