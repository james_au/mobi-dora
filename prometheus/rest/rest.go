package rest

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

const DEFAULT_MEDIA_TYPE = "application/json"

// Header used by VCenter REST API for its authentication
const VCENTER_AUTH_HEADER = "vmware-api-session-id"

var rest *http.Client

// A valid VCenter auth token will be cached here so subsequent VCenter rest calls do
// not have to get a new auth token every time
var VCenterAuthToken string

// For deserializing VCenter's token response for REST login authentication
type vSphereAuthTokenResponse struct {
	Value string `json:"value"`
}

// Maximum number of retries.  Can be set from outside the package.
var MaxRetries = 2

// The default HTTP header for authorization.  This should be changed for the respective system.
// Example VCenter REST API uses custom header name "vmware-api-session-id" instead.
// NOTE: this is not thread safe and may break if multiple threads use different auth headers!
//
var AuthorizationHeaderName = "authorization"

func init() {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	rest = &http.Client{Transport: tr}
}

// checkClose is used to check the return from Close in a defer
// statement.
func checkClose(c io.Closer, err *error) {
	cerr := c.Close()
	if *err == nil {
		*err = cerr
	}
}

// drainAndClose discards all data from rd and closes it.
// If an error occurs during Read, it is discarded.
func drainAndClose(rd io.ReadCloser, err *error) {
	if rd == nil {
		return
	}

	_, _ = io.Copy(ioutil.Discard, rd)
	cerr := rd.Close()
	if err != nil && *err == nil {
		*err = cerr
	}
}

// Get creates a GET request and sends it
// auth will be performed automatically (like for VCenterAuth)
func Get(url string) ([]byte, error) {
	var attemptsRemaining = MaxRetries
	var attemptErrors []string

	for attemptsRemaining > 0 {
		attemptsRemaining--

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			attemptErrors = append(attemptErrors, fmt.Sprintf("New Request Error: %s", err))
			continue // will retry
		}

		req.Header.Add("content-type", DEFAULT_MEDIA_TYPE)

		res, err := rest.Do(req)
		if err != nil {
			attemptErrors = append(attemptErrors, fmt.Sprintf("GET error result: %v", err.Error()))
			continue // will retry
		}

		// Close the body at the end
		defer checkClose(res.Body, &err)

		//color.Green("%s %s", url, res.Status)

		if res.StatusCode == 401 {
			attemptErrors = append(attemptErrors, fmt.Sprintf("GET encountered HTTP 401 - Unauthorized"))

			continue
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			attemptErrors = append(attemptErrors, fmt.Sprintf("Response Body Error: %v", err))
			continue // will retry
		}

		return body, nil
	}

	// Fatal
	return nil, errors.New(fmt.Sprintf("Failed to do GET successfully after %d retries.  Errors: %v", MaxRetries, attemptErrors))
}

// Use HTTP Basic Auth to POST to a server and receive JSON auth token response.  The auth token
// will be deserialized to the "dataContainer" which must be annotated with the matching JSON data
// fields.
func PostWithHttpBasicAuth(url string, username string, password string, dataContainer interface{}) error {
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		// Fatal
		return errors.New(fmt.Sprintf("New Request Error: %s", err))
	}

	req.Header.Add("accept", DEFAULT_MEDIA_TYPE)
	req.SetBasicAuth(username, password)

	res, err := rest.Do(req)
	if err != nil {
		// Fatal
		return errors.New(fmt.Sprintf("POST Error: %s", err))
	}

	// Close the body at the end
	defer checkClose(res.Body, &err)

	//color.Green("%s %s", url, res.Status)

	b, err := ioutil.ReadAll(res.Body)
	if res.StatusCode >= 400 {
		if err != nil {
			return errors.New(fmt.Sprintf("Response Body Error: %s", err))
		}
		return errors.New(fmt.Sprintf("Response Body: %s", string(b)))
	}

	// Unmarshal directly into the given struct
	json.Unmarshal(b, &dataContainer)

	return nil
}

// Post creates a POST request and sends it.
func Post(url string, body io.Reader) ([]byte, error) {
	return doHttpAction("POST", url, body)
}

// Patch creates a PATCH request and sends it.
func Patch(url string, body io.Reader) ([]byte, error) {
	return doHttpAction("PATCH", url, body)
}

// Does an HTTP action specified by "httpAction".
func doHttpAction(httpAction string, url string, body io.Reader) ([]byte, error) {
	var attemptsRemaining = MaxRetries

	for attemptsRemaining > 0 {
		attemptsRemaining--

		req, err := http.NewRequest(httpAction, url, body)
		if err != nil {
			//color.Red("New Request Error: %s", err)
			continue // will retry
		}

		req.Header.Add("content-type", DEFAULT_MEDIA_TYPE)

		res, err := rest.Do(req)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("%s Error: %s", httpAction, err))
		}

		// Close the body at the end
		defer checkClose(res.Body, &err)

		//color.Green("%s %s", url, res.Status)

		b, err := ioutil.ReadAll(res.Body)
		if res.StatusCode == 401 {
			//color.Yellow("WARN: Server reports Authorization is required.")

			continue // will retry
		} else if res.StatusCode >= 400 {
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Response Body Error: %s", err))
			}
			return nil, errors.New(fmt.Sprintf("Response Body: %s", string(b)))
		}

		return b, nil
	}

	// Fatal
	return nil, errors.New(fmt.Sprintf("Failed to do %s successfully after %d retries", httpAction, MaxRetries))
}

// Put creates a PUT request and sends it
func Put(url string, body io.Reader) ([]byte, error) {
	var attemptsRemaining = MaxRetries

	for attemptsRemaining > 0 {
		attemptsRemaining--

		req, err := http.NewRequest("PUT", url, body)
		if err != nil {
			//color.Red("New Request Error: %s", err)
			continue // will retry
		}

		req.Header.Add("content-type", DEFAULT_MEDIA_TYPE)

		res, err := rest.Do(req)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("PUT Error: %s", err))
		}

		// Close the body at the end
		defer checkClose(res.Body, &err)

		//color.Green("%s %s", url, res.Status)

		b, err := ioutil.ReadAll(res.Body)
		if res.StatusCode == 401 {
			//color.Yellow("WARN: Server reports Authorization is required.")

			continue
		} else if res.StatusCode >= 400 {
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Response Body Error: %s", err))
			}
			return nil, errors.New(fmt.Sprintf("Response Body: %s", string(b)))
		}

		return b, nil
	}

	// Fatal
	return nil, errors.New(fmt.Sprintf("Failed to do PUT successfully after %d retries", MaxRetries))
}

// Delete creates a DELETE request and sends it
func Delete(url string, body io.Reader) ([]byte, error) {
	var attemptsRemaining = MaxRetries

	for attemptsRemaining > 0 {
		attemptsRemaining--

		req, err := http.NewRequest("DELETE", url, body)
		if err != nil {
			//color.Red("New Request Error: %s", err)
			continue // will retry
		}

		req.Header.Add("content-type", DEFAULT_MEDIA_TYPE)

		res, err := rest.Do(req)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("PUT Error: %s", err))
		}

		// Close the body at the end
		defer checkClose(res.Body, &err)

		//color.Green("%s %s", url, res.Status)

		b, err := ioutil.ReadAll(res.Body)
		if res.StatusCode == 401 {
			//color.Yellow("WARN: Server reports Authorization is required.")

			continue
		} else if res.StatusCode >= 400 {
			if err != nil {
				return nil, errors.New(fmt.Sprintf("Response Body Error: %s", err))
			}
			return nil, errors.New(fmt.Sprintf("Response Body: %s", string(b)))
		}

		return b, nil
	}

	// Fatal
	return nil, errors.New(fmt.Sprintf("Failed to do DELETE successfully after %d retries", MaxRetries))
}
