package util

import (
	"dora/prometheus/rest"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/spf13/afero"
	"os"
	"strconv"
	"strings"
)

func TrimByteArray(data []byte, maxLength int) []byte {
	if len(data) > maxLength {
		return data[:maxLength]
	} else {
		// No trimming required
		return data
	}
}

// Retrieves JSON data at given URL and attempts to unmarshal it into the given target object.
// The target object must be properly annotated otherwise an error object is returned.
// A return of 'nil' will indicate no errors were encountered and the target object should
// now contain the expected unmarshalled data.
//
func RetrieveJsonData(url string, targetObj interface{}) error {
	if !strings.HasPrefix(strings.ToLower(url), "http://") && !strings.HasPrefix(strings.ToLower(url), "https://") {
		// Default scheme to http:// if not specified
		url = "http://" + url
	}

	// Peform network I/O to fetch the body of the HTTP document at the given URL
	if bytes, errNetworking := rest.Get(url); errNetworking == nil {
		// Connection established and data received. Now try to parse the JSON data
		//
		if errParse := UnmarshalWithChecks(bytes, targetObj); errParse != nil {
			// Fatal error - details will be within error object
			return errParse
		}
	} else {
		// Network error.  Could not even access the host at the given URL.  Common errors: Host not found,
		// connection refused, and other low level network connection errors.
		//
		return errors.New(fmt.Sprintf("Can't get telemetry JSON data.  Fatal networking error encountered: %v", errNetworking))
	}

	// No errors.  Data is populated on the given target object at this point.
	return nil
}

// Unmarshals the given raw bytes into the given target object.  The target object must be
// properly JSON annotated for the expected data.  Some checks are performed and may trigger
// an error return which will contain an error message.
//
func UnmarshalWithChecks(jsonData []byte, targetObj interface{}) error {
	if len(jsonData) > 0 {
		if err := json.Unmarshal(jsonData, targetObj); err != nil {
			// FATAL - Unmarshalling failed
			return errors.New(fmt.Sprintf("Unmarshalling failed because of error: %v.  Received unexpected data (raw text): %s", err, TrimByteArray(jsonData, 256)))
		} else {
			// Successful unmarshalling
			return nil
		}
	} else {
		// Fatal - empty data was given for unmarshalling
		return errors.New("Unmarshalling failed because data received was zero length.  Check status data URL is correct location.")
	}
}

// Translates a string "ok" to be 1 and anything else to be 0 which can be understood
// by Prometheus
//
func StatusOkTranslate(text string) int {
	if text == "ok" {
		return 1
	} else {
		return 0
	}
}

// Ensure that the file at the given path exists.  NOTE that if a new file is created then
// it will be owned by running user ID and be zero bytes in size.
func EnsureFileExists(path string) error {
	if _, errStat := os.Stat(path); errStat != nil {
		// File does not exist.  Create a zero byte file.
		if _, errCreate := os.Create(path); errCreate != nil {
			// Got some creation error
			return errCreate
		}
	}

	// No errors
	return nil
}

func Atoi(s string) int {
	if n, errConvert :=strconv.Atoi(s); errConvert != nil {
		return -1
	} else {
		return n
	}
}

// Returns true if file exists at given path and false otherwise
func FileExists(path string) bool {
	info, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

// Read opens the file at the given path and returns the content in bytes
var aferoFs afero.Fs
func ReadFile(path string) ([]byte, error) {
	if aferoFs == nil {
		// Lazy instantiation on first use
		aferoFs = afero.NewOsFs()
	}

	if strings.TrimSpace(path) == "" {
		// Fatal
		return nil, errors.New(fmt.Sprintf("File path was empty"))
	}

	b, err := afero.ReadFile(aferoFs, path)
	if err != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Error reading file '%s' because of error: %v", path, err))
	}

	return b, nil
}
