package metrics

// Data record for internally storing required metrics for output
type MetricsStruct struct {
	TimestampMs  string `json:"timestampMs"`
	TotalLogins string `json:totalLogins"`
	ActiveLogins string `json:"activeLogins"`
}