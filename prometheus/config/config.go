package config

import (
	"dora/prometheus/util"
	"encoding/json"
	"errors"
	"fmt"
)

const (
	// Can be overridden
	DEFAULT_LISTEN_PORT int32 = 9108

	// Can be overridden
	DEFAULT_SOURCE_URL  string = "http://envoy:8000/v1/GetMetricsReport"

	// CANNOT be overridden
	DEFAULT_NAMESPACE   string = "dora"

	// CANNOT be overridden
	DEFAULT_CONFIG_PATH string = "nodeexporter-override-config.json"
)

// Contain values for overridable configurations
type ConfigStruct struct {
	DatasourceUrl  string   `json:"datasource"`
	HttpListenPort int32    `json:"http_listen_port"`
}

// Returns reference a config struct that will either contain default values or overridden values if a valid override
// JSON configuration file was found at the expected location
//
func New() (*ConfigStruct, error) {
	// Start with default values and override as necessary
	var configRec = ConfigStruct{
		DatasourceUrl:  DEFAULT_SOURCE_URL,
		HttpListenPort: DEFAULT_LISTEN_PORT,
	}

	// Check if override config exists and read that config in if available
	if util.FileExists(DEFAULT_CONFIG_PATH) {
		if data, errRead := util.ReadFile(DEFAULT_CONFIG_PATH); errRead != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("File I/O error occurred reading config at '%s' with error: %v", DEFAULT_CONFIG_PATH, errRead))
		} else {
			// IO Read successful so parse the JSON formatted configuration data
			//
			// Unmarshall file bytes into the config structure
			json.Unmarshal(data, &configRec)

			// Update the config with the path the configuration data was read from
			return &configRec, nil
		}
	} else {
		fmt.Printf("Default config file '%s' not found.  Using all default values instead.\n", DEFAULT_CONFIG_PATH)

		return &configRec, nil
	}
}


