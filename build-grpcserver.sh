#!/bin/sh
#
# Builds the GoLang Dora gRPC server-side executable for one more architectures
#
# Verify required command line arguments are specified
#
if [[ $# -lt 3 ]]; then
    echo "Usage: build.sh Major Minor Patch [os...]"
    echo "Major  = major version number (required)"
    echo "Minor  = minor version number (required)"
    echo "Patch  = minor version number (required)"
    echo "os...  = optional list of OS architectures to build and overrides default -- must match Go compiler's available list (optional)"
    echo "-----"
    echo "Example (basic):  ./build.sh 1 0 4"
    echo "Example (OS override):  ./build.sh 1 0 4 linux"
    exit 1
fi

# Change working directory to script's location (project folder) otherwise script fails.  Will be
# restored to user's original directory at the end of the script.
pushd "$(dirname "$0")"

# Generated binaries will go to the project folder
export GOBIN=$(pwd)

# go generate
#cd assets
#go generate
#cd ..

# Shift off the first three required args to get the major, minor, and patch
declare major=$1
shift
declare minor=$1
shift
declare patch=$1
shift

declare version="$major.$minor.$patch"
echo "Version specified: $version"

declare arch_list="amd64"   # Can also add "386" for 32-bit x86 (use space delimiter)

# Use default OS architectures to build unless overridden by user
#
declare os_list="darwin linux windows"
if [[ -z "$1" ]]; then
    echo "Using default OS list: $os_list"
else
    #
    # User has provided override os architecture list
    #

    # Reset os list for appending from user instead
    os_list=""

    # Read in a variable length list of OS architectures from the user
    while (( "$#" )); do
        os_list="$os_list $1"  # Append with space delimiter
        shift
    done

    echo "Using override OS list: $os_list"
fi

if [[ -z "$os_list" ]]; then
    # Sanity check we have at least one OS to build
    echo "No target OS specified for Go build!"
    exit -1
else
    # Create executables for specified OS list with specified CPU architectures
    #
    for GOOS in $os_list; do
        for GOARCH in $arch_list; do
            output_name='dora-grpcserver-'$GOOS'-'$GOARCH'-v'$version
            if [ $GOOS = "windows" ]; then
                output_name+='.exe'
            fi

            # compile
            env GOOS=$GOOS GOARCH=$GOARCH go build -o $output_name pkg/main.go || { echo "GRPC server Go build failed"; exit 1; }
        done
    done
fi

# Return user to original directory
popd
