module dora

go 1.13

require (
	crawshaw.io/sqlite v0.3.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ldap/ldap/v3 v3.1.5
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v0.0.0-20170306145142-6a5e28554805
	github.com/gorilla/mux v1.7.4
	github.com/jszwec/csvutil v1.4.0
	github.com/mediocregopher/radix/v3 v3.4.2
	github.com/oklog/run v1.1.0
	github.com/opentracing/basictracer-go v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prometheus/client_golang v0.9.3
	github.com/robfig/cron/v3 v3.0.1
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/spf13/afero v1.2.2
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.4.0
	github.com/vmware/govmomi v0.22.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c
	k8s.io/klog/v2 v2.0.0-20200127113903-12be8a0d907a
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
	sourcegraph.com/sourcegraph/appdash-data v0.0.0-20151005221446-73f23eafcf67 // indirect
)
