package balancing

import (
	"dora/pkg/api"
	"dora/pkg/balancingutil"
	"dora/pkg/logger"
	"dora/pkg/util"
	"fmt"
	"strings"
)

//
// Defines data structures for parsing the user given balance rules YAML definition
//

type VmListType int

const (
	VMLIST_CURRENT VmListType = iota
	VMLIST_PROPOSAL
)

// Root node only
type BalancingStruct struct {
	Definitions DefinitionsStruct `yaml:"definitions"`
	Rules       RulesStruct       `yaml:"rules"`
	Constraints ConstraintsStruct `yaml:"constraints"`

	// private internal data field (not used in yaml serialization)
	rackLookup map[int32]string // Will lazily initialized on first use for fast rack mapping look up
}

// Set of rules
type RulesStruct struct {
	Cpu          float64            `yaml:"cpu"`
	Mem          float64            `yaml:"mem"`
	Restrictions RestrictionsStruct `yaml:"restrictions"`
}

// Allows users to constrain the environment in certain ways prior to even applying rules
type ConstraintsStruct struct {
	RackLimit int `yaml:"rackLimit"`
}

// User defines which VCenter tags are considered primary, secondary, or quorum rack
type RackMapStruct struct {
	Primary   string `yaml:"primary"`
	Secondary string `yaml:"secondary"`
	Quorum    string `yaml:"quorum"`
}

type RestrictionsStruct struct {
	Affinity     []AffinityStruct `yaml:"affinity"`
	RackRestrict bool             `yaml:"rackRestriction"`
}

// When NodeType is present then limit the other NodeTypes by their given limits
type AffinityStruct struct {
	NodeType string                `yaml:"nodeType"`
	Limits   []AffinityLimitStruct `yaml:"limits"`
}

type AffinityLimitStruct struct {
	NodeType string `yaml:"nodeType"`
	Max      int    `yaml:"max"`
}

type DefinitionsStruct struct {
	VmType      VmTypeStruct  `yaml:"vmTypes"`
	RackMapping RackMapStruct `yaml:"rackMapping"`
}

type VmTypeStruct struct {
	HeavyWriters []VmType `yaml:"heavyWriters"`
	HeavyReaders []VmType `yaml:"heavyReaders"`
	Sensitive    []VmType `yaml:"sensitive"`

	// Fast vmType lookup maps and will lazily initialized once on first use and re-used repeatedly afterwards
	_mapHeavyWriters map[string]VmType // Use getter method only to access
	_mapHeavyReaders map[string]VmType // Use getter method only to access
	_mapSensitive    map[string]VmType // Use getter method only to access
}

type VmType struct {
	Order int `yaml:"order"`
	RegEx []string `yaml:"regex"`
	HypervisorTags []string `yaml:"hypervisors"`
}

//
// Utility Methods
//
func (o *VmTypeStruct) GetHeavyWritersMap() map[string]VmType {
	if o._mapHeavyWriters == nil || len(o._mapHeavyWriters) == 0 {
		// Lazy instantiation on first use
		//
		o._mapHeavyWriters = make(map[string]VmType)
		for _, nodeType := range o.HeavyWriters {
			for _, regex := range nodeType.RegEx {
				o._mapHeavyWriters[regex] = nodeType
			}
		}
	}

	return o._mapHeavyWriters
}

func (o *VmTypeStruct) GetHeavyReadersMap() map[string]VmType {
	if o._mapHeavyReaders == nil || len(o._mapHeavyReaders) == 0 {
		// Lazy instantiation on first use
		//
		o._mapHeavyReaders = make(map[string]VmType)
		for _, nodeType := range o.HeavyReaders {
			for _, regex := range nodeType.RegEx {
				o._mapHeavyReaders[regex] = nodeType
			}
		}
	}

	return o._mapHeavyReaders
}

func (o *VmTypeStruct) GetSensitiveMap() map[string]VmType {
	if o._mapSensitive == nil || len(o._mapSensitive) == 0 {
		// Lazy instantiation on first use
		//
		o._mapSensitive = make(map[string]VmType)
		for _, nodeType := range o.Sensitive {
			for _, regex := range nodeType.RegEx {
				o._mapSensitive[regex] = nodeType
			}
		}
	}

	return o._mapSensitive
}

// Returns either total "current" or "proposed" VM lists (from all specified hypervisors) based on the enumerated type value
//
func (o *BalancingStruct) getVmListFromType(vmListType VmListType, hypervisorRecs ...*api.HypervisorLoadOut) []*api.VirtualMachineLoad {
	if vmListType == VMLIST_PROPOSAL {
		// Concatenate all lists from specified hypervisors
		var list = []*api.VirtualMachineLoad{}

		for i := 0; i < len(hypervisorRecs); i++ {
			list = append(list, hypervisorRecs[i].VmProposed...)
		}

		return list
	} else {
		// Concatenate all lists from specified hypervisors
		var list = []*api.VirtualMachineLoad{}

		for i := 0; i < len(hypervisorRecs); i++ {
			list = append(list, hypervisorRecs[i].VmCurrent...)
		}

		return list
	}
}

// Returns all CPU rule violations currently on the given hypervisor or empty list if no violations found
func (o *BalancingStruct) ValidateCpu(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)

	// NOTE: CPU will be 0 if rule is *not* specified
	if o.Rules.Cpu > 0 {
		var cpuSum int32

		if hypervisorRec.VmProposed != nil {
			for _, vmRec := range o.getVmListFromType(listType, hypervisorRec) {
				cpuSum += vmRec.Cpu
			}
		}

		if float64(cpuSum) > float64(hypervisorRec.Cpu)*o.Rules.Cpu {
			violations = append(violations, &api.BalanceRuleViolation{
				Name:        "CPU",
				Description: fmt.Sprintf("Total VM allocation %d exceeds threshold %.2f", cpuSum, float64(hypervisorRec.Cpu)*o.Rules.Cpu),
			})
		}
	}

	// NOTE: violations list will empty if no violations found
	return violations
}

// Returns all MEM rule violations currently on the given hypervisor or empty list if no violations found
func (o *BalancingStruct) ValidateMem(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)

	// NOTE: MEM will be 0 if rule is *not* specified
	if o.Rules.Mem > 0 {
		var memGBSum float32

		if hypervisorRec.VmProposed != nil {
			for _, vmRec := range o.getVmListFromType(listType, hypervisorRec) {
				memGBSum += vmRec.MemGb
			}
		}

		if float64(memGBSum) > float64(hypervisorRec.MemGb)*o.Rules.Mem {
			violations = append(violations, &api.BalanceRuleViolation{
				Name:        "MEM",
				Description: fmt.Sprintf("Total VM allocation %.2f GB exceeds threshold %.2f GB", memGBSum, float64(hypervisorRec.MemGb)*o.Rules.Mem),
			})
		}
	}

	// NOTE: violations list will empty if no violations found
	return violations
}

// Returns all rack rule violations currently on the given hypervisor or empty list if no violations found
//
// NOTE: rack restrictions do not apply to "sensitive" VM node types
func (o *BalancingStruct) ValidateRackMapping(hypervisorRec *api.HypervisorLoadOut, hypervisorLoadOutList []*api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)

	if o.Rules.Restrictions.RackRestrict {
		// Perform O(n^2) search for other node types that are in this list
		// NOTE: performance could benefit from a hash index created once a start that indexes existing rackTag => node
		for _, vmRec1 := range o.getVmListFromType(listType, hypervisorRec) {
			if vmRec1.GetVmType() != balancingutil.VMTYPE_SENSITIVE { // Sensitive nodes have no rack restriction to allow them to pack into a few hypervisors as possible
				var vmRec1NodeType = balancingutil.ParseNodeType(vmRec1.GetHostname())

				for _, vmRec2 := range o.getVmListFromType(listType, filterHypervisors(hypervisorRec.Rack, hypervisorLoadOutList)...) {
					// Linear search.  Check against *other* VMs (not itself)
					if vmRec1.Hostname != vmRec2.Hostname {
						var vmRec2NodeType = balancingutil.ParseNodeType(vmRec2.GetHostname())

						if vmRec1NodeType != nil && vmRec2NodeType != nil {
							if vmRec1NodeType.NodeType == vmRec2NodeType.NodeType {
								if vmRec1NodeType.Leg == vmRec2NodeType.Leg {
									// Rack violation found: another node of same nodetype and same leg was found in the same
									// rack. Not necessarily same hypervisor and can be a different hypervisor in the same rack.
									violations = append(violations, &api.BalanceRuleViolation{
										Name:        "Rack",
										Description: fmt.Sprintf("Node %s is on same rack '%s' as Node %s with same leg %d", vmRec1.GetHostname(), hypervisorRec.GetRack(), vmRec2.GetHostname(), vmRec1NodeType.Leg),
									})

									break
								}
							}
						}
					}
				}
			}
		}
	}

	// NOTE: violations list will be empty if no violations found
	return violations
}

// Returns all Tag rule violations currently on the given hypervisor or empty list if no violations found
func (o *BalancingStruct) ValidateHypervisorTags(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)

	if hypervisorRec.GetTags() != nil && len(hypervisorRec.GetTags()) > 0 {
		var hypervisorTagsJoinedString = strings.Join(hypervisorRec.GetTags(), ", ")
		var list []*api.VirtualMachineLoad

		// Select the VM list to operate on based on VmList argument
		if listType == VMLIST_PROPOSAL {
			list = hypervisorRec.GetVmProposed()
		} else {
			list = hypervisorRec.GetVmCurrent()
		}

		// Check the list of VMs on this hypervisor for tag violations
		for _, vmRec := range list {
			if len(util.Intersection(hypervisorRec.GetTags(), vmRec.GetTags())) == 0 {
				// Tag violation: This VM does not have any tags that match the hypervisor's tags so the VM should not be placed on this reserved hypervisor
				violations = append(violations, &api.BalanceRuleViolation{
					Name:        "TAG",
					Description: fmt.Sprintf("VM %s does not have any of hypervisors tags: %s", vmRec.GetHostname(), hypervisorTagsJoinedString),
				})
			}
		}
	}

	// NOTE: violations list will be empty if no violations found
	return violations
}

// Returns the given hypervisor list filtered down to hypervisors that match the given rack tag string.  May return
// an empty list if no matches found or the original list was empty.
//
func filterHypervisors(rackTag string, hypervisorRecs []*api.HypervisorLoadOut) []*api.HypervisorLoadOut {
	var list = make([]*api.HypervisorLoadOut, 0)

	if hypervisorRecs != nil {
		// Linear search
		for i := 0; i < len(hypervisorRecs); i++ {
			var hypervisorRec = hypervisorRecs[i]

			if hypervisorRec.Rack == rackTag {
				list = append(list, hypervisorRec)
			}
		}
	}

	return list
}

// Returns all affinity violations currently on the given hypervisor or empty list if no violations found
//
func (o *BalancingStruct) ValidateAffinity(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var affinityList = o.Rules.Restrictions.Affinity
	var vmList = o.getVmListFromType(listType, hypervisorRec)
	var violations = make([]*api.BalanceRuleViolation, 0)

	if affinityList != nil {
		for _, affinityRule := range affinityList {
			if o.getNodeTypeCount(vmList, affinityRule.NodeType) > 0 {
				if affinityRule.Limits != nil {
					for _, nodeLimitRule := range affinityRule.Limits {
						if o.getNodeTypeCount(vmList, nodeLimitRule.NodeType) > nodeLimitRule.Max {
							// Max affinity exceeded for given node type affinity rule
							violations = append(violations, &api.BalanceRuleViolation{
								Name:        "Affinity",
								Description: fmt.Sprintf("Node type %s exceeds limit of %d with node type %s", affinityRule.NodeType, nodeLimitRule.Max, nodeLimitRule.NodeType),
							})
						}
					}
				}
			}
		}
	}

	// NOTE: violations list will empty if no violations found
	return violations
}

// Returns all "Sensitive" VmType violations currently on the given hypervisor or empty list if no violations found
//
func (o *BalancingStruct) ValidateSensitive(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)

	if len(o.Definitions.VmType.Sensitive) > 0 {
		var vmList = o.getVmListFromType(listType, hypervisorRec)
		var sensitiveList = make([]*api.VirtualMachineLoad, 0)
		var heavyReaderList = make([]*api.VirtualMachineLoad, 0)
		var heavyWriterList = make([]*api.VirtualMachineLoad, 0)

		// Separate the hypervisor's VM list into "sensitive", "heavy writer", and "heavy reader" lists
		for _, vmRec := range vmList {
			switch vmRec.GetVmType() {
			case balancingutil.VMTYPE_SENSITIVE:
				sensitiveList = append(sensitiveList, vmRec)
				break
			case balancingutil.VMTYPE_HEAVY_READERS:
				heavyReaderList = append(heavyReaderList, vmRec)
				break
			case balancingutil.VMTYPE_HEAVY_WRITERS:
				heavyWriterList = append(heavyWriterList, vmRec)
				break
			}
		}

		// Error state is if the hypervisor has Sensitive node types are mixed with *either* HeavyWriter or HeavyReader
		// node types.  Generate appropriate violation messages when encountered.
		if len(sensitiveList) > 0 && (len(heavyReaderList) > 0 || len(heavyWriterList) > 0) {
			for _, vmRec := range heavyReaderList {
				// Violation: Sensitive vmType mixed with HeavyReader
				violations = append(violations, &api.BalanceRuleViolation{
					Name:        "Sensitive",  // violation category
					Description: fmt.Sprintf("HeavyReader node type %s found with sensitive node type(s) on same hypervisor", vmRec.GetHostname()),
				})
			}

			for _, vmRec := range heavyWriterList {
				// Violation: Sensitive mixed with HeavyWriter
				violations = append(violations, &api.BalanceRuleViolation{
					Name:        "Sensitive",  // violation category
					Description: fmt.Sprintf("HeavyWriter node type %s found with sensitive node type(s) on same hypervisor", vmRec.GetHostname()),
				})
			}
		}
	} else {
		logger.Log.Info("Sensitive VM restriction disabled or no values set")
	}

	// NOTE: violations list will empty if no violations found
	return violations
}

// Returns all VCenter move incompatibility violations currently on the given hypervisor or empty list if no violations
// found
//
// Current incompatibility checks are:
// (1) Datastore
// (2) Network
// Where the hypervisor *must* contain *all* of the datastores and networks specified by the VM otherwise an
// incompatibility is detected and a violation generated
func (o *BalancingStruct) ValidateMoveIncompatible(hypervisorRec *api.HypervisorLoadOut, listType VmListType) []*api.BalanceRuleViolation {
	var violations = make([]*api.BalanceRuleViolation, 0)
	var violationCategory = "Compatibility"

	// Choose from 1 of the 2 VM lists of the hypervisor (proposed vs current)
	var vmList = o.getVmListFromType(listType, hypervisorRec)

	for _, vmRec := range vmList {
		//
		// Check datastores
		//

		// Subtract VM's datastore URLs.  If the Hypervisor contains all the datastore of the VM then the difference
		// will be zero.  Otherwise, datastore move violation is found.
		var diffDatastores = util.StringArraySubtraction(getDatastoreUrls(vmRec.GetDatastores()), getDatastoreUrls(hypervisorRec.GetDatastores())...)
		if diffDatastores != nil && len(diffDatastores) > 0 {
			// Violation: At least one Datastore of the VM was *not* found in the Hypervisor
			violations = append(violations, &api.BalanceRuleViolation{
				Name:        violationCategory,  // violation category
				Description: fmt.Sprintf("VM %s has datastores %s not present on hypervisor", vmRec.GetHostname(), strings.Join(getDatastoreUrls(vmRec.GetDatastores()), ",")),
			})
		}

		//
		// Check networks
		//
		if len(networkIntersect(vmRec.GetNetworks(), hypervisorRec.GetNetworks())) == 0 {
			// Violation: At least one Network of the VM was *not* found in the Hypervisor
			violations = append(violations, &api.BalanceRuleViolation{
				Name:        violationCategory,  // violation category
				Description: fmt.Sprintf("VM %s has network %s not present on hypervisor", vmRec.GetHostname(), strings.Join(vmRec.GetNetworks(), ",")),
			})
		}
	}

	return violations
}

// Extracts just the URL from the list of datastores and returns the list of URLs
//
func getDatastoreUrls(datastores []*api.Datastore) []string {
	var urls = make([]string, 0)

	if datastores != nil {
		for _, datastore := range datastores {
			urls = append(urls, datastore.GetDatastoreUrl())
		}
	}

	return urls
}

// Uses a hashmap for faster lookup of the rack mapping. It will create the hashmap on first use as necessary but
// later lookups will use the hashmap for fast lookup.
//
func (o *BalancingStruct) getRackMapFastMap() map[int32]string {
	// Lazy instantiation (one-time)
	if o.rackLookup == nil {
		o.rackLookup = make(map[int32]string)
		o.rackLookup[1] = o.Definitions.RackMapping.Primary
		o.rackLookup[2] = o.Definitions.RackMapping.Secondary
		o.rackLookup[3] = o.Definitions.RackMapping.Quorum
	}

	return o.rackLookup
}

// Forces the (re)initialize the fast rack lookup map
func (o *BalancingStruct) InitRackLookup() {
	// Clear out existing caching map
	o.rackLookup = nil

	// Call the getter which automatically re-initializes cache map when it is null
	o.getRackMapFastMap()
}

func (o *BalancingStruct) getNodeTypeCount(vmList []*api.VirtualMachineLoad, nodeType string) int {
	var count = 0

	if vmList != nil {
		for _, vmRec := range vmList {
			if vmRec.NodeType == nodeType {
				count++
			}
		}
	}

	return count
}
