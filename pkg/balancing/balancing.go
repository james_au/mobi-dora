package balancing

import (
	"dora/pkg/addressbook"
	"dora/pkg/api"
	"dora/pkg/balancingutil"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/util"
	"errors"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gopkg.in/yaml.v3"
	"sort"
	"strings"
)

// Unmarshals the user provided YAML text of rules
//
// NOTE: User will have to deal with unmarshalling errors as they are likely YAML syntax or type errors.  If no
// rules text was given then a nil reference is returned instead.
//
func ReadBalanceRules(rulesText string) (*BalancingStruct, error) {
	if util.IsBlank(rulesText) {
		logger.Log.Info("Cannot parse balance rules because no rule data was provided")
		return nil, nil
	} else {
		// Unmarshal as normal
		//
		var balancingStruct = BalancingStruct{}
		var errUnmarshal = yaml.Unmarshal([]byte(rulesText), &balancingStruct)

		return &balancingStruct, errUnmarshal
	}
}

// Enume to determine to balance from the VM "current list" (normal) or from the VM "unplaced" list
type VmListSource int

const (
	SOURCE_FROM_CURRENT  VmListSource = iota
	SOURCE_FROM_UNPLACED VmListSource = iota
)

// Performs the balancing operation on the given hypervisor load out DTO
//
// Results are returned in the same DTO object
//
func Balance(balancingRules *BalancingStruct, balanceDTO *api.HypervisorLoadOutDTO, addressBook *addressbook.AddressBookStruct, source VmListSource) error {
	var hypervisorLoadOuts = balanceDTO.GetHypervisorLoadOuts()

	// Ensure hypervisor list is sorted by hostname to help with consistent sorting
	// NOTE: the hypervisor list should already be sorted but need to ensure it remains sorted in case the client
	// has changed the ordering in some way.
	sort.Slice(hypervisorLoadOuts, func(i int, j int) bool {
		return hypervisorLoadOuts[i].Hostname < hypervisorLoadOuts[j].Hostname
	})

	if hypervisorLoadOuts == nil {
		// Fatal
		return errors.New(fmt.Sprint("Cannot perform balancing.  Hypervisor layout data was not provided."))
	} else if balancingRules == nil {
		// Fatal
		return errors.New(fmt.Sprint("Cannot perform balancing.  Balance rules were not provided."))
	}

	// Apply the rules definitions to the hypervisors & VM list.  This is a required pre-processing step prior to
	// performing the actual balancing procedure.
	DefineVMsForBalancing(
		&api.HypervisorLoadOutList{
			Load: hypervisorLoadOuts,
			RackMapping: nil,   // Rack mapping will be populated after the call compeltes (side effect)
		},
		balanceDTO.GetVmUnplaced(),
		addressBook,
		balancingRules,
		balanceDTO.GetVcenterName())

	var vmRecList []*api.VirtualMachineLoad

	//
	// Generate the source list of VMs which can come from the hypervisor's "current" VMs or from ust the "unplaced" VMs
	// NOTE: that the "current" mode will also include any "unplaced" VMs since the hypervisor's "current" list of VMs
	// is static.
	//
	//
	if source == SOURCE_FROM_CURRENT {
		// Clear any proposed list contents on previous balance
		for _, hypervisorRec := range hypervisorLoadOuts {
			hypervisorRec.VmProposed = []*api.VirtualMachineLoad{}
			hypervisorRec.ProposedRoomCpu.RoomAvailable = hypervisorRec.Cpu
			hypervisorRec.ProposedRoomCpu.VmTotal = 0
			hypervisorRec.ProposedRoomMemGb.RoomAvailable = hypervisorRec.MemGb
			hypervisorRec.ProposedRoomMemGb.VmTotal = 0
			hypervisorRec.RulesViolations = []*api.BalanceRuleViolation{}
		}

		// Get entire VM list across _all_ hypervisors.  These are the VMs that need to be placed into balanced
		// configuration across the hypervisors.
		vmRecList = GenVMList(hypervisorLoadOuts)
	} else if source == SOURCE_FROM_UNPLACED {
		for _, hypervisorRec := range hypervisorLoadOuts {
			hypervisorRec.ProposedRoomCpu.RoomAvailable = hypervisorRec.Cpu
			hypervisorRec.ProposedRoomCpu.VmTotal = 0
			hypervisorRec.ProposedRoomMemGb.RoomAvailable = hypervisorRec.MemGb
			hypervisorRec.ProposedRoomMemGb.VmTotal = 0
			hypervisorRec.RulesViolations = []*api.BalanceRuleViolation{}
		}

		// User is only requesting to balance the "unplaced list".  Make copy of the elements because the
		// original list is going to be cleared out.
		vmRecList = make([]*api.VirtualMachineLoad, 0)
		vmRecList = append(vmRecList, balanceDTO.VmUnplaced...)
	}

	// Clear unplaced VMs as they will be updated after balancing execution completes below
	balanceDTO.VmUnplaced = []*api.VirtualMachineLoad{}

	// Sort the VM list by weight which causes the heaviest VMs to be placed first which optimizes the balance flow
	sortVMListByBalanceSortOrderAndName(vmRecList...)

	// Iterate through VM list and place node types in the following order: Sensitive, HeavyWriter, HeavyReader, &
	// normal checking for balance rule violations
	var currHypervisorStartIndex = 0
	var successfulPlacement *api.HypervisorLoadOut
	for _, vmRec := range vmRecList {
		successfulPlacement = nil
		var circulatedList = circulateHypervisorList(hypervisorLoadOuts, currHypervisorStartIndex, vmRec)
		for _, hypervisorRec := range circulatedList {
			// Place current VM in current Hypervisor's proposal list and check for rules violations.  If it causes
			// at least 1 rule violation then remove the VM and repeat for the next hypervisor.
			//
			hypervisorRec.VmProposed = append(hypervisorRec.VmProposed, vmRec)

			//
			// Check for rules violations and try next available Hypervisor as necessary
			//

			var ruleViolations = make([]*api.BalanceRuleViolation, 0)

			// Validate VM CPU load allocation
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateCpu(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// Validate VM MEM load allocation
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateMem(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// Validate VM affinity
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateAffinity(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// Validate Sensitive restriction
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateSensitive(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// Validate rack mapping
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateRackMapping(hypervisorRec, hypervisorLoadOuts, VMLIST_PROPOSAL)...)
			}

			// Validate hypervisor "tag" restrictions
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateHypervisorTags(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// Validate "move compatibility" issues.  This guards against VM moves that would actually fail in VCenter migration.
			if len(ruleViolations) == 0 {
				appendIfNotNil(&ruleViolations, balancingRules.ValidateMoveIncompatible(hypervisorRec, VMLIST_PROPOSAL)...)
			}

			// If any rules violations detected, then remove the VM from the proposal list and try the VM on the next hypervisor
			if len(ruleViolations) > 0 {
				// Remove the VM from the hypervisor's proposed list since it broke rules
				hypervisorRec.VmProposed = hypervisorRec.VmProposed[:len(hypervisorRec.VmProposed)-1]

				// Continue the loop to try the next hypervisor
				continue
			} else {
				// VM was successfully placed without any rules violations
				//
				successfulPlacement = hypervisorRec
				currHypervisorStartIndex++
				if currHypervisorStartIndex >= len(hypervisorLoadOuts) {
					// Wrap around to zero if reached the end of the hypervisor loadout list
					currHypervisorStartIndex = 0
				}

				// Update room totals
				hypervisorRec.GetProposedRoomCpu().VmTotal += vmRec.Cpu
				hypervisorRec.GetProposedRoomMemGb().VmTotal += vmRec.MemGb
				hypervisorRec.GetProposedRoomCpu().RoomAvailable = hypervisorRec.Cpu - hypervisorRec.GetProposedRoomCpu().VmTotal
				hypervisorRec.GetProposedRoomMemGb().RoomAvailable = hypervisorRec.MemGb - hypervisorRec.GetProposedRoomMemGb().VmTotal

				if float64(hypervisorRec.GetProposedRoomCpu().RoomAvailable) < float64(hypervisorRec.GetCpu())*(1-balancingRules.Rules.Cpu) {
					hypervisorRec.GetProposedRoomCpu().Severity = constants.SEVERITY_CRITICAL
				}

				if float64(hypervisorRec.GetProposedRoomMemGb().RoomAvailable) < float64(hypervisorRec.GetMemGb())*(1-balancingRules.Rules.Mem) {
					hypervisorRec.GetProposedRoomMemGb().Severity = constants.SEVERITY_CRITICAL
				}

				break
			}
		}

		if successfulPlacement == nil {
			// VM could not be placed any hypervisor that doesn't break a rule
			logger.Log.Info(fmt.Sprintf("VM: %s could not be placed in hypervisor", vmRec.GetHostname()))

			// Store VM in a "unplaceable" list for user to manually place in the UI
			balanceDTO.VmUnplaced = append(balanceDTO.VmUnplaced, vmRec)
		} else {
			logger.Log.Info(fmt.Sprintf("VM: %s successfully placed in Hypervisor host: %s", vmRec.GetHostname(), successfulPlacement.GetHostname()))
		}
	}

	if balanceDTO.GetReduceMoves() {
		logger.Log.Info("Performing 'move reduction' procedure...")
		reduceMoves(balancingRules, hypervisorLoadOuts)
	} else {
		logger.Log.Info("Not performing 'move reduction' procedure")
	}

	// No errors
	return nil
}

// Scans through the rules data structure and returns a list of all detected VM-to-hypervisor tags
//
func GetVmTags(rules *BalancingStruct) (results []string) {
	if rules != nil {
		for _, vmType := range rules.Definitions.VmType.HeavyWriters {
			results = append(results, vmType.HypervisorTags...)
		}
		for _, vmType := range rules.Definitions.VmType.HeavyReaders {
			results = append(results, vmType.HypervisorTags...)
		}
		for _, vmType := range rules.Definitions.VmType.Sensitive {
			results = append(results, vmType.HypervisorTags...)
		}
	}

	return
}

// Performs a "check only" balance check and will update the DTO with the list of violations per hypervisor record
//
// No changes to the given balancing is performed.  Check only with updated violations list.
//
func BalanceCheckOnly(balancingRules *BalancingStruct, balanceDTO *api.HypervisorLoadOutDTO) ([]*api.HypervisorLoadOut, error) {
	// Process each hypervisor record's VM list and check for balance violations
	for _, hypervisorRec := range balanceDTO.GetHypervisorLoadOuts() {
		hypervisorRec.GetProposedRoomCpu().VmTotal = 0
		hypervisorRec.GetProposedRoomMemGb().VmTotal = 0
		hypervisorRec.GetProposedRoomCpu().RoomAvailable = hypervisorRec.Cpu
		hypervisorRec.GetProposedRoomMemGb().RoomAvailable = hypervisorRec.MemGb

		// Create a new blank rules violation list for this hypervisor which will also clear out the
		// hypervisor's existing violations
		hypervisorRec.RulesViolations = []*api.BalanceRuleViolation{}

		// Iterate through this hypervisor's VM list while updating rules violations & totals
		for _, vmRec := range hypervisorRec.VmProposed {
			//
			// Update CPU/MEM/Room totals
			//
			hypervisorRec.GetProposedRoomCpu().VmTotal += vmRec.Cpu
			hypervisorRec.GetProposedRoomMemGb().VmTotal += vmRec.MemGb
			hypervisorRec.GetProposedRoomCpu().RoomAvailable = hypervisorRec.Cpu - hypervisorRec.GetProposedRoomCpu().VmTotal
			hypervisorRec.GetProposedRoomMemGb().RoomAvailable = hypervisorRec.MemGb - hypervisorRec.GetProposedRoomMemGb().VmTotal

			if float64(hypervisorRec.GetProposedRoomCpu().RoomAvailable) < float64(hypervisorRec.GetCpu())*(1-balancingRules.Rules.Cpu) {
				hypervisorRec.GetProposedRoomCpu().Severity = constants.SEVERITY_CRITICAL
			} else {
				hypervisorRec.GetProposedRoomCpu().Severity = constants.SEVERITY_OK
			}

			if float64(hypervisorRec.GetProposedRoomMemGb().RoomAvailable) < float64(hypervisorRec.GetMemGb())*(1-balancingRules.Rules.Mem) {
				hypervisorRec.GetProposedRoomMemGb().Severity = constants.SEVERITY_CRITICAL
			} else {
				hypervisorRec.GetProposedRoomMemGb().Severity = constants.SEVERITY_OK
			}

			//
			// Check for rules violations
			//

			// Validate VM CPU load allocation
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateCpu(hypervisorRec, VMLIST_PROPOSAL)...)

			// Validate VM MEM load allocation
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateMem(hypervisorRec, VMLIST_PROPOSAL)...)

			// Validate VM affinity
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateAffinity(hypervisorRec, VMLIST_PROPOSAL)...)

			// Validate Sensitive vmType restriction
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateSensitive(hypervisorRec, VMLIST_PROPOSAL)...)

			// Validate rack mapping
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateRackMapping(hypervisorRec, balanceDTO.HypervisorLoadOuts, VMLIST_PROPOSAL)...)

			// Validate hypervisor tag restrictions
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateHypervisorTags(hypervisorRec, VMLIST_PROPOSAL)...)

			// Validate "move compatibility" issues.  This guards against VM moves that would actually fail in VCenter migration.
			appendIfNotNil(&hypervisorRec.RulesViolations, balancingRules.ValidateMoveIncompatible(hypervisorRec, VMLIST_PROPOSAL)...)

			//
			// Check for VM move "incompatibility" either from incompatible Network or incompatible Datastore
			//
			if len(networkIntersect(vmRec.GetNetworks(), hypervisorRec.GetNetworks())) == 0 {
				// Mark that this VM is incompatible for move to this hypervisor based on Network incompatibility
				hypervisorRec.MoveWarnNetwork = append(hypervisorRec.MoveWarnNetwork, vmRec)
			}

			// Check for Datastore move/migration compatibility by checking the Hypervisor has all the datastores
			// referenced by the VM
			if !util.IsStringArraySubset(collectDatastoreUrls(vmRec.GetDatastores()), collectDatastoreUrls(hypervisorRec.GetDatastores())...) {
				// Mark that this VM is incompatible for move to this hypervisor
				hypervisorRec.MoveWarnDatastore = append(hypervisorRec.MoveWarnDatastore, vmRec)
			}
		}
	}

	// Remove any duplicate violation records that can happen from separate runs of rule checks on a hypervisor's
	// VM list.
	// PERFORMANCE NOTE: This is a one-time O(n^2) operation post balance so performance is not problem
	for _, hypervisorRec := range balanceDTO.GetHypervisorLoadOuts() {
		var mapDistinct = make(map[string]*api.BalanceRuleViolation)

		// Use a map to de-duplicate keys
		for _, ruleViolationRec := range hypervisorRec.RulesViolations {
			var key = getRuleViolationString(ruleViolationRec)
			if _, keyFound := mapDistinct[key]; !keyFound {
				// Key doesn't exist so insert it (no duplicate)
				mapDistinct[key] = ruleViolationRec
			}
		}

		// Replace hypervisor's existing rules map by replace with de-duplicated version
		hypervisorRec.RulesViolations = []*api.BalanceRuleViolation{} // clear any existing first
		for _, value := range mapDistinct {
			// Replace with de-dupped
			hypervisorRec.RulesViolations = append(hypervisorRec.RulesViolations, value)
		}
	}

	return balanceDTO.HypervisorLoadOuts, nil
}

// Performs an intersection between two lists of networks to produce common networks between the two lists.
//
// NOTE: Networks are compared as substring so short name "VM-324" will match full path "/INFRA/network/VM-324 Network"
// NOTE: A result that is 0 length means there are not common networks between the two lists
//
// PARAM: networks1   network list (may or may not be "full path")
// PARAM: networks2   network list (may or may not be "full path")
// RETURN: string array that is intersection of common network paths between the two lists.  A 0 size array means there are no common networks.
func networkIntersect(networks1 []string, networks2 []string) (intersection []string) {
	intersection = make([]string, 0)

	// Search is approximately O(n^2)
	for _, network := range networks1 {
		if networkContains(network, networks2) {
			intersection = append(intersection, network)
		}
	}

	return
}

// Determines if the given network name (full path or not) is contained within the network paths
//
// NOTE: Networks are compared as substring so short name "VM-324" will match full path "/INFRA/network/VM-324 Network"
//
// PARAM: network  a network (may or may not be "full path")
// PARAM: networks  network list (may or may not be "full path")
// RETURN: true if the network is found within the given list and false otherwise
func networkContains(network string, networks []string) bool {
	if networks != nil {
		// Linear search.  Return on first match.
		for _, n := range networks {
			if strings.HasSuffix(strings.TrimSpace(n), strings.TrimSpace(network)) {
				return true
			}
		}
	}

	// No match found if this point reached
	return false
}

// Extracts just the URL fields of the given array of datastore records.  This is then used to
// make runtime check if one set is a subset of the other for VM move compatibility checks.
//
func collectDatastoreUrls(datastores []*api.Datastore) []string {
	var datastoreUrls = make([]string, 0)

	for _, datastore := range datastores {
		datastoreUrls = append(datastoreUrls, datastore.GetDatastoreUrl())
	}

	return datastoreUrls
}

func getRuleViolationString(ruleViolation *api.BalanceRuleViolation) string {
	if ruleViolation != nil {
		return ruleViolation.GetName() + ":" + ruleViolation.GetDescription()
	} else {
		return ""
	}
}

// Simple macro to append a string to a given array only if the string is *not* blank
func appendIfNotBlank(array *[]string, s string) {
	if !util.IsBlank(s) {
		*array = append(*array, s)
	}
}

// Simple macro to append a string to a given array only if the string is *not* blank
func appendIfNotNil(originalList *[]*api.BalanceRuleViolation, elements ...*api.BalanceRuleViolation) {
	if elements != nil && len(elements) > 0 {
		for _, ele := range elements {
			if ele != nil {
				*originalList = append(*originalList, ele)
			}
		}
	}
}

// Circulates the hypervisor source list starting at index 'n' where 0 <= n < len(hypervisor source list).
// NOTE: Circulation is disabled for "sensitive" node types and the unmodified list is returned so that "sensitive" node
// types can be concentrated on fewer hypervisors.
// NOTE: If VM node type is set with a hypervisor tag, the VM will only be placed in those tagged hypervisors.  If such
// such hypervisors are tagged or all tagged hypervisors are "full" then the VM will end up as "unplaced"
//
func circulateHypervisorList(hypervisorLoadOutList []*api.HypervisorLoadOut, n int, vmRec *api.VirtualMachineLoad) []*api.HypervisorLoadOut {
	// If VM specifies a tag then restrict the VM to only hypervisors that are tagged with matching name
	if vmRec.GetTags() != nil && len(vmRec.GetTags()) > 0 {
		// Filter the Hypervisor list
		hypervisorLoadOutList = filterHypervisorList(hypervisorLoadOutList, vmRec.GetTags()...)
	} else {
		// Filter out hypervisors that are tagged because only matching VM can be placed on them
		hypervisorLoadOutList = filterHypervisorListRemoveTagged(hypervisorLoadOutList)
	}

	// Locate the given VM in any of the hypervisors first.  This gives the balancer algorithm a tendency to keep
	// VM's in the original locations versus moving them to another VM.  VM moves are time consuming and can be risky
	// especially in production environments.
	var hypervisorIndexContainingVM = findVm(hypervisorLoadOutList, vmRec)

	// Override circulation to the found hypervisor so the VM is checked on its original hypervisor first before
	// trying other hypervisors.  This increases the chance the VM does not end up moving in the balance operation.
	if hypervisorIndexContainingVM > -1 {
		n = hypervisorIndexContainingVM
	} else {
		n = util.Min(len(hypervisorLoadOutList), n)
	}

	if vmRec.GetVmType() != balancingutil.VMTYPE_SENSITIVE || hypervisorIndexContainingVM > -1 {
		// Create new empty list to contain circulated hypervisor list
		var circulatedList = make([]*api.HypervisorLoadOut, 0)

		// Process first half starting at the designated index up to the end of the list
		for i := n; i < len(hypervisorLoadOutList); i++ {
			circulatedList = append(circulatedList, hypervisorLoadOutList[i])
		}

		// Process second half which is the first half of the list up to and not including the index position
		for i := 0; i < n; i++ {
			circulatedList = append(circulatedList, hypervisorLoadOutList[i])
		}

		return circulatedList
	} else {
		// Do not circulate the list if the VM is a "Sensitive" VMT ype as they should group together on the same hypervisor
		// as much as possible and *not* be distributed like other VM types
		return hypervisorLoadOutList // return list as-is (no circulation of values)
	}
}

// Filters the given hypervisor load out list to only hypervisors that have the given tags.
//
// NOTE: the returned filtered list might become zero size but will never be nil
//
func filterHypervisorList(hypervisorLoadOutList []*api.HypervisorLoadOut, tags... string) []*api.HypervisorLoadOut {
	if tags != nil && len(tags) > 0 {
		var filteredList = make([]*api.HypervisorLoadOut, 0)

		// Linear search.  Add matched elements to new returned list as encountered.
		for _, hypervisorLoadOut := range hypervisorLoadOutList {
			if hypervisorLoadOut.GetTags() != nil {
				for _, tag := range tags {
					if util.StringArrayContains(tag, hypervisorLoadOut.GetTags()...) {
						filteredList = append(filteredList, hypervisorLoadOut)
					}
				}
			}
		}

		return filteredList
	} else {
		// No tags were given to restrict.  Just return the original list as-is.
		return hypervisorLoadOutList
	}
}

func filterHypervisorListRemoveTagged(hypervisorLoadOutList []*api.HypervisorLoadOut) []*api.HypervisorLoadOut {
	var filteredList = make([]*api.HypervisorLoadOut, 0)

	// Linear search.  Add matched elements to new returned list as encountered.
	for _, hypervisorLoadOut := range hypervisorLoadOutList {
		if hypervisorLoadOut.GetTags() == nil || len(hypervisorLoadOut.GetTags()) == 0 {
			filteredList = append(filteredList, hypervisorLoadOut)
		}
	}

	return filteredList
}

// Returns the index of the Hypervisor record within the HypervisorLoadOut list that contains the specified VM record.
// If the VM is not found in any hypervisor then a -1 value is returned instead to indicate such
//
func findVm(hypervisorLoadOutList []*api.HypervisorLoadOut, vmRec *api.VirtualMachineLoad) int {
	// Two nested linear search loops.  Return on first match.
	for index, hypervisorLoadOut := range hypervisorLoadOutList {
		for _, vmCurrentRec := range hypervisorLoadOut.GetVmCurrent() {
			if vmCurrentRec.GetPath() == vmRec.GetPath() {
				// Found the VM in the hypervisor at the current index
				return index
			}
		}
	}

	// VM was *not* found in any hypervisor if this point is reached
	return -1;
}

// Sorts the VM list by balance sort order then by VM name.  The order in which the balancer selects VMs has a huge impact
// on the final balance result.  Because VMs balanced first tend to get placed with higher chance of successful
// placement versus VM's placed last. Since Hypervisors fill up as balancing progresses and the hypervisors have
// less space and more VM's already on them near the end of the balance.
//
func sortVMListByBalanceSortOrderAndName(virtualMachineLoads ...*api.VirtualMachineLoad) {
	// Sort the VM list with PRIMARY SORT ASCENDING on user defined "balance sort order" and SECONDARY SORT ASCENDING
	// on VM host
	sort.Slice(virtualMachineLoads, func(i int, j int) bool {
		if virtualMachineLoads[i].GetVmType() != virtualMachineLoads[j].GetVmType() {
			// Primary sort in effect: sort ascending on user defined "balance sort order"
			return virtualMachineLoads[i].GetBalanceSortOrder() < virtualMachineLoads[j].GetBalanceSortOrder()
		} else {
			// VM's with the same sort order are secondarily sorted by name ascendingly
			return strings.Compare(virtualMachineLoads[i].Hostname, virtualMachineLoads[j].Hostname) < 0
		}
	})
}

func GenVMList(hypervisorLoadOuts []*api.HypervisorLoadOut) []*api.VirtualMachineLoad {
	var vmList = make([]*api.VirtualMachineLoad, 0)
	if hypervisorLoadOuts != nil {
		for _, hypervisorRec := range hypervisorLoadOuts {
			if hypervisorRec.VmCurrent != nil {
				for _, vmRec := range hypervisorRec.VmCurrent {
					if util.IsBlank(vmRec.NodeType) {
						// Always keep unidentified VMs on the same hypervisor
						logger.Log.Info(fmt.Sprintf("Unidentified VM '%s' will not be moved", vmRec.GetHostname()))

						// Move the VM directly to this hypervisor's proposed list which keeps the VM on the same hypervisor
						hypervisorRec.VmProposed = append(hypervisorRec.VmProposed, vmRec)
					} else {
						// Normal identified VM
						vmList = append(vmList, vmRec)
					}
				}
			}
		}
	}

	return vmList
}

// This should be run as a post-balance process to reduce the number of moves since an unnecessary move requires a
// longer PCC deployment and introduces risk for the deployment.
//
// This runs in O(n^2) time and should only be run once at the end of a balance.
func reduceMoves(balancingRules *BalancingStruct, hypervisorLoadOut []*api.HypervisorLoadOut) {
	for i := 0; i < len(hypervisorLoadOut); i++ {
		var hypervisorRecSrc = hypervisorLoadOut[i]

		// Iterate through each VM in the hypervisor's "current" list
		for j := 0; j < len(hypervisorRecSrc.GetVmCurrent()); j++ {
			var vmRec = hypervisorRecSrc.GetVmCurrent()[j]
			var hypervisorRecDst = findVmInProposedList(vmRec, hypervisorLoadOut) // Find the destination hypervisor that VM moved to

			// If the VM has moved to another hypervisor then check if the hypervisor's proposed list has an
			// "equivalent node type" that can be "swapped" out to bring the  original VM back.  All swaps are
			// subject to balance check otherwise the swap is undone.
			if hypervisorRecDst != nil && hypervisorRecDst.GetManagedObjRef() != hypervisorRecSrc.GetManagedObjRef() {
				// VM has moved to another hypervisor
				// Now check if there are any "equivalent" nodeTypes in this hypervisor's propose list that are "new"
				// and can be swapped out.
				var nodeType = balancingutil.ParseNodeType(vmRec.GetHostname())
				var newVmList = filterNewNodes(hypervisorRecSrc.GetVmCurrent(), hypervisorRecSrc.GetVmProposed())
				var equivalentVmList = filterNodeTypes(nodeType.NodeType, newVmList) // TODO: nodeType could be null!

				if equivalentVmList != nil && len(equivalentVmList) > 0 {
					// There are equivalent VMs that be swapped out to bring back the original VM
					for k := 0; k < len(equivalentVmList); k++ {
						var equivalentNewVm = equivalentVmList[k]

						// Swap
						logger.Log.Info(fmt.Sprintf("VM swap attempt: original HV(VM): %s(%s) and swapped out HV(VM): %s(%s)", hypervisorRecSrc.GetHostname(), vmRec.GetHostname(), hypervisorRecDst.GetHostname(), equivalentNewVm.GetHostname()))

						//
						// Put equivalent VM on target host (add new node & remove node to bring back)
						hypervisorRecDst.VmProposed = append(hypervisorRecDst.VmProposed, equivalentNewVm)
						deleteVmInList(vmRec, &hypervisorRecDst.VmProposed)

						// Put back old node & remove the equivalent node
						hypervisorRecSrc.VmProposed = append(hypervisorRecSrc.VmProposed, vmRec)
						deleteVmInList(equivalentNewVm, &hypervisorRecSrc.VmProposed)

						//
						// Check balance on *both* current hypervisor and destinaion hypervisor.  Undo the swap
						// if it breaks any balance rules
						if checkReducedBalance(balancingRules, hypervisorRecSrc, hypervisorRecDst) {
							// Undo the move
							logger.Log.Info("...swap failed validation.  Undoing.")
							hypervisorRecDst.VmProposed = append(hypervisorRecDst.VmProposed, vmRec)
							deleteVmInList(equivalentNewVm, &hypervisorRecDst.VmProposed)

							// Put back old node & remove the equivalent node
							hypervisorRecSrc.VmProposed = append(hypervisorRecSrc.VmProposed, equivalentNewVm)
							deleteVmInList(vmRec, &hypervisorRecSrc.VmProposed)
						} else {
							// Swap successful with no rules broken
							logger.Log.Info("...swap successful (no rules violations)")
							break
						}
					}
				}
			}
		}
	}
}

// Categorizes the VMs from the "definitions" section of the rules text.  This is in preparation for subsequent
// balancing operations.  Particularly, this improves balancing coding and speed since the rule definitions have been
// directly applied to each VM record for easy processing.
//
// THIS MUST BE PERFORMED PRIOR TO ANY BALANCE OPERATION
//
func DefineVMsForBalancing(hypervisorLoadOutList *api.HypervisorLoadOutList, unplacedVMs []*api.VirtualMachineLoad, addressBook  *addressbook.AddressBookStruct, rules *BalancingStruct, vcenterName string) error {
	// Update VM Weights based on the rules definition of HeavyReader & HeavyWriter node types. This
	// will set the weight value for all subsequent "balance" or "balance check" calls since the VM
	// list is passed back and forth on subsequent client-server interactions.
	//
	// DEV NOTE: must use regular FOR loop to get element pointer since this operation must be able to mutate the
	// original object. The GoLang FOR/RANGE pattern will *not* work for this since the element is a copy and not a
	// pointer to the original object.

	//
	// Gather all VM records from both the unplaced VM's and the given hypervisors into one large list for
	// further processing
	//
	var vmRecs = make([]*api.VirtualMachineLoad, 0)
	if unplacedVMs != nil {
		// Include any unplaced VMs
		vmRecs = append(vmRecs, unplacedVMs...)
	}
	for i := 0; i < len(hypervisorLoadOutList.Load); i++ {
		var hypervisorRec = hypervisorLoadOutList.Load[i]

		// Include hypervisor current VMs (all)
		vmRecs = append(vmRecs, hypervisorRec.VmCurrent...)
	}

	//
	//
	//
	//
	//
outer:
	for i := 0; i < len(vmRecs); i++ {
		var vmRec = vmRecs[i]

		// Clear/reset any existing tags and vmtype values because they could be incorrect if user has changed the
		// balance rule definitions from a previous run.  These values will be re-computed below.
		vmRec.Tags = []string{}
		vmRec.VmType = balancingutil.VMTYPE_NORMAL

		// Apply VM weight values from the given use balance definitions
		var mapHeavyWriters = rules.Definitions.VmType.GetHeavyWritersMap()
		var mapHeavyReaders = rules.Definitions.VmType.GetHeavyReadersMap()
		var mapSensitive = rules.Definitions.VmType.GetSensitiveMap()
		if _, keyFound := mapHeavyWriters[vmRec.NodeType]; keyFound {
			// This is a HeavyWriter node type
			vmRec.VmType = balancingutil.VMTYPE_HEAVY_WRITERS

			// Set balance sort order based on user defined sort order
			vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.HeavyWriters[0].Order)

			// Set target hypervisor tags (if any) specified by the user rules
			if mapHeavyWriters[vmRec.NodeType].HypervisorTags != nil {
				for _, tagName := range mapHeavyWriters[vmRec.NodeType].HypervisorTags {
					vmRec.Tags = append(vmRec.Tags, tagName)
				}
			}

			continue outer
		} else if _, keyFound := mapHeavyReaders[vmRec.NodeType]; keyFound {
			// This is a HeavyReader type
			vmRec.VmType = balancingutil.VMTYPE_HEAVY_READERS

			// Set balance sort order based on user defined sort order
			vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.HeavyReaders[0].Order)

			// Set target hypervisor tags (if any) specified by the user rules
			if mapHeavyReaders[vmRec.NodeType].HypervisorTags != nil {
				for _, tagName := range mapHeavyReaders[vmRec.NodeType].HypervisorTags {
					vmRec.Tags = append(vmRec.Tags, tagName)
				}
			}

			continue outer
		} else if _, keyFound := mapSensitive[vmRec.NodeType]; keyFound {
			// This is a Sensitive node type
			vmRec.VmType = balancingutil.VMTYPE_SENSITIVE

			// Set balance sort order based on user defined sort order
			vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.Sensitive[0].Order)

			// Set target hypervisor tags (if any) specified by the user rules
			if mapSensitive[vmRec.NodeType].HypervisorTags != nil {
				for _, tagName := range mapSensitive[vmRec.NodeType].HypervisorTags {
					vmRec.Tags = append(vmRec.Tags, tagName)
				}
			}

			continue outer
		} else {
			//
			// No direct nodeType matches were found so try to do hostname match via regexp.
			//

			// Regexp match for "HeavyReaders" VM by hostname
			for _, heavyReaderVmType := range rules.Definitions.VmType.HeavyReaders {
				for _, regex := range heavyReaderVmType.RegEx {
					if isHeavyReader, errRegex := balancingutil.HostRegexMatch(regex, vmRec.GetHostname()); errRegex != nil {
						// Fatal - regex syntax error by user
						var message = fmt.Sprintf("The node expression '%s' in YAML has a parsing error: %v", heavyReaderVmType, errRegex)
						return status.Errorf(codes.InvalidArgument, message)
					} else {
						if isHeavyReader {
							// Set VM's type
							vmRec.VmType = balancingutil.VMTYPE_HEAVY_READERS

							// Set balance sort order based on user defined sort order
							vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.HeavyReaders[0].Order)

							// Set target hypervisor tags (if any) specified by the user rules
							if heavyReaderVmType.HypervisorTags != nil {
								for _, tagName := range heavyReaderVmType.HypervisorTags {
									vmRec.Tags = append(vmRec.Tags, tagName)
								}
							}

							continue outer
						}
					}
				}
			}

			// Regexp match for "Heavy Writers" VM by hostname
			for _, heavyWriterType := range rules.Definitions.VmType.HeavyWriters {
				for _, regex := range heavyWriterType.RegEx {
					if isHeavyWriter, errRegex := balancingutil.HostRegexMatch(regex, vmRec.GetHostname()); errRegex != nil {
						// Fatal - regex syntax error by user
						var message = fmt.Sprintf("The node expression '%s' in YAML has a parsing error: %v", heavyWriterType, errRegex)
						return status.Errorf(codes.InvalidArgument, message)
					} else {
						if isHeavyWriter {
							// Set VM's type
							vmRec.VmType = balancingutil.VMTYPE_HEAVY_WRITERS

							// Set balance sort order based on user defined sort order
							vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.HeavyWriters[0].Order)

							// Set target hypervisor tags (if any) specified by the user rules
							if heavyWriterType.HypervisorTags != nil {
								for _, tagName := range heavyWriterType.HypervisorTags {
									vmRec.Tags = append(vmRec.Tags, tagName)
								}
							}

							continue outer
						}
					}
				}
			}

			// Regexp match for "Sensitive" VM by hostname
			for _, sensitiveVmType := range rules.Definitions.VmType.Sensitive {
				for _, regex := range sensitiveVmType.RegEx {
					if isSensitive, errRegex := balancingutil.HostRegexMatch(regex, vmRec.GetHostname()); errRegex != nil {
						// Fatal - regex syntax error by user
						var message = fmt.Sprintf("The node expression '%s' in YAML has a parsing error: %v", sensitiveVmType, errRegex)
						return status.Errorf(codes.InvalidArgument, message)
					} else {
						if isSensitive {
							// Set VM's type
							vmRec.VmType = balancingutil.VMTYPE_SENSITIVE

							// Set balance sort order based on user defined sort order
							vmRec.BalanceSortOrder = int32(rules.Definitions.VmType.Sensitive[0].Order)

							// Set target hypervisor tags (if any) specified by the user rules
							if sensitiveVmType.HypervisorTags != nil {
								for _, tagName := range sensitiveVmType.HypervisorTags {
									vmRec.Tags = append(vmRec.Tags, tagName)
								}
							}

							continue outer
						}
					}
				}
			}

			// If this point is reached then the VM is the default "Normal" weight and is sorted last during balancing
			vmRec.VmType = balancingutil.VMTYPE_NORMAL
			vmRec.BalanceSortOrder = 2147483647  // Set to max int32 signed value for sorting last
		}
	}

	// Set rack mapping data for this environment VCenter connection's addressbook value
	//
	if vsphereConnection, addrErr := addressBook.GetSelectedVSphereConnection(vcenterName); addrErr != nil {
		// Fatal
		return status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not find VCenter connection from given name \"%s\" because of error: %v", vcenterName, addrErr))
	} else {
		hypervisorLoadOutList.RackMapping = &api.RackMapping{
			Primary:   strings.TrimSpace(vsphereConnection.RackPrimary),
			Secondary: strings.TrimSpace(vsphereConnection.RackSecondary),
			Quorum:    strings.TrimSpace(vsphereConnection.RackQuorum),
		}
	}

	// Limit and re-assign racks if the "rack limit" constraint is enabled by the user
	if rules.Constraints.RackLimit > 0 {
		switch rules.Constraints.RackLimit {
		case 1:
			logger.Log.Info("Limiting racks to primary")
			for i := 0; i < len(hypervisorLoadOutList.Load); i++ {
				// Just all hypervisors to primary rack since that's the only rack available when
				// this constraint is set to 1
				hypervisorLoadOutList.Load[i].Rack = hypervisorLoadOutList.RackMapping.Primary
			}
		case 2:
			logger.Log.Info("Limiting racks to primary and secondary only")
			for i := 0; i < len(hypervisorLoadOutList.Load); i++ {
				var hypervisorRec = hypervisorLoadOutList.Load[i]

				// If hypervisor is Rack 3 or unassigned (blank), then alternately assign them to
				// Rack 1 and Rack 2 for the new model
				if util.IsBlank(hypervisorRec.Rack) || hypervisorRec.Rack == hypervisorLoadOutList.RackMapping.Quorum {
					if i%2 == 0 {
						hypervisorRec.Rack = hypervisorLoadOutList.RackMapping.Primary
					} else {
						hypervisorRec.Rack = hypervisorLoadOutList.RackMapping.Secondary
					}
				}
			}
		default:
			// Value 3 and above is identical to "no constraints" since 3 is the maximum number of
			// racks that MobiTV clusters use.
			logger.Log.Info(fmt.Sprintf("Got rack limit value '%d' so using all 3 racks", rules.Constraints.RackLimit))
		}
	} else {
		logger.Log.Info("No rack constraints were declared")
	}

	// No errors if this point reached
	return nil
}


// Performs a "check balance" on an arbitrary set of hypervisors.  This is useful for "exploratory" checks such as for
// "reduce moves" operations.  Or any situation where a subset of hypervisors requires rules check.
//
func checkReducedBalance(balancingRules *BalancingStruct, hypervisors ...*api.HypervisorLoadOut) bool {
	if hypervisors != nil {
		var dto = api.HypervisorLoadOutDTO{}
		dto.HypervisorLoadOuts = hypervisors

		if results, err := BalanceCheckOnly(balancingRules, &dto); err != nil {
			// TODO: FATAL
		} else {
			// Linear search.  Return on first match.
			for _, hypervisorRec := range results {
				if len(hypervisorRec.GetRulesViolations()) > 0 {
					return true
				}
			}

			return false
		}
	}

	return false
}

// Deletes the given VirtualMachineLoad record in the referenced array if the record was found.  NOTE: The original list
// is modified!  If no match then this method has no effect.
func deleteVmInList(vmRec *api.VirtualMachineLoad, virtualMachineLoadOut *[]*api.VirtualMachineLoad) {
	// Linear search. Delete on first match.
	for i, _ := range *virtualMachineLoadOut {
		if ((*virtualMachineLoadOut)[i]).GetHostname() == vmRec.GetHostname() {
			// Found - splice out the element and set the updated array back to the caller (side effect)
			*virtualMachineLoadOut = append((*virtualMachineLoadOut)[:i], (*virtualMachineLoadOut)[i+1:]...)
			break
		}
	}
}

func findVmInProposedList(vmRec *api.VirtualMachineLoad, hypervisorLoadOut []*api.HypervisorLoadOut) *api.HypervisorLoadOut {
	if vmRec != nil && hypervisorLoadOut != nil {
		// Start O(n^2) search.  Return on first match.
		for _, hypervisorRec := range hypervisorLoadOut {
			for _, vm := range hypervisorRec.GetVmProposed() {
				if vmRec.GetHostname() == vm.GetHostname() {
					return hypervisorRec
				}
			}
		}
	}

	// Not found
	return nil
}

func filterNodeTypes(nodeType string, vmRecs []*api.VirtualMachineLoad) []*api.VirtualMachineLoad {
	var matchingNodes = make([]*api.VirtualMachineLoad, 0)

	// Linear search.  Copy all matching VirtualMachineLoad records to a list for return.
	for _, vmRec := range vmRecs {
		var nodeTypeStruct = balancingutil.ParseNodeType(vmRec.GetHostname())
		if nodeTypeStruct != nil {
			if nodeTypeStruct.NodeType == nodeType {
				matchingNodes = append(matchingNodes, vmRec)
			}
		}
	}

	return matchingNodes
}

func filterNewNodes(currentList []*api.VirtualMachineLoad, proposedList []*api.VirtualMachineLoad) []*api.VirtualMachineLoad {
	var newVmList = make([]*api.VirtualMachineLoad, 0)

	// Search O(n^2)
outer:
	for _, vmRecProposed := range proposedList {
		for _, vmRecCurrent := range currentList {
			if vmRecCurrent.GetHostname() == vmRecProposed.GetHostname() {
				// The proposed VM was found in the current VM list so it is *not* new
				break outer
			}
		}

		// Propose VM *not* found in current VM so it is a new VM
		newVmList = append(newVmList, vmRecProposed)
	}

	return newVmList
}
