package balancing

import (
	"dora/pkg/addressbook"
	"dora/pkg/testutil"
	"dora/pkg/vcenter"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

//
// END-TO-END (e2e) TEST: This test has dependencies on external systems that must be available and
// have the required configuration.  ALL TESTS WILL FAIL IF ALL DEPENDENCIES ARE NOT MET!
//
// AUTH: Must set private auth credientials in OS environment variable.  See testutil package.
//
//

// Test retrieval of a set of hypervisors load outs that is the basis for starting environment
// balancing.
//
// Pre-reqs:
// (1) You must set auth
// (2) You must provide matching sqlite datafile in current folder and have data in it (best to copy it from the Dora prod or another environment has actual data)
// (3) You query regex and assertions must match actual data in the sqlite file
//
func TestHypervisorVMListing(t *testing.T) {
	if testVCLogin, errAuthEnv := testutil.GetVCAuthInfoFromOSEnv(); errAuthEnv != nil {
		panic(errAuthEnv)
	} else {
		fmt.Println(testVCLogin.GetStatus())

		// Use the OS env values to create the vsphere connection
		var addressRecord = &addressbook.VSphereConnectionStruct{
			Name:     testVCLogin.Name,
			Url:      testVCLogin.URL,
			Username: testVCLogin.Username,
			Password: testVCLogin.Password,
		}

		// Create database cache from login and it will access the sqlite file in current folder
		dbCache, errLogin := vcenter.New(addressRecord)
		assert.Nil(t, errLogin)
		assert.NotNil(t, dbCache)

		// Test the actual code here
		//
		hypervisorLoadOut, errLoadOut := dbCache.GenerateHypervisorLoadOut("^/PAYTV-PSR/host/")
		assert.Nil(t, errLoadOut)
		assert.NotNil(t, hypervisorLoadOut)

		assert.True(t, len(hypervisorLoadOut.Load) > 0)

		assert.True(t, len(hypervisorLoadOut.Load[0].GetPath()) > 0)
		assert.True(t, len(hypervisorLoadOut.Load[0].VmProposed) == 0)            // Proposed list is always empty at first
		assert.True(t, len(hypervisorLoadOut.Load[0].VmCurrent) > 0)              // Must target a hypervisor that has children VMs for this to pass
		assert.True(t, len(hypervisorLoadOut.Load[0].VmCurrent[0].GetPath()) > 0) // Must target a hypervisor that has children VMs for this to pass
	}
}
