package balancing

import (
	"dora/pkg/balancingutil"
	"fmt"
	"github.com/go-yaml/yaml"
	"github.com/stretchr/testify/assert"
	"testing"
)

//
// END-TO-END (e2e) TEST: This test has dependencies on external systems that must be available and
// have the required configuration.  ALL TESTS WILL FAIL IF ALL DEPENDENCIES ARE NOT MET!
//
// AUTH: Must set private auth credientials in OS environment variable.  See testutil package.
//
//

// Test retrieval of a set of hypervisors load outs that is the basis for starting environment
// balancing.
//
// Pre-reqs:
// (1) You must set auth
// (2) You must provide matching sqlite datafile in current folder and have data in it (best to copy it from the Dora prod or another environment has actual data)
// (3) You query regex and assertions must match actual data in the sqlite file
//
func TestNodeTypeParse(t *testing.T) {
	var nodeType *balancingutil.NodeTypeStruct

	// Test typical node type
	nodeType = balancingutil.ParseNodeType("msepg01p1.paytv.smf1.mobitv")
	assert.NotNil(t, nodeType)
	assert.Equal(t, "msepg", nodeType.NodeType)
	assert.Equal(t, int32(1), nodeType.Leg)
	assert.Equal(t, int32(1), nodeType.Cluster)

	// Test stsgl node with large cluster
	nodeType = balancingutil.ParseNodeType("stsgl02p100.paytv.smf1.mobitv")
	assert.NotNil(t, nodeType)
	assert.Equal(t, "stsgl", nodeType.NodeType)
	assert.Equal(t, int32(2), nodeType.Leg)
	assert.Equal(t, int32(100), nodeType.Cluster)

	// Test nvrjd node with 'a' or 'b' in cluster
	nodeType = balancingutil.ParseNodeType("nvrjd01p2b.paytv.smf1.mobitv")
	assert.NotNil(t, nodeType)
	assert.Equal(t, "nvrjd", nodeType.NodeType)
	assert.Equal(t, int32(1), nodeType.Leg)
	assert.Equal(t, int32(2), nodeType.Cluster)

	// Test VM with non-standard domain.  This was actually found in NewPayTV environment!  It will
	// return correct parse as the parser ignores everything after the cluster match.
	nodeType = balancingutil.ParseNodeType("mszoo03p1-clone-pcc-17912")
	assert.NotNil(t, nodeType)
	assert.Equal(t, "mszoo", nodeType.NodeType)
	assert.Equal(t, int32(3), nodeType.Leg)
	assert.Equal(t, int32(1), nodeType.Cluster)

	// Test completely non-standard VM name.  This was actually found in NewPayTV environment!  It will return a nil
	// reference because no matches found at all.
	nodeType = balancingutil.ParseNodeType("qumulo_core_2.12.6")
	assert.Nil(t, nodeType)

	// Test completely non-standard VM name.  This was actually found in NewPayTV environment!  It will return a nil
	// reference because no matches found at all.
	nodeType = balancingutil.ParseNodeType("test")
	assert.Nil(t, nodeType)

	// Test completely non-standard VM name.  This was actually found in NewPayTV environment!  It will return a nil
	// reference because no matches found at all.
	nodeType = balancingutil.ParseNodeType("isilonsync.paytv.smf1.mobitv")
	assert.Nil(t, nodeType)
}

func TestWriteRules(t *testing.T) {
	var rules = RulesStruct{
		Cpu: 0.9,
		Mem: 0.95,
		Restrictions: RestrictionsStruct{
			Affinity: []AffinityStruct{{
				NodeType: "stmmx",
				Limits: []AffinityLimitStruct{{
					NodeType: "stsgl",
					Max:      1,
				}},
			}, {
				NodeType: "stmmx",
				Limits: []AffinityLimitStruct{
					{
						NodeType: "nvrcw",
						Max:      0,
					},
					{
						NodeType: "stvar",
						Max:      0,
					}},
			}},
			RackRestrict: true,
		},
	}

	var definitions = DefinitionsStruct{
		VmType: VmTypeStruct{
			HeavyWriters: []VmType{{
				RegEx:          []string{"stmmx"},
				HypervisorTags: []string{"segmenters"},
			}},
			HeavyReaders: []VmType{{
				RegEx: []string{"nvrcw"},
			}},
		},
	}

	var constraints = ConstraintsStruct{
		RackLimit: 3,
	}

	marshal, _ := yaml.Marshal(&BalancingStruct{
		Rules:       rules,
		Definitions: definitions,
		Constraints: constraints,
	})

	fmt.Printf(string(marshal))
}

// Tests the regex hostname matching feature used for regex VMWeight matching
//
func TestNodeTypeWeightParse(t *testing.T) {
	{
		match, err := balancingutil.HostRegexMatch("nvrcw.*b", "nvrcw01p1b.paytv.smf1.mobitv")

		assert.Nil(t, err)
		assert.True(t, match)
	}
	{
		match, err := balancingutil.HostRegexMatch("nvrcw01p.*a", "nvrcw01p1b.paytv.smf1.mobitv")

		assert.Nil(t, err)
		assert.False(t, match) // Must not match since pattern is for "a" node but the actual hostname is an "b" node
	}
}
