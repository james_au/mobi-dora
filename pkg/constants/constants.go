package constants

import "time"

// Define default configuration values to use if they are not overriden
// by external configuration file
const (
	DEFAULT_GRPC_SERVER_PORT       = 9090
	DEFAULT_CONFIG_FILE_PATH       = "dora-config-overrides.json"
	DEFAULT_JWT_KEY                = "h3LL0K1tty!"
	DEFAULT_LDAP_URL               = "ldap://innsv01p1.infra.smf1.mobitv:389"
	DEFAULT_LDAP_DN                = "cn=users,cn=accounts,dc=infra,dc=smf1,dc=mobitv"
	DEFAULT_AUTH_IDLE_MINUTES      = 30
	DEFAULT_REDIS_URL              = "localhost:6379"
	DEFAULT_REDIS_TTL_SECS         = 300
	DEFAULT_VCENTER_WORK_THREADS   = 10 // Default # of threads to run parallel against VCenter
	DEFAULT_REDIS_WORK_THREADS     = 10 // Default # of consumer threads to process the Redis work queue
	DEFAULT_ADDRESSBOOK_PATH       = "address-book.json"
	DEFAULT_APPDASH_HTTP_PORT               = 8700
	DEFAULT_APPDASH_COLLECTOR_PORT          = 50041
	DEFAULT_SQLITE_DATA_FOLDER              = "./"
	DEFAULT_VCENTER_CATEGORY_RACKS          = "dora-balance-racks"
	DEFAULT_VCENTER_CATEGORY_HV_TAGS        = "dora-balance-hv-tags"
	DEFAULT_VCENTER_DIRECT_ALLOW            = "none"
	DEFAULT_NFS_CONFIG_RETRIEVE_MAX_THREADS = 32
	DEFAULT_NFS_CONFIG_RETRIEVE_USERNAME    = "dorasys"   // Recommended to always override this
	DEFAULT_NFS_CONFIG_RETRIEVE_PASSWORD    = ""          // Recommended to always override this
	DEFAULT_SMTP_SERVER                     = "localhost:25" // If localhost doesn't work then try innsv01p1.infra.smf1.mobitv:25
	DEFAULT_SYNC_WARN_HISTORY_RANGE         = 10
	DEFAULT_SYNC_WARN_THRESHOLD             = 2.5
	DEFAULT_SYNC_WARN_RECIPIENTS            = "release-internal@mobitv.com"
	DEFAULT_SYNC_WARN_FROM_ADDRESS          = "dora@mobitv.com"
)

// VCenter constants
const (
	KIND_VIRTUAL_MACHINE = "VirtualMachine"
	KIND_HOST_SYSTEM     = "HostSystem"
	KIND_NETWORK         = "Network"
	KIND_DATASTORE       = "Datastore"
)

// Balance constants
//
const (
	SEVERITY_CRITICAL = "CRITICAL"
	SEVERITY_OK       = "OK"
)

// Program constants
const (
	DEFAULT_TIME_FORMAT = time.RFC3339
)

// Data source info constants
const DATASOURCE_HEADER = "DATASOURCE:"
const DATASOURCE_SQLITE = "SQLite"
const DATASOURCE_REDIS = "Redis"
const DATASOURCE_VCENTER = "VCenter"

// Context key values.  Key names specified here must not have any duplicates.
const (
	// DB data synchronization
	CTX_KEY_SYNC_SYNCHISTORYID = "syncHistoryId"
	CTX_KEY_SYNC_VCENTERSYNC   = "vcenterSync"

	// NFS data synchronization
	CTX_KEY_NFS_SYNC_CONNECTION       = "nfs_sync_connection"
	CTX_KEY_NFS_SYNC_SYNC_HISTORY_ID  = "nfs_sync_history_id"
	CTX_KEY_NFS_SYNC_VCENTER_SYNC_REC = "nfs_sync_vcenter_rec"

	// Auth
	CTX_KEY_AUTH_USERNAME = "authUser"

	// Environment - for passing the environment name when no other means available to communicate it
	CTX_KEY_VCENTER_ENV_NAME = "vcenter_env_name"

	// Cancel flags for own internal cancel signalling separate from Go.  Users of these flags should ensure they
	// are properly coordinated between signal sender(s) and signal receiver(s) to ensure signals are not crossed
	CTX_CANCEL_FLAG_01 = "dora_cancel_flag_01"
	CTX_CANCEL_FLAG_02 = "dora_cancel_flag_02"
	CTX_CANCEL_FLAG_03 = "dora_cancel_flag_03"
)

// Dora admin group required for administration functions
const DORA_ADMIN_GROUP = "dora-admins"

// Search key value denoting to search all which activates special logic
const DORA_SEARCH_ALL = "(All)"

// Data sync constants
const (
	DATA_SYNC_STATUS_COMPLETE     = "COMPLETE"
	DATA_SYNC_STATUS_INCOMPLETE   = "INCOMPLETE"
	DATA_SYNC_STATUS_ABORTED      = "ABORTED"
	DATA_SYNC_STATUS_IN_PROGRESS  = "IN PROGRESS"
)