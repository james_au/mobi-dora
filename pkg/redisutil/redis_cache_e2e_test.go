package redisutil

import (
	"context"
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

//
// END-TO-END (e2e) TEST: This test has dependencies on external systems that must be available and
// have the required configuration.  ALL TESTS WILL FAIL IF ALL DEPENDENCIES ARE NOT MET!
//
// TESTS
// Verifies connection to an actual Redis server
//

// Basic sanity test that Redis server is accessible and accepting a SET, GET and DEL commands
//
func TestRedisConnection01(t *testing.T) {
	assert.NotPanics(t, func() {
		if server, err := New(redisServerUrl, username, password); err != nil {
			assert.Fail(t, fmt.Sprintf("Redis server connection failed to server '%s' because of error: %v", redisServerUrl, err))
		} else {
			assert.NotNil(t, server)

			// Use test key/value.  The key is in a separate "TEST" space from app code so should
			// be fine even if this test is pointed at a production Redis instance.
			var key = "TEST::HypervisorsListKey::NewPayTV---c-05-*"
			var value = "Hello World!"
			var status string

			// Verify SET a key/value succeeds
			//
			var errSet = server.Pool.Do(radix.Cmd(&status, SET, key, value))
			assert.Nil(t, errSet)
			assert.Equal(t, OK, status)

			// Verify retrieval succeeds with same value set above
			//
			var retrievedValue string
			var errGet = server.Pool.Do(radix.Cmd(&retrievedValue, GET, key))
			assert.Nil(t, errGet)
			assert.NotEmpty(t, retrievedValue)
			assert.Equal(t, value, retrievedValue)

			// Verify deletion succeeds
			//
			var delStatus string
			var errDel = server.Pool.Do(radix.Cmd(&delStatus, DEL, key))
			assert.Nil(t, errDel)
			assert.NotEmpty(t, delStatus)
			assert.Equal(t, "1", delStatus)

			// EXPIRATION set and TTL verify test
			//
			var keyCategory = "DORATEST"
			var keyValue = "ttl_test"
			var keyTTL = createKey(keyCategory, keyValue)
			server.SaveToRedisCache(context.TODO(), keyCategory, keyValue, []byte("Hello world"))
			var ttl int
			var errTTL error
			ttl, errTTL = server.GetTTL(keyTTL)
			assert.Nil(t, errTTL)
			fmt.Printf("TTL = %d\n", ttl)

			var creationTimeUnixSecs = server.KeyCreationTime(keyTTL)
			assert.True(t, creationTimeUnixSecs >= 0)
			var timeObj = time.Unix(creationTimeUnixSecs, 0)

			fmt.Printf("Key creation derived: %s", timeObj.String())
		}
	})
}
