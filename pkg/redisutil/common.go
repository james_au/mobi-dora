package redisutil

import (
	cfg "dora/pkg/config"
	"errors"
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"time"
)

// Redis commands used in app enumerated for safer consistent usage across the program
//
// https://redis.io/commands
//
const (
	GET    = "GET"    // https://redis.io/commands/get
	SET    = "SET"    // https://redis.io/commands/set
	DEL    = "DEL"    // https://redis.io/commands/del
	TTL    = "TTL"    // https://redis.io/commands/ttl    - For checking key's TTL (seconds)
	KEYS   = "KEYS"   // https://redis.io/commands/keys   - For getting all keys matching pattern or use * for all keys
	EXPIRE = "EXPIRE" // https://redis.io/commands/expire - For setting key's TTL (seconds)
	LPUSH  = "LPUSH"  // https://redis.io/commands/lpush  - Insert item into the head of the list for FIFO queue operations
	BRPOP  = "BRPOP"  // https://redis.io/commands/brpop  - I/O blocking remove tail element from a list for FIFO queue operations
)

// Redis responses
const (
	OK = "OK"
)

// Error definitions
var (
	ErrRedisCacheDisabled = errors.New("Redis cache is disabled")
)

// Custom connection record
//
type RedisConnectStruct struct {
	Url        string
	Connection *radix.Conn
	Pool       *radix.Pool
	Disabled   bool
}

// Creates a new Dora Redis connection instance.  Typically there is only one instance for
// the entire app.
//
func New(url string, username string, password string) (*RedisConnectStruct, error) {
	var redisConfig = RedisConnectStruct{}

	redisConfig.Disabled = cfg.Config.GetRedisDisabled()

	var conn = func(network, addr string) (radix.Conn, error) {
		return radix.Dial(network, addr, radix.DialAuthPass(""), radix.DialReadTimeout(10*time.Minute), radix.DialWriteTimeout(10*time.Minute))
	}

	if pool, err := radix.NewPool("tcp", url, 10, radix.PoolConnFunc(conn)); err != nil {
		return nil, errors.New(fmt.Sprintf("Redis connection failed with url: %s userName: %s pw: %s because of error: %v\n", url, username, password, err))
	} else {
		redisConfig.Pool = pool

		return &redisConfig, nil
	}
}
