package redisutil

import (
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

//
// END-TO-END (e2e) TEST: This test has dependencies on external systems that must be available and
// have the required configuration.  ALL TESTS WILL FAIL IF ALL DEPENDENCIES ARE NOT MET!
//
// TESTS
// Verifies Redis queueing operations using its LIST data type which is capable of blocking for synchronizing multiple
// consumer/producer threads
//
// https://redislabs.com/ebook/part-1-getting-started/chapter-1-getting-to-know-redis/1-2-what-redis-data-structures-look-like/1-2-2-lists-in-redis/
//

// Uses the configured Redis for a "synchronized queue" test where a consumer waits on I/O block (BRPOP) on an empty
// Redis queue until a producer pushes (LPUSH) a queue item onto it.
//
func TestQueue01(t *testing.T) {
	assert.NotPanics(t, func() {
		if server, err := New(redisServerUrl, username, password); err != nil {
			assert.Fail(t, fmt.Sprintf("Redis server connection failed to server '%s' because of error: %v", redisServerUrl, err))
		} else {
			assert.NotNil(t, server)

			// Use test key/value.  The key is in a separate "TEST" space from app code so should
			// be fine even if this test is pointed at a production Redis instance.
			var key = "TEST::RedisQueueTest::BlockingListTest"
			var value = "QueueElement"
			var status string

			// DEL the key first to ensure consistent test start conditions
			var errDel = server.Pool.Do(radix.Cmd(&status, DEL, key))
			if errDel != nil {
				fmt.Printf("WARN: Deletion of test key %s failed because of error: %v", key, errDel)
			}

			// Start the "consumer" first and it will wait until the queue is empty.  It relies on the BRPOP command
			// to block on I/O until an item on the queue becomes available.
			//
			// The POP is started *before* PUSH because this tests the blocking abilities of the BRPOP Redis command
			// since the queue will be empty until the actual value PUSH occurs below
			go func() {
				// Pop a value off the tail of the list using blocking pop operation
				//
				var results []string
				t.Log("Consumer thread waiting on Redis queue for an item...")
				var errPop = server.Pool.Do(radix.Cmd(&results, BRPOP, key, "0"))
				assert.Nil(t, errPop)
				assert.Equal(t, 2, len(results)) // Expected is 2 because Redis returns the name of the key in element 0 and the actual value in element 1
			}()

			// Push a value into the list.  The asynchronous POP above will be blocked until this PUSH completes!
			//
			time.Sleep(10 * time.Second) // Sleep a few seconds longer than default Redis timeout before pushing an item on the queue
			var errPush = server.Pool.Do(radix.Cmd(&status, LPUSH, key, value))
			assert.Nil(t, errPush)
			assert.Equal(t, "1", status)
		}
	})
}
