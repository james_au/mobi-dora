package redisutil

import (
	"dora/pkg/workqueue"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"strings"
)

// :: DORA Redis Command Queue ::
//
// This package implements the command to push and pop values from the Redis queue used as a "work queue" to
// asynchronously process incoming requests using Redis as an external queue.
//

// The Redis queue key name where producers will enter command for worker consumers to process
//
const REDIS_COMMAND_QUEUE_NAME = "DORA::COMMAND_QUEUE"

// Max number of retries.  Momentary errors like timeouts can be overcome with a retry.
const QUEUE_RETRIES = 3

// Serializable command that will be entered into Redis worker queue for processing by some worker
//
type RedisQueueCommand struct {
	Command       string `json:"command"`        // Enumerated command
	Data          string `json:"data"`           // The command data to process
	ResponseQueue string `json:"response_queue"` // Worker will place results into this specified queue's keyname
}

// Enumerates the available Redis command queue command labels.  These must match up
// between producer and consumer portions of the Redis command queue.
const (
	COMMAND_SEARCH_HYPERVISOR     = "SearchHypervisor"
	COMMAND_SEARCH_VIRTUALMACHINE = "SearchVirtualMachine"
	COMMAND_ROOM_REPORT           = "RoomReport"
)

// Push a command to be processed by the next available worker
//
func (r *RedisConnectStruct) PushCommand(cmd RedisQueueCommand) error {
	// Execute Redis pop blocking pop command which automatically blocks until an item is available on the queue
	//
	// First need to serialize/marshal the command so it can be pushed in the Redis queue as a simple string
	if cmdJson, errJson := json.Marshal(cmd); errJson != nil {
		// Fatal JSON serialization error
		return errors.New(fmt.Sprintf("Could not serialize command '%v' because of error: %v", cmd, errJson))
	} else {
		return r.pushElement(REDIS_COMMAND_QUEUE_NAME, string(cmdJson))
	}
}

// Workers wrap individual responses and use this method to push them onto the given queue
//
func (r *RedisConnectStruct) PushResponse(queueName string, response workqueue.QueueResponseWrapper) error {
	// Serialize encode the response for Redis queue
	if databytes, errMarshal := json.Marshal(response); errMarshal != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not marshal queue response data '%v' because of error: %v", response, errMarshal))
	} else {
		return r.pushElement(queueName, string(databytes))
	}
}

// Pushes any string value into the given Redis queue name.  This is an internal private method.
func (r *RedisConnectStruct) pushElement(queueName string, value string) error {
	// TODO: Investigate if server can be shared (as private member field) to avoid recreating this connection over
	//  and over again because this can be called by worker threads as their results become available
	for attempt := 1; attempt <= QUEUE_RETRIES; attempt++ {
		var results string

		// Execute Redis pop blocking pop command which automatically blocks until an item is available on the queue
		//

		// Push serialized command to the queue and check against errors
		if errPush := r.Pool.Do(radix.Cmd(&results, LPUSH, queueName, value)); errPush != nil {
			if attempt > QUEUE_RETRIES {
				// Fatal Redis POP error
				return errors.New(fmt.Sprintf("Could not push item from given Redis queue '%s' because of error: %v", REDIS_COMMAND_QUEUE_NAME, errPush))
			} else {
				// Still have retries available
				continue
			}
		} else {
			// No errors at this point
			return nil
		}
	}

	// No errors at this point
	return nil
}

// Waits for an item on the command queue key
//
// NOTE: This method blocks until a item is available
func (r *RedisConnectStruct) WaitForCommand() (*RedisQueueCommand, error) {
	// Call the base wait function fixed on the command queue
	if data, errQueue := r.WaitForItem(REDIS_COMMAND_QUEUE_NAME); errQueue != nil {
		// Just pass the error from the base command
		return nil, errQueue
	} else {
		// Data will be unmarshalled into this object
		var redisQueueCommand RedisQueueCommand

		if errJson := json.Unmarshal([]byte(*data), &redisQueueCommand); errJson != nil {
			return nil, errors.New(fmt.Sprintf("Could not unmarshal commmand object because of error: %v", errJson))
		} else {
			// No errors at this point
			return &redisQueueCommand, nil
		}
	}
}

// Wait for an item on the given Redis queue key
//
// NOTE: This method blocks until a item is available
func (r *RedisConnectStruct) WaitForItem(redisKey string) (*string, error) {
	var attempts = 0
	var results []string

	// This outer loop is for retry logic only
	for {
		// Execute Redis pop blocking pop command which automatically blocks until an item is available on the queue
		if errPop := r.Pool.Do(radix.Cmd(&results, BRPOP, redisKey, "0")); errPop != nil {
			if strings.Contains(errPop.Error(), "i/o timeout") {
				// Just restart the loop and continue waiting.  I/O timeout should not affect waiting because
				// waiting time must be indefinite since a worker thread must process any command at any time.
				continue
			} else {
				if attempts > QUEUE_RETRIES {
					// Fatal Redis POP error
					return nil, errors.New(fmt.Sprintf("Made %d attempts but could not pop item from given Redis queue '%s' because of error: %v", attempts, redisKey, errPop))
				} else {
					// Retry again
					attempts++
					continue
				}
			}
		} else {
			if len(results) >= 2 {
				// Successful pop and results are in &result[1] returned below outside of loop
				break
			} else {
				// Got no actual item -- some sporadic Redis return when there was not actual item in the queue to pop.
				attempts = 0 // Reset attempts count
				continue
			}
		}
	}

	// Successful item retrieval from the queue.  NOTE: the item is actually in the 2nd element because the
	// 1st element contains the key name
	return &results[1], nil
}
