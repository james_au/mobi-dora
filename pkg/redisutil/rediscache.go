package redisutil

// :: DORA Redis Cache ::
//
// This library facilitates using Redis as a key/value cache for caching Dora requests and responses.
// Identical requests later will come from the cache nearly instantly versus going to the backend.
// Cached values are kept only for a certain TTL expiration time before they are removed to reduce
// chances of stale cache problems but this can still occur as there is no cache sync strategy at
// this point.
//

import (
	"context"
	dora "dora/pkg/api"
	cfg "dora/pkg/config"
	"dora/pkg/logger"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/mediocregopher/radix/v3"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/protobuf/proto"
	"strconv"
	"strings"
)

const (
	KEY_SEPARATOR               = "---"
	KEY_SPACE                   = "DORA"
	KEY_CATEGORY_HYPERVISOR     = "HypervisorsListKey"
	KEY_CATEGORY_VIRTUALMACHINE = "VirtualMachinesListKey"
)

// Tracing/profiling constants for tagging.  Use these pre-defined constants to keep data consistent.
const (
	TRACETAG_NAME          = "DoraRedisMethod"
	TRACETAG_CACHE_HITMISS = "DoraCacheHitMiss"
	TRACETAG_CACHE_KEY     = "DoraCacheKey"
	TRACEVALUE_HIT         = "HIT"
	TRACEVALUE_MISS        = "MISS"
)

// Creates the "base" key value without category yet.  Needs to be combined with a category for the complete lookup
// key usually via a category lookup.
//
// NOTE: This method should be avoided for *direct* use and, instead, be used from one of the macro functions.
//
func CreateKeyBaseValue(keyValues ...string) string {
	if len(keyValues) > 0 {
		return strings.Join(keyValues, KEY_SEPARATOR)
	} else {
		return ""
	}
}

// Creates the actual Redis key in the format:
//
// main key space + key category + the provided key
//
// Ex: DORA::HypervisorsListKey::NewPayTV---c-03-*
//
// This function is idempotent for consistent key lookup.
//
func createKey(keyCategory string, keyValue string) string {
	return KEY_SPACE + "::" + keyCategory + "::" + keyValue
}

func GenerateHypervisorKey(keyFragment string) string {
	return createKey(COMMAND_SEARCH_HYPERVISOR, keyFragment)    // NOTE: Re-using command value as category
}

// This is a macro that generates the VirtualMachine search key fragment from user entry.  Both the cache set and get
// should use this method to keep the key creation consistent otherwise cache lookup is easily broken.
func GenerateVmSearchKeyFragment(vcenterName, pathFilter, hostFilter string, listingOnly bool) string {
	return CreateKeyBaseValue(vcenterName, pathFilter, hostFilter, strconv.FormatBool(listingOnly))
}

// This is a macro that generates the VirtualMachine search key fragment from user entry.  Both the cache set and get
// should use this method to keep the key creation consistent otherwise cache lookup is easily broken.
func GenerateHvSearchKeyFragment(vcenterName, pathFilter, hostFilter string, listingOnly bool) string {
	return CreateKeyBaseValue(vcenterName, pathFilter, hostFilter, strconv.FormatBool(listingOnly))
}

func GenerateVirtualMachineKey(keyFragment string) string {
	return createKey(COMMAND_SEARCH_VIRTUALMACHINE, keyFragment)    // NOTE: Re-using command value as category
}

func GenerateRoomReportKey(keyFragment string) string {
	return createKey(COMMAND_ROOM_REPORT, keyFragment)    // NOTE: Re-using command value as category
}

// Saves given binary data to the Redis cache using the given key
//
// The key must be a standard key that can be looked up later using the same values
//
func (r *RedisConnectStruct) SaveToRedisCache(ctx context.Context, keyCategory string, keyValue string, data []byte) error {
	var key = createKey(keyCategory, keyValue)

	var spanParent = util.GetParentSpan(ctx)
	var spanRedis = spanParent.Tracer().StartSpan(profiling.OP_REDIS_SET, opentracing.ChildOf(spanParent.Context()))
	spanRedis.SetTag(TRACETAG_NAME, "RedisSaveCache")
	spanRedis.SetTag(TRACETAG_CACHE_KEY, key)
	defer spanRedis.Finish()

	// Successful Protobuf marshalling so save it to Redis as ase64 encoded data bytes
	var marshalledB64 = base64.StdEncoding.EncodeToString(data)
	//logger.Log.Info(fmt.Sprintf("Marshalled %d element array into base64: %v\n", len(hypervisorResponseArray.Items), marshalledB64))

	var success string
	if errRedisExec := r.Pool.Do(radix.Cmd(&success, SET, key, marshalledB64)); errRedisExec != nil {
		return errors.New(fmt.Sprintf("Redis set failed for key '%s' because of error: %v", key, errRedisExec))
	} else {
		// Success
		if errTTL := r.SetExpire(key); errTTL != nil {
			return errTTL
		} else {
			return nil
		}
	}
}

// Gets a raw cache data form the Redis cache given a full key (not a key fragment).  Clients are responsible
// to de-serialize/unmarshal the raw binary data as necessary.
//
func (r *RedisConnectStruct) getFromRedisCache(ctx context.Context, key string) ([]byte, error) {
	if errDisabled := r.checkDisabled(); errDisabled != nil {
		return nil, errDisabled
	}
	var base64Data string

	var spanParent = util.GetParentSpan(ctx)
	var spanRedis = spanParent.Tracer().StartSpan(profiling.OP_REDIS_GET, opentracing.ChildOf(spanParent.Context()))
	spanRedis.SetTag(TRACETAG_NAME, "RedisGetCache")
	spanRedis.SetTag(TRACETAG_CACHE_KEY, key)
	defer spanRedis.Finish()

	if errRedisExec := r.Pool.Do(radix.Cmd(&base64Data, GET, key)); errRedisExec != nil {
		// Fatal - Redis runtime error
		return nil, errors.New(fmt.Sprintf("Redis lookup failure for Redis key '%s' failed because of error: %v", key, errRedisExec))
	} else {
		if rawBytes, errBase64 := base64.StdEncoding.DecodeString(base64Data); errBase64 != nil {
			// Fatal base64 decode error
			return nil, errors.New(fmt.Sprintf("Base64 decode failed for Redis key '%s' because of error: %v", key, errBase64))
		} else {
			if len(rawBytes) > 0 {
				return rawBytes, nil
			} else {
				// Cache miss.  No key found.
				//
				spanRedis.SetTag(TRACETAG_CACHE_HITMISS, TRACEVALUE_MISS)
				return nil, nil
			}
		}
	}
}

// Looks up the Hypervisor list using the given key
//
func (r *RedisConnectStruct) GetCacheHyperVisorList(ctx context.Context, keyFragment string) (*dora.HypervisorResponseArray, error) {
	// Create actual key from given key fragment
	var key = GenerateHypervisorKey(keyFragment)

	if dataBytes, errRedis := r.getFromRedisCache(ctx, key); errRedis != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not cached Hypervisor results because of Redis error: %v", errRedis))
	} else {
		if dataBytes != nil {
			// Unmarshal back to protobuf type
			var hypervisorResponseArray = dora.HypervisorResponseArray{}

			if errUnmarshal := proto.Unmarshal(dataBytes, &hypervisorResponseArray); errUnmarshal != nil {
				// Fatal unmarshalling error
				return nil, errors.New(fmt.Sprintf("Protobuf unmarshalling failed for Redis key '%s' because of error: %v", keyFragment, errUnmarshal))
			} else {
				return &hypervisorResponseArray, nil
			}
		} else {
			// Cache miss - span already traced in getFromRedisCache
			return nil, nil
		}
	}
}

// Looks up the VirtualMachine list using the given key fragment
//
func (r *RedisConnectStruct) GetCacheVirtualMachineList(ctx context.Context, keyFragment string) (*dora.VirtualMachineResponseArray, error) {
	// Create actual key from given key fragment
	var key = GenerateVirtualMachineKey(keyFragment)

	if dataBytes, errRedis := r.getFromRedisCache(ctx, key); errRedis != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not cached VirtualMachine results because of Redis error: %v", errRedis))
	} else {
		if dataBytes != nil {
			// Unmarshal back to protobuf type
			var virtualMachineResponseArray = dora.VirtualMachineResponseArray{}

			if errUnmarshal := proto.Unmarshal(dataBytes, &virtualMachineResponseArray); errUnmarshal != nil {
				// Fatal unmarshalling error
				return nil, errors.New(fmt.Sprintf("Protobuf unmarshalling failed for Redis key '%s' because of error: %v", keyFragment, errUnmarshal))
			} else {
				return &virtualMachineResponseArray, nil
			}
		} else {
			// Cache miss - span already traced in getFromRedisCache
			return nil, nil
		}
	}
}

// Looks up the Room report using the given key fragment
//
func (r *RedisConnectStruct) GetCacheRoomReport(ctx context.Context, keyFragment string) (*dora.RoomResponseArray, error) {
	// Create actual key from given key fragment
	var key = GenerateRoomReportKey(keyFragment)

	if dataBytes, errRedis := r.getFromRedisCache(ctx, key); errRedis != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not get cached Room results because of Redis error: %v", errRedis))
	} else {
		if dataBytes != nil {
			// Unmarshal back to protobuf type
			var roomResponseArray = dora.RoomResponseArray{}

			if errUnmarshal := proto.Unmarshal(dataBytes, &roomResponseArray); errUnmarshal != nil {
				// Fatal unmarshalling error
				return nil, errors.New(fmt.Sprintf("Protobuf unmarshalling failed for Redis key '%s' because of error: %v", keyFragment, errUnmarshal))
			} else {
				return &roomResponseArray, nil
			}
		} else {
			// Cache miss - span already traced in getFromRedisCache
			return nil, nil
		}
	}
}

// Check if this Redis cache is disabled and throw descriptive error if so and nil otherwise.
// This needs to be placed at the beginning of every cache method.
//
func (r *RedisConnectStruct) checkDisabled() error {
	if r.Disabled {
		// Cache is disabled
		return ErrRedisCacheDisabled
	} else {
		// Cache is enabled
		return nil
	}
}

// Sets the EXPIRE for the key using the pre-configured duratino.  The expiration for the key will begin immediately
// after setting and when it reaches 0, the key and value is removed from Redis.  NOTE: in redis-cli, you can use
// the "TTL" command to view any key's current TTL value.
//
// Ref: https://redis.io/commands/expire
// Ref: https://redis.io/commands/ttl
func (r *RedisConnectStruct) SetExpire(key string) error {
	var updateStatusCode string

	var expirationSecs = strconv.Itoa(int(cfg.Config.GetRedisTTLSecs()))
	if errRedisExec := r.Pool.Do(radix.Cmd(&updateStatusCode, EXPIRE, key, expirationSecs)); errRedisExec != nil {
		return errors.New(fmt.Sprintf("Redis TTL expiration set failed for key '%s' because of error: %v", key, errRedisExec))
	} else {
		// Success
		fmt.Sprintf("Expiration set to %d seconds for key '%s'", cfg.Config.RedisTTLSecs, key)
		return nil
	}
}

// Returns the current TTL seconds remaining for the given Redis key.  Errors will be returned if the
// key (a) does not exist or (b) has no TTL associated with it
//
// Ref: https://redis.io/commands/ttl
func (r *RedisConnectStruct) GetTTL(key string) (int, error) {
	var ttl int

	if errRedisExec := r.Pool.Do(radix.Cmd(&ttl, TTL, key)); errRedisExec != nil {
		return 0, errors.New(fmt.Sprintf("Redis TTL expiration set failed for key '%s' because of error: %v", key, errRedisExec))
	} else {
		switch ttl {
		case -2: return -1, errors.New(fmt.Sprintf("Redis key '%s' does not exist", key))
		case -1: return -1, errors.New(fmt.Sprintf("Redis key '%s' does not have any TTL associated with it", key))
		default:
			// Success
			fmt.Sprintf("Expiration set to %d seconds for key '%s'", cfg.Config.RedisTTLSecs, key)
			return ttl, nil
		}
	}
}

// Returns creation time of a Redis key in unix seconds (not milliseconds)
// Value is derived from the key's current TTL and the system EXPIRE duration.
//
// NOTE: A return value  of < 0 is returned if the key doesn't exist or has no
// TTL associated with it.
//
func (r *RedisConnectStruct) KeyCreationTime(key string) int64 {
	if ttl, errTTL := r.GetTTL(key); errTTL != nil {
		// Not fatal but return invalid value
		logger.Log.Info(fmt.Sprintf("Redis key creation timestamp could not be determined because of Redis TTL lookup error: %v", errTTL))
		return -1;
	} else {
		return util.CurrentTimeMillis()/1000 + int64(ttl) - int64(cfg.Config.GetRedisTTLSecs())
	}
}

// Requests Redis to delete the given key name
//
// Return (status code, error [if any])
func (r *RedisConnectStruct) DeleteKey(key string) (int, error) {
	var updateStatusCode string

	if errRedisExec := r.Pool.Do(radix.Cmd(&updateStatusCode, DEL, key)); errRedisExec != nil {
		return 0, errors.New(fmt.Sprintf("Redis DEL failed for key '%s' because of error: %v", key, errRedisExec))
	} else {
		logger.Log.Info(fmt.Sprintf("Redis key '%s' deleted successfully", key))

		if delStatusCode, errAtoi := strconv.Atoi(updateStatusCode); errAtoi != nil {
			return 0, errors.New(fmt.Sprintf("DEL status code %s unparseable as integer", updateStatusCode))
		} else {
			// Success
			return delStatusCode, nil
		}
	}
}
