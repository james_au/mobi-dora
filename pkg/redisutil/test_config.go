package redisutil

//
// File contains Redis e2e testing configuration shared by several tests.
//
// These configurations must be changed to match the environment or the e2e tests will fail!
//

var (
	// Must point to a working Redis.  NOTE: this server could later be started & then shutdown by this test itself
	// as it only needs one blank accessible Redis server.
	redisServerUrl = "localhost:6379"
	username       = ""
	password       = ""
)
