package mail

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Basic mail test.  This must pass or other tests will not work either. Valid login is required.
func TestLdapGroupRetrieval(t *testing.T) {
	assert.NotPanics(t, func() {
		var ctx = context.Background()
		var sendMail = New("innsv01p1.infra.smf1.mobitv:25")

		// Send the test mail to test recipient
		var mailErr = sendMail.Send(ctx, "jau@mobitv.com", []string{"jau@mobitv.com"}, "Subject: Test Message\nThis is a test email from GoLang Dora")

		assert.Nil(t, mailErr)
	})
}
