package mail

import (
	"context"
	"fmt"
	"net/smtp"
)

type MailRec struct {
	smtpServer string
}

// Creates new instance of the mailer
//
// PARAM: server    must be in the form of "host:server" like "localhost:25"
//
func New(server string) *MailRec {
	return &MailRec{
		smtpServer: server,
	}
}

// Sends an email message using pre-configured settings in mailer object
//
// PARAM: ctx        standard Go context
// PARAM: from       the email's "from" address
// PARAM: recipients one or more email recipients
// PARAM: message    standard message (can include subject or cc) according to mailer's formatting
func (o *MailRec) Send(ctx context.Context, from string, recipients []string, message string) error {
	var errMailSend = smtp.SendMail(o.smtpServer, nil, from, recipients, []byte(message))

	if errMailSend != nil {
		return errMailSend
	} else {
		return nil
	}
}

// Creates a basic email message from the provided "subject" and "body" portions
//
// PARAM: subject    email's subject header
// PARAM: body       email's body
func CreateMessageBasic(subject string, body string) string {
	return fmt.Sprintf("subject: %s\n%s", subject, body)
}
