package main

import (
	"context"
	"dora/pkg/addressbook"
	"dora/pkg/api"
	cfg "dora/pkg/config"
	"dora/pkg/util"
	"dora/pkg/vcenter"
	"dora/pkg/vcenterutil"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/vmware/govmomi/vim25/mo"
	"testing"
)

// This must match a pre-defined connection in the configured address book
const VCENTER_CONNECTION_NAME = "OldPayTV"

// This is a SQLite test to see if basic SQL room report is functioning.  All dependencies must be met.
//
func Test001_Room(t *testing.T) {
	//var ctx = context.WithValue(context.Background(), "environment", "e2e_test")

	assert.NotPanics(t, func() {
		addressBook, errAddressBook := addressbook.New("../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Create new sync object which also creates the SQLite instance
		vcenterSync, errVCenter := vcenter.New(vcConnection)
		assert.Nil(t, errVCenter)
		assert.NotNil(t, vcenterSync, "Could not create sync object")

		var channel = make(chan *api.RoomResponse, 64)
		var err = vcenterSync.GenerateRoomReport("/PAYTV-STAGING", channel)
		assert.Nil(t, err)

		// Read & display all records until end-of-channel indicated
		for {
			var roomRecord = <-channel

			if roomRecord != nil {
				if roomRecord.UiMsgError == "" {
					fmt.Printf("Room report record: %v\n", *roomRecord)
				} else {
					// End the test with an error.  This would normally be an error return object that is handled in
					// the respective code context (Javascript, console, etc.
					panic(fmt.Sprintf("Room report indicated fatal error: %s", roomRecord.UiMsgError))
				}
			} else {
				// Normal termination - no more rows remaining
				break
			}
		}
	})
}

// This test a SOAP VCenter data pull for a Hypervisor's datastores to determine the Hypervisor's available disk
// storage.  All dependencies must be met.
//
func Test002_DatastoreSizeRetrievalSOAP(t *testing.T) {
	var ctx = context.WithValue(context.Background(), "environment", "e2e_test")

	assert.NotPanics(t, func() {
		addressBook, errAddressBook := addressbook.New("../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vcConnection)
		assert.Nil(t, errLogin)
		assert.NotNil(t, vcSoapConnection)

		es, errVcSearch := vcenter.Search(vcSoapConnection, "c-03-13.infra.smf1.mobitv", vcenter.KIND_HOST_SYSTEM)
		assert.Nil(t, errVcSearch)
		assert.NotNil(t, es)

		for _, vmElement := range es {
			hs, err := vcSoapConnection.Finder.HostSystem(vcSoapConnection.Ctx, vmElement.Path)
			if err != nil {
				// Fatal
				panic(fmt.Sprintf("HostSystem Finder Error: %s", err))
			}

			props := []string{"summary", "vm", "config"}
			var h mo.HostSystem

			if util.StringArrayContains(hs.Reference().Type, "HostSystem") {
				err = hs.Properties(vcSoapConnection.Ctx, hs.Reference(), props, &h)
				if err != nil {
					// Fatal
					panic(fmt.Sprintf("HostSystem Props Error: %s", err))
				}
			}

			fmt.Printf("%v\n", h)
		}
	})
}
