package auth

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

//
// UNIT TEST: No test dependencies required for this test
//
// TESTS:
// Verify basic JWT token generation for any given username.   This test bypasses auth and generates a token
// for any user.
//

// Any username can be provided for this test
var testUser = "user"

func TestJwtGeneration(t *testing.T) {
	assert.NotPanics(t, func() {
		tokenString, token, tokenErr := GenerateJwtToken(context.TODO(), testUser)

		assert.Nil(t, tokenErr)
		assert.NotNil(t, token)
		assert.NotEmpty(t, tokenString)
	})
}

// UNIT TEST: Verify token generated plus verifies token can be decrypted/deserialized and that the
// payload matches the original payload
func TestVerify(t *testing.T) {
	assert.NotPanics(t, func() {
		var now = time.Now()

		tokenString, tokenLogin, tokenErrLogin := GenerateJwtToken(context.TODO(), testUser)
		assert.Nil(t, tokenErrLogin)
		assert.NotNil(t, tokenString)
		assert.NotNil(t, tokenLogin)

		tokenParsed, tokenErrParsed := parseTokenRawString(tokenLogin.Raw)
		assert.Nil(t, tokenErrParsed)
		assert.NotNil(t, tokenParsed)

		// Now check the parsed response JWT claim values match
		var claims = tokenParsed.Claims.(*Claims)
		assert.Equal(t, testUser, claims.Username)
		assert.NotNil(t, claims.ExpiresAt)

		assert.True(t, claims.ExpiresAt >= now.Unix()) // expiration date is some time in the future from when the token was generated
	})
}
