package auth

import (
	"dora/pkg/logger"
	"fmt"
	jwtlib "github.com/dgrijalva/jwt-go"
	"sync"
	"time"
)

// Data struct to act as a "directory" of current logins to Dora which is needed for basic auth control and useage
// metrics.
//
// NOTE: access is serialized to prevent potential race conditions from concurrent logins
type DoraLoginMap struct {
	loginMap map[string]*jwtlib.Token
	mutex    sync.RWMutex
}

// Creates a new login directory
//
// PARAM: autoPurge   set to "true" to enable background daemon that automatically purges expired login tokens
//
func NewLoginDirectory(autoPurge bool) *DoraLoginMap {
	var loginMap = DoraLoginMap{
		loginMap: make(map[string]*jwtlib.Token, 0),
		mutex:    sync.RWMutex{},
	}

	// Start the background "cron" to auto purge expired and idle timed out sessions automatically
	if autoPurge {
		logger.Log.Info("Starting background cron to automatically purge idle expired login sessions...")
		go func() {
			// Run a "daemon" loop continuously while Dora is running
			for {
				loginMap.PurgeExpired()

				time.Sleep(2 * time.Minute)
			}
		}()
	}

	return &loginMap
}

// Removes all current logins expired or not
func (o *DoraLoginMap) PurgeAll() {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	// Clear the map but do not change its reference because it could be referenced by other areas of the code
	for key := range o.loginMap {
		delete(o.loginMap, key)
	}
}

// Removes all Dora logins from the map that have elapsed expiration
//
// NOTE: This should be done periodically on a cron background thread
//
func (o *DoraLoginMap) PurgeExpired() {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	var currentTimestampSecs = time.Now().Unix()
	for key := range o.loginMap {
		var claims = o.loginMap[key].Claims.(*Claims)
		if claims.ExpiresAt < currentTimestampSecs {
			logger.Log.Info(fmt.Sprintf("Deleting expired login session for user '%s' because token has expired as of: %v", claims.Username, time.Unix(claims.ExpiresAt, 0)))
			delete(o.loginMap, key)
		}
	}
}

// Removes the specific auth token referenced by the given auth token string
//
// PARAM: tokenString   reference to the session to remove
//
func (o *DoraLoginMap) PurgeToken(tokenString string) {
	// Obtain "writer" lock
	defer o.mutex.Unlock()
	o.mutex.Lock()

	delete(o.loginMap, tokenString)
}

// Add a new login token
func (o *DoraLoginMap) AddToken(jwtString string, token *jwtlib.Token) {
	// Get writer lock
	defer o.mutex.Unlock()
	o.mutex.Lock()

	o.loginMap[jwtString] = token
}

// Gets only currently non-expired logins
//
// RETURN: A new list of tokens that are not expired
func (o *DoraLoginMap) GetActiveLogins() []*jwtlib.Token {
	// Obtain reader lock and return active logins
	defer o.mutex.RUnlock()
	o.mutex.RLock()

	var activeLogins = make([]*jwtlib.Token, 0)

	// Iterate through all logins but do not include login session that are expired
	for key := range o.loginMap {
		var token = o.loginMap[key]
		if !IsTokenExpired(token) {
			activeLogins = append(activeLogins, token)
		}
	}

	// Result is array of zero or more elements and result is never nil
	return activeLogins
}

// Get current list of *all* login tokens without checking if any token is expired or not
func (o *DoraLoginMap) GetAllLogins() []*jwtlib.Token {
	defer o.mutex.RUnlock()
	o.mutex.RLock()

	var allLogins = make([]*jwtlib.Token, 0)

	for key := range o.loginMap {
		var token = o.loginMap[key]

		allLogins = append(allLogins, token)
	}

	// Result is array of zero or more elements and result is never nil
	return allLogins
}

// Looks up token directly from its auth token string
//
// PARAM: auth token string
// RETURN: Matching auth login record or nil if not found
func (o *DoraLoginMap) LookupDoraLogin(token string) *jwtlib.Token {
	 for key := range o.loginMap {
	 	if key == token {
	 		return o.loginMap[key]
		}
	 }

	 // Not found if this point is reached
	 return nil
}