package auth

import (
	"context"
	cfg "dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"errors"
	"fmt"
	jwtlib "github.com/dgrijalva/jwt-go"
	"github.com/opentracing/opentracing-go"
	"strings"
	"time"
)

// :: Dora Authentication & Authorization ::
//
// Dora defers both authentication and authorization functions to the configured LDAP/IPA server.
// A user is considered authenticated and has non-admin access to Dora if they are an authenticated LDAP user.  Dora
// access is open to all within the company.
//
// ADMIN ACCESS
// Although all LDAP users have Dora access (see above), only users with a special LDAP group will have access to
// Dora's administration pages (cron sync management, backend config view, etc).  This is controlled by the placing
// users within or out of a specific LDAP group -- usually called "dora-admin" LDAP group.
//

// This is Dora's single global logins directory and will be referenced from other Dora subsystems particularly the
// main package.
//
// NOTE: the "auto purge" feature is enabled which automatically purges expired logins periodically.  This keeps helps
// keep the usage metrics exported to Prometheus to be more accurate.
var LoginSessionsDirectory = NewLoginDirectory(true)

// A custom struct to store JWT session payload compatible with JWT by implementing
// JWT's StandardClaims interface
type Claims struct {
	Username string   `json:"username"`
	Groups   []string `json:"groups"`
	jwtlib.StandardClaims
}

// Secure endpoints need to use this to verify the auth token string provided by the user is valid & not expired.
// Errors such as expired tokens will be returned as an error and the code should error out accordingly with
// appropriate error message to the user.
//
// Upon valid check, the user will then have all access for which the token is authorized for such as admin vs.
// non-admin.
func CheckToken(authToken string, requires ...string) (*jwtlib.Token, error) {
	if !cfg.Config.GetAuthDisabled() {
		// Token must exist in server directory to allow login and may be removed by server daemon at any time if
		// expired
		if LoginSessionsDirectory.LookupDoraLogin(authToken) == nil {
			// FATAL: Auth record not found for given token string.  Most likely the session timed out and was removed
			// by the server-side login sessions daemon.
			return nil, errors.New(fmt.Sprintf("Auth token not found on server.  Most likely timed out.  Auth token: %s", authToken))
		}

		// Verify and unpack token payload to get auth session data
		//
		if token, errToken := parseTokenRawString(authToken, requires...); errToken != nil {
			// FATAL - auth token potentially invalid
			var authErrMsg = fmt.Sprintf("Authentication rejected: %v", errToken)

			logger.Log.Error(errToken, authErrMsg+".  authToken = "+authToken)

			return nil, errors.New(authErrMsg)
		} else {
			// NOTE: Auth expiration check is performed in parse token call above
			logger.Log.Info(fmt.Sprintf("Auth token for user '%s' verified", token.Claims.(*Claims).Username))
			return token, nil
		}
	} else {
		logger.Log.Info("Auth disabled.  Using bypass auth token instead.")

		var ByPassClaim = &Claims{
			Username:       "ByPassUser",
			Groups:         []string{"dora-admins"},
			StandardClaims: jwtlib.StandardClaims{},
		}

		// Dora's auth is disabled.  Just return a static claim.
		return &jwtlib.Token{
			Raw:       "",
			Method:    nil,
			Header:    nil,
			Claims:    ByPassClaim,
			Signature: "",
			Valid:     true,
		}, nil
	}
}

// Parses (deserialize) the raw token string back to a token object
//
func parseTokenRawString(token string, requires ...string) (*jwtlib.Token, error) {
	// Token payload will deserialize into this struct below
	var claims = Claims{}

	// Parse
	if tokenParsed, errParse := jwtlib.ParseWithClaims(token, &claims, func(token *jwtlib.Token) (interface{}, error) {
		// NOTE: the signing keyphrase must match the same phrase used in the counterpart GenerateJwtToken() call
		// otherwise all tokens will fail with "invalid signature" error.
		return []byte(cfg.Config.GetJwKeyPhrase()), nil
	}); errParse != nil {
		// Fatal - some JWT token parsing error.  Likely causes:
		// (1) JWT token missing or malformed
		// (2) Token has expire.  Message in error object should indicate "expiration" problem.
		// (3) Signature error occurred.  Message in error object will be "signature is invalid".  Check the proper JWT
		//     token is read-in from config and matches what was used to originally generate the token.  Or get the client
		//     to just get a new fresh token.
		return nil, errParse
	} else {
		// Successful parse and signature verified on token
		if !IsTokenExpired(tokenParsed) {
			if len(requires) == 0 {
				// Authorization successful -- no group restrictions specified
				return tokenParsed, nil
			} else {
				// Authorization successful but need to check user has the specified groups
				if len(util.Intersection(tokenParsed.Claims.(*Claims).Groups, requires)) > 0 {
					// Authorization sucessful.  User has all required groups.
					return tokenParsed, nil
				} else {
					// Reject.  Authenticated but authorization fails by not having all required groups.
					return nil, errors.New(fmt.Sprintf("User \"%s\" authenticated but not authorized for this function.  User requires LDAP groups (%s).  Please see Dora administrator get proper user roles.", claims.Username, strings.Join(requires, ",")))
				}
			}
		} else {
			// Authentication failed.  Token valid but expired.
			//
			var claims = tokenParsed.Claims.(*Claims)

			return nil, errors.New(fmt.Sprintf("Auth token for user '%s' is expired as of time %s.  User needs to get a new token.",
				claims.Username,
				time.Unix(claims.ExpiresAt, 0).Format(time.RFC1123Z)))
		}
	}
}

// Extracts the username in the encoded JWT token string.  Otherwise an empty string "" is returned on any error.  Check
// logs for error details.
func GetUsername(token string) string {
	if tokenObj, errParseToken := parseTokenRawString(token); errParseToken != nil {
		// Error
		logger.Log.Info(fmt.Sprintf("Could not parse token '%s' because of error: %v", token, errParseToken))
		return ""
	} else {
		if tokenObj != nil {
			if tokenObj.Claims != nil {
				switch tokenObj.Claims.(type) {
				case *Claims:
					return tokenObj.Claims.(*Claims).Username
				default:
					// Error
					logger.Log.Info("Token object not the correct type.  Expecting *Claims type")
					return ""
				}
			} else {
				// Error
				logger.Log.Info("Token parsed but did not contain any info")
				return ""
			}
		} else {
			// Error
			logger.Log.Info("Token was null")
			return ""
		}
	}
}

// Generates a JWT token and is used to start an auth session for a user after their login
// credentials have been successfully verified.
//
// RETURN  signed string
// RETURN  original token object (so don't need to parse)
// ERROR   on any token generation error
func GenerateJwtToken(ctx context.Context, username string, groups ...string) (string, *jwtlib.Token, error) {
	var spanParent = util.GetParentSpan(ctx)
	var spanGenJwtToken = spanParent.Tracer().StartSpan(profiling.OP_JWT_GENERATE_TOKEN, opentracing.ChildOf(spanParent.Context()))
	spanGenJwtToken.SetTag("username", username)
	spanGenJwtToken.SetTag("groups", strings.Join(groups, ","))
	defer spanGenJwtToken.Finish()

	// Set the auth token to expire at a specific time in the future given by configured auth expiration time
	var expirationTime = time.Now().Add(time.Duration(cfg.Config.GetAuthIdleExpirationMinutes()) * time.Minute)

	// Create the new token with custom payload
	var token = jwtlib.NewWithClaims(jwtlib.SigningMethodHS256, &Claims{
		Username: username,
		Groups:   groups,
		StandardClaims: jwtlib.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	})

	// Create the cryptographically signed token string in the standard JWT format "header.token.signature".
	// NOTE: The signing key phrase must be the same phrase in the counterpart Verify() call otherwise all
	// auth tokens will fail with "invalid signature".
	if tokenString, err := token.SignedString([]byte(cfg.Config.GetJwKeyPhrase())); err != nil {
		return "", nil, errors.New(fmt.Sprintf("Token generation failed because of error: %v", err))
	} else {
		// Success
		spanGenJwtToken.SetTag("token", tokenString)

		logger.Log.Info(fmt.Sprintf("Auth token successfully generated: %s", tokenString))

		return tokenString, token, nil
	}
}

// Returns true if the given token has expired and false otherwise
//
func IsTokenExpired(token *jwtlib.Token) bool {
	// NOTE: JWT timestamps use Unix time units
	var expiryTimeSec = token.Claims.(*Claims).ExpiresAt
	var timeNowSec = time.Now().Unix()
	var isExpired = expiryTimeSec < timeNowSec

	return isExpired
}

// Returns true if the token has the Dora Admin group and false otherwise
//
func IsAdmin(token *jwtlib.Token) bool {
	var groups = token.Claims.(*Claims).Groups
	if groups != nil {
		var intersection = util.Intersection(groups, []string{constants.DORA_ADMIN_GROUP})
		if intersection != nil && len(intersection) > 0 {
			// User has Dora admin privilege
			return true
		}
	}

	// User does not have Admin privileges if this point reached
	return false
}
