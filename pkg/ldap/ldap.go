package ldap

import (
	"context"
	"dora/pkg/config"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"errors"
	"fmt"
	ldapv3 "github.com/go-ldap/ldap/v3"
	"github.com/opentracing/opentracing-go"
	"strings"
)

// Login for the user via LDAP using the given credentials. If successful, returns a list of groups
// the user belongs to.  Otherwise, errors are thrown and should be captured for display back to the
// user to take corrective action.
func LoginAndGetGroups(ctx context.Context, username, password string) ([]string, error) {
	var spanParent = util.GetParentSpan(ctx)
	var spanLogin = spanParent.Tracer().StartSpan(profiling.OP_LDAP_LOGIN, opentracing.ChildOf(spanParent.Context()))
	spanLogin.SetTag("username", username)
	defer spanLogin.Finish()

	if ldap, errDial := ldapv3.DialURL(config.Config.GetLdapUrl()); errDial != nil { // TODO: URL needs to come from config properties
		return nil, errDial
	} else {
		defer ldap.Close()

		// This confirms the given user exists and has login rights
		//
		if ldapBindErr := ldap.Bind(fmt.Sprintf("uid=%s,cn=users,cn=accounts,dc=infra,dc=smf1,dc=mobitv", username), password); ldapBindErr != nil {
			// Fatal - likely user doesn't exist or password is incorrect.  Error message is not very helpful from this LDAP library.
			return nil, errors.New(fmt.Sprintf("Login failed for user '%s'.  Login possibly incorrect. LDAP Binding error: %v", username, ldapBindErr))
		}

		// Search for the given username
		var searchRequest = ldapv3.NewSearchRequest(
			config.Config.GetLdapDn(),
			ldapv3.ScopeWholeSubtree, ldapv3.NeverDerefAliases, 0, 0, false,
			fmt.Sprintf("(&(uid=%s))", username),
			[]string{"memberOf"}, // Attributes filter can also be empty array to return all available attributes (good for debugging)
			nil,
		)

		if searchResults, errSearch := ldap.Search(searchRequest); errSearch != nil {
			// Fatal - LDAP search error
			return nil, errSearch
		} else {
			fmt.Sprintf("Got search result: %v\n", searchResults)

			if searchResults != nil && len(searchResults.Entries) > 0 {
				var values = searchResults.Entries[0].GetAttributeValues("memberOf")

				if values != nil {
					// Success - Values should contains the LDAP list of groups.
					// Ex:
					// cn=ipausers,cn=groups,cn=accounts,dc=infra,dc=smf1,dc=mobitv
					// cn=clienteng,cn=groups,cn=accounts,dc=infra,dc=smf1,dc=mobitv
					// .
					// .
					return values, nil
				} else {
					return nil, errors.New(fmt.Sprintf("No groups found for user: %s", username))
				}
			} else {
				// No results found so cannot find group membership
				return nil, errors.New(fmt.Sprintf("No groups found for user: %s", username))
			}
		}
	}
}

// Returns true if the given group is found in the list of groups
//
func UserIsInLdapGroup(requiredGroup string, ldapGroups []string) bool {
	for _, ldapGroup := range ldapGroups {
		// Linear search.  Return on first match.
		// TODO: need more robust matching code in case of space or new line characters
		if strings.Contains(ldapGroup, "cn="+requiredGroup) {
			return true
		}
	}

	// No match found if this point is reached
	return false
}
