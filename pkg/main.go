// Server business logic implementation.
//
// NOTE: The Dora core RPC can be regenerated from the proto definition using the
// targets in the project's Makefile
//
//

// Package main implements the server for Dora
package main

import (
	"context"
	"dora/pkg/addressbook"
	"dora/pkg/api"
	"dora/pkg/auth"
	"dora/pkg/balancing"
	"dora/pkg/constants"
	"dora/pkg/dbcache"
	"dora/pkg/ldap"
	"dora/pkg/logger"
	"dora/pkg/nfs"
	"dora/pkg/profiling"
	"dora/pkg/redisutil"
	"dora/pkg/util"
	"dora/pkg/util/count"
	"dora/pkg/util/stringarrayutil"
	"dora/pkg/util/threadsafequeue"
	"dora/pkg/vcenter"
	vcentermove "dora/pkg/vcenter/move"
	"dora/pkg/vcenterutil"
	"dora/pkg/workqueue"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	jwtlib "github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/oklog/run"
	"github.com/opentracing/opentracing-go"
	"github.com/spf13/pflag"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sourcegraph.com/sourcegraph/appdash"
	appdash_opentracing "sourcegraph.com/sourcegraph/appdash/opentracing"
	"sourcegraph.com/sourcegraph/appdash/traceapp"
	"strings"
	"sync"
	"time"

	dora "dora/pkg/api"
	cfg "dora/pkg/config"

	"google.golang.org/grpc"
)

// Declare up global objects
var (
	redisConnect *redisutil.RedisConnectStruct
)

// Declare pflag command line flags
var (
	versionFlag = pflag.BoolP("version", "v", false, "Print version")
	helpFlag    = pflag.BoolP("help", "h", false, "Print help text")
)

// Provides access to the generated gRPC server code
//
type server struct {
	dora.DoraServer
}

// Single Hypervisor record return
func (s *server) GetHypervisor(context context.Context, query *dora.HVQueryRequest) (*dora.HypervisorResponse, error) {
	log.Printf("Received Hypervisor Query: (%s, %s, %s)", query.VcenterName, query.PathFilter, query.HostFilter)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		return nil, authError
	} else {
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		return &dora.HypervisorResponse{Name: "hypervisor_name"}, nil
	}
}

// Multiple Hypervisor records returned in a fixed size array.  This endpoint will block until all records are
// available so this might cause *timeout* on the client for longer queries.  Use the "streamed" version of this
// endpoint to get asynchronous streaming of records as they become available.
//
func (s *server) GetHypervisors(ctx context.Context, query *dora.HVQueryRequest) (*dora.HypervisorResponseArray, error) {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GET_HYPERVISORS_ARRAY)
	defer span.Finish()

	log.Printf("Received GetHypervisors Query: (%s, %s, %s)", query.VcenterName, query.PathFilter, query.HostFilter)

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(ctx, profiling.PARENT_SPAN_NAME, span)

	// Add the actual query to this span for more tracing details
	span.SetTag("Query", fmt.Sprintf("%v", query))

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		// Redis base key value
		var redisKeyBaseValue = redisutil.GenerateHvSearchKeyFragment(query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly())

		// Perform redis lookup
		if hypervisorList, errRedis := redisConnect.GetCacheHyperVisorList(ctxSpan, redisKeyBaseValue); errRedis != nil {
			if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
				logger.Log.Info(errRedis.Error())
			} else {
				// Not-fatal but log it.  Can work without caching but administrator should know about this
				// error as it represents application instability.
				logger.Log.Error(errRedis, fmt.Sprintf("Redis cache lookup failed because of error: %v", errRedis))
			}
		} else {
			if hypervisorList != nil {
				// Cache hit - just send the results back to client which bypasses call to VCenter
				return hypervisorList, nil
			}
		}

		//
		// Proceed with lookup (this will runs on a cache miss or if cache disabled)
		//

		// Use a protobuf response array for Redis caching because the protobuf can be serialized to bytes
		// and then a base64 string for easy Redis caching
		var hypervisorResponseArray = dora.HypervisorResponseArray{}

		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(query.GetVcenterName()); errAddressBook != nil {
			// Fatal - most common problem is the user specifed connection name is incorrect
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Could not look up connection '%s' in address book because of error: %v", query.GetVcenterName(), errAddressBook))
		} else {
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctxSpan, vsphereConnection); errLogin != nil {
				// Fatal
				return nil, status.Error(codes.Unauthenticated, fmt.Sprintf("Could not log into VCenter '%s'.  Check that your connection name is correct.  Error: %v", query.GetVcenterName(), errLogin))
			} else {
				if outputChannel, errVCenter := vcenter.ListHypervisorRecords(ctxSpan, query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly(), true, vcSoapConnection); errVCenter != nil {
					// Fatal
					return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to execute Hypervisor search on filter (%s, %s) because of error: %v", query.GetPathFilter(), query.GetHostFilter(), errLogin))
				} else {
					// Loop through the Go channel object and save results to the array
					for {
						var hypervisorResponse = <-outputChannel

						if hypervisorResponse != nil {
							// Save to a Protobuf list for Redis caching
							hypervisorResponseArray.Items = append(hypervisorResponseArray.Items, hypervisorResponse)
						} else {
							// Stream signalled completion with its nil result
							break
						}
					}

					// Save to the Redis cache so later lookups on same query will return the result instantly (up until
					// the cache record expires via TTL though)
					if protoMarshaledData, errProto := proto.Marshal(&hypervisorResponseArray); errProto != nil {
						// Fatal
						return nil, status.Errorf(codes.Internal, "Proto marshalling failed for caching because of error: %v", errProto)
					} else {
						if errRedis := redisConnect.SaveToRedisCache(ctxSpan, redisutil.COMMAND_SEARCH_HYPERVISOR, redisKeyBaseValue, protoMarshaledData); errRedis != nil {
							if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
								// Not a fatal error but log it as it represents system instability
								logger.Log.Info(errRedis.Error())
							} else {
								// Actual REDIS error that needs to be displayed
								logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyBaseValue, errRedis))
							}
						}
					}
				}
			}
		}

		// No errors if this point reached
		return &hypervisorResponseArray, nil
	}
}

// Multiple Hypervisor records search (streaming mode)
func (s *server) GetHypervisorsStreamed(query *dora.HVQueryRequest, stream dora.Dora_GetHypervisorsStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GET_HYPERVISORS_STREAMED) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetHypervisorsStreamed Query: (%s, %s, %s)", query.VcenterName, query.PathFilter, query.HostFilter)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctx = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		// Redis base key value
		var redisKeyBaseValue = redisutil.GenerateHvSearchKeyFragment(query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly())

		// Perform redis lookup
		if hypervisorList, errRedis := redisConnect.GetCacheHyperVisorList(ctx, redisKeyBaseValue); errRedis != nil {
			if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
				logger.Log.Info(errRedis.Error())
			} else {
				// Not-fatal but log it.  Can work without caching but administrator should know about this
				// error as it represents application instability.
				logger.Log.Error(errRedis, fmt.Sprintf("Redis cache lookup failed because of error: %v", errRedis))
			}
		} else {
			if hypervisorList != nil {
				// Cache hit - just send the results back to client which bypasses call to VCenter
				logger.Log.Info("Hypervisor search Redis Cache hit")

				// Send UI info about Redis data source
				if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
					Name:        constants.DATASOURCE_REDIS,
					Description: "Retrieved from super fast Redis local short-term cache",
					LastUpdated: redisConnect.KeyCreationTime(redisutil.GenerateHypervisorKey(redisKeyBaseValue)) * 1000, // Derive this from current time secs - Redis TTL secs
				}); errJson != nil {
					// Fatal - unlikely
					logger.Log.Error(errJson, "Could not marshal Datasource record")
				} else {
					stream.Send(&api.HypervisorResponse{
						UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
					})
				}

				// Send unmarshaled Redis cache data row directly back to the client
				for _, hypervisorResponse := range hypervisorList.Items {
					stream.Send(hypervisorResponse)
				}

				// Done.  Do not continue with VCenter lookup below.
				return nil
			}
		}

		logger.Log.Info("Hypervisor search Redis Cache miss.  Will fall back to either SQLite or direct VCenter search.")

		// Redis cache miss.  Use backends to lookup in the following precedence order:
		// (1) SQLite - preferred if available
		// (2) VCenter - last precedence - slow but is the master data source
		//
		// Use SQLite fast search if its determined to be available otherwise fallback to slow VCenter SOAP search
		//
		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				if dbCache == nil || dbCache.GetRecordCount("VirtualMachine") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.HypervisorResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			var dbCache = dbSyncIndex.GetSyncObject(vcenterName)
			if dbCache != nil && dbCache.GetRecordCount("Hypervisor") > 0 {
				//
				// SQLite fast mode (DB cached data)
				//
				logger.Log.Info("Searching Hypervisor records via fast SQLite cached mode...")
				var searchError = searchHypervisorsSqLite(authToken, vcenterName, query, stream)

				if searchError != nil {
					searchErrors = append(searchErrors, searchError)
				}
			} else {
				//
				// SOAP mode (direct access to master data but slow) -- only available for Admin users because of VCenter
				// load concerns
				//
				if deniedErr := checkVCenterDirectAccessEnabled(authToken, query.GetVcenterName()); deniedErr != nil {
					// VCenter access disabled for given user and mode.  NOTE: error contains descriptive message
					searchErrors = append(searchErrors, deniedErr)
				} else {
					logger.Log.Info("Searching Hypervisor records via via SOAP/VCenter direct mode...")
					var searchError = searchHypervisorsSoap(authToken, query, stream)

					if searchError != nil {
						searchErrors = append(searchErrors, searchError)
					}
				}
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// Performs the Hypervisor streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchHypervisorsSqLite(authToken *jwtlib.Token, vcenterName string, query *dora.HVQueryRequest, stream dora.Dora_GetHypervisorsStreamedServer) error {
	var ctx = context.Background()
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.HypervisorResponse, 64)
	var hypervisorResponseArray = api.HypervisorResponseArray{}

	if pathRegexp, errRegexp := regexp.Compile(query.GetPathFilter()); errRegexp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Hypervisor search failed: %v", errRegexp))
	} else if hostregExp, errRegexp := regexp.Compile(query.GetHostFilter()); errRegexp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Hypervisor search failed: %v", errRegexp))
	} else {
		if errSearch := vcSyncDb.HypervisorRegexpSearch(pathRegexp, hostregExp, channelResults); errSearch != nil {
			// Fatal - Something wrong with search pattern or initial database connection
			return status.Errorf(codes.Internal, "Hypervisor search failed to start because of error: %v", errSearch)
		} else {
			// Connection OK - stream results
			for {
				var hypervisorResponse = <-channelResults

				if hypervisorResponse == nil {
					// End of records reached
					break
				} else {
					hypervisorResponse.Vcenter = vcenterName

					// Send new record to stream for immediate display to the frontend
					stream.Send(hypervisorResponse)

					// Accumulate to list for Redis caching below but ignore UI control and UI Info metadata records
					if !hypervisorResponse.UiControlIsPreview &&
						len(hypervisorResponse.UiMsgInfo) == 0 &&
						len(hypervisorResponse.UiMsgWarning) == 0 &&
						len(hypervisorResponse.UiMsgError) == 0 {
						hypervisorResponseArray.Items = append(hypervisorResponseArray.Items, hypervisorResponse)
					}
				}
			}

			// Save to the Redis cache so later lookups on same query will return the result instantly (up until
			// the cache record expires via TTL though)
			var redisKeyValue = redisutil.GenerateHvSearchKeyFragment(vcenterName, query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly())
			if protoMarshaledData, errProto := proto.Marshal(&hypervisorResponseArray); errProto != nil {
				// Fatal
				return errors.New(fmt.Sprintf("Proto marshalling failed for caching because of error: %v", errProto))
			} else {
				if errRedis := redisConnect.SaveToRedisCache(ctx, redisutil.COMMAND_SEARCH_HYPERVISOR, redisKeyValue, protoMarshaledData); errRedis != nil {
					if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
						// Not a fatal error but log it as it represents system instability
						logger.Log.Info(errRedis.Error())
					} else {
						// Actual REDIS error that needs to be displayed
						logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyValue, errRedis))
					}
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}

// Performs the Shared Storage report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchSharedStorageSQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.SharedStorageReportRequest, stream dora.Dora_GetSharedStorageReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.SharedStorageReportResponse, 64)

	if sharedStorageTypeRegexp, errRegexp := regexp.Compile(query.GetStorageType()); errRegexp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Storage ID filter regex is invalid: %v", errRegexp))
	} else {
		if sharedStorageNameRegexp, errRegexp := regexp.Compile(query.GetStorageName()); errRegexp != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Storage Name filter regex is invalid: %v", errRegexp))
		} else {
			if errSearch := vcSyncDb.GenerateSharedStorageReport(ctx, sharedStorageTypeRegexp, sharedStorageNameRegexp, channelResults); errSearch != nil {
				// Fatal - Something wrong with search pattern or initial database connection
				return status.Errorf(codes.Internal, "Shared Storage report search failed to start because of error: %v", errSearch)
			} else {
				// Connection OK - stream results
				for {
					var sharedStorageReportResponse = <-channelResults

					if sharedStorageReportResponse == nil {
						// End of records reached
						break
					} else {
						sharedStorageReportResponse.VcenterName = vcenterName

						// Send new record to stream for immediate display to the frontend
						stream.Send(sharedStorageReportResponse)
					}
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}


// Performs the Datastore report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchDatastoreSQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.DatastoreReportRequest, stream dora.Dora_GetDatastoreReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.DatastoreReportResponse, 64)

	if hypervisorPathRegexp, errRegexp := regexp.Compile(query.GetHypervisorPathFilter()); errRegexp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Hypervisor path filter regex is invalid: %v", errRegexp))
	} else {
		if datastorePathRegexp, errRegexp := regexp.Compile(query.GetDatastorePathFilter()); errRegexp != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Datastore path filter regex is invalid: %v", errRegexp))
		} else {
			if errSearch := vcSyncDb.GenerateDatastoreReport(ctx, hypervisorPathRegexp, datastorePathRegexp, channelResults); errSearch != nil {
				// Fatal - Something wrong with search pattern or initial database connection
				return status.Errorf(codes.Internal, "Datastore report search failed to start because of error: %v", errSearch)
			} else {
				// Connection OK - stream results
				for {
					var datastoreReportResponse = <-channelResults

					if datastoreReportResponse == nil {
						// End of records reached
						break
					} else {
						datastoreReportResponse.VcenterName = vcenterName

						// Send new record to stream for immediate display to the frontend
						stream.Send(datastoreReportResponse)
					}
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}

// Performs the NFS report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchNfsSQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.NfsReportRequest, stream dora.Dora_GetNfsReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.NfsReportResponse, 64)

	if nfsHostRegexp, errRegexp := regexp.Compile(query.GetNfsHost()); errRegexp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("NFS host filter regex is invalid: %v", errRegexp))
	} else {
		if nfsPathRegexp, errRegexp := regexp.Compile(query.GetNfsPath()); errRegexp != nil {
			// Fatal
			return errors.New(fmt.Sprintf("NFS path filter regex is invalid: %v", errRegexp))
		} else {
			if vmHostRegexp, errRegexp := regexp.Compile(query.GetVmHost()); errRegexp != nil {
				// Fatal
				return errors.New(fmt.Sprintf("VM host filter regex is invalid: %v", errRegexp))
			} else {
				if vmHostPathRegexp, errRegexp := regexp.Compile(query.GetVmPath()); errRegexp != nil {
					// Fatal
					return errors.New(fmt.Sprintf("VM path filter regex is invalid: %v", errRegexp))
				} else {
					if vmMountPointRegexp, errRegexp := regexp.Compile(query.GetVmMountPoint()); errRegexp != nil {
						// Fatal
						return errors.New(fmt.Sprintf("VM mountpoint filter regex is invalid: %v", errRegexp))
					} else {
						if errSearch := vcSyncDb.GenerateNfsReport(ctx, nfsHostRegexp, nfsPathRegexp, vmHostRegexp, vmHostPathRegexp, vmMountPointRegexp, channelResults); errSearch != nil {
							// Fatal - Something wrong with search pattern or initial database connection
							return status.Errorf(codes.Internal, "NFS report search failed to start because of error: %v", errSearch)
						} else {
							// Connection OK - stream results
							for {
								var nfsReportResponse = <-channelResults

								if nfsReportResponse == nil {
									// End of records reached
									break
								} else {
									nfsReportResponse.VcenterName = vcenterName

									// Send new record to stream for immediate display to the frontend
									stream.Send(nfsReportResponse)
								}
							}
						}
					}
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}



// Performs the VM Storage report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchVmStorageSQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.VmStorageReportRequest, stream dora.Dora_GetVmStorageReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.VmStorageReportResponse, 64)

	if util.IsBlank(vcenterName) {
		// Fatal
		return status.Errorf(codes.Internal, "VM Storage report search failed because required VCenter name was not supplied")
	} else {
		if errSearch := vcSyncDb.GenerateVmStorageReport(ctx, channelResults); errSearch != nil {
			// Fatal - Something wrong with search pattern or initial database connection
			return status.Errorf(codes.Internal, "VM Storage report search failed to start because of error: %v", errSearch)
		} else {
			// Connection OK - stream results
			for {
				var reportResponse = <-channelResults

				if reportResponse == nil {
					// End of records reached
					break
				} else {
					reportResponse.VcenterName = vcenterName

					// Send new record to stream for immediate display to the frontend
					stream.Send(reportResponse)
				}
			}
		}

	}

	// No errors if this point is reached
	return nil
}

// Performs the IP Range report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchIpRangeSQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.IpRangeReportRequest, stream dora.Dora_GetIpRangeReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.IpRangeReportResponse, 64)

	if util.IsBlank(vcenterName) {
		// Fatal
		return status.Errorf(codes.Internal, "IP Range report search failed because required VCenter name was not supplied")
	} else {
		if errSearch := vcSyncDb.GenerateIpRangeReport(ctx, channelResults); errSearch != nil {
			// Fatal - Something wrong with search pattern or initial database connection
			return status.Errorf(codes.Internal, "IP Range report search failed to start because of error: %v", errSearch)
		} else {
			// Connection OK - stream results
			for {
				var reportResponse = <-channelResults

				if reportResponse == nil {
					// End of records reached
					break
				} else {
					reportResponse.VcenterName = vcenterName

					// Send new record to stream for immediate display to the frontend
					stream.Send(reportResponse)
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}

// Performs the Storage Summary report streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchStorageSummarySQLite(ctx context.Context, authToken *jwtlib.Token, vcenterName string, query *dora.StorageSummaryReportRequest, stream dora.Dora_GetStorageSummaryReportStreamedServer) error {
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.StorageSummaryReportResponse, 64)

	if util.IsBlank(vcenterName) {
		// Fatal
		return status.Errorf(codes.Internal, "Storage Summary report search failed because required VCenter name was not supplied")
	} else {
		if errSearch := vcSyncDb.GenerateStorageSummaryReport(ctx, channelResults); errSearch != nil {
			// Fatal - Something wrong with search pattern or initial database connection
			return status.Errorf(codes.Internal, "Storage Summary report search failed to start because of error: %v", errSearch)
		} else {
			// Connection OK - stream results
			for {
				var reportResponse = <-channelResults

				if reportResponse == nil {
					// End of records reached
					break
				} else {
					reportResponse.VcenterName = vcenterName

					// Send new record to stream for immediate display to the frontend
					stream.Send(reportResponse)
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}

// Multiple Hypervisor records return (streaming mode)
func searchHypervisorsSoap(authToken *jwtlib.Token, query *dora.HVQueryRequest, stream dora.Dora_GetHypervisorsStreamedServer) error {
	logger.Log.Info(fmt.Sprintf("Received Hypervisor search streaming request for environment: %s and query: (%s, %s)", query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter()))

	var startTime = time.Now()

	// Send UI info about VCenter data source
	if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
		Name:        constants.DATASOURCE_VCENTER,
		Description: "Retrieved from master data source VCenter",
		LastUpdated: -1, // VCenter is the "master" data so has not last_updated concept
	}); errJson != nil {
		// Fatal - unlikely
		logger.Log.Error(errJson, "Could not marshal Datasource record")
	} else {
		stream.Send(&api.HypervisorResponse{
			UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
		})
	}

	//
	// Proceed with lookup (runs on a cache miss or cache disabled)
	//

	// Push a command message to the Redis queue so some Go worker can process it
	//
	var vcenterSearchCmd []byte
	var errJson error
	vcenterSearchCmd, errJson = json.Marshal(workqueue.VCenterSearchCmd{
		AuthToken:      authToken.Raw,
		ConnectionName: query.GetVcenterName(),
		PathRegExp:     query.GetPathFilter(),
		HostRegExp:     query.GetHostFilter(),
		ListOnly:       query.ListingOnly,
	})

	if errJson != nil {
		// Fatal
		return status.Errorf(codes.Internal, "Could not serialize work queue command because of error: %v", errJson)
	}

	//
	// Create and push a work queue command for worker threads to process
	//
	var responseQueueName = workqueue.CreateResponseQueueName("hypervisorSearch")
	var command = redisutil.RedisQueueCommand{
		Command:       redisutil.COMMAND_SEARCH_HYPERVISOR,
		Data:          string(vcenterSearchCmd),
		ResponseQueue: responseQueueName,
	}
	if errCommand := redisConnect.PushCommand(command); errCommand != nil {
		// Fatal
		return status.Errorf(codes.Internal, "Could not push command '%v' onto the work queue because of error: %v", command, errCommand)
	}

	//
	// Use the "response queue" and wait for the background threads to provide their results.
	// Loop through all response items until the worker thread indicates it is completed.
	//
outer:
	for {
		// Now wait for worker thread to push results onto the specified queue name specified in the command
		if dataString, errQueue := redisConnect.WaitForItem(command.ResponseQueue); errQueue != nil {
			// Fatal
			//return status.Errorf(codes.Internal, "Worker thread Could not pull command '%v' onto the work queue because of error: %v", command, errQueue)
		} else {
			var hypervisorResponse = dora.HypervisorResponse{}
			var queueResponse = workqueue.QueueResponseWrapper{}

			if errQueueResponse := json.Unmarshal([]byte(*dataString), &queueResponse); errQueueResponse != nil {
				// Fatal - could not unmarshal the queue response object for whatever reason
				return status.Errorf(codes.Internal, "Could not unmarshal queue response object '%v' because of error: %v", queueResponse, errQueueResponse)
			} else {
				switch queueResponse.StatusCode {
				case workqueue.STREAM_STATUS_OK: // Response OK
					if protoDataBytes, errBase64 := base64.StdEncoding.DecodeString(queueResponse.Data); errBase64 != nil {
						// Fatal - Base64 decode failure
						return status.Errorf(codes.Internal, "Could base64 decode the protobuf record because of error: %v", errBase64)
					} else {
						// Reconstitute the ProtoBuf record from the serialized binary data to prepare for transmission
						// back the client with gRPC stream
						if errProtoUnmarshal := proto.Unmarshal(protoDataBytes, &hypervisorResponse); errProtoUnmarshal != nil {
							// Fatal
							return status.Errorf(codes.Internal, "Could not unmarshal binary protobuf record because of error: %v", errProtoUnmarshal)
						} else {
							// Send ProtoBuf binary data record response to the client's grpc stream directly
							// which allows clients to see the record immediately
							if errStream := stream.Send(&hypervisorResponse); errStream != nil {
								// Not fatal but log it
								logger.Log.Info(fmt.Sprintf("Stream send of record caused error: %v", errStream))
							}
						}
					}
				case workqueue.STREAM_STATUS_ERROR: // Response: Error
					return status.Errorf(codes.Internal, "Stream encountered fatal error during processing: %v", queueResponse.Data)
				case workqueue.STREAM_STATUS_COMPLETE: // Response: Completed
					// Normal stream completion
					logger.Log.Info(fmt.Sprintf("Hypervisor search completed in %d secs for Connection '%s' and Query (%s, %s)", time.Now().Unix()-startTime.Unix(), query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter()))
					break outer
				}
			}
		}
	}

	// No errors if this point reached
	return nil
}

// Multiple VirtualMachine records return (streaming mode)
func (s *server) GetVirtualMachinesStreamed(query *dora.VMQueryRequest, stream dora.Dora_GetVirtualMachinesStreamedServer) error {
	logger.Log.Info(fmt.Sprintf("Received VirtualMachine search streaming request with parameter list: (vcenter: %s, path filter: %s, host filter: %s)", query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter()))

	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GET_VIRTUALMACHINES_STREAMED) // Start a new root span
	defer span.Finish()

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctx = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	// Trim the text values
	query.VcenterName = strings.TrimSpace(query.VcenterName)
	query.PathFilter = strings.TrimSpace(query.PathFilter)
	query.HostFilter = strings.TrimSpace(query.HostFilter)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		// Redis base key value
		var redisKeyBaseValue = redisutil.GenerateVmSearchKeyFragment(query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly())

		// Perform redis lookup
		if virtualMachineList, errRedis := redisConnect.GetCacheVirtualMachineList(ctx, redisKeyBaseValue); errRedis != nil {
			if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
				logger.Log.Info(errRedis.Error())
			} else {
				// Not-fatal but log it.  Can work without caching but administrator should know about this
				// error as it represents application instability.
				logger.Log.Error(errRedis, fmt.Sprintf("Redis cache lookup failed because of error: %v", errRedis))
			}
		} else {
			if virtualMachineList != nil {
				// Cache hit - just send the results back to client which bypasses call to VCenter
				logger.Log.Info("VirtualMachine search Redis Cache hit")

				// Send UI info about Redis data source
				if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
					Name:        constants.DATASOURCE_REDIS,
					Description: "Retrieved from super fast Redis local short-term cache",
					LastUpdated: redisConnect.KeyCreationTime(redisutil.GenerateVirtualMachineKey(redisKeyBaseValue)) * 1000, // Derive this from current time secs - Redis TTL secs
				}); errJson != nil {
					// Fatal - unlikely
					logger.Log.Error(errJson, "Could not marshal Datasource record")
				} else {
					stream.Send(&api.VirtualMachineResponse{
						UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
					})
				}

				// Send unmarshaled Redis cache data row directly back to the client
				for _, virtualMachineResponse := range virtualMachineList.Items {
					stream.Send(virtualMachineResponse)
				}

				// Done.  Do not continue with VCenter lookup below.
				return nil
			}
		}

		logger.Log.Info("VirtualMachine search Redis Cache miss.  Will fall back to either SQLite or direct VCenter search.")

		// Redis cache miss.  Use backends to lookup in the following precedence order:
		// (1) SQLite - preferred if available
		// (2) VCenter - last precedence - slow but is the master data source
		//
		// Use SQLite fast search if its determined to be available otherwise fallback to slow VCenter SOAP search
		//
		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search feature is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				if dbCache == nil || dbCache.GetRecordCount("VirtualMachine") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.VirtualMachineResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			var dbCache = dbSyncIndex.GetSyncObject(vcenterName)
			if dbCache != nil && dbCache.GetRecordCount("VirtualMachine") > 0 {
				//
				// SQLite fast mode (db cached)
				//
				logger.Log.Info("Searching VirtualMachine records via fast SQLite cached mode...")
				var searchError = searchVirtualMachinesSqlite(authToken, vcenterName, query, stream)

				if searchError != nil {
					searchErrors = append(searchErrors, searchError)
				}
			} else {
				//
				// SOAP mode (direct access to master data but slow) -- may be only available for certain user types
				// depending on the current VCenter access mode in app's configuration
				//
				if deniedErr := checkVCenterDirectAccessEnabled(authToken, query.GetVcenterName()); deniedErr != nil {
					// VCenter access disabled for given user and mode.  NOTE: error contains descriptive message
					searchErrors = append(searchErrors, deniedErr)
				} else {
					logger.Log.Info("Searching VirtualMachine records via SOAP/VCenter direct mode...")
					var searchError = searchVirtualMachinesSoap(authToken, query, stream)

					if searchError != nil {
						searchErrors = append(searchErrors, searchError)
					}
				}
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// Gets room report with response streamed
func (s *server) GetRoomStreamed(query *dora.QueryRequest, stream dora.Dora_GetRoomStreamedServer) error {
	var ctx = context.Background()
	logger.Log.Info(fmt.Sprintf("Received Room search streaming request for environment: %s and query: %s", query.GetConnectionName(), query.GetSearchPattern()))

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		// Redis key used for cache lookup/retrieval (must be the exact parameters and order saved to cache previously)
		var redisKeyBaseValue = redisutil.CreateKeyBaseValue(query.ConnectionName, query.SearchPattern)

		// Perform redis lookup
		if roomReport, errRedis := redisConnect.GetCacheRoomReport(ctx, redisKeyBaseValue); errRedis != nil {
			if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
				logger.Log.Info(errRedis.Error())
			} else {
				// Not-fatal but log it.  Can work without caching but administrator should know about this
				// error as it represents application instability.
				logger.Log.Error(errRedis, fmt.Sprintf("Redis cache lookup failed because of error: %v", errRedis))
			}
		} else {
			if roomReport != nil {
				// Cache hit - just send the results back to client which bypasses call to backend

				// Send UI info about Redis data source
				if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
					Name:        constants.DATASOURCE_REDIS,
					Description: "Retrieved from super fast Redis local short-term cache",
					LastUpdated: redisConnect.KeyCreationTime(redisutil.GenerateRoomReportKey(redisKeyBaseValue)) * 1000, // Derive this from current time secs - Redis TTL secs
				}); errJson != nil {
					// Fatal - unlikely
					logger.Log.Error(errJson, "Could not marshal Datasource record")
				} else {
					stream.Send(&api.RoomResponse{
						UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
					})
				}

				// Send unmarshaled Redis cache data row directly back to the client
				for _, roomReportResponse := range roomReport.Items {
					stream.Send(roomReportResponse)
				}

				// Done.  Cache was hit so do not continue with below backend lookup.
				return nil
			}
		}

		// Redis cache miss.  Use backends to lookup in the following precedence order:
		// (1) SQLite - preferred if available
		// (2) VCenter - last precedence - slow but is the master data source
		var dbCache = dbSyncIndex.GetSyncObject(query.ConnectionName)
		if dbCache != nil && dbCache.GetRecordCount("VirtualMachine") > 0 {
			//
			// SQLite fast mode (db cache mode)
			//
			logger.Log.Info("Generating Room report via fast SQLite cached mode...")
			return GenerateRoomReportSQLite(ctx, dbCache, query, stream)
		} else {
			//
			// SOAP slow mode (master data)
			//
			logger.Log.Info("Generating Room report via fast SOAP/VCenter direct mode...")
			return status.Errorf(codes.Unimplemented, "Room report not yet implemented for VCenter mode -- use SQLite mode instead for now.")
		}
	}
}

// Performs the VirtualMachine streaming search using SQLite cached backend which provides extremely fast response times.
// NOTE: This is the preferred normal search method but requires the SQLite backend is consistently synchronized with
// VCenter!
//
func searchVirtualMachinesSqlite(authToken *jwtlib.Token, vcenterName string, query *dora.VMQueryRequest, stream dora.Dora_GetVirtualMachinesStreamedServer) error {
	var ctx = context.Background()
	var vcSyncDb = dbSyncIndex.GetSyncObject(vcenterName)
	var channelResults = make(chan *api.VirtualMachineResponse, 64)
	var virtualMachineResponseArray = dora.VirtualMachineResponseArray{}

	// Compile the regex expressions.  Throw a fatal error if any query fails to compile.
	//
	if regexpPathFilter, errRegExp := regexp.Compile(query.GetPathFilter()); errRegExp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("VCenter \"path\" regexp pattern is invalid: %v", errRegExp))
	} else if regexpHostFilter, errRegExp := regexp.Compile(query.GetHostFilter()); errRegExp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("VCenter \"host\" regexp pattern is invalid: %v", errRegExp))
	} else {
		if errSearch := vcSyncDb.VirtualMachineRegexpSearch(regexpPathFilter, regexpHostFilter, channelResults); errSearch != nil {
			// Fatal - Something wrong with search pattern or initial database connection
			return status.Errorf(codes.Internal, "VirtualMachine search failed to start because of error: %v", errSearch)
		} else {
			// Connection OK - stream results
			for {
				var virtualMachineResponse = <-channelResults

				if virtualMachineResponse == nil {
					// End of records reached
					break
				} else {
					virtualMachineResponse.Vcenter = vcenterName;

					// Send new record to stream for immediate display to the frontend
					stream.Send(virtualMachineResponse)

					// Also accumulate to a list for Redis caching below
					if !virtualMachineResponse.UiControlIsPreview &&
						len(virtualMachineResponse.UiMsgInfo) == 0 &&
						len(virtualMachineResponse.UiMsgWarning) == 0 &&
						len(virtualMachineResponse.UiMsgError) == 0 {
						virtualMachineResponseArray.Items = append(virtualMachineResponseArray.Items, virtualMachineResponse)
					}
				}
			}

			// Save to the Redis cache so later lookups on same query will return the result instantly (up until
			// the cache record expires via TTL though)
			var redisKeyValue = redisutil.GenerateVmSearchKeyFragment(vcenterName, query.GetPathFilter(), query.GetHostFilter(), query.GetListingOnly())
			if protoDataMarshalled, errProto := proto.Marshal(&virtualMachineResponseArray); errProto != nil {
				// Fatal
				return errors.New(fmt.Sprintf("Proto marshalling failed for caching because of error: %v", errProto))
			} else {
				if errRedis := redisConnect.SaveToRedisCache(ctx, redisutil.COMMAND_SEARCH_VIRTUALMACHINE, redisKeyValue, protoDataMarshalled); errRedis != nil {
					if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
						// Not a fatal error but log it as it represents system instability
						logger.Log.Info(errRedis.Error())
					} else {
						// Actual REDIS error that needs to be displayed
						logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyValue, errRedis))
					}
				}
			}
		}

		// No errors if this point is reached
		return nil
	}
}

// Multiple VirtualMachine records return (streaming mode)
func searchVirtualMachinesSoap(authToken *jwtlib.Token, query *dora.VMQueryRequest, stream dora.Dora_GetVirtualMachinesStreamedServer) error {
	// Send UI info about VCenter data source
	if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
		Name:        constants.DATASOURCE_VCENTER,
		Description: "Retrieved from master data source VCenter",
		LastUpdated: -1, // VCenter is the "master" data so has not last_updated concept
	}); errJson != nil {
		// Fatal - unlikely
		logger.Log.Error(errJson, "Could not marshal Datasource record")
	} else {
		stream.Send(&api.VirtualMachineResponse{
			UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
		})
	}

	var startTime = time.Now()

	//
	// Proceed with lookup (runs on a cache miss or cache disabled)
	//

	// Push a command message to the Redis queue so some Go worker can process it
	//
	var workQueueCmd []byte
	var errJson error
	workQueueCmd, errJson = json.Marshal(workqueue.VCenterSearchCmd{
		AuthToken:      authToken.Raw,
		ConnectionName: query.GetVcenterName(),
		PathRegExp:     query.GetPathFilter(),
		HostRegExp:     query.GetHostFilter(),
		ListOnly:       query.ListingOnly,
	})

	if errJson != nil {
		// Fatal
		return status.Errorf(codes.Internal, "Could not serialize work queue command because of error: %v", errJson)
	}

	//
	// Create and push a work queue command for background threads asynchronously
	//
	var responseQueueName = workqueue.CreateResponseQueueName(redisutil.COMMAND_SEARCH_VIRTUALMACHINE)
	var command = redisutil.RedisQueueCommand{
		Command:       redisutil.COMMAND_SEARCH_VIRTUALMACHINE,
		Data:          string(workQueueCmd),
		ResponseQueue: responseQueueName,
	}
	if errCommand := redisConnect.PushCommand(command); errCommand != nil {
		// Fatal
		return status.Errorf(codes.Internal, "Could not push command '%v' onto the work queue because of error: %v", command, errCommand)
	}

	//
	// Use the "response queue" and wait for the background threads to provide their results.
	// Loop through all response items until the worker thread indicates it is completed.
	//
outer:
	for {
		// Now wait for worker thread to push results onto the specified queue name specified in the command
		if dataString, errQueue := redisConnect.WaitForItem(command.ResponseQueue); errQueue != nil {
			// Not-fatal but log it - worker thread an continue waiting
			logger.Log.Info(fmt.Sprintf("Worker thread Could not pull command '%v' onto the work queue because of error: %v", command, errQueue))
		} else {
			var virtualMachineResponse = dora.VirtualMachineResponse{}
			var queueResponse = workqueue.QueueResponseWrapper{}

			if errQueueResponse := json.Unmarshal([]byte(*dataString), &queueResponse); errQueueResponse != nil {
				// Fatal - could not unmarshal the queue response object for whatever reason
				return status.Errorf(codes.Internal, "Could not unmarshal queue response object '%v' because of error: %v", queueResponse, errQueueResponse)
			} else {
				switch queueResponse.StatusCode {
				case workqueue.STREAM_STATUS_OK: // Response OK
					if protoDataBytes, errBase64 := base64.StdEncoding.DecodeString(queueResponse.Data); errBase64 != nil {
						// Fatal - Base64 decode failure
						return status.Errorf(codes.Internal, "Could base64 decode the protobuf record because of error: %v", errBase64)
					} else {
						// Reconstitute the ProtoBuf record from the serialized binary data to prepare for transmission
						// back the client with gRPC stream
						if errProtoUnmarshal := proto.Unmarshal(protoDataBytes, &virtualMachineResponse); errProtoUnmarshal != nil {
							// Fatal
							return status.Errorf(codes.Internal, "Could not unmarshal binary protobuf record because of error: %v", errProtoUnmarshal)
						} else {
							// Send ProtoBuf binary data record response to the client's grpc stream directly
							// which allows clients to see the record immediately
							if errStream := stream.Send(&virtualMachineResponse); errStream != nil {
								// Not fatal but log it
								logger.Log.Info(fmt.Sprintf("Stream send of record caused error: %v", errStream))
							}
						}
					}
				case workqueue.STREAM_STATUS_ERROR: // Response: Error
					return status.Errorf(codes.Internal, "Stream encountered fatal error during processing: %v", queueResponse.Data)
				case workqueue.STREAM_STATUS_COMPLETE: // Response: Completed
					// Normal stream completion
					logger.Log.Info(fmt.Sprintf("VirtualMachine search completed in %d secs for Connection '%s' and Query (pathFilter: '%s', hostFilter '%s')", time.Now().Unix()-startTime.Unix(), query.GetVcenterName(), query.GetPathFilter(), query.GetHostFilter()))
					break outer
				}
			}
		}
	}

	// No errors if this point reached
	return nil
}



// Shared Storage Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetSharedStorageReportStreamed(query *dora.SharedStorageReportRequest, stream dora.Dora_GetSharedStorageReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETSHAREDSTORAGE_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetSharedStorageReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				if dbCache == nil || dbCache.GetRecordCount("Datastore") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.SharedStorageReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			var dbCache = dbSyncIndex.GetSyncObject(vcenterName)
			if dbCache != nil && dbCache.GetRecordCount("Datastore") > 0 {
				//
				// SQLite fast mode (DB cached data)
				//
				logger.Log.Info("Searching Shared Storage records via fast SQLite cached mode...")
				var searchError = searchSharedStorageSQLite(ctxSpan, authToken, vcenterName, query, stream)

				if searchError != nil {
					searchErrors = append(searchErrors, searchError)
				}
			} else {
				//
				// FATAL: Direct VCenter SOAP access is not available for Shared Storage report
				//
				return status.Errorf(codes.Unimplemented, "Shared Storage report not available for direct VCenter mode -- check that SQLite data cache is available for the environment")
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// Datastore Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetDatastoreReportStreamed(query *dora.DatastoreReportRequest, stream dora.Dora_GetDatastoreReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETDATASTORE_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetDatastoreReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				if dbCache == nil || dbCache.GetRecordCount("Datastore") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.DatastoreReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			var dbCache = dbSyncIndex.GetSyncObject(vcenterName)
			if dbCache != nil && dbCache.GetRecordCount("Datastore") > 0 {
				//
				// SQLite fast mode (DB cached data)
				//
				logger.Log.Info("Searching Datastore records via fast SQLite cached mode...")
				var searchError = searchDatastoreSQLite(ctxSpan, authToken, vcenterName, query, stream)

				if searchError != nil {
					searchErrors = append(searchErrors, searchError)
				}
			} else {
				//
				// FATAL: Direct VCenter SOAP access is not available for Datastore report
				//
				return status.Errorf(codes.Unimplemented, "Datastore report not available for direct VCenter mode -- check that SQLite data cache is available for the environment")
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// NFS Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetNfsReportStreamed(query *dora.NfsReportRequest, stream dora.Dora_GetNfsReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETNFS_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetNfsReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				if dbCache == nil || dbCache.GetRecordCount("NfsMount") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.NfsReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			var dbCache = dbSyncIndex.GetSyncObject(vcenterName)
			if dbCache != nil && dbCache.GetRecordCount("NfsMount") > 0 {
				//
				// SQLite fast mode (DB cached data)
				//
				logger.Log.Info("Searching NFS records via fast SQLite cached mode...")
				var searchError = searchNfsSQLite(ctxSpan, authToken, vcenterName, query, stream)

				if searchError != nil {
					searchErrors = append(searchErrors, searchError)
				}
			} else {
				//
				// FATAL: Direct VCenter SOAP access is not available for NFS report
				//
				return status.Errorf(codes.Unimplemented, "NFS report not available for direct VCenter mode -- check that SQLite data cache is available for the environment")
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// VM Storage Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetVmStorageReportStreamed(query *dora.VmStorageReportRequest, stream dora.Dora_GetVmStorageReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETVMSTORAGE_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetVmStorageReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				// NOTE: in this case, use the "VirtualMachine" table to test that DB has data since the VM Storage
				// report can actually be empty in normal operation
				if dbCache == nil || dbCache.GetRecordCount("VirtualMachine") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.VmStorageReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			//
			// SQLite fast mode (DB cached data)
			//
			var searchError = searchVmStorageSQLite(ctxSpan, authToken, vcenterName, query, stream)

			if searchError != nil {
				searchErrors = append(searchErrors, searchError)
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// Uploads the VM Storage records into the database.
//
// NOTE: The AppendMode flag will determine if existing data is overwritten or appended to
//
func (s *server) UploadVmStorageData(ctx context.Context, request *dora.VmStorageUploadRequest) (*dora.VmStorageUploadResponse, error) {
	var response = api.VmStorageUploadResponse{
		Status:   "",  // TBD
		Warnings: nil,
		Errors:   nil,
	}

	var span = opentracing.StartSpan(profiling.OP_UPLOADVMSTORAGE_REPORT) // Start a new root span
	defer span.Finish()

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(ctx, profiling.PARENT_SPAN_NAME, span)

	// Get the VCenter connection

	if authToken, authError := auth.CheckToken(request.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var vcenterIndex = dbSyncIndex.GetSyncObject(request.GetVcenterName())
		var connection = vcenterIndex.GetConnectionPool().Get(ctxSpan)
		defer connection.Close()

		if errUpload := vcenterIndex.UploadVmStorage(ctxSpan, connection, request.GetRecords(), request.GetAppendMode()); errUpload != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Upload failed because of error: %v", errUpload.Error()))
		}
	}

	// Successful upload if this point is reached
	response.Status = "OK"
	response.RecordsUploaded = int32(len(request.GetRecords()))
	return &response, nil
}


// IP Range Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetIpRangeReportStreamed(query *dora.IpRangeReportRequest, stream dora.Dora_GetIpRangeReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETIPRANGE_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetIpRangeReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				// NOTE: in this case, use the "VirtualMachine" table to test that DB has data since the VM Storage
				// report can actually be empty in normal operation
				if dbCache == nil || dbCache.GetRecordCount("VirtualMachine") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.IpRangeReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			//
			// SQLite fast mode (DB cached data)
			//
			var searchError = searchIpRangeSQLite(ctxSpan, authToken, vcenterName, query, stream)

			if searchError != nil {
				searchErrors = append(searchErrors, searchError)
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}


// Storage Summary Report.  Implements the gRPC end point defined in the proto.
//
func (s *server) GetStorageSummaryReportStreamed(query *dora.StorageSummaryReportRequest, stream dora.Dora_GetStorageSummaryReportStreamedServer) error {
	// Declare a new root OpenTracing span since this is the beginning of the API call
	var span = opentracing.StartSpan(profiling.OP_GETSTORAGESUMMARY_REPORT) // Start a new root span
	defer span.Finish()

	log.Printf("Received GetStorageSummaryReport Query: (%s)", query.VcenterName)

	span.SetTag("Query", fmt.Sprintf("%v", query))

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(context.Background(), profiling.PARENT_SPAN_NAME, span)

	if authToken, authError := auth.CheckToken(query.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var allModeEnabled = strings.TrimSpace(query.GetVcenterName()) == constants.DORA_SEARCH_ALL
		var vcenterNames = make([]string, 0)

		// Determine if special "all" VCenter search mode is activated
		if allModeEnabled {
			// All mode - set to load *all* available VCenter
			for _, vcenterName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
				var dbCache = dbSyncIndex.GetSyncObject(vcenterName)

				// Only add VCenter that have SQLite data and warn to user otherwise
				// NOTE: in this case, use the "VirtualMachine" table to test that DB has data since the VM Storage
				// report can actually be empty in normal operation
				if dbCache == nil || dbCache.GetRecordCount("VirtualMachine") == 0 {
					// Warning: this VCenter doesn't have SQLite cache data so will *not* be searched in "all" mode
					stream.Send(&api.StorageSummaryReportResponse{
						UiMsgWarning: fmt.Sprintf("VCenter '%s' does not have SQLite cache data so will not be searched", vcenterName),
					})
				} else {
					vcenterNames = append(vcenterNames, vcenterName)
				}
			}
		} else {
			// Single mode - set to load the single VCenter
			vcenterNames = append(vcenterNames, query.GetVcenterName())
		}

		var searchErrors = make([]error, 0)
		for _, vcenterName := range vcenterNames {
			//
			// SQLite fast mode (DB cached data)
			//
			var searchError = searchStorageSummarySQLite(ctxSpan, authToken, vcenterName, query, stream)

			if searchError != nil {
				searchErrors = append(searchErrors, searchError)
			}
		}

		// Process any errors that might have occurred in setting up the search above
		if len(searchErrors) > 0 {
			var errorMessageCombined = ""

			// Collect errors
			for _, err := range searchErrors {
				errorMessageCombined += err.Error() + " "
			}

			return errors.New(strings.TrimSpace(errorMessageCombined))
		} else {
			// No fatal errors encountered
			return nil
		}
	}
}

// Uploads the VM Storage records into the database.
//
// NOTE: The AppendMode flag will determine if existing data is overwritten or appended to
//
func (s *server) UploadIpRangeData(ctx context.Context, request *dora.IpRangeUploadRequest) (*dora.IpRangeUploadResponse, error) {
	var response = api.IpRangeUploadResponse{
		Status:   "",  // TBD
		Warnings: nil,
		Errors:   nil,
	}

	var span = opentracing.StartSpan(profiling.OP_UPLOADIPRANGE_REPORT) // Start a new root span
	defer span.Finish()

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(ctx, profiling.PARENT_SPAN_NAME, span)

	// Get the VCenter connection

	if authToken, authError := auth.CheckToken(request.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var vcenterIndex = dbSyncIndex.GetSyncObject(request.GetVcenterName())
		var connection = vcenterIndex.GetConnectionPool().Get(ctxSpan)
		defer connection.Close()

		if errUpload := vcenterIndex.UploadIpStorage(ctxSpan, connection, request.GetRecords(), request.GetAppendMode()); errUpload != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Upload failed because of error: %v", errUpload.Error()))
		}
	}

	// Successful upload if this point is reached
	response.Status = "OK"
	response.RecordsUploaded = int32(len(request.GetRecords()))
	return &response, nil
}

// Uploads the Summary Storage records into the database.
//
// NOTE: The AppendMode flag will determine if existing data is overwritten or appended to
//
func (s *server) UploadStorageSummaryData(ctx context.Context, request *dora.StorageSummaryUploadRequest) (*dora.StorageSummaryUploadResponse, error) {
	var response = api.StorageSummaryUploadResponse{
		Status:   "",  // TBD
		Warnings: nil,
		Errors:   nil,
	}

	var span = opentracing.StartSpan(profiling.OP_UPLOADSTORAGESUMMARY_REPORT) // Start a new root span
	defer span.Finish()

	// Store the newly created OpenTrace span into new "Value-based" context object with defned PARENT_SPAN_NAME key
	// so subsequent methods can attach their child spans to it and "build out" the trace tree
	var ctxSpan = context.WithValue(ctx, profiling.PARENT_SPAN_NAME, span)

	// Get the VCenter connection

	if authToken, authError := auth.CheckToken(request.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//
		log.Println(fmt.Sprintf("User: %s", authToken.Claims.(*auth.Claims).Username))

		var vcenterIndex = dbSyncIndex.GetSyncObject(request.GetVcenterName())
		var connection = vcenterIndex.GetConnectionPool().Get(ctxSpan)
		defer connection.Close()

		if errUpload := vcenterIndex.UploadStorageSummary(ctxSpan, connection, request.GetRecords(), request.GetAppendMode()); errUpload != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Upload failed because of error: %v", errUpload.Error()))
		}
	}

	// Successful upload if this point is reached
	response.Status = "OK"
	response.RecordsUploaded = int32(len(request.GetRecords()))
	return &response, nil
}

// ENDPOINT - Login
func (s *server) GetLoginToken(req context.Context, loginRequest *dora.LoginRequest) (*dora.LoginResponse, error) {
	var span = opentracing.StartSpan(profiling.OP_DORA_GETLOGIN) // Start a new root span
	var ctx = context.WithValue(req, profiling.PARENT_SPAN_NAME, span)
	defer span.Finish()

	if groups, errLdap := ldap.LoginAndGetGroups(ctx, loginRequest.Username, loginRequest.Password); errLdap != nil {
		// Fatal - could be simply due to invalid credentials though.
		return nil, errors.New(fmt.Sprintf("Could not validate login for user '%s' because of LDAP error: %v", loginRequest.Username, errLdap))
	} else {
		// Login credentials successfully validated & groups retrieved from LDAP
		// Example group record: cn=dora-admins,cn=groups,cn=accounts,dc=infra,dc=smf1,dc=mobitv
		if tokenString, token, errJwt := auth.GenerateJwtToken(ctx, loginRequest.Username, groups...); errJwt != nil {
			// Fatal - unknown JWT error
			return nil, errors.New(fmt.Sprintf("Could not generate JWT auth token for user '%s' because of error: %v", loginRequest.Username, errJwt))
		} else {
			// SUCCESSFUL TOKEN GENERATION
			//
			logger.Log.Info(fmt.Sprintf("Successfully Generated new auth token for user '%s' with expiration date: %v", loginRequest.Username, time.Unix(token.Claims.(*auth.Claims).ExpiresAt, 0)))

			// Create new session record in the login directory which is needed for (a) session expiration control &
			// (b) metrics for Prometheus
			auth.LoginSessionsDirectory.AddToken(tokenString, token)

			// Generate JWT access token and return it wrapped in a gRPC response object
			return &dora.LoginResponse{Token: tokenString}, nil
		}
	}
}

func (s *server) KeepAlive(ctx context.Context, request *dora.KeepAliveRequest) (*dora.KeepAliveResponse, error) {
	// Check for valid auth first
	if authToken, authError := auth.CheckToken(request.AuthToken); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		//
		// Authentication & Authorization accepted
		//

		var claims = authToken.Claims.(*auth.Claims)

		// SUCCESS - Generate new token, update internal logins table, give new token to client
		if tokenString, token, errJwt := auth.GenerateJwtToken(ctx, claims.Username, claims.Groups...); errJwt != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Cannot generate a new token because of error: %v", errJwt))
		} else {
			// Replace user's current token with updated replacement that has extended timeout
			auth.LoginSessionsDirectory.PurgeToken(request.AuthToken)
			auth.LoginSessionsDirectory.AddToken(tokenString, token)

			return &dora.KeepAliveResponse{
				AuthTokenReplacement: tokenString,
				DoraVersion:          "",
				Error:                "",
			}, nil
		}
	}
}

func (s *server) Logout(ctx context.Context, request *dora.LogoutRequest) (*dora.LogoutResponse, error) {
	auth.LoginSessionsDirectory.PurgeToken(request.AuthToken)

	return &dora.LogoutResponse{Success: true}, nil
}

// Initiates manual VCenter sync for the referenced environment
//
// PARAM: ctx   standard Go context
// PARAM: query request parameters
// SEE: ResyncNfs
//
func (s *server) ResyncVCenter(ctx context.Context, query *dora.ResyncVCenterRequest) (*dora.SyncHistoryRec, error) {
	// Verify authentication and authorization first
	logger.Log.Info(fmt.Sprintf("Received request for to start VCenter-to-SQLite sync for environment: %s", query.GetEnvironmentName()))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Perform the sync only after successful authentication & authorization
		return syncVCenter(ctx, query)
	}
}

// Performs the backend logic to run the actual VCenter data sync
//
func syncVCenter(ctx context.Context, request *dora.ResyncVCenterRequest) (*dora.SyncHistoryRec, error) {
	logger.Log.Info(fmt.Sprintf("Starting SQLite sync for environment %s", request.EnvironmentName))
	ctx = context.WithValue(ctx, constants.CTX_KEY_AUTH_USERNAME, auth.GetUsername(request.AuthToken))

	// Get the sync SQLite instance for the requested environment name ("NewPayTV", "OldPayTV, "Comporium", etc)
	var vCenterSyncRec = dbSyncIndex.GetSyncObject(request.GetEnvironmentName())

	// Initiate the sync task and return the sync record to the client.  The sync will proceed in the background and
	// the user can check on sync status via the SyncHistory record.
	if syncHistoryId, errSyncStart := vCenterSyncRec.StartDataSyncInBackground(ctx); errSyncStart != nil {
		// Synchronization process failed to start because of some error
		return nil, status.Errorf(codes.Internal, "Could not create SyncHistory record for VCenter-to-SQLite sync for environment '%s' because of error: %v", request.GetEnvironmentName(), errSyncStart)
	} else {
		// Query the SQLite database for the sync record matching the given ID and return to user as confirmation
		// of the resync start
		if syncHistoryRec, errSyncStart := vCenterSyncRec.GetSyncHistoryRecord(syncHistoryId); errSyncStart != nil {
			// FATAL
			return nil, errors.New(fmt.Sprintf("Could not initiate VCenter-to SQLite sync for environment '%s' because of error: %v", request.GetEnvironmentName(), errSyncStart))
		} else {
			// Sync successfully started in the background. Return the ID of the newly created sync history record
			// which allows the user to view the progress by viewing that record directly in the SQLite database.
			return syncHistoryRec, nil
		}
	}
}

// Initiates manual NFS sync for the referenced environment
//
// PARAM: ctx   standard Go context
// PARAM: query request parameters
// SEE: ResyncVCenter
//
func (s *server) ResyncNfs(ctx context.Context, query *dora.ResyncNfsRequest) (*dora.NfsSyncHistoryRec, error) {
	// Verify authentication and authorization first
	logger.Log.Info(fmt.Sprintf("Received request for to start VCenter-to-SQLite sync for environment: %s", query.GetEnvironmentName()))

	// Add username to context for sync history logging
	ctx = context.WithValue(ctx, constants.CTX_KEY_AUTH_USERNAME, auth.GetUsername(query.AuthToken))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// VCenter connection is needed because NFS logic needs the VM list from VCenter
		var vcenterSyncRec = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

		// Perform the sync only after successful authentication & authorization
		if nfsSyncRec, errSyncStart := nfs.StartSyncInBackground(auth.GetUsername(query.GetAuthToken()), vcenterSyncRec); errSyncStart != nil {
			// Fatal - Synchronization process failed to start because of unexpected error
			return nil, status.Errorf(codes.Internal, "Could not create NFS SyncHistory record for NFS sync for environment '%s' because of error: %v", query.GetEnvironmentName(), errSyncStart)
		} else {
			return nfsSyncRec, nil
		}

		//
		// NOTE: This function ends here but the NFS sync continues to run in the background from above
		//
	}
}


// Retrieves SyncHistory records for viewing by clients
//
func (s *server) GetVCenterSyncHistory(ctx context.Context, query *dora.SyncHistoryRequest) (*dora.SyncHistoryRecArray, error) {
	// Verify authentication and authorization first

	logger.Log.Info(fmt.Sprintf("Received request for VCenter SyncHistory for environment: %s", query.GetEnvironmentName()))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up the appropriate host (if any)
		var vCenterCache = dbSyncIndex.GetSyncObject(query.EnvironmentName)

		if vCenterCache == nil {
			// Fatal
			return nil, status.Errorf(codes.Internal, "Could not retrieve VCenter SyncHistory records for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.EnvironmentName)
		} else {
			if syncHistoryRecArray, err := vCenterCache.GetSyncHistoryRecordsRecent(query.GetNumRecords()); err != nil {
				// Fatal
				return nil, status.Errorf(codes.Internal, "Could not retrieve VCenter SyncHistory records for environment '%s' because of error: %v", query.GetEnvironmentName(), err)
			} else {
				// Success
				return syncHistoryRecArray, nil
			}
		}
	}
}

// Retrieves all child SyncLogEntry records for referenced parent SyncHistory record
//
func (s *server) GetVCenterSyncLogs(ctx context.Context, query *dora.SyncLogRecRequest) (*dora.SyncLogRecResponseArray, error) {
	// Verify authentication and authorization first
	logger.Log.Info(fmt.Sprintf("Received request for VCenter SyncLogEvent for environment: %s", query.GetEnvironmentName()))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up the appropriate host (if any)
		var vCenterCache = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

		if vCenterCache == nil {
			// Fatal
			return nil, status.Errorf(codes.Internal, "Could not retrieve VCenter SyncLogEntry records for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetEnvironmentName())
		} else {
			if syncLogRecArray, err := vCenterCache.GetSyncLogRecords(query.GetParentSyncHistoryId()); err != nil {
				// Fatal
				return nil, status.Errorf(codes.Internal, "Could not retrieve VCenter SyncLogEntry records for environment '%s' because of error: %v", query.GetEnvironmentName(), err)
			} else {
				// Success
				return syncLogRecArray, nil
			}
		}
	}
}

// Retrieves NfsSyncHistory records for viewing by clients
//
func (s *server) GetNfsSyncHistory(ctx context.Context, query *dora.SyncHistoryRequest) (*dora.NfsSyncHistoryRecArray, error) {
	// Verify authentication and authorization first
	logger.Log.Info(fmt.Sprintf("Received request for NFS SyncHistory for environment: %s", query.GetEnvironmentName()))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Get database connection
		var dbSync = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

		if dbSync == nil {
			// Fatal
			return nil, status.Errorf(codes.Canceled, fmt.Sprintf("No such DB file for environment '%s'", query.GetEnvironmentName()))
		} else {
			var connection = dbSync.GetConnectionPool().Get(ctx)

			// Always return connection to the pool at end of processing
			defer dbSync.GetConnectionPool().Put(connection)

			//
			// PROTOBUF CALL: Get records
			//
			if syncHistoryRecArray, err := nfs.GetNfsSyncHistoryRecordsRecent(ctx, connection, query.GetNumRecords()); err != nil {
				// Fatal
				return nil, status.Errorf(codes.Internal, "Could not retrieve NFS SyncHistory records for environment '%s' because of error: %v", query.GetEnvironmentName(), err)
			} else {
				// Success
				return syncHistoryRecArray, nil
			}
		}
	}
}

// Retrieves all child NfsSyncLogEntry records for referenced parent NfsSyncHistory record
//
func (s *server) GetNfsSyncLogs(ctx context.Context, query *dora.SyncLogRecRequest) (*dora.NfsSyncLogRecResponseArray, error) {
	// Verify authentication and authorization first
	logger.Log.Info(fmt.Sprintf("Received request for NFS SyncLogEvent for environment: %s", query.GetEnvironmentName()))

	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Get database connection
		var dbSync = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

		if dbSync == nil {
			// Fatal
			return nil, status.Errorf(codes.Canceled, fmt.Sprintf("No such DB file for environment '%s'", query.GetEnvironmentName()))
		} else {
			var connection = dbSync.GetConnectionPool().Get(ctx)

			// Always return connection to the pool at end of processing
			defer dbSync.GetConnectionPool().Put(connection)

			//
			// PROTOBUF CALL: Get records
			//
			if syncLogRecArray, err := nfs.GetNfsSyncLogRecords(ctx, connection, query.GetParentSyncHistoryId()); err != nil {
				// Fatal
				return nil, status.Errorf(codes.Internal, "Could not retrieve NFS SyncLogEntry records for environment '%s' because of error: %v", query.GetEnvironmentName(), err)
			} else {
				// Success
				return syncLogRecArray, nil
			}
		}
	}
}

// Retrieves the cron runtime information for the given VCenter referenced by the environment name
//
func (s *server) GetCronVCenterInfo(ctx context.Context, query *dora.CronVCenterRequest) (*dora.CronVCenterResponse, error) {
	// Look up the appropriate host (if any)
	var vCenterCache = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

	if vCenterCache == nil {
		// Fatal
		return nil, status.Errorf(codes.Internal, "Could not retrieve Cron runtime information for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetEnvironmentName())
	} else {
		var cronNextRunTimestamp = vCenterCache.CronNextScheduleRunTime()
		var cronExpression = vCenterCache.CronExpression()

		return &dora.CronVCenterResponse{
			NextRunTimeMs:  cronNextRunTimestamp.UnixNano() / 1000000, // convert time object to epoch milliseconds timestamp
			CronExpression: cronExpression,
			Enabled:        vCenterCache.CronGetEnabled(),
		}, nil
	}
}



// Retrieves the cron runtime information for the given environment referenced by the environment name
//
func (s *server) GetCronNfsInfo(ctx context.Context, query *dora.CronNfsRequest) (*dora.CronNfsResponse, error) {
	// Look up the appropriate host (if any)
	var cronSequence = nfs.GetNfsCronObject(query.GetEnvironmentName())

	if cronSequence == nil {
		// Fatal
		return nil, status.Errorf(codes.Internal, "Could not retrieve NFS Cron runtime information for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetEnvironmentName())
	} else {
		var cronNextRunTimestamp = cronSequence.CronNextScheduleRunTime()
		var cronExpression = cronSequence.CronExpression

		return &dora.CronNfsResponse{
			NextRunTimeMs:  cronNextRunTimestamp.UnixNano() / 1000000, // convert time object to epoch milliseconds timestamp
			CronExpression: cronExpression,
			Enabled:        cronSequence.IsCronEnabled(),
		}, nil
	}

	//return nil, nil
}

// Enables or disables the VCenter sync cron for the VCenter referenced by the environment name
//
func (s *server) SetCronVCenterEnabled(ctx context.Context, query *dora.CronEnabledRequest) (*dora.CronVCenterResponse, error) {
	// Verify authentication and authorization first
	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up the appropriate host (if any)
		var vCenterCache = dbSyncIndex.GetSyncObject(query.GetEnvironmentName())

		if vCenterCache == nil {
			// Fatal
			return nil, status.Errorf(codes.Internal, "Could not set Cron enabled/disabled for VCenter environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetEnvironmentName())
		} else {
			vCenterCache.CronSetEnabled(query.GetEnabled())

			var cronNextRunTimestamp = vCenterCache.CronNextScheduleRunTime()
			var cronExpression = vCenterCache.CronExpression()
			var cronEnabled = vCenterCache.CronGetEnabled()

			return &dora.CronVCenterResponse{
				NextRunTimeMs:  cronNextRunTimestamp.UnixNano() / 1000000, // convert time object to epoch milliseconds timestamp
				CronExpression: cronExpression,
				Enabled:        cronEnabled,
			}, nil
		}
	}
}

// Enables or disables the NFS sync cron for the NFS environment referenced by the environment name
//
func (s *server) SetCronNfsEnabled(ctx context.Context, query *dora.CronEnabledRequest) (*dora.CronNfsResponse, error) {
	// Verify authentication and authorization first
	if _, authError := auth.CheckToken(query.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Authentication rejected
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up the appropriate host (if any)
		var nfsSyncRec = nfs.GetNfsCronObject(query.GetEnvironmentName())

		if nfsSyncRec == nil {
			// Fatal
			return nil, status.Errorf(codes.Internal, "Could not set Cron enabled/disabled for NFS environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetEnvironmentName())
		} else {
			nfsSyncRec.SetCronEnabled(query.GetEnabled())

			var cronNextRunTimestamp = nfsSyncRec.CronNextScheduleRunTime()
			var cronExpression = nfsSyncRec.CronExpression
			var cronEnabled = nfsSyncRec.IsCronEnabled()

			return &dora.CronNfsResponse{
				NextRunTimeMs:  cronNextRunTimestamp.UnixNano() / 1000000, // convert time object to epoch milliseconds timestamp
				CronExpression: cronExpression,
				Enabled:        cronEnabled,
			}, nil
		}
	}
}

// Gets the hypervisor loadout for given environment. This must be performed once before any "balance" operation can
// be performed because the balance algorithm(s) use the hypervisor loadout as input.
//
// Example testing curl: curl -i "http://localhost:8000/v1/sqlite/balance/hvloadout/{auth_token}/NewPayTV/%5E%2FPAYTV-PSR%2Fhost%2F
// Need to get auth token first though.
//
func (s *server) GetHypervisorLoadOutList(ctx context.Context, query *dora.HypervisorLoadOutRequest) (*dora.HypervisorLoadOutList, error) {
	// Look up the matching VCenter cache object by name
	if vCenterCache := dbSyncIndex.GetSyncObject(query.GetVcenterName()); vCenterCache == nil {
		// Fatal
		return nil, status.Errorf(codes.InvalidArgument, "Could not retrieve Hypervisor loadout for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetVcenterName())
	} else {
		if hypervisorLoadOutList, errHypervisorLoadOut := vCenterCache.GenerateHypervisorLoadOut(query.GetPathRegexp()); errHypervisorLoadOut != nil {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, "Could not retrieve Hypervisor loadout for environment '%s' because of error: %v", query.GetVcenterName(), errHypervisorLoadOut)
		} else if util.IsBlank(query.GetRulesText()) {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, "Balance rules are required but are missing.  Specify rules text first or use defaults.")
		} else {
			if rules, errRules := balancing.ReadBalanceRules(query.GetRulesText()); errRules != nil {
				// Fatal - user provided rules invalid
				var message = fmt.Sprintf("Balance rules are invalid: %v", errRules)

				logger.Log.Error(errRules, message)

				return nil, status.Errorf(codes.InvalidArgument, message)
			} else {
				if errRules := balancing.DefineVMsForBalancing(hypervisorLoadOutList, nil, addressbook.GlobalAddressBook, rules, query.GetVcenterName()); errRules != nil {
					// Fatal
					return nil, errRules
				} else {
					// Successful load out retrieval
					//

					// Calculate and set the severity level of hypervisors on the "current" side according to the CPU &
					// MEM thresholds defined in the user rules parsed above.
					for _, hypervisorRec := range hypervisorLoadOutList.Load {
						// Calculate CPU Room severity by comparing against threshold percentage defined in user rule
						if float64(hypervisorRec.GetCurrentRoomCpu().RoomAvailable) < float64(hypervisorRec.GetCpu())*(1-rules.Rules.Cpu) {
							hypervisorRec.GetCurrentRoomCpu().Severity = constants.SEVERITY_CRITICAL
						} else {
							hypervisorRec.GetCurrentRoomCpu().Severity = constants.SEVERITY_OK
						}

						// Calculate MEM Room severity by comparing against threshold percentage defined in user rule
						if float64(hypervisorRec.GetCurrentRoomMemGb().RoomAvailable) < float64(hypervisorRec.GetMemGb())*(1-rules.Rules.Mem) {
							hypervisorRec.GetCurrentRoomMemGb().Severity = constants.SEVERITY_CRITICAL
						} else {
							hypervisorRec.GetCurrentRoomMemGb().Severity = constants.SEVERITY_OK
						}
					}

					// Create and attach a unique balance session ID to this result object
					hypervisorLoadOutList.BalanceSessionId = util.CreateBalanceSessionId(query.GetVcenterName())

					return hypervisorLoadOutList, nil
				}
			}
		}
	}
}
// Performs a balance operation on the given hypervisor list and returns an updated same list but with the
// "proposed" entries populated.
//
// NOTE: the GetHypervisorLoadOutList() is typically called prior to this to generate the initial hypervisor loadout
// which is the input to BalanceExec()
//
func (s *server) BalanceExec(ctx context.Context, query *dora.HypervisorLoadOutDTO) (*dora.HypervisorLoadOutDTO, error) {
	if rules, errRules := balancing.ReadBalanceRules(query.GetRulesText()); errRules != nil {
		// Fatal - user provided rules invalid
		var message = fmt.Sprintf("Balance rules are invalid: %v", errRules)

		logger.Log.Error(errRules, message)

		return nil, status.Errorf(codes.InvalidArgument, message)
	} else {
		// Set rack mapping data for this environment VCenter connection's addressbook value.  This rack mappping
		// is required for balancing.
		if vsphereConnection, addrErr := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(query.GetVcenterName()); addrErr != nil {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not find VCenter connection from given name \"%s\" because of error: %v", query.GetVcenterName(), addrErr))
		} else {
			rules.Definitions.RackMapping.Primary = strings.TrimSpace(vsphereConnection.RackPrimary)
			rules.Definitions.RackMapping.Secondary = strings.TrimSpace(vsphereConnection.RackSecondary)
			rules.Definitions.RackMapping.Quorum = strings.TrimSpace(vsphereConnection.RackQuorum)
			rules.InitRackLookup()
		}

		// Clear any existing errors/warnings as they will be re-populated as necessary during the rest of the balance
		// operation
		query.ErrorsWarnings = []*api.BalanceWarnErrorResponse{}

		// Look up the matching VCenter cache object by name
		if vCenterCache := dbSyncIndex.GetSyncObject(query.GetVcenterName()); vCenterCache == nil {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, "Could not retrieve Hypervisor loadout for environment '%s'.  No such environment found.  Check environment name.  Check environment has been loaded.", query.GetVcenterName())
		} else {
			// SANITY CHECK: Check if any hypervisor tags exist for the rules
			//
			if allTags, errTags := vCenterCache.GetAllHypervisorTags(); errTags != nil {
				// Fatal - DB lookup error
				return nil, status.Errorf(codes.Internal, fmt.Sprintf("Could not lookup hypervisor tags for VCenter '%s' because of error: %v", query.GetVcenterName(), errTags))
			} else {
				// All hypervisor tags specified in the rules for comparison with actual available tags below
				var vmTags = balancing.GetVmTags(rules)

				// Check that the balance "Order" value is within expected range
				var mapBalanceOrderCheck = make(map[string]int)
				mapBalanceOrderCheck["Sensitive"] = rules.Definitions.VmType.Sensitive[0].Order
				mapBalanceOrderCheck["HeavyWriter"] = rules.Definitions.VmType.HeavyWriters[0].Order
				mapBalanceOrderCheck["HeavyReader"] = rules.Definitions.VmType.HeavyReaders[0].Order
				for key, value := range mapBalanceOrderCheck {
					if value < 1 {
						query.ErrorsWarnings = append(
							query.ErrorsWarnings,
							&api.BalanceWarnErrorResponse{
								Type:    "ERROR",
								Message: fmt.Sprintf("The \"order\" value <b><code>%d</code></b> for <b>%s</b> vmType is invalid", value, key),
								Solutions: []string{"Specify an order value that is in the range [1, 2, 3]", "If \"order\" field not specified in YAML, then value defaults to 0.  Set an explicit value to avoid this problem.", "Refer to default rules YAML for example."},
							})
					}
				}

				// Sanity check vmType ordering for duplicate values
				var orderCount = count.CountStruct{}
				for _, vmType := range rules.Definitions.VmType.Sensitive {
					orderCount.AddCount(vmType.Order, "sensitive")
				}
				for _, vmType := range rules.Definitions.VmType.HeavyWriters {
					orderCount.AddCount(vmType.Order, "heavyWriters")
				}
				for _, vmType := range rules.Definitions.VmType.HeavyReaders {
					orderCount.AddCount(vmType.Order, "heavyReaders")
				}
				// Now check for duplicates
				for key, value := range orderCount.GetMap() {
					if len(value) > 1 {
						query.ErrorsWarnings = append(
							query.ErrorsWarnings,
							&api.BalanceWarnErrorResponse{
								Type:    "WARNING",
								Message: fmt.Sprintf("Specified vmTypes: [ %s ] have the same order value of <b>%d</b>.  This ordering is <b>ambiguous</b> and may produce unexpected balancing results.",
									strings.Join(stringarrayutil.Map(value, func(s string) string { return "\"<code><b>" + s + "</b></code>\"" }),", "), key),
								Solutions: []string{"Update each VmType to have its own unique order value (1, 2, 3)", "Refer to default rules YAML for example."},
							})
					}
				}

				// Sanity check if rules specify a hypervisorTag but no hypervisors are tagged with such a tag. This
				// incorrect scenario can have a serious effect on balancing the tagged VMs will end up all in the
				// "unplaced" bin.
				for _, vmTag := range vmTags {
					if !util.StringArrayContains(vmTag, allTags...) {
						// VM tag does not match any hypervisor tags currently in VCenter
						query.ErrorsWarnings = append(
							query.ErrorsWarnings,
							&api.BalanceWarnErrorResponse{
								 Type:    "WARNING",
								 Message: fmt.Sprintf("VM tag \"<b>%s</b>\" specified in YAML does <i>not</i> match any VCenter hypervisor tags. All VMs with this tag <b>cannot</b> be placed into any hypervisors!", vmTag),
								 Solutions: []string{"Check for typo in tag name in YAML rules", "Check that hypervisors are tagged correctly in VCenter", "Remove tag in YAML if its not applicable to the environment"},
							})
					}
				}

				// Sanity check CPU limit value
				if rules.Rules.Cpu < 0.0 || rules.Rules.Cpu > 1.0 {
					query.ErrorsWarnings = append(
						query.ErrorsWarnings,
						&api.BalanceWarnErrorResponse{
							Type:    "ERROR",
							Message: fmt.Sprintf("Specified <code>cpu</code> value %f is out of range", rules.Rules.Cpu),
							Solutions: []string{"Set value of <code>cpu</code> to between 0.0 and 1.0 only"},
						})
				}

				// Sanity check MEM limit
				if rules.Rules.Mem < 0.0 || rules.Rules.Mem > 1.0 {
					query.ErrorsWarnings = append(
						query.ErrorsWarnings,
						&api.BalanceWarnErrorResponse{
							Type:    "ERROR",
							Message: fmt.Sprintf("Specified <code>mem</code> value %f is out of range", rules.Rules.Mem),
							Solutions: []string{"Set value of <code>mem</code> to between 0.0 and 1.0 only"},
						})
				}

				//
				// Call balancing operation which mutates the DTO object as a result of balance operations
				//
				if errBalancing := balancing.Balance(rules, query, addressbook.GlobalAddressBook, balancing.SOURCE_FROM_CURRENT); errBalancing != nil {
					var message = fmt.Sprintf("Balancing operation produced an error: %v", errBalancing)

					return nil, status.Errorf(codes.InvalidArgument, message)
				} else {
					return query, nil
				}
			}
		}
	}
}

// Performs a balance "check" operation which is a validation of the current balance layout both "current" and
// "proposed" based on the given balance rules.  No balancing is performed only validations.
//
// NOTE: the GetHypervisorLoadOutList() is typically called prior to this to generate the initial hypervisor loadout
// which is the input to BalanceCheck()
func (s *server) BalanceCheck(ctx context.Context, query *dora.HypervisorLoadOutDTO) (*dora.HypervisorLoadOutDTO, error) {
	if rules, errRules := balancing.ReadBalanceRules(query.GetRulesText()); errRules != nil {
		// Fatal - user provided rules invalid
		var message = fmt.Sprintf("Balance rules are invalid and cannot be parsed: %v", errRules)

		logger.Log.Error(errRules, message)

		return nil, status.Errorf(codes.InvalidArgument, message)
	} else {
		// Set rack mapping data for this environment VCenter connection's addressbook value.  This rack mappping
		// is required for balancing.
		//
		if vsphereConnection, addrErr := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(query.GetVcenterName()); addrErr != nil {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not find VCenter connection from given name \"%s\" because of error: %v", query.GetVcenterName(), addrErr))
		} else {
			rules.Definitions.RackMapping.Primary = strings.TrimSpace(vsphereConnection.RackPrimary)
			rules.Definitions.RackMapping.Secondary = strings.TrimSpace(vsphereConnection.RackSecondary)
			rules.Definitions.RackMapping.Quorum = strings.TrimSpace(vsphereConnection.RackQuorum)
			rules.InitRackLookup()
		}

		if updatedHypervisorLoadOuts, errBalancing := balancing.BalanceCheckOnly(rules, query); errBalancing != nil {
			var message = fmt.Sprintf("Balance check operation produced an error: %v", errBalancing)

			return nil, status.Errorf(codes.InvalidArgument, message)
		} else {
			// Update with the newly returned list that now as the "proposed" balance list populated
			query.HypervisorLoadOuts = updatedHypervisorLoadOuts

			return query, nil
		}
	}
}

// Distributes balance unplaceable VMs with "relaxed" balance rules.  The rules text is expected to be
// updated by the client to reflect the relaxed balanced rules.
func (s *server) BalanceForceDistribute(ctx context.Context, dto *dora.HypervisorLoadOutDTO) (*dora.HypervisorLoadOutDTO, error) {
	if rules, errRules := balancing.ReadBalanceRules(dto.GetRulesText()); errRules != nil {
		// Fatal - user provided rules invalid
		var message = fmt.Sprintf("Balance rules are invalid and cannot be parsed: %v", errRules)

		logger.Log.Error(errRules, message)

		return nil, status.Errorf(codes.InvalidArgument, message)
	} else {
		// Set rack mapping data for this environment VCenter connection's addressbook value.  This rack mappping
		// is required for balancing.
		//
		if vsphereConnection, addrErr := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(dto.GetVcenterName()); addrErr != nil {
			// Fatal
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not find VCenter connection from given name \"%s\" because of error: %v", dto.GetVcenterName(), addrErr))
		} else {
			rules.Definitions.RackMapping.Primary = strings.TrimSpace(vsphereConnection.RackPrimary)
			rules.Definitions.RackMapping.Secondary = strings.TrimSpace(vsphereConnection.RackSecondary)
			rules.Definitions.RackMapping.Quorum = strings.TrimSpace(vsphereConnection.RackQuorum)
			rules.InitRackLookup()
		}

		logger.Log.Info(fmt.Sprintf("YAML balance rules: %v", rules))
		if errBalancing := balancing.Balance(rules, dto, addressbook.GlobalAddressBook, balancing.SOURCE_FROM_UNPLACED); errBalancing != nil {
			var message = fmt.Sprintf("Balancing operation produced an error: %v", errBalancing)

			return nil, status.Errorf(codes.InvalidArgument, message)
		} else {
			return dto, nil
		}

		return dto, nil
	}
}

// Execute a single move of a referenced Virtual Machine object to the destination hypervisor referenced by full path
//
// NOTES:
// (1) Execution occurs on master VCenter server so expect slow performance
//   (1a) SQLite database *will* be updated upon successful VM move so client and refresh view from SQLite immediately
//        to get most recent VM layout view
// (2) Errors can be returned at any point and they must be shown to the user to take corrective action
// (3) No retry logic implemented here -- client code must implement any retry logic
// (4) No VM shutdown logic -- client code should handle this with call to the VM shutdown API (grpc) as necessary
//
func (s *server) VmMove(ctx context.Context, request *dora.VmMoveRequest) (*dora.VmMoveResponse, error) {
	var response = dora.VmMoveResponse{}
	var startTime = util.CurrentTimeMillis()

	// Verify authentication and authorization first
	if _, authError := auth.CheckToken(request.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Not authorized or not authenticated for this function
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up referenced VCenter
		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(request.GetVcenterName()); errAddressBook != nil {
			// Fatal - no such VCenter found by given name
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: No such VCenter found by given name \"%s\"", request.GetVcenterName()));
		} else {
			// Login to VCenter
			// NOTE: Login itself can take 2 to 3 seconds alone and would benefit from caching since this is will be
			//       a repetitive operation if user is moving multiple VMs fom the client UI
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vsphereConnection); errLogin != nil {
				// Fatal - login failed (incorrect credentials, network route down, etc)
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not log into VCenter \"%s\" because of error: %v", request.GetVcenterName(), errLogin));
			} else {
				// Look up the VCenter VM object
				if vm, errLookup := vcSoapConnection.Finder.VirtualMachine(ctx, request.GetVmPath()); errLookup != nil {
					// Fatal - VirtualMachine vcenter object could not be found - user might have incorrect path
					// to the VM
					return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not find Virtual Machine at path \"%s\" in VCenter \"%s\" because of error: %v", request.GetVmPath(), request.GetVcenterName(), errLogin));
				} else {
					// Get the matching SQLite instance which allow DB operations below on the proper environment
					var vCenterCache = dbSyncIndex.GetSyncObject(request.GetVcenterName())

					//
					// Call API to make actual VM move here
					// NOTE: audit logs are created accordingly to success/failure
					//
					if warnings, errMove := vcentermove.ExecuteVmMove(vcSoapConnection, vm, request.HypervisorDestPath); errMove != nil {
						// Fatal - move error
						var errMessage = fmt.Sprintf("VM Move Failed: Unexpected VCenter VM move error: %v", errMove)

						// Save a "failed" audit log
						vCenterCache.InsertLogVmMoveWithLog(ctx, vcenter.VmMoveAuditLogStruct{
							Username:          auth.GetUsername(request.GetAuthToken()),
							VCenter:           request.GetVcenterName(),
							SessionUuid:       request.GetBalanceSessionUuid(),
							DurationSec:       float64(util.CurrentTimeMillis() - startTime) / 1000,
							VmPath:            request.GetVmPath(),
							SourceHvPath:      request.GetHypervisorSrcPath(),
							DestinationHvPath: request.GetHypervisorDestPath(),
							Status:            errMessage,
							Extra:             "auth token: " + request.GetAuthToken(),
						})

						return nil, status.Errorf(codes.Internal, errMessage);
					} else {
						// Move complete -- just attach any warnings to the response object back to the user
						response.Warnings = warnings

						// Save a "success" audit log for this VM
						vCenterCache.InsertLogVmMoveWithLog(ctx, vcenter.VmMoveAuditLogStruct{
							Username:          auth.GetUsername(request.GetAuthToken()),
							VCenter:           request.GetVcenterName(),
							SessionUuid:       request.GetBalanceSessionUuid(),
							DurationSec:       float64(util.CurrentTimeMillis() - startTime) / 1000,
							VmPath:            request.GetVmPath(),
							SourceHvPath:      request.GetHypervisorSrcPath(),
							DestinationHvPath: request.GetHypervisorDestPath(),
							Status:            "OK",
							Extra:             "auth token: " + request.GetAuthToken(),
						})

						// Update the local SQLite cache to reflect the VM move so clients can immediately refresh
						// the VM layout and get the correct updated view of it
						if vCenterCache != nil {
							if errSqlite := vCenterCache.UpdateVmParentHypervisor(request.GetVmPath(), request.GetHypervisorDestPath()); errSqlite != nil {
								// Unlikely but need to warn user of possible inconsistent state between SQLite and
								// VCenter
								return nil, status.Errorf(codes.Internal, fmt.Sprintf("VM Move SQLite cached Update error: Unexpected SQLite error: %v", errSqlite));
							};
						} else {
							// Unlikely but cannot update SQLite cache because cache was not found
							return nil, status.Errorf(codes.Internal, fmt.Sprintf("Unable to find SQLite cache: '%s'.  Check Dora server configuration is correct.", request.GetVcenterName()));
						}
					}
				}
			}
		}
	}

	// Successful move if this point is reached
	return &response, nil
}

// Powers off the referenced VM directly in VCenter (restricted to Dora Administrator users only)
//
func (s *server) VmPowerOff(ctx context.Context, request *dora.VmPowerChangeRequest) (*dora.VmPowerChangeResponse, error) {
	var response = dora.VmPowerChangeResponse{}

	// Verify authentication and authorization first
	if _, authError := auth.CheckToken(request.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Not authorized or not authenticated for this function
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up referenced VCenter
		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(request.GetVcenterName()); errAddressBook != nil {
			// Fatal - no such VCenter found by given name
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: No such VCenter found by given name \"%s\"", request.GetVcenterName()));
		} else {
			// Login to VCenter
			// TODO: Login itself can take 2 to 3 seconds alone.  So consider caching the vcSoapConnection object in
			//  Go memory in some kind of timed expiring cache -- Redis is *not* a viable cache in this case because
			//  the connection object cannot be serialized.
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vsphereConnection); errLogin != nil {
				// Fatal - login failed (incorrect credentials, network route down, etc)
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not log into VCenter \"%s\" because of error: %v", request.GetVcenterName(), errLogin));
			} else {
				// Look up the VCenter VM object
				if vm, errLookup := vcSoapConnection.Finder.VirtualMachine(ctx, request.GetVmPath()); errLookup != nil {
					// Fatal - VirtualMachine vcenter object could not be found - user might have incorrect path to the VM
					return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not find Virtual Machine at path \"%s\" in VCenter \"%s\" because of error: %v", request.GetVmPath(), request.GetVcenterName(), errLogin));
				} else {
					logger.Log.Info(fmt.Sprintf("Attempting to power off VM %s", request.GetVmPath()));
					if taskPowerOff, errPowerOff := vm.PowerOff(vcSoapConnection.Ctx); errPowerOff != nil {
						// Fatal
						return nil, status.Errorf(codes.Internal, fmt.Sprintf("Could not power off VM %s because of error: %v", request.GetVmPath(), errPowerOff));
					} else {
						// Power off successfully initiated -- need to wait for successful shutdown confirmation
						if errConfirmation := taskPowerOff.Wait(vcSoapConnection.Ctx); errConfirmation != nil {
							// Power off confirmation *not* confirmed because error
							return nil, status.Errorf(codes.Internal, fmt.Sprintf("Shutdown of VM %s initiated but could not confirm (requires manual confirmation) because of error: %v", request.GetVmPath(), errConfirmation))
						}
					}
				}
			}
		}
	}

	return &response, nil
}

// Powers on the referenced VM directly in VCenter (restricted to Dora Administrator users only)
//
func (s *server) VmPowerOn(ctx context.Context, request *dora.VmPowerChangeRequest) (*dora.VmPowerChangeResponse, error) {
	var response = dora.VmPowerChangeResponse{}

	// Verify authentication and authorization first
	if _, authError := auth.CheckToken(request.AuthToken, constants.DORA_ADMIN_GROUP); authError != nil {
		//
		// Not authorized or not authenticated for this function
		//
		return nil, status.Errorf(codes.Unauthenticated, "Authentication failed: %v", authError)
	} else {
		// Look up referenced VCenter
		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(request.GetVcenterName()); errAddressBook != nil {
			// Fatal - no such VCenter found by given name
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: No such VCenter found by given name \"%s\"", request.GetVcenterName()));
		} else {
			// Login to VCenter
			// TODO: Login itself can take 2 to 3 seconds alone.  So consider caching the vcSoapConnection object in
			//  Go memory in some kind of timed expiring cache -- Redis is *not* a viable cache in this case because
			//  the connection object cannot be serialized.
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vsphereConnection); errLogin != nil {
				// Fatal - login failed (incorrect credentials, network route down, etc)
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not log into VCenter \"%s\" because of error: %v", request.GetVcenterName(), errLogin));
			} else {
				// Look up the VCenter VM object
				if vm, errLookup := vcSoapConnection.Finder.VirtualMachine(ctx, request.GetVmPath()); errLookup != nil {
					// Fatal - VirtualMachine vcenter object could not be found - user might have incorrect path to the VM
					return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("VM Move Failed: Could not find Virtual Machine at path \"%s\" in VCenter \"%s\" because of error: %v", request.GetVmPath(), request.GetVcenterName(), errLogin));
				} else {
					logger.Log.Info(fmt.Sprintf("Attempting to power on VM %s", request.GetVmPath()));
					if taskPowerOn, errPowerOn := vm.PowerOn(vcSoapConnection.Ctx); errPowerOn != nil {
						// Fatal
						return nil, status.Errorf(codes.Internal, fmt.Sprintf("Could not power on VM %s because of error: %v", request.GetVmPath(), errPowerOn));
					} else {
						// Power off successfully initiated -- need to wait for successful power on confirmation
						if errConfirmation := taskPowerOn.Wait(vcSoapConnection.Ctx); errConfirmation != nil {
							// Power on confirmation *not* confirmed because error
							return nil, status.Errorf(codes.Internal, fmt.Sprintf("Power on of VM %s initiated but could not confirm (requires manual confirmation) because of error: %v", request.GetVmPath(), errConfirmation))
						}
					}
				}
			}
		}
	}

	return &response, nil
}

// Get the latest VM move info for the given VCenter.  This allows clients to check if their balance session is still
// valid or invalidated by another balance session on the same VCenter has made one or more VM moves.
//
func (s *server) BalanceInvalidCheck(ctx context.Context, request *dora.BalanceInvalidCheckRequest) (*dora.BalanceInvalidCheckResponse, error) {
	// Look up the appropriate vCenter connection
	var vCenterCache = dbSyncIndex.GetSyncObject(request.GetVcenterName())

	if vCenterCache != nil {
		// Set the last lookup time from SQL query
		if balanceLockCheckResponse, err := vCenterCache.GetLastVmMoveInfo(); err != nil {
			// Fatal - unexpected error possibly DB or network related
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Lookup failed due to unexpected error: %v", err));
		} else {
			// Successful lookup. NOTE: The timestamp can still be -1 if the database table doesn't have a record of a
			// last VM move.
			return balanceLockCheckResponse, nil
		}
	} else {
		// Fatal
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("No such VCenter: %s", request.GetVcenterName()));
	}
}

// Allows client to obtain a lock for balance operations on the given VCenter.  If lock is already owned by another
// session then an Aborted gRPC error object is returned instead with information about the other session.
//
// RETURN  unlockcount = 1 if actual unlock occurs and 0 if the user didn't have any lock to begin with
func (s *server) BalanceLock(ctx context.Context, request *dora.BalanceLockRequest) (*dora.BalanceLockResponse, error) {
	// Acquire a lock for the given VCenter environment and process rejection if lock is not owned by current session
	if lockRec, isOwner := vcentermove.LockVCenterForMove(request.GetVcenterName(), request.GetUsername(), request.GetSessionUuid()); !isOwner {
		// ABORT: Another session is currently moving VM's in this VCenter
		return nil, status.Error(
			codes.Aborted,
			fmt.Sprintf("Lock held by another session. (user = %s, session_id = %s)", request.GetUsername(), request.GetSessionUuid()))
	} else {
		// SUCCESSFUL LOCK.  Client can proceed with balance operations.
		return &dora.BalanceLockResponse {
			TimestampMs: lockRec.Date.UnixNano() / 1000000,
			SessionUuid: request.GetSessionUuid(),
			Username:    request.GetUsername(),
			VcenterName: request.GetVcenterName(),
		}, nil
	}
}

// Allows client to unlock a previous balance lock
//
func (s *server) BalanceUnlock(ctx context.Context, request *dora.BalanceUnlockRequest) (*dora.BalanceUnlockResponse, error) {
	var unlockCount = vcentermove.UnlockVCenterForMove(request.GetVcenterName(), request.GetSessionUuid(), 1000)

	return &dora.BalanceUnlockResponse {
		UnlockCount: int64(unlockCount),
	}, nil
}

func GenerateRoomReportSQLite(ctx context.Context, vcCache *vcenter.VCenterSyncRec, query *dora.QueryRequest, stream dora.Dora_GetRoomStreamedServer) error {
	// Results will asynchronously populated
	//
	var channelResults = make(chan *dora.RoomResponse, 64)

	// Get room report results.  Asynchronous with results to be provided in the channel.
	//
	vcCache.GenerateRoomReport(query.SearchPattern, channelResults)

	var roomReport = dora.RoomResponseArray{}

	// Send results back to user as they arrive from the report
	for {
		var roomRecord = <-channelResults

		if roomRecord == nil {
			// Report generation completed
			break
		} else {
			// Send the new record immediately back to the frontend/user
			stream.Send(roomRecord)

			// Accumulate to a list which is then cached to Redis but ignore UI control and UI Info metadata records
			if !roomRecord.UiControlIsPreview &&
				len(roomRecord.UiMsgInfo) == 0 &&
				len(roomRecord.UiMsgWarning) == 0 &&
				len(roomRecord.UiMsgError) == 0 {
				roomReport.Items = append(roomReport.Items, roomRecord)
			}
		}
	}

	// Save to the Redis cache so later lookups on same query will return the result instantly (up until
	// the cache record expires via TTL though)
	var redisKeyBaseValue = redisutil.CreateKeyBaseValue(query.ConnectionName, query.SearchPattern) // this must be the exact key values used by lookup later
	if protoMarshaledData, errProto := proto.Marshal(&roomReport); errProto != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Proto marshalling failed for caching because of error: %v", errProto))
	} else {
		if errRedis := redisConnect.SaveToRedisCache(ctx, redisutil.COMMAND_ROOM_REPORT, redisKeyBaseValue, protoMarshaledData); errRedis != nil {
			if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
				// Not a fatal error but log it as it represents system instability
				logger.Log.Info(errRedis.Error())
			} else {
				// Actual REDIS error that needs to be displayed
				logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyBaseValue, errRedis))
			}
		}
	}

	return nil
}

func main() {
	// Read in runtime configuration file
	if properties, errConfigRead := cfg.Config.ReadConfig(true); errConfigRead != nil {
		log.Fatalf("Unable to read configuration file due to fatal error: %v", errConfigRead)
	} else {
		// Assign to global
		cfg.Config = properties

		logger.Log.Info(fmt.Sprintf("Successfully read configuration file '%s' data: %v", cfg.Config.Path, cfg.Config))
	}

	// Load the JWT key from config now that its available.  This will either be the default or an overrided value
	// if one is specified in the config file.
	//
	// TROUBLESHOOTING:
	// This key must load correctly or auth against ALL existing tokens may break with "signature is invalid" runtime
	// errors.
	//cfg.Config.JwtKey = []byte(cfg.Config.GetJwKeyPhrase())
	if errAddressBook := addressbook.InitDefaultAddressBook(""); errAddressBook != nil {
		// Fatal - abort server startup by exitting
		logger.Log.Error(errAddressBook, "Could not initialize the default address book")
		os.Exit(0)
	}

	// Bring up SQLite databases
	//
	if errSqlLiteStart := startSQLite(); errSqlLiteStart != nil {
		// Fatal
		logger.Log.Error(errSqlLiteStart, fmt.Sprintf("Could not start SQLite cache instances because of error: %v", errSqlLiteStart))
		os.Exit(0)
	}

	// Connect to Redis for Caching and Queueing functions.
	//
	// NOTE: This uses a retry delay loop because this could be started in a Docker container and the startup of
	// Redis is not determinate so Redis may not be started up in time which would otherwise cause an error here
	// without retry logic.
	//
	const REDIS_CONNECT_RETRIES = 8
	const REDIS_CONNECT_WAIT_SECS = 5
	for attempt := 1; attempt <= REDIS_CONNECT_RETRIES; attempt++ {
		if redis, errRedis := redisutil.New(cfg.Config.GetRedisUrl(), "", ""); errRedis != nil {
			if attempt >= REDIS_CONNECT_RETRIES {
				// Fatal
				logger.Log.Error(errRedis, fmt.Sprintf("Could not connect to Redis server at '%s' after %d retries because of error: %v", cfg.Config.GetRedisUrl(), attempt, errRedis))
				os.Exit(1)
			} else {
				// Retries remaining
				logger.Log.Info(fmt.Sprintf("Attempt %d out of %d.  Redis connection to URL '%s' failed.  Will wait %d sec before retying.  Error was: %v", attempt, REDIS_CONNECT_RETRIES, cfg.Config.GetRedisUrl(), REDIS_CONNECT_WAIT_SECS, errRedis))

				// Wait a few seconds before restarting the loop
				time.Sleep(REDIS_CONNECT_WAIT_SECS * time.Second)
				continue
			}
		} else {
			// Connected successfully so save Redis connection reference to global
			redisConnect = redis

			logger.Log.Info(fmt.Sprintf("Successfully connected to Redis URL: %s", cfg.Config.GetRedisUrl()))

			break
		}
	}

	lis, err := net.Listen("tcp", ":"+fmt.Sprintf("%d", cfg.Config.GetServerPort()))
	if err != nil {
		logger.Log.Error(err, "failed to listen: %v")
	}

	//
	// Start AppDash HTTP server UI on its own TCP port
	//
	go func() {
		var appDashError = startAppDash(int(cfg.Config.GetAppDashCollectorPort()), int(cfg.Config.GetAppDashHttpPort()))

		if appDashError != nil {
			logger.Log.Error(appDashError, "Error starting AppDash server")
		} else {
			logger.Log.Info("AppDash server started normally")
		}
	}()

	//
	// Start the gRPC server itself
	//
	//creds, err := credentials.NewServerTLSFromFile("pkg/service.pem", "pkg/service.key")
	//if err != nil {
	//	log.Fatalf("Failed to setup TLS: %v", err)
	//}
	//var grpcServer = grpc.NewServer(grpc.Creds(creds))
	var grpcServer = grpc.NewServer()
	logger.Log.Info(fmt.Sprintf("Starting gRPC server on port %d...", cfg.Config.GetServerPort()), "port", cfg.Config.GetServerPort())
	dora.RegisterDoraServer(grpcServer, &server{})
	if errServer := grpcServer.Serve(lis); errServer != nil {
		log.Fatalf("failed to serve: %v", errServer)
	}
}

// Stores all SQLite instances ready for querying
var dbSyncIndex = dbcache.NewDbIndex()

// Starts SQLite instances for all VCenter connections described in the AddressBook.  All created instances are
// stored in a global index object.
//
// NOTE: This function runs asynchronously and results may not be available right away
//
func startSQLite() error {
	var retries = 3

	// Ensure SQLite data folder exists and is writeable.  If not, throw an error which will stop server startup.
	//
	if err := vcenter.EnsureSQLiteFolderExists(); err != nil {
		// FATAL.  Likely the folder doesn't exist and app process has no permission to create it or the directory
		// exists but is not writeable.  Either way, need to abort server startup!  A descriptive error message to the
		// administrator will allow them to correct the problem first before restarting the Dora server.
		return err
	}

	// Get all available VCenter connection records from the AddressBook and push into a thread safe queue
	//
	var logins = threadsafequeue.New(nil)
	for _, vcConnectionName := range addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames() {
		if vcLoginInfo, errAddressBookLookup := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vcConnectionName); errAddressBookLookup != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not look up VCenter connection name '%s' because of error: %v", vcConnectionName, errAddressBookLookup))
		} else {
			logins.Add(vcLoginInfo)
		}
	}

	// Create the SQLite synchronization object for each VCenter record. These connections are performed in back-
	// ground parallel go-routines.
	//
	var mutex = sync.Mutex{}
outer:
	for {
		switch vcenterConnectionRec := logins.Pull().(type) {
		case *addressbook.VSphereConnectionStruct:
			// Initialize each connection in parallel as they are I/O intensive and will slow server startup
			//
			go func(vcLoginInfo *addressbook.VSphereConnectionStruct) {
				for attemptCount := 1; attemptCount <= retries; attemptCount++ {
					if vCenterSync, errSyncCreate := vcenter.New(vcLoginInfo); errSyncCreate != nil {
						if attemptCount == retries {
							// Not fatal but represents major system instability
							var errMsg = fmt.Sprintf("Could not create SQLite DB instance (even after %d retries) for VCenter connection name '%s' -- fast SQLite lookups will *not* be available for this environment.  Error was: %v", retries, vcenterConnectionRec.Name, errSyncCreate)

							logger.Log.Info(errMsg)

							dbSyncIndex.AddError(errMsg)
						} else {
							if attemptCount > 1 {
								logger.Log.Info(fmt.Sprintf("Could not connect to VCenter instance '%s'.  Will retry %d more times...", vcenterConnectionRec.Name, retries-attemptCount))
							}

							// Sleep a short time before retrying just in case problem can clear on its own with time
							// instead of using up retries attempt too quickly
							time.Sleep(2 * time.Second)
						}
					} else {
						// Short but important mutex lock so only one go routine inserts into the map at once
						mutex.Lock()

						// Start the sync crons based on configuration boolean
						// NOTE: Typically, DEV environments should be cron disabled while PROD environment should have cron
						// autostart enabled.
						vCenterSync.CronSetEnabled(cfg.Config.GetCronAutoStart())

						// Save object to the map for lookup later at runtime during user searches
						dbSyncIndex.AddSyncObject(vcenterConnectionRec.Name, vCenterSync)

						var vcenterNextRunTime string
						if vCenterSync.CronNextScheduleRunTime().Unix() > 0 {
							vcenterNextRunTime = vCenterSync.CronNextScheduleRunTime().String()
						} else {
							vcenterNextRunTime = "** Cron disabled or not scheduled yet **"
						}

						logger.Log.Info(fmt.Sprintf("VCenter cron for environment '%s' next run at: %v", vcenterConnectionRec.Name, vcenterNextRunTime))

						// Setup NFS data sync schedules from the address book entries per environment
						//
						// NOTE: NFS data sync runs separately from VCenter sync and with its own GUI as of Dora 0.9.10
						//
						if vSphereConnection, errVsphereLookup := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vCenterSync.GetName()); errVsphereLookup != nil {
							// Not fatal & unlikely since the connection name comes from the addressbook
							logger.Log.Info(fmt.Sprintf("WARN: Cannot setup NFS cron sync for environment '%s' because of address book record lookup error: %v", vCenterSync.GetName(), errVsphereLookup))
						} else {
							if cronRecord, errSyncSetup := nfs.PutSyncObject(vCenterSync.GetName(), vSphereConnection.CronSyncNfs, vCenterSync, cfg.Config.GetCronAutoStart()); errSyncSetup != nil {
								logger.Log.Error(errSyncSetup, fmt.Sprintf("Could not create cron for NFS environment '%s' because of error: %v", vCenterSync.GetName(), errSyncSetup))
							} else {
								if cronRecord.IsCronEnabled() {
									logger.Log.Info(fmt.Sprintf("NFS cron for environment '%s' next run at: %v", vCenterSync.GetName(), cronRecord.CronNextScheduleRunTime()))
								} else {
									logger.Log.Info(fmt.Sprintf("NFS cron for environment '%s' next run at: ** Cron disabled or not scheduled yet **", vCenterSync.GetName()))
								}
							}
						}

						mutex.Unlock()

						// Done. Don't retry.
						break
					}
				}
			}(vcenterConnectionRec)
		case threadsafequeue.EmptyMarker:
			// All VCenter objects sent for connection processing.  Done.
			break outer
		}
	}

	// No errors if this point is reached
	return nil
}

// Starts both the AppDash Collector and the AppDash Web Admin UI servers
//
// Ref: https://medium.com/opentracing/distributed-tracing-in-10-minutes-51b378ee40f1
func startAppDash(collectorTcpPort int, adminTcpPort int) error {
	var store = appdash.NewMemoryStore()

	// Listen on any available TCP port locally.
	l, errListenTcp := net.ListenTCP("tcp", &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: collectorTcpPort})
	if errListenTcp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not start AppDash server on port %d because of error: %v", collectorTcpPort, errListenTcp))
	}
	var collectorTcpAddr = l.Addr().(*net.TCPAddr)

	// Start an Appdash collection server that will listen for spans and
	// annotations and add them to the local collector (stored in-memory).
	var collectorServer = appdash.NewServer(l, appdash.NewLocalCollector(store))
	go collectorServer.Start()

	// Print the URL at which the web UI will be running.
	var appdashURLStr = fmt.Sprintf("http://localhost:%d", adminTcpPort)
	appdashURL, errParse := url.Parse(appdashURLStr)
	if errParse != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Error parsing %s: %s", appdashURLStr, errParse))
	}
	logger.Log.Info(fmt.Sprintf("To see your traces, go to %s/traces", appdashURL))

	// Start the web UI in a separate goroutine.
	tapp, errListenTcp := traceapp.New(nil, appdashURL)
	if errListenTcp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not start AppDash HTTP UI on host %s because of error: %v", appdashURL, errListenTcp))
	}
	tapp.Store = store
	tapp.Queryer = store

	// Run AppDash HTTP UI in background
	go func() {
		// Thread starts server and does not exit unless server crashes
		var httpServerError = http.ListenAndServe(fmt.Sprintf(":%d", adminTcpPort), tapp)

		if httpServerError != nil {
			logger.Log.Error(httpServerError, "AppDash server crashed out with error.")
		}
	}()

	var tracer = appdash_opentracing.NewTracer(appdash.NewRemoteCollector(collectorTcpAddr.String()))
	opentracing.InitGlobalTracer(tracer)

	// Start the background Redis worker threads
	//
	if errWorkerThreads := startWorkerThreads(); errWorkerThreads != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not start worker threads because of error: %v", errWorkerThreads))
	}

	// Successful startup
	return nil
}

func (s *server) Ready(req context.Context, in *empty.Empty) (*dora.ReadyResponse, error) {
	// TODO: Add logic to determine actual ready state
	return &dora.ReadyResponse{ReadyState: "LIVE"}, nil
}

func (s *server) Echo(req context.Context, in *dora.EchoStruct) (*dora.EchoStruct, error) {
	return &dora.EchoStruct{Message: in.Message}, nil
}

func (s *server) GetAppEnvironmentLabel(ctx context.Context, in *empty.Empty) (*dora.EnvironmentLabelResponse, error) {
	return &dora.EnvironmentLabelResponse{
		EnvironmentLabel: cfg.Config.GetEnvironmentLabel(),
	}, nil
}

func (s *server) GetVsphereEnvironments(req context.Context, in *empty.Empty) (*dora.EnvironmentNameResponseArray, error) {
	// Decrypt address book and get just the connection names
	var availableVSphereConnections = addressbook.GlobalAddressBook.GetAvailableVSphereConnectionNames()

	// This will be the grpc response
	var grpcResponse = dora.EnvironmentNameResponseArray{}

	// Populate the grpc response object with the VSphere names
	for _, vsphereConnection := range availableVSphereConnections {
		grpcResponse.Items = append(grpcResponse.Items, &dora.EnvironmentNameResponse{EnvironmentName: vsphereConnection})
	}

	// No errors, return response with populated environments
	return &grpcResponse, nil
}

func (s *server) GetForemanEnvironments(req context.Context, in *empty.Empty) (*dora.EnvironmentNameResponseArray, error) {
	// Decrypt address book and get just the connection names
	var availableForemanConnections = addressbook.GlobalAddressBook.GetAvailableForemanConnectionNames()

	// This will be the grpc response
	var grpcResponse = dora.EnvironmentNameResponseArray{}

	// Populate the grpc response object with the Foreman names
	for _, foremanConnection := range availableForemanConnections {
		grpcResponse.Items = append(grpcResponse.Items, &dora.EnvironmentNameResponse{EnvironmentName: foremanConnection})
	}

	// No errors, return response with populated environments
	return &grpcResponse, nil
}

func (s *server) GetMetricsReport(ctx context.Context, in *empty.Empty) (*dora.MetricsReportResponse, error) {
	var response = dora.MetricsReportResponse{
		TimestampMs: time.Now().UnixNano() / time.Millisecond.Nanoseconds(),
		TotalLogins: int64(len(auth.LoginSessionsDirectory.GetAllLogins())),
		ActiveLogins: int64(len(auth.LoginSessionsDirectory.GetActiveLogins())),
	}

	return &response, nil
}

// Starts the worker threads that will asynchronously process commands on the Redis command queue as command
// are entered from users via the gRPC endpoints.
//
// ** THIS METHOD SHOULD ONLY BE CALLED ONCE AND USUALLY AT START OF THE APPLICATION **
//
func startWorkerThreads() error {
	var runGroup = run.Group{}
	var redisWorkQueueThreads = int(cfg.Config.GetRedisWorkQueueThreads())

	logger.Log.Info(fmt.Sprintf("Starting %d Redis worker threads...", redisWorkQueueThreads))
	for i := 0; i < redisWorkQueueThreads; i++ {
		runGroup.Add(func() error {
			for {
				if cmd, errCmd := redisConnect.WaitForCommand(); errCmd != nil {
					logger.Log.Error(errCmd, fmt.Sprintf("Redis waiting error.  Will try again.  Error = %v", errCmd))
				} else {
					//
					// Got a valid command.  Now take action based on its command action string.
					//
					switch cmd.Command {
					case redisutil.COMMAND_SEARCH_HYPERVISOR:
						if err := processHypervisorSearch(cmd); err != nil {
							return err
						}
					case redisutil.COMMAND_SEARCH_VIRTUALMACHINE:
						if err := processVirtualMachineSearch(cmd); err != nil {
							return err
						}
					}
				}
			}
		}, func(err error) {
			logger.Log.Error(err, "Worker thread quitting because it encountered error during processing: %v", err)
		})
	}

	// Start all the threads in the background
	if errRun := runGroup.Run(); errRun != nil {
		// Some thread start error occurred
		return errRun
	} else {
		// Successful start of background threads.  No error.
		return nil
	}
}

// This method is called by worker thread with the command to start a search for VCenter Hypervisors records
//
// Steps:
// (1) Deserialize command object from JSON
// (2) Login to VCenter
// (3) Start AppDash span from parent span (serialized as part of the command record)
// (4) Perform VCenter search and push results to Redis response queue as results are returned "live" (streaming mode)
// (5) Save results to Redis cache
//
func processHypervisorSearch(cmd *redisutil.RedisQueueCommand) error {
	// Data for this command.  Will be populated with JSON unmarshal below from the command data.
	var workQueueCmd = workqueue.VCenterSearchCmd{}

	// Deserialize search command to get the search parameters as requested by the user
	if errJson := json.Unmarshal([]byte(cmd.Data), &workQueueCmd); errJson != nil {
		// Fatal - could not deserialize the hypervisor list command's data
		return errors.New(fmt.Sprintf("Unable to JSON unmarshal the hypervisor search command record because of error: %v", errJson))
	} else {
		// TODO: This ctx needs to be some serialized version that links to the parent AppDash span
		var ctx = context.Background()

		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(workQueueCmd.ConnectionName); errAddressBook != nil {
			// Fatal for the stream - use response wrapper set to "error" with a message the error message
			// which will cause the client UI to display the message and gracefully quit streaming
			var response = workqueue.QueueResponseWrapper{
				StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
				Data:       fmt.Sprintf("Could not look up connection '%s' in address book because of error: %v", workQueueCmd.ConnectionName, errAddressBook),
			}
			if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
				logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
			}
		} else {
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vsphereConnection); errLogin != nil {
				// Fatal for the stream - use response wrapper set to "error" with a message the error message
				// which will cause the client UI to display the message and gracefully quit streaming
				var response = workqueue.QueueResponseWrapper{
					StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
					Data:       fmt.Sprintf("Could not log into VCenter '%s'.  Check that your connection name is correct.  Error: %v", workQueueCmd.ConnectionName, errLogin),
				}
				if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
					logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
				}
			} else {
				if outputChannel, errVCenter := vcenter.ListHypervisorRecords(ctx, workQueueCmd.PathRegExp, workQueueCmd.HostRegExp, workQueueCmd.ListOnly, true, vcSoapConnection); errVCenter != nil {
					// Fatal for the stream - use response wrapper set to "error" with a message the error message
					// which will cause the client UI to display the message and gracefully quit streaming
					var response = workqueue.QueueResponseWrapper{
						StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
						Data:       fmt.Sprintf("Failed to execute Hypervisor search on pattern '%s' because of error: %v", workQueueCmd.PathRegExp, errVCenter),
					}
					if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
						logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
					}
				} else {
					// Redis key used for cache lookup/retrieval
					var redisKeyFragment = redisutil.GenerateHvSearchKeyFragment(workQueueCmd.ConnectionName, workQueueCmd.PathRegExp, workQueueCmd.HostRegExp, workQueueCmd.ListOnly)

					// Use a protobuf response array for Redis caching because the protobuf can be serialized to bytes
					// and then a base64 string for easy Redis caching
					var hypervisorResponseArray = dora.HypervisorResponseArray{}

					// Loop through the Go channel object and send results to the Redis queue as soon
					// as they become available
					for {
						var hypervisorResponse = <-outputChannel

						if hypervisorResponse != nil {
							if dataBytes, errProtoMarshal := proto.Marshal(hypervisorResponse); errProtoMarshal != nil {
								// Error marshaling the record.  Encode the error for return via the response queue as
								// it is the only way to communicate with the client from this level in the code
								//
								var response = workqueue.QueueResponseWrapper{
									StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
									Data:       fmt.Sprintf("Could not marshal protobuf record for Redis queue because of error: %v", errProtoMarshal),
								}
								if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
									logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
								}
							} else {
								// Push the base64 encoded bytes of the marshalled Proto data type to the Redis queue.
								// Consumer thread that originally requested this data will process it for the client response
								var response = workqueue.QueueResponseWrapper{
									StatusCode: workqueue.STREAM_STATUS_OK,                   // Success
									Data:       base64.StdEncoding.EncodeToString(dataBytes), // <-- binary protobuf record encoded into base64 format
								}

								if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
									logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push result '%v' back to queue.", response))
								}
							}

							// This is for saving the record for Redis caching after loop exits.
							// NOTE: "preview" and "UI message" records are not cached since they are transient.
							if !hypervisorResponse.UiControlIsPreview &&
								len(hypervisorResponse.UiMsgInfo) == 0 &&
								len(hypervisorResponse.UiMsgWarning) == 0 &&
								len(hypervisorResponse.UiMsgError) == 0 {
								hypervisorResponseArray.Items = append(hypervisorResponseArray.Items, hypervisorResponse)
							}
						} else {
							// Stream signalled completion by placing "complete" record into the response queue
							if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, workqueue.Complete); pushErr != nil {
								logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push completion record '%v' back to queue.", workqueue.Complete))
							}

							// Normal completion
							break
						}
					}

					// Save to the Redis cache so later lookups on same query will return the result instantly (up until
					// the cache record expires via TTL though)
					if protoMarshaledData, errProto := proto.Marshal(&hypervisorResponseArray); errProto != nil {
						// Fatal
						return errors.New(fmt.Sprintf("Proto marshalling failed for caching because of error: %v", errProto))
					} else {
						if errRedis := redisConnect.SaveToRedisCache(ctx, redisutil.COMMAND_SEARCH_HYPERVISOR, redisKeyFragment, protoMarshaledData); errRedis != nil {
							if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
								// Not a fatal error but log it as it represents system instability
								logger.Log.Info(errRedis.Error())
							} else {
								// Actual REDIS error that needs to be displayed
								logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyFragment, errRedis))
							}
						}
					}
				}
			}
		}
	}

	// No fatal system errors if this point is reached
	return nil
}

// This method is called by worker thread with the command to start a search for VCenter VirtualMachine records
//
// Steps:
// (1) Deserialize command object from JSON
// (2) Login to VCenter
// (3) Start AppDash span from parent span (serialized as part of the command record)
// (4) Perform VCenter search and push results to Redis response queue as results are returned "live" (streaming mode)
// (5) Save results to Redis cache
func processVirtualMachineSearch(cmd *redisutil.RedisQueueCommand) error {
	// Data for this command.  Will be populated with JSON unmarshal below from the command data.
	var vcenterSearchCmd = workqueue.VCenterSearchCmd{}

	// Deserialize search command to get the search parameters as requested by the user
	if errJson := json.Unmarshal([]byte(cmd.Data), &vcenterSearchCmd); errJson != nil {
		// Fatal - could not deserialize the hypervisor list command's data
		return errors.New(fmt.Sprintf("Unable to JSON unmarshal the hypervisor search command record because of error: %v", errJson))
	} else {
		// TODO: This ctx needs to be some serialized version that links to the parent AppDash span
		var ctx = context.Background()

		if vsphereConnection, errAddressBook := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vcenterSearchCmd.ConnectionName); errAddressBook != nil {
			// Fatal for the stream - use response wrapper set to "error" with a message the error message
			// which will cause the client UI to display the message and gracefully quit streaming
			var response = workqueue.QueueResponseWrapper{
				StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
				Data:       fmt.Sprintf("Could not look up connection '%s' in address book because of error: %v", vcenterSearchCmd.ConnectionName, errAddressBook),
			}
			if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
				logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
			}
		} else {
			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vsphereConnection); errLogin != nil {
				// Fatal for the stream - use response wrapper set to "error" with a message the error message
				// which will cause the client UI to display the message and gracefully quit streaming
				var response = workqueue.QueueResponseWrapper{
					StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
					Data:       fmt.Sprintf("Could not log into VCenter '%s'.  Check that your connection name is correct.  Error: %v", vcenterSearchCmd.ConnectionName, errLogin),
				}
				if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
					logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
				}
			} else {
				if outputChannel, errVCenter := vcenter.ListVirtualMachineRecords(ctx, vcenterSearchCmd.PathRegExp, vcenterSearchCmd.HostRegExp, vcenterSearchCmd.ListOnly, true, vcSoapConnection); errVCenter != nil {
					// Fatal for the stream - use response wrapper set to "error" with a message the error message
					// which will cause the client UI to display the message and gracefully quit streaming
					var response = workqueue.QueueResponseWrapper{
						StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
						Data:       fmt.Sprintf("Failed to execute VirtualMachine search on pattern (pathFilter: '%s', hostFilter '%s') because of error: %v", vcenterSearchCmd.PathRegExp, vcenterSearchCmd.HostRegExp, errVCenter),
					}
					if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
						logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
					}
				} else {
					// Redis key used for cache lookup/retrieval
					var redisKeyValue = redisutil.GenerateVmSearchKeyFragment(vcenterSearchCmd.ConnectionName, vcenterSearchCmd.PathRegExp, vcenterSearchCmd.HostRegExp, vcenterSearchCmd.ListOnly)

					// Use a protobuf response array for Redis caching because the protobuf can be serialized to bytes
					// and then a base64 string for easy Redis caching
					var virtualMachineResponseArray = dora.VirtualMachineResponseArray{}

					// Loop through the Go channel object and send results to the Redis queue as soon
					// as they become available
					for {
						var virtualMachineResponse = <-outputChannel

						if virtualMachineResponse != nil {
							if dataBytes, errProtoMarshal := proto.Marshal(virtualMachineResponse); errProtoMarshal != nil {
								// Error marshaling the record.  Encode the error for return via the response queue as
								// it is the only way to communicate with the client from this level in the code
								//
								var response = workqueue.QueueResponseWrapper{
									StatusCode: workqueue.STREAM_STATUS_ERROR, // Error
									Data:       fmt.Sprintf("Could not marshal protobuf record for Redis queue because of error: %v", errProtoMarshal),
								}
								if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
									logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push error message '%v' back to queue.", response))
								}
							} else {
								// Push the base64 encoded bytes of the marshalled Proto data type to the Redis queue.
								// Consumer thread that originally requested this data will process it for the client response
								var response = workqueue.QueueResponseWrapper{
									StatusCode: workqueue.STREAM_STATUS_OK,                   // Success
									Data:       base64.StdEncoding.EncodeToString(dataBytes), // <-- binary protobuf record encoded into base64 format
								}

								if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, response); pushErr != nil {
									logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push result '%v' back to queue.", response))
								}
							}

							// This is for saving the record for Redis caching after loop exits.
							// NOTE: "preview" and "UI Message" records are not cached since they are transient.
							if !virtualMachineResponse.UiControlIsPreview &&
								len(virtualMachineResponse.UiMsgInfo) == 0 &&
								len(virtualMachineResponse.UiMsgWarning) == 0 &&
								len(virtualMachineResponse.UiMsgError) == 0 {
								virtualMachineResponseArray.Items = append(virtualMachineResponseArray.Items, virtualMachineResponse)
							}
						} else {
							// Stream signalled completion by placing "complete" record into the response queue
							if pushErr := redisConnect.PushResponse(cmd.ResponseQueue, workqueue.Complete); pushErr != nil {
								logger.Log.Error(pushErr, fmt.Sprintf("Worker thread could not push completion record '%v' back to queue.", workqueue.Complete))
							}

							// Normal completion
							break
						}
					}

					// Save to the Redis cache so later lookups on same query will return the result instantly (up until
					// the cache record expires via TTL though)
					if protoDataMarshalled, errProto := proto.Marshal(&virtualMachineResponseArray); errProto != nil {
						// Fatal
						return errors.New(fmt.Sprintf("Proto marshalling failed for caching because of error: %v", errProto))
					} else {
						if errRedis := redisConnect.SaveToRedisCache(ctx, redisutil.COMMAND_SEARCH_VIRTUALMACHINE, redisKeyValue, protoDataMarshalled); errRedis != nil {
							if errors.Is(errRedis, redisutil.ErrRedisCacheDisabled) {
								// Not a fatal error but log it as it represents system instability
								logger.Log.Info(errRedis.Error())
							} else {
								// Actual REDIS error that needs to be displayed
								logger.Log.Error(errRedis, fmt.Sprintf("Could not save Redis key value '%s' because of error: %v", redisKeyValue, errRedis))
							}
						}
					}
				}
			}
		}
	}

	// No fatal system errors if this point is reached
	return nil
}

// Returns a descriptive error message if access is denied for direct VCenter access otherwise returns "nil" error which
// will allow VCenter direct access
//
func checkVCenterDirectAccessEnabled(authToken *jwtlib.Token, vcenterName string) error {
	switch strings.ToLower(cfg.Config.GetVCenterDirectAllow()) {
	case "all":
		// No action -- allow for all
	case "none":
		// VCenter direct access is denied for everyone
		return status.Errorf(codes.PermissionDenied, "No SQLite Data Found for Environment %s and VCenter direct access is disabled for all users.  Check SQLite data sync is working for this environment.", vcenterName)
	case "admin":
		if !auth.IsAdmin(authToken) {
			// VCenter direct access is denied
			return status.Errorf(codes.PermissionDenied, "No SQLite Data Found for Environment %s and VCenter direct mode disabled for non-admin user.  Check SQLite data sync is working for this environment.", vcenterName)
		}
	default:
		// Unknown mode is equivalent to "none"
		return status.Errorf(codes.Internal, "VCenter direct access disallowed for unknown mode '%s'", cfg.Config.GetVCenterDirectAllow())
	}

	// Permission allowed so return no error object
	return nil
}
