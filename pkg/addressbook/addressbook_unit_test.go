package addressbook

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

//
// UNIT TEST: No test dependencies required for this test
//
// TESTS:
// Verify basic JWT token generation for any given username.   This test bypasses auth and generates a token
// for any user.
//

func TestAddressBookRead(t *testing.T) {
	assert.NotPanics(t, func() {
		addressBook, errAddressBook02 := New("address-book-test.json")

		//
		// Basic assertions
		//
		assert.Nil(t, errAddressBook02)
		assert.NotEmpty(t, addressBook)
		assert.Equal(t, 11, len(addressBook.VSphere.VSphereDirectory))
		assert.Equal(t, 10, len(addressBook.Foreman.ForemanDirectory))

		// Test individual connections based on connection02 name
		//
		//
		connection01, errAddressBook01 := addressBook.GetSelectedVSphereConnection("***bogus name***102939")
		assert.NotNil(t, errAddressBook01)
		assert.Nil(t, connection01)

		connection02, errAddressBook02 := addressBook.GetSelectedVSphereConnection("NewPayTV")
		assert.Nil(t, errAddressBook02)
		assert.NotNil(t, connection02)
		assert.Equal(t, "NewPayTV", connection02.Name)
		assert.Equal(t, "invct02p1.infra.smf1.mobitv", connection02.Url)
		assert.Equal(t, "sre@infra.smf1.mobitv", connection02.Username)

		connection03, errAddressBook03 := addressBook.GetSelectedVSphereConnection("Comporium")
		assert.Nil(t, errAddressBook03)
		assert.NotNil(t, connection03)
		assert.Equal(t, "Comporium", connection03.Name)
		assert.Equal(t, "10.62.100.250", connection03.Url)
		assert.Equal(t, "administrator@vsphere.local", connection03.Username)

		// Test that the default SSH credentials are read in successfully
		//
		//
		assert.NotNil(t, addressBook.Ssh)
		assert.NotNil(t, addressBook.Ssh.DefaultLogin)
		assert.NotNil(t, addressBook.Ssh.DefaultLogin.Isilon)
		assert.NotNil(t, addressBook.Ssh.DefaultLogin.Qumulo)
		assert.Equal(t, "root", addressBook.Ssh.DefaultLogin.Isilon.Username)
		assert.Equal(t, "root_password", addressBook.Ssh.DefaultLogin.Isilon.Password)
		assert.Equal(t, "admin", addressBook.Ssh.DefaultLogin.Qumulo.Username)
		assert.Equal(t, "admin_password", addressBook.Ssh.DefaultLogin.Qumulo.Password)
	})
}
