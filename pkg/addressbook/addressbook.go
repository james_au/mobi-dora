package addressbook

import (
	"bytes"
	"dora/pkg/config"
	"dora/pkg/logger"
	"dora/pkg/util/file"
	"errors"
	"fmt"
	"github.com/spf13/viper"
	"strings"
)

// This global address book instance can be accessed by the rest of the application and should be used as a singleton
// instance
var GlobalAddressBook *AddressBookStruct

// Initializes the global (singleton) addressbook.  This should be called exactly once at server startup.
//
// NOTE: The default addressbook path can be overridden by parameter.  This is useful for situations like unit testing.
//
// PARAM: addressBookPath   relative path to address book to init with or use empty string "" to load default addressbook defined in Dora runtime config file
//
func InitDefaultAddressBook(addressBookPath string) error {
	if addressBook, errAddressBook := New(addressBookPath); errAddressBook != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not load addressbook specified at %s because of error: %v", addressBookPath, errAddressBook))
	} else {
		// Successful address book initialization
		GlobalAddressBook = addressBook
	}

	// If this point is reached then no errors
	return nil
}

// Reads address book from optional given path.  If path is not provided, then the configured default path
// is used instead.
func New(addressBookPath string) (*AddressBookStruct, error) {
	var actualAddressBookPath string
	if len(addressBookPath) > 0 {
		logger.Log.Info(fmt.Sprintf("Reading provided address book '%s'", addressBookPath))
		actualAddressBookPath = addressBookPath
	} else {
		actualAddressBookPath = config.Config.GetAddressBookPath()
		logger.Log.Info(fmt.Sprintf("Reading provided address book '%s'", actualAddressBookPath))
	}

	if addressBook, errParse := ParseFromFile(actualAddressBookPath); errParse != nil {
		// Fatal JSON parsing error
		return nil, errors.New(fmt.Sprintf("Could not parse JSON data from file '%s' because of error: %v", actualAddressBookPath, errParse))
	} else {
		// Successful read & parse
		return addressBook, nil
	}
}

// Reads the raw configuration data by unmarshaling to internal annotated type
//
// See: https://github.com/spf13/viper#unmarshaling
func ParseFromBytes(configData []byte) (*AddressBookStruct, error) {
	// The root of our internal data struct.  Will be populated
	// during unmarshalling operation below.
	var addressBook AddressBookStruct

	//
	// Setup Viper for JSON read and then unmarshal
	//
	viper.SetConfigType("json")

	//
	// Parse the JSON.  This may generate fatal error if the user has any JSON syntax errors!
	//
	if readErr := viper.ReadConfig(bytes.NewBuffer(configData)); readErr != nil {
		// Fatal - Basic JSON syntax error - the JSON could not be parsed due to basic JSON syntax error.  This error
		// needs to be delivered to the user to take corrective action.
		return nil, errors.New(fmt.Sprintf("Failed to parse JSON address book data because of error: %v", readErr))
	}

	//
	// JSON parsing completed.  Now unmarshal to custom internal data structs
	//
	if viperErr := viper.Unmarshal(&addressBook); viperErr == nil {
		return &addressBook, nil
	} else {
		return nil, viperErr
	}
}

// Alternative function to read from JSON at file path
func ParseFromFile(filePath string) (*AddressBookStruct, error) {
	if dataBytes, errRead := file.ReadFile(filePath); errRead != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Reading address book file at '%s' failed because of error: %v", filePath, errRead))
	} else {
		return ParseFromBytes(dataBytes)
	}
}

// Gets the connection record matching the given connection name.
//
// If the connection name is blank, then the default connection is returned (if any) otherwise a descriptive
// error is returned.
func (o *AddressBookStruct) GetSelectedForemanConnection(connectionName string) (*ForemanConnectionStruct, error) {
	var activeConnection string
	if len(strings.TrimSpace(connectionName)) > 0 {
		activeConnection = connectionName
	} else {
		// Fatal - no connection specified
		return nil, errors.New("No connection name was specified")
	}

	// Linear search.  Return on first match.
	for i, _ := range o.Foreman.ForemanDirectory {
		if o.Foreman.ForemanDirectory[i].Name == activeConnection {
			return &o.Foreman.ForemanDirectory[i], nil
		}
	}

	// If this point is reached then no connection was found matching the connectionName
	return nil, errors.New(fmt.Sprintf(
		"No foreman connection found in directory for connection name '%s'.  Available names are: %s",
		connectionName,
		strings.Join(o.GetAvailableForemanConnectionNames(), ", ")))
}

// Gets an array of the names of all available Foreman connections.  Useful for printing out to the user.
//
func (o *AddressBookStruct) GetAvailableForemanConnectionNames() []string {
	var names []string

	for _, foremanConnection := range o.Foreman.ForemanDirectory {
		names = append(names, foremanConnection.Name)
	}

	return names
}

// Gets the connection record matching the given connection name.
//
// If the connection name is blank, then the default connection is returned (if any) otherwise a descriptive
// error is returned.
func (o *AddressBookStruct) GetSelectedVSphereConnection(connectionName string) (*VSphereConnectionStruct, error) {
	var activeConnection string
	if len(strings.TrimSpace(connectionName)) > 0 {
		activeConnection = connectionName
	} else {
		// Fatal - no connection specified
		return nil, errors.New("No connection name was specified")
	}

	// Linear search.  Return on first match.
	for i, _ := range o.VSphere.VSphereDirectory {
		if o.VSphere.VSphereDirectory[i].Name == activeConnection {
			return &o.VSphere.VSphereDirectory[i], nil
		}
	}

	// If this point is reached then no connection was found matching the connectionName
	return nil, errors.New(fmt.Sprintf(
		"No VSphere connection found in directory for connection name '%s'.  Available names are: %s",
		connectionName,
		strings.Join(o.GetAvailableVSphereConnectionNames(), ", ")))
}

// Gets an array of the names of all available VSphpere connections.  Useful for printing out to the user.
//
func (o *AddressBookStruct) GetAvailableVSphereConnectionNames() []string {
	var names []string

	for _, vsphereConnection := range o.VSphere.VSphereDirectory {
		names = append(names, vsphereConnection.Name)
	}

	return names
}

// Returns a redacted copy of the configuration so users can know what available configurations are available
//
func (o *AddressBookStruct) GetAvailableConnections() string {
	var buffer string

	buffer += "*** AVAILABLE VSPHERE CONNECTIONS: ***\n"
	var vsphereDirectory = o.VSphere.VSphereDirectory
	if vsphereDirectory != nil {
		for _, vsphereConnection := range vsphereDirectory {
			buffer += fmt.Sprintf("\tName: %s\n", vsphereConnection.Name)
			buffer += fmt.Sprintf("\tURL : %s\n", vsphereConnection.Url)
			buffer += fmt.Sprintf("\t---\n")
		}
	}

	buffer += "\n"
	buffer += "*** AVAILABLE FOREMAN CONNECTIONS: ***\n"
	var foremanDirectory = o.Foreman.ForemanDirectory
	if foremanDirectory != nil {
		for _, foremanConnection := range foremanDirectory {
			buffer += fmt.Sprintf("\tName: %s\n", foremanConnection.Name)
			buffer += fmt.Sprintf("\tBase URL: %s\n", foremanConnection.BaseUrl)
			buffer += fmt.Sprintf("\tPuppet Master: %s\n", foremanConnection.PuppetMasterHost)
			buffer += fmt.Sprintf("\tSalt Master: %s\n", foremanConnection.SaltHost)
			buffer += fmt.Sprintf("\tVCenter Reference: %s\n", foremanConnection.VCenterReference)
			buffer += fmt.Sprintf("\t---\n")
		}
	}

	return buffer
}

// This is the root of the data structure object hierarchy
//
type AddressBookStruct struct {
	VSphere VSphereStruct `mapstructure:"vsphere"`
	Foreman ForemanStruct `mapstructure:"foreman"`
	Ssh SshStruct         `mapstructure:"ssh"`
}

type SshStruct struct {
	DefaultLogin DefaultLoginStruct   `mapstructure:"default_login"`
}

type DefaultLoginStruct struct {
	Isilon IsilonLoginStruct   `mapstructure:"isilon"`
	Qumulo QumuloLoginStruct   `mapstructure:"qumulo"`
}

type IsilonLoginStruct struct {
	Username   string `mapstructure:"username"`
	Password   string `mapstructure:"password"`
}

type QumuloLoginStruct struct {
	Username   string `mapstructure:"username"`
	Password   string `mapstructure:"password"`
}

type VSphereStruct struct {
	Config           VSphereConfigStruct       `mapstructure:"config"`
	VSphereDirectory []VSphereConnectionStruct `mapstructure:"directory"`
}

type VSphereConfigStruct struct {
}

type VSphereConnectionStruct struct {
	Name                  string `mapstructure:"name"`
	Url                   string `mapstructure:"url"`
	Username              string `mapstructure:"username"`
	Password              string `mapstructure:"password"`
	SshUsername           string `mapstructure:"ssh_username"`
	SshPassword           string `mapstructure:"ssh_password"`
	SshHypervisorUsername string `mapstructure:"ssh_hypervisor_username"`
	SshHypervisorPassword string `mapstructure:"ssh_hypervisor_password"`
	CronSyncVCenter       string `mapstructure:"cron_sync_vcenter"`
	CronSyncNfs           string `mapstructure:"cron_sync_nfs"`
	RackPrimary           string `mapstructure:"rack_primary"`
	RackSecondary         string `mapstructure:"rack_secondary"`
	RackQuorum            string `mapstructure:"rack_quorum"`
}

type ForemanStruct struct {
	Config           ForemanConfigStruct       `mapstructure:"config"`
	ForemanDirectory []ForemanConnectionStruct `mapstructure:"directory"`
}

type ForemanConfigStruct struct {
}

type ForemanConnectionStruct struct {
	Name             string `mapstructure:"name"`
	BaseUrl          string `mapstructure:"baseurl"`
	Authorization    string `mapstructure:"authorization"`
	SshUsername      string `mapstructure:"ssh_username"`
	SshPassword      string `mapstructure:"ssh_password"`
	SaltHost         string `mapstructure:"salt_host"`
	PuppetMasterHost string `mapstructure:"puppet_master_host"`
	VCenterReference string `mapstructure:"vcenter_ref"`
}
