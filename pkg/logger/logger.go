package logger

import kloglib "k8s.io/klog/v2/klogr"

// Setup global logging library
var Log = kloglib.New()
