package dbutil

import (
	"context"
	"crawshaw.io/sqlite"
	"crawshaw.io/sqlite/sqlitex"
	"dora/pkg/logger"
	"dora/pkg/util"
	"errors"
	"fmt"
	"sync"
)

// Creates the SQLite database schema in preparation for data load.
//
// NOTE:  This function only needs to be called once but MULTIPLE calls to this function are okay.
//        Because schema objects are created only if they do *not* exist and ignored otherwise.
//
func CreateSchema(environmentName string, dbPool *sqlitex.Pool) error {
	if connection := dbPool.Get(context.Background()); connection == nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not get a SQLite connection from the connection pool.  Pool returned a null connection which indicates an error but no further error information is available"))
	} else {
		// Ensure only on thread can create DDL per environment at a time
		defer getSyncMutex(environmentName).Unlock()
		getSyncMutex(environmentName).Lock()

		// At the end, must manually return connection to the DB connection pool otherwise will eventually run out of
		// connections and database operations will appear to hang
		defer dbPool.Put(connection)

		// Create the specified table from the data structure in sequence
		for _, ddl := range createDDL {
			if !util.IsBlank(ddl.name) && !util.IsBlank(ddl.sql)  {
				if stmt, errStmt := connection.Prepare(ddl.sql); errStmt != nil {
					// Fatal
					return errors.New(fmt.Sprintf("Could not create prepared statement for %s table creation because of error: %v", ddl.name, errStmt))
				} else {
					// Execute prepared statement
					if _, errStmtExec := stmt.Step(); errStmtExec != nil {
						// Fatal
						return errors.New(fmt.Sprintf("Could not setup SQLite tables because of database execution error: %v", errStmtExec))
					} else {
						logger.Log.Info(fmt.Sprintf("SQLite cache DB table setup for %s for VCenter connection: %s", ddl.name, environmentName))
					}
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}

var mutex sync.Mutex
var mapEnvironmentLock = make(map[string]*sync.Mutex)
func getSyncMutex(environmentName string) *sync.Mutex {
	defer mutex.Unlock()
	mutex.Lock()

	if value, exists := mapEnvironmentLock[environmentName]; exists {
		// Mutex for environment already exists so just return it directly
		return value
	} else {
		// Lazy create first
		var newMutex = &sync.Mutex{}
		mapEnvironmentLock[environmentName] = newMutex

		return newMutex
	}
}

// Clears all tables specified in the list
//
// PARAM: tableNames   1 or more tableNames matching names of table in the database
func ClearTables(connection *sqlite.Conn, tableNames ...string) error {
	if tableNames != nil {
		for _, tableName := range tableNames {
			if stmt, errStmt := connection.Prepare("DELETE FROM " + tableName); errStmt != nil {
				// Fatal
				var errMessage = fmt.Sprintf("Couldn't clear table '%s' because of error: %v", tableName, errStmt)

				return errors.New(errMessage)
			} else {
				if _, errExec := stmt.Step(); errExec != nil {
					// Fatal
					var errMessage = fmt.Sprintf("Could NOT execute SQL during table clear '%s' because of error: %v", tableName, errExec)

					return errors.New(errMessage)
				}
			}
		}
	}

	// No errors if this point is reached
	return nil
}



// Gets a count of records from the named database table using the current connection
//
// PARAM: connection   active SQLite database connection
// PARAM: tableName    name of table to count
func CountRecords(connection *sqlite.Conn, tableName string) (int64, error) {
	if stmtCount, errStmt := connection.Prepare(fmt.Sprintf("SELECT COUNT(*) as cnt FROM %s", tableName)); errStmt != nil {
		// Fatal - unlikely since SQL statement is fixed
		return -1, errors.New(fmt.Sprintf("Could not count records in %s table because of database statement creation error: %v", tableName, errStmt))
	} else {
		// Expecting exactly 1 row to be returned
		if isRowReturned, errStmtExec := stmtCount.Step(); errStmtExec != nil {
			// Not-fatal but log it
			return -1, errors.New(fmt.Sprintf("Could not count records in %s table because of database execution error: %v", tableName, errStmt))
		} else {
			if isRowReturned {
				var recordCount = stmtCount.GetInt64("cnt")

				// Must be finalized or cause SQLite pool to throw a panic when returning the parent connection to the pool
				stmtCount.Finalize()

				return recordCount, nil
			} else {
				// Unlikely since table name is fixed
				return 0, nil
			}
		}
	}
}

// Returns the average duration (in floating point seconds) of the past 'n syncs for the referenced sync log table
//
// PARAM: n          up to 'n' number of most recent "completed" sync history records will be used in the calculation
// PARAM: tableName  name of the sync history table (e.g. "NfsSyncHistory") which allows this function to be re-used for both VCenter and NFS sync processes
// PARAM: connection live connection to a Dora database
// RETURN: average duration in floating point seconds of the past 'n' most recent syncs
func GetAverageSyncSecs(n int32, tableName string, connection *sqlite.Conn) (float64, error) {
	if stmtUpdate, errUpdate := connection.Prepare(fmt.Sprintf(
		"SELECT AVG((end_time_ms - start_time_ms) / 1000.0) avg_completion_secs\n"+
			"FROM %s\n"+
			"WHERE status = 'COMPLETE' AND end_time_ms IS NOT NULL\n"+
			"ORDER BY end_time_ms DESC\n"+
			"LIMIT $limit", tableName)); errUpdate != nil {
		// Fatal - unlikely
		return -1, errors.New(fmt.Sprintf("Could not get average sync durations because of database statement error: %v", errUpdate))
	} else {
		// Always close the statement at the end otherwise the connection cannot be returned to the pool
		defer func() {
			stmtUpdate.Reset()
			stmtUpdate.Finalize()
		}()

		// Sets the range over the statistics calculation
		stmtUpdate.SetInt64("$limit", int64(n))

		// Expecting exactly 1 row returned
		if isRowReturned, errStep := stmtUpdate.Step(); errStep != nil {
			return -1, errors.New(fmt.Sprintf("Could not get average sync durations because of database execution error: %v", errStep))
		} else {
			if isRowReturned {
				var average_secs = stmtUpdate.GetFloat("avg_completion_secs")

				return float64(average_secs), nil
			} else {
				return -1, errors.New("No rows returned but expecting exactly 1 row")
			}
		}
	}
}

