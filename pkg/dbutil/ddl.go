package dbutil

// Simple 2-tuple type to specify a named DDL creation record
//
type SQLCreateDDL struct {
	name string
	sql string
}

// Base SQL query for room report
const RoomSql = `SELECT 
  hv.path, 
  hv.vm_count, 
  hv.cpus, 
  hv.mem_mb / 1024 AS mem_gb, 
  hv.hdd_capacity_gb,
  hv.cpus - IFNULL((SELECT sum(cpus) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn'), 0) AS headroom_cpu,
  hv.mem_mb/1024 - IFNULL((SELECT sum(mem_mb/1024) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn'), 0) AS headroom_mem_gb,
  hv.hdd_capacity_gb - IFNULL((SELECT sum(hdd_allocated_gb) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn'), 0) AS headroom_hdd_gb,
  (SELECT COUNT(*) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn' AND vm.name like 'stsgl%') AS stsgl_count, 
  (SELECT COUNT(*) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn' AND vm.name like 'stvar%') AS stvar_count, 
  (SELECT COUNT(*) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn' AND vm.name like 'nvrcw%') AS nvrcw_count, 
  (SELECT COUNT(*) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn' AND vm.name like 'stmmx%') AS stmmx_count, 
  (SELECT GROUP_CONCAT(name) FROM VirtualMachine AS vm WHERE vm.host = hv.path AND state = 'poweredOn' ORDER BY name) AS vm_list 
FROM Hypervisor AS hv`


// Base SQL query for Shared Storage report
const SharedStorageReportSql = `
SELECT * FROM
(
    SELECT nfs_host AS storage_id, nfs_host AS name, nfs_type AS type, nfs_cluster_name AS cluster_name, nfs_capacity_bytes AS capacity_bytes, nfs_used_bytes AS capacity_used_bytes
    FROM NfsMount
    GROUP BY nfs_host
)
UNION
SELECT url AS storage_id, name AS name, 'VMWare-' || type AS type, 'N/A' AS nfs_cluster_name, capacity_bytes AS capacity_bytes, capacity_bytes - capacity_remaining_bytes AS capacity_used_bytes
FROM Datastore
group by storage_id
ORDER BY storage_id`


// Base SQL query for Datastore report
const DatastoreReportSql = `
SELECT
  ds.url AS datastore_url,
  ds.name AS datastore_name,
  ds.path AS datastore_path,
  ds.managed_obj_ref AS datastore_managed_obj_ref,
  ds.capacity_bytes AS datastore_capacity_bytes,
  ds.capacity_remaining_bytes AS datastore_capacity_remaining_bytes,
  ds.type AS datastore_type,
  Hypervisor.name AS hypervisor_name,
  Hypervisor.path AS hypervisor_path,
  Hypervisor.managed_obj_ref AS hypervisor_managed_obj_ref,
  ds.mount_point AS hypervisor_mount_point,
  VirtualMachine.name as virtualmachine_name
FROM (
       SELECT Datastore.*, DatastoreHypervisorRefs.*
       FROM Datastore LEFT OUTER JOIN DatastoreHypervisorRefs WHERE Datastore.managed_obj_ref = DatastoreHypervisorRefs.datastore_managed_obj_ref
   ) AS ds
       LEFT OUTER JOIN Hypervisor ON Hypervisor.managed_obj_ref = ds.hypervisor_managed_obj_ref
       LEFT OUTER JOIN VirtualMachine ON Hypervisor.path = VirtualMachine.host`

// Base SQL query for NFS report
const NfsReportSql = `
SELECT hostname AS vm_host, hostpath as vm_path, mount AS vm_mount_point, mount_user AS vm_mount_user, mount_group AS vm_mount_group, mount_perms AS vm_mount_perms, nfs_host, nfs_path, nfs_type, nfs_cluster_name, nfs_isilon_lnn, nfs_capacity_bytes, nfs_used_bytes, fstab_line AS fstab
FROM NfsMount
ORDER BY nfs_host, nfs_path, vm_host, vm_mount_point`

// Base SQL query for VM Storage report
const VmStorageReportSql = `
SELECT name, size_tb
FROM UserNotesVmStorage
ORDER BY name, size_tb`

// Base SQL query for IP Range report
const IpRangeReportSql = `
SELECT name, ip_range, vlan, user_notes
FROM UserNotesIpRange
ORDER BY name`

// Base SQL query for Storage Summary report
const StorageSummaryReportSql = `
SELECT name, cidr, ports, username, password
FROM UserNotesStorageSummary
ORDER BY name`

// This SQL is used for Balance view to get its initial Hypervisor "load out" which is a list of the hypervisor's
// "powered on" child Virtual Machines.
//
// The hypervisor's full VCPath is required as input:
// "/INT-MSP/host/Integration-msp Cluster/f-03-35.infra.smf1.mobitv"
//
// NOTE: Balance view does *not* include "powered off" VMs
const HypervisorLoadOutSql = `SELECT name, path, cpus, mem_mb / 1024.0 as mem_gb FROM VirtualMachine WHERE host = $hostVcPath AND state = 'poweredOn' ORDER BY name;`

var createDDL = []SQLCreateDDL{
	{
		"Hypervisor",
		`CREATE TABLE IF NOT EXISTS Hypervisor (name TEXT, path TEXT PRIMARY KEY, cpus INTEGER, mem_mb INTEGER, hdd_capacity_gb INTEGER, vm_count INTEGER, mem_usage_perc FLOAT, cpu_usage_perc FLOAT, uptime STRING, state STRING, status STRING, vmotion_enabled INTEGER, net_port_id TEXT, net_system_name TEXT, last_sync_ms INTEGER, rack TEXT, managed_obj_ref string)`,
	},
	{
		"VirtualMachine",
		`CREATE TABLE IF NOT EXISTS VirtualMachine (name TEXT, path TEXT PRIMARY KEY, host TEXT, os TEXT, cpus INTEGER, total_cpu_mhz INTEGER, hdd_allocated_gb INTEGER, mem_mb INTEGER, cpu_hot_add_enabled INTEGER, mem_hot_add_enabled INTEGER, uptime_sec INTEGER, mem_usage_perc FLOAT, cpu_usage_perc FLOAT, state STRING, overall_status STRING, last_sync_ms INTEGER, managed_obj_ref STRING)`,
	},
	{
		"SyncHistory",
		`CREATE TABLE IF NOT EXISTS SyncHistory (id INTEGER PRIMARY KEY, environment_name TEXT NOT NULL, status TEXT NOT NULL, initiated_by TEXT NOT NULL, start_time_ms INTEGER NOT NULL, end_time_ms INTEGER, records_processed INTEGER);`,
	},
	{
		"SyncLogEntry",
		`CREATE TABLE IF NOT EXISTS SyncLogEntry (parent_id INTEGER NOT NULL, time_ms INTEGER NOT NULL, msg_err TEXT, msg_warn TEXT, msg_info TEXT, FOREIGN KEY(parent_id) REFERENCES SyncHistory(id) ON DELETE CASCADE);`,
	},
	{
		"NfsSyncHistory",
		`CREATE TABLE IF NOT EXISTS NfsSyncHistory (id INTEGER PRIMARY KEY, environment_name TEXT NOT NULL, status TEXT NOT NULL, initiated_by TEXT NOT NULL, start_time_ms INTEGER NOT NULL, end_time_ms INTEGER, records_processed INTEGER);`,
	},
	{
		"NfsSyncLogEntry",
		`CREATE TABLE IF NOT EXISTS NfsSyncLogEntry (parent_id INTEGER NOT NULL, time_ms INTEGER NOT NULL, msg_err TEXT, msg_warn TEXT, msg_info TEXT, FOREIGN KEY(parent_id) REFERENCES NfsSyncHistory(id) ON DELETE CASCADE);`,
	},
	// DDL to create the child HypervisorDatastore table which stores the hypervisor's attached VCenter Datastores (NFS, VFS5)
	//
	{
		"HypervisorDatastore",
		`
CREATE TABLE IF NOT EXISTS HypervisorDatastore (
   parentHypervisorPath STRING NOT NULL,
   path STRING NOT NULL,
   url STRING NOT NULL,
   PRIMARY KEY(parentHypervisorPath, path),
   FOREIGN KEY(parentHypervisorPath) REFERENCES Hypervisor(path) ON DELETE CASCADE
);`,
	},
	{
		"HypervisorNetwork",
		`
CREATE TABLE IF NOT EXISTS HypervisorNetwork (
   parentHypervisorPath STRING NOT NULL, 
   name STRING NOT NULL,
   PRIMARY KEY(parentHypervisorPath, name),
   FOREIGN KEY(parentHypervisorPath) REFERENCES Hypervisor(path) ON DELETE CASCADE
);`,
	},
	// DDL to create the child VirtualMachineDatastore table which stores the VM's attached VCenter Datastores (NFS, VFS5)
	//
	{
		"VirtualMachineDatastore",
		`
CREATE TABLE IF NOT EXISTS VirtualMachineDatastore (
   parentVirtualMachinePath STRING NOT NULL,
   path STRING NOT NULL,
   url STRING NOT NULL,
   PRIMARY KEY(parentVirtualMachinePath, path),
   FOREIGN KEY(parentVirtualMachinePath) REFERENCES VirtualMachine(path) ON DELETE CASCADE
);`,
	},
	// DDL to create the child VirtualMachineNetwork table which stores the VM's attached VCenter Network paths
	//
	{
		"VirtualMachineNetwork",
		`
CREATE TABLE IF NOT EXISTS VirtualMachineNetwork (
   parentVirtualMachinePath STRING NOT NULL,
   path STRING NOT NULL,
   adapterType STRING,
   macAddress  STRING,
   PRIMARY KEY(parentVirtualMachinePath, path),
   FOREIGN KEY(parentVirtualMachinePath) REFERENCES VirtualMachine(path) ON DELETE CASCADE
);`,
	},
	// DDL to create the child Hypervisor tags used for balance restrictions
	//
	{
		"HypervisorTag",
		`
CREATE TABLE IF NOT EXISTS HypervisorTag (
   parentHypervisorPath STRING NOT NULL,
   tagName STRING NOT NULL,
   PRIMARY KEY(parentHypervisorPath, tagName),
   FOREIGN KEY(parentHypervisorPath) REFERENCES Hypervisor(path) ON DELETE CASCADE
);`,
	},
	// DDL to create the VM Audit Move Log table
	//
	{
		"AuditVmMove",
		`
CREATE TABLE IF NOT EXISTS AuditVmMove (
    id INTEGER NOT NULL,        /* id will be auto-incremented by SQLite -- this only works with INTEGER data type */
    username STRING NOT NULL, 
    vcenter STRING NOT NULL, 
    hostname STRING NOT NULL,   /* hostname of the processing host */
    session_uuid STRING NOT NULL,   /* unique global UUID of session */
    timestamp_ms LONG NOT NULL,
    duration_sec FLOAT NOT NULL,  /* the duration (in seconds) to move the VM either success or failure */
    vm_path STRING NOT NULL,
    source_hv_path STRING NOT NULL,
    destination_hv_path STRING NOT NULL,
    status STRING NOT NULL,
    extra STRING,
    PRIMARY KEY(id)
);`,
	},
	// DDL to create the DatastoreVirtualMachineRefs table
	//
	{
		"DatastoreVirtualMachineRefs",
		`
CREATE TABLE IF NOT EXISTS DatastoreVirtualMachineRefs (
    datastore_managed_obj_ref STRING NOT NULL,
    vm_managed_obj_ref STRING NOT NULL,
    PRIMARY KEY(datastore_managed_obj_ref, vm_managed_obj_ref)
);`,
	},
	// DDL to create the DatastoreHypervisorRefs table
	//
	{
		"DatastoreHypervisorRefs",
		`
CREATE TABLE IF NOT EXISTS DatastoreHypervisorRefs (
    datastore_managed_obj_ref STRING NOT NULL,
    hypervisor_managed_obj_ref STRING NOT NULL,
    mount_point STRING,
    PRIMARY KEY(datastore_managed_obj_ref, hypervisor_managed_obj_ref)
);`,
	},
	// DDL to create the Datastore table
	//
	{
		"Datastore",
		`
CREATE TABLE IF NOT EXISTS Datastore (
    name STRING NOT NULL,
    path STRING NOT NULL,
    url STRING NOT NULL,
    managed_obj_ref STRING NOT NULL,
    capacity_bytes INTEGER DEFAULT 0,
    capacity_remaining_bytes INTEGER DEFAULT 0,
    type STRING,
    PRIMARY KEY(url, path)
);`,
	},
	// DDL to create the NfsMount table which stores environment SSH scan of NFS mounts across hosts
	//
	{
		"NfsMount",
		`
CREATE TABLE IF NOT EXISTS NfsMount (
    hostname     STRING NOT NULL,
    hostpath     STRING NOT NULL,
    mount        STRING NOT NULL,
    mount_user   STRING NOT NULL,    
    mount_group  STRING NOT NULL,
    mount_perms  STRING NOT NULL,
    nfs_host     STRING NOT NULL,
    nfs_path     STRING NOT NULL,
    nfs_type           STRING,
    nfs_cluster_name   STRING,
    nfs_isilon_lnn     STRING,
    nfs_capacity_bytes INTEGER DEFAULT 0 NOT NULL,
    nfs_used_bytes     INTEGER DEFAULT 0 NOT NULL,
    fstab_line   STRING NOT NULL,
    last_sync_ms INT DEFAULT 0 NOT NULL,
    CONSTRAINT NfsMount_pk  PRIMARY KEY (hostname, mount)
);`,
	},
	// DDL to create the UserNotesVmStorage table which stores user notes for typical VM sizes
	//
	{
		"UserNotesVmStorage",
		`
CREATE TABLE IF NOT EXISTS UserNotesVmStorage
(
    name STRING NOT NULL CONSTRAINT UserNotesVmStorage_pk unique,
    size_tb FLOAT DEFAULT 0 NOT NULL
);`,
	},
	// DDL to create the UserNotesIpRange table
	//
	{
		"UserNotesIpRange",
		`
CREATE TABLE IF NOT EXISTS UserNotesIpRange
(
    name STRING NOT NULL CONSTRAINT UserNotesIpRange_pk unique,
    ip_range STRING,
    vlan STRING,
    user_notes STRING
);`,
	},
	// DDL to create the UserNotesStorageSummary table
	//
	{
		"UserNotesStorageSummary",
		`
CREATE TABLE IF NOT EXISTS UserNotesStorageSummary
(
    name STRING NOT NULL CONSTRAINT UserNotesStorageSummary unique,
    cidr STRING,
    ports STRING,
    username STRING,
    password STRING
);`,
	},
}
