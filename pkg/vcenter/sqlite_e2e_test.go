package vcenter

import (
	"context"
	"dora/pkg/addressbook"
	cfg "dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/dbutil"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

//
// Test the SQLite crawshaw.io implementation with an integration e2e test.  This test has environment
// dependencies that must be met before the test can complete:
//
// (1) Network and login access to a functioning VCenter
// (2) Disk access to write a SQLite database file
//
// NOTE:
// (1) This test function can actually be used to load data from any instance by updating the VCenter connection name and
//     running.  A new .sqlite database file will be generated and populated with the latest data.
//
// SQLITE DATAFILE:
// This test will load a corresponding SQLite DB in the *test folder* using the same logic as the AUT.  If no such DB
// files exists then it will be created.  If a DB file exists then it will be modified.  Be sure to manage this file
// accordingly depending on if you want to load new data or overwrite existing data.
//
// Refs:
// (1) https://github.com/crawshaw/sqlite
// (2) https://godoc.org/crawshaw.io/sqlite
//

// This must match a pre-defined connection in the configured address book
const VCENTER_CONNECTION_NAME = "OldPayTV"

// Syncs all latest records from the given VCenter environment into a SQLite database file -- see notes above
// on how to manage SQLite DB files in the test folder.
//
// ENVIRONMENT DEPENDENCIES:
// All environment dependencies must be satisfied before this e2e test will complete successfully.
//
func Test001_SyncData(t *testing.T) {
	var ctx = context.WithValue(context.Background(), "environment", "e2e_test")

	assert.NotPanics(t, func() {
		addressBook, errAddressBook := addressbook.New("../../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Create new sync object which also creates the SQLite instance
		vcenterSync, errVCenter := New(vcConnection)
		assert.Nil(t, errVCenter)
		assert.NotNil(t, vcenterSync, "Could not create sync object")

		//
		// Perform the DB synchronization task -- this will take a while!
		//
		vcenterSync.StartDataSyncInBackground(ctx)

		// Perform tasks to verify database synchronization was successful
		var dbConnection = vcenterSync.dbPool.Get(ctx)
		assert.NotNil(t, dbConnection)

		// Assert conditions are true
		assert.FileExists(t, fmt.Sprintf("dora-%s.sqlite", vcenterSync.GetName()), "Expected SQLite database file to be created on disk")
		// TODO: assert some records were actually created in the respective database tables
	})
}

// Uses API to create a new SyncHistory record which is required to track all sync runs within the app's UI and which
// helps with Dora data management
//
// ENVIRONMENT DEPENDENCIES:
// All environment dependencies must be satisfied before this e2e test will complete successfully.
//
func Test001_SyncHistoryrecord(t *testing.T) {
	assert.NotPanics(t, func() {
		addressBook, errAddressBook := addressbook.New("../../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Create new sync object which also creates the SQLite instance
		vcenterSync, errVCenter := New(vcConnection)
		assert.Nil(t, errVCenter)
		assert.NotNil(t, vcenterSync, "Could not create sync object")

		dbutil.CreateSchema(vcConnection.Name, vcenterSync.GetConnectionPool())

		//
		// Create the SyncHistory record in DB -- completes very quickly
		//
		syncHistoryId, errHistoryRec := vcenterSync.createDbSyncRecordEntry(context.Background())
		assert.Nil(t, errHistoryRec)
		assert.True(t, syncHistoryId > 0)

		fmt.Printf("Created SyncHistory record ID: %d", syncHistoryId)

		//
		// Update the SyncHistory record simulating completion
		//
		var errSyncHistoryUpdate = vcenterSync.UpdateSyncCompletion(syncHistoryId, constants.DATA_SYNC_STATUS_COMPLETE, 1263)
		assert.Nil(t, errSyncHistoryUpdate)

		// TODO: Read back the SyncHistory record from the given historyRecId to confirm it really was created
	})
}
