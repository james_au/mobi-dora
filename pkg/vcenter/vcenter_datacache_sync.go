package vcenter

import (
	"context"
	"crawshaw.io/sqlite"
	"crawshaw.io/sqlite/sqlitex"
	"dora/pkg/addressbook"
	"dora/pkg/api"
	"dora/pkg/balancingutil"
	"dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/dbutil"
	"dora/pkg/logger"
	"dora/pkg/mail"
	"dora/pkg/syncrefreshlock"
	"dora/pkg/util"
	"dora/pkg/util/file"
	"dora/pkg/utilsqlite"
	"dora/pkg/vcenterutil"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/robfig/cron/v3"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Default cron schedule for database auto-update
//
const DEFAULT_CRON_SCHEDULE = "0 0,6,12,18 * * *"

//
// Creates a local SQLite instance and keeps it in sync with select VCenter data (Hypervisor, VM, etc)
//
// Refs:
// (1) https://github.com/crawshaw/sqlite
// (2) https://godoc.org/crawshaw.io/sqlite
//

// Represents the VCenter synchronization parameters and runtime.  This object is needed for
// all interactions with the SQLite data for both query and load.
type VCenterSyncRec struct {
	dbFilePath          string                               // current path to the SQLite database file providing the data for this environment
	syncCron            *cron.Cron                           // current active cron object (initialized to nil)
	cronExpression      string                               // current cron schedule expression string
	vsphereConnection   *addressbook.VSphereConnectionStruct // connection address book entry
	dbPool              *sqlitex.Pool                        // the SQLite db pool provides connection to the database to run all queries
	dbPoolLog           *sqlitex.Pool                        // the SQLite db pool for logging code only
	activeSyncHistoryId *int64                               // The current syncHistoryId otherwise is nil
}

// Initializes a new SQLite cache for the given VSphere connection.
//
// NOTES:
// (1) the existing SQLite database file with any of its previously saved data will be used if found.  Otherwise,
// a new SQLite database file is created from scratch and populated via a full load.  If you want to continue using
// existing data and avoid a long full load, then ensure the corresponding SQLite file exists on disk at the proper
// location prior server startup.
// (2) This does not load/synchronize data.  Call the SyncData() for that.
func New(vsphereConnection *addressbook.VSphereConnectionStruct) (*VCenterSyncRec, error) {
	//
	// Connect to SQLite by creating a connection pool
	//

	// Generate a SQLite filepath for SQLite to create the database file.  Its based on
	// (1) Configured data folder path (default app value or overriden by override configs)
	// (2) Name of the VCenter/VSphere instance
	//
	// Ex: ./dora-CSpire.sqlite
	// Ex: /opt/dora/sqlite/dora-NewPayTV.sqlite
	var dbFilePath = utilsqlite.GenerateDbFilePath(config.Config.GetSQLiteDataFolder(), vsphereConnection.Name)

	// Create the database by using SQLite API Open() method on the file
	//
	if dbPool, errOpen := sqlitex.Open(fmt.Sprintf("file:%s", dbFilePath), 0, 10); errOpen != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not create or connect to SQLite database file at '%s' because of error: %v", dbFilePath, errOpen))
	} else {
		if dbPoolLog, errOpenLog := sqlitex.Open(fmt.Sprintf("file:%s", dbFilePath), 0, 5); errOpenLog != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Could open SQLite database file at '%s' because of error: %v", dbFilePath, errOpen))
		} else {
			// Use default cron expression for syncing unless overriden by value specified in the VCenter connection's
			// address book
			var cronExpression = DEFAULT_CRON_SCHEDULE
			if len(vsphereConnection.CronSyncVCenter) > 0 {
				// Cron expression specified so use override default with it
				cronExpression = vsphereConnection.CronSyncVCenter
				logger.Log.Info(fmt.Sprintf("Overriding cron expression for VCenter environment %s : [ %s ]", vsphereConnection.Name, cronExpression))
			} else {
				logger.Log.Info(fmt.Sprintf("Using default cron expression for VCenter environment %s : [ %s ]", vsphereConnection.Name, cronExpression))
			}

			// Create the VCenterSync record with given and computed parameters
			var vcenterSync = VCenterSyncRec{
				dbFilePath:        dbFilePath,
				vsphereConnection: vsphereConnection,
				dbPool:            dbPool,
				dbPoolLog:         dbPoolLog,
				cronExpression:    cronExpression,
			}

			// Ensure SQLite database schema exists in preparation for data sync later.  Schema objects are *not*
			// created if they already exist.
			dbutil.CreateSchema(vcenterSync.GetName(), vcenterSync.dbPool)

			return &vcenterSync, nil
		}
	}
}

// Ensures the SQLite data folder exists and will attempt to create it if possible.  But this can fail if the server's
// UNIX user does not have write permissions to the configured path.
//
// IMPORTANT:
// Startup sequence should run this at least once and abort server startup if the SQLite folder cannot be accessed
//
func EnsureSQLiteFolderExists() error {
	// Try to create the configuration folder -- this could likely fail if the path is in a root system area like /opt
	// and app user is not root.  But its worth a try.  Otherwise, will generate an error otherwise so administrator
	// can take corrective action.
	if errMkdir := os.MkdirAll(config.Config.GetSQLiteDataFolder(), 0755); errMkdir != nil {
		return errors.New(fmt.Sprintf("Cannot create required SQLite data dir %s because of error: %v", config.Config.GetSQLiteDataFolder(), errMkdir))
	}

	// Ensure the file path specified exists can can be written to otherwise generate an error
	if _, errWriteable := file.IsWriteableDir(config.Config.GetSQLiteDataFolder()); errWriteable != nil {
		// Cannot write or access the configured folder
		return errors.New(fmt.Sprintf("Cannot create/access SQLite files in configured directory '%s'. Check that directory exists and has write permissions.", config.Config.GetSQLiteDataFolder()))
	} else {
		// Success
		return nil
	}
}

// Gets the name of the VCenter connection this sync is operating on.  Good for things like logging and database file
// creation.
func (o *VCenterSyncRec) GetName() string {
	return o.vsphereConnection.Name
}

// Gets the name of the SQLite DB pool which allows creating connections to execute query against the SQLite database
func (o *VCenterSyncRec) GetConnectionPool() *sqlitex.Pool {
	return o.dbPool
}

// Gets the name of the SQLite DB pool for logging connections
func (o *VCenterSyncRec) GetConnectionLoggerPool() *sqlitex.Pool {
	return o.dbPoolLog
}

// This map is used to prevent concurrent sync of the same environment by more than one process
//
var syncLockMap = syncrefreshlock.New()

// Initiates data sync on the pre-defined VCenter connection.  This could take a long time if there are a lot of records
// to process in VCenter.
//
var startLock sync.Mutex
func (o *VCenterSyncRec) StartDataSyncInBackground(ctx context.Context) (int64, error) {
	// Lock the entire sync startup to prevent race condition if two or more go routines try to start sync on the same
	// environment at the same time but only one go routine will be able to start
	defer startLock.Unlock()
	startLock.Lock()

	// Ensure schema is created first.  This has no effect if the schema already exists.
	if errSchema := dbutil.CreateSchema(o.GetName(), o.dbPool); errSchema != nil {
		// Fatal
		return -1, errors.New(fmt.Sprintf("Could not initialize DB schema because of error: %v", errSchema))
	}

	logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("Re-syncing Hypervisor records into SQLite for VCenter connection \"%s\"", o.GetName())))

	if lockRecord := syncLockMap.Exists(o.GetName()); lockRecord != nil {
		// Some other go-routine process is currently syncing this environment
		var errMessage = fmt.Sprintf("Cannot start sync because another process with environment lock ID '%s' is currently syncing this environment", lockRecord.GenerateID())

		return -1, errors.New(errMessage)
	} else {
		// No other go-routine process is syncing so lock this environment
		var lockStruct = syncLockMap.AddLock(o.GetName(), ctx.Value(constants.CTX_KEY_AUTH_USERNAME).(string))

		if syncHistoryId, errHistoryRec := o.createDbSyncRecordEntry(ctx); errHistoryRec != nil {
			// Fatal
			return -1, errors.New(fmt.Sprintf("Could not start sync because could not create sync history record in database because of error: %v", errHistoryRec))
		} else {
			logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("SyncRecord ID: %d created.  Starting data synchronization run for environment '%s'.", syncHistoryId, o.vsphereConnection.Name)))

			if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, o.vsphereConnection); errLogin != nil {
				// Fatal

				// Wrap up sync with status record updates and mark SyncHistory DB record as failed
				o.CreateDbSyncLogEntry(syncHistoryId, errLogin.Error(), "", "")
				o.GenerateSyncLogMsg(errLogin.Error())
				o.UpdateSyncCompletion(syncHistoryId, "FAILED", 0)

				// Update the UI with error via error object
				return -1, errors.New(fmt.Sprintf("Could not log into VCenter connection named '%s' at %s because of error: %v", o.vsphereConnection.Name, o.vsphereConnection.Url, errLogin))
			} else {
				// Run logic in background.  Caller can use the syncHistoryId to check on progress messages directly in
				// the database.
				go o.syncData(ctx, vcSoapConnection, syncHistoryId, lockStruct)
			}

			// Set the current syncHistory ID
			o.activeSyncHistoryId = &syncHistoryId

			// No errors if this point is reached. Give the ID of the sync history DB record which will have messages
			// and sync status attached to it
			return syncHistoryId, nil
		}
	}
}

// Call this to update and "finalize" a SyncHistory record's completion time and completion values
//
func (o *VCenterSyncRec) UpdateSyncCompletion(syncHistoryId int64, status string, nbrRecordsSync int64) error {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("UPDATE SyncHistory SET status = ?, end_time_ms = ?, records_processed = ? WHERE id = ?"); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not update SyncHistory record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindText(bindIncrementor(), status)
		stmt.BindInt64(bindIncrementor(), util.CurrentTimeMillis()) // Set completion time to UTC time right now on app server
		stmt.BindInt64(bindIncrementor(), nbrRecordsSync)
		stmt.BindInt64(bindIncrementor(), syncHistoryId)

		if _, errStmt := stmt.Step(); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not update SyncHistory record ID %d because of database statement execution error: %v", syncHistoryId, errStmt))
		} else {
			logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("SyncHistory record ID: %d updated successfully", syncHistoryId)))
		}
	}

	return nil
}

// Call this after a VCenter VM move/migration to update SQLite with the VM's new location
//
func (o *VCenterSyncRec) UpdateVmParentHypervisor(vmPath string, hypervisorPath string) error {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("UPDATE VirtualMachine SET host = ? WHERE path = ?"); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not update VM SQLite cache DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindText(bindIncrementor(), hypervisorPath)
		stmt.BindText(bindIncrementor(), vmPath)

		if _, errStmt := stmt.Step(); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not update VirtualMachine record with path: '%s' because of database statement execution error: %v", vmPath, errStmt))
		} else {
			logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("VirtualMachine record with path: '%s' updated successfully", vmPath)))
		}
	}

	// No error
	return nil
}

// This must be run in the background
//
// SQLITE TRANSACTION: The loading occurs in a SQLite database transaction.  The "rollback" or "commit" action is
// performed in a defer call based on if an error is set or not.
//
// PARAM: ctx              standard Go context
// PARAM: vcSoapConnection live connection to the VCenter
// PARAM: vcenterSyncRec   vcenter cache object matching the database and is needed to query the VM list for the environment which drives the NFS retrieval process
// PARAM: lockStruct       locking record for this sync and should be removed at the end of the sync even if sync failed
func (o *VCenterSyncRec) syncData(ctx context.Context, vcSoapConnection *vcenterutil.VcSoapConnection, syncHistoryId int64, lockStruct *syncrefreshlock.SyncLockStruct) {
	// Keep these counts for completion stats
	var countHypervisor, countVirtualMachine, countDatastore int64
	var completionStatus = constants.DATA_SYNC_STATUS_COMPLETE

	// Ensure the SyncRecord is always updated regardless of any runtime errors
	defer func() {
		// Update the SyncHistory record with the completion status -- either COMPLETE or INCOMPLETE
		if errUpdateSyncCompletion := o.UpdateSyncCompletion(syncHistoryId, completionStatus, countHypervisor+countVirtualMachine); errUpdateSyncCompletion != nil {
			// Could not update the SyncHistory record.  Not fatal but warn as it represents some system instability
			logger.Log.Error(errUpdateSyncCompletion, o.GenerateSyncLogMsg(fmt.Sprintf("WARN: %v", errUpdateSyncCompletion)))
		}

		// Clear active sync since it has ended either successfully or with error
		o.activeSyncHistoryId = nil
	}()

	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Set the context with values which enable downstream components insert child log entries to the database
	// SyncLogEntry table & linked to the parent SyncHistory record.
	ctx = context.WithValue(ctx, constants.CTX_KEY_SYNC_SYNCHISTORYID, syncHistoryId)
	ctx = context.WithValue(ctx, constants.CTX_KEY_SYNC_VCENTERSYNC, o)

	// Initialize Crawshaw SQLite "SavePoint" and deer its completion function.  The completion function will
	// COMMIT or ROLLBACK the transaction depending on if the error is null or not at function execution time.
	var errRollback error = nil
	defer sqlitex.Save(connection)(&errRollback)
	logSyncHistoryInfo(ctx, "Created SQLite transaction savepoint")

	// Panic handler -- rollsback the transaction before returning from this function should a panic occur
	defer func() {
		if r := recover(); r != nil {
			// Panic occurred -- log it and rollback DB transaction
			var errMsg = ""
			errMsg = fmt.Sprintf("DB Sync process generated a panic with message: %v", r)
			errRollback = errors.New(errMsg)
			logSyncHistoryError(ctx, fmt.Sprintf("Fatal panic error during DB Sync process.  All DB statements in transaction will be rolled back.  Error was: %s", errRollback.Error()))
			completionStatus = constants.DATA_SYNC_STATUS_INCOMPLETE
		}
	}()

	defer func() {
		// Unlock environment at function completion regardless of sync success or failure otherwise no other process
		// will be able to sync this environment unless the Go server process is restarted
		var logMessage = fmt.Sprintf("Removing '%s' sync lock record: %s", o.GetName(), lockStruct.GenerateID())
		logger.Log.Info(logMessage)   // Low level log
		logSyncHistoryInfo(ctx, logMessage)  // Log to sync history record too

		syncLockMap.DeleteLock(o.GetName())
	}()

	// Periodically check the sync run time and send out warning email if current running time exceeds thresholds
	// This will run in a separate background go routine and provide the necessary checks.  Go routine will be
	// canceled via context
	var syncCompleteFlag = false
	ctx = context.WithValue(ctx, constants.CTX_CANCEL_FLAG_01, &syncCompleteFlag) // store *pointer* to the boolean so that main loop can change its value and value will be visible to the Go routine as well
	go o.checkSyncDelayAndMail(ctx, connection)

	// Raise flag that sync is complete.  All background go-routines looping will eventually quit normally after reading
	// the flag's value.
	defer func() {
		syncCompleteFlag = true
	}()

	//
	// Clear all tables first (within the transaction)
	//
	logSyncHistoryInfo(ctx, "Clearing all tables in preparation for full data refresh...")
	var tableNames = []string {"HypervisorTag", "HypervisorDatastore", "VirtualMachineDatastore", "HypervisorNetwork", "VirtualMachineNetwork", "Hypervisor", "VirtualMachine", "DatastoreHypervisorRefs", "DatastoreVirtualMachineRefs", "Datastore"}
	if errClearTables := dbutil.ClearTables(connection, tableNames...); errClearTables != nil {
		// Fatal
		logSyncHistoryError(ctx, errClearTables.Error())

		// Causes SQLite rollback via its completion function
		errRollback = errClearTables
	} else {
		logSyncHistoryInfo(ctx, fmt.Sprintf("(%s) Successfully cleared all rows from tables: %s'", o.GetName(), strings.Join(tableNames, ", ")))

		// Load Hypervisors records -- just use the same function that Dora uses to load Hypervisor records from VCenter
		// NOTE: the blank regexp values "" will match everything
		//
		if outputChannel, errList := ListHypervisorRecords(ctx, "", "", false, false, vcSoapConnection); errList != nil {
			// Fatal  TODO: Need to implement retry logic here as it has failed with network timeouts before
			o.CreateDbSyncLogEntry(syncHistoryId, fmt.Sprintf("Could not sync Hypervisor records because of error: %v", errList), "", "")

			// Set SQLite error object which causes transaction ROLLBACK in the deferral
			errRollback = errList

			// Set completion status to update the SyncHistory record's final status accordingly
			completionStatus = constants.DATA_SYNC_STATUS_INCOMPLETE
		} else {
			// VCenter connection opened.  Now load the VCenter records into SQLite appropriate database tables.
			//
			for {
				// Wait for VCenter records on the channel and process them as they arrive
				var hypervisorResponse = <-outputChannel

				if hypervisorResponse == nil {
					// End of channel indicated so stop
					//
					break
				} else {
					// Upsert the record into SQLite
					//
					countHypervisor++
					if errUpsert := o.UpsertHypervisorRecord(connection, hypervisorResponse); errUpsert != nil {
						// Not fatal but log this hypervisor record could not be upserted into the database
						var infoMsg = fmt.Sprintf("Could not complete upsert of Hypervisor records into SQLite because of error: %v", errUpsert)
						o.CreateDbSyncLogEntry(syncHistoryId, infoMsg, "", "")
						logger.Log.Info(o.GenerateSyncLogMsg(infoMsg))
					} else {
						// Do not log individual upsert messages to DB SyncLogEntry table because there are too many of these messages
						logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("Successful upsert of Hypervisor record : %s", hypervisorResponse.GetPath())))
					}
				}
			}
		}

		// Load VirtualMachine records.  Use blank regexp filter matches which will sync *everything*.
		logger.Log.Info(fmt.Sprintf("Re-syncing VirtualMachine records into SQLite for VCenter connection \"%s\"", o.GetName()))
		if outputChannel, errList := ListVirtualMachineRecords(context.Background(), "", "", false, false, vcSoapConnection); errList != nil {
			// Fatal
			var infoMsg = fmt.Sprintf("Could not sync VirtualMachine records because of error: %v", errList)
			o.CreateDbSyncLogEntry(syncHistoryId, infoMsg, "", "")
			logger.Log.Info(o.GenerateSyncLogMsg(infoMsg))

			// Set SQLite error object which causes transaction ROLLBACK in the deferral
			errRollback = errList

			// Set completion status to update the SyncHistory record's final status accordingly
			completionStatus = constants.DATA_SYNC_STATUS_INCOMPLETE
		} else {
			// VCenter connection opened.  Now load the VCenter records into SQLite appropriate database tables.
			//
			for {
				// Wait for VCenter records on the channel and process them as they arrive
				var virtualMachineResponse = <-outputChannel

				if virtualMachineResponse == nil {
					// End of channel indicated so stop
					//
					break
				} else {
					// Upsert the record into SQLite
					//
					countVirtualMachine++
					if errUpsert := o.UpsertVirtualMachineRecord(connection, virtualMachineResponse); errUpsert != nil {
						// Not fatal but log this virtual machine record could not be upserted into the database
						var infoMsg = fmt.Sprintf("Could not complete upsert of Virtual Machine records into SQLite because of error: %v", errUpsert)
						o.CreateDbSyncLogEntry(syncHistoryId, infoMsg, "", "")
						logger.Log.Info(o.GenerateSyncLogMsg(infoMsg))
					} else {
						// Do not log individual upsert messages to DB SyncLogEntry table because there are too many of these messages
						logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("Successful upsert of Virtual Machine record : %s", virtualMachineResponse.GetPath())))
					}
				}
			}
		}

		// Sync Datastore records and its child table records
		//
		logger.Log.Info(fmt.Sprintf("Re-syncing Datastore records into SQLite for VCenter connection \"%s\"", o.GetName()))
		if outputChannel, errList := ListDatastoreRecords(context.Background(), "", "", false, false, vcSoapConnection); errList != nil {
			// Fatal
			var infoMsg = fmt.Sprintf("Could not sync Datastore records because of error: %v", errList)
			o.CreateDbSyncLogEntry(syncHistoryId, infoMsg, "", "")
			logger.Log.Info(o.GenerateSyncLogMsg(infoMsg))

			// Set SQLite error object which causes transaction ROLLBACK in the deferral
			errRollback = errList

			// Set completion status to update the SyncHistory record's final status accordingly
			completionStatus = constants.DATA_SYNC_STATUS_INCOMPLETE
		} else {
			// VCenter connection opened.  Now load the VCenter records into SQLite appropriate database tables.
			//
			for {
				// Wait for VCenter records on the channel and process them as they arrive
				var datastoreResponse = <-outputChannel

				if datastoreResponse == nil {
					// End of channel indicated so stop
					//
					break
				} else {
					// Upsert the record into SQLite
					//
					countDatastore++
					if errUpsert := o.UpsertDatastoreRecord(connection, datastoreResponse); errUpsert != nil {
						// Not fatal but log this Datastore record could not be upserted into the database
						var infoMsg = fmt.Sprintf("Could not complete upsert of Datastore records into SQLite because of error: %v", errUpsert)
						o.CreateDbSyncLogEntry(syncHistoryId, infoMsg, "", "")
						logger.Log.Info(o.GenerateSyncLogMsg(infoMsg))
					} else {
						// Do not log individual upsert messages to DB SyncLogEntry table because there are too many of these messages
						logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("Successful upsert of Datastore record : %s", datastoreResponse.GetName())))
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// NOTE: the deferred functions above will close the connection and finalize the SavePoint make permanent //
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
}

// Periodically checks this current run's duration and compares against the average duration of last few runs to send
// email if current run is exceeding pre-configured threshold of time
//
const WATCHER_POLLING_INTERVAL = 10 * time.Second
func (o *VCenterSyncRec) checkSyncDelayAndMail(ctx context.Context, connection *sqlite.Conn) {
	var startTime = time.Now()
	var mailer = mail.New(config.Config.GetSmtpServer())

	// Periodically check the sync run time and send out warning email if current running time exceeds thresholds
	if avgDurationSyncHistory, errAvgSyncHistory := dbutil.GetAverageSyncSecs(config.Config.GetSyncWarnHistoryRange(), "SyncHistory", connection); errAvgSyncHistory != nil {
		// Sync history average duration could not be looked up
		// Not fatal to run but log as warning
		logger.Log.Info(fmt.Sprintf("WARN: Could not look up average sync duration for environments %s because of error: %v", o.GetName(), errAvgSyncHistory))
	} else {
		logger.Log.Info(fmt.Sprintf("Time duration watcher starting for VCenter sync on environment '%s' (polling interval = %d sec, average sync duration = %.2f, threshold multiplier = %.2f).  Will watch sync run and warn if it's time duration exceeds previous average runs durations.", o.GetName(), WATCHER_POLLING_INTERVAL / time.Second, avgDurationSyncHistory, config.Config.GetSyncWarnThreshold()))

		// Keep checking runtime duration until the cancel flag in the Go context has been raised externally by the
		// main loop
		for {
			if (*ctx.Value(constants.CTX_CANCEL_FLAG_01).(*bool)) {
				// External loop has signaled sync has completed so no need to continue checking
				//
				logger.Log.Info("Time duration watching for VCenter sync terminating normally since completion flag was raised")

				break
			} else {
				// Main sync still running so continue monitoring
				//
				var currentDurationSec = float64(time.Now().Unix() - startTime.Unix())
				var warnThresholdSecs = avgDurationSyncHistory * config.Config.GetSyncWarnThreshold()
				if currentDurationSec > warnThresholdSecs {
					// Run time of current sync has exceeded the threshold limit so send out warning email
					//

					// Email that current sync run is taking longer than configured threshold
					var message = fmt.Sprintf("VCenter sync for environment %s is taking longer than usual and it could be stalled.  Past runs average duration: %.1f secs, Warn threshold (secs): %.1f, Current run duration (secs): %.1f", o.GetName(), avgDurationSyncHistory, warnThresholdSecs, currentDurationSec)
					var errMail = mailer.Send(ctx, config.Config.GetSyncWarnFromAddress(), strings.Split(config.Config.GetSyncWarnRecipients(), ","), mail.CreateMessageBasic("Dora VCenter Sync Duration Warning", message))

					if errMail != nil {
						logger.Log.Info(fmt.Sprintf("WARN: VCenter sync warning email sending failed to recipients '%s' with error: %v", config.Config.GetSyncWarnRecipients(), errMail))
					} else {
						logger.Log.Info(fmt.Sprintf("VCenter sync alert email sent successfully to recipients: %s ", config.Config.GetSyncWarnRecipients()))
					}

					// Log important event to the sync log & console log as well
					logSyncHistoryWarn(ctx, message)
					logger.Log.Info(message)

					// One alert email per incident is sufficient so quit checking after sending
					break
				}

				// Sleep for a bit and check again
				time.Sleep(WATCHER_POLLING_INTERVAL)
			}
		}
	}
}

// Queries the SQLite Virtual Machine table for the VMs by their VCenter path.  Useful for "auto-scanning" operations
// like SSH exec to retrieve host config like NFS mounts.
//
// PARAM:  includePoweredOff   set to true to include powered off VMs and false to exclude them
func (o *VCenterSyncRec) ListAllVmPath(includePoweredOff bool) ([]string, error) {
	var vcPaths []string
	var responseChannel = make(chan *api.VirtualMachineResponse)
	if errVmSearch := o.VirtualMachineRegexpSearch(regexp.MustCompile(""), regexp.MustCompile(""), responseChannel); errVmSearch != nil {
		// Fatal
		return nil, errVmSearch
	} else {
		// Collect the stream results into an array
		for {
			var vmResponseRecord = <-responseChannel

			if vmResponseRecord == nil {
				// Stream completed
				break
			} else {
				if !util.IsBlank(vmResponseRecord.GetPath()) {
					if includePoweredOff || strings.EqualFold(strings.TrimSpace(vmResponseRecord.State), "poweredOn") {
						// Append the result to the array
						vcPaths = append(vcPaths, vmResponseRecord.GetPath())
					} else {
						logger.Log.Info(fmt.Sprintf("Not including %s because host is powered off", vmResponseRecord.GetPath()))
					}
				}
			}
		}
	}

	// No error if this point reached
	return vcPaths, nil
}

// Create a new sync record in the SyncHistory SQLite database table.  The ID of the new record is returned.
// Use this method when starting a new sync task whether it be for auto or manual sync run.
//
func (o *VCenterSyncRec) createDbSyncRecordEntry(ctx context.Context) (int64, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(ctx)

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("INSERT INTO SyncHistory (environment_name, status, initiated_by, start_time_ms, end_time_ms, records_processed) VALUES (?, ?, ?, ?, ?, ?)"); errStmt != nil {
		// Fatal
		return -1, errors.New(fmt.Sprintf("Could not create SyncHistory record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindText(bindIncrementor(), o.vsphereConnection.Name)
		stmt.BindText(bindIncrementor(), constants.DATA_SYNC_STATUS_IN_PROGRESS)
		stmt.BindText(bindIncrementor(), util.ExtractUser(ctx))
		stmt.BindInt64(bindIncrementor(), util.CurrentTimeMillis())
		stmt.BindNull(bindIncrementor())     // The sync "end time " is null which indicates it is still in progress -- it will be populated with the actual end date when the sync completes
		stmt.BindInt64(bindIncrementor(), 0) // Initially, 0 records processed

		if _, errStep := stmt.Step(); errStep != nil {
			// Fatal
			return -1, errors.New(fmt.Sprintf("Could not execute SQL statement because of error: %v", errStep))
		} else {
			// Expecting exactly one row to be returned
			// INSERT completed successfully
			return connection.LastInsertRowID(), nil
		}
	}
}

// Create a new sync log event record as a child of the given parent SyncHistory record
//
func (o *VCenterSyncRec) CreateDbSyncLogEntry(parentSyncHistoryId int64, msgErr, msgWarn, msgInfo string) (int64, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.GetConnectionLoggerPool().Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.GetConnectionLoggerPool().Put(connection)

	if stmt, errStmt := connection.Prepare("INSERT INTO SyncLogEntry VALUES (?, ?, ?, ?, ?)"); errStmt != nil {
		// Fatal
		return -1, errors.New(o.GenerateSyncLogMsg(fmt.Sprintf("Could not create SyncLogEntry record because of DB statement creation error: %v", errStmt)))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindInt64(bindIncrementor(), parentSyncHistoryId)
		stmt.BindInt64(bindIncrementor(), util.CurrentTimeMillis()) // Current time millis UTC on app server

		if !util.IsBlank(msgErr) {
			stmt.BindText(bindIncrementor(), msgErr)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if !util.IsBlank(msgWarn) {
			stmt.BindText(bindIncrementor(), msgWarn)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if !util.IsBlank(msgInfo) {
			stmt.BindText(bindIncrementor(), msgInfo)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if _, errStep := stmt.Step(); errStep != nil {
			// Fatal
			return -1, errors.New(fmt.Sprintf("Could not execute SQL statement because of error: %v", errStep))
		} else {
			// Expecting exactly one row to be returned
			// INSERT completed successfully
			return connection.LastInsertRowID(), nil
		}
	}
}

// Inserts a VM Storage record to the database
//
func (o *VCenterSyncRec) UploadVmStorage(ctx context.Context, connection *sqlite.Conn, uploadRecords []*api.VmStorageUploadRec, append bool) error {
	const TABLE_NAME = "UserNotesVmStorage";

	if !append {
		if errClearTables := dbutil.ClearTables(connection, TABLE_NAME); errClearTables != nil {
			// Fatal
			return errClearTables
		}
	}

	var sql = fmt.Sprintf("INSERT INTO %s(name, size_tb) VALUES ($name, $size_tb)", TABLE_NAME)
	if stmtInsert, errStmt := connection.Prepare(sql); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not insert into table %s because of database statement error: %v", TABLE_NAME, errStmt))
	} else {
		for _, rec := range uploadRecords {
			stmtInsert.SetText("$name", rec.GetName())
			stmtInsert.SetFloat("$size_tb", rec.GetSizeTb())
			stmtInsert.Step()
			stmtInsert.Reset()
		}

		stmtInsert.Finalize()
	}

	// No errors if this point is reached
	return nil
}

// Inserts IP Range record(s) to the database
//
func (o *VCenterSyncRec) UploadIpStorage(ctx context.Context, connection *sqlite.Conn, uploadRecords []*api.IpRangeRec, append bool) error {
	const TABLE_NAME = "UserNotesIpRange";

	if !append {
		if errClearTables := dbutil.ClearTables(connection, TABLE_NAME); errClearTables != nil {
			// Fatal
			return errClearTables
		}
	}

	var sql = fmt.Sprintf("INSERT INTO %s(name, ip_range, vlan, user_notes) VALUES ($name, $ip_range, $vlan, $user_notes)", TABLE_NAME)
	if stmtInsert, errStmt := connection.Prepare(sql); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not insert into table %s because of database statement error: %v", TABLE_NAME, errStmt))
	} else {
		for _, rec := range uploadRecords {
			stmtInsert.SetText("$name", rec.GetName())
			stmtInsert.SetText("$ip_range", rec.GetIpRange())
			stmtInsert.SetText("$vlan", rec.GetVlan())
			stmtInsert.SetText("$user_notes", rec.GetNotes())
			stmtInsert.Step()
			stmtInsert.Reset()
		}

		stmtInsert.Finalize()
	}

	// No errors if this point is reached
	return nil
}

// Inserts Storage Summary record(s) to the database
//
func (o *VCenterSyncRec) UploadStorageSummary(ctx context.Context, connection *sqlite.Conn, uploadRecords []*api.StorageSummaryRec, append bool) error {
	const TABLE_NAME = "UserNotesStorageSummary";

	if !append {
		// Delete records first if NOT in append mode
		if errClearTables := dbutil.ClearTables(connection, TABLE_NAME); errClearTables != nil {
			// Fatal
			return errClearTables
		}
	}

	var sql = fmt.Sprintf("INSERT INTO %s(name, cidr, ports, username, password) VALUES ($name, $cidr, $ports, $username, $password)", TABLE_NAME)
	if stmtInsert, errStmt := connection.Prepare(sql); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not insert into table %s because of database statement error: %v", TABLE_NAME, errStmt))
	} else {
		for _, rec := range uploadRecords {
			stmtInsert.SetText("$name", rec.GetName())
			stmtInsert.SetText("$cidr", rec.GetCidr())
			stmtInsert.SetText("$ports", rec.GetPorts())
			stmtInsert.SetText("$username", rec.GetUsername())
			stmtInsert.SetText("$password", rec.GetPassword())
			stmtInsert.Step()
			stmtInsert.Reset()
		}

		stmtInsert.Finalize()
	}

	// No errors if this point is reached
	return nil
}

// Upserts a Hypervisor record into the database.  Used when synchronizing with VCenter records.
//
func (o *VCenterSyncRec) UpsertHypervisorRecord(connection *sqlite.Conn, hypervisorRec *api.HypervisorResponse) error {
	//
	// IMPLEMENTATION NOTE: SQLite automatically does "upsert" because its INSERT command will overwrite any existing
	// record.  That is, SQLite does *not* error out like other RDBMS if inserting a record that has the same PRIMARY KEY
	// value of an existing record.  So its *not* necessary to do a DELETE first before INSERT with SQLite.
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO Hypervisor VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not upsert hypervisor record %v because of database execution error: %v", hypervisorRec, errStmt))
	} else {
		// Translate VSphere record into SQLite row
		//
		var bindIncrementor = sqlite.BindIncrementor()
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetName())
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetPath())
		stmtInsert.BindInt64(bindIncrementor(), int64(hypervisorRec.GetCpuCount()))
		stmtInsert.BindInt64(bindIncrementor(), hypervisorRec.GetMemory_MB())
		stmtInsert.BindInt64(bindIncrementor(), int64(hypervisorRec.GetHdd_GB()))
		stmtInsert.BindInt64(bindIncrementor(), int64(hypervisorRec.GetVmCount()))
		stmtInsert.BindFloat(bindIncrementor(), float64(hypervisorRec.GetUsageMB()))
		stmtInsert.BindFloat(bindIncrementor(), float64(hypervisorRec.GetUsageCPU()))
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetUptime()) // SQLite doesn't have a DATE type
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetState())
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetStatus())
		stmtInsert.BindInt64(bindIncrementor(), BoolToInt(hypervisorRec.GetVmotion())) // SQLite doesn't have BOOL type -- just use 0 or 1
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetNetPortId())
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetNetSystemName())
		stmtInsert.BindInt64(bindIncrementor(), time.Now().UnixNano()/1000000)
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetRack())
		stmtInsert.BindText(bindIncrementor(), hypervisorRec.GetManagedObjRef())
		stmtInsert.Step()
		stmtInsert.Reset()
		stmtInsert.Finalize()
	}

	// Upsert the Hypervisor child "Network" records if any
	if hypervisorRec.GetNetworks() != nil {
		if stmtInsert, errStmt := connection.Prepare(`INSERT INTO HypervisorNetwork VALUES ($parentHypervisorPath, $networkName)`); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not upsert Hypervisor's child Network record %v because of database execution error: %v", hypervisorRec, errStmt))
		} else {
			for _, networkName := range hypervisorRec.GetNetworks() {
				stmtInsert.SetText("$parentHypervisorPath", hypervisorRec.GetPath())
				stmtInsert.SetText("$networkName", networkName)
				stmtInsert.Step()
				stmtInsert.Reset()
			}

			stmtInsert.Finalize()
		}
	}


	// Upsert the Hypervisor child "Datastore" records if any
	if hypervisorRec.GetDatastores() != nil {
		if stmtInsert, errStmt := connection.Prepare(`INSERT INTO HypervisorDatastore VALUES ($parentHypervisorPath, $datastorePath, $datastoreUrl)`); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not upsert Hypervisor's child Datastore record %v because of database execution error: %v", hypervisorRec, errStmt))
		} else {
			for _, datastoreRec := range hypervisorRec.GetDatastores() {
				stmtInsert.SetText("$parentHypervisorPath", hypervisorRec.GetPath())
				stmtInsert.SetText("$datastorePath", datastoreRec.GetDatastorePath())
				stmtInsert.SetText("$datastoreUrl", datastoreRec.GetDatastoreUrl())
				stmtInsert.Step()
				stmtInsert.Reset()
			}

			stmtInsert.Finalize()
		}
	}

	// Upsert the Hypervisor child "HypervisorTag" records if any
	if hypervisorRec.GetTags() != nil {
		if stmtInsert, errStmt := connection.Prepare(`INSERT INTO HypervisorTag VALUES ($parentHypervisorPath, $tag)`); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not upsert Hypervisor's child tag record %v because of database execution error: %v", hypervisorRec, errStmt))
		} else {
			for _, tag := range hypervisorRec.GetTags() {
				stmtInsert.SetText("$parentHypervisorPath", hypervisorRec.GetPath())
				stmtInsert.SetText("$tag", tag)
				stmtInsert.Step()
				stmtInsert.Reset()
			}

			stmtInsert.Finalize()
		}
	}

	return nil
}

// Upserts a VirtualMachine record into the database.  Used when synchronizing with VCenter records.
//
func (o *VCenterSyncRec) UpsertVirtualMachineRecord(connection *sqlite.Conn, virtualMachineRec *api.VirtualMachineResponse) error {
	//
	// IMPLEMENTATION NOTE: SQLite automatically does "upsert" because its INSERT command will overwrite any existing
	// record.  That is, SQLite does *not* error out like other RDBMS if inserting a record that has the same PRIMARY KEY
	// value of an existing record.  So its *not* necessary to do a DELETE first before INSERT with SQLite.
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO VirtualMachine VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not upsert VirtualMachine record %v because of database execution error: %v", virtualMachineRec, errStmt))
	} else {
		// Translate VSphere record into SQLite row
		//
		var bindIncrementor = sqlite.BindIncrementor()
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetName())
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetPath())
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetHost())
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetOs())
		stmtInsert.BindInt64(bindIncrementor(), int64(virtualMachineRec.GetCpus()))
		stmtInsert.BindInt64(bindIncrementor(), int64(virtualMachineRec.GetTotalCpuMhz()))
		stmtInsert.BindInt64(bindIncrementor(), int64(virtualMachineRec.GetHddAllocatedGb()))
		stmtInsert.BindFloat(bindIncrementor(), float64(virtualMachineRec.GetMemMb()))
		stmtInsert.BindInt64(bindIncrementor(), BoolToInt(virtualMachineRec.GetCpuHotAddEnabled())) // SQLite doesn't have BOOL type -- just use 0 or 1
		stmtInsert.BindInt64(bindIncrementor(), BoolToInt(virtualMachineRec.GetMemHotAddEnabled())) // SQLite doesn't have BOOL type -- just use 0 or 1
		stmtInsert.BindInt64(bindIncrementor(), int64(virtualMachineRec.GetUptimeSec()))            // SQLite doesn't have a DATE type
		stmtInsert.BindFloat(bindIncrementor(), virtualMachineRec.GetMemUsagePerc())
		stmtInsert.BindFloat(bindIncrementor(), virtualMachineRec.GetCpuUsagePerc())
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetState())
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetOverallStatus())
		stmtInsert.BindInt64(bindIncrementor(), time.Now().UnixNano()/1000000)
		stmtInsert.BindText(bindIncrementor(), virtualMachineRec.GetManagedObjRef())
		stmtInsert.Step()
		stmtInsert.Reset()
		stmtInsert.Finalize()
	}

	// Upsert the VirtualMachine child "Datastore" records if any
	if virtualMachineRec.GetDatastores() != nil {
		if stmtInsert, errStmt := connection.Prepare(`INSERT INTO VirtualMachineDatastore VALUES ($parentVirtualMachinePath, $datastorePath, $datastoreUrl)`); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not upsert VirtualMachine %s child Datastore record because of database execution error: %v", virtualMachineRec.GetName(), errStmt))
		} else {
			for _, datastoreRec := range virtualMachineRec.GetDatastores() {
				stmtInsert.SetText("$parentVirtualMachinePath", virtualMachineRec.GetPath())
				stmtInsert.SetText("$datastorePath", datastoreRec.GetDatastorePath())
				stmtInsert.SetText("$datastoreUrl", datastoreRec.GetDatastoreUrl())
				stmtInsert.Step()
				stmtInsert.Reset()
			}

			stmtInsert.Finalize()
		}
	}

	// Upsert the VirtualMachine child "Network" records if any
	if virtualMachineRec.GetNetworks() != nil {
		if stmtInsert, errStmt := connection.Prepare(`INSERT INTO VirtualMachineNetwork VALUES ($parentVirtualMachinePath, $path, $networkName, $adapterType, $macAddress, $ipAddresses)`); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not upsert VirtualMachine %s child Network record because of database execution error: %v", virtualMachineRec.GetName(), errStmt))
		} else {
			for _, networkRec := range virtualMachineRec.GetNetworks() {
				stmtInsert.SetText("$parentVirtualMachinePath", virtualMachineRec.GetPath())
				stmtInsert.SetText("$path", networkRec.GetNetworkPath())
				stmtInsert.SetText("$networkName", networkRec.GetNetworkName())
				stmtInsert.SetText("$adapterType", networkRec.GetAdapterType())
				stmtInsert.SetText("$macAddress", networkRec.GetMacAddress())
				stmtInsert.SetText("$ipAddresses", strings.Join(networkRec.GetIpAddresses(), ","))
				stmtInsert.Step()
				stmtInsert.Reset()
			}

			stmtInsert.Finalize()
		}
	}

	return nil
}

// Upserts a Datastore record and associated child records into the database.  Use when synchornizing with VCenter
// records.
//
func (o *VCenterSyncRec) UpsertDatastoreRecord(connection *sqlite.Conn, datastoreRec *api.DatastoreResponse) error {
	//
	// IMPLEMENTATION NOTE: SQLite automatically does "upsert" because its INSERT command will overwrite any existing
	// record.  That is, SQLite does *not* error out like other RDBMS if inserting a record that has the same PRIMARY KEY
	// value of an existing record.  So its *not* necessary to do a DELETE first before INSERT with SQLite.
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO Datastore(name, path, url, managed_obj_ref, capacity_bytes, capacity_remaining_bytes, type) VALUES($name, $path, $url, $managed_obj_ref, $capacity_bytes, $capacity_remaining_bytes, $type)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not upsert Datastore record %v because of database execution error: %v", datastoreRec, errStmt))
	} else {
		// Translate data record into SQLite row
		//
		stmtInsert.SetText("$name", datastoreRec.GetName())
		stmtInsert.SetText("$path", datastoreRec.GetPath())
		stmtInsert.SetText("$url", datastoreRec.GetUrl())
		stmtInsert.SetText("$managed_obj_ref", datastoreRec.GetManagedObjRef())
		stmtInsert.SetInt64("$capacity_bytes", datastoreRec.GetCapacityBytes())
		stmtInsert.SetInt64("$capacity_remaining_bytes", datastoreRec.GetCapacityRemainingBytes())
		stmtInsert.SetText("$type", datastoreRec.GetType())
		stmtInsert.Step()
		stmtInsert.Reset()
		stmtInsert.Finalize()
	}

	// Upsert 0 or more child DatastoreHypervisorRef records
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO DatastoreHypervisorRefs(datastore_managed_obj_ref, hypervisor_managed_obj_ref, mount_point) VALUES($datastore_managed_obj_ref, $hypervisor_managed_obj_ref, $mount_point)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not upsert DatastoreHypervisorRefs records because of database execution error: %v", errStmt))
	} else {
		for _, datastoreHypervisorRef := range datastoreRec.GetHypervisorsRefs() {
			// Translate data record into SQLite row
			//
			stmtInsert.SetText("$datastore_managed_obj_ref", datastoreHypervisorRef.GetDatastoreRef())
			stmtInsert.SetText("$hypervisor_managed_obj_ref", datastoreHypervisorRef.GetHypervisorRef())
			stmtInsert.SetText("$mount_point", datastoreHypervisorRef.GetMountPoint())
			stmtInsert.Step()
			stmtInsert.Reset()
		}

		stmtInsert.Finalize()
	}

	// Upsert 0 or more child DatastoreVirtualMachineRef records
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO DatastoreVirtualMachineRefs(datastore_managed_obj_ref, vm_managed_obj_ref) VALUES($datastore_managed_obj_ref, $vm_managed_obj_ref)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not upsert DatastoreVirtualMachineRefs records because of database execution error: %v", errStmt))
	} else {
		for _, datastoreVirtualMachineRef := range datastoreRec.GetVmRefs() {
			// Translate data record into SQLite row
			//
			stmtInsert.SetText("$datastore_managed_obj_ref", datastoreRec.GetManagedObjRef())
			stmtInsert.SetText("$vm_managed_obj_ref", datastoreVirtualMachineRef)
			stmtInsert.Step()
			stmtInsert.Reset()
		}

		stmtInsert.Finalize()
	}

	// No errors if this point is reached
	return nil
}

// Selects from the Hypervisor table and filters using the Go regex engine.  This operation runs O(n) time (full table
// scan) because a regexp search cannot be indexed.
//
func (o *VCenterSyncRec) HypervisorRegexpSearch(pathRegexp *regexp.Regexp, hostRegexp *regexp.Regexp, outputChannel chan *api.HypervisorResponse) error {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())
	if stmtSelect, errSelect := connection.Prepare("SELECT * FROM Hypervisor ORDER BY name"); errSelect != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Hypervisor database SELECT failed because of error: %v", errSelect))
	} else {
		// Run main search asynchronously. Results will be pushed to the given channel.
		go func(stmt *sqlite.Stmt, channel chan *api.HypervisorResponse) {
			// At the end, returns connection to the DB connection pool otherwise will eventually run out of
			// connections and database operations will appear to hang
			defer o.dbPool.Put(connection)

			// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
			// possible deadlock if caller is the same thread consuming the items off the channel.
			go func() {
				if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
					Name:        constants.DATASOURCE_SQLITE,
					Description: "Generated using fast SQLite database cache",
					LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
				}); errJson != nil {
					// Fatal - unlikely
					logger.Log.Error(errJson, "Could not marshal Datasource record")
				} else {
					channel <- &api.HypervisorResponse{
						UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
					}
				}
			}()

			for {
				// Step through the cursor and stop when completed
				//
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.HypervisorResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query on hypervisor table: %v", errStep),
					}

					// Indicate stream is complete
					channel <- nil

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var vCenterPath = stmt.GetText("path")
						var hostname = stmt.GetText("name")
						if pathRegexp.Match([]byte(vCenterPath)) && hostRegexp.Match([]byte(hostname)) {
							var hypervisorResponse = api.HypervisorResponse{
								Name:                 hostname,
								Path:                 vCenterPath,
								Cluster:              "TODO: Cluster",
								CpuCount:             int32(stmt.GetInt64("cpus")), // SQLite only has int64 int type
								Memory_MB:            stmt.GetInt64("mem_mb"),
								VmCount:              int32(stmt.GetInt64("vm_count")), // SQLite only has int64 int type
								UsageMB:              float32(stmt.GetFloat("mem_usage_mb")),
								UsageCPU:             float32(stmt.GetFloat("cpu_usage_ghz")),
								Uptime:               stmt.GetText("uptime"),
								State:                stmt.GetText("state"),
								Status:               stmt.GetText("status"),
								Vmotion:              IntToBool(stmt.GetInt64("vmotion_enabled")),
								NetPortId:            stmt.GetText("net_port_id"),
								NetSystemName:        stmt.GetText("net_system_name"),
								UiControlIsPreview:   false,
								Networks:             []string{},   // Will be populated with separate DB call below
								Datastores:           []*api.Datastore{}, // Will be populated with separate DB call below
								Rack:                 stmt.GetText("rack"),
								ManagedObjRef:        stmt.GetText("managed_obj_ref"),
							}

							// Add the hypervisor's child Network records retrieved from a separate DB call
							if networks, errNetworks := o.GetHypervisorNetworks(vCenterPath); errNetworks != nil {
								// Fatal
								// Send error record
								channel <- &api.HypervisorResponse{
									UiMsgError: fmt.Sprintf("Fatal encountered while executing database query on child HypervisorNetwork table: %v", errStep),
								}

								// Indicate stream is complete
								channel <- nil
							} else {
								hypervisorResponse.Networks = append(hypervisorResponse.Networks, networks...)
							}

							// Add the hypervisor's child Datastore records retrieved from a separate DB call
							if datastores, errDatastores := o.GetHypervisorDatastores(vCenterPath); errDatastores != nil {
								// Fatal
								// Send error record
								channel <- &api.HypervisorResponse{
									UiMsgError: fmt.Sprintf("Fatal encountered while executing database query on child HypervisorDatastore table: %v", errStep),
								}

								// Indicate stream is complete
								channel <- nil
							} else {
								for _, datastore := range datastores {
									// Convert internal datastore record to external protobuf record
									hypervisorResponse.Datastores = append(hypervisorResponse.Datastores, &api.Datastore{
										DatastorePath: datastore.Path,
										DatastoreUrl:  datastore.Url,
									})
								}
							}

							// Send new record for immediate display to user
							channel <- &hypervisorResponse
						}
					} else {
						// No more rows -- end normally and indicate streaming is complete
						channel <- nil
						break
					}
				}
			}
		}(stmtSelect, outputChannel)
	}

	// Returns by go routine will likely still be processing the search and populating the provided channel
	return nil
}

// Selects from the VirtualMachine table and filters using the Go regex engine.  This operation runs O(n) time (full table
// scan) because a regexp search cannot be indexed.
//
// ASYNCHRONOUS OPERATION:
// After some setup, this function runs in a parallel go-routine and will return results in the provided channel.  The
// loading is complete when a "nil" reference is placed into the channel.
//
func (o *VCenterSyncRec) VirtualMachineRegexpSearch(regexPathFilter *regexp.Regexp, regexHostFilter *regexp.Regexp, outputChannel chan *api.VirtualMachineResponse) error {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	if stmtSelect, errSelect := connection.Prepare("SELECT * FROM VirtualMachine ORDER BY name"); errSelect != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Virtual Machine database SELECT failed because of error: %v", errSelect))
	} else {
		// Run main search asynchronously. Results will be pushed to the given channel.
		go func(stmt *sqlite.Stmt, channel chan *api.VirtualMachineResponse) {
			// At the end, returns connection to the DB connection pool otherwise will eventually run out of
			// connections and database operations will appear to hang
			defer o.dbPool.Put(connection)

			// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
			// possible deadlock if caller is the same thread consuming the items off the channel.
			go func() {
				if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
					Name:        constants.DATASOURCE_SQLITE,
					Description: "Generated using fast SQLite database cache",
					LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
				}); errJson != nil {
					// Fatal - unlikely
					logger.Log.Error(errJson, "Could not marshal Datasource record")
				} else {
					channel <- &api.VirtualMachineResponse{
						UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
					}
				}
			}()

			for {
				// Step through the cursor and stop when completed
				//
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.VirtualMachineResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query on VirtualMachine table: %v", errStep),
					}

					// Indicate stream is complete
					channel <- nil

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var vCenterPath = stmt.GetText("path")
						var vmName = stmt.GetText("name")
						var hypervisorPath = stmt.GetText("host")

						if virtualMachineDataStores, errVmDatastores := o.GetVirtualMachineDatastores(vCenterPath); errVmDatastores != nil {
							// Fatal
							// Send error record
							channel <- &api.VirtualMachineResponse{
								UiMsgError: fmt.Sprintf("Fatal encountered while retrieving VirtualMachine datastores for VM %s.  Error: %v", vmName, errStep),
							}

							// Indicate stream is complete
							channel <- nil
						} else {
							if virtualMachineNetworks, errVmNetworks := o.GetVirtualMachineNetworks(vCenterPath); errVmNetworks != nil {
								// Fatal
								// Send error record
								channel <- &api.VirtualMachineResponse{
									UiMsgError: fmt.Sprintf("Fatal encountered while retrieving VirtualMachine networks for VM %s.  Error: %v", vmName, errStep),
								}

								// Indicate stream is complete
								channel <- nil
							} else {
								if regexPathFilter.Match([]byte(vCenterPath)) && regexHostFilter.Match([]byte(vmName)) {
									var virtualMachineResponse = api.VirtualMachineResponse{
										Name:               vmName,
										Path:               vCenterPath,
										Host:               hypervisorPath,
										Os:                 stmt.GetText("os"),
										Cpus:               int32(stmt.GetInt64("cpus")),
										TotalCpuMhz:        int32(stmt.GetInt64("total_cpu_mhz")),
										HddAllocatedGb:     int32(stmt.GetInt64("hdd_allocated_gb")),
										MemMb:              int32(stmt.GetInt64("mem_mb")),
										Networks:           virtualMachineNetworks,
										Datastores:         convertDatastoreList(virtualMachineDataStores),
										CpuHotAddEnabled:   IntToBool(stmt.GetInt64("cpu_hot_add_enabled")),
										MemHotAddEnabled:   IntToBool(stmt.GetInt64("mem_hot_add_enabled")),
										UptimeSec:          int32(stmt.GetInt64("uptime_sec")),
										MemUsagePerc:       stmt.GetFloat("mem_usage_perc"),
										CpuUsagePerc:       stmt.GetFloat("cpu_usage_perc"),
										State:              stmt.GetText("state"),
										OverallStatus:      stmt.GetText("overall_status"),
										UiMsgInfo:          "",
										UiMsgWarning:       "",
										UiMsgError:         "",
										UiControlIsPreview: false,
									}

									// Send new record for immediate display to user
									channel <- &virtualMachineResponse
								}
							}
						}
					} else {
						// No more rows -- end normally and indicate streaming is complete
						channel <- nil
						break
					}
				}
			}
		}(stmtSelect, outputChannel)
	}

	// Returns by go routine will likely still be processing the search and populating the provided channel
	return nil
}

// Asynchronously generates the room report using fast SQLite DB cache.  Runtime report generation data and errors
// will be communicated back throug the channel.  The client will have to process error and data records accordingly.
//
func (o *VCenterSyncRec) GenerateRoomReport(regexSearchPattern string, channel chan *api.RoomResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.RoomResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	if re, errRegExp := regexp.Compile(regexSearchPattern); errRegExp != nil {
		// Fatal
		return errors.New(fmt.Sprintf("RegExp '%s' provided for Room Filter is invalid: %v", regexSearchPattern, errRegExp))
	} else {
		// Run in background go-routine
		go func() {
			// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
			var connection = o.dbPool.Get(context.Background())

			// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
			// run out of connections and database operations will appear to hang
			defer o.dbPool.Put(connection)

			// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
			defer func() { channel <- nil }()

			if stmt, errStmt := connection.Prepare(dbutil.RoomSql); errStmt != nil {
				var errMsg = fmt.Sprintf("Could not create Room report because of DB statement creation error: %v", errStmt)
				logger.Log.Info(errMsg)

				// Send error record through the channel since communication with an asynchronous client can only occur
				// via the given channel
				channel <- &api.RoomResponse{
					UiMsgError: errMsg,
				}
			} else {
				// Used to store records locally for additional room calculations
				var roomRecords = make([]api.RoomResponse, 0)

				for {
					if isRowReturned, errStep := stmt.Step(); errStep != nil {
						// Fatal

						// Send error record
						channel <- &api.RoomResponse{
							UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for room report: %v", errStep),
						}

						// Discontinue processing
						break
					} else {
						if isRowReturned {
							var hvPath = stmt.GetText("path")

							// Match after the search. This is a bit inefficient but simpler code.  Ideally the matched
							// hypervisor names should be populated into a temp DB server-side table and the SQL engine
							// can match there which reduces DB server CPU and I/O load.
							if re.Match([]byte(hvPath)) {
								var roomRecord = api.RoomResponse{
									HypervisorPath: hvPath,
									VmCount:        int32(stmt.GetInt64("vm_count")),
									TotalHddGb:     int32(stmt.GetInt64("hdd_capacity_gb")),
									TotalCpu:       int32(stmt.GetInt64("cpus")),
									TotalMemGb:     int32(stmt.GetInt64("mem_gb")),
									HeadroomCpu:    int32(stmt.GetInt64("headroom_cpu")),
									HeadroomMemGb:  int32(stmt.GetInt64("headroom_mem_gb")),
									HeadroomHddGb:  int32(stmt.GetInt64("headroom_hdd_gb")),
									NumStsgl:       int32(stmt.GetInt64("stsgl_count")),
									NumStvar:       int32(stmt.GetInt64("stvar_count")),
									NumNvrcw:       int32(stmt.GetInt64("nvrcw_count")),
									NumStmmx:       int32(stmt.GetInt64("stmmx_count")),
									GuestVms:       strings.Join(removeFqdns(strings.Split(stmt.GetText("vm_list"), ",")), " "),
								}

								// Updated post-calculated fields that are dependent on other fields
								roomRecord.Severity = calcSeverityLevel(roomRecord.GetHeadroomCpu(), roomRecord.GetHeadroomMemGb(), roomRecord.GetHeadroomHddGb())

								// Send new record through channel for immediate viewing on the frontend/user
								channel <- &roomRecord

								// Also save to internal list for post-calculations like "balance score" calcs below
								roomRecords = append(roomRecords, roomRecord)
							}
						} else {
							// No more rows so end loop normaly
							break
						}
					}
				}

				//
				// POST PROCESS CPU & MEM BALANCE SCORES
				// Each update will re-calculate the "live" running balances.  NOTE these values will change
				// as new data rows are available
				//

				// Update current totals required to calculate the current average for each CPU and MEM allocated
				// amounts
				var cpuTotalAlloc int32
				var memTotalAlloc int32
				for _, room := range roomRecords {
					cpuTotalAlloc += room.TotalCpu - room.GetHeadroomCpu()
					memTotalAlloc += room.GetTotalMemGb() - room.GetHeadroomMemGb()
				}

				// Re-calculate the current scores based on the current averages
				for i, _ := range roomRecords {
					var cpuAllocated = roomRecords[i].TotalCpu - roomRecords[i].GetHeadroomCpu()
					var memAllocated = roomRecords[i].GetTotalMemGb() - roomRecords[i].GetHeadroomMemGb()

					// Update running average and re-score CPU, MEM and total environment "balance score"
					var cpuCurrentAvg = float64(cpuTotalAlloc) / float64(len(roomRecords))
					var memCurrentAvg = float64(memTotalAlloc) / float64(len(roomRecords))

					roomRecords[i].CpuBalanceScore = float32(math.Abs(float64(cpuAllocated)-cpuCurrentAvg) + math.Pow(10, -1*float64(roomRecords[i].GetHeadroomCpu()-3)/4))
					roomRecords[i].MemBalanceScore = float32(math.Abs(float64(memAllocated)-memCurrentAvg) + math.Pow(10, -1*float64(roomRecords[i].GetHeadroomMemGb()-10)/64))

					// Resend updated rows -- UI will replace existing rows with the latest data
					channel <- &roomRecords[i]
				}
			}
		}()
	}

	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}


// Asynchronously generates the Shared Storage report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back through the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateSharedStorageReport(ctx context.Context, regexSharedStorageType *regexp.Regexp, regexSharedStorageName *regexp.Regexp, channel chan *api.SharedStorageReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Shared Storage record")
		} else {
			channel <- &api.SharedStorageReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.SharedStorageReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create Shared Storage report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.SharedStorageReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.SharedStorageReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for Shared Storage report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var sharedStorageType = stmt.GetText("type")
						var sharedStorageName = stmt.GetText("name")

						// Match after the search. This is a bit inefficient but simpler code.  Ideally the matched
						// hypervisor names should be populated into a temp DB server-side table and the SQL engine
						// can match there which reduces DB server CPU and I/O load.
						if regexSharedStorageType.Match([]byte(sharedStorageType)) {
							if regexSharedStorageName.Match([]byte(sharedStorageName)) {
								var sharedStorageRec = api.SharedStorageReportResponse{
									StorageId:            stmt.GetText("storage_id"),
									StorageName:          sharedStorageName,
									StorageType:          sharedStorageType,
									StorageClusterName:   stmt.GetText("cluster_name"),
									StorageCapacityBytes: stmt.GetInt64("capacity_bytes"),
									StorageUsedBytes:     stmt.GetInt64("capacity_used_bytes"),
									UiControlIsPreview:   false,
								}

								// Send new record through channel for immediate viewing on the frontend/user
								channel <- &sharedStorageRec
							}
						}
					} else {
						// No more rows so end loop normally
						break
					}
				}
			}
		}
	}()


	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}

// Asynchronously generates the Datastore report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back through the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateDatastoreReport(ctx context.Context, regexHypervisorPath *regexp.Regexp, regexDatastorePath *regexp.Regexp, channel chan *api.DatastoreReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.DatastoreReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.DatastoreReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create Datastore report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.DatastoreReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.DatastoreReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for Datastore report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var hypervisorPath = stmt.GetText("hypervisor_path")
						var datastorePath = stmt.GetText("datastore_path")

						// Match after the search. This is a bit inefficient but simpler code.  Ideally the matched
						// hypervisor names should be populated into a temp DB server-side table and the SQL engine
						// can match there which reduces DB server CPU and I/O load.
						if regexHypervisorPath.Match([]byte(hypervisorPath)) {
							if regexDatastorePath.Match([]byte(datastorePath)) {
								var datastoreReportRec = api.DatastoreReportResponse{
									Url:                    stmt.GetText("datastore_url"),
									DatastoreMoRef:         stmt.GetText("datastore_managed_obj_ref"),
									DatastoreName:          stmt.GetText("datastore_name"),
									DatastorePath:          datastorePath,
									Type:                   stmt.GetText("datastore_type"),
									CapacityBytes:          stmt.GetInt64("datastore_capacity_bytes"),
									CapacityRemainingBytes: stmt.GetInt64("datastore_capacity_remaining_bytes"),
									HypervisorPath:         stmt.GetText("hypervisor_path"),
									HypervisorMoRef:        stmt.GetText("hypervisor_managed_obj_ref"),
									HypervisorMount:        stmt.GetText("hypervisor_mount_point"),
									VmHostName:             stmt.GetText("virtualmachine_name"),
									UiControlIsPreview:     false,
								}

								// Send new record through channel for immediate viewing on the frontend/user
								channel <- &datastoreReportRec
							}
						}
					} else {
						// No more rows so end loop normaly
						break
					}
				}
			}
		}
	}()


	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}


// Asynchronously generates the NFS report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back throug the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateNfsReport(ctx context.Context, regexNfsHost *regexp.Regexp, regexNfsPath *regexp.Regexp, regexVmHost *regexp.Regexp, regexVmPath *regexp.Regexp, regexVmMount *regexp.Regexp, channel chan *api.NfsReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.NfsReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.NfsReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create NFS report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.NfsReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.NfsReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for NFS report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var nfsHost = stmt.GetText("nfs_host")
						var nfsPath = stmt.GetText("nfs_path")
						var vmHost = stmt.GetText("vm_host")
						var vmPath = stmt.GetText("vm_path")
						var vmMountPoint = stmt.GetText("vm_mount_point")

						// Match after the search. This is a bit inefficient but simpler code.  Ideally the matched
						// hypervisor names should be populated into a temp DB server-side table and the SQL engine
						// can match there which reduces DB server CPU and I/O load.
						if regexNfsHost.Match([]byte(nfsHost)) {
							if regexNfsPath.Match([]byte(nfsPath)) {
								if regexVmHost.Match([]byte(vmHost)) {
									if regexVmPath.Match([]byte(vmPath)) {
										if regexVmMount.Match([]byte(vmMountPoint)) {
											var nfsReportRec = api.NfsReportResponse{
												NfsHost:            nfsHost,
												NfsPath:            nfsPath,
												NfsType:            stmt.GetText("nfs_type"),
												NfsClusterName:     stmt.GetText("nfs_cluster_name"),
												NfsCapacityBytes:   stmt.GetInt64("nfs_capacity_bytes"),
												NfsUsedBytes:       stmt.GetInt64("nfs_used_bytes"),
												NfsIsilonLnn:       stmt.GetInt64("nfs_isilon_lnn"),
												VmHost:             vmHost,
												VmPath:             vmPath,
												VmMountPoint:       vmMountPoint,
												VmMountUser:        stmt.GetText("vm_mount_user"),
												VmMountGroup:       stmt.GetText("vm_mount_group"),
												VmMountPermissions: stmt.GetText("vm_mount_perms"),
												Fstab:              stmt.GetText("fstab"),
												UiControlIsPreview: false,
											}

											// Send new record through channel for immediate viewing on the frontend/user
											channel <- &nfsReportRec
										}
									}
								}
							}
						}
					} else {
						// No more rows so end loop normaly
						break
					}
				}
			}
		}
	}()


	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}


// Asynchronously generates the VM Storage report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back throug the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateVmStorageReport(ctx context.Context, channel chan *api.VmStorageReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.VmStorageReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.VmStorageReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create VM Storage report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.VmStorageReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.VmStorageReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for VM Storage report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						var reportRec = api.VmStorageReportResponse{
							Name:   stmt.GetText("name"),
							SizeTb: stmt.GetFloat("size_tb"),
							UiControlIsPreview: false,
						}

						// Send new record through channel for immediate viewing on the frontend/user
						channel <- &reportRec
					} else {
						// No more rows so end loop normaly
						break
					}
				}
			}
		}
	}()

	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}

// Asynchronously generates the IP Range report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back throug the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateIpRangeReport(ctx context.Context, channel chan *api.IpRangeReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.IpRangeReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.IpRangeReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create IP Range report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.IpRangeReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.IpRangeReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for IP Range report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						// Created nested report row
						var reportRec = api.IpRangeRec{
							Name:     stmt.GetText("name"),
							IpRange:  stmt.GetText("ip_range"),
							Vlan:     stmt.GetText("vlan"),
							Notes:    stmt.GetText("user_notes"),
						}

						// Send new record through channel for immediate viewing on the frontend/user
						channel <- &api.IpRangeReportResponse{
							IpRangeRecord:      &reportRec,
							UiControlIsPreview: false,
						}
					} else {
						// No more rows so end loop normaly
						break
					}
				}
			}
		}
	}()

	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}


// Asynchronously generates the IP Range report using the data from the fast SQLite DB cache.  Runtime report
// generation data and errors will be communicated back throug the channel.  The client will have to process error
// and data records accordingly.
//
func (o *VCenterSyncRec) GenerateStorageSummaryReport(ctx context.Context, channel chan *api.StorageSummaryReportResponse) error {
	// Send informational record about data source immediately to the frontend/user.  Use go-routine to help avoid
	// possible deadlock if caller is the same thread consuming the items off the channel.
	go func() {
		if dataBytes, errJson := json.Marshal(util.DatasourceMetaStruct{
			Name:        constants.DATASOURCE_SQLITE,
			Description: "Generated using fast SQLite database cache",
			LastUpdated: o.GetLatestCompletedSyncTime(), // Query environment's latest SQLite sync time
		}); errJson != nil {
			// Fatal - unlikely
			logger.Log.Error(errJson, "Could not marshal Datasource record")
		} else {
			channel <- &api.StorageSummaryReportResponse{
				UiMsgInfo: constants.DATASOURCE_HEADER + string(dataBytes),
			}
		}
	}()

	// Process asynchronously and provide results through the given channel queue object
	//
	// Run in background go-routine
	go func() {
		// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
		var connection = o.dbPool.Get(context.Background())

		// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
		// run out of connections and database operations will appear to hang
		defer o.dbPool.Put(connection)

		// DEFER: Always end the channel with a nil element which indicates to caller streaming is completed
		defer func() { channel <- nil }()

		if stmt, errStmt := connection.Prepare(dbutil.StorageSummaryReportSql); errStmt != nil {
			var errMsg = fmt.Sprintf("Could not create Storage Summary report because of DB statement creation error: %v", errStmt)
			logger.Log.Info(errMsg)

			// Send error record through the channel since communication with an asynchronous client can only occur
			// via the given channel
			channel <- &api.StorageSummaryReportResponse{
				UiMsgError: errMsg,
			}
		} else {
			for {
				if isRowReturned, errStep := stmt.Step(); errStep != nil {
					// Fatal

					// Send error record
					channel <- &api.StorageSummaryReportResponse{
						UiMsgError: fmt.Sprintf("Fatal encountered while executing database query for Storage Summary report: %v", errStep),
					}

					// Discontinue processing
					break
				} else {
					if isRowReturned {
						// Created nested report row
						var reportRec = api.StorageSummaryRec{
							Name:      stmt.GetText("name"),
							Cidr:      stmt.GetText("cidr"),
							Ports:     stmt.GetText("ports"),
							Username:  stmt.GetText("username"),
							Password:  stmt.GetText("password"),
						}

						// Send new record through channel for immediate viewing on the frontend/user
						channel <- &api.StorageSummaryReportResponse{
							StorageSummaryRecord:      &reportRec,
							UiControlIsPreview: false,
						}
					} else {
						// No more rows so end loop normaly
						break
					}
				}
			}
		}
	}()

	// No errors during setup but go routine will most likely still be processing in the parallel execution
	return nil
}

// Generates the Hypervisor load out information on this VCenter cached instance with the hypervisors matching the
// given path regexp
//
func (o *VCenterSyncRec) GenerateHypervisorLoadOut(hypervisorPathRegex string) (*api.HypervisorLoadOutList, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if re, errRegExp := regexp.Compile(hypervisorPathRegex); errRegExp != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Host path regex filter '%s' is invalid: %v", hypervisorPathRegex, errRegExp))
	} else {
		var hypervisorVcPaths = make([]string, 0)

		if stmtGetHypervisorPaths, errStmt := connection.Prepare("SELECT path FROM Hypervisor ORDER BY name"); errStmt != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Fatal error while creating database statement for Hypervisor: %v", errStmt))
		} else {
			for {
				if isRowReturned, errStep := stmtGetHypervisorPaths.Step(); errStep != nil {
					// Fatal
					return nil, errors.New(fmt.Sprintf("Fatal error while retrieving database query rows for hypervisor path fields: %v", errStep))
				} else {
					if isRowReturned {
						var hypervisorVcPath = stmtGetHypervisorPaths.GetText("path")

						// Use regexp to get user's requested hypervisor paths
						if re.Match([]byte(hypervisorVcPath)) {
							hypervisorVcPaths = append(hypervisorVcPaths, hypervisorVcPath)
						}
					} else {
						// All rows returned & processed
						break
					}
				}
			}

			// This is the returned list that will contain all results with 0 or more Hypervisors each with 0 or
			// more child VirtualMachine load records
			var hypervisorLoadOutList = api.HypervisorLoadOutList{}

			// Use matched paths to generate Hypervisor load out for each matched Hypervisor path
			for _, hypervisorPath := range hypervisorVcPaths {
				if stmtChildVm, errStmtChildVm := connection.Prepare(dbutil.HypervisorLoadOutSql); errStmtChildVm != nil {
					// Fatal
					return nil, errors.New(fmt.Sprintf("Fatal DB error while retrieving rows: %v", errStmtChildVm))
				} else {
					if hypervisorLoadOut, errHypervisorInit := o.InitializeHypervisorCapacity(hypervisorPath); errHypervisorInit != nil {
						// Fatal
						return nil, errors.New(fmt.Sprintf("Error with load out data retrieval: %v", errHypervisorInit))
					} else {
						// Add this Hypervisor load out to the main list
						hypervisorLoadOutList.Load = append(hypervisorLoadOutList.Load, hypervisorLoadOut)

						// Set VirtualMachine search for all VirtualMachine that are child of the reference Hypervisor
						stmtChildVm.SetText("$hostVcPath", hypervisorPath)

						// Save each VirtualMachine to the parent HypervisorLoadOut record
						for {
							if rowReturned, errChildVm := stmtChildVm.Step(); errChildVm != nil {
								return nil, errors.New(fmt.Sprintf("Fatal DB error while retrieving rows: %v", errChildVm))
							} else {
								if rowReturned {
									var vmPath = stmtChildVm.GetText("path")

									if virtualMachineDataStores, errVmDatastores := o.GetVirtualMachineDatastores(vmPath); errVmDatastores != nil {
										// Fatal
										return nil, errVmDatastores
									} else {
										if virtualMachineNetworks, errVmNetworks := o.GetVirtualMachineNetworks(vmPath); errVmNetworks != nil {
											// Fatal
											return nil, errVmNetworks
										} else {
											// Add each child VirtualMachine to the current Hypervisor's load out list
											var virtualMachineLoad = &api.VirtualMachineLoad{
												Hostname:    stmtChildVm.GetText("name"),
												Path:        vmPath,
												Cpu:         int32(stmtChildVm.GetInt64("cpus")),
												MemGb:       float32(stmtChildVm.GetFloat("mem_gb")),
												Networks:    translate(virtualMachineNetworks),
												Datastores:  convertDatastoreList(virtualMachineDataStores),
											}

											// Pre-parse balancing fields (if available).  This is convenient reference
											// for parsing operations both server and clientside and improves balancing
											// speed.
											var nodeTypeStruct = balancingutil.ParseNodeType(virtualMachineLoad.Hostname)
											if nodeTypeStruct != nil {
												virtualMachineLoad.NodeType = nodeTypeStruct.NodeType
												virtualMachineLoad.Leg = nodeTypeStruct.Leg
												virtualMachineLoad.Cluster = nodeTypeStruct.Cluster
												virtualMachineLoad.VmType = balancingutil.VMTYPE_NORMAL // VM weight will be adjusted after reading in the user's balance rules & definitions
											}

											// Add to the parent hypervisor
											hypervisorLoadOut.VmCurrent = append(hypervisorLoadOut.VmCurrent, virtualMachineLoad)

											// Keep running tally of CPU & MEM total allocation and available room
											hypervisorLoadOut.GetCurrentRoomCpu().VmTotal += virtualMachineLoad.Cpu
											hypervisorLoadOut.GetCurrentRoomMemGb().VmTotal += virtualMachineLoad.MemGb
											hypervisorLoadOut.GetCurrentRoomCpu().RoomAvailable = hypervisorLoadOut.Cpu - hypervisorLoadOut.GetCurrentRoomCpu().VmTotal
											hypervisorLoadOut.GetCurrentRoomMemGb().RoomAvailable = hypervisorLoadOut.MemGb - hypervisorLoadOut.GetCurrentRoomMemGb().VmTotal
										}
									}
								} else {
									// No more rows
									break
								}
							}
						}

						// NOTE: CPU and Mem Room Severity is calculated after this where the user-defined rules have
						// been parsed and ready for comparison.
					}
				}
			}

			return &hypervisorLoadOutList, nil
		}
	}
}

func translate(networkRecs []*api.VirtualMachineNetwork) (networkNames []string) {
	networkNames = make([]string, 0)

	for _, networkRec := range networkRecs {
		networkNames = append(networkNames, networkRec.GetNetworkName())
	}

	return
}

// Returns an "initialized" Hypervisor record with capacity values filled out but the load out is empty for later
// computation
//
func (o *VCenterSyncRec) InitializeHypervisorCapacity(path string) (*api.HypervisorLoadOut, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	var hypervisorLoadOut *api.HypervisorLoadOut

	if stmtHypervisor, errStmt := connection.Prepare("SELECT name, path, managed_obj_ref, cpus, mem_mb / 1024.0 as mem_gb, rack FROM Hypervisor WHERE path = $path;"); errStmt != nil {
		// Fatal
		return nil, errStmt
	} else {
		defer stmtHypervisor.Finalize()
		stmtHypervisor.SetText("$path", path)

		// Expecting exactly 0 or 1 rows returned
		for {
			if hasRow, errStep := stmtHypervisor.Step(); errStep != nil {
				// Fatal
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errStep))
			} else {
				if hasRow {
					hypervisorLoadOut = &api.HypervisorLoadOut{
						Hostname:          stmtHypervisor.GetText("name"),
						Path:              stmtHypervisor.GetText("path"),
						ManagedObjRef:     stmtHypervisor.GetText("managed_obj_ref"),
						Cpu:               int32(stmtHypervisor.GetInt64("cpus")),
						MemGb:             float32(stmtHypervisor.GetFloat("mem_gb")),
						Rack:              stmtHypervisor.GetText("rack"),
						Tags:              []string{}, //  Will be populated below with separate DB call
						CurrentRoomCpu:    &api.RoomCpu{},   // will be incremented as child VMs are loaded
						CurrentRoomMemGb:  &api.RoomMemGB{}, // will be incremented as child VMs are loaded
						ProposedRoomCpu:   &api.RoomCpu{},   // will be incremented as child VMs are loaded
						ProposedRoomMemGb: &api.RoomMemGB{}, // will be incremented as child VMs are loaded
					}

					// Initialize room availability as "max available" since no child VMs are added yet
					hypervisorLoadOut.GetCurrentRoomCpu().RoomAvailable = hypervisorLoadOut.Cpu
					hypervisorLoadOut.GetCurrentRoomMemGb().RoomAvailable = hypervisorLoadOut.MemGb
					hypervisorLoadOut.GetCurrentRoomCpu().Severity = constants.SEVERITY_OK
					hypervisorLoadOut.GetCurrentRoomMemGb().Severity = constants.SEVERITY_OK
					hypervisorLoadOut.GetProposedRoomCpu().RoomAvailable = hypervisorLoadOut.Cpu
					hypervisorLoadOut.GetProposedRoomMemGb().RoomAvailable = hypervisorLoadOut.MemGb
					hypervisorLoadOut.GetProposedRoomCpu().Severity = constants.SEVERITY_OK
					hypervisorLoadOut.GetProposedRoomMemGb().Severity = constants.SEVERITY_OK

					// Fetch the 0 or more Network child records for this hypervisor
					if networks, errNetworks := o.GetHypervisorNetworks(path); errNetworks != nil {
						// Fatal
						return nil, errNetworks
					} else {
						// Set to object
						hypervisorLoadOut.Networks = networks
					}

					// Fetch the 0 or more Datastore child records for this hypervisor
					if datastores, errNetworks := o.GetHypervisorDatastores(path); errNetworks != nil {
						// Fatal
						return nil, errNetworks
					} else {
						// Set to object
						hypervisorLoadOut.Datastores = convertDatastoreList(datastores)
					}

					// Fetch 0 or more Tag child records for this hypervisor
					if tagName, errTags := o.GetHypervisorTags(path); errTags != nil {
						// Fatal
						return nil, errTags
					} else {
						// Set to object
						hypervisorLoadOut.Tags = append(hypervisorLoadOut.Tags, tagName...)
					}
				} else {
					// All rows returned & processed
					break
				}
			}
		}

		// No matching hypervisor for the given path so return a nil reference
		return hypervisorLoadOut, nil
	}
}

func (o *VCenterSyncRec) GetHypervisorTags(hypervisorPath string) ([]string, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT * FROM HypervisorTag WHERE parentHypervisorPath = $parentHypervisorPath"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$parentHypervisorPath", hypervisorPath)
		var tagList = make([]string, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					tagList = append(tagList, strings.TrimSpace(stmt.GetText("tagName")))
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return tagList, nil
	}
}

// Retrieves a *distinct* list of all hypervisor tag strings.  This is useful for confirming if any matching hypervisor
// tags exist at all for sanity checking and post balance user alerts.
//
func (o *VCenterSyncRec) GetAllHypervisorTags() ([]string, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT DISTINCT tagName FROM HypervisorTag"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var tagList = make([]string, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					tagList = append(tagList, strings.TrimSpace(stmt.GetText("tagName")))
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return tagList, nil
	}
}

// Retrieves the Hypervisor's associated child Network records from SQLite
//
func (o *VCenterSyncRec) GetHypervisorNetworks(hypervisorPath string) ([]string, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT name FROM HypervisorNetwork WHERE parentHypervisorPath = $parentHypervisorPath"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$parentHypervisorPath", hypervisorPath)
		var networkList = make([]string, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					networkList = append(networkList, strings.TrimSpace(stmt.GetText("name")))
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return networkList, nil
	}
}

// Retrieves the Hypervisor's associated child Datastore records from SQLite
//
func (o *VCenterSyncRec) GetHypervisorDatastores(hypervisorPath string) ([]*vcenterutil.DatastoreStruct, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT * FROM HypervisorDatastore WHERE parentHypervisorPath = $parentHypervisorPath"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$parentHypervisorPath", hypervisorPath)
		var datastores = make([]*vcenterutil.DatastoreStruct, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					datastores = append(datastores, &vcenterutil.DatastoreStruct{
						Path: strings.TrimSpace(stmt.GetText("path")),
						Url:  strings.TrimSpace(stmt.GetText("url")),
					})
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return datastores, nil
	}
}

// Retrieves the VM's associated child Datastore records from SQLite
//
func (o *VCenterSyncRec) GetVirtualMachineDatastores(virtualMachinePath string) ([]*vcenterutil.DatastoreStruct, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT path, url FROM VirtualMachineDatastore WHERE parentVirtualMachinePath = $parentVirtualMachinePath"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$parentVirtualMachinePath", virtualMachinePath)
		var datastoreStructs = make([]*vcenterutil.DatastoreStruct, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					datastoreStructs = append(datastoreStructs, &vcenterutil.DatastoreStruct{
						strings.TrimSpace(stmt.GetText("path")),
						strings.TrimSpace(stmt.GetText("url")),
					})
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return datastoreStructs, nil
	}
}

// Retrieves the VM's associated child Network records from SQLite
//
func (o *VCenterSyncRec) GetVirtualMachineNetworks(virtualMachinePath string) ([]*api.VirtualMachineNetwork, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT path, networkName, adapterType, macAddress, ipAddresses FROM VirtualMachineNetwork WHERE parentVirtualMachinePath = $parentVirtualMachinePath"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$parentVirtualMachinePath", virtualMachinePath)
		var vmNetworkRecs = make([]*api.VirtualMachineNetwork, 0)
		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					vmNetworkRecs = append(vmNetworkRecs, &api.VirtualMachineNetwork{
						NetworkPath: stmt.GetText(strings.TrimSpace("path")),
						NetworkName: stmt.GetText(strings.TrimSpace("networkName")),
						AdapterType: stmt.GetText(strings.TrimSpace("adapterType")),
						MacAddress:  stmt.GetText(strings.TrimSpace("macAddress")),
						IpAddresses: strings.Split(stmt.GetText(strings.TrimSpace("ipAddresses")), ","),
					})
				} else {
					// All rows processed
					break
				}
			}
		}

		// No errors.  Return the requested list.
		return vmNetworkRecs, nil
	}
}

// Gets the SyncHistory database record matching the given ID.  Otherwise a nil reference is returned if
// no matching record is found.
func (o *VCenterSyncRec) GetSyncHistoryRecord(id int64) (*api.SyncHistoryRec, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT * FROM SyncHistory WHERE id = ?"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind
		stmt.BindInt64(1, id)

		if isRowReturned, errExec := stmt.Step(); errExec != nil {
			// FATAL
			return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
		} else {
			if isRowReturned {
				var syncHistoryRec = api.SyncHistoryRec{
					SyncHistoryId:    id,
					EnvironmentName:  stmt.GetText("environment_name"),
					Status:           stmt.GetText("status"),
					InitiatedBy:      stmt.GetText("initiated_by"),
					StartTimeMs:      stmt.GetInt64("start_time_ms"),
					EndTimeMs:        stmt.GetInt64("end_time_ms"),
					RecordsProcessed: stmt.GetInt64("records_processed"),
				}

				return &syncHistoryRec, nil
			} else {
				// No matching record
				return nil, nil
			}
		}
	}
}

// Gets the 'n' latest SyncHistory records for this environment.  Sort on start time descendingly (latest first).
//
func (o *VCenterSyncRec) GetSyncHistoryRecordsRecent(n int32) (*api.SyncHistoryRecArray, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Result will be accumulated and returned here
	var syncHistoryRecs = api.SyncHistoryRecArray{}

	if stmt, errStmt := connection.Prepare("SELECT * FROM SyncHistory ORDER BY start_time_ms DESC LIMIT ?"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB statement because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind for limit on number to return
		stmt.BindInt64(1, int64(n))

		for {
			if isRowReturned, errStep := stmt.Step(); errStep != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errStep))
			} else {
				if isRowReturned {
					var syncHistoryRec = api.SyncHistoryRec{
						SyncHistoryId:    stmt.GetInt64("id"),
						EnvironmentName:  stmt.GetText("environment_name"),
						Status:           stmt.GetText("status"),
						InitiatedBy:      stmt.GetText("initiated_by"),
						StartTimeMs:      stmt.GetInt64("start_time_ms"),
						EndTimeMs:        stmt.GetInt64("end_time_ms"),
						RecordsProcessed: stmt.GetInt64("records_processed"),
					}

					syncHistoryRecs.Items = append(syncHistoryRecs.Items, &syncHistoryRec)
				} else {
					// No more rows left -- exit loop normally
					break
				}
			}
		}
	}

	// Search completed
	return &syncHistoryRecs, nil
}

// Gets all the child sync log entries for the parent SyncHistory referenced by the given ID.  Results are sorted
// by time descendingly to show latest record first.
//
func (o *VCenterSyncRec) GetSyncLogRecords(parentSyncHistoryId int64) (*api.SyncLogRecResponseArray, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually r
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Result will be accumulated and returned here
	var syncLogRecs = api.SyncLogRecResponseArray{}

	if stmt, errStmt := connection.Prepare("SELECT * FROM SyncLogEntry WHERE parent_id = ? ORDER BY time_ms DESC"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind to select child records of parent SyncHistory record
		stmt.BindInt64(1, parentSyncHistoryId)

		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					var syncLogRec = api.SyncLogRecResponse{
						ParentId:     parentSyncHistoryId,
						TimeMs:       stmt.GetInt64("time_ms"),
						MessageError: stmt.GetText("msg_err"),
						MessageWarn:  stmt.GetText("msg_warn"),
						MessageInfo:  stmt.GetText("msg_info"),
					}

					syncLogRecs.Items = append(syncLogRecs.Items, &syncLogRec)
				} else {
					// No more rows left -- exit loop normally
					break
				}
			}
		}
	}

	// Search completed
	return &syncLogRecs, nil
}

// Returns the time (millis) of the latest complete successful SyncHistory record.  If any error occurs, then a -1
// value is returned which should be taken as an error.  Check logs for more details.
//
func (o *VCenterSyncRec) GetLatestCompletedSyncTime() int64 {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmt, errStmt := connection.Prepare("SELECT start_time_ms FROM SyncHistory WHERE status = 'COMPLETE' ORDER BY start_time_ms DESC LIMIT 1;"); errStmt != nil {
		// Not-fatal but log it
		logger.Log.Info(fmt.Sprintf("Cannot get latest sync time because of SQL error: %v", errStmt))
		return -1
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		if isRowReturned, errExec := stmt.Step(); errExec != nil {
			// No-fatal but log it
			logger.Log.Info(fmt.Sprintf("Cannot get latest sync time because of DB execution error: %v", errExec))
			return -1
		} else {
			if isRowReturned {
				// Successfully found latest sync time
				return stmt.GetInt64("start_time_ms")
			} else {
				// No-fatal but log it
				logger.Log.Info("Cannot get latest sync time because no row was returned")
				return -1
			}
		}
	}
}

// Returns a standardized log message for sync activities which is easily greppable
//
func (o *VCenterSyncRec) GenerateSyncLogMsg(message string) string {
	var syncId = "--"
	if o.activeSyncHistoryId != nil {
		syncId = strconv.Itoa(int(*o.activeSyncHistoryId))
	}

	return fmt.Sprintf("SYNC::[%s,%s]:%s", o.vsphereConnection.Name, syncId, message)
}

// Removes domain names from the list of FQDNs and returns the list with only hostnames
//
func removeFqdns(fqdnList []string) []string {
	var vmList = make([]string, 0)
	for _, fqdn := range fqdnList {
		vmList = append(vmList, util.ExtractHostname(fqdn))
	}

	// Sorted list makes viewing and comparison easier for the user
	sort.Strings(vmList)

	return vmList
}

// Calculates severity level for the set of available headroom
//
func calcSeverityLevel(cpuHeadroom, memGbHeadroom, hddGbHeadroom int32) string {
	levelCritical := `CRITICAL`
	levelWarn := `WARN`
	levelOk := `OK`

	if cpuHeadroom <= 0 {
		return levelCritical
	}

	if memGbHeadroom <= 0 {
		return levelCritical
	}

	if hddGbHeadroom <= 0 {
		return levelCritical
	}

	if cpuHeadroom > 0 && cpuHeadroom < 2 {
		return levelWarn
	}

	if hddGbHeadroom > 0 && hddGbHeadroom < 100 {
		return levelWarn
	}

	if memGbHeadroom > 0 && memGbHeadroom < 5 {
		return levelWarn
	}

	return levelOk
}

// Return an integer representing the record count of the specified table name from this SQLite DB instance.
//
// NOTE: If a -1 is returned, then some error occurred and the count could not be ascertained -- most likely the given
// table was not found in the SQLite database.  Check logs for error.
//
func (o *VCenterSyncRec) GetRecordCount(tableName string) int64 {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// At the end, must manually return connection to the DB connection pool otherwise will eventually run out of
	// connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmtCount, errStmt := connection.Prepare(fmt.Sprintf("SELECT COUNT(*) AS cnt FROM %s", tableName)); errStmt != nil {
		// Not-fatal but log it -- likely the referenced table name was not found in the SQLite database
		logger.Log.Info(fmt.Sprintf("%s count failed because of DB statement creation error: %v", tableName, errStmt))
		return -1
	} else {
		// Translate VSphere record into SQLite row
		//
		if isRowReturned, errStmtExec := stmtCount.Step(); errStmtExec != nil {
			// Not-fatal but log it
			logger.Log.Info(fmt.Sprintf("%s count failed because of DB execution error: %v", tableName, errStmtExec))
			return -1
		} else {
			if isRowReturned {
				var recordCount = stmtCount.GetInt64("cnt")

				// Must be finalized or cause SQLite pool to throw a panic when returning the parent connection to the pool
				stmtCount.Finalize()

				return recordCount
			} else {
				// Not-fatal but log it
				logger.Log.Info(fmt.Sprintf("%s count failed because DB execution did not return a count", tableName))
				return -1
			}
		}
	}
}

//func (o *VCenterSyncRec) CreateHypervisorListingTable(basename string, regexSearchPattern string) string {
//
//}

// Creates a uniquely named temp table with the given basename as the prefix
//
func (o *VCenterSyncRec) CreateTempTable(basename string) (*string, error) {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// At the end, must manually return connection to the DB connection pool otherwise will eventually run out of
	// connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Derive the table name from the given basename and a UUID to gurantee uniqueness
	var tempTableName = basename + "_" + uuid.New().String()

	if stmtCreateTempTable, errStmt := connection.Prepare(fmt.Sprintf("CREATE TABLE %s (name TEXT)", tempTableName)); errStmt != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not create temp table '%s' because of error: %v", tempTableName, errStmt))
	} else {
		stmtCreateTempTable.Step()
		stmtCreateTempTable.Reset()
		stmtCreateTempTable.Finalize()
	}

	return &tempTableName, nil
}

func (o *VCenterSyncRec) DropTable(tableName string) error {
	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// At the end, must manually return connection to the DB connection pool otherwise will eventually run out of
	// connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	if stmtCreateTempTable, errStmt := connection.Prepare(fmt.Sprintf("DROP TABLE %s", tableName)); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not DROP TABLE '%s' because of error: %v", tableName, errStmt))
	} else {
		stmtCreateTempTable.Step()
		stmtCreateTempTable.Reset()
		stmtCreateTempTable.Finalize()
	}

	return nil
}

// Initializes the cron with a default schedule.  Cron is initially disabled until started.
//
// Ref: https://godoc.org/github.com/robfig/cron
//
func (o *VCenterSyncRec) CronInitDefault() (*cron.EntryID, error) {
	return o.CronInit(DEFAULT_CRON_SCHEDULE)
}

// Starts the cron with a custom cron schedule expression.   Cron is initially disabled until started.
//
// Ref: https://godoc.org/github.com/robfig/cron#Cron.AddFunc
//
func (o *VCenterSyncRec) CronInit(cronExpression string) (*cron.EntryID, error) {
	var ctx = context.WithValue(context.Background(), constants.CTX_KEY_AUTH_USERNAME, "<cron>")

	// Stop all active jobs
	if o.syncCron != nil {
		o.syncCron.Stop()
	}

	// Save the cron expression for viewing later by UI if required
	o.cronExpression = cronExpression

	// Recreate with new job with the given cron expression.  This is only one cron entry per environment!
	o.syncCron = cron.New()
	if entry, err := o.syncCron.AddFunc(cronExpression, func() {
		o.StartDataSyncInBackground(ctx)
	}); err != nil {
		return nil, errors.New(fmt.Sprintf("Could not create cron with given cron expression '%s' because of error: %v", cronExpression, err))
	} else {
		return &entry, nil
	}
}

// Enables or disables the cron process
//
func (o *VCenterSyncRec) CronSetEnabled(enable bool) {
	if enable {
		o.CronInit(o.cronExpression)
		o.syncCron.Start()
	} else {
		if o.syncCron != nil {
			// Stop the current cron job then nil it out so it gets recreated
			o.syncCron.Stop()
			o.syncCron = nil
		}
	}
}

// Returns true if the cron process is enabled (running) and false otherwise
//
func (o *VCenterSyncRec) CronGetEnabled() bool {
	return o.syncCron != nil && o.CronNextScheduleRunTime().Unix() != 0
}

// Gets the next scheduled cron auto-update run for this environment.  Return will be nil if no cron job is currently
// set up.  A time value <= 0 indicates cron is not running per documentation.
//
func (o *VCenterSyncRec) CronNextScheduleRunTime() time.Time {
	if o.syncCron != nil {
		if o.syncCron.Entries() != nil && len(o.syncCron.Entries()) > 0 {
			// There should only be one entry
			return o.syncCron.Entries()[0].Next
		} else {
			// Cron job object exists but has no entries
			return time.Unix(0, 0) // 0 means cron not running
		}
	} else {
		// No such cron object currently so create one for next time
		o.syncCron = cron.New()

		return time.Unix(0, 0) // 0 means cron not running
	}
}

// Gets the current cron expression
//
func (o *VCenterSyncRec) CronExpression() string {
	return o.cronExpression
}

// Logs the VM move to the current SQLite connection's database VM move audit table
//
// RETURN: ID of new audit log record
// RETURN: error reference if error occurs and nil otherwise
var mutexInsertLogVmMove = sync.Mutex{}
func (o *VCenterSyncRec) InsertLogVmMove(ctx context.Context, auditLogRec VmMoveAuditLogStruct) (int64, error) {
	defer mutexInsertLogVmMove.Unlock()
	mutexInsertLogVmMove.Lock();

	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(ctx)

	// DEFER: At the end, must manually return connection to the DB connection pool otherwise will eventually
	// run out of connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Construct a prepared statement for audit log row creation.
	// NOTE: the "ID" column is omitted so will be automatically assigned a unique ID by SQLite during the insertion
	if stmt, errStmt := connection.Prepare("INSERT INTO AuditVmMove (username, vcenter, hostname, session_uuid, timestamp_ms, duration_sec, vm_path, source_hv_path, destination_hv_path, status, extra) VALUES ($username, $vcenter, $hostname, $session_uuid, $timestamp_ms, $duration_sec, $vm_path, $source_hv_path, $destination_hv_path, $status, $extra)"); errStmt != nil {
		// Fatal
		return -1, errors.New(fmt.Sprintf("Could not create AuditVmMove record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		stmt.SetText("$username", auditLogRec.Username)
		stmt.SetText("$vcenter", auditLogRec.VCenter)
		stmt.SetText("$hostname", util.GetHostname())  // The processing host - might contain error message if hostname could not be obtained
		stmt.SetText("$session_uuid", auditLogRec.SessionUuid)
		stmt.SetInt64("$timestamp_ms", util.CurrentTimeMillis())
		stmt.SetFloat("$duration_sec", auditLogRec.DurationSec)
		stmt.SetText("$vm_path", auditLogRec.VmPath)
		stmt.SetText("$source_hv_path", auditLogRec.SourceHvPath)
		stmt.SetText("$destination_hv_path", auditLogRec.DestinationHvPath)
		stmt.SetText("$status", auditLogRec.Status)
		stmt.SetText("$extra", auditLogRec.Extra)

		if _, errStep := stmt.Step(); errStep != nil {
			// Fatal
			return -1, errors.New(fmt.Sprintf("Could not execute SQL statement because of error: %v", errStep))
		} else {
			// Expecting exactly one row to be returned
			// INSERT completed successfully
			return connection.LastInsertRowID(), nil
		}
	}
}

// Returns the most recent move time (epoch milliseconds) for the current VCenter instance.  Or -1 if no such move found.
//
var mutexVmMoveInfo = sync.Mutex{}
func (o *VCenterSyncRec) GetLastVmMoveInfo() (*api.BalanceInvalidCheckResponse, error) {
	defer mutexVmMoveInfo.Unlock()
	mutexVmMoveInfo.Lock()

	// Get a DB connection from the pool -- connection must be manually returned to the pool afterwards!
	var connection = o.dbPool.Get(context.Background())

	// At the end, must manually return connection to the DB connection pool otherwise will eventually run out of
	// connections and database operations will appear to hang
	defer o.dbPool.Put(connection)

	// Select the most recent successful VM move for the given environment.  NOTE: this can return no rows depending
	// on the state of the VM move data.
	//
	if stmt, errStmt := connection.Prepare("SELECT username, vcenter, session_uuid, timestamp_ms FROM AuditVmMove WHERE vcenter = $vcenter AND status = 'OK' ORDER BY timestamp_ms DESC LIMIT 1"); errStmt != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Get Last VM move time failed because of DB statement creation error: %v", errStmt))
	} else {
		// Must be finalized or cause SQLite pool to throw a panic when returning the parent connection to the pool
		defer stmt.Finalize()

		// Set VCenter name to filter query on
		stmt.SetText("$vcenter", o.GetName())

		// Process 0 or more rows
		if isRowReturned, errStmtExec := stmt.Step(); errStmtExec != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Get Last VM move time failed because of DB execution error: %v", errStmtExec))
		} else {
			if isRowReturned {
				return &api.BalanceInvalidCheckResponse {
					VcenterName: stmt.GetText("vcenter"),
					Username:    stmt.GetText("username"),
					TimestampMs: stmt.GetInt64("timestamp_ms"),
					SessionUuid: stmt.GetText("session_uuid"),
				}, nil
			} else {
				// No matching result return response without time or session values
				return &api.BalanceInvalidCheckResponse {
					VcenterName: stmt.GetText("vcenter"),
					Username:    stmt.GetText("username"),
					TimestampMs: -1,
					SessionUuid: "",
				}, nil
			}
		}
	}
}

// Logs the VM move but will log errors instead of returning them.  This is a convenience macro that assumes caller
// does not consider audit log write failure to be fatal.
//
func (o *VCenterSyncRec) InsertLogVmMoveWithLog(ctx context.Context, auditLogRec VmMoveAuditLogStruct) int64 {
	if auditLogId, auditLogErr := o.InsertLogVmMove(ctx, auditLogRec); auditLogErr != nil {
		// WARN - not a blocker but log it because it represents system instability
		logger.Log.Info(fmt.Sprintf("WARN: cannot write VM move log audit record becuase of error: %v", auditLogErr))

		return -1;
	} else {
		// Successful log
		logger.Log.Info(fmt.Sprintf("VM move log ID %d written successfully to SQLite", auditLogId))

		return auditLogId
	}
}

// SQLite does not have a BOOL type but uses C language convention of 1 = true and 0 = false
//
func BoolToInt(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

// SQLite does not have a BOOL type but uses C language convention of 1 = true and 0 = false
//
func IntToBool(n int64) bool {
	return n != 0
}
