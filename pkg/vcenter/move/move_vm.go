package move

import (
	"dora/pkg/util"
	"dora/pkg/vcenterutil"
	"errors"
	"fmt"
	"github.com/vmware/govmomi/object"
	"github.com/vmware/govmomi/vim25/types"
	"sync"
	"time"
)

// Calls VCenter and moves (migrates) a specified VM (referenced by full path) to a destination hypervisor (referenced
// by full path).
//
// NOTE: Must use full VCenter paths like: hypervisorDestPath: /PAYTV-INT/host/Integration/f-05-30.infra.smf1.mobitv
// NOTE: If VM is already located at the destination VM then no action occurs and a warning message is returned instead
// NOTE: Validation that VM is already on the host must be performed separately prior to calling this move function
//
// RETURN:
// (1) []string - Zero or more list of warnings
// (2) error - if a fatal error occurred and nil otherwise
//
func ExecuteVmMove(vcSoapConnection *vcenterutil.VcSoapConnection, vm *object.VirtualMachine, hypervisorDestPath string) ([]string, error) {
	//vmHs, err := vm.HostSystem(vcSoapConnection.ctx)
	//if err != nil {
	//	return nil, errors.New(fmt.Sprintf("Host move error.  VM HostSystem Error: %s", err.Error()))
	//}
	//vmHsMO := vmHs.Reference()

	destHs, err := vcSoapConnection.Finder.HostSystem(vcSoapConnection.Ctx, hypervisorDestPath)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Host move error.  Find 'destination' HOSTSYSTEM Error: %s", err.Error()))
	}

	//if vmHsMO.Value == destHs.Reference().Value {
	//	return []string{fmt.Sprintf("HOST move not necessary.  VM is already located on specified hypervisor host '%s'.", destHostPath)}, nil
	//}
	//log.Info("Current Host %s", vmHsMO.Value)

	newHsR, err := destHs.ResourcePool(vcSoapConnection.Ctx)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("VM Host Resource Pool Error: %s", err.Error()))
	}

	newHsMO := destHs.Reference()
	newHsRMO := newHsR.Reference()
	spec := types.VirtualMachineRelocateSpec{
		Host: &newHsMO,
		Pool: &newHsRMO,
	}

	task, err := vm.Relocate(vcSoapConnection.Ctx, spec, types.VirtualMachineMovePriorityLowPriority)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("VM Relocate Error: %s", err.Error()))
	}

	err = task.Wait(vcSoapConnection.Ctx)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("VM Relocate Task Error: %s", err.Error()))
	}

	return nil, nil
}

// Mutex and mapping for Balance environment locking per VCenter
///
var mutexMoveLock = sync.Mutex{}
var mapMoveLock = make(map[string]*LockStruct)

// Gives access to the VCenter lock map
func GetLockMap() *map[string]*LockStruct {
	return &mapMoveLock
}

// Creates or looks up a locking record for the current VCenter.  A lock record is returned either way.
//
// NOTE: The lock record must be checked if it belongs to the current session or not (see "isOwner" value)
// PARAM: vcenterName    the name of the VCenter environment -- ex: "NewPayTV", "OldPayTV", "Comporium", "Arvig-PROD", etc
// PARAM: vmMoveRequest  will be used as a lock record if no existing lock exists for the vcenter
// RETURN: lock record for environment -- lock can be existing or a new lock (must check "isOwner" value)
// RETURN isOwner   bool value indicates if the lock is for current session or not
func LockVCenterForMove(vcenterName string, username string, sessionUuid string) (lock *LockStruct, isOwner bool) {
	defer mutexMoveLock.Unlock()
	mutexMoveLock.Lock()

	// Lookup any existing locking record for the given VCenter
	lock = mapMoveLock[vcenterName]

	if lock != nil {
		//
		// Process existing lock
		//
		if sessionUuid != lock.SessionUuid {
			// Another session is currently moving VM's in this VCenter.  Return that record to the caller so it can be
			// displayed to user their VM move operation can be aborted cleanly.
			return lock, false
		} else {
			return lock,true
		}
	} else {
		//
		// No existing lock so create a new one
		//
		var newLock = &LockStruct {
			VCenterName: vcenterName,
			Username:    username,
			SessionUuid: sessionUuid,
			Date:        time.Now(),
		}

		// Okay to proceed and set a new lock record
		mapMoveLock[vcenterName] = newLock

		return newLock, true
	}
}

// Removes the lock for the given VCenter.  This must only be called by the go-routine that holds the lock but no
// check is enforced.
//
// PARAM: vcenterName   the name of the original vCenter
// PARAM: sessionUuid   the original session UUID
// PARAM: delayMillis   delay lock removal this number of milliseconds -- use 0 to effectively disable this feature
// RETURN: unlockCount  1 if actual unlock occurred and 0 otherwise which can happen if session doesn't hold any lock
//                      to be unlocked
//
func UnlockVCenterForMove(vcenterName string, sessionUuid string, delayMillis int) int {
	defer mutexMoveLock.Unlock()

	// Process the rest under mutually exclusive lock
	mutexMoveLock.Lock()

	var lock = mapMoveLock[vcenterName]

	if lock != nil && lock.SessionUuid == sessionUuid {
		// Sleep required period before actually releasing the lock
		time.Sleep(time.Duration(util.Max(0, delayMillis)) * time.Millisecond)

		// Remove the lock record for the VCenter environment
		delete(mapMoveLock, vcenterName)

		return 1
	} else {
		// Lock doesn't exist or does not match the given balance session UUID
		return 0
	}
}

// Simple internal record to track locking records
//
type LockStruct struct {
	VCenterName  string
	Username     string
	SessionUuid  string
	Date         time.Time    // Original lock creation time.  Can be used to expire old locks, generate reports, or for basic debugging
}