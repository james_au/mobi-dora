package vcenter

// Define the four "kinds" of VCenter objects that can be searched for
//
const (
	KIND_VIRTUAL_MACHINE = "VirtualMachine"
	KIND_HOST_SYSTEM     = "HostSystem"
	KIND_NETWORK         = "Network"
	KIND_DATASTORE       = "Datastore"
)

// Retries for VCenter operations
//
const (
	MAX_RETRIES = 3
)
