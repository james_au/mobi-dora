package vcenter

import (
	"context"
	"dora/pkg/addressbook"
	"dora/pkg/api"
	cfg "dora/pkg/config"
	"dora/pkg/vcenterutil"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/vmware/govmomi/list"
	"testing"
)

// This must match a pre-defined connection in the configured address book
const TEST_VCENTER_CONNECTION_NAME = "NewPayTV"
const PATH = "/PAYTV-2/vm/judrm01p1.paytv2.smf1.mobitv"

// Test Virtual Machine VCenter retrieval.  You can use this to test for bugs and develop enhancements to this core
// feature.
//
// NOTE: a valid address-book.json must exist in project's home dir and the connection and search parameters are entered
//       above as constants
// NOTE: This test is "read only" so is safe to run but it can generate network and I/O load on VCenter
//
func Test001_RetrieveVirtualMachine(t *testing.T) {
	assert.NotPanics(t, func() {
		var ctx = context.Background()
		addressBook, errAddressBook := addressbook.New("../../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(TEST_VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Get the ManagedObjectReference for the VM and test
		if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vcConnection); errLogin != nil {
			// Failed
			panic(errLogin)
		} else {
			if vm, errVm := vcSoapConnection.Finder.VirtualMachine(ctx, PATH); errVm != nil {
				panic(errVm)
			} else {
				var element = list.Element{
					Path:   PATH,
					Object: vm.Reference(),
				}

				// Call the actual API here
				if vcvm, errLookup := vcenterutil.RetrieveVmDetailRecord(vcSoapConnection, &element); errLookup != nil {
					panic(errLookup)
				} else {
					// This is a good place to place a debug breakpoint to deep inspect the results
					assert.NotNil(t, vcvm)
				}
			}
		}
	})
}


// Test Hypervisor VCenter retrieval.  You can use this to test for bugs and develop enhancements to this core feature.
//
// NOTE: a valid address-book.json must exist in project's home dir and the connection and search parameters are entered
//       above as constants
// NOTE: This test is "read only" so is safe to run but it can generate network and I/O load on VCenter
//
func Test002_RetrieveHypervisor(t *testing.T) {
	assert.NotPanics(t, func() {
		var ctx = context.Background()
		addressBook, errAddressBook := addressbook.New("../../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(TEST_VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Get the ManagedObjectReference for the VM and test
		if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vcConnection); errLogin != nil {
			// Failed
			panic(errLogin)
		} else {
			// NOTE: Update path and host regexp to filter hypervisors as needed.  Use "" to not filter at all.
			if outputChannel, errVCenter := ListHypervisorRecords(ctx, "", "", false, false, vcSoapConnection); errVCenter != nil {
				panic(errVCenter)
			} else {
				// Will store results from the channel as they arrive
				var hypervisors = make([]*api.HypervisorResponse, 0)

				// Loop through the Go channel object and save results to the array
				for {
					var hypervisorResponse = <-outputChannel  // Get response from channel as they arrive (a null reference indicates channel completion)

					if hypervisorResponse != nil {
						hypervisors = append(hypervisors, hypervisorResponse)
					} else {
						// All hypervisors received from the channel
						break
					}
				}

				// This is a good place to place a debug breakpoint to deep inspect the results
				assert.False(t, len(hypervisors) == 0)
			}
		}
	})
}



// Test Datastore retrieval.  You can use this to test for bugs and develop enhancements to this core feature.
//
// NOTE: a valid address-book.json must exist in project's home dir and the connection and search parameters are entered
//       above as constants
// NOTE: This test is "read only" so is safe to run but it can generate network and I/O load on VCenter
//
func Test003_RetrieveDatastore(t *testing.T) {
	assert.NotPanics(t, func() {
		var ctx = context.Background()
		addressBook, errAddressBook := addressbook.New("../../address-book.json")
		assert.NotNil(t, addressBook, "Address book returned null value")
		assert.Nil(t, errAddressBook, fmt.Sprintf("Unable to read address book at path '%s' because of error: %v", cfg.Config.GetAddressBookPath(), errAddressBook))

		// Select an actual VCenter connection from the address book
		vcConnection, errAddressBookLookup := addressBook.GetSelectedVSphereConnection(TEST_VCENTER_CONNECTION_NAME)
		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection, "Could not get connection")

		// Get the ManagedObjectReference for the VM and test
		if vcSoapConnection, errLogin := vcenterutil.VCenterSoapLogin(ctx, vcConnection); errLogin != nil {
			// Failed
			panic(errLogin)
		} else {
			// NOTE: Update path and host regexp to filter Datastore records by name as needed.  Use "" to not filter at all.
			if outputChannel, errVCenter := ListDatastoreRecords(ctx, "", "", false, false, vcSoapConnection); errVCenter != nil {
				panic(errVCenter)
			} else {
				// Will store results from the channel as they arrive
				var datastores = make([]*api.DatastoreResponse, 0)

				// Loop through the Go channel object and save results to the array
				for {
					var datastoreResponse = <-outputChannel // Get response from channel as they arrive (a null reference indicates channel completion)

					if datastoreResponse != nil {
						fmt.Sprintf(datastoreResponse.Name);
						datastores = append(datastores, datastoreResponse);
					} else {
						// All hypervisors received from the channel
						break
					}
				}

				// This is a good place to place a debug breakpoint to deep inspect the results
				assert.False(t, len(datastores) == 0)
			}
		}
	})
}
