package vcenter

import (
	"context"
	"dora/pkg/api"
	"dora/pkg/config"
	"dora/pkg/logger"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"dora/pkg/util/threadsafequeue"
	"dora/pkg/vcenter/tags"
	"dora/pkg/vcenterutil"
	"errors"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/vmware/govmomi/list"
	"github.com/vmware/govmomi/vim25/mo"
)

// Performs and asynchronous query of Hypervisor objects matching the given query string.
//
// This method runs in the background and returns immediately.  The returned channel object
// will have results appear as soon as they are available from the internal worker threads.
func ListHypervisorRecords(ctx context.Context, pathRegexp string, hostRegexp string, listingOnly bool, providePreview bool, vcSoapConnection *vcenterutil.VcSoapConnection) (chan *api.HypervisorResponse, error) {
	if list, errSearch := listVCenterRecords(ctx, vcSoapConnection, KIND_HOST_SYSTEM, pathRegexp, hostRegexp); errSearch != nil {
		// Fatal -- query syntax error, connection error, etc.
		return nil, errors.New(fmt.Sprintf("Search failed for query (%s, %s) because of error: %v", pathRegexp, hostRegexp, errSearch))
	} else {
		var responseChannel = make(chan *api.HypervisorResponse, len(list))

		logSyncHistoryInfo(ctx, fmt.Sprintf("Starting async retrieval of %d hypervisor record details from VCenter.  This will take a while...", len(list)))

		// Process in the background in a Go routine
		go retrieveHypervisorDetailRecords(ctx, vcSoapConnection, responseChannel, list, providePreview, listingOnly)

		return responseChannel, nil
	}
}


// Performs SOAP style processing of elements.  This is most compatible but slow.
//
// This function operates asynchronously and will return immediately but the given channel object will receive the
// Hypervisor records as the threads work in the background to retrieve the records from VCenter.  A Nil object will
// be placed into the channel at the end of this process which signals completion that all Hypervisor records have
// been delivered to the channel.
//
func retrieveHypervisorDetailRecords(ctx context.Context, vcSoapConnection *vcenterutil.VcSoapConnection, channel chan *api.HypervisorResponse, elements []list.Element, providePreview bool, listOnly bool) {
	var spanParent = util.GetParentSpan(ctx)
	var spanProcessElements = spanParent.Tracer().StartSpan(profiling.OP_GETHYPERVISOR_DETAILS, opentracing.ChildOf(spanParent.Context()))

	// VCenter balancing tags (racks) will be populated into here.  Readers can read it after the writer go routine
	// as populated it
	var tagRackMapping = tags.NewRackTagMap(vcSoapConnection)
	var tagHypervisorMapping = tags.NewHypervisorTagMap(vcSoapConnection)

	// Get the balancing tags from VCenter which is used to populate the "rack" field in each hypervisor record.
	// Designed to be run asynchronously but will lock on a RWMutex to prevent read access until population is done.
	var asyncContext = context.Background()
	go tagRackMapping.Populate(asyncContext)
	go tagHypervisorMapping.Populate(asyncContext)

	// Summary of found objects first
	logger.Log.Info(fmt.Sprintf("Total %d objects", len(elements)))
	if providePreview || listOnly {
		for _, vmElement := range elements {
			logger.Log.Info(vmElement.Path)

			// Send the preview rows now.  This finishes quickly and the UI can setup the rows in its tables and use the
			// count for progress bar setup
			channel <- &api.HypervisorResponse{
				Path:               vmElement.Path,
				UiControlIsPreview: true,
			}
		}

		if listOnly {
			// User requested "list only" so can terminate here
			channel <- nil
			return
		}
	}

	// Now get details for each object
	logger.Log.Info(fmt.Sprintf("Getting details for %d objects...", len(elements)))

	if len(elements) == 0 {
		// No matches so no further work needs to be done
		channel <- nil
		return
	}

	var workQueue = threadsafequeue.New(elements)                     // Wrap work items in a thread safe queue for multiple threads to work off
	var maxWorkerThreads = int(config.Config.GetVCenterWorkThreads()) // Max worker threads count comes from config
	var countQueue = threadsafequeue.New(nil)                         // Use a thread safe queue to keep track of number of items processed -- required to know when streaming is complete
	logger.Log.Info(fmt.Sprintf("Starting at most %d threads for LS execution...", maxWorkerThreads))

	const MAX_ENTRY_RETRIES = 3
	var mapRetries = make(map[string]int)

	// Start a pre-defined quantity of worker threads to process the work queue
	for i := 0; i < maxWorkerThreads; i++ {
		go func() {
			var listElement *list.Element

		loop:
			for true {
				switch obj := workQueue.Pull().(type) {
				case list.Element:
					// Got a work item so process it
					//
					listElement = &obj
					logger.Log.Info(fmt.Sprintf("Thread processing element: %v\n", listElement))
				case threadsafequeue.EmptyMarker:
					// Signal completion to output channel if and only if all items are processed.
					//
					if countQueue.Size() >= len(elements) {
						// Last worker thread to complete will send completion signal which follows the principle of
						// "last one out turns off the lights"!
						channel <- nil

						// Actual completion is here because of multi-threaded implementation
						spanProcessElements.Finish()
					}

					// No more work left so break out work loop which allows the worker thread to terminate normally
					break loop
				}

				if listElement != nil {
					var objectRef = listElement.Object.Reference()
					if objectRef.Type == KIND_HOST_SYSTEM {
						hs, errVCenter := vcSoapConnection.Finder.HostSystem(vcSoapConnection.Ctx, listElement.Path)
						if errVCenter != nil {
							logSyncHistoryWarn(ctx, fmt.Sprintf("HostSystem Finder Error: %v", errVCenter))

							// Could not retrieve the VCenter record -- probably an network I/O timeout like:
							// '"HostSystem Finder Error: Post \"https://10.173.96.148/sdk\": net/http: TLS handshake timeout"'
							mapRetries[listElement.Path]++ // increment attempt count on the list element
							if mapRetries[listElement.Path] <= MAX_ENTRY_RETRIES {
								// RETRY LATER: Put the element on the back the queue so it can be retried
								logSyncHistoryWarn(ctx, fmt.Sprintf("Failed but will retry later.  VCenter record %s failed to load because of error: %v", listElement.Path, errVCenter))
								workQueue.Add(listElement)
							} else {
								// GIVE UP ON RECORD: Retries exhausted.  Do *not* put the failing record back on the
								// work queue.
								logSyncHistoryError(ctx, fmt.Sprintf("Exceeded retry limit of %d and giving up on VCenter hypervisor record: %s", MAX_ENTRY_RETRIES, listElement.Path))

								// Add blank entry for count.  Just need anything to push to increment the count even for
								// failed retrievals.  Otherwise the worker thread will never exit because count is never
								//reached.
								countQueue.Add("")
							}

							// Restart the loop and skip processing this failed record
							continue loop
						}

						var props = []string{"summary", "vm", "config", "network", "datastore"}
						var h mo.HostSystem
						var errProperties = hs.Properties(vcSoapConnection.Ctx, objectRef.Reference(), props, &h)
						if errProperties != nil {
							// This can timeout and requires retry logic!
							// EX: "msg"="HostSystem Props Error: Post \"https://10.173.96.148/sdk\": dial tcp 10.173.96.148:443: i/o timeout"
							logSyncHistoryWarn(ctx, fmt.Sprintf("HostSystem Props Error: %v", errProperties))

							// Could not retrieve the VCenter properties -- probably an network I/O timeout like:
							// '"HostSystem Finder Error: Post \"https://10.173.96.148/sdk\": net/http: TLS handshake timeout"'
							mapRetries[listElement.Path]++ // increment attempt count on the list element
							if mapRetries[listElement.Path] <= MAX_ENTRY_RETRIES {
								// RETRY LATER: Put the parent element on the back the queue so it can be retried
								logSyncHistoryWarn(ctx, fmt.Sprintf("Failed but will retry later.  VCenter record %s failed to load because of error: %v", listElement.Path, errProperties))
								workQueue.Add(listElement)
							} else {
								// GIVE UP ON RECORD: Retries exhausted.  Do *not* put the failing record back on the
								// work queue.
								logSyncHistoryError(ctx, fmt.Sprintf("Exceeded retry limit of %d and giving up on retrieval for VCenter hypervisor: %s", MAX_ENTRY_RETRIES, listElement.Path))

								// Add blank entry for count.  Just need anything to push to increment the count even for
								// failed retrievals.  Otherwise the worker thread will never exit because count is never
								//reached.
								countQueue.Add("")
							}

							// Restart the loop and skip processing this failed record
							continue loop
						}

						// Get CPU and Mem values if available
						var ncpu int32 = -1
						var totalMemMB int64 = -1
						var cpuUsage float64 = -1
						var memUsage float64 = -1
						var s = h.Summary
						var sH = s.Hardware
						if sH != nil {
							ncpu = int32(sH.NumCpuThreads)
							totalMemMB = sH.MemorySize / (1024 * 1024)
							cpuUsage = 100 * float64(s.QuickStats.OverallCpuUsage) / float64(ncpu*sH.CpuMhz)
							memUsage = 100 * float64(s.QuickStats.OverallMemoryUsage) / float64(sH.MemorySize>>20)
						} else {
							logSyncHistoryWarn(ctx, fmt.Sprintf("Hypervisor %s does not have any hardware properties associated with it.  Cannot get CPU or MEM properties.", listElement.Path))
						}

						// Get storage capacity if available
						var storageCapacityGB int32
						if h.Config != nil {
							storageCapacityGB = vcenterutil.CalcTotalHypervisorStorageGB(h.Config.FileSystemVolume.MountInfo)
						} else {
							storageCapacityGB = -1
							logSyncHistoryError(ctx, fmt.Sprintf("Guest VM '%s' does not contain filesystem configuration data element.  Cannot get total storage capacity for this node.", listElement.Path))
						}

						// Get boot time if system is booted
						var uptime = "Not booted"
						if s.Runtime.BootTime != nil {
							// System actually booted and has boot time
							util.FormatTime(*s.Runtime.BootTime)
						}

						// Translate VCenter record to gRPC record
						var hypervisor = api.HypervisorResponse{
							Name:          s.Config.Name,
							Path:          listElement.Path,
							CpuCount:      ncpu,              // If -1 then value could not be read
							Memory_MB:     totalMemMB,        // If -1 then value could not be read
							Hdd_GB:        storageCapacityGB, // If -1 then value could not be read
							VmCount:       int32(len(h.Vm)),
							UsageMB:       float32(memUsage), // If -1 then value could not be read
							UsageCPU:      float32(cpuUsage), // If -1 then value could not be read
							Uptime:        uptime,
							State:         string(s.Runtime.PowerState),
							Status:        string(s.OverallStatus),
							Vmotion:       s.Config.VmotionEnabled,
							NetPortId:     "",                                                           // TODO
							NetSystemName: "",                                                           // TODO
							Rack:          tagRackMapping.GetRack(listElement.Object.Reference().Value), // Acquired from previously retrieved VCenter rack tags lookup.  If "" then the hypervisor is not tagged with any rack.
							Tags:          tagHypervisorMapping.GetTags(listElement.Object.Reference().Value),
							ManagedObjRef: vcenterutil.GetManagedObjValue(listElement.Object),
							Networks:      []string{},   // Will be populated be separate govmomi Finder call below
							Datastores:    collectRelevantDatastoreNames(vcSoapConnection, h),
						}

						// Perform separate call to get the Hypervisor's child network paths
						if h.Network != nil {
							for _, networkRef := range h.Network {
								if listElement, errNetwork := vcSoapConnection.Finder.Element(vcSoapConnection.Ctx, networkRef); errNetwork != nil {
									// Not fatal but mark it
									logger.Log.Info(fmt.Sprintf("Lookup of child Network records for Hypervisor %s failed because of error: %v", hypervisor.GetPath(), errNetwork))
									hypervisor.Networks = append(hypervisor.Networks, errNetwork.Error())
								} else {
									// Add the network path to the VM's networks array
									hypervisor.Networks = append(hypervisor.Networks, listElement.Path)
								}
							}
						}

						// Message result object to the Channel -- this also signals thread completion
						channel <- &hypervisor
						countQueue.Add(hypervisor)
					}
				}
			}
		}()
	}
}

// Returns a string array of zero or more related datastore records of the given VCenter Hypervisor host object
//
func collectRelevantDatastoreNames(vcSoapConnection *vcenterutil.VcSoapConnection, host mo.HostSystem) []*api.Datastore {
	var results = make([]*api.Datastore, 0)

	if host.Datastore != nil {
		if datastores, errDatastores := vcenterutil.GetDatastoreInfo(vcSoapConnection, host.Datastore); errDatastores != nil {
			// Not fatal but log it
			logSyncHistoryWarn(vcSoapConnection.Ctx, fmt.Sprintf("Datastore retrieval error: %v", errDatastores))
		} else {
			for _, datastore := range datastores {
				results = append(results, &api.Datastore{
					DatastorePath: datastore.Path,
					DatastoreUrl:  datastore.Url,
				})
			}
		}
	}

	return results
}