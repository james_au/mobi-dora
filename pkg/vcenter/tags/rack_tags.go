package tags

import (
	"context"
	cfg "dora/pkg/config"
	"dora/pkg/logger"
	"dora/pkg/vcenterutil"
	"errors"
	"fmt"
	govmomirest "github.com/vmware/govmomi/vapi/rest"
	"github.com/vmware/govmomi/vapi/tags"
	"net/url"
	"sync"
	"time"
)

//
// This code retrieves VCenter "rack" tags and indexes them based on VCenter hypervisor host.  It operates quickly using
// VCenter's REST API.
//
// The rack information can then be added into the Hypervisor record as another field and is used during "balancing"
// operations
//

// Return type is defined by this struct which has multiple fields to define the tag mappings results
//
type RackTagMap struct {
	HypervisorToTagMapping map[string]tags.Tag // Do not use directly -- use getter method instead as it is synchronized
	ErrorMapping           map[*tags.Tag]error // Asynchronous errors populated here.  Should be displayed on some UI or output to the user to know tag retrieval was not 100% successful
	FatalError             error
	tagRetrievalMutex      sync.RWMutex                  // This RW lock allows the Writer to lock until write completion then all Readers have full access
	vcSoapConnection       *vcenterutil.VcSoapConnection // Contains a live connection to the VCenter include username/password to create a tag REST client
}

// Creates a new tag mapping instance
//
func NewRackTagMap(vcSoapConnection *vcenterutil.VcSoapConnection) *RackTagMap {
	var tagMappings = RackTagMap{}

	// Initialize data mappings which will be populated by code below
	tagMappings.vcSoapConnection = vcSoapConnection
	tagMappings.HypervisorToTagMapping = make(map[string]tags.Tag)
	tagMappings.ErrorMapping = make(map[*tags.Tag]error)

	return &tagMappings
}

// Uses VCenter REST calls to get the tagged inventory for balancing. This method is designed to be
// run asynchronously so errors are not returned but can be accessed by the error fields on this
// object after the run is completed.
//
// Ref: https://godoc.org/github.com/vmware/govmomi/vapi/tags
func (o *RackTagMap) Populate(ctx context.Context) {
	// Lock with the RWMutex in exclusive "write mode"
	o.tagRetrievalMutex.Lock()
	defer o.tagRetrievalMutex.Unlock()

	var startTime = time.Now()

	// Create the VCenter Rest client and set the username and password because it peforms its own auth
	var restClient = govmomirest.NewClient(o.vcSoapConnection.VimClient)
	if errRest := restClient.Login(ctx, url.UserPassword(o.vcSoapConnection.VcAddressRec.Username, o.vcSoapConnection.VcAddressRec.Password)); errRest != nil {
		// Fatal
		var err = errors.New(fmt.Sprintf("Tag mapping could not create REST connection because of error: %v", errRest))
		o.FatalError = err
	} else {
		// Initialize the govmomi tag manager which contains the tag API we need
		var tagManager = tags.NewManager(restClient)

		if category, errCategory := tagManager.GetCategory(ctx, cfg.Config.GetBalanceCategory()); errCategory != nil {
			// Fatal
			o.FatalError = errCategory
		} else {
			logger.Log.Info(fmt.Sprintf("Found categoryID for category name '%s': %s", cfg.Config.GetBalanceCategory(), category.ID))
			if tags, errTags := tagManager.GetTagsForCategory(ctx, category.ID); errTags != nil {
				// Fatal
				o.FatalError = errTags
			} else {
				logger.Log.Info(fmt.Sprintf("Found %d tags for category '%s':", len(tags), cfg.Config.GetBalanceCategory()))
				for _, tag := range tags {
					logger.Log.Info("   " + tag.Name)
				}

				// For each rack tag, get all VCenter objects (presuming Hypervisor host) tagged with the rack tag
				for _, tag := range tags {
					if managedObjRefs, errGetAttached := tagManager.ListAttachedObjects(ctx, tag.ID); errGetAttached != nil {
						// Not fatal but log as a warning because it severely affects data integrity
						o.ErrorMapping[&tag] = errors.New(fmt.Sprintf("Could get attached objects for tag '%v'.  Some balancing rack information will be missing.  Caused by error: %v", tag, errGetAttached))
					} else {
						logger.Log.Info(fmt.Sprintf("Found %d VCenter object(s) tagged with '%s'", len(managedObjRefs), tag.Name))

						// Save all managed objects refs to the map for fast lookup.  The caller will have to
						// perform mapping of managed object ref to VCenter object as necessary.
						for _, managedObjRef := range managedObjRefs {
							logger.Log.Info(fmt.Sprintf("   %s", managedObjRef.Reference().Value))
							o.HypervisorToTagMapping[managedObjRef.Reference().Value] = tag
						}
					}
				}
			}
		}
	}

	// Output summary information to log output
	var completionSecs = (float64(time.Now().UnixNano()) - float64(startTime.UnixNano())) / 1000000000
	if o.FatalError != nil {
		logger.Log.Error(o.FatalError, fmt.Sprintf("Tag mapping could not complete because of fatal error: %v", o.FatalError))
	} else {
		logger.Log.Info(fmt.Sprintf("Processed %d tagged items with %d errors in %.2f seconds", len(o.HypervisorToTagMapping), len(o.ErrorMapping), completionSecs))
	}
}

// Returns the raw tag for the given host key.  The rack name can be obtained from the tag's Name field.
//
// NOTE:
// If no matching key found, then a nil reference is returned instead
//
// NOTE: This method uses a RWLock to wait for writer/producer thread to complete population first.  The producer
// thread should finish quickly since it is based on VCenter REST API but the lock guarantees no readers can access
// the data unil the population process is completed.
//
func (o *RackTagMap) GetTagObj(hostMoRef string) *tags.Tag {
	// Lock with the RWMutex in shared "read mode"
	o.tagRetrievalMutex.RLock()
	defer o.tagRetrievalMutex.RUnlock()

	_, containsKey := o.HypervisorToTagMapping[hostMoRef]
	if containsKey {
		// Actual key found
		var tagObj = o.HypervisorToTagMapping[hostMoRef]
		return &tagObj
	} else {
		// Return blank if no key found
		return nil
	}
}

// Returns the rack name only from the given host key (typically a hypervisor host like "host-2837")
//
// NOTE:
// If no matching key found, then an empty string "" is returned instead
//
func (o *RackTagMap) GetRack(hostMoRef string) string {
	var tag = o.GetTagObj(hostMoRef)

	if tag != nil {
		return tag.Name
	} else {
		return ""
	}
}
