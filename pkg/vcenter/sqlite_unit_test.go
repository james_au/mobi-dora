package vcenter

import (
	"context"
	"crawshaw.io/sqlite"
	"crawshaw.io/sqlite/sqlitex"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

//
// Test the SQLite crawshaw.io implementation.
//
// Refs:
// (1) https://github.com/crawshaw/sqlite
// (2) https://godoc.org/crawshaw.io/sqlite
//


// Test basic DDL creation, INSERT and then SELECT back the data using the crashaw.io/sqlite library.
//
func Test001_CreateSelect(t *testing.T) {
	assert.NotPanics(t, func() {
		// Connect to SQLite
		//
		dbpool, errOpen := sqlitex.Open("file:memory:?mode=memory", 0, 10)
		//dbpool, errOpen := sqlitex.Open("file:dora.sqlite", 0, 10)
		assert.Nil(t, errOpen);

		var conn = dbpool.Get(context.Background())
		assert.NotNil(t, conn);

		//defer dbpool.Put(conn)
		assert.True(t, conn.GetAutocommit())

		// Create the test database table
		stmtCREATE, errCreate := conn.Prepare(`CREATE TABLE IF NOT EXISTS Hypervisor (path TEXT PRIMARY KEY, cpus INTEGER, mem_mb INTEGER, vm_count INTEGER, mem_usage_mb FLOAT, cpu_usage_ghz FLOAT, uptime STRING, state STRING, vmotion_enabled INTEGER )`)
		assert.Nil(t, errCreate)
		_, errStmt := stmtCREATE.Step()
		assert.Nil(t, errStmt)

		// Insert two rows for testing using a compiled prepared statement
		//
		stmtINSERT, errInsert := conn.Prepare(`INSERT INTO Hypervisor VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`)
		assert.Nil(t, errInsert)
		{
			var bindIncrementor = sqlite.BindIncrementor()
			stmtINSERT.BindText(bindIncrementor(), "/PAYTV-PSR/host/g-04-39.infra.smf1.mobitv/g-04-39.infra.smf1.mobitv")
			stmtINSERT.BindInt64(bindIncrementor(), 96)
			stmtINSERT.BindInt64(bindIncrementor(), 391648)
			stmtINSERT.BindInt64(bindIncrementor(), 18)
			stmtINSERT.BindFloat(bindIncrementor(), 67.3434)
			stmtINSERT.BindFloat(bindIncrementor(), 12.3212846)
			stmtINSERT.BindText(bindIncrementor(), "2020-01-08T04:40:15Z") // SQLite doesn't have a DATE type
			stmtINSERT.BindText(bindIncrementor(), "poweredOn")
			stmtINSERT.BindInt64(bindIncrementor(), 1)   // SQLite doesn't have BOOL type -- just use 0 or 1
			_, errStep := stmtINSERT.Step()  // execute
			assert.Nil(t, errStep)
			stmtINSERT.Reset() // reset must be called in order to re-use the statement object
		}
		{
			var bindIncrementor = sqlite.BindIncrementor()
			stmtINSERT.BindText(bindIncrementor(), "/ASPERA EXT/host/f-03-37.infra.smf1.mobitv/f-03-37.infra.smf1.mobitv")
			stmtINSERT.BindInt64(bindIncrementor(), 24)
			stmtINSERT.BindInt64(bindIncrementor(), 106485)
			stmtINSERT.BindInt64(bindIncrementor(), 1)
			stmtINSERT.BindFloat(bindIncrementor(), 67.3434)
			stmtINSERT.BindFloat(bindIncrementor(), 12.3212846)
			stmtINSERT.BindNull(bindIncrementor()) // SQLite doesn't have a DATE type
			stmtINSERT.BindText(bindIncrementor(), "poweredOn")
			stmtINSERT.BindInt64(bindIncrementor(), 1)   // SQLite doesn't have BOOL type -- just use 0 or 1
			_, errStep := stmtINSERT.Step()  // execute
			assert.Nil(t, errStep)
			stmtINSERT.Reset() // reset must be called in order to re-use the statement object
		}
		stmtINSERT.Finalize()  // Close the statement object

		// Select back the rows inserted and verify they were inserted correctly
		//
		stmtSELECT, errSelect := conn.Prepare(`SELECT * FROM Hypervisor`)
		assert.Nil(t, errSelect)
		var count = 0
		for {
			hasRow, errStep := stmtSELECT.Step()
			assert.Nil(t, errStep)
			if !hasRow {
				// No more rows remaining
				break
			} else {
				count++

				fmt.Printf("Hypervisor path: (%s, %d)\n",
					stmtSELECT.GetText("path"),
					stmtSELECT.GetInt64("cpus"),
				)
			}
		}

		assert.Equal(t, 2, count, "SELECT did not retrieve the number of records in INSERT")

		conn.Close()
	});
}

// Test SQLite transaction with rollback via a crawshaw api
//
func Test002_TransactionsWithRollback(t *testing.T) {
	// Connect to SQLite.  This test requires an actual file database (not memory).  If memory database is used, the
	// rollback doesn't appear to function properly.
	//
	//dbpool, errOpen := sqlitex.Open("file::memory:?mode=memory", 0, 10)
	const databaseFile = "dora-unit-test-db.sqlite"

	// Must remove any existing database first to ensure consistent start state for the test
	os.Remove(databaseFile)

	// Create the file based SQLite database
	dbpool, errOpen := sqlitex.Open("file:" + databaseFile, 0, 10)
	assert.Nil(t, errOpen);

	var conn = dbpool.Get(context.Background())
	assert.NotNil(t, conn);
	defer dbpool.Put(conn)

	assert.True(t, conn.GetAutocommit())
	// Create the test database table
	stmtCREATE, errCreate := conn.Prepare(`CREATE TABLE IF NOT EXISTS Hypervisor (path TEXT PRIMARY KEY, cpus INTEGER, mem_mb INTEGER, vm_count INTEGER, mem_usage_mb FLOAT, cpu_usage_ghz FLOAT, uptime STRING, state STRING, vmotion_enabled INTEGER)`)
	assert.Nil(t, errCreate)
	assert.NotNil(t, stmtCREATE)
	_, errStmt := stmtCREATE.Step()
	assert.Nil(t, errStmt)

	// Use a new connection to the database representing a separate user or process to insert rows into the database
	// that will end up being rolled back and will *not* be available
	var conn2 = dbpool.Get(context.Background())
	assert.NotNil(t, conn2)
	defer dbpool.Put(conn2)
	insert(t, conn2)

	// Select back the rows inserted and verify they were rolled back correctly.  Use a new connection
	// to the database representing a separate user or process.
	//
	var conn3 = dbpool.Get(context.Background())
	assert.NotNil(t, conn3)
	defer dbpool.Put(conn3)
	stmtSELECT, errSelect := conn3.Prepare(`SELECT * FROM Hypervisor`)
	assert.Nil(t, errSelect)
	var count = 0
	for {
		hasRow, errStep := stmtSELECT.Step()
		assert.Nil(t, errStep)
		if !hasRow {
			// No more rows remaining
			break
		} else {
			count++

			fmt.Printf("Hypervisor path: (%s, %d)\n",
				stmtSELECT.GetText("path"),
				stmtSELECT.GetInt64("cpus"),
			)
		}
	}

	//assert.Equal(t, 2, count, "SELECT did not retrieve the number of records in INSERT")
	assert.Equal(t, 0, count, "Rollback did not occur.  Should have made 0 rows available")

}

// Inserts two test records into the database via the provdied database connection.  The table to insert into
// is assumed to be already created in the database.
func insert(t *testing.T, conn *sqlite.Conn) error {
	var err error = nil
	defer sqlitex.Save(conn)(&err)
	assert.False(t, conn.GetAutocommit())  // Autocommit gets disabled when a savepoint is created on the connection

	// ** KEY FUNCTION FOR ROLLBACK **
	// Defer the ROLLBACK or COMMIT based on if the referenced error object is NIL or not
	//defer dbSavePointFunction(&err)

	// Insert two rows for testing using a compiled prepared statement
	//
	stmtINSERT, errInsert := conn.Prepare(`INSERT INTO Hypervisor VALUES($path, $cpus, $mem_mb, $vm_count, $mem_usage_mb, $cpu_usage_ghz, $uptime, $state, $vmotion_enabled)`)
	assert.Nil(t, errInsert)
	{
		stmtINSERT.SetText("$path", "/PAYTV-PSR/host/g-04-39.infra.smf1.mobitv/g-04-39.infra.smf1.mobitv")
		stmtINSERT.SetInt64("$cpus", 96)
		stmtINSERT.SetInt64("$mem_mb", 391648)
		stmtINSERT.SetInt64("$vm_count", 18)
		stmtINSERT.SetFloat("$mem_usage_mb", 67.3434)
		stmtINSERT.SetFloat("$cpu_usage_ghz", 12.3212846)
		stmtINSERT.SetText("$uptime", "2020-01-08T04:40:15Z") // SQLite doesn't have a DATE type
		stmtINSERT.SetText("$state", "poweredOn")
		stmtINSERT.SetInt64("$vmotion_enabled", 1)   // SQLite doesn't have BOOL type -- just use 0 or 1
		_, errStep := stmtINSERT.Step()  // execute
		assert.Nil(t, errStep)
		stmtINSERT.Reset() // reset must be called in order to re-use the statement object
	}
	{
		stmtINSERT.SetText("$path", "/ASPERA EXT/host/f-03-37.infra.smf1.mobitv/f-03-37.infra.smf1.mobitv")
		stmtINSERT.SetInt64("$cpus", 24)
		stmtINSERT.SetInt64("$mem_mb", 106485)
		stmtINSERT.SetInt64("$vm_count", 1)
		stmtINSERT.SetFloat("$mem_usage_mb", 67.3434)
		stmtINSERT.SetFloat("$cpu_usage_ghz", 12.3212846)
		stmtINSERT.SetNull("$uptime") // SQLite doesn't have a DATE type
		stmtINSERT.SetText("$state", "poweredOn")
		stmtINSERT.SetInt64("$vmotion_enabled", 1)   // SQLite doesn't have BOOL type -- just use 0 or 1
		_, errStep := stmtINSERT.Step()  // execute
		assert.Nil(t, errStep)
		stmtINSERT.Reset() // reset must be called in order to re-use the statement object
	}

	// Ensure the rows were actually created within the transaction even though they haven't been committed yet
	// since this is still operating within the original transaction block
	//
	stmtSELECT, errSelect := conn.Prepare(`SELECT * FROM Hypervisor`);
	assert.Nil(t, errSelect)
	var count = 0
	for {
		hasRow, errStep := stmtSELECT.Step()
		assert.Nil(t, errStep)
		if !hasRow {
			// No more rows remaining
			break
		} else {
			count++

			fmt.Printf("Hypervisor path: (%s, %d)\n",
				stmtSELECT.GetText("path"),
				stmtSELECT.GetInt64("cpus"),
			)
		}
	}
	assert.Equal(t, 2, count, "Rows must be created and viewable within the transaction.")

	// Simulate an error which will then cause rollback in the defered function.  After rollback, other database
	// connections must *not* be able to see the table created above above.
	err = errors.New("Database execution failed. :-(")

	return err
}