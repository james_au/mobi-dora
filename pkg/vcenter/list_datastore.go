package vcenter

import (
	"context"
	"dora/pkg/api"
	"dora/pkg/config"
	"dora/pkg/logger"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"dora/pkg/util/threadsafequeue"
	"dora/pkg/vcenterutil"
	"errors"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/vmware/govmomi/list"
)

// Performs and asynchronous query of Hypervisor objects matching the given query string.
//
// This method runs in the background and returns immediately.  The returned channel object
// will have results appear as soon as they are available from the internal worker threads.
func ListDatastoreRecords(ctx context.Context, pathRegexp string, datastoreNameRegexp string, listingOnly bool, providePreview bool, vcSoapConnection *vcenterutil.VcSoapConnection) (chan *api.DatastoreResponse, error) {
	if list, errSearch := listVCenterRecords(ctx, vcSoapConnection, KIND_DATASTORE, pathRegexp, datastoreNameRegexp); errSearch != nil {
		// Fatal -- query syntax error, connection error, etc.
		return nil, errors.New(fmt.Sprintf("Search failed for query (%s, %s) because of error: %v", pathRegexp, datastoreNameRegexp, errSearch))
	} else {
		var responseChannel = make(chan *api.DatastoreResponse, len(list))

		logSyncHistoryInfo(ctx, fmt.Sprintf("Starting async retrieval of %d hypervisor record details from VCenter.  This will take a while...", len(list)))

		// Process in the background in a Go routine
		go retrieveDatastoreDetailRecords(ctx, vcSoapConnection, responseChannel, list, providePreview, listingOnly)

		return responseChannel, nil
	}
}


// Retreives Datastore records from VCenter via its SOAP APIs
//
// NOTE: This function operates asynchronously and will return immediately with a channel object that the caller can listen
// for results
// NOTE: The VCenter SOAP protocol can be quiet slow
//
func retrieveDatastoreDetailRecords(ctx context.Context, vcSoapConnection *vcenterutil.VcSoapConnection, channel chan *api.DatastoreResponse, elements []list.Element, providePreview bool, listOnly bool) {
	var spanParent = util.GetParentSpan(ctx)
	var spanProcessElements = spanParent.Tracer().StartSpan(profiling.OP_GETDATASTORE_DETAILS, opentracing.ChildOf(spanParent.Context()))

	// Summary of found objects first
	logger.Log.Info(fmt.Sprintf("Total %d objects", len(elements)))
	if providePreview || listOnly {
		for _, vmElement := range elements {
			logger.Log.Info(vmElement.Path)

			// Send the preview rows now.  This finishes quickly and the UI can setup the rows in its tables and use the
			// count for progress bar setup.
			channel <- &api.DatastoreResponse{
				Name:               vmElement.Path,
				UiControlIsPreview: true,
			}
		}

		if listOnly {
			// Can terminate here since user requested "list only"
			channel <- nil
			return
		}
	}

	// Now get details for each object
	logger.Log.Info(fmt.Sprintf("Getting details for %d objects...", len(elements)))

	if len(elements) == 0 {
		// No matches so no further work needs to be done
		channel <- nil
		return
	}

	var workQueue = threadsafequeue.New(elements)                     // Wrap work items in a thread safe queue for multiple threads to work off
	var maxWorkerThreads = int(config.Config.GetVCenterWorkThreads()) // Max worker threads count comes from config
	var countQueue = threadsafequeue.New(nil)                         // Use a thread safe queue to keep track of number of items processed -- required to know when streaming is complete
	logger.Log.Info(fmt.Sprintf("Starting at most %d threads for LS execution...", maxWorkerThreads))

	const MAX_ENTRY_RETRIES = 3
	var mapRetries = make(map[string]int)

	// Start a pre-defined quantity of worker threads to process the work queue
	for i := 0; i < maxWorkerThreads; i++ {
		go func() {
			var listElement *list.Element

		loop:
			for true {
				switch obj := workQueue.Pull().(type) {
				case list.Element:
					// Got a work item so process it
					//
					listElement = &obj
					logger.Log.Info(fmt.Sprintf("Thread processing element: %v\n", listElement))
				case threadsafequeue.EmptyMarker:
					// Signal completion to output channel if and only if all items are processed.
					//
					if countQueue.Size() >= len(elements) {
						// Last worker thread to complete will send completion signal which follows the principle of
						// "last one out turns off the lights"!
						channel <- nil

						// Actual completion is here because of multi-threaded implementation
						spanProcessElements.Finish()
					}

					// No more work left so break out work loop which allows the worker thread to terminate normally
					break loop
				}

				if listElement != nil {
					var objectRef = listElement.Object.Reference()
					if objectRef.Type == KIND_DATASTORE {
						datastoreDetailStruct, errVCenter := vcenterutil.RetrieveDatastoreDetailRecord(vcSoapConnection, listElement)
						if errVCenter != nil {
							logSyncHistoryWarn(ctx, fmt.Sprintf("VM lookup for object '%v' failed. Cause = %v", objectRef.Reference().Value, errVCenter))

							// Could not retrieve the VCenter record -- probably an network I/O timeout like:
							// '"HostSystem Finder Error: Post \"https://10.173.96.148/sdk\": net/http: TLS handshake timeout"'
							mapRetries[listElement.Path]++ // increment attempt count on the list element
							if mapRetries[listElement.Path] <= MAX_ENTRY_RETRIES {
								// RETRY LATER: Put the element on the back the queue so it can be retried
								logSyncHistoryWarn(ctx, fmt.Sprintf("Failed but will retry later.  VCenter record %s failed to load because of error: %v", listElement.Path, errVCenter))
								workQueue.Add(listElement)
							} else {
								// GIVE UP ON RECORD: Retries exhausted.  Do *not* put the failing record back on the
								// work queue.
								logSyncHistoryError(ctx, fmt.Sprintf("Exceeded retry limit of %d and giving up on VCenter record: %s", MAX_ENTRY_RETRIES, listElement.Path))

								// Add blank entry for count.  Just need anything to push to increment the count even for
								// failed retrievals.  Otherwise the worker thread will never exit because count is never
								//reached.
								countQueue.Add("")
							}

							// Restart the loop and skip processing the failed record
							continue loop
						}

						datastoreDetailStruct.UiControlIsPreview = false

						channel <- datastoreDetailStruct
						countQueue.Add(datastoreDetailStruct)
					}
				}
			}
		}()
	}
}
