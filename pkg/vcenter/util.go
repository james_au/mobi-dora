package vcenter

import (
	"context"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/vcenterutil"
	"errors"
	"fmt"
	"github.com/vmware/govmomi/list"
	"github.com/vmware/govmomi/property"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25/types"
	"regexp"
)

//
// Provides utility method for working with VCenter
//

// Returns the FQDN from a given VM Path that contains the FQDN at end
//
func VmFqdnFromReferencePath(refPath string) string {
	pattern := regexp.MustCompile("^.+/([^/]+)$")
	matches := pattern.FindAllStringSubmatch(refPath, -1)

	if len(matches) == 1 {
		return matches[0][1]
	} else {
		// The path given is already a non-full FQDN
		return refPath
	}
}

// Performs VCenter search for records matching the name.  This performs quickly unlike most VCenter APIs because
// it only returns a named "Element" references to the object *without* details.  The Elements can then be used
// to perform a full search for the full details of the record.
//
// Params:
// (1) vcSoapConnection   An active unexpired connection to VCenter
// (2) searchGlobPattern  A glob search pattern defined by VCenter's own internal search.  Can use "*" to return a complete list
// (3) kind               The kind of VCenter record type to search for "HostSystem", "Virtual Machine", ... to limit search to
//
func Search(vcSoapConnection *vcenterutil.VcSoapConnection, searchGlobPattern string, kind string) ([]list.Element, error) {
	var rootC types.ManagedObjectReference
	var dataCenter = "" // <-- hard coding datacenter to "" as we do not use this field

	if len(dataCenter) > 0 {
		dc, err := vcSoapConnection.Finder.ManagedObjectList(vcSoapConnection.Ctx, dataCenter)
		if err != nil || dc == nil || len(dc) != 1 {
			return nil, errors.New(fmt.Sprintf("DataCenter Finder Error: %s", err))
		}

		rootC = dc[0].Object.Reference()
	} else {
		rootC = vcSoapConnection.VcClient.ServiceContent.RootFolder
	}

	vcM := view.NewManager(vcSoapConnection.VcClient.Client)
	v, err := vcM.CreateContainerView(vcSoapConnection.Ctx, rootC, []string{kind}, true)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("vCenter Container View Error: %s", err))
	}

	filter := property.Filter{}
	filter["name"] = searchGlobPattern
	objs, err := v.Find(vcSoapConnection.Ctx, []string{kind}, filter)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Find Error: %s", err))
	}

	var es []list.Element
	for _, o := range objs {
		e, err := vcSoapConnection.Finder.Element(vcSoapConnection.Ctx, o)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Element Finder Error: %s", err))
		}

		es = append(es, *e)
	}

	return es, nil
}

// Lists VCenter records of given type (HOST, VM, DATASTORE, etc) via substring regexp match.
//
// NOTE: This method performs full regexp matching unlike the lower level search() which only does
// VCenter glob searching.
//
// Params:
// (1) vcSoapConnection   An active unexpired connection to VCenter
// (2) kind               Enumerated "kind" of VCenter record to lookup (see constant enumeration)
// (3) regexpVcPath       regexp pattern string to filter on the VC Path
// (4) regexpObjectName   regexp pattern string to filter on the VCenter object name (coudl be VM name, host name, datastore name, etc depending on the "kind")
//
func listVCenterRecords(ctx context.Context, vcSoapConnection *vcenterutil.VcSoapConnection, kind string, regexpVcPath string, regexpObjectName string) ([]list.Element, error) {
	var attemptCount = 0

	// Use retry loop
	for {
		attemptCount++

		// List search results will be populated into this array
		var searchResults []list.Element

		// NOTE: Use VCenter glob pattern "*" to get a list of *all* VCenter record names -- will perform
		// own regexp filter on the full list below.  Because the regexp search provides most control of search.
		if es, errSearch := Search(vcSoapConnection, "*", kind); errSearch != nil {
			// Fatal
			var errorMessage = errors.New(fmt.Sprintf("Fatal search error occurred with search (pathFilter: '%s', objectNameFilter '%s') listing VCenter records: %v", regexpVcPath, regexpObjectName, errSearch))

			if attemptCount >= MAX_RETRIES {
				// Retries exhausted
				return nil, errors.New(fmt.Sprintf("Retries %d out of %d exhausted: %v", attemptCount, MAX_RETRIES, errorMessage))
			} else {
				// Retry attempts still available
				logger.Log.Info(fmt.Sprintf("Got error but will retry.  Retry attempt %d of %d.  Error: %v", attemptCount, MAX_RETRIES, errorMessage))
			}
		} else {
			// Create own regexp search pattern
			if patternPath, errRegExp := regexp.Compile(regexpVcPath); errRegExp != nil {
				// Fatal -- no need for retry on regex failure
				return nil, errors.New(fmt.Sprintf("Could NOT compile given regexp pattern '%s' because of error: %v", regexpVcPath, errRegExp))
			} else if patternHost, errRegExp := regexp.Compile(regexpObjectName); errRegExp != nil {
				// Fatal -- no need for retry on regex failure
				return nil, errors.New(fmt.Sprintf("Could NOT compile given regexp pattern '%s' because of error: %v", regexpObjectName, errRegExp))
			} else {
				// Filter the list down based on regexp engine match on the user provided "path" and "host" fields of
				// each returned VCenter record
				for _, element := range es {
					// Must match both the given VCenter path regex and the object name regex but either could be blank "" or "." to match anything
					if patternPath.Match([]byte(element.Path)) && patternHost.Match([]byte(ExtractHostnameFromVcPath(element.Path))) {
						// Matched.  So add to output list for return to the user.
						searchResults = append(searchResults, element)
					}
				}

				logSyncHistoryInfo(ctx, fmt.Sprintf("Found %d VCenter %s records", len(searchResults), kind))

				// No errors at this point and results in the list
				return searchResults, nil
			}
		}
	}
}

// Extracts the hostname from a full VC path
// Example:
// Input: "/PAYTV/vm/stsgl01p81.paytv.smf1.mobitv"
// Output: "stsgl01p81.paytv.smf1.mobitv"
//
var regexHostName = regexp.MustCompile("^.*?/([^/]+)$") // NOTE: Regexp objects are thread-safe so can be shared (ref: https://golang.org/pkg/regexp/#Regexp)
func ExtractHostnameFromVcPath(vcpath string) string {
	var matches = regexHostName.FindAllStringSubmatch(vcpath, 1)

	if len(matches) == 0 {
		return ""
	} else {
		return matches[0][1]
	}
}

// Logs an INFO SyncLogEntry into the database if the context contains the keys for DB logging.
// All downstream components that do not have direct access to the DB logger should log their
// message to this or the logSync loggers.
//
func logSyncHistoryInfo(ctx context.Context, infoMessage string) {
	if ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID) != nil && ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC) != nil {
		var syncHistoryId = ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID).(int64)
		var vcenterSync = ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC).(*VCenterSyncRec)

		vcenterSync.CreateDbSyncLogEntry(syncHistoryId, "", "", infoMessage)
	}

	// Always log to app's logger
	logger.Log.Info("DB Sync INFO: " + infoMessage)
}

// Logs a WARN SyncLogEntry into the database if the context contains the keys for DB logging.
// All downstream components that do not have direct access to the DB logger should log their
// message to this or the logSync loggers.
//
func logSyncHistoryWarn(ctx context.Context, warnMessage string) {
	if ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID) != nil && ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC) != nil {
		var syncHistoryId = ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID).(int64)
		var vcenterSync = ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC).(*VCenterSyncRec)

		vcenterSync.CreateDbSyncLogEntry(syncHistoryId, "", warnMessage, "")
	}

	// Always log to app's logger
	logger.Log.Info("DB Sync WARN: " + warnMessage)
}

// Logs an ERROR SyncLogEntry into the database if the context contains the keys for DB logging.
// All downstream components that do not have direct access to the DB logger should log their
// message to this or the logSync loggers.
//
func logSyncHistoryError(ctx context.Context, errMessage string) {
	if ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID) != nil && ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC) != nil {
		var syncHistoryId = ctx.Value(constants.CTX_KEY_SYNC_SYNCHISTORYID).(int64)
		var vcenterSync = ctx.Value(constants.CTX_KEY_SYNC_VCENTERSYNC).(*VCenterSyncRec)

		vcenterSync.CreateDbSyncLogEntry(syncHistoryId, errMessage, "", "")
	}

	// Always log to app's logger
	logger.Log.Info("DB Sync ERROR: " + errMessage)
}

// A data struct to define a VM move audit log entry
//
type VmMoveAuditLogStruct struct {
	ID                int64
	Username          string
	VCenter           string
	Hostname          string
	SessionUuid       string
	TimestampMs       int64
	DurationSec       float64
	VmPath            string
	SourceHvPath      string
	DestinationHvPath string
	Status            string
	Extra             string
}
