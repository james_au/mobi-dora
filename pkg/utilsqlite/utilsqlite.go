package utilsqlite

import (
	"fmt"
	"path/filepath"
)

// Generates the corresponding SQLite database file name for the given VCenter connection name.  This will be the
// SQLite file on disk that represents the VCenter instance such as NewPayTV, Arvig, Comporium, etc.
//
// All parts of the code must use this method so the file location is consistent regardless of where it is used in the
// application.
//
// PARAM: directory     parent directory
// PARAM: vcenterName   name of VCenter environment (OldPayTV, NewPayTV, Comporium, MSG, ATL-DR, EPB, etc.)
func GenerateDbFilePath(directory string, vcenterName string) string {
	return filepath.Join(directory, fmt.Sprintf("dora-%s.sqlite", vcenterName))
}

