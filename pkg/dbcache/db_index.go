package dbcache

import (
	"dora/pkg/vcenter"
	"time"
)

//
// Stores all the available SQLite sync objects that will be available at runtime.  One SQLite instance is
// created for each VCenter environment and these instances stored and indexed here.
//
type DbCacheIndexRec struct {
	Index     map[string]*vcenter.VCenterSyncRec
	Errors    []LogEventRec
}

// Represents an internal log event
//
type LogEventRec struct {
	Timestamp  time.Time
	Message    string
}

// Create a new instance of the cache.  There should only be one instance for the server.
//
func NewDbIndex() *DbCacheIndexRec {
	var dbCacheIndex = DbCacheIndexRec{}

	dbCacheIndex.Index = make(map[string]*vcenter.VCenterSyncRec)

	return &dbCacheIndex
}

// Add a new SQLite sync object.  This is usually done at server startup.
//
func (o *DbCacheIndexRec) AddSyncObject(name string, syncObj *vcenter.VCenterSyncRec) {
	o.Index[name] = syncObj
}

// Get the SQLite sync object matching the name which provides access to all of that instance's data.  This
// would be called whenever a user or cron access needs access to the cache to do a VCenter lookup.
//
func (o *DbCacheIndexRec) GetSyncObject(name string) *vcenter.VCenterSyncRec {
	return o.Index[name]
}

// Adds an error log entry to track any errors during loading process
//
func (o *DbCacheIndexRec) AddError(msg string) {
	o.Errors = append(o.Errors, LogEventRec{time.Now(), msg})
}
