package main

import (
	"context"
	"dora/pkg/ldap"
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

//
// END-TO-END (e2e) TEST: This test has dependencies on external systems that must be available and
// have the required configuration.  ALL TESTS WILL FAIL IF ALL DEPENDENCIES ARE NOT MET!
//
// TESTS
// Verify authentication against an actual LDAP server.
//

// This test user must exist in the configured LDAP server for the tests to pass
//
var (
	username = "test"
	password = "4testonly!"
)

// Basic LDAP test.  This must pass or other tests will not work either. Valid login is required.
func TestLdapGroupRetrieval(t *testing.T) {
	assert.NotPanics(t, func() {
		userGroups, ldapErr := ldap.LoginAndGetGroups(context.TODO(), username, password)

		fmt.Printf("Got usergroups array: %v\n", userGroups)

		assert.Nil(t, ldapErr)
		assert.NotEmpty(t, userGroups)
	})
}

// Performs basic test plus checks user's groups are as expected.  Valid login is required.
func TestLdapGroupsCheck(t *testing.T) {
	assert.NotPanics(t, func() {
		userGroups, ldapErr := ldap.LoginAndGetGroups(context.TODO(), username, password)
		assert.Nil(t, ldapErr)
		assert.NotEmpty(t, userGroups)

		fmt.Printf("Got usergroups array: %v\n", userGroups)

		assert.True(t, ldap.UserIsInLdapGroup("testusers", userGroups))
		assert.False(t, ldap.UserIsInLdapGroup("bogus_group", userGroups))
	})
}

// Performs test with invalid user produces an error return
func TestLdapIncorrectLogin(t *testing.T) {
	assert.NotPanics(t, func() {
		// Must use a username that does *not* exist in the system
		userGroups, ldapErr := ldap.LoginAndGetGroups(context.TODO(), "bogususername394392048", "aldjfa;lsdflk;adsjfl;")
		assert.NotEmpty(t, ldapErr)
		assert.True(t, strings.Contains(ldapErr.Error(), "Login failed"))
		assert.Empty(t, userGroups)
	})
}
