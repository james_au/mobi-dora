package vcenterutil

import (
	"context"
	"dora/pkg/addressbook"
	"dora/pkg/api"
	"dora/pkg/profiling"
	"dora/pkg/util"
	"errors"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/find"
	"github.com/vmware/govmomi/list"
	"github.com/vmware/govmomi/session"
	"github.com/vmware/govmomi/vim25"
	"github.com/vmware/govmomi/vim25/mo"
	"github.com/vmware/govmomi/vim25/soap"
	"github.com/vmware/govmomi/vim25/types"
	"net/url"
	"strings"
	"sync"
	"time"
)

// Logs into the VCenter/VSphere SOAP API using the pre-configured authentication information
// Returns loaded and auth'd connection object.
//
// NOTE: The caller is responsible to logout as necessary via the sessionManager reference
func VCenterSoapLogin(ctx context.Context, connection *addressbook.VSphereConnectionStruct) (*VcSoapConnection, error) {
	var vcURL string
	var vcUsername string
	var vcPassword string

	var spanParent = util.GetParentSpan(ctx)
	var spanVCenterLogin = spanParent.Tracer().StartSpan(profiling.OP_VCENTER_LOGIN, opentracing.ChildOf(spanParent.Context()))
	defer spanVCenterLogin.Finish()

	// Extract loaded Viper config properties for VCenter SOAP login
	if len(strings.TrimSpace(connection.Url)) > 0 {
		vcURL = connection.Url
	} else {
		return nil, errors.New(fmt.Sprintf("Cannot log into VCenter.  Connection URL is missing or blank."))
	}

	if len(strings.TrimSpace(connection.Username)) > 0 {
		vcUsername = connection.Username
	} else {
		return nil, errors.New(fmt.Sprintf("Cannot log into VCenter.  Connection username is missing or blank."))
	}

	if len(strings.TrimSpace(connection.Password)) > 0 {
		vcPassword = connection.Password
	} else {
		return nil, errors.New(fmt.Sprintf("Cannot log into VCenter.  Connection password is missing or blank."))
	}

	// URL object contains the username password and provides sanity
	// checking of the URL syntax.
	urlObj, err := soap.ParseURL(vcURL)
	if err != nil {
		// Cannot proceed with an invalid VSphere/VCenter URL
		return nil, errors.New(fmt.Sprintf("vCenter URL Error: %v", err))
	}

	// Set the username & password to the URL object which will
	// be used for actual login below with the session manager
	urlObj.User = url.UserPassword(vcUsername, vcPassword)

	// Create a new context.  Context is loaded with login information
	// below after clients are created.
	//
	var ctxLogin = context.Background()

	// Main govmomi client
	//
	vcClient, err := govmomi.NewClient(ctxLogin, urlObj, true)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("vCenter Client Error: %s", err.Error()))
	}

	// Get vim25client directly from the govmomi client
	var vimClient = vcClient.Client

	// This logs into the SOAP API with side-effects stored to the context object.  If this is not successful,
	// you may get "Unauthenticated" error when using a Finder object attached to this context.
	manager := session.NewManager(vimClient)
	manager.Login(ctxLogin, urlObj.User)

	// Finder
	//
	finder := find.NewFinder(vimClient, true)

	// Create the loaded connection object which can be passed to various business logic functions
	// that require VCenter SOAP API access
	return &VcSoapConnection{
		Ctx:            ctxLogin,  // Context holds the auth information
		Url:            urlObj,    // URL contains username/password info
		VcClient:       vcClient,  // The main govmomi client used for various connection object creation
		VimClient:      vimClient, // VIM client used to create the Finder objects
		Finder:         finder,    // Finder for searches
		SessionManager: manager,   // the session manager object is used to log out
		VcAddressRec:   connection, // the actual addressbook entry used to create the SOAP connection (contains the URL & login that is used by certain business logic downstream)
	}, nil
}

func createBooleanMap(kinds []string) map[string]bool {
	kindMap := make(map[string]bool)
	for _, k := range kinds {
		kindMap[k] = true
	}
	return kindMap
}

// Retrieves the VCenter Virtual Machine record translated to a local data types
//
// Params:
// 1) vcSoapConnection - a valid authenticated connection to the VCenter
// 2) vmElement - a list of ManagedObjectReferences that specify the VCenter VM records to retrieve. TIP: You can use
//                the govmomi Finder object to find the MangedObjectReference for a VM
//
func RetrieveVmDetailRecord(vcSoapConnection *VcSoapConnection, vmElement *list.Element) (*Vcvm, error) {
	// Will store lookup for MAC adapter type
	var mapMacAdapterType = make(map[string]string)
	var mutexMacAdapterType = sync.Mutex{} // prevent simulteneous access to map otherwise Go will panic at runtime

	// color.Green("Processing VirtualMachine %s", e.Path)
	vm, errFinder := vcSoapConnection.Finder.VirtualMachine(vcSoapConnection.Ctx, vmElement.Path)
	if errFinder != nil {
		return nil, errors.New(fmt.Sprintf("VirtualMachine Finder Error: %s", errFinder))
	}

	var props = []string{"summary", "guest", "storage", "config", "datastore", "network"}
	var v mo.VirtualMachine
	errProps := vm.Properties(vcSoapConnection.Ctx, vmElement.Object.Reference(), props, &v)
	if errProps != nil {
		return nil, errors.New(fmt.Sprintf("VirtualMachine Props Error: %s", errProps))
	}

	s := v.Summary
	vmh, err := vcSoapConnection.Finder.Element(vcSoapConnection.Ctx, *s.Runtime.Host)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("VirtualMachine.Runtime.Host Finder  Error: %s", err))
	}

	cpuUsage := 100 * float64(s.QuickStats.OverallCpuUsage) / float64(s.Runtime.MaxCpuUsage)
	memUsage := 100 * float64(s.QuickStats.GuestMemoryUsage) / float64(s.Config.MemorySizeMB)
	_vm := Vcvm{
		Name:           s.Config.Name,
		ManagedObjRef:  s.Vm.Value,
		Path:           vmElement.Path,
		Host:           vmh.Path,
		OS:             s.Guest.GuestFullName,
		CPUs:           s.Config.NumCpu,
		TotalCPUMhz:    s.Runtime.MaxCpuUsage,
		HddAllocatedGB: calcTotalVmStorageAllocatedGB(v.Storage.PerDatastoreUsage),
		MemoryMB:       s.Config.MemorySizeMB,
		UpTimeSeconds:  s.QuickStats.UptimeSeconds,
		MemoryUsageMB:  memUsage,
		CPUUsageMhz:    cpuUsage,
		Networks:       []VcvmNetwork{}, // Child records will be populated in separate VCenter call below
		Datastores:     make([]*DatastoreStruct, 0), // Will be populated in separate VCenter call below
		State:          string(s.Runtime.PowerState),
		OverallStatus:  string(s.OverallStatus),
		CPUHotAdd:      getCpuHotAddEnabled(v),
		MEMHotAdd:      getMemHotAddEnabled(v),
	}

	// Look up MAC addresses and their adapter types
	//
	// NOTE: value are stored in lookup table for use during VM's network enumeration below
	if v.Config != nil && v.Config.Hardware.Device != nil {
		mutexMacAdapterType.Lock()
		for _, device := range v.Config.Hardware.Device {
			if device != nil {
				// Map only known VirtualEthernet records to the common adapter type names. Ignore all other type of devices (disk, cpu, etc.)
				switch device.(type) {
				case *types.VirtualVmxnet:
					mapMacAdapterType[device.(*types.VirtualVmxnet).MacAddress] = "VMXNET"
				case *types.VirtualVmxnet3:
					mapMacAdapterType[device.(*types.VirtualVmxnet3).MacAddress] = "VMXNET 3"
				case *types.VirtualE1000:
					mapMacAdapterType[device.(*types.VirtualE1000).MacAddress] = "E1000"
				case *types.VirtualE1000e:
					mapMacAdapterType[device.(*types.VirtualE1000e).MacAddress] = "E1000e"
				case *types.VirtualPCNet32:
					mapMacAdapterType[device.(*types.VirtualPCNet32).MacAddress] = "PCNET32"
				default:
					// Ignore all other non-network adapter related devices (not relevant)
				}
			}
		}
		mutexMacAdapterType.Unlock()

		// Store each network's VCenter full path for lookup later
		//
		var networkPathsLookup = VcPathNameLookup{}
		if v.Network != nil {
			for _, networkRef := range v.Network {
				if listElement, errNetwork := vcSoapConnection.Finder.Element(vcSoapConnection.Ctx, networkRef); errNetwork != nil {
					// Fatal
					return nil, errNetwork
				} else {
					// Add the network path to the VM's networks array
					//_vm.Networks = append(_vm.Networks, listElement.Path)
					networkPathsLookup.AddPath(listElement.Path)
				}
			}
		}

		// Add networks associated with the VCenter VM record
		if v.Guest.Net != nil {
			for _, networkRec := range v.Guest.Net {
				var vcvmNetwork = VcvmNetwork{
					NetworkPath: networkPathsLookup.Lookup(networkRec.Network),
					NetworkName: networkRec.Network,
					AdapterType: mapMacAdapterType[networkRec.MacAddress], // Needs lookup later from MacAddress
					MacAddress:  networkRec.MacAddress,                    // 1 MAC address
					IpAddresses: networkRec.IpAddress,                     // 0 or more IP addresses
				}

				_vm.Networks = append(_vm.Networks, vcvmNetwork)
			}
		}
	}

	if datastores, errDatastores := GetDatastoreInfo(vcSoapConnection, v.Datastore); errDatastores != nil {
		// Fatal
		return nil, errDatastores
	} else {
		_vm.Datastores = datastores
	}

	return &_vm, nil
}

// Retrieves the VCenter Datastore detailed record translated to a local data type
//
// Params:
// 1) vcSoapConnection - a valid authenticated connection to the VCenter
// 2) vmElement - a list of ManagedObjectReferences that specify the VCenter Datstore records to retrieve. TIP: You
//                can use the govmomi Finder object to find the MangedObjectReference for a VM
//
func RetrieveDatastoreDetailRecord(vcSoapConnection *VcSoapConnection, vmElement *list.Element) (*api.DatastoreResponse, error) {
	vm, errFinder := vcSoapConnection.Finder.Datastore(vcSoapConnection.Ctx, vmElement.Path)
	if errFinder != nil {
		return nil, errors.New(fmt.Sprintf("Datastore Finder Error: %s", errFinder))
	}

	var props = []string{"summary", "host", "vm"}
	var moDatastore mo.Datastore
	errProps := vm.Properties(vcSoapConnection.Ctx, vmElement.Object.Reference(), props, &moDatastore)
	if errProps != nil {
		return nil, errors.New(fmt.Sprintf("Datastore Props Error: %s", errProps))
	}

	var datastoreDetailStruct = api.DatastoreResponse {
		Name:                   moDatastore.Summary.Name,
		Url:                    moDatastore.Summary.Url,
		Path: 					vmElement.Path,
		ManagedObjRef:          moDatastore.Summary.Datastore.Value,
		CapacityBytes:          moDatastore.Summary.Capacity,
		CapacityRemainingBytes: moDatastore.Summary.FreeSpace,
		Type:                   moDatastore.Summary.Type,
		HypervisorsRefs:        packageHypervisorRefs(moDatastore),
		VmRefs:                 packageVmRefs(moDatastore),
	}

	return &datastoreDetailStruct, nil
}

// Packages up the datastore's child hypervisor references into a pre-defined array
//
func packageHypervisorRefs(moDatastore mo.Datastore) []*api.DatastoreHost {
	var datastoreHosts []*api.DatastoreHost

	for _, hypervisorDatastoreRef := range moDatastore.Host {
		datastoreHosts = append(datastoreHosts, &api.DatastoreHost{
			DatastoreRef:  moDatastore.Summary.Datastore.Value,
			HypervisorRef: hypervisorDatastoreRef.Key.Value,
			MountPoint:    hypervisorDatastoreRef.MountInfo.Path,
		})
	}

	return datastoreHosts
}

// Packages up the datastore's child VM references into a pre-defined array
//
func packageVmRefs(moDatastore mo.Datastore) []string {
	var vmRefs []string

	for _, vm := range moDatastore.Vm {
		vmRefs = append(vmRefs, vm.Value)
	}

	return vmRefs
}

// Retrieves the datastore info records (path & url) for the given list of managed object references
//
func GetDatastoreInfo(vcSoapConnection *VcSoapConnection, datastores []types.ManagedObjectReference) ([]*DatastoreStruct, error) {
	var results = make([]*DatastoreStruct, 0);

	if datastores != nil {
		for _, datastoreRef := range datastores {
			if listElement, errDatastore := vcSoapConnection.Finder.Element(vcSoapConnection.Ctx, datastoreRef); errDatastore != nil {
				return nil, errDatastore
			} else {
				// Look up and process VM's datastore records
				if datastoreFinder, errFinder := vcSoapConnection.Finder.Datastore(vcSoapConnection.Ctx, listElement.Path); errFinder != nil {
					// Fatal
					return nil, errFinder
				} else {
					var datastore = mo.Datastore{}
					var errDatastoreFind = datastoreFinder.Properties(
						vcSoapConnection.Ctx,
						datastoreRef,
						[]string{"summary"},  // Only need summary info to get the datastore URL
						&datastore)

					if errDatastoreFind != nil {
						return nil, errDatastoreFind
					} else {
						// Add datastore structure to the VM's existing datastore list
						results = append(results, &DatastoreStruct{listElement.Path,datastore.Summary.Url})
					}
				}
			}
		}
	}

	return results, nil
}

// Gets the "CPU Hot Add Enabled" flag from the VirtualMachine object and guards against nil values
func getCpuHotAddEnabled(v mo.VirtualMachine) *bool {
	if v.Config != nil && v.Config.CpuHotAddEnabled != nil {
		return v.Config.CpuHotAddEnabled
	} else {
		var b = false
		return &b
	}
}

// Gets the "Memory Hot Add Enabled" flag from the VirtualMachine object and guards against nil values
func getMemHotAddEnabled(v mo.VirtualMachine) *bool {
	if v.Config != nil && v.Config.MemoryHotAddEnabled != nil {
		return v.Config.MemoryHotAddEnabled
	} else {
		var b = false
		return &b
	}
}

// Calculates the total GB storage capacity of the given list of HostFileSystemMountInfo
// objects which can be obtained from the mo.HostSystem.Config.FileSystemVolume.MountInfo
//
func CalcTotalHypervisorStorageGB(mounts []types.HostFileSystemMountInfo) int32 {
	var totalBytes int64

	for _, mount := range mounts {
		totalBytes += mount.Volume.GetHostFileSystemVolume().Capacity
	}

	return int32(totalBytes / 1024 / 1024 / 1024)
}

// Calculates the total GB VM allocated (not necessarily used) from the given list of
// types.VirtualMachineUsageOnDatastore which can be obtained from the VM type at
// mo.VirtualMachine.Storage.PerDatastoreUsage
func calcTotalVmStorageAllocatedGB(usages []types.VirtualMachineUsageOnDatastore) int32 {
	var totalBytes int64

	for _, usage := range usages {
		totalBytes += usage.Committed + usage.Uncommitted
	}

	return int32(totalBytes / 1024 / 1024 / 1024)
}

// Get the Value field form an mo.Reference and guards against null pointer exception by returning empty string ""
// if the given mo.Reference is nil
func GetManagedObjValue(managedObj mo.Reference) (moValue string) {
	if managedObj != nil {
		return managedObj.Reference().Value
	} else {
		return ""
	}
}



//
// Struct for saving VCenter paths (full length) and looking them up by name later
//
type VcPathNameLookup struct {
	vcPaths []string
}

// Looks up a previously stored long VCenter path by short name
//
// Example: Stored VCenter path "/PAYTV-2/network/VM-468 Network" with lookup short name input of "VM-468 Network" will
// return the VCenter path "/PAYTV-2/network/VM-468 Network"
//
// PARAM: name  the short name only of the netwrok
// RETURN: the matching full VCenter path or "" if no match found
//
func (o *VcPathNameLookup) Lookup(name string) string {
	for _, path := range o.vcPaths {
		// Linear search & return on first match
		if strings.HasSuffix(strings.TrimSpace(path), name) {
			return strings.TrimSpace(path)
		}
	}

	// No match if this point is reached
	return ""
}

// Adds a new VCenter path for lookup later.  Typically invoked during reading of VCenter data
//
// PARAM:  path   full vcenter path to object
func (o *VcPathNameLookup) AddPath(fullPath string) {
	o.vcPaths = append(o.vcPaths, fullPath)
}



type VcSoapConnection struct {
	Ctx            context.Context
	Url            *url.URL
	VcClient       *govmomi.Client
	VimClient      *vim25.Client
	Finder         *find.Finder
	SessionManager *session.Manager
	VcAddressRec   *addressbook.VSphereConnectionStruct  // stores the VCenter addressbook info that was used to create this connection  (url, username, password)
}

type VCHost struct {
	Name          string
	Path          string
	ID            string // VCenter primary key.  E.x. "host-1127"
	Vendor        string
	Model         string
	CPUModel      string
	CPUs          int32
	CPUMhz        int32
	TotalMemoryMB int64
	StorageCapGB  int32
	NetSystemName string // E.x. "csc06a"
	NetPortId     string // E.x. "Ethernet1/24"

	// host log
	VMotionEnabled bool
	VMs            int
	VMList         []types.ManagedObjectReference // This field is no longer applicable in new VIMCMD design
	VimVmList      []VimVmReference
	BootTime       *time.Time
	MemoryUsage    float64
	CPUUsageMhz    float64
	State          string
	OverallStatus  string

	// Full VM details (optional)
	GuestVMDetails []Vcvm

	// Error in retrieving this record.  Would be nil if no error
	Error error
}

type Vcvm struct {
	Name           string
	ManagedObjRef  string
	Path           string
	Host           string
	HostFullVcPath string
	OS             string
	CPUs           int32
	TotalCPUMhz    int32
	HddAllocatedGB int32
	MemoryMB       int32
	Networks       []VcvmNetwork   // 0  networks -- contains IP address(es) and network information
	Datastores     []*DatastoreStruct   // 0 or more datastore records
	CPUHotAdd      *bool
	MEMHotAdd      *bool

	// vm log
	UpTimeSeconds int32
	MemoryUsageMB float64 // in percentage
	CPUUsageMhz   float64 // in percentage
	State         string
	OverallStatus string
}

type VcvmNetwork struct {
	NetworkPath    string     // ex: /TMOBILE/network/VM-96 Network
	NetworkName    string     // ex: VM-428 Network, VM-320 Network
	AdapterType    string     // ex: VMXNET 3, E1000, PCNET32
	MacAddress     string
	IpAddresses    []string   // A network can 0 or more IP addresses but usually just 1 IP address
}

type HypervisorDatastoreRef struct {
	HypervisorRef    string
	MountPoint       string
}

type DatastoreDetailStruct struct {
	Name                   string
	Url                    string
	CapacityBytes          int64
	CapacityRemainingBytes int64
	HypervisorRefs         []HypervisorDatastoreRef
	VmRefs                 []string
}



// Stores a pair of values that uniquely identify the datastore (specifically the URL)
type DatastoreStruct struct {
	Path     string        // VCenter path to datastore (actually is *not* unique in this case because more than one path can reference the same datastore objectg)
	Url      string        // True primary key for the datastore:  ds:///vmfs/volumes/593f9715-68082d86-db28-ac162d84d074/
}



// Stores values related to a VM's network
type NetworkStruct struct {
	Path        string        // VCenter path to network (ex. /PAYTV-2/network/VM-468 Network)
	AdapterType string        // Ex. VMXNET3, E1000
	Mac         string        // MAC address (ex. 00:50:56:8e:fc:57)
}

type VCElement struct {
	Name string
	Path string
}

type VCObjects struct {
	errMsg string // Consumers of this object must check for the presence of this error
	VMs    []Vcvm
	Hosts  []VCHost
	Datastores []DatastoreDetailStruct
	Others []VCElement
}

// A struct to contain the output from the "vmsvc/getallvms" command.  This structure is
// specific for "vim-cmd"
//
type VimVmReference struct {
	Vmid       int
	Fqdn       string
	File       string
	GuestOS    string
	Version    string
	Annotation string

	// An supplemental field to allow searching for a VM from the VCenter path.  This will
	// be populated with VCenter search call.
	VcPath string
}
