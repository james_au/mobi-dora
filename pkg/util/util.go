package util

import (
	"context"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/profiling"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/opentracing/opentracing-go"
	"net"
	"os"
	"regexp"
	"strings"
	"time"
)

// Current time in milliseconds epoch time
//
func CurrentTimeMillis() int64 {
	return int64(time.Now().UnixNano() / 1000000)
}

// Returns the lesser of two integers
func Min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

// Returns the greater of two integers
func Max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

// All time output must go through this function for consistent time format output across the program
func FormatTime(t time.Time) string {
	return t.Format(constants.DEFAULT_TIME_FORMAT)
}

// Gets the parent Opentracing Span object from context if available otherwise returns a "disconnected" span
//
func GetParentSpan(ctx context.Context) opentracing.Span {
	// Get context parent tracing span if available
	if ctx != nil && ctx.Value(profiling.PARENT_SPAN_NAME) != nil {
		// Return the enclosed parent tracing span object
		return ctx.Value(profiling.PARENT_SPAN_NAME).(opentracing.Span)
	} else {
		// Context does not contain a parent span so return a placeholder span
		// that will allow the dependent code to still process normally.  This
		// can occur during test runs that do not have a context or a declare a
		// tracing span.
		return opentracing.StartSpan("placeholder")
	}
}

// Returns true if the given string is found in the given string array and
// false otherwise
func StringArrayContains(s string, array ...string) bool {
	// Linear search.  Return on first match.
	for _, element := range array {
		if element == s {
			return true
		}
	}

	// If this point is reached then no match was found
	return false
}

// Returns true if every string in array s1 can be found in s2 and false otherwise
//
func IsStringArraySubset(s1 []string, s2... string) bool {
	if s1 != nil && s2 != nil {
		for _, ele := range s1 {
			if !StringArrayContains(ele, s2...) {
				//  Not subset.  There is at least one element in s1 not found in s2.
				return false
			}
		}

		// Is subset.  The string array s2 contains every element of s1.
		return true
	}

	return false
}

// Subtracts string array s2 from string array s1 producing the difference result.  The resultant array will be
// empty if s1 is a subset of s2.  NOTE: Resultant array will never be nil under any circumstances.
//
// Example:
//   s1 = ["b", "c", "f"]
//   s2 = ["a", "b", "c", "d"]
//   then: result = ["f"]
//
// Example:
//   s1 = ["b", "c"]
//   s2 = ["a", "b", "c", "d"]
//   then: result = []
//
// Example:
//   s1 = ["b", "c"]
//   s2 = []
//   then: result = ["b", "c"]
func StringArraySubtraction(s1 []string, s2... string) (result []string) {
	// Prevent null pointer by converting nil list to empty list
	if s2 == nil {
		s2 = make([]string, 0)
	}

	if s1 != nil {
		// O(n^2) search
		for _, element := range s1 {
			if !StringArrayContains(element, s2...) {
				result = append(result, element)
			}
		}
	}

	return
}

// Extracts the hostname from an FQDN.  Ex: an input of "msfrn01p1.paytv.smf1.mobitv" would return "msfrn01p1".
// NOTE that if a non-FQDN is given then it will be returned as-is.  Ex: input "msfrn01p1" would return "msfrn01p1"
// as is.
var patternHostNameOnly = regexp.MustCompile(`([^\.]*)`) // NOTE: regex pattern objects are thread-safe
func ExtractHostname(fqdn string) string {
	var matches = patternHostNameOnly.FindAllStringSubmatch(fqdn, 1)
	if len(matches) > 0 {
		return strings.TrimSpace(matches[0][1])
	} else {
		// NO match so just return original string to caller
		return fqdn
	}
}

// Extracts the FQDN from a given VCenter path to a hypervisor or VM
//
// EXAMPLE: "/PAYTV-1/host/PAYTV-1/f-02-29.infra.smf1.mobitv" => "f-02-29.infra.smf1.mobitv"
// EXAMPLE: "f-02-29.infra.smf1.mobitv" => "f-02-29.infra.smf1.mobitv"
var patternFqdnOnly = regexp.MustCompile(`.+/([^/]+)`) // NOTE: regex pattern objects are thread-safe
func ExtractFqdnFromVCPath(vcPath string) string {
	if strings.Contains(vcPath, "/") {
		var matches = patternFqdnOnly.FindAllStringSubmatch(vcPath, 1)
		if len(matches) > 0 {
			return strings.TrimSpace(matches[0][1])
		} else {
			// NO match so just return original string to caller
			return vcPath
		}
	} else {
		// The path is already just an FQDN.  This allows a shortcut for test code that does not have a full VC path but
		// only an FQDN.
		return vcPath
	}
}

// Returns true if the given string is blank and false otherwise
func IsBlank(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}

// Inverse of IsBlank()
func IsNotBlank(s string) bool {
	return !IsBlank(s)
}

// Intelligently joins a list of paths into a single string with elements jointed by a forward slash (/)
//
var reTrimLeft = regexp.MustCompile(`^/+`)
var reTrimRight = regexp.MustCompile(`/+$`)

func JoinUrlPaths(paths ...string) string {
	var trimmedPaths []string

	// Trim all path elements of forward slash which "cleans" them to guarantee proper joining
	for _, path := range paths {
		path = reTrimLeft.ReplaceAllString(path, "")
		path = reTrimRight.ReplaceAllString(path, "")

		trimmedPaths = append(trimmedPaths, path)
	}

	// Return cleaned re-joined string
	return strings.Join(trimmedPaths, "/")
}

// Describes the datasource for downstream components like UIs.  HTML Javascript can decode this their own datastructure
// for display accordingly.
//
type DatasourceMetaStruct struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	LastUpdated int64  `json:"lastUpdated"`
}

// Returns the intersection of the two string lists which is useful for finding
// if a user's groups matchings a predefined required group for computing
// authorization access
//
func Intersection(section1, section2 []string) (intersection []string) {
	// Prevent null pointer exceptions if user passes nil array references by converting nil arrays to empty arrays
	if section1 == nil {
		section1 = []string{}
	}
	if section2 == nil {
		section2 = []string{}
	}

	str1 := strings.Join(filter(section1), " ")
	for _, s := range filter(section2) {
		if strings.Contains(str1, s) {
			intersection = append(intersection, s)
		}
	}
	return
}

func filter(src []string) (res []string) {
	for _, s := range src {
		newStr := strings.Join(res, " ")
		if !strings.Contains(newStr, s) {
			res = append(res, s)
		}
	}
	return
}

// Extracts that hypervisor's Cluster and Hostname from the given path.
// Example Hypervisor path with both cluster and hostname:
//     /PAYTV-SEGMENTERS/host/PAYTV-SEGMENTERS/c-04-16.infra.smf1.mobitv will return cluster = "PAYTV-SEGMENTERS" and
//     hostname = "c-04-16.infra.smf1.mobitv"
//
// Example Hypervisor path with both no cluster and hostname.  Cluster is "" because of duplicate host
//     /PAYTV-SEGMENTERS/host/c-04-16.infra.smf1.mobitv/c-04-16.infra.smf1.mobitv will return cluster = "" and
//     hostname = "c-04-16.infra.smf1.mobitv"
func hypervisorPathParse(hypervisorPath string) (cluster string, hostname string) {
	var pathElements = strings.Split(strings.TrimSpace(hypervisorPath), "/")

	// The hypervisor's hostname is always the last position element provided the given path is not empty
	if len(pathElements) > 0 {
		hostname = pathElements[len(pathElements)-1]
	} else {
		// Unlikely if given proper VCenter path but need to guard against array out of bounds error
		hostname = ""
	}

	// Traverse the path elements looking for positions containing the expected cluster position
	for i := 0; i < len(pathElements); i++ {
		var currentElement = pathElements[i]
		if currentElement == "host" {
			// Hypervisor's cluster is the path element following host but only if that value is not equal to the hostname
			if i < len(pathElements) && currentElement != hostname {
				cluster = pathElements[i + 1]
			} else {
				// Hypervisor is not assigned to a cluster since it has a duplicate path element
				// Eg
				cluster = ""
			}

			break
		}
	}

	return
}

// Gets the host this Go process is running on
//
// RETURN: hostname of running host or "Error: ..." if hostname could not be obtained with message included
func GetHostname() string {
	var hostname = "n/a"
	var hostNameErr error
	hostname, hostNameErr = os.Hostname()
	if hostNameErr != nil {
		hostname = "Error: " + hostNameErr.Error()
	}

	return hostname
}

// Creates a unique session ID string that can be used to uniquely identify a Balance session.  This is part of the
// system to prevent concurrent VM moves within the same VCener instance across multiple user or browsers
//
// RETURN: unique balance session string
func CreateBalanceSessionId(vcenterName string) string {
	return fmt.Sprintf("%s:::%s:::%d", vcenterName, uuid.New().String(), CurrentTimeMillis())
}

// Lookups the IP address of the given host
//
func LookupIpAddress(hostname string) (net.IP, error) {
	ip, err := net.LookupIP(hostname)

	if err != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("IP lookup for host \"%s\" failed because of error: %v", hostname, err))
	} else {
		// Successful lookup.  Always return 1st value.
		if len(ip) > 0 {
			if len(ip) > 1 {
				// Log a warning only if more than 1 IP is found for the given hostname
				logger.Log.Info(fmt.Sprintf("Returning one IP but hostname \"%s\" has %d IP addresses: %v", hostname, len(ip), ip))
			}

			// Always return only the first IP address
			return ip[0], nil
		}
	}

	return nil, nil
}

// Attempts to extract the username from the given context
//
func ExtractUser(ctx context.Context) string {
	if ctx != nil {
		if ctx.Value(constants.CTX_KEY_AUTH_USERNAME) != nil {
			return ctx.Value(constants.CTX_KEY_AUTH_USERNAME).(string)
		}
	}

	// If this point is reached, then no username could be identified from the context
	return "<not specified>"
}

