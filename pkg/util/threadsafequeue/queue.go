package threadsafequeue

import (
	"reflect"
	"sync"
)

// Implements a general-purpose thread safe queue that can act as either a LIFO mode (stack) or FIFO mode (queue).

// Base data structure
type Queue struct {
	items []interface{}   // The items of the queue in order with the head of the queue at index 0
	mutex sync.RWMutex    // The mutex lock to synchronize access which provides the "thread safety" feature
}

// Package method creates a new object initialized with an optional list of items.  If omitted, then the queue
// will start out as an empty queue.  All instances of this object acquired with this method.
func New(items interface{}) *Queue {
	var queue = Queue{}

	// Items are implemented internally by this list
	queue.items = make([]interface{}, 0)

	if items != nil {
		switch reflect.TypeOf(items).Kind() {
		case reflect.Slice:
			var baseArray = reflect.ValueOf(items)

			for i := 0; i < baseArray.Len(); i++ {
				queue.items = append(queue.items, baseArray.Index(i).Interface())
			}
		}
	}

	return &queue
}

// Add an item into the queue normally
//
// NOTE: this is the only way to add items to the queue after constructor initialization
// NOTE: this method is used *regardless* of FIFO or LIFO operation.  Whether the queue behaves in FIFO (queue) or
//       LIFO (stack) actually depends methods Pop() or Pull() determined by client code usage.
// NOTE: Access is serialized by mutex
func (queue *Queue) Add(t interface{}) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	queue.items = append(queue.items, t)
}

// Removes one item from the tail of the queue making it behave like a LIFO stack.
//

// NOTE: Access is serialized by mutex
// SEE: Pull()  the counterpart function for FIFO operation
func (queue *Queue) Pop() interface{} {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	if len(queue.items) > 0 {
		item := queue.items[len(queue.items)-1]
		queue.items = queue.items[0 : len(queue.items)-1]
		return item
	} else {
		// EmptyMarker
		return EmptyMarker{}
	}
}

// Removes one item from the head of the queue making it behave like a regular FIFO queue.
//
// NOTE: if the stack is empty, then a special EmptyMarker marker type is returned.  Callers should process this special
//       type accordingly.
// NOTE: Access is serialized by mutex
// SEE: Pop()  the counterpart function for LIFO operation
func (queue *Queue) Pull() interface{} {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	if len(queue.items) > 0 {
		item := queue.items[0]
		queue.items = queue.items[1:len(queue.items)]
		return item
	} else {
		// EmptyMarker
		return EmptyMarker{}
	}
}

// Returns the current count of the queue's items.  This number can increase or decrease during the
// lifetime of the queue.
func (queue *Queue) Size() int {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	return len(queue.items)
}

// Returns true if the queue is currently empty and false otherwise
func (queue *Queue) IsEmpty() bool {
	return queue.Size() == 0
}

// Used to indicate when a Push() or Pull() is done on an empty stack.  This special marker is needed because
// the queue could actually contain Nil items depending on the user's requirements.
type EmptyMarker struct{}
