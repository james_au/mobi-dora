package threadsafequeue

import (
	"sync"
)

// A "Buffered" queue is the same as a regular thread safe queue but adds the constraint of a "buffer size limit"
type BufferedThreadSafeQueue struct {
	Queue
	bufferSize        int
	mutexBuffered     sync.RWMutex   // Need to use own mutex separate from parent struct's mutex otherwise can deadlock
}

func NewBufferedQueue(size int) *BufferedThreadSafeQueue {
	return &BufferedThreadSafeQueue{Queue{}, size, sync.RWMutex{}}
}

// Add an item into the queue normally with buffer limits in effect.  Items at the head of the queue might be dropped if
// the buffer is full.
//
// NOTE: This overrides parent struct's function
// NOTE: If queue buffer limit is reached then the item at the head of the queue is dropped before adding
// NOTE: Access is serialized by mutex
func (o *BufferedThreadSafeQueue) Add(t interface{}) {
	defer o.mutexBuffered.Unlock()
	o.mutexBuffered.Lock()

	// Remove the first item if queue buffer limit has been reached which prevents the queue from exceeding the max
	// queue size
	if o.Size() == o.bufferSize {
		o.Queue.Pull()
	}

	o.items = append(o.items, t)
}
