package threadsafequeue

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

// Basic test for FIFO based operation with buffer limit
//
func TestBuffered01(t *testing.T) {
	assert.NotPanics(t, func() {
		var bufferedThreadSafeQueue = NewBufferedQueue(3)

		// Add items normally.  Note adding does not determine FIFO or LIFO operation yet -- that depends on
		// later use of Pop() or Pull()
		bufferedThreadSafeQueue.Add("a")
		bufferedThreadSafeQueue.Add("b")
		bufferedThreadSafeQueue.Add("c")
		bufferedThreadSafeQueue.Add("d")
		bufferedThreadSafeQueue.Add("e")

		// Using Pull() for LIFO operation and since buffer size is only 3 when 5 values are added then only the last
		// three added entries will exist in the buffered queue
		assert.Equal(t, 3, bufferedThreadSafeQueue.Size())
		assert.Equal(t, reflect.Slice, reflect.TypeOf(bufferedThreadSafeQueue.items).Kind())
		assert.Equal(t, bufferedThreadSafeQueue.Pull(), "c")
		assert.Equal(t, bufferedThreadSafeQueue.Pull(), "d")
		assert.Equal(t, bufferedThreadSafeQueue.Pull(), "e")
		assert.True(t, bufferedThreadSafeQueue.IsEmpty())
	})
}
