package threadsafequeue

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"sync"
	"testing"
)

//
// UNIT TEST: No test dependencies required for this test
//

// Basic test for LIFO "stack" based operation
//
func Test01(t *testing.T) {
	assert.NotPanics(t, func() {
		var queue = New(nil)

		// Add items normally.  Note adding does not determine FIFO or LIFO operation yet -- that depends on
		// later use of Pop() or Pull()
		queue.Add("a")
		queue.Add("b")
		queue.Add("c")

		// Pop value must be in reverse order to order they were originally added for proper LIFO stack operation
		assert.Equal(t, 3, queue.Size())
		assert.Equal(t, reflect.Slice, reflect.TypeOf(queue.items).Kind())
		assert.Equal(t, queue.Pop(), "c")
		assert.Equal(t, queue.Pop(), "b")
		assert.Equal(t, queue.Pop(), "a")
		assert.True(t, queue.IsEmpty())
	})
}

// Concurrency testing
//
func Test02(t *testing.T) {
	assert.NotPanics(t, func() {
		var testSize = 100000
		var lockQueue = make(chan int, testSize)
		var queue = New("")

		// Start a large amount of worker threads to populate the queue concurrently
		for i := 1; i <= testSize; i++ {
			go func(index int) {
				queue.Add(index)
				lockQueue <- index
			}(i)
		}

		// Main thread needs to wait until all workers are done
		var finishedCount = 0
		for {
			<-lockQueue
			finishedCount++

			if finishedCount == testSize {
				break
			}
		}

		// Assert final size of the queue is as expected
		assert.Equal(t, testSize, queue.Size())
		assert.Equal(t, reflect.Slice, reflect.TypeOf(queue.items).Kind())

		// Start a large amount of worker threads to de-popopulate the queue concurrently
		var sumManualCount int
		var mutex sync.Mutex
		for i := 0; i < testSize; i++ {
			go func(index int) {
				// Mutex lock is required here even for a simple int summation otherwise the count is
				// non-deterministic (randomly off by a bit)
				mutex.Lock()
				sumManualCount += queue.Pop().(int)
				mutex.Unlock()

				lockQueue <- index
			}(i)
		}

		// Main thread needs to wait until all workers are done
		finishedCount = 0
		for {
			<-lockQueue
			finishedCount++

			if finishedCount == testSize {
				break
			}
		}

		// Queue should be back to empty and the checksum is a summation of all values 1 thru "testSize"
		assert.Equal(t, 0, queue.Size())
		assert.Equal(t, testSize * (testSize + 1) / 2, sumManualCount)
	})
}

// Basic test for FIFO "queue" based operation
func Test03_fifo(t *testing.T) {
	assert.NotPanics(t, func() {
		var queue = New(nil)

		// Add items normally.  Note adding does not determine FIFO or LIFO operation yet -- that depends on
		// later use of Pop() or Pull()
		queue.Add("a")
		queue.Add("b")
		queue.Add("c")

		// Pulled value must be in the same order they were originally added for proper FIFO queue operation
		assert.Equal(t, 3, queue.Size())
		assert.Equal(t, reflect.Slice, reflect.TypeOf(queue.items).Kind())
		assert.Equal(t, queue.Pull(), "a")
		assert.Equal(t, queue.Pull(), "b")
		assert.Equal(t, queue.Pull(), "c")
		assert.True(t, queue.IsEmpty())
	})
}
