package file

import (
	"dora/pkg/logger"
	"dora/pkg/util"
	"errors"
	"fmt"
	"github.com/spf13/afero"
	"os"
	"path/filepath"
	"strings"
)

var aferoFs afero.Fs

// Read opens the file at the given path and returns the content in bytes
func ReadFile(path string) ([]byte, error) {
	if aferoFs == nil {
		// Lazy instantiation on first use
		aferoFs = afero.NewOsFs()
	}

	if strings.TrimSpace(path) == "" {
		// Fatal
		return nil, errors.New(fmt.Sprintf("File path was empty"))
	}

	b, err := afero.ReadFile(aferoFs, path)
	if err != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Error reading file '%s' because of error: %v", path, err))
	}

	return b, nil
}

// Returns true if file exists at given path and false otherwise
func FileExists(path string) bool {
	info, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

// Returns true if the specified directory exists and can be written to.  Otherwise returns a descriptive error object.
//
func IsWriteableDir(dirPath string) (bool, error) {
	// IMPLEMENTATION NOTE: Safest most accurate way is to just try to create a blank temp file at the given path
	// and check for errors.
	//
	var tempFilename = fmt.Sprintf("testfile_%d.tmp", util.CurrentTimeMillis())
	if fileTemp, errCreate := os.Create(filepath.Join(dirPath, tempFilename)); errCreate != nil {
		return false, errCreate
	} else {
		// Delete the temp file and return success conditions
		if errRemoveFile := os.Remove(fileTemp.Name()); errRemoveFile != nil {
			// Not fatal but log it as it represents some kind of system instability
			logger.Log.Info(fmt.Sprintf("WARN: Could not remove temp file %s because of error: %v", fileTemp.Name(), errRemoveFile))
		}

		return true, nil
	}
}
