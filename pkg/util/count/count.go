package count

//
// Utility to keep track of the number of times a number is associated with strings.  Useful for tracking
// if multiple values/variables have the same numeric value.
//

// Data structure record
type CountStruct struct {
	mapCounts map[int][]string
}

// Adds a count pair to be tracked
//
// PARAM: count   the numeric value
// PARAM: label   the string label/variable name
//
func (o *CountStruct) AddCount(count int, label string) {
	if o.mapCounts == nil {
		// Lazy instantiation
		o.mapCounts = make(map[int][]string)
	}

	if _, containsKey := o.mapCounts[count]; !containsKey {
		// Initialize - Key doesn't yet exist
		o.mapCounts[count] = []string{label}
	} else {
		// Key already exists -- append the label as another label having the same numeric value
		o.mapCounts[count] = append(o.mapCounts[count], label)
	}
}

// Returns the raw map for inspecting current results
//
func (o *CountStruct) GetMap() map[int][]string {
	return o.mapCounts
}

