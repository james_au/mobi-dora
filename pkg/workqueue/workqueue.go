package workqueue

import "github.com/google/uuid"

//
// Declares structures and utility methods related to the Redis work queue
//

// All queue response data are wrapped in this data structure to providing meta functions like:
// (1) end-of-stream control
// (2) error handling control
//
type QueueResponseWrapper struct {
	StatusCode int    `json:"status_code"`
	Data       string `json:"data"`
}

// Defines all stream wrapper status codes
const (
	STREAM_STATUS_OK       = 0  // Data is good and can be used or deserialized as expected by the client
	STREAM_STATUS_ERROR    = -1 // Error occurred.  The data value will contain the erromessage
	STREAM_STATUS_COMPLETE = 2  // Stream is complete and client can stop.  Data field may or may not contain any useful information.
)

// Completion record (statusCode 2) can be re-used because it doesn't have any specific or unique data
// associated with it
var Complete = QueueResponseWrapper{StatusCode: STREAM_STATUS_COMPLETE}

// Represents a VCenter search command and contains all data for the worker thread to complete the task.  This
// record is used for both Hypervisor and VirtualMachine VCenter searches.
//
type VCenterSearchCmd struct {
	AuthToken      string `json:"auth_token"`      // A valid JWT auth token
	ConnectionName string `json:"connection_name"` // Ex: NewPayTV, OldPayTV, CSpire, EPB
	PathRegExp     string `json:"path_search_regex"`  // VCenter path filter
	HostRegExp     string `json:"host_search_regex"`  // VCenter VM hostname filter
	ListOnly       bool   `json:"list_only"`       // Set to "true" to just the list preview
}

// Creates a consistently named response queue name based on a given 'base name'
//
func CreateResponseQueueName(baseName string) string {
	return "DORA::RESPONSEQUEUE::" + baseName + "-" + uuid.New().String()
}
