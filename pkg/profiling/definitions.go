package profiling

// Parent span name for context value extraction
const PARENT_SPAN_NAME = "parent_span"

// Operations
const (
	OP_GET_HYPERVISORS_STREAMED      = "GetHypervisorsStreamed"
	OP_GET_VIRTUALMACHINES_STREAMED  = "GetVirtualMachinesStreamed"
	OP_GET_HYPERVISORS_ARRAY         = "GetHypervisorsArray"
	OP_GETHYPERVISOR_SEARCH_ELEMENTS = "SearchElement"
	OP_GETHYPERVISOR_DETAILS         = "GetHypervisorDetails"
	OP_GETVIRTUALMACHINE_DETAILS     = "GetVirtualMachineDetails"
	OP_GETDATASTORE_DETAILS          = "GetDatastoreDetails"
	OP_GETSTORAGECOMBINED_REPORT     = "GetStorageCombinedReport"
	OP_GETSHAREDSTORAGE_REPORT       = "GetSharedStorageReport"
	OP_GETDATASTORE_REPORT           = "GetDatastoreReport"
	OP_GETNFS_REPORT                 = "GetNfsReport"
	OP_GETVMSTORAGE_REPORT           = "GetVMStorageReport"
	OP_GETIPRANGE_REPORT             = "GetIPRangeReport"
	OP_GETSTORAGESUMMARY_REPORT      = "GetStorageSummaryReport"
	OP_UPLOADVMSTORAGE_REPORT        = "UploadVMStorageReport"
	OP_UPLOADIPRANGE_REPORT          = "UploadIpRangeReport"
	OP_UPLOADSTORAGESUMMARY_REPORT   = "UploadStorageSummaryReport"
	OP_VCENTER_LOGIN                 = "VCenterLogin"
	OP_DORA_GETLOGIN                 = "DoraGetLogin"
	OP_LDAP_LOGIN                    = "LDAPLogin"
	OP_JWT_GENERATE_TOKEN            = "JWTGenerateToken"
	OP_REDIS_SET                     = "RedisSet"
	OP_REDIS_GET                     = "RedisGet"
)

// Tags
const (
	TAG_REDIS_CACHE_HIT  = "REDIS_CACHE_HIT"
	TAG_REDIS_CACHE_MISS = "REDIS_CACHE_MISS"
)
