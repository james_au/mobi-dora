package config

import (
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/util"
	"dora/pkg/util/file"
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"
)

// Application configuration.  This package is implemented as an "object" with attached receiver methods defined
// below.
type ConfigStruct struct {
	// Public serialized fields types
	//
	// NOTE: these fields are for serialization access only.  Code should *not* reference this field.
	// Use Getter method instead.
	//
	ServerPort            int32  `json:"server_port"`
	JwtKey                string `json:"jwt_key"`
	LdapUrl               string `json:"ldap_url"`
	LdapDn                string `json:"ldap_dn"`
	CronAutoStart         bool   `json:"cron_auto_start"`
	AuthDisabled          bool   `json:"auth_disabled"`
	AuthIdleExpireMinutes int32  `json:"auth_idle_expire_minutes"`
	RedisUrl              string `json:"redis_url"`
	RedisDisabled         bool   `json:"redis_disabled"`
	RedisTTLSecs          int32  `json:"redis_ttl_secs"`
	VCenterWorkThreads    int32  `json:"vcenter_work_threads"`
	RedisWorkThreads      int32  `json:"redis_work_threads"`
	AddressBookPath       string `json:"address_book_path"`
	AppDashHttpPort       int32  `json:"appdash_http_port"`
	AppDashCollectorPort  int32  `json:"appdash_collector_port"`
	SQLiteDataFolder      string `json:"sqlite_data_folder"`
	BalanceCategory       string `json:"balance_category"`
	VCenterDirectAllow          string  `json:"vcenter_direct_allow"`
	NfsConfigRetrieveMaxThreads int32   `json:"nfs_config_retrieval_max_threads"`
	NfsConfigRetrieveUsername   string  `json:"nfs_config_retrieval_username"`
	NfsConfigRetrievePassword   string  `json:"nfs_config_retrieval_password"`
	EnvironmentLabel            string  `json:"environment_label"`
	SmtpServer                  string  `json:"smtp_server"`
	SyncWarnFromAddress         string  `json:"sync_warn_from_address"`
	SyncWarnHistoryRange        int32   `json:"sync_warn_history_range"`
	SyncWarnThreshold           float64 `json:"sync_warn_threshold"`
	SyncWarnRecipients          string  `json:"sync_warn_email_recipients"`

	// Internal non-serialized fields used only within the codebase and not accessible by user
	//
	Path string // The path the configuration file was read from

	// Set to true on first load
	configLoaded bool

	// The load time
	loadTime time.Time

	// Mutex for serialized config access when appropriate
	mutex sync.Mutex
}

// Global value.  NOTE: Configuration will be read-in from properties file at program startup
// and must not be used until then.  The client program is responsible to initialize the configs.
var Config = &ConfigStruct{configLoaded: false}

// Set the path to the config file to read.  If not set, hard-coded default path is used.
func SetPath(path string) error {

	if file.FileExists(path) {
		// Test that path is accessible and fail otherwise
		Config.Path = path

		return nil
	} else {
		return errors.New(fmt.Sprintf("Given config file at path does not exist: %s", path))
	}
}

// Reads in the runtime configuration path.
//
// NOTE: This must be called at least once otherwise custom configuration properties will not be read and default values
// will always be returned instead.
//
// Input: forceReload   set to true to force a reload even if already loaded already
func (cs *ConfigStruct) ReadConfig(forceReload bool) (*ConfigStruct, error) {
	cs.mutex.Lock()
	defer cs.mutex.Unlock()

	if !cs.configLoaded || forceReload {
		var configPath string

		// Use given path if provided otherwise use a default path
		if len(cs.Path) > 0 {
			logger.Log.Info("Reading configuration from specified path.", "path", Config.Path)
			configPath = Config.Path
		} else {
			logger.Log.Info("Reading configuration from default path.", "path", constants.DEFAULT_CONFIG_FILE_PATH)
			configPath = constants.DEFAULT_CONFIG_FILE_PATH
		}

		if file.FileExists(configPath) {
			if data, errRead := file.ReadFile(configPath); errRead != nil {
				// Fatal
				return nil, errors.New(fmt.Sprintf("File I/O error occurred during configuration read: %v", errRead))
			} else {
				// IO Read successful so parse the JSON formatted configuration data
				//
				// Unmarshall file bytes into the config structure
				json.Unmarshal(data, Config)

				// Update the config with the path the configuration data was read from
				Config.Path = configPath
				Config.configLoaded = true
				Config.loadTime = time.Now()

				return Config, nil
			}
		} else {
			// Can operate *without* a configuration because hard coded defaults are available.  Therefore do not error
			// out if the specified configuration file is not found on disk.
			logger.Log.Info(fmt.Sprintf("WARN: File %s does not exist.  Using default hard coded configurations instead.", configPath))

			// Just return a blank config struct which will return all hard coded default values.
			return &ConfigStruct{configLoaded: true}, nil
		}
	} else {
		return Config, nil
	}
}

// Returns the config server port number.  This will be either the override value or
// the default value
func (cs *ConfigStruct) GetServerPort() int32 {
	if cs.ServerPort != 0 {
		logger.Log.Info("Using override server port", "port", cs.ServerPort)
		return cs.ServerPort
	} else {
		logger.Log.Info("Using default server port", "port", constants.DEFAULT_GRPC_SERVER_PORT)
		return constants.DEFAULT_GRPC_SERVER_PORT
	}
}

// Returns the config JWT key phrase.  This will be either the override value or
// the default value.
func (cs *ConfigStruct) GetJwKeyPhrase() string {
	if len(cs.JwtKey) > 0 {
		logger.Log.Info("Using override JWT phrase")
		return cs.JwtKey
	} else {
		logger.Log.Info("Using default JWT phrase")
		return constants.DEFAULT_JWT_KEY
	}
}

// Returns the LDAP URL either default or overriden value
func (cs *ConfigStruct) GetLdapUrl() string {
	if len(cs.LdapUrl) > 0 {
		logger.Log.Info("Using override LDAP URL")
		return cs.LdapUrl
	} else {
		logger.Log.Info("Using default LDAP URL")
		return constants.DEFAULT_LDAP_URL
	}
}

// Returns the LDAP DN either default or overriden value
func (cs *ConfigStruct) GetLdapDn() string {
	if len(cs.LdapDn) > 0 {
		logger.Log.Info("Using override LDAP DN")
		return cs.LdapDn
	} else {
		logger.Log.Info("Using default LDAP DN")
		return constants.DEFAULT_LDAP_DN
	}
}

// Returns boolean indicating if Dora cron auto-start is enabled/disabled
func (cs *ConfigStruct) GetCronAutoStart() bool {
	// NOTE: GoLang defaults booleans to "false"
	return cs.CronAutoStart
}

// Returns the auth idle expiry time in minutes.  Either default or overriden value.
func (cs *ConfigStruct) GetAuthIdleExpirationMinutes() int32 {
	if cs.AuthIdleExpireMinutes != 0 {
		logger.Log.Info("Using override auth idle expiration minutes")
		return cs.AuthIdleExpireMinutes
	} else {
		logger.Log.Info("Using default auth idle expiration minutes")
		return constants.DEFAULT_AUTH_IDLE_MINUTES
	}
}

// Returns boolean indicating if Dora auth is enabled/disabled
func (cs *ConfigStruct) GetAuthDisabled() bool {
	return cs.AuthDisabled
}

// Returns Redis URL.  Either default or overriden value.
func (cs *ConfigStruct) GetRedisUrl() string {
	if len(cs.RedisUrl) > 0 {
		logger.Log.Info("Using override Redis URL")
		return cs.RedisUrl
	} else {
		logger.Log.Info("Using default Redis URL")
		return constants.DEFAULT_REDIS_URL
	}
}

// Returns max number of threads to use for VCenter operations.  Either default or overriden value.
func (cs *ConfigStruct) GetVCenterWorkThreads() int32 {

	if cs.VCenterWorkThreads > 0 {
		logger.Log.Info("Using override Max Worker Threads")
		return cs.VCenterWorkThreads
	} else {
		logger.Log.Info("Using default Max Worker Threads")
		return constants.DEFAULT_VCENTER_WORK_THREADS
	}
}

// Returns address book path.  Either default or overriden value.
func (cs *ConfigStruct) GetAddressBookPath() string {
	if len(cs.AddressBookPath) > 0 {
		logger.Log.Info("Using override address book path")
		return cs.AddressBookPath
	} else {
		logger.Log.Info("Using default address book path")
		return constants.DEFAULT_ADDRESSBOOK_PATH
	}
}

// Returns if Redis disabled
func (cs *ConfigStruct) GetRedisDisabled() bool {
	return cs.RedisDisabled
}

// Returns Redis key TTL expiration in seconds
func (cs *ConfigStruct) GetRedisTTLSecs() int32 {

	if cs.RedisTTLSecs > 0 {
		logger.Log.Info(fmt.Sprintf("Using override Redis TTL value: %d", cs.RedisTTLSecs))
		return cs.RedisTTLSecs
	} else {
		logger.Log.Info(fmt.Sprintf("Using default Redis TTL value: %d", constants.DEFAULT_REDIS_TTL_SECS))
		return constants.DEFAULT_REDIS_TTL_SECS
	}
}

// Returns AppDash HTTP server port.  Either default or overriden value.
func (cs *ConfigStruct) GetAppDashHttpPort() int32 {
	if cs.AppDashHttpPort > 0 {
		logger.Log.Info("Using override AppDash HTTP port")
		return cs.AppDashHttpPort
	} else {
		logger.Log.Info("Using default AppDash HTTP port")
		return constants.DEFAULT_APPDASH_HTTP_PORT
	}
}

// Returns AppDash collector server port.  Either default or overriden value.
func (cs *ConfigStruct) GetAppDashCollectorPort() int32 {
	if cs.AppDashCollectorPort > 0 {
		logger.Log.Info("Using override AppDash Collector port")
		return cs.AppDashCollectorPort
	} else {
		logger.Log.Info("Using default AppDash Collector port")
		return constants.DEFAULT_APPDASH_COLLECTOR_PORT
	}
}

// Returns the number of Redis work queue threads to start in the background.  Either default or overriden value.
func (cs *ConfigStruct) GetRedisWorkQueueThreads() int32 {
	if cs.RedisWorkThreads > 0 {
		logger.Log.Info("Using override Redis work queue thread count")
		return cs.RedisWorkThreads
	} else {
		logger.Log.Info("Using default Redis work queue thread count")
		return constants.DEFAULT_REDIS_WORK_THREADS
	}
}

// Returns the SQLite data folder
func (cs *ConfigStruct) GetSQLiteDataFolder() string {
	if len(cs.SQLiteDataFolder) > 0 {
		logger.Log.Info("Using override SQLite data folder")
		return cs.SQLiteDataFolder
	} else {
		logger.Log.Info("Using default SQLite data folder")
		return constants.DEFAULT_SQLITE_DATA_FOLDER
	}
}

// Returns the "balance category name" that should match the tag category name in VCenter
func (cs *ConfigStruct) GetBalanceCategory() string {
	if len(cs.BalanceCategory) > 0 {
		logger.Log.Info("Using override VCenter balance rack category name")
		return cs.BalanceCategory
	} else {
		logger.Log.Info("Using default VCenter balance rack category name")
		return constants.DEFAULT_VCENTER_CATEGORY_RACKS
	}
}

// Returns the "balance category name" used for VCenter hypervisor tags
func (cs *ConfigStruct) GetHypervisorTagCategory() string {
	if len(cs.BalanceCategory) > 0 {
		logger.Log.Info("Using override VCenter balance hypervisor category name")
		return cs.BalanceCategory
	} else {
		logger.Log.Info("Using default VCenter balance hypervisor category name")
		return constants.DEFAULT_VCENTER_CATEGORY_HV_TAGS
	}
}

// Returns the VCenter direct access mode which is one of ["none" | "all" | "admin"]
func (cs *ConfigStruct) GetVCenterDirectAllow() string {
	if len(cs.VCenterDirectAllow) > 0 {
		logger.Log.Info("Using override VCenter direct mode")
		return cs.VCenterDirectAllow
	} else {
		logger.Log.Info("Using default VCenter direct mode value")
		return constants.DEFAULT_VCENTER_DIRECT_ALLOW
	}
}

// Returns the maximum number of worker threads to use when retrieving NFS configuration by SSH exec call
func (cs *ConfigStruct) GetNfsConfigRetrievalMaxThreads() int32 {
	if cs.NfsConfigRetrieveMaxThreads > 0 {
		return cs.NfsConfigRetrieveMaxThreads
	} else {
		return constants.DEFAULT_NFS_CONFIG_RETRIEVE_MAX_THREADS
	}
}

// Returns the username used by SSH exec call
func (cs *ConfigStruct) GetNfsConfigRetrieveUsername() string {
	if !util.IsBlank(cs.NfsConfigRetrieveUsername) {
		return cs.NfsConfigRetrieveUsername
	} else {
		return constants.DEFAULT_NFS_CONFIG_RETRIEVE_USERNAME
	}
}

// Returns the password used by SSH exec call
func (cs *ConfigStruct) GetNfsConfigRetrievePassword() string {
	if !util.IsBlank(cs.NfsConfigRetrievePassword) {
		return cs.NfsConfigRetrievePassword
	} else {
		return constants.DEFAULT_NFS_CONFIG_RETRIEVE_PASSWORD
	}
}

// Returns the Dora environment label or "" if none set
func (cs *ConfigStruct) GetEnvironmentLabel() string {
	// NOTE: There is no default value for this config parameter
	return cs.EnvironmentLabel
}

// Returns the SMTP server in form "host:port"
func (cs *ConfigStruct) GetSmtpServer() string {
	if util.IsNotBlank(cs.SmtpServer) {
		return cs.SmtpServer
	} else {
		return constants.DEFAULT_SMTP_SERVER
	}
}

// Returns the sync warning "look-back" range -- the number of records to look back when performing stats calculation
func (cs *ConfigStruct) GetSyncWarnHistoryRange() int32 {
	if cs.SyncWarnHistoryRange > 0 {
		return cs.SyncWarnHistoryRange
	} else {
		return constants.DEFAULT_SYNC_WARN_HISTORY_RANGE
	}
}

// Returns the sync warning threshold which is a multiplier over the average which is th actual time limit that
// would invoke a warning email
func (cs *ConfigStruct) GetSyncWarnThreshold() float64 {
	if cs.SyncWarnThreshold > 0 {
		return cs.SyncWarnThreshold
	} else {
		return constants.DEFAULT_SYNC_WARN_THRESHOLD
	}
}

// Returns the comma separated list of email addresses to mail alerts to
func (cs *ConfigStruct) GetSyncWarnRecipients() string {
	if util.IsNotBlank(cs.SyncWarnRecipients) {
		return cs.SyncWarnRecipients
	} else {
		return constants.DEFAULT_SYNC_WARN_RECIPIENTS
	}
}

// Returns the "from" email address for mailing of Sync Warn messages
func (cs *ConfigStruct) GetSyncWarnFromAddress() string {
	if util.IsNotBlank(cs.SyncWarnFromAddress) {
		return cs.SyncWarnFromAddress
	} else {
		return constants.DEFAULT_SYNC_WARN_FROM_ADDRESS
	}
}