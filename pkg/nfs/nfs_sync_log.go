package nfs

import (
	"context"
	"crawshaw.io/sqlite"
	"dora/pkg/api"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/util"
	"errors"
	"fmt"
	"sync"
	"time"
)

//
//
// This module contains the code to read the sync log information to populate the UI and also to actually log NFS data
// sync activity that saves the raw logging data
//
//

// Gets the NfsSyncHistory database record matching the given ID.  Otherwise a nil reference is returned if
// no matching record is found.
func GetNfsSyncHistoryRecord(ctx context.Context, connection *sqlite.Conn, id int64) (*api.NfsSyncHistoryRec, error) {
	// This is needed for crawshaw SQLite DB driver because it may cause "concurrent write" panic within crawshaw library
	defer nfsDbSyncMutex.Unlock()
	nfsDbSyncMutex.Lock()

	if stmt, errStmt := connection.Prepare("SELECT * FROM NfsSyncHistory WHERE id = ?"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end otherwise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind
		stmt.BindInt64(1, id)

		if isRowReturned, errExec := stmt.Step(); errExec != nil {
			// FATAL
			return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
		} else {
			if isRowReturned {
				var syncHistoryRec = api.NfsSyncHistoryRec{
					SyncHistoryId:    id,
					EnvironmentName:  stmt.GetText("environment_name"),
					Status:           stmt.GetText("status"),
					InitiatedBy:      stmt.GetText("initiated_by"),
					StartTimeMs:      stmt.GetInt64("start_time_ms"),
					EndTimeMs:        stmt.GetInt64("end_time_ms"),
					RecordsProcessed: stmt.GetInt64("records_processed"),
				}

				return &syncHistoryRec, nil
			} else {
				// No matching record
				return nil, nil
			}
		}
	}
}

// Gets the 'n' latest NfsSyncHistory records for this environment.  Sort on start time descendingly (latest first).
//
func GetNfsSyncHistoryRecordsRecent(ctx context.Context, connection *sqlite.Conn, n int32) (*api.NfsSyncHistoryRecArray, error) {
	// This is needed for crawshaw SQLite DB driver because it may cause "concurrent write" panic within crawshaw library
	defer nfsDbSyncMutex.Unlock()
	nfsDbSyncMutex.Lock()

	// Result will be accumulated and returned here
	var nfsSyncHistoryRecs = api.NfsSyncHistoryRecArray{}

	if stmt, errStmt := connection.Prepare("SELECT * FROM NfsSyncHistory ORDER BY start_time_ms DESC LIMIT ?"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB statement because of error: %v", errStmt))
	} else {
		// Close up statement at the end otherwise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind for limit on number to return
		stmt.BindInt64(1, int64(n))

		for {
			if isRowReturned, errStep := stmt.Step(); errStep != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errStep))
			} else {
				if isRowReturned {
					var syncHistoryRec = api.NfsSyncHistoryRec{
						SyncHistoryId:    stmt.GetInt64("id"),
						EnvironmentName:  stmt.GetText("environment_name"),
						Status:           stmt.GetText("status"),
						InitiatedBy:      stmt.GetText("initiated_by"),
						StartTimeMs:      stmt.GetInt64("start_time_ms"),
						EndTimeMs:        stmt.GetInt64("end_time_ms"),
						RecordsProcessed: stmt.GetInt64("records_processed"),
					}

					nfsSyncHistoryRecs.Items = append(nfsSyncHistoryRecs.Items, &syncHistoryRec)
				} else {
					// No more rows left -- exit loop normally
					break
				}
			}
		}
	}

	// Search completed
	return &nfsSyncHistoryRecs, nil
}

// Gets all the child sync log entries for the parent NfsSyncHistory referenced by the given ID.  Results are sorted
// by time descendingly to show latest record first.
//
func GetNfsSyncLogRecords(ctx context.Context, connection *sqlite.Conn, parentNfsSyncHistoryId int64) (*api.NfsSyncLogRecResponseArray, error) {
	// This is needed for crawshaw SQLite DB driver because it may cause "concurrent write" panic within crawshaw library
	defer nfsDbSyncMutex.Unlock()
	nfsDbSyncMutex.Lock()

	// Result will be accumulated and returned here
	var nfsSyncLogRecs = api.NfsSyncLogRecResponseArray{}

	if stmt, errStmt := connection.Prepare("SELECT * FROM NfsSyncLogEntry WHERE parent_id = ? ORDER BY time_ms DESC"); errStmt != nil {
		// FATAL
		return nil, errors.New(fmt.Sprintf("Could not create DB connection because of error: %v", errStmt))
	} else {
		// Close up statement at the end otherwise the connection cannot be returned to the pool
		defer stmt.Finalize()

		// Only one parameter to bind to select child records of parent NfsSyncHistory record
		stmt.BindInt64(1, parentNfsSyncHistoryId)

		for {
			if isRowReturned, errExec := stmt.Step(); errExec != nil {
				// FATAL
				return nil, errors.New(fmt.Sprintf("Could not execute prepared SQL statement because of error: %v", errExec))
			} else {
				if isRowReturned {
					var syncLogRec = api.NfsSyncLogRecResponse{
						ParentId:     parentNfsSyncHistoryId,
						TimeMs:       stmt.GetInt64("time_ms"),
						MessageError: stmt.GetText("msg_err"),
						MessageWarn:  stmt.GetText("msg_warn"),
						MessageInfo:  stmt.GetText("msg_info"),
					}

					nfsSyncLogRecs.Items = append(nfsSyncLogRecs.Items, &syncLogRec)
				} else {
					// No more rows left -- exit loop normally
					break
				}
			}
		}
	}

	// Search completed
	return &nfsSyncLogRecs, nil
}

// Returns the time (millis) of the latest complete successful NfsSyncHistory record.  If any error occurs, then a -1
// value is returned which should be taken as an error.  Check logs for more details.
//
func GetLatestCompletedSyncTime(ctx context.Context, connection *sqlite.Conn, ) int64 {
	// This is needed for crawshaw SQLite DB driver because it may cause "concurrent write" panic within crawshaw library
	defer nfsDbSyncMutex.Unlock()
	nfsDbSyncMutex.Lock()

	if stmt, errStmt := connection.Prepare(fmt.Sprintf("SELECT start_time_ms FROM NfsSyncHistory WHERE status = '%s' ORDER BY start_time_ms DESC LIMIT 1", constants.DATA_SYNC_STATUS_COMPLETE)); errStmt != nil {
		// Not-fatal but log it
		logger.Log.Info(fmt.Sprintf("Cannot get latest sync time because of SQL error: %v", errStmt))
		return -1
	} else {
		// Close up statement at the end otherwise the connection cannot be returned to the pool
		defer stmt.Finalize()

		if isRowReturned, errExec := stmt.Step(); errExec != nil {
			// No-fatal but log it
			logger.Log.Info(fmt.Sprintf("Cannot get latest sync time because of DB execution error: %v", errExec))
			return -1
		} else {
			if isRowReturned {
				// Successfully found latest sync time
				return stmt.GetInt64("start_time_ms")
			} else {
				// No-fatal but log it
				logger.Log.Info("Cannot get latest NFS sync time because no row was returned")
				return -1
			}
		}
	}
}

// Create a new sync log event record as a child of the given parent SyncHistory record
//
// NOTE: This is function should not be used directly.  Use the logNfsSyncHistory... functions above depending on INFO,
// WARN or ERROR levels
//
// PARAM: parentSyncHistoryId  ID of parent sync history record. A SyncHistory record has 0 or more sync log messages.
// PARAM: connection  active connection to the corresponding database
// PARAM: msgErr    set this if log is intended as an ERROR
// PARAM: msgWarn   set this if log is intended as a WARN
// PARAM: msgInfo   set this if log is intended as an INFO
// RETURN: the ID of the new log entry record
// ERROR: if any fatal exception occurs typically with DB execution problems
func insertNfsSyncLogEntry(parentSyncHistoryId int64, connection *sqlite.Conn, timestamp time.Time, msgErr, msgWarn, msgInfo string) (int64, error) {
	// This is needed for crawshaw SQLite DB driver because it may cause "concurrent write" panic within crawshaw library
	defer nfsDbSyncMutex.Unlock()
	nfsDbSyncMutex.Lock()

	if stmt, errStmt := connection.Prepare("INSERT INTO NfsSyncLogEntry VALUES (?, ?, ?, ?, ?)"); errStmt != nil {
		// Fatal
		return -1, errors.New(fmt.Sprintf("Could not create NfsSyncLogEntry record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindInt64(bindIncrementor(), parentSyncHistoryId)
		stmt.BindInt64(bindIncrementor(), timestamp.UnixNano() / 1000000)

		if !util.IsBlank(msgErr) {
			stmt.BindText(bindIncrementor(), msgErr)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if !util.IsBlank(msgWarn) {
			stmt.BindText(bindIncrementor(), msgWarn)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if !util.IsBlank(msgInfo) {
			stmt.BindText(bindIncrementor(), msgInfo)
		} else {
			stmt.BindNull(bindIncrementor())
		}

		if _, errStep := stmt.Step(); errStep != nil {
			// Fatal
			return -1, errors.New(fmt.Sprintf("Could not execute SQL statement because of error: %v", errStep))
		} else {
			// Expecting exactly one row to be returned
			// INSERT completed successfully
			return connection.LastInsertRowID(), nil
		}
	}
}

//
//
// A log buffer because SQLite writing to multiple tables in transactions is buggy and can corrupt the entire sqlite
// database file.  Log to memory and write logs out after the Dora sync operation is complete.
//
type LogBuffer struct {
	buffer  []LogRecord      // Actual buffer will be used as FIFO queue
	mutex   sync.Mutex       // For short lock because GoLang arrays are not thread safe
}

// Constructs a new log buffer
//
// PARAM: ctx      standard GoLang context
// PARAM: initMsg  optional init message that will be the first message in the log (set to empty string if not needed)
func NewLogBuffer(ctx context.Context, initMsg string) *LogBuffer {
	var logBuffer = LogBuffer{
		buffer: make([]LogRecord, 0),
	}

	// Add opening init message if specified
	if util.IsNotBlank(initMsg) {
		logBuffer.logNfsSyncHistoryInfo(ctx, initMsg)
		time.Sleep(5 * time.Millisecond) // Tiny delay to ensure this message is first
	}

	return &logBuffer
}

// Logs a WARN SyncLogEntry into the database into the log buffer
//
// PARAM: ctx          standard GoLang context
// PARAM: warnMessage  warning message
func (o * LogBuffer) logNfsSyncHistoryWarn(ctx context.Context, warnMessage string) {
	// A short lock because GoLang arrays are not thread safe
	defer o.mutex.Unlock()
	o.mutex.Lock()

	o.buffer = append(o.buffer, NewLogRecord("", warnMessage, ""))
}

// Logs an ERROR SyncLogEntry into the log buffer
//
// PARAM: ctx         standard GoLang context
// PARAM: errMessage  error message
func (o * LogBuffer) logNfsSyncHistoryError(ctx context.Context, errMessage string) {
	// A short lock because GoLang arrays are not thread safe
	//defer o.mutex.Unlock()
	o.mutex.Lock()

	o.buffer = append(o.buffer, NewLogRecord(errMessage, "", ""))
}

// Logs an INFO SyncLogEntry into the log buffer
//
// PARAM: ctx         standard GoLang context
// PARAM: infoMessage info message
func (o * LogBuffer) logNfsSyncHistoryInfo(ctx context.Context, infoMessage string) {
	// A short lock because GoLang arrays are not thread safe
	defer o.mutex.Unlock()
	o.mutex.Lock()

	o.buffer = append(o.buffer, NewLogRecord("", "", infoMessage))
}

// Clears the log buffer
//
func (o *LogBuffer) clear() {
	// A short lock because GoLang arrays are not thread safe
	defer o.mutex.Unlock()
	o.mutex.Lock()

	o.buffer = make([]LogRecord, 0)
}

// Writes all the currently buffered logs to the database
//
// NOTE: Buffer is cleared after all writes completed successfully
//
// PARAM: parentSyncHistoryId  the parent record of all the logs
// PARAM: connection           a live connection to the database to insert logs into
//
// ERROR: If any error occurs during writing.  The client should rollback.  Log buffer remains intact for retry.
func (o * LogBuffer) WriteAllLogs(parentSyncHistoryId int64, connection *sqlite.Conn) error {
	//  Lock to ensure all-or-nothing write
	defer o.mutex.Unlock()
	o.mutex.Lock()

	// Write each buffered log record out in FIFO queue order to the database via provided database connection object
	for _, logRecord := range o.buffer {
		if _, errLogInsert := insertNfsSyncLogEntry(parentSyncHistoryId, connection, logRecord.Time, logRecord.ErrMsg, logRecord.WarnMsg, logRecord.InfoMsg); errLogInsert != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Fatal error encountered while writing log record.  Client should roll back and try again.  Error: %v", errLogInsert))
		}
	}

	// Clear the buffer
	o.buffer = make([]LogRecord, 0)

	// Success if this point is reached
	return nil
}


//
//
// Log record
//
//
type LogRecord struct {
	Time      time.Time
	InfoMsg   string
	WarnMsg   string
	ErrMsg    string
}

// Creates a new log record.  Only one field should be filled and the others an empty string ("" value)
//
// PARAM: msgErr   error message or "" if not an error
// PARAM: msgWarn  warning message or "" if not a warning
// PARAM: msgInfo  info message or "" if not an informational message
func NewLogRecord(msgErr, msgWarn, msgInfo string) LogRecord {
	return LogRecord {
		Time:    time.Now(),
		InfoMsg: msgInfo,
		WarnMsg: msgWarn,
		ErrMsg:  msgErr,
	}
}