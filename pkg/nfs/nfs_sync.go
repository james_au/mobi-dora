package nfs

import (
	"context"
	"crawshaw.io/sqlite"
	"dora/pkg/addressbook"
	"dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/dbutil"
	"dora/pkg/logger"
	"dora/pkg/mail"
	"dora/pkg/util"
	"dora/pkg/util/threadsafequeue"
	"dora/pkg/utilssh"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jszwec/csvutil"
	"golang.org/x/crypto/ssh"
	"net"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

const NFS_TYPE_ISILON = "Isilon"
const NFS_TYPE_QUMULO = "Qumulo"
const NFS_TYPE_UNRETRIEVED = "<unretrieved>"
const NFS_TYPE_UNKNOWN = "<unknown>"

// Number of times process kill will wait to give process time to terminate normally without requiring hard kill
const KILL_DEFER_LIMIT = 3

//
//
// This module contains the code to actually execute the NFS data sync logic via remote SSH commands
//
//


// The command to filter out NFS related entries in the /etc/fstab file
//

var splitPattern = regexp.MustCompile("\\s+")
var pathTrimPattern = regexp.MustCompile("/+$")

// Sync mutex required for access because crawshaw SQLite driver library cannot handle multiple threads and can panic
// out with Go "concurrent map" access fatal errors.  Functions in this package are also typically access from multiple
// parallel go routines as well as from requests from the web UI.
var nfsDbSyncMutex sync.Mutex

// Retrieve NFS configuration from the given list of hosts.  This method runs asynchronously so clients must
// communicate with the returned channel object.  Performance can be increased when maxThread > 1.
//
// NOTE: Parallel operation occurs if maxThreads > 1 and the processing order of host may appear random
// NOTE: the boolean "|| true" is needed otherwise grep return status code 1 which is an error code and the SSH library errors out
// NOTE: the FSTAB_MARKER is needed to print out a header that will be used to identify this specific output.  This is
//       for the case where the host contains login output like MOTD and other output from its bash profile which would
//       otherwise be mistaken as an fstab entry
// NOTE: This function does *not* need or use DB connection because it only performs SSH and returns results via channel for caller to process as needed
// PARAM: hosts   a list of 1 or more hosts (FQDN) to process. Ex: stmmv01p1.paytv.smf1.mobitv
// PARAM: maxThreads maximum number of worker threads for parallel processing or use 1 to for single threaded
func RetrieveNfsConfigs(ctx context.Context, vcPaths []string, syncHistoryId int64, logBuffer *LogBuffer, maxThreads int) (chan *ResultStruct, error) {
	const FSTAB_MARKER = "** DORA FSTAB GREP OUTPUT **"
	const SSH_FSTAB_COMMAND = "echo '" + FSTAB_MARKER + "'; grep ' nfs ' /etc/fstab || true"
	const SSH_DF_COMMAND_TEMPLATE = "df -P | grep " // -P option forces df POSIX format so it can be parsed reliably across OS types
	const SSH_STAT_PERMS_COMMAND_TEMPLATE = "stat -c '%m:::%U (%u):::%G (%g):::%a' " // stat retrieves a specified local path stats in format: username (id):::groupname(id):::octal perms
	const SSH_EXEC_TIMEOUT_DURATION = 6 * time.Second            // Force timeout if SSH remote exec takes longer than this duration

	// Caches NFS type look (Isilon, Qumulo) because one NFS server can service multiple clients and caching
	// eliminates unnecessary repeated SSH lookup calls
	var nfsTypeCache = newNfsCache()

	// Helps with debugging NFS thread activity per environment
	var nfsThreadMap = NewThreadMap()

	// Sanity check
	if maxThreads < 1 {
		// Fatal
		return nil, errors.New(fmt.Sprintf("maxThreads argument must be greater than 0 but received %d instead", maxThreads))
	}

	// Setup SSH context configuration from config (timeout) and setup SSH access
	var sshExec = utilssh.SshExec{
		TimeoutConnectMs:   3000, // TODO: should be configurable
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	// Store list of all hosts to query in a thread safe queue for concurrent access by multiple worker threads below
	var workQueue = threadsafequeue.New(createNfsWorkItems(vcPaths...))

	// Output to client will be returned via this channel
	var outputChannel = make(chan *ResultStruct, 1024)

	// Internal worker thread return via this channel
	var workerResultsChannel = make(chan *ResultStruct)

	// Use a thread safe queue to keep track of number of items processed -- required to know when streaming is complete
	var completionQueue = threadsafequeue.New(nil)

	// Get required username and fail out if no username configured
	var username = config.Config.GetNfsConfigRetrieveUsername()
	if util.IsBlank(username) {
		return nil, errors.New("Cannot retrieve NFS configuration because no SSH username was given.")
	}

	// Ensure either SSH password or SSH private key auth is specified
	if util.IsBlank(config.Config.GetNfsConfigRetrievePassword()) && util.IsBlank(sshExec.PrivateKeyFilePath) {
		return nil, errors.New("No SSH password or SSH private key file was specified.  Cannot log in to any host.")
	}

	// Start a number of worker background go routine threads as specified in configuration
	var actualThreadCount = util.Min(maxThreads, len(vcPaths))
	logger.Log.Info(fmt.Sprintf("Starting %d worker threads to process NFS configuration from %d hosts", actualThreadCount, len(vcPaths)))
	for i := 0; i < actualThreadCount; i++ {
		go func() {
			// Get unique sequence number to identify this thread
			var threadNumber = getThreadSequenceNumber()

			// Keep list for post-thread completion statistics/debugging output for the hosts this worker thread
			// processed.
			var hostsProcessed []string

			// Keep list of opened SSH clients so they can be closed in defer call when this thread completes.
			// This helps ensure SSH clients are closed even if thread crashes out or exits unexpectedly.
			var sshClientsList []*ssh.Client
			defer func() {
				var countSuccess = 0
				var countFail = 0

				logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: About to close %d SSH active connections...", threadNumber, len(sshClientsList)))
				for _, sshClient := range sshClientsList {
					if sshClient != nil {
						var errClose = sshClient.Close()

						if errClose != nil {
							// Warn - unlikely but warn about it
							logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: WARN: Could not close SSH client because of error: %v", threadNumber, errClose))
							countFail++
						} else {
							countSuccess++
						}
					}
				}

				logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Successfully closed: %d / Failed close: %d", threadNumber, countSuccess, countFail))
			}()

			outer:
			for {
				// "Pull" untyped item object from work queue (in FIFO "queue" order)
				var object = workQueue.Pull()

				// Get the generic work item object and process according to its actual runtime type
				//
				switch workItem := object.(type) {
				case NfsWorkItem:
					// Extract the host's FQDN from its VCenter path so that SSH connection can be made to it
					var fqdn = util.ExtractFqdnFromVCPath(workItem.VcPath)

					// Update tracker this thread is working on host FQDN
					nfsThreadMap.Update(threadNumber, fqdn)

					if workItem.RetriesRemaining > 0 {
						// Decrement retry remaining on the work item since about to perform one attempt
						workItem.RetriesRemaining--
						//logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d starting to process host: %s", threadNumber, host))

						// Open SSH connection to the host
						if sshClient, errClient := utilssh.OpenSSHClient(sshExec, fqdn, username, config.Config.GetNfsConfigRetrievePassword()); errClient != nil {
							// Not fatal but return it as an error in the result queue
							var errorMsg = fmt.Sprintf("Warning: Failed to create SSH context because of error: %v", errClient)

							logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d could not connect to host %s because of error: %v", threadNumber, fqdn, errClient))

							// Add error result to internal channel
							workerResultsChannel <- &ResultStruct{
								Error:    errorMsg,
								Hostname: fqdn,
							}
						} else {
							logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d successfully connected to host %s", threadNumber, fqdn))

							// Save the client to outer list so they can all be closed in a final defer call.  Cannot use
							// defer() here because the worker thread is not likely to exit between SSH invocations before
							// starting another SSH connection to process the next work queue item.
							//
							// UPDATE: Alternatively, a single-threaded daemon that accepts SSH connection objects on a
							// channel can improve performance and connections can be closed faster and independently
							// regardless of original thread crashing.
							sshClientsList = append(sshClientsList, sshClient)

							// Execute SSH remote exec on host
							if outputFstab, errExec := sshExec.RemoteRun(sshClient, SSH_FSTAB_COMMAND, "", &utilssh.UNIX_PRIORITY_MIN); errExec != nil {
								// Not fatal but return it as an error in the result queue
								var errorMsg = fmt.Sprintf("Warning: Failed to execute SSH remote command \"%s\" on host \"%s\" because of error: %v", SSH_FSTAB_COMMAND, fqdn, errExec)

								logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d failed to execute command '%s' on host %s because of error: %v", threadNumber, SSH_FSTAB_COMMAND, fqdn, errExec))

								// Add error result to internal channel
								workerResultsChannel <- &ResultStruct{
									Error:    errorMsg,
									Hostname: fqdn,
								}
							} else {
								var lines = strings.Split(strings.TrimSpace(outputFstab), "\n")

								if !util.IsBlank(outputFstab) && lines != nil && len(lines) > 0 {
									var foundMarker = false
									for _, fstabLine := range lines {
										var resultStruct = ResultStruct{
											NfsType: NFS_TYPE_UNRETRIEVED,
										}
										if !util.IsBlank(fstabLine) {
											// Skip any extraneous SSH login info lines until the marker line is found
											if fstabLine == FSTAB_MARKER {
												foundMarker = true
												continue
											} else if !foundMarker {
												// Skip line as marker was not found yet
												logger.Log.Info(fmt.Sprintf("Ignoring SSH extraneous line for host %s: \"%s\"", fqdn, fstabLine))
												continue
											}

											if foundMarker && !isCommentedOut(fstabLine) {
												var split = splitPattern.Split(fstabLine, -1)

												// Must have at least 6 columns
												if len(split) >= 6 {
													// Save fstab results to the data record (note: capacity information looked up later below)
													resultStruct.HostVcPath = workItem.VcPath
													resultStruct.Hostname = fqdn
													resultStruct.HostMount = strings.TrimSpace(split[1])
													resultStruct.NfsHost = extractNfsHost(strings.TrimSpace(split[0]))
													resultStruct.NfsPath = extractNfsPath(strings.TrimSpace(split[0]))
													resultStruct.FstabLine = fstabLine

													// Now issue a "df" command and grep for the mount point discovered above to get the
													// NFS mount's available capacity and remaining capacity as reported by the operating
													// system
													//
													if pids, errProcessCount := utilssh.GetExistingProcessesOnHost(sshExec, sshClient, username, SSH_DF_COMMAND_TEMPLATE); errProcessCount != nil {
														// Warn - could not get process count so cannot continue with dF
														logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d cannot check for existing processes because of error: %v", threadNumber, errProcessCount))

														// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
														logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
														workQueue.Add(workItem); continue outer
													} else {
														// Create the actual "df" command by adding the grep pattern for the
														// given mount
														var dfCommand = SSH_DF_COMMAND_TEMPLATE + pathTrimPattern.ReplaceAllString(resultStruct.HostMount, "") + " || true"

														if len(pids) > 0 {
															// Warn - there is a stale hung "df" process from a previous run
															var logMsg = fmt.Sprintf("SSH NFS Worker thread #%02d on VM %s: Warn: There are hung processes for command \"%s\" with PIDs: %v. Will attempt to kill these hung processes...", threadNumber, workItem.VcPath, dfCommand, pids)
															logger.Log.Info(logMsg)
															logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)

															// Kill hung processes on host via the identified PIDs above
															var warnings = utilssh.KillProcessesOnHost(sshExec, sshClient, pids...)
															if warnings != nil || len(warnings) > 0 {
																// Not fatal but log it
																var logPids = fmt.Sprintf("WARN: Some hung PIDs for hung command '%s' on VM %s could not be killed.  Warnings received: %v", dfCommand, fqdn, warnings)
																logger.Log.Info(logPids)
																logBuffer.logNfsSyncHistoryWarn(ctx, logPids)
															} else {
																var logMsg = fmt.Sprintf("Successfully killed hung commands '%s' with PIDs: %v on VM %s", dfCommand, pids, fqdn)
																logger.Log.Info(logMsg)
																logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)
															}

															// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
															var logRetryMsg = fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining)
															logger.Log.Info(logRetryMsg)
															logBuffer.logNfsSyncHistoryWarn(ctx, logRetryMsg)
															workQueue.Add(workItem); continue outer
														} else {
															//
															// Okay to run the DF command
															//
															if outputDf, errTimeout, errDfExec := sshExec.RemoteRunWithTimeout(sshClient, dfCommand, "", SSH_EXEC_TIMEOUT_DURATION, &utilssh.UNIX_PRIORITY_MIN); errDfExec != nil {
																// Warn
																var logMsg = fmt.Sprintf("SSH NFS Worker thread #%02d failed to execute command '%s' on host %s because of error: %v", threadNumber, dfCommand, fqdn, errDfExec)
																logger.Log.Info(logMsg)
																logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)

																// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																var logRetryMsg = fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining)
																logger.Log.Info(logRetryMsg)
																logBuffer.logNfsSyncHistoryWarn(ctx, logRetryMsg)
																workQueue.Add(workItem); continue outer
															} else if errTimeout != nil {
																// Warn - failed because of timeout (not actual remote-side error)
																logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d timed out after %v waiting for command '%s' on host %s", threadNumber, SSH_EXEC_TIMEOUT_DURATION, dfCommand, fqdn))

																// Find and kill the df process to not leave it hanging.
																// NOTE: the df command has been known to hard hang the shell and remain running indefinitely if the host has NFS networking
																// connectivity problems!  This problem was seen on the juasp nodes with network issues.
																//
																if pidsLookup, errProcessLookup := utilssh.GetExistingProcessesOnHost(sshExec, sshClient, username, SSH_DF_COMMAND_TEMPLATE); errProcessLookup != nil {
																	// Not fatal but log it
																	logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Attempt to find hanging command '%s' failed because of error: %v", threadNumber, dfCommand, errProcessLookup))
																} else {
																	// Manually kill the hanging processes
																	logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d attempting to kill timed out command '%s' on host %s...", threadNumber, dfCommand, fqdn))
																	var warnings = utilssh.KillProcessesOnHost(sshExec, sshClient, pidsLookup...)

																	if warnings != nil && len(warnings) > 0 {
																		logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Previous command '%s' hung and tried to manually kill it but encountered warnings: %v", threadNumber, dfCommand, warnings))
																	} else {
																		logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d successfully killed hung command '%s' on host %s...", threadNumber, dfCommand, fqdn))
																	}
																}

																// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																workQueue.Add(workItem); continue outer
															} else {
																// Remote "df" command successful so process its results
																//
																if !util.IsBlank(outputDf) {
																	var lines = strings.Split(strings.TrimSpace(outputDf), "\n")

																	// Expecting exactly 1 line.  But could be 0 if somehow there is mismatch
																	// between the mount point defined in the /etc/fstab and the actual mount
																	// point reported by the "df" command.  This can happen if the mount fails to
																	// mount or becomes disconnected.
																	for _, dfLine := range lines {
																		var split = splitPattern.Split(dfLine, -1)

																		// Must have 6 columns
																		if len(split) == 6 {
																			// Parse the capacity bytes from "df" kilobytes
																			if capacityKiloBytes, errParse := strconv.ParseInt(split[1], 10, 64); errParse != nil {
																				// Warn
																				logger.Log.Error(errParse, "Could not parse mount's 'capacity 1024-blocks' value")
																			} else {
																				resultStruct.CapacityBytes = 1024 * capacityKiloBytes
																			}

																			// Parse the capacity used bytes from "df" kilobytes
																			if capacityUsedKiloBytes, errParse := strconv.ParseInt(split[2], 10, 64); errParse != nil {
																				// Warn
																				logger.Log.Error(errParse, "Could not parse mount's 'capacity used 1024-blocks' value")
																			} else {
																				resultStruct.CapacityUsedBytes = 1024 * capacityUsedKiloBytes
																			}
																		} else {
																			logger.Log.Info(fmt.Sprintf("The \"df\" output did not contain the exected number of columns: %s", dfLine))

																			// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																			workQueue.Add(workItem); continue outer
																		}
																	}
																}

																// Find and kill any hanging stat process(es) on the VM host before starting any another stat command
																//
																var statCommand = SSH_STAT_PERMS_COMMAND_TEMPLATE + " " + resultStruct.HostMount
																if pidsLookup, errProcessLookup := utilssh.GetExistingProcessesOnHost(sshExec, sshClient, username, SSH_STAT_PERMS_COMMAND_TEMPLATE); errProcessLookup != nil {
																	// Not fatal but log it
																	logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Attempt to find hanging command '%s' failed because of error: %v", threadNumber, statCommand, errProcessLookup))
																} else {
																	if len(pidsLookup) > 0 {
																		// Manually kill the hanging processes
																		logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d attempting to kill timed out command '%s' on host %s...", threadNumber, statCommand, fqdn))
																		var warnings = utilssh.KillProcessesOnHost(sshExec, sshClient, pidsLookup...)

																		if warnings != nil && len(warnings) > 0 {
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Previous command '%s' hung and tried to manually kill it but encountered warnings: %v", threadNumber, statCommand, warnings))

																			// ABORT this VM retrieval: Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																			workQueue.Add(workItem);
																			continue outer
																		} else {
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d successfully killed hung command '%s' on host %s...", threadNumber, statCommand, fqdn))
																		}
																	}
																}

																if outputStat, errTimeout, errStatExec := sshExec.RemoteRunWithTimeout(sshClient, statCommand, "", SSH_EXEC_TIMEOUT_DURATION, &utilssh.UNIX_PRIORITY_MIN); errStatExec != nil {
																	// Warn
																	var logMsg = fmt.Sprintf("SSH NFS Worker thread #%02d failed to execute command '%s' on host %s because of error: %v", threadNumber, statCommand, fqdn, errStatExec)
																	logger.Log.Info(logMsg)
																	logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)

																	// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																	var logRetryMsg = fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining)
																	logger.Log.Info(logRetryMsg)
																	logBuffer.logNfsSyncHistoryWarn(ctx, logRetryMsg)
																	workQueue.Add(workItem); continue outer
																} else if errTimeout != nil {
																	// Warn - failed because of timeout (not actual remote-side error)
																	logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d timed out after %v waiting for command '%s' on host %s", threadNumber, SSH_EXEC_TIMEOUT_DURATION, statCommand, fqdn))

																	// Find and kill the stat process to not leave it hanging
																	//
																	if pidsLookup, errProcessLookup := utilssh.GetExistingProcessesOnHost(sshExec, sshClient, username, SSH_STAT_PERMS_COMMAND_TEMPLATE); errProcessLookup != nil {
																		// Not fatal but log it
																		logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Attempt to find hanging command '%s' failed because of error: %v", threadNumber, statCommand, errProcessLookup))
																	} else {
																		// Manually kill the hanging processes
																		logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d attempting to kill timed out command '%s' on host %s...", threadNumber, statCommand, fqdn))
																		var warnings = utilssh.KillProcessesOnHost(sshExec, sshClient, pidsLookup...)

																		if warnings != nil && len(warnings) > 0 {
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Warn: Previous command '%s' hung and tried to manually kill it but encountered warnings: %v", threadNumber, statCommand, warnings))
																		} else {
																			logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d successfully killed hung command '%s' on host %s...", threadNumber, statCommand, fqdn))
																		}
																	}

																	// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																	logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																	workQueue.Add(workItem); continue outer
																} else {
																	// Remote "stat" command successful so process its results
																	//
																	var splitStatOutput = strings.Split(outputStat, ":::")

																	// Get relevant fields. NOTE: first element is the mount name and not needed here
																	resultStruct.HostMountUsername = splitStatOutput[1]
																	resultStruct.HostMountGroupname = splitStatOutput[2]
																	resultStruct.HostMountPerms = splitStatOutput[3]
																}
															}
														}
													}

													//
													// Log onto NFS server and check for type (Isilon, Qumulo, etc)
													//
													var hostLock = nfsTypeCache.lockHost(resultStruct.NfsHost)

													// Look up cache item for this host and try to get cached value when
													// available
													var nfsTypeCacheRec = nfsTypeCache.GetEntry(resultStruct.NfsHost)

													// Lock for this host so no other thread can log into it while the data
													// is retrieved.  Other threads should be able to see cached data when owner
													// completes.
													//
													// NOTE: Make sure to UNLOCK if "continue" to restart the loop for when setting up retry!
													if nfsTypeCacheRec.Type == NFS_TYPE_UNRETRIEVED {
														// Cache miss.  Perform SSH lookup on the NFS host for Isilon's name (via the "isi stat" command line)
														//
														logger.Log.Info(fmt.Sprintf("Checking if host %s is Isilon...", resultStruct.NfsHost))
														isilonClusterName, errIsilon, errTimeout := RetrieveIsilonName(resultStruct.NfsHost, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Username, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Password)

														logger.Log.Info(fmt.Sprintf("NSF ClusterName Cache Miss for Isilon NFS host '%s'", resultStruct.NfsHost))

														if errIsilon != nil {
															// Detected no Isilon server
															// NOTE: no need to retry this error because subsequent retries would just result in the same error
															logger.Log.Info(fmt.Sprintf("Isilon detection for '%s' is probably not an Isilon server because of execution error: %v", resultStruct.NfsHost, errIsilon))
														} else if errTimeout != nil {
															// Inconclusive but probably not an Isilon server if networking is okay
															logger.Log.Info(fmt.Sprintf("Isilon detection inconclusive for host at '%s' because of timeout error: %v", resultStruct.NfsHost, errTimeout))

															// Do not add this to retry list yet in order to allow to check it as Qumulo below
														} else {
															// Successfully identified Isilon host
															logger.Log.Info(fmt.Sprintf("Host %s identified as Isilon (clustername: %s)", resultStruct.NfsHost, isilonClusterName))
															resultStruct.NfsType = NFS_TYPE_ISILON
															resultStruct.NfsClusterName = isilonClusterName

															// Save the entry to a simple hash cache since one NFS server typically is mounted by several hosts and
															// avoid an SSH call for the same host next time
															//nfsTypeCache.PutEntry(resultStruct.NfsHost, &NfsType{resultStruct.NfsType, resultStruct.NfsClusterName})
															nfsTypeCacheRec.Type = resultStruct.NfsType
															nfsTypeCacheRec.ClusterName = resultStruct.NfsClusterName
														}

														// Continue check if host is Qumulo if above Isilon detection did not confirm it is Isilon
														if resultStruct.NfsType == NFS_TYPE_UNRETRIEVED {
															// Check for Qumulo if Isilon detection failed above
															logger.Log.Info(fmt.Sprintf("Checking if host %s is Qumulo...", fqdn))
															// Retrieve Qumulo name via the 'qq cluster_conf' command
															qumuloClusterName, errQumulo, errTimeout := RetrieveQumuloName(resultStruct.NfsHost, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Qumulo.Username, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Qumulo.Password)

															if errQumulo != nil {
																// Detected no Qumulo server
																// NOTE: no need to retry this error because subsequent retries would just result in the same error
																logger.Log.Info(fmt.Sprintf("Qumulo detection for '%s' is probably not a Qumulo server because of execution error: %v", resultStruct.NfsHost, errQumulo))
															} else if errTimeout != nil {
																// Inconclusive but probably not a Qumulo server if networking is okay
																logger.Log.Info(fmt.Sprintf("Qumulo detection inconclusive for host at '%s' because of timeout error: %v", resultStruct.NfsHost, errTimeout))

																// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																workQueue.Add(workItem); hostLock.Unlock(); continue outer
															} else {
																// Successfully identified Qumulo host
																logger.Log.Info(fmt.Sprintf("Host %s identified as Qumulo (clustername: %s)", resultStruct.NfsHost, qumuloClusterName))
																resultStruct.NfsType = NFS_TYPE_QUMULO
																resultStruct.NfsClusterName = qumuloClusterName

																// Save the entry to a simple hash cache since one NFS server typically is mounted by several hosts and
																// avoid an SSH call for the same host next time
																//nfsTypeCache.PutEntry(resultStruct.NfsHost, &NfsType{resultStruct.NfsType, resultStruct.NfsClusterName})
																nfsTypeCacheRec.Type = resultStruct.NfsType
																nfsTypeCacheRec.ClusterName = resultStruct.NfsClusterName
															}
														}

														if resultStruct.NfsType == NFS_TYPE_UNRETRIEVED	{
															// Cannot detect type of NFS host!
															//
															// Force NFS type to UNKNOWN so that this NFS host is not
															// retried over and over again by successive threads
															resultStruct.NfsType = NFS_TYPE_UNKNOWN  // Update current value
															nfsTypeCacheRec.Type = NFS_TYPE_UNKNOWN  // Update cached value too

															var logMsg = fmt.Sprintf("NFS host '%s' used by VM %s could not be identified. Possible login issues. Check for further details in console log.", resultStruct.NfsHost, fqdn)
															logger.Log.Info(logMsg)
															logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)
														}
													} else {
														// Cache hit.  Skip SSH and just use the cached value for this host
														resultStruct.NfsType = nfsTypeCacheRec.Type
														resultStruct.NfsClusterName = nfsTypeCacheRec.ClusterName
														logger.Log.Info(fmt.Sprintf("NFS ClusterName Cache Hit for Isilon NFS host '%s' clusterName '%s'", resultStruct.NfsHost, resultStruct.NfsClusterName))
													}

													//
													// ISILON: Try to lookup Isilon LNN (Logical Number Number) via remote
													// SSH exec command only if: (1) LNN has not been retrieved previously,
													// and (2) the host was identified as Isilon
													//
													if !nfsTypeCacheRec.LNNretrieved && nfsTypeCacheRec.Type == NFS_TYPE_ISILON {
														// Isilon LNN has not yet been retrieved - perform lookup and save to cache for future lookups
														//
														if ipAddressLookup, errIpLookup := util.LookupIpAddress(resultStruct.NfsHost); errIpLookup != nil {
															// Not fatal to sync process but log it
															logger.Log.Info(fmt.Sprintf("WARN: Could not retrieve Isilon LNN from host %s because cannot lookup host's IP address due to error: %v", resultStruct.NfsHost, errIpLookup, ))

															// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
															logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
															workQueue.Add(workItem); hostLock.Unlock(); continue outer
														} else {
															// Perform remote SSH command
															//
															logger.Log.Info(fmt.Sprintf("Checking if Isilon host %s at IP %s for LNN value", resultStruct.NfsHost, ipAddressLookup))
															lnn, errSsh, errTimeout, errExec := RetrieveIsilonLNN(ipAddressLookup, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Username, addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Password)

															if errSsh != nil {
																// Not fatal to sync process but log it
																logBuffer.logNfsSyncHistoryWarn(ctx, fmt.Sprintf("SSH NFS Worker thread #%02d: WARN: Could not retrieve Isilon LNN from host %s because of SSH error: %v", threadNumber, resultStruct.NfsHost, errSsh))

																if !(strings.Contains(errSsh.Error(), "unable to authenticate") && strings.Contains(errSsh.Error(), "no supported methods remain")) {
																	// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																	var logRetryMsg = fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining)
																	logger.Log.Info(logRetryMsg)
																	logBuffer.logNfsSyncHistoryWarn(ctx, logRetryMsg)

																	// Add back on queue
																	workQueue.Add(workItem); hostLock.Unlock(); continue outer
																} else {
																	// Do not add back to work queue for retry - auth issue will repeatedly error out subsequent retries would just fail again.
																	// Also the SSH lib takes a long time to figure out auth type is not allowed (1 to 2 minutes)
																	var errMsg = fmt.Sprintf("SSH NFS Worker thread #%02d: Not putting VM %s back on queue for retry because of NFS login incorrect type", threadNumber, workItem.VcPath)
																	logger.Log.Info(errMsg)
																	logBuffer.logNfsSyncHistoryWarn(ctx, errMsg)
																}
															} else if errTimeout != nil {
																// Not fatal to sync process but log it
																logger.Log.Info(fmt.Sprintf("WARN: Could not retrieve Isilon LNN from host %s because of timeout: %v", resultStruct.NfsHost, errTimeout))

																// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																logger.Log.Info(fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																workQueue.Add(workItem); hostLock.Unlock(); continue outer
															} else if errExec != nil {
																// Not fatal to sync process but log it
																var logMsg = fmt.Sprintf("WARN: Could not retrieve Isilon LNN from host %s because of command execution error: %v", resultStruct.NfsHost, errExec)
																logger.Log.Info(logMsg)
																logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)

																// Add the item back to the work item so it can be retried later (in order) and abort further processing on this work item
																logBuffer.logNfsSyncHistoryWarn(ctx, fmt.Sprintf("SSH NFS Worker thread #%02d: Putting VM %s back on queue for retry.  Retries remaining: %d", threadNumber, workItem.VcPath, workItem.RetriesRemaining))
																workQueue.Add(workItem); hostLock.Unlock(); continue outer
															} else {
																if lnn != nil {
																	var lnnCast = int64(*lnn)
																	resultStruct.NfsIsilonLNN = &lnnCast

																	// Save to cache
																	nfsTypeCacheRec.LNN = lnnCast

																	logger.Log.Info(fmt.Sprintf("NFS LNN Cache Miss for Isilon NFS host '%s' LNN '%d'", resultStruct.NfsHost, *resultStruct.NfsIsilonLNN))
																} else {
																	// Save nil result to cache - nil result indicates no LNN could be identified for this Isilon host
																	resultStruct.NfsIsilonLNN = nil
																	var logMsg = fmt.Sprintf("Could not identify Isilon NFS host '%s' LNN value", resultStruct.NfsHost)
																	logger.Log.Info(logMsg)
																	logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)
																}

																nfsTypeCacheRec.LNNretrieved = true
															}
														}
													} else {
														// CACHE HIT
														resultStruct.NfsIsilonLNN = &nfsTypeCacheRec.LNN

														if resultStruct.NfsIsilonLNN != nil {
															logger.Log.Info(fmt.Sprintf("NFS LNN Cache Hit for Isilon NFS host '%s' LNN '%d'", resultStruct.NfsHost, *resultStruct.NfsIsilonLNN))
														} else {
															logger.Log.Info(fmt.Sprintf("NFS LNN Cache Hit for Isilon NFS host '%s' LNN  null", resultStruct.NfsHost))
														}
													}

													// Done with this host so allow other threads that may want to update it.
													// Other threads likely will get benefit of cache hit.
													//
													// NOTE: This unlock is on normal completion of loop iteration.  If any of the
													// code above skips (continue) a loop iteration then it has to unlock also by
													// itself -- otherwise NFS retrieval will hang indefinitely as threads are trying
													// to lock on mutex that is never unlocked.  Also GoLang mutexes are *not* re-entrant
													// so a thread could lock itself out when it tries to lock the same lock it already
													// owns but never unlocked!
													hostLock.Unlock()

													// Make result available to database updater
													workerResultsChannel <- &resultStruct
												} else {
													logger.Log.Info("Ignoring fstab entry because it does not have the expected number of columns: " + fstabLine)
												}
											} else {
												logger.Log.Info("Ignoring commented out fstab entry: " + fstabLine)
											}
										}
									}
								} else {
									// Host has no NFS mounts.  Add empty results.
									workerResultsChannel <- &ResultStruct{
										HostVcPath:        workItem.VcPath,
										Hostname:          strings.TrimSpace(fqdn),
										HostMount:         "",
										NfsHost:           "",
										NfsPath:           "",
										CapacityBytes:     0,
										CapacityUsedBytes: 0,
										FstabLine:         "",
									}

									logBuffer.logNfsSyncHistoryInfo(ctx, fmt.Sprintf("VM '%s' does not have any NFS mounts", fqdn))
								}
							}
						}

						// Keep stat on hosts this thread processed for log printout later
						hostsProcessed = append(hostsProcessed, fqdn)

						// Increase completion count for host -- value doesn't really matter, just the count
						//logger.Log.Info(fmt.Sprintf("SSH NFS worker thread #%02d pushing completion for host: %s", threadNumber, host))
						completionQueue.Add(fqdn)
					} else {
						// Retry attempts now exhausted for work item -- abandon processing but log it
						time.Sleep(64 * time.Millisecond) // wait just short millis as sometimes this executes within 1ms of previous entries making sorting incorrect or ambigouous.
						var logMsg = fmt.Sprintf("Retries exhausted for host at VCPath %s and no more retries remaining.  Attempts made: %d", workItem.VcPath, NFS_RETRY_LIMIT)
						logger.Log.Info(logMsg)
						logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)

						// Need to also add failed item to completion queue otherwise the reader thread will never be
						// able to finish since it will only stop if completion queue count matches the original work
						// queue count
						completionQueue.Add(fqdn)
					}
				case threadsafequeue.EmptyMarker:
					// Work queue is empty.  The go-routine can exit normally.
					logger.Log.Info(fmt.Sprintf("SSH NFS worker thread #%02d completed retrieval for hosts: %v", threadNumber, hostsProcessed))

					nfsThreadMap.Update(threadNumber, "")

					break outer
				}
			}
		}()
	}

	// Asynchronously process the worker results for the caller by coordinating through the channel
	go func() {
		var mailer = mail.New(config.Config.GetSmtpServer())
		logBuffer.logNfsSyncHistoryInfo(ctx, fmt.Sprintf("Starting NFS data sync using %d threads on %d VMs", actualThreadCount, len(vcPaths)))
		var result *ResultStruct

		var emailSent = false
		var connection = ctx.Value(constants.CTX_KEY_NFS_SYNC_CONNECTION).(*sqlite.Conn)
		var startTime = time.Now()
		var averageSyncTimeSecs = -1.0
		var n = config.Config.GetSyncWarnHistoryRange()
		if lookup, errAvgLookup := dbutil.GetAverageSyncSecs(n, "NfsSyncHistory", connection); errAvgLookup != nil {
			logger.Log.Info(fmt.Sprintf("WARN: Lookup of average durations of %s runs for environment %s failed because of error: %v", n, "TODO", errAvgLookup))
		} else {
			averageSyncTimeSecs = lookup

			var logMsg = fmt.Sprintf("Average sync duration for past %d runs is %f seconds", n, averageSyncTimeSecs)
			logger.Log.Info(logMsg)
			logBuffer.logNfsSyncHistoryInfo(ctx, logMsg)
		}

		for {
			// Get the latest worker result (wait with timeout to break out temporarily to do some checks while waiting)
			select {
				case result = <-workerResultsChannel:
					break  // break out of select (but will return to wait after checks below)
				case <-time.After(10 * time.Second):
					// Print out status information check every few seconds which can also help with debugging parallel
					// execution
					result = nil
					break  // break out of select
			}

			if result != nil {
				// Send result to caller
				outputChannel <- result
			}

			// Completion only when count queue equals the original input list size
			logger.Log.Info(fmt.Sprintf("Checking completion queue size is %d out of %d (%.2f%%)", completionQueue.Size(), len(vcPaths), 100.0 * float64(completionQueue.Size()) / float64(len(vcPaths))))
			logger.Log.Info(fmt.Sprintf("Work queue size is: %d", workQueue.Size()))
			logger.Log.Info(fmt.Sprintf("Thread map: %s", nfsThreadMap.Dump()))

			// Check if runtime has exceeded threshold of past runs 'n' runs which would then trigger an e-mail warning
			if !emailSent && averageSyncTimeSecs > 0 {
				var currentDurationSecs = float64(time.Now().Unix() - startTime.Unix())
				var warningThresholdSecs = config.Config.GetSyncWarnThreshold() * averageSyncTimeSecs
				if currentDurationSecs > warningThresholdSecs {
					// Email that current sync run is taking longer than configured threshold
					var message = fmt.Sprintf("NFS sync for environment %s is taking longer than usual and it could be stalled.  Past runs average duration: %.1f secs, Warn threshold (secs): %.1f, Current run duration (secs): %.1f", ctx.Value(constants.CTX_KEY_VCENTER_ENV_NAME).(string), averageSyncTimeSecs, warningThresholdSecs, currentDurationSecs)
					var errMail = mailer.Send(ctx, config.Config.GetSyncWarnFromAddress(), strings.Split(config.Config.GetSyncWarnRecipients(), ","), mail.CreateMessageBasic("Dora NFS Sync Duration Warning", message))

					if errMail != nil {
						logger.Log.Info(fmt.Sprintf("WARN: NFS sync warning email sending failed to recipients '%s' with error: %v", config.Config.GetSyncWarnRecipients(), errMail))
					} else {
						logger.Log.Info(fmt.Sprintf("NFS sync alert email sent successfully to recipients: %s ", config.Config.GetSyncWarnRecipients()))
					}

					// Log important event to the sync log & console log as well
					logBuffer.logNfsSyncHistoryWarn(ctx, message)
					logger.Log.Info("WARN: " + message)

					// Raise flag to prevent sending multiple time for the same warning event
					emailSent = true
				}
			}

			if completionQueue.Size() == len(vcPaths) {
				// Send nil through channel to indicate results are complete
				outputChannel <- nil

				// Exit go-routine normally by breaking out of infinite loop
				break
			}
		}
	}()

	// No errors during thread setup but this method will continue to run asynchronously from the caller.  The client
	// knows to receive updates from the output channel.
	return outputChannel, nil
}

// Attempts to retrieve the Isilon cluster name from the given host.  This may fail if the host is not an Isilon host!
//
// PARAM:  host      host to connect to
// PARAM:  username  SSH login username (admin, root, etc)
// PARAM:  password  SSH password
// RETURN: cluster name if found (check for errors first)
// ERROR:  (first position) if SSH error occurs
// ERROR:  (second position) if command timeout error occurs
func RetrieveIsilonName(host string, username string, password string) (string, error, error) {
	// NOTE: Isilon servers run on OpenBSD OS and Z-shell (not the usual CentOS and Bash-shell)
	//
	const SSH_COMMAND = `isi stat | grep 'Cluster Name:' | sed -e 's/\s*Cluster Name:\s*//'`
	const SSH_EXEC_TIMEOUT_DURATION = 100 * time.Second // Force timeout if SSH remote exec takes longer than this duration. NOTE: timeout longer than usual because 'isi stat' takes some time to complete.

	// Setup SSH context configuration from config (timeout) and setup SSH access
	var sshExec = utilssh.SshExec{
		TimeoutConnectMs:   SSH_EXEC_TIMEOUT_DURATION, // TODO: should be configurable
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	// Open SSH connection to the host
	if sshClient, errClient := utilssh.OpenSSHClient(sshExec, host, username, password); errClient != nil {
		// Fatal
		return "", errors.New(fmt.Sprintf("Could not connect to host %s because of error: %v",  host, errClient)), nil
	} else {
		// Pre-kill any existing command first before running the command again which helps guard against any potential
		// server problem can cause a slow build up of "stuck" commands on the server over many data sync runs
		if killErr := utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT); killErr != nil {
			// Found existing processes but process kill failed.  Cannot continue with the command for server safety
			// reasons
			return "", nil, errors.New(fmt.Sprintf("Could not kill process '%s' after trying %d times.  Will not proceed with the command on this host for safety reasons.", SSH_COMMAND, KILL_DEFER_LIMIT))
		}

		// Close SSH connection when exiting this function
		defer func() {
			// Kill any remaining processes for this user that are matching the
			// original command line
			utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT)

			sshClient.Close()
		}()

		logger.Log.Info(fmt.Sprintf("Connected to verify presumed Isilon host %s", host))

		//
		// Execute SSH command and obtain results
		//
		result, errSsh, errTimeout := sshExec.RemoteRunWithTimeout(
			sshClient,
			SSH_COMMAND,
			"set -o pipefail",  // Needed so that errors during pipe are shown in stdout
			SSH_EXEC_TIMEOUT_DURATION,
			&utilssh.UNIX_PRIORITY_MIN)

		// Process results (may be errors)
		if errSsh != nil {
			// Fatal - Not an Isilon host
			return "", errors.New(fmt.Sprintf("Not an Isilon host: could not run 'isi' command because of error: %s", errSsh)), nil
		} else if errTimeout != nil {
			// Fatal
			return "", nil, errTimeout
		} else {
			// Successful execution
			return strings.TrimSpace(result), nil, nil
		}
	}
}

// Represents CSV for Isilon network interface record used for de-serializing the retrieved data
//
type IsilonNetworkInterfaceStruct struct {
	LNN          string    `csv:"LNN"`
	Name         string    `csv:"Status"`
	Owners       string    `csv:"Owners"`
	IpAddresses  string    `csv:"IP Addresses"`    // NOTE: the IP addresses are themselves comma-separated and requires separate string split operation to retrieve 0 or more values
}

// Attempts to retrieve the Isilon Logical Node Number (LNN) from the given host.  This may fail if the host is not an
// Isilon host and results can be ignored or used to test if node is Isilon.
//
// PARAM:  host      host to connect to
// PARAM:  username  SSH login username (admin, root, etc)
// PARAM:  password  SSH password
// RETURN: pointer to LNN int if found and nil if not found
// ERROR:  (first position) if SSH error occurs
// ERROR:  (second position) if command timeout error occurs
// ERROR:  (third position) if some processing error (not SSH related) occurs
func RetrieveIsilonLNN(ipAddress net.IP, username string, password string) (*int, error, error, error) {
	// NOTE: Isilon servers run on OpenBSD OS and Z-shell (not the usual CentOS and Bash-shell)
	//
	const SSH_COMMAND = "isi network interfaces list --no-footer --format csv"  // NOTE: the --no-footer option must be enabled otherwise output contains non-parsable lines at bottom
	const SSH_EXEC_TIMEOUT_DURATION = 8 * time.Second // Force timeout if SSH remote exec takes longer than this duration

	// Setup SSH context configuration from config (timeout) and setup SSH access
	var sshExec = utilssh.SshExec{
		TimeoutConnectMs:   SSH_EXEC_TIMEOUT_DURATION, // TODO: should be configurable
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	// Open SSH connection to the host
	if sshClient, errClient := utilssh.OpenSSHClient(sshExec, ipAddress.String(), username, password); errClient != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not connect to host %s because of error: %v", ipAddress.String(), errClient)), nil, nil
	} else {
		// Pre-kill any existing command first before running the command again which helps guard against any potential
		// server problem can cause a slow build up of "stuck" commands on the server over many data sync runs
		if killErr := utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT); killErr != nil {
			// Found existing processes but process kill failed.  Cannot continue with the command for server safety
			// reasons
			return nil, nil, nil, errors.New(fmt.Sprintf("Could not kill process '%s' after trying %d times.  Will not proceed with the command on this host for safety reasons.", SSH_COMMAND, KILL_DEFER_LIMIT))
		}

		// Close SSH connection when exiting this function
		defer func() {
			// Kill any remaining processes for this user that are matching the
			// original command line
			utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT)

			sshClient.Close()
		}()

		logger.Log.Info(fmt.Sprintf("Connected to verify presumed Isilon host at IP: %s", ipAddress.String()))

		var existingPidsMsg = utilssh.CheckOnExistingProcessOnHost(sshExec, sshClient, username, SSH_COMMAND)
		if !util.IsBlank(existingPidsMsg) {
			// ABORT ISILON LNN RETRIEVAL:
			//
			// One or more previous commands are possibly hung from a previous SSH Isilon LNN retrieval.  Do NOT proceed
			// and warn in logs instead to avoid possibly launching multiple instances of the same command on each NFS
			// retrieval.
			var errorMsg = fmt.Sprintf("Unable to retrieve Isilon LNN for host %s because of existing PIDs found for user '%s' that may be hung from previous SSH runs.  See check-process messages: %s", ipAddress.String(), username, existingPidsMsg)
			logger.Log.Info(errorMsg)

			return nil, nil, nil, errors.New(errorMsg);
		} else {
			// CLEAR TO RUN: No previous commands on same user found
			//
			//
			// Execute SSH command and obtain results
			//
			result, errSsh, errTimeout := sshExec.RemoteRunWithTimeout(sshClient, SSH_COMMAND, "", SSH_EXEC_TIMEOUT_DURATION, &utilssh.UNIX_PRIORITY_MIN)

			// Process results (may be errors)
			if errSsh != nil {
				// Fatal - Not an Isilon host
				return nil, errors.New(fmt.Sprintf("Not an Isilon host: could not run 'isi' command because of error: %s", errSsh)), nil, nil
			} else if errTimeout != nil {
				// Fatal
				return nil, nil, errTimeout, nil
			} else {
				// Successful SSH retrieval.  Now search within retrieved CSV data for matching LNN for given IP address.
				//
				if lnn, errParse := findMatchingIsilonLNN(result, ipAddress.String()); errParse != nil {
					// Fatal
					return nil, nil, nil, errors.New(fmt.Sprintf("Could not parse Isilon network interface CSV data because of error: %v", errParse))
				} else {
					// NOTE: the LNN number may be -1 if parsing was successful but no matching IP address was found
					return lnn, nil, nil, nil
				}
			}
		}
	}
}

// Parses the Isilon network interface data (CSV format) for the LNN matching the given IP address string
//
// The matching LNN integer is returned otherwise a nil is returned if no matches was found for the IP address
//
func findMatchingIsilonLNN(csvData string, ipAddressString string) (*int, error) {
	// Will store unmarshalled results into this array
	var isilonNetworkRec []IsilonNetworkInterfaceStruct

	// Unmarshal
	if errUnmarshal := csvutil.Unmarshal([]byte(csvData), &isilonNetworkRec); errUnmarshal != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not unmarshal CSV data because of error: %v", errUnmarshal))
	} else {
		// Iterate through unmarshalled results looking for matching LNN
		for _, rec := range isilonNetworkRec {
			// There are 1 or more IP addresses for each LNN so split and match for each IP address
			var ipAddresses = strings.Split(rec.IpAddresses, ",")
			for _, ipString := range ipAddresses {
				if strings.TrimSpace(ipString) == strings.TrimSpace(ipAddressString) {
					if lnn, errParse := strconv.Atoi(rec.LNN); errParse != nil {
						// Unlikely - but identified LNN that was not a parseable number
						return nil, errors.New(fmt.Sprintf("LNN identified as \"%s\" but could not be parsed to an integer because of error: %v", rec.LNN, errParse))
					} else {
						// Successful parse and match
						return &lnn, nil
					}
				}
			}
		}

		// Successful parse but no matching LNN found for given IP within the Isilon CSV data
		return nil, nil
	}
}


// Attempts to retrieve the Qumulo cluster name from the given host.  This may fail if the host is not a Qumulo host!
//
// PARAM:  host      host to connect to
// PARAM:  username  SSH login username (admin, root, etc)
// PARAM:  password  SSH password
// RETURN: cluster name if found (check for errors first)
// ERROR:  (first position) if SSH error occurs
// ERROR:  (second position) if command timeout error occurs
func RetrieveQumuloName(host string, username string, password string) (string, error, error) {
	// NOTE: Qumulo servers run on Ubuntu OS (not the usual CentOS)
	//
	const SSH_COMMAND = `qq cluster_conf`  // Returns JSON formatted data
	const SSH_EXEC_TIMEOUT_DURATION = 8 * time.Second // Force timeout if SSH remote exec takes longer than this duration

	// Setup SSH context configuration from config (timeout) and setup SSH access
	var sshExec = utilssh.SshExec{
		TimeoutConnectMs:   SSH_EXEC_TIMEOUT_DURATION, // TODO: should be configurable
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	// Open SSH connection to the host
	if sshClient, errClient := utilssh.OpenSSHClient(sshExec, host, username, password); errClient != nil {
		// Fatal
		return "", errors.New(fmt.Sprintf("Could not connect to host %s because of error: %v",  host, errClient)), nil
	} else {
		logger.Log.Info(fmt.Sprintf("Connected to verify presumed Qumulo host %s", host))

		// Pre-kill any existing command first before running the command again which helps guard against any potential
		// server problem can cause a slow build up of "stuck" commands on the server over many data sync runs
		if killErr := utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT); killErr != nil {
			// Found existing processes but process kill failed.  Cannot continue with the command for server safety
			// reasons
			return "", nil, errors.New(fmt.Sprintf("Could not kill process '%s' after trying %d times.  Will not proceed with the command on this host for safety reasons.", SSH_COMMAND, KILL_DEFER_LIMIT))
		}

		// Close SSH connection when exiting this function
		defer func() {
			// Kill any remaining processes for this user that are matching the
			// original command line
			utilssh.KillProcessesOnHostByCommand(sshExec, sshClient, username, SSH_COMMAND, KILL_DEFER_LIMIT)

			sshClient.Close()
		}()

		//
		// Execute SSH command and obtain results
		//
		result, errSsh, errTimeout := sshExec.RemoteRunWithTimeout(sshClient, SSH_COMMAND, "", SSH_EXEC_TIMEOUT_DURATION, &utilssh.UNIX_PRIORITY_MIN)

		// Process results (may be errors)
		if errSsh != nil {
			// Fatal - command not found so this is not a Qumulo host
			return "", errors.New(fmt.Sprintf("Not a Qumulo host: could not run 'qq' command because of error: %s", errSsh)), nil
		} else if errTimeout != nil {
			// Fatal
			return "", nil, errTimeout
		} else {
			// Successful execution
			var jsonData = strings.TrimSpace(result)
			var data = QumuloClusterName{}

			if errJson := json.Unmarshal([]byte(jsonData), &data); errJson != nil {
				// Fatal
				return "", errors.New(fmt.Sprintf("Could not unmarshal Qumulo data '%s' from JSON because of error: %v", jsonData, errJson)), nil
			} else {
				return data.ClusterName, nil, nil
			}
		}
	}
}

// Qumulo returns results in formatted JSON so can deserialize with JSON lib
type QumuloClusterName struct {
	ClusterName   string   `json:"cluster_name"`
}

// A worker thread can call this to get a unique sequence number
var mutex = sync.Mutex{}
var threadSequenceNumber = 0
func getThreadSequenceNumber() int {
	defer mutex.Unlock()
	mutex.Lock()
	var currentCount = threadSequenceNumber
	threadSequenceNumber++

	return currentCount
}


// Extracts the host from a colon-separated fstab host entry
// Ex: 10.170.111.220:/ifs/integration-msp/originals => 10.170.111.220
func extractNfsHost(fstabHost string) string {
	if !util.IsBlank(fstabHost) {
		var split = strings.Split(strings.TrimSpace(fstabHost), ":")

		if len(split) > 1 {
			return strings.TrimSpace(split[0])
		} else {
			return fstabHost
		}
	} else {
		return ""
	}
}

// Extracts the path from a colon-separated fstab host entry
// Ex: 10.170.111.220:/ifs/integration-msp/originals => /ifs/integration-msp/originals
func extractNfsPath(fstabHost string) string {
	if !util.IsBlank(fstabHost) {
		var split = strings.Split(strings.TrimSpace(fstabHost), ":")

		if len(split) > 1 {
			return strings.TrimSpace(split[1])
		} else {
			return fstabHost
		}
	} else {
		return ""
	}
}

// Returns true if given fstab line is commented out and false otherwise
//
func isCommentedOut(fstabLine string) bool {
	if !util.IsBlank(fstabLine) {
		// NOTE: trimming eliminates spaces so if there is a # character then it will be first character
		return strings.HasPrefix(strings.TrimSpace(fstabLine), "#")
	} else {
		// Vacuously false since line is blank
		return false
	}
}

// Contains the results of NFS configuration line parsed from fstab entry
//
// NOTE: If error occured during retrieval then the Error field will be non-blank
// NOTE: Hostname field is always present regardless of failure or not
//
type ResultStruct struct {
	// If this is not blank then the NFS check failed and should be reported -- and *all* regulars fields are invalid
	//and should not used
	Error       string

	// The VM's VCenter path needed for filtering since NFS servers are external to VCenter
	HostVcPath  string

	// Hostname will always be populated regardless of error as the host gives context in either condition
	Hostname    string

	// Regular fields
	HostMount         string
	HostMountUsername string
	HostMountGroupname string
	HostMountPerms    string
	NfsHost           string
	NfsPath           string
	NfsType           string
	NfsClusterName    string
	NfsIsilonLNN      *int64
	CapacityBytes     int64
	CapacityUsedBytes int64
	FstabLine         string
}

//
//
// Simple tuple to contain NFS type values in a data record
//
//
type NfsType struct {
	Type           string
	ClusterName    string
	LNN            int64
	LNNretrieved   bool        // Separate bool flag to determine if LNN has been retrieved because nil value is a legit LNN value
	Mutex          sync.Mutex   // allows locking per host so that only one thread can connect to the same host at a time
}

// A simple cache to store NFS type (Isilon, Qumulo, etc).  This cache is thread safe and designed to work with multiple
// parallel Go routines
//
type NfsTypeCache struct {
	mapCache  map[string]*NfsType
	mutex     sync.Mutex
	mapHostLock map[string]*sync.Mutex
}

// Creates a new cache object
//
func newNfsCache() *NfsTypeCache {
	var nfsTypeCheckCache = NfsTypeCache{}

	nfsTypeCheckCache.mapCache = make(map[string]*NfsType)
	nfsTypeCheckCache.mapHostLock = make(map[string]*sync.Mutex)

	return &nfsTypeCheckCache
}

// Returns the NFS type for the given hostname or nil if hostname is not currently in cache
//
func (o *NfsTypeCache) GetEntry(hostname string) *NfsType {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	// Might return nil if key doesn't exist
	if value, containsKey := o.mapCache[hostname]; containsKey {
		return value
	} else {
		// Lazy create an empty value for this hostname before returning it
		o.mapCache[hostname] = &NfsType{
			NFS_TYPE_UNRETRIEVED,    // Initial NFS type is the special "unretrieved" type which indicates that type has not been looked up yet
			"",
			0,
			false,
			sync.Mutex{},
		}
		return o.mapCache[hostname]
	}
}

// A thread must be the owner of the lock for the host to start processing the host.  This prevents more than one thread
// from processing the same host at the same time.
//
// PARAM: hostname     the identifier for the host usually an FQDN or an IP address string
// RETURN: mutex object for the host.  Owner thread *must* unlock after completing processing of the host!
func (o *NfsTypeCache) lockHost(hostname string) *sync.Mutex {
	// Access to maps must be limited to single thread at a time otherwise Go can panic: "fatal error: concurrent map
	// iteration and map write"
	o.mutex.Lock()
	if _, found := o.mapHostLock[hostname]; !found {
		// Lazy create
		o.mapHostLock[hostname] = &sync.Mutex{}
	}
	o.mutex.Unlock()

	// Lock here if not owner of the lock which will allow the actual owner thread to perform the processing on this
	// host -- after which, the data will be available in the cache.  This ensures that one NFS hostname  is never re-
	// processed more than once which increases efficiency.
	//
	// NOTE: Thread that acquires this lock will exit the function and that thread is responsible for unlocking!
	o.mapHostLock[hostname].Lock()

	return o.mapHostLock[hostname]
}

// Loads up the NFS work items from the given VC paths and sets their retry limit
//
const NFS_RETRY_LIMIT = 2
func createNfsWorkItems(vcPaths ...string) (workItems []NfsWorkItem) {
	if vcPaths != nil {
		for _, vcPath := range vcPaths {
			workItems = append(workItems, NfsWorkItem{vcPath, NFS_RETRY_LIMIT})
		}
	}

	return
}

//
// A simple tuple that represnts an NFS work item.  It encapsulate the VcPath to the VM and its retry remaining count.
//
type NfsWorkItem struct {
	VcPath           string
	RetriesRemaining int
}

//
//
// Simple wrapper around a hash map to keep track of current NFS thread activity
//
//
type NfsThreadMap struct {
	mapThreadMap map[int]string
	mutex sync.Mutex
}

// Create a new instance
//
func NewThreadMap() *NfsThreadMap {
	return &NfsThreadMap{
		mapThreadMap: make(map[int]string),
	}
}

// Update mapping of thread number with the host being processed
//
// PARAM: threadNumber    thread's unique serial number
// PARAM: host     a string representing the host.  Or use "" to remove the thread from the map which is needed when the worker thread completes
func (o *NfsThreadMap) Update(threadNumber int, host string) {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	if util.IsNotBlank(host) {
		// Set or update the key value
		o.mapThreadMap[threadNumber] = host
	} else {
		// Remove the key completely
		delete(o.mapThreadMap, threadNumber)
	}
}

// Returns a string dump of current thread mapping
//
func (o *NfsThreadMap) Dump() string {
	// Access to hashmap objects (even for read) must be limited to one thread at a time otherwise Go can panic with
	// error: "fatal error: concurrent map iteration and map write"
	defer o.mutex.Unlock()
	o.mutex.Lock()

	var lines []string

	for key, value := range o.mapThreadMap {
		lines = append(lines, fmt.Sprintf("%d => %s", key, value))
	}

	return strings.Join(lines, ", ")
}