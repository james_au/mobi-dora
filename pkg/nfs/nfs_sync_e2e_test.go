package nfs

import (
	"context"
	"dora/pkg/addressbook"
	"dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/logger"
	"dora/pkg/testutil"
	"dora/pkg/util"
	"dora/pkg/vcenter"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

// This is the default VCenter if one is not provided in shell environment variable.
// NOTE: This is only the default and environment variable named VCENTER_ENV_LOOKUP's
// value will take precedence
const DEFAULT_VCENTER = "OldPayTV"   // <-- Check environment variable override

// Environment variable lookup.  Set this environment variable to override the default VCenter
const VCENTER_ENV_LOOKUP = "DORA_TEST_VCENTER"

// Input list -- You must modify this for available hosts in your environment and password credentials
//var hosts = []string{"indor01p1.infra.smf1.mobitv", "insre01p1.infra.smf1.mobitv", "judrm02p1.integration-msp.smf1.mobitv"}
var hosts = []string{"JUMPHOST-ramsay", "dbmgt01p1.ci-cp.dev.smf1.mobitv", "birpt01p1.integration-msp.smf1.mobitv", "birpt01p2.integration-msp.smf1.mobitv", "birpt02p2.integration-msp.smf1.mobitv", "dbmgt01p1.integration-msp.smf1.mobitv", "inepd01p1.integration-msp.smf1.mobitv", "inepd02p1.integration-msp.smf1.mobitv", "inepd03p1.integration-msp.smf1.mobitv", "ingan01p2.integration-msp.smf1.mobitv", "ingra01p1.integration-msp.smf1.mobitv", "inicg01p2.integration-msp.smf1.mobitv", "inicg02p1.integration-msp.smf1.mobitv", "inkba01p1.integration-msp.smf1.mobitv", "inldp01p1.integration-msp.smf1.mobitv", "inldp02p1.integration-msp.smf1.mobitv", "inlpd01p1.integration-msp.smf1.mobitv", "inlpd02p1.integration-msp.smf1.mobitv", "inpro01p1.integration-msp.smf1.mobitv", "juamq01p1.integration-msp.smf1.mobitv", "juamq02p1.integration-msp.smf1.mobitv", "juarn01p1.integration-msp.smf1.mobitv", "juarn02p1.integration-msp.smf1.mobitv", "juasp01p1.integration-msp.smf1.mobitv", "juasp02p1.integration-msp.smf1.mobitv", "judai01p1.integration-msp.smf1.mobitv", "judrm01p1.integration-msp.smf1.mobitv", "judrm02p1.integration-msp.smf1.mobitv", "juing01p1.integration-msp.smf1.mobitv", "juing02p1.integration-msp.smf1.mobitv", "nvrjd01p1b.integration-msp.smf1.mobitv"}
//var hosts = []string{"bidtb01p1.func-cp.qa.smf1.mobitv", "bidtb01p1.staging-msp.ops.smf1.mobitv", "bidtb02.ci-cp.dev.smf1.mobitv", "bidtb02p1.staging-msp.ops.smf1.mobitv", "bidtb03p1.prod-msp.smf1.mobitv", "bidtb04p1.prod-msp.smf1.mobitv", "biehd01p1.dev-paytv.smf1.mobitv", "biehd02p1.dev-paytv.smf1.mobitv", "biehd03p1.dev-paytv.smf1.mobitv", "biemo01p1.dev-paytv.smf1.mobitv", "biesm01p1.dev-paytv.smf1.mobitv", "biesm02p1.dev-paytv.smf1.mobitv", "biesm03p1.dev-paytv.smf1.mobitv", "biesm04p1.dev-paytv.smf1.mobitv", "biesm05p1.dev-paytv.smf1.mobitv", "biesm06p1.dev-paytv.smf1.mobitv", "biewd01p1.dev-paytv.smf1.mobitv", "biewd02p1.dev-paytv.smf1.mobitv", "biewd03p1.dev-paytv.smf1.mobitv", "bijpl01p1.dev-paytv.smf1.mobitv", "bikba01p1.dev-paytv.smf1.mobitv", "bikba02p1.dev-paytv.smf1.mobitv", "birpt01p1.ci-cp.dev.smf1.mobitv", "birpt01p1.dev-paytv.smf1.mobitv", "birpt01p1.func-cp.qa.smf1.mobitv", "birpt01p1.integration-msp.smf1.mobitv", "birpt01p1.prod-msp.smf1.mobitv", "birpt01p1.staging-msp.ops.smf1.mobitv", "birpt01p2.integration-msp.smf1.mobitv", "birpt02p1.ci-cp.dev.smf1.mobitv", "birpt02p1.dev-paytv.smf1.mobitv", "birpt02p1.func-cp.qa.smf1.mobitv", "birpt02p1.prod-msp.smf1.mobitv", "birpt02p1.staging-msp.ops.smf1.mobitv", "birpt02p2.integration-msp.smf1.mobitv", "birpt03p1.func-cp.qa.smf1.mobitv", "bisls01p1.dev-paytv.smf1.mobitv", "bisls02p1.dev-paytv.smf1.mobitv", "blackout.qa.smf1.mobitv (leave off)", "build-08.mobitv.corp", "build.mobitv.corp_leave_off", "dbmgt01p1.ci-cp.dev.smf1.mobitv", "dbmgt01p1.dev-paytv.smf1.mobitv", "dbmgt01p1.dev.smf1.mobitv", "dbmgt01p1.func-cp.qa.smf1.mobitv", "dbmgt01p1.integration-msp.smf1.mobitv", "dbmgt01p1.media.dev.smf1.mobitv", "dbmgt01p1.prod-msp.smf1.mobitv", "dbmgt01p1.staging-msp.ops.smf1.mobitv", "dork01p1.dev.smf1.mobitv", "dork01p1.infra.smf1.mobitv", "dork02p1.dev.smf1.mobitv", "dork02p1.infra.smf1.mobitv", "dork03p1.dev.smf1.mobitv", "esximon.infra.smf1.mobitv", "git.ci-cp.dev.smf1.mobitv", "inalm01p1.ci-cp.dev.smf1.mobitv", "inalm01p1.dev-paytv.smf1.mobitv", "incfg01p1.infra.smf1.mobitv", "incol01p1.ci-cp.dev.smf1.mobitv", "incpl01p1.dev-paytv.smf1.mobitv", "indsm01p1.ci-cp.dev.smf1.mobitv", "indsw01p1.ci-cp.dev.smf1.mobitv", "indsw02p1.ci-cp.dev.smf1.mobitv", "inedm01p4.dev-paytv.smf1.mobitv", "inepd01p1.ci-cp.dev.smf1.mobitv", "inepd01p1.dev-paytv.smf1.mobitv", "inepd01p1.dev.smf1.mobitv", "inepd01p1.func-cp.qa.smf1.mobitv", "inepd01p1.integration-msp.smf1.mobitv", "inepd02p1.ci-cp.dev.smf1.mobitv", "inepd02p1.dev-paytv.smf1.mobitv", "inepd02p1.dev.smf1.mobitv", "inepd02p1.func-cp.qa.smf1.mobitv", "inepd02p1.integration-msp.smf1.mobitv", "inepd03p1.ci-cp.dev.smf1.mobitv", "inepd03p1.dev-paytv.smf1.mobitv", "inepd03p1.dev.smf1.mobitv", "inepd03p1.integration-msp.smf1.mobitv", "inesd02p1.ci-cp.dev.smf1.mobitv_2019", "inesd03p1.ci-cp.dev.smf1.mobitv", "inesd03p1.dev-paytv.smf1.mobitv", "inesm01p1.ci-cp.dev.smf1.mobitv", "inesm02p1.ci-cp.dev.smf1.mobitv", "inesm03p1.ci-cp.dev.smf1.mobitv", "infor01p1-generic.qa.smf1.mobitv", "ingan01p1.dev-paytv.smf1.mobitv", "ingan01p1.func-cp.qa.smf1.mobitv", "ingan01p1.prod-msp.smf1.mobitv", "ingan01p2.integration-msp.smf1.mobitv", "ingan01p2.prod-msp.smf1.mobitv", "ingra01p1.ci-cp.dev.smf1.mobitv", "ingra01p1.dev-paytv.smf1.mobitv", "ingra01p1.func-cp.qa.smf1.mobitv", "ingra01p1.integration-msp.smf1.mobitv", "inicg01p1.ci-cp.dev.smf1.mobitv", "inicg01p1.dev-paytv.smf1.mobitv", "inicg01p1.media.dev.smf1.mobitv", "inicg01p2.ci-cp.dev.smf1.mobitv", "inicg01p2.dev.smf1.mobitv", "inicg01p2.func-cp.qa.smf1.mobitv", "inicg01p2.integration-msp.smf1.mobitv", "inicg01p2.prod-msp.smf1.mobitv", "inicg01p2.staging-msp.ops.smf1.mobitv", "inicg02p1.ci-cp.dev.smf1.mobitv", "inicg02p1.integration-msp.smf1.mobitv", "inkba01p1.ci-cp.dev.smf1.mobitv", "inkba01p1.dev-paytv.smf1.mobitv", "inkba01p1.integration-msp.smf1.mobitv", "inldp01p1.ci-cp.dev.smf1.mobitv", "inldp01p1.dev-paytv.smf1.mobitv", "inldp01p1.dev.smf1.mobitv", "inldp01p1.func-cp.qa.smf1.mobitv", "inldp01p1.integration-msp.smf1.mobitv", "inldp01p1.prod-msp.smf1.mobitv", "inldp01p1.staging-msp.ops.smf1.mobitv", "inldp01p2.ci-cp.dev.smf1.mobitv", "inldp01p2.func-cp.qa.smf1.mobitv", "inldp02p1.ci-cp.dev.smf1.mobitv", "inldp02p1.dev-paytv.smf1.mobitv", "inldp02p1.dev.smf1.mobitv", "inldp02p1.func-cp.qa.smf1.mobitv", "inldp02p1.integration-msp.smf1.mobitv", "inldp02p1.prod-msp.smf1.mobitv", "inldp02p1.staging-msp.ops.smf1.mobitv (2)", "inldp02p2.ci-cp.dev.smf1.mobitv", "inldp02p2.func-cp.qa.smf1.mobitv", "inlog01p1.ci-cp.dev.smf1.mobitv", "inlog01p1.dev-paytv.smf1.mobitv", "inlog01p1.func-cp.qa.smf1.mobitv", "inlog02p1.dev-paytv.smf1.mobitv", "inlpd01p1.ci-cp.dev.smf1.mobitv", "inlpd01p1.integration-msp.smf1.mobitv", "inlpd02p1.ci-cp.dev.smf1.mobitv", "inlpd02p1.integration-msp.smf1.mobitv", "innma02p2.ci-cp.dev.smf1.mobitv", "innsv01p1.prod-msp.smf1.mobitv", "innsv01p1.smf1.mobitv_leave_off", "innsv02p1.prod-msp.smf1.mobitv", "innsv02p1.smf1.mobitv_leave_off", "inpdb01p1.prod-msp.smf1.mobitv", "inpdb01p1.staging-msp.ops.smf1.mobitv (2)", "inpro01p1.ci-cp.dev.smf1.mobitv", "inpro01p1.dev-paytv.smf1.mobitv", "inpro01p1.func-cp.qa.smf1.mobitv", "inpro01p1.integration-msp.smf1.mobitv", "inprs01p1.ci-cp.dev.smf1.mobitv", "inpup01p1.staging-msp.ops.smf1.mobitv", "inrdk.prod-msp.smf1.mobitv", "inshk01p1.infra.smf1.mobitv", "inson01p1.infra.smf1.mobitv", "inson01p1.infra.smf1.mobitv_clone_09282018_leaveoff", "insondb01p1.infra.smf1.mobitv", "intes01p1.ci-cp.dev.smf1.mobitv", "inusr01p1.smf1.mobitv", "invma01p1.smf1.mobitv_leave_off", "isilonmon.infra.smf1.mobitv", "jenkins-build01.mobitv.corp 2_leave_off", "jenkins-build01.mobitv.corp_leave_off", "juamq01p1.ci-cp.dev.smf1.mobitv", "juamq01p1.dev-paytv.smf1.mobitv", "juamq01p1.dev.smf1.mobitv", "juamq01p1.func-cp.qa.smf1.mobitv", "juamq01p1.integration-msp.smf1.mobitv", "juamq01p1.media.dev.smf1.mobitv", "juamq01p1.prod-msp.smf1.mobitv", "juamq01p1.staging-msp.ops.smf1.mobitv", "juamq01p3.func-cp.qa.smf1.mobitv-leave_off", "juamq02p1.ci-cp.dev.smf1.mobitv", "juamq02p1.dev-paytv.smf1.mobitv", "juamq02p1.func-cp.qa.smf1.mobitv", "juamq02p1.integration-msp.smf1.mobitv", "juamq02p1.media.dev.smf1.mobitv", "juamq02p1.prod-msp.smf1.mobitv", "juamq02p1.staging-msp.ops.smf1.mobitv", "juarn01p1.ci-cp.dev.smf1.mobitv", "juarn01p1.dev-paytv.smf1.mobitv", "juarn01p1.dev.smf1.mobitv", "juarn01p1.func-cp.qa.smf1.mobitv", "juarn01p1.integration-msp.smf1.mobitv", "juarn01p1.prod-msp.smf1.mobitv", "juarn01p1.staging-msp.ops.smf1.mobitv", "juarn02p1.ci-cp.dev.smf1.mobitv", "juarn02p1.dev-paytv.smf1.mobitv", "juarn02p1.func-cp.qa.smf1.mobitv", "juarn02p1.integration-msp.smf1.mobitv", "juarn02p1.prod-msp.smf1.mobitv", "juarn02p1.staging-msp.ops.smf1.mobitv", "juasp01p1.ci-cp.dev.smf1.mobitv", "juasp01p1.dev.smf1.mobitv", "juasp01p1.func-cp.qa.smf1.mobitv2019", "juasp01p1.integration-msp.smf1.mobitv", "juasp02p1.func-cp.qa.smf1.mobitv2019", "juasp02p1.integration-msp.smf1.mobitv", "judai01p1.ci-cp.dev.smf1.mobitv", "judai01p1.func-cp.qa.smf1.mobitv", "judai01p1.integration-msp.smf1.mobitv", "judrm01p1.ci-cp.dev.smf1.mobitv", "judrm01p1.dev-paytv.smf1.mobitv", "judrm01p1.dev.smf1.mobitv", "judrm01p1.func-cp.qa.smf1.mobitv", "judrm01p1.integration-msp.smf1.mobitv", "judrm01p1.media.dev.smf1.mobitv", "judrm01p1.prod-msp.smf1.mobitv", "judrm01p1.staging-msp.ops.smf1.mobitv", "judrm02p1.ci-cp.dev.smf1.mobitv", "judrm02p1.dev-paytv.smf1.mobitv", "judrm02p1.func-cp.qa.smf1.mobitv", "judrm02p1.integration-msp.smf1.mobitv", "judrm02p1.prod-msp.smf1.mobitv", "judrm02p1.staging-msp.ops.smf1.mobitv", "juing01p1.ci-cp.dev.smf1.mobitv", "juing01p1.dev-paytv.smf1.mobitv", "juing01p1.dev.smf1.mobitv", "juing01p1.func-cp.qa.smf1.mobitv", "juing01p1.integration-msp.smf1.mobitv", "juing01p1.prod-msp.smf1.mobitv", "juing01p1.staging-msp.ops.smf1.mobitv", "juing02p1.dev-paytv.smf1.mobitv", "juing02p1.func-cp.qa.smf1.mobitv", "juing02p1.integration-msp.smf1.mobitv", "juing02p1.prod-msp.smf1.mobitv", "juing02p1.staging-msp.ops.smf1.mobitv", "jupkg01p1.ci-cp.dev.smf1.mobitv", "jupkg01p1.dev-paytv.smf1.mobitv", "jupkg01p1.dev.smf1.mobitv", "jupkg01p1.func-cp.qa.smf1.mobitv", "jupkg01p1.integration-msp.smf1.mobitv", "jupkg01p1.prod-msp.smf1.mobitv", "jupkg01p1.staging-msp.ops.smf1.mobitv", "jupkg02p1.ci-cp.dev.smf1.mobitv", "jupkg02p1.dev-paytv.smf1.mobitv", "jupkg02p1.func-cp.qa.smf1.mobitv", "jupkg02p1.integration-msp.smf1.mobitv", "jupkg02p1.staging-msp.ops.smf1.mobitv", "jupkg03p1.prod-msp.smf1.mobitv", "jupkg04p1.prod-msp.smf1.mobitv", "juplm01p1.ci-cp.dev.smf1.mobitv", "juplm01p1.dev.smf1.mobitv", "juplm01p1.func-cp.qa.smf1.mobitv", "juplm01p1.integration-msp.smf1.mobitv", "juplm02p1.ci-cp.dev.smf1.mobitv", "juplm02p1.func-cp.qa.smf1.mobitv", "juplm02p1.integration-msp.smf1.mobitv", "ltpsr01p1.qa.smf1.mobitv", "ltpsr02p1.qa.smf1.mobitv", "ltpsr03p1.qa.smf1.mobitv", "mcmon.infra.smf1.mobitv", "mediabuild.mobitv.corp_leave_off", "msacc01p1.dev.smf1.mobitv", "msacc01p1.func-cp.qa.smf1.mobitv", "msacc01p1.integration-msp.smf1.mobitv", "msacc02p1.func-cp.qa.smf1.mobitv", "msacc02p1.integration-msp.smf1.mobitv", "msamq01p1.integration-msp.smf1.mobitv", "msamq01p1.prod-msp.smf1.mobitv", "msamq01p1.staging-msp.ops.smf1.mobitv", "msamq02p1.integration-msp.smf1.mobitv", "msamq02p1.prod-msp.smf1.mobitv", "msamq02p1.staging-msp.ops.smf1.mobitv", "msapi01p1.dev.smf1.mobitv", "msapi01p1.func-cp.qa.smf1.mobitv", "msapi01p1.integration-msp.smf1.mobitv", "msapi01p3.ci-cp.dev.smf1.mobitv", "msapi02p1.func-cp.qa.smf1.mobitv", "msapi02p1.integration-msp.smf1.mobitv", "msath01p1.ci-cp.dev.smf1.mobitv", "msath01p1.dev-paytv.smf1.mobitv", "msath01p1.dev.smf1.mobitv", "msath01p1.func-cp.qa.smf1.mobitv", "msath01p1.integration-msp.smf1.mobitv", "msath01p1.prod-msp.smf1.mobitv", "msath01p1.staging-msp.ops.smf1.mobitv", "msath02p1.ci-cp.dev.smf1.mobitv", "msath02p1.dev-paytv.smf1.mobitv", "msath02p1.func-cp.qa.smf1.mobitv", "msath02p1.integration-msp.smf1.mobitv", "msath02p1.prod-msp.smf1.mobitv", "msath02p1.staging-msp.ops.smf1.mobitv", "msath03p1.dev-paytv.smf1.mobitv", "msbko01p1.dev-paytv.smf1.mobitv", "msbko01p1.dev.smf1.mobitv", "msbko01p1.func-cp.qa.smf1.mobitv", "msbko01p1.integration-msp.smf1.mobitv", "msbko02p1.func-cp.qa.smf1.mobitv", "msbko02p1.integration-msp.smf1.mobitv", "msdai01p1.ci-cp.dev.smf1.mobitv", "msdai01p1.func-cp.qa.smf1.mobitv", "msdai01p1.integration-msp.smf1.mobitv", "mseac01p1.ci-cp.dev.smf1.mobitv", "mseac01p1.dev-paytv.smf1.mobitv", "mseac01p1.dev.smf1.mobitv", "mseac01p1.func-cp.qa.smf1.mobitv", "mseac01p1.integration-msp.smf1.mobitv", "mseac02p1.ci-cp.dev.smf1.mobitv", "mseac02p1.dev-paytv.smf1.mobitv", "mseac02p1.func-cp.qa.smf1.mobitv", "mseac02p1.integration-msp.smf1.mobitv", "mseag01p1.dev-paytv.smf1.mobitv", "mseag01p1.dev.smf1.mobitv", "mseag01p1.func-cp.qa.smf1.mobitv", "mseag01p1.integration-msp.smf1.mobitv", "mseag02p1.dev-paytv.smf1.mobitv", "mseag02p1.func-cp.qa.smf1.mobitv", "mseag02p1.integration-msp.smf1.mobitv", "msepg01p1.ci-cp.dev.smf1.mobitv", "msepg01p1.dev-paytv.smf1.mobitv", "msepg01p1.dev.smf1.mobitv", "msepg01p1.func-cp.qa.smf1.mobitv", "msepg01p1.integration-msp.smf1.mobitv", "msepg01p1.prod-msp.smf1.mobitv", "msepg01p1.staging-msp.ops.smf1.mobitv", "msepg02p1.ci-cp.dev.smf1.mobitv", "msepg02p1.dev-paytv.smf1.mobitv", "msepg02p1.func-cp.qa.smf1.mobitv", "msepg02p1.integration-msp.smf1.mobitv", "msepg02p1.prod-msp.smf1.mobitv", "msepg02p1.staging-msp.ops.smf1.mobitv", "msepg03p1.dev-paytv.smf1.mobitv", "msepg03p1.prod-msp.smf1.mobitv", "msepg04p1.dev-paytv.smf1.mobitv", "msepg04p1.prod-msp.smf1.mobitv", "msfrn01p1.ci-cp.dev.smf1.mobitv", "msfrn01p1.dev-paytv.smf1.mobitv", "msfrn01p1.dev.smf1.mobitv", "msfrn01p1.func-cp.qa.smf1.mobitv", "msfrn01p1.integration-msp.smf1.mobitv", "msfrn01p1.prod-msp.smf1.mobitv", "msfrn01p1.staging-msp.ops.smf1.mobitv", "msfrn02p1.ci-cp.dev.smf1.mobitv", "msfrn02p1.dev-paytv.smf1.mobitv", "msfrn02p1.func-cp.qa.smf1.mobitv", "msfrn02p1.integration-msp.smf1.mobitv", "msfrn02p1.prod-msp.smf1.mobitv", "msfrn02p1.staging-msp.ops.smf1.mobitv", "msfrn03p1.ci-cp.dev.smf1.mobitv", "msfrn03p1.dev-paytv.smf1.mobitv", "msfrn04p1.dev-paytv.smf1.mobitv", "msliv01p1.ci-cp.dev.smf1.mobitv", "msliv01p1.dev-paytv.smf1.mobitv", "msliv01p1.dev.smf1.mobitv", "msliv01p1.func-cp.qa.smf1.mobitv", "msliv01p1.integration-msp.smf1.mobitv", "msliv01p1.prod-msp.smf1.mobitv", "msliv01p1.staging-msp.ops.smf1.mobitv", "msliv02p1.ci-cp.dev.smf1.mobitv", "msliv02p1.dev-paytv.smf1.mobitv", "msliv02p1.func-cp.qa.smf1.mobitv", "msliv02p1.integration-msp.smf1.mobitv", "msliv02p1.prod-msp.smf1.mobitv", "msliv02p1.staging-msp.ops.smf1.mobitv", "msnom01p1.integration-msp.smf1.mobitv", "msnom01p1.prod-msp.smf1.mobitv", "msnom01p1.staging-msp.ops.smf1.mobitv", "msnom02p1.integration-msp.smf1.mobitv", "msnom02p1.staging-msp.ops.smf1.mobitv", "msofm01p1.ci-cp.dev.smf1.mobitv", "msofm01p1.dev-paytv.smf1.mobitv", "msofm01p1.dev.smf1.mobitv", "msofm01p1.func-cp.qa.smf1.mobitv", "msofm01p1.integration-msp.smf1.mobitv", "msofm01p1.staging-msp.ops.smf1.mobitv", "msofm02p1.ci-cp.dev.smf1.mobitv", "msofm02p1.dev-paytv.smf1.mobitv", "msofm02p1.func-cp.qa.smf1.mobitv", "msofm02p1.integration-msp.smf1.mobitv", "msofm02p1.prod-msp.smf1.mobitv", "msofm02p1.staging-msp.ops.smf1.mobitv", "msotv01p1.infra.smf1.mobitv", "msprf01p1.ci-cp.dev.smf1.mobitv", "msprf01p1.dev-paytv.smf1.mobitv", "msprf01p1.dev.smf1.mobitv", "msprf01p1.func-cp.qa.smf1.mobitv", "msprf01p1.integration-msp.smf1.mobitv", "msprf01p1.prod-msp.smf1.mobitv", "msprf01p1.staging-msp.ops.smf1.mobitv", "msprf02p1.ci-cp.dev.smf1.mobitv", "msprf02p1.dev-paytv.smf1.mobitv", "msprf02p1.func-cp.qa.smf1.mobitv", "msprf02p1.integration-msp.smf1.mobitv", "msprf02p1.prod-msp.smf1.mobitv", "msprf02p1.staging-msp.ops.smf1.mobitv", "mspxy01p1.integration-msp.smf1.mobitv", "mspxy02p1.integration-msp.smf1.mobitv", "msqov01p1.prod-msp.smf1.mobitv", "msqov01p1.staging-msp.ops.smf1.mobitv", "msqov02p1.prod-msp.smf1.mobitv", "mssch01p1.ci-cp.dev.smf1.mobitv", "mssch01p1.dev-paytv.smf1.mobitv", "mssch01p1.dev.smf1.mobitv", "mssch01p1.func-cp.qa.smf1.mobitv", "mssch01p1.integration-msp.smf1.mobitv", "mssch01p1.prod-msp.smf1.mobitv", "mssch01p1.staging-msp.ops.smf1.mobitv", "mssch02p1.ci-cp.dev.smf1.mobitv", "mssch02p1.dev-paytv.smf1.mobitv", "mssch02p1.func-cp.qa.smf1.mobitv", "mssch02p1.integration-msp.smf1.mobitv", "mssch02p1.prod-msp.smf1.mobitv", "mssch02p1.staging-msp.ops.smf1.mobitv", "mssub01p1.ci-cp.dev.smf1.mobitv", "mssub01p1.dev-paytv.smf1.mobitv", "mssub01p1.dev.smf1.mobitv", "mssub01p1.func-cp.qa.smf1.mobitv", "mssub01p1.integration-msp.smf1.mobitv", "mssub01p1.prod-msp.smf1.mobitv", "mssub01p1.staging-msp.ops.smf1.mobitv", "mssub02p1.ci-cp.dev.smf1.mobitv", "mssub02p1.dev-paytv.smf1.mobitv", "mssub02p1.func-cp.qa.smf1.mobitv", "mssub02p1.integration-msp.smf1.mobitv", "mssub02p1.prod-msp.smf1.mobitv", "mssub02p1.staging-msp.ops.smf1.mobitv", "mssub03p1.prod-msp.smf1.mobitv", "mssub04p1.prod-msp.smf1.mobitv", "mszoo01p1.ci-cp.dev.smf1.mobitv", "mszoo01p1.dev-paytv.smf1.mobitv", "mszoo01p1.dev.smf1.mobitv", "mszoo01p1.func-cp.qa.smf1.mobitv", "mszoo01p1.integration-msp.smf1.mobitv", "mszoo01p1.prod-msp.smf1.mobitv", "mszoo01p1.staging-msp.ops.smf1.mobitv", "mszoo02p1.ci-cp.dev.smf1.mobitv", "mszoo02p1.dev-paytv.smf1.mobitv", "mszoo02p1.dev.smf1.mobitv", "mszoo02p1.func-cp.qa.smf1.mobitv", "mszoo02p1.integration-msp.smf1.mobitv", "mszoo02p1.prod-msp.smf1.mobitv", "mszoo02p1.staging-msp.ops.smf1.mobitv", "mszoo03p1.ci-cp.dev.smf1.mobitv", "mszoo03p1.dev-paytv.smf1.mobitv", "mszoo03p1.dev.smf1.mobitv", "mszoo03p1.func-cp.qa.smf1.mobitv", "mszoo03p1.integration-msp.smf1.mobitv", "mszoo03p1.prod-msp.smf1.mobitv", "mszoo03p1.staging-msp.ops.smf1.mobitv", "mysql01p1.ci-cp.dev.smf1.mobitv(leave off)", "mysqlmon.infra.smf1.mobitv", "namon03p1.infra.smf1.mobitv", "namon03p1.infra.smf1.mobitv-DND and DNP", "nbadmin.smf1.mobitv", "nbe03a.smf1.mobitv", "nocas01p1.ci-cp.dev.smf1.mobitv", "nocas01p1.dev-paytv.smf1.mobitv", "nocas01p1.dev.smf1.mobitv", "nocas01p1.func-cp.qa.smf1.mobitv", "nocas01p1.integration-msp.smf1.mobitv", "nocas01p1.prod-msp.smf1.mobitv", "nocas01p1.staging-msp.ops.smf1.mobitv", "nocas01p2.ci-cp.dev.smf1.mobitv", "nocas02p1.ci-cp.dev.smf1.mobitv", "nocas02p1.dev-paytv.smf1.mobitv", "nocas02p1.dev.smf1.mobitv", "nocas02p1.func-cp.qa.smf1.mobitv", "nocas02p1.integration-msp.smf1.mobitv", "nocas02p1.prod-msp.smf1.mobitv", "nocas02p1.staging-msp.ops.smf1.mobitv", "nocas02p2.ci-cp.dev.smf1.mobitv", "nocas03p1.ci-cp.dev.smf1.mobitv", "nocas03p1.dev-paytv.smf1.mobitv", "nocas03p1.dev.smf1.mobitv", "nocas03p1.func-cp.qa.smf1.mobitv", "nocas03p1.integration-msp.smf1.mobitv", "nocas03p1.prod-msp.smf1.mobitv", "nocas03p1.staging-msp.ops.smf1.mobitv", "nocon01p1.ci-cp.dev.smf1.mobitv", "nocon01p1.dev-paytv.smf1.mobitv", "nocon01p1.dev.smf1.mobitv", "nocon01p1.func-cp.qa.smf1.mobitv-2019", "nocon01p1.integration-msp.smf1.mobitv", "nocon02p1.ci-cp.dev.smf1.mobitv", "nocon02p1.dev-paytv.smf1.mobitv", "nocon02p1.dev.smf1.mobitv", "nocon02p1.func-cp.qa.smf1.mobitv2019", "nocon02p1.integration-msp.smf1.mobitv", "nocon03p1.ci-cp.dev.smf1.mobitv", "nocon03p1.dev-paytv.smf1.mobitv", "nocon03p1.dev.smf1.mobitv", "nocon03p1.func-cp.qa.smf1.mobitv2019", "nocon03p1.integration-msp.smf1.mobitv", "nodse01p1.ci-cp.dev.smf1.mobitv", "nodse02p1.ci-cp.dev.smf1.mobitv", "nodse03p1.ci-cp.dev.smf1.mobitv", "nogit01p1.ci-cp.dev.smf1.mobitv", "nogit01p1.dev-paytv.smf1.mobitv", "nogit01p1.dev.smf1.mobitv", "nogit01p1.func-cp.qa.smf1.mobitv", "nogit01p1.integration-msp.smf1.mobitv", "nolsr02p2.ci-cp.dev.smf1.mobitv", "nomdb01p1.infra.smf1.mobitv", "noops01p1.ci-cp.dev.smf1.mobitv", "noops01p1.dev.smf1.mobitv", "noops01p1.func-cp.qa.smf1.mobitv", "noops01p1.integration-msp.smf1.mobitv", "nords01p1.ci-cp.dev.smf1.mobitv", "nords01p1.func-cp.qa.smf1.mobitv", "nords01p1.integration-msp.smf1.mobitv", "nords01p1.media.dev.smf1.mobitv", "nords02p1.func-cp.qa.smf1.mobitv", "nords02p1.integration-msp.smf1.mobitv", "noslr01p1.ci-cp.dev.smf1.mobitv", "noslr01p1.dev.smf1.mobitv", "noslr01p1.staging-msp.ops.smf1.mobitv", "noslr01p2.ci-cp.dev.smf1.mobitv", "noslr01p2.dev-paytv.smf1.mobitv", "noslr01p2.func-cp.qa.smf1.mobitv", "noslr01p2.integration-msp.smf1.mobitv", "noslr01p3.func-cp.qa.smf1.mobitv", "noslr02p1.ci-cp.dev.smf1.mobitv", "noslr02p1.dev.smf1.mobitv", "noslr02p1.func-cp.qa.smf1.mobitv", "noslr02p1.integration-msp.smf1.mobitv", "noslr02p1.prod-msp.smf1.mobitv", "noslr02p1.staging-msp.ops.smf1.mobitv", "noslr02p2.dev-paytv.smf1.mobitv", "noslr02p2.func-cp.qa.smf1.mobitv", "noslr02p2.integration-msp.smf1.mobitv", "noslr03p1.ci-cp.dev.smf1.mobitv", "noslr03p1.dev.smf1.mobitv", "noslr03p1.prod-msp.smf1.mobitv", "noslr03p1.staging-msp.ops.smf1.mobitv", "noslr03p2.ci-cp.dev.smf1.mobitv", "noslr03p2.dev-paytv.smf1.mobitv", "noslr03p2.func-cp.qa.smf1.mobitv", "noslr03p2.integration-msp.smf1.mobitv", "noslr03p3.func-cp.qa.smf1.mobitv", "noslr04p1.func-cp.qa.smf1.mobitv_leave_off", "noslr04p1.prod-msp.smf1.mobitv", "noslr05p1.prod-msp.smf1.mobitv", "noslr05p1.staging-msp.ops.smf1.mobitv", "nozoo01p1.dev.smf1.mobitv", "nozoo01p1.func-cp.qa.smf1.mobitv", "nozoo01p1.integration-msp.smf1.mobitv", "nozoo01p3.ci-cp.dev.smf1.mobitv", "nozoo02p1.func-cp.qa.smf1.mobitv", "nozoo02p1.integration-msp.smf1.mobitv", "nvmxv01p1.staging-msp.ops.smf1.mobitv", "nvmxv02p1.staging-msp.ops.smf1.mobitv", "nvncw01p1.integration-msp.smf1.mobitv", "nvncw02p1.integration-msp.smf1.mobitv", "nvnrw01p1.media.dev.smf1.mobitv", "nvrcw01p1.ci-cp.dev.smf1.mobitv", "nvrcw01p1.func-cp.qa.smf1.mobitv", "nvrcw01p1.integration-msp.smf1.mobitv", "nvrcw01p1.media.dev.smf1.mobitv", "nvrcw01p1.staging-msp.ops.smf1.mobitv", "nvrcw01p1a.dev-paytv.smf1.mobitv", "nvrcw01p1b.dev-paytv.smf1.mobitv", "nvrcw01p1b.func-cp.qa.smf1.mobitv", "nvrcw01p1b.integration-msp.smf1.mobitv", "nvrcw01p2b.func-cp.qa.smf1.mobitv", "nvrcw01p2b.integration-msp.smf1.mobitv", "nvrcw02p1.ci-cp.dev.smf1.mobitv", "nvrcw02p1.func-cp.qa.smf1.mobitv", "nvrcw02p1.media.dev.smf1.mobitv", "nvrcw02p1.staging-msp.ops.smf1.mobitv", "nvrcw02p1a.dev-paytv.smf1.mobitv", "nvrcw02p1b.dev-paytv.smf1.mobitv", "nvrcw02p1b.func-cp.qa.smf1.mobitv", "nvrcw02p1b.integration-msp.smf1.mobitv", "nvrcw02p2b.func-cp.qa.smf1.mobitv", "nvrcw02p2b.integration-msp.smf1.mobitv", "nvrcw03p1.func-cp.qa.smf1.mobitv", "nvrcw03p1.media.dev.smf1.mobitv", "nvrcw03p1a.dev-paytv.smf1.mobitv", "nvrcw03p1b.dev-paytv.smf1.mobitv", "nvrcw03p1b.integration-msp.smf1.mobitv", "nvrcw04p1.func-cp.qa.smf1.mobitv", "nvrcw04p1.media.dev.smf1.mobitv", "nvrcw04p1b.integration-msp.smf1.mobitv", "nvrcw05p1.media.dev.smf1.mobitv", "nvrcw06p1.media.dev.smf1.mobitv", "nvrjd01p1.ci-cp.dev.smf1.mobitv", "nvrjd01p1.func-cp.qa.smf1.mobitv", "nvrjd01p1.integration-msp.smf1.mobitv", "nvrjd01p1.staging-msp.ops.smf1.mobitv", "nvrjd01p1a.dev-paytv.smf1.mobitv", "nvrjd01p1b.dev-paytv.smf1.mobitv", "nvrjd01p1b.func-cp.qa.smf1.mobitv", "nvrjd01p1b.integration-msp.smf1.mobitv", "nvrjd01p1b.media.dev.smf1.mobitv", "nvrjd01p2b.func-cp.qa.smf1.mobitv", "nvrjd01p2b.integration-msp.smf1.mobitv", "nvrjd02p1.ci-cp.dev.smf1.mobitv", "nvrjd02p1.func-cp.qa.smf1.mobitv", "nvrjd02p1.integration-msp.smf1.mobitv", "nvrjd02p1.staging-msp.ops.smf1.mobitv", "nvrjd02p1a.dev-paytv.smf1.mobitv", "nvrjd02p1a.media.dev.smf1.mobitv", "nvrjd02p1b.dev-paytv.smf1.mobitv", "nvrjd02p1b.func-cp.qa.smf1.mobitv", "nvrjd02p1b.integration-msp.smf1.mobitv", "nvrjd02p1b.media.dev.smf1.mobitv", "nvrjd02p2b.func-cp.qa.smf1.mobitv", "nvrjd02p2b.integration-msp.smf1.mobitv", "nvrjd03p2a.func-cp.qa.smf1.mobitv", "nvrjd04p2a.func-cp.qa.smf1.mobitv", "nvrmg01p1b.ci-cp.dev.smf1.mobitv", "nvrmg01p1b.func-cp.qa.smf1.mobitv", "nvrmg02p1b.func-cp.qa.smf1.mobitv", "pebbles01.mobitv.corp_leave_off", "pebbles02.mobitv.corp_leave_off", "pebbles03.mobitv.corp_leave_off", "pmmcluster1.func-cp.qa.smf1.mobitv", "pmmcluster2.func-cp.qa.smf1.mobitv", "pmmcluster3.func-cp.qa.smf1.mobitv", "pmmcluster5.func-cp.qa.smf1.mobitv", "pmmcluster6.func-cp.qa.smf1.mobitv", "proxysql.func-cp.qa.smf1.mobitv", "psr-aaa-brick-01.qa.smf1.mobitv", "psr-aaa-brick-02.qa.smf1.mobitv", "psr-ms-frn-01.qa.smf1.mobitv", "psr-ms-prf-01.qa.smf1.mobitv", "psr-nfl-brick-01.qa.smf1.mobitv", "psr-nfl-brick-02.qa.smf1.mobitv", "psr-server-brick-01.qa.smf1.mobitv", "psr-server-brick-02.qa.smf1.mobitv", "psr-server-brick-03.qa.smf1.mobitv", "psr-tool-brick-01.qa.smf1.mobitv", "psr-tool-brick-04.qa.smf1.mobitv", "psr-tool-brick-05.qa.smf1.mobitv", "pxmon1.func-cp.qa.smf1.mobitv", "qaid.mobitv.corp_leave_off", "retst01p1.dev-paytv.smf1.mobitv", "rsa1.smf1.mobitv", "rsa2.smf1.mobitv", "sqdtb01p1.ci-cp.dev.smf1.mobitv", "sqdtb01p1.dev-paytv.smf1.mobitv", "sqdtb01p1.dev.smf1.mobitv", "sqdtb01p1.func-cp.qa.smf1.mobitv", "sqdtb01p1.integration-msp.smf1.mobitv", "sqdtb01p1.media.dev.smf1.mobitv", "sqdtb01p1.prod-msp.smf1.mobitv", "sqdtb01p1.staging-msp.ops.smf1.mobitv", "sqdtb01p2.ci-cp.dev.smf1.mobitv", "sqdtb01p2.dev-paytv.smf1.mobitv", "sqdtb01p2.dev.smf1.mobitv", "sqdtb01p2.func-cp.qa.smf1.mobitv", "sqdtb01p2.integration-msp.smf1.mobitv", "sqdtb01p2.media.dev.smf1.mobitv", "sqdtb01p2.prod-msp.smf1.mobitv", "sqdtb01p2.staging-msp.ops.smf1.mobitv", "sqdtb01p3.dev.smf1.mobitv", "sqdtb01p5.ci-cp.dev.smf1.mobitv", "sqdtb01t2.ci-cp.dev.smf1.mobitv", "sqdtb02p1.ci-cp.dev.smf1.mobitv", "sqdtb02p1.dev-paytv.smf1.mobitv", "sqdtb02p1.dev.smf1.mobitv", "sqdtb02p1.func-cp.qa.smf1.mobitv", "sqdtb02p1.integration-msp.smf1.mobitv", "sqdtb02p1.media.dev.smf1.mobitv", "sqdtb02p1.prod-msp.smf1.mobitv", "sqdtb02p1.staging-msp.ops.smf1.mobitv", "sqdtb02p2.ci-cp.dev.smf1.mobitv", "sqdtb02p2.dev-paytv.smf1.mobitv", "sqdtb02p2.dev.smf1.mobitv", "sqdtb02p2.integration-msp.smf1.mobitv", "sqdtb02p2.prod-msp.smf1.mobitv", "sqdtb02p2.staging-msp.ops.smf1.mobitv", "sqdtb02p3.dev.smf1.mobitv", "sqdtb02t2.ci-cp.dev.smf1.mobitv", "sqdtb03p1.prod-msp.smf1.mobitv", "sqdtb03t2.ci-cp.dev.smf1.mobitv", "sqdtb04p1.prod-msp.smf1.mobitv", "sqdtbmon.ci-cp.dev.smf1.mobitv", "srekb01p1.ci-cp.dev.smf1.mobitv", "stdps01p1.ci-cp.dev.smf1.mobitv", "stdps01p1.dev-paytv.smf1.mobitv", "stdps01p1.func-cp.qa.smf1.mobitv", "stdps01p1.media.dev.smf1.mobitv", "stdps01p1.prod-msp.smf1.mobitv", "stdps01p1.staging-msp.ops.smf1.mobitv", "stdps01p2.ci-cp.dev.smf1.mobitv", "stdps01p2.func-cp.qa.smf1.mobitv", "stdps01p2.integration-msp.smf1.mobitv", "stdps01p2.prod-msp.smf1.mobitv", "stdps01p2.staging-msp.ops.smf1.mobitv", "stdps02p1.dev-paytv.smf1.mobitv", "stdps02p1.prod-msp.smf1.mobitv", "stdps02p1.staging-msp.ops.smf1.mobitv", "stdps02p2.integration-msp.smf1.mobitv", "stdps02p2.prod-msp.smf1.mobitv", "stdps02p2.staging-msp.ops.smf1.mobitv", "sthpy01p1.ci-cp.dev.smf1.mobitv", "sthpy01p1.func-cp.qa.smf1.mobitv", "sthpy01p1.integration-msp.smf1.mobitv", "sthpy01p2.func-cp.qa.smf1.mobitv", "sthpy02p1.ci-cp.dev.smf1.mobitv", "sthpy02p1.integration-msp.smf1.mobitv", "sthpy02p2.func-cp.qa.smf1.mobitv", "sthpy02p2.integration-msp.smf1.mobitv", "stmmx01p1.ci-cp.dev.smf1.mobitv", "stmmx01p1.dev-paytv.smf1.mobitv", "stmmx01p1.func-cp.qa.smf1.mobitv", "stmmx01p1.integration-msp.smf1.mobitv", "stmmx01p1.media.dev.smf1.mobitv", "stmmx01p12.integration-msp.smf1.mobitv", "stmmx01p13.integration-msp.smf1.mobitv", "stmmx01p2.func-cp.qa.smf1.mobitv", "stmmx01p2.integration-msp.smf1.mobitv", "stmmx02p1.ci-cp.dev.smf1.mobitv", "stmmx02p1.dev-paytv.smf1.mobitv", "stmmx02p1.func-cp.qa.smf1.mobitv", "stmmx02p1.integration-msp.smf1.mobitv", "stmmx02p12.integration-msp.smf1.mobitv", "stmmx02p13.integration-msp.smf1.mobitv", "stmmx02p2.func-cp.qa.smf1.mobitv", "stmmx02p2.integration-msp.smf1.mobitv", "stmmx03p1.func-cp.qa.smf1.mobitv", "stmmx03p1.media.dev.smf1.mobitv", "stmmx03p2.integration-msp.smf1.mobitv", "stmmx04p1.func-cp.qa.smf1.mobitv", "stmmx04p2.integration-msp.smf1.mobitv", "stmvl01p1.prod-msp.smf1.mobitv", "stmvl01p1.staging-msp.ops.smf1.mobitv", "stmvl02p1.prod-msp.smf1.mobitv", "stmvl02p1.staging-msp.ops.smf1.mobitv", "stmxv01p1.prod-msp.smf1.mobitv", "stmxv01p1.staging-msp.ops.smf1.mobitv", "stmxv01p2.prod-msp.smf1.mobitv", "stmxv02p1.prod-msp.smf1.mobitv", "stmxv02p1.staging-msp.ops.smf1.mobitv", "stmxv02p2.prod-msp.smf1.mobitv", "stmxv03p1.prod-msp.smf1.mobitv", "stsgl01p1.ci-cp.dev.smf1.mobitv", "stsgl01p1.dev-paytv.smf1.mobitv", "stsgl01p1.func-cp.qa.smf1.mobitv", "stsgl01p1.integration-msp.smf1.mobitv", "stsgl01p1.media.dev.smf1.mobitv", "stsgl01p1.prod-msp.smf1.mobitv", "stsgl01p1.staging-msp.ops.smf1.mobitv", "stsgl01p12.integration-msp.smf1.mobitv", "stsgl01p13.integration-msp.smf1.mobitv", "stsgl01p2.dev-paytv.smf1.mobitv", "stsgl01p2.integration-msp.smf1.mobitv", "stsgl01p2.media.dev.smf1.mobitv", "stsgl01p2.prod-msp.smf1.mobitv", "stsgl01p2.staging-msp.ops.smf1.mobitv", "stsgl01p3.dev-paytv.smf1.mobitv", "stsgl01p3.integration-msp.smf1.mobitv", "stsgl01p4.dev-paytv.smf1.mobitv", "stsgl01p4.integration-msp.smf1.mobitv", "stsgl01p5.dev-paytv.smf1.mobitv", "stsgl01p5.integration-msp.smf1.mobitv", "stsgl01p6.dev-paytv.smf1.mobitv", "stsgl01p6.integration-msp.smf1.mobitv", "stsgl02p1.ci-cp.dev.smf1.mobitv", "stsgl02p1.dev-paytv.smf1.mobitv", "stsgl02p1.func-cp.qa.smf1.mobitv", "stsgl02p1.integration-msp.smf1.mobitv", "stsgl02p1.media.dev.smf1.mobitv", "stsgl02p1.prod-msp.smf1.mobitv", "stsgl02p1.staging-msp.ops.smf1.mobitv", "stsgl02p12.integration-msp.smf1.mobitv", "stsgl02p13.integration-msp.smf1.mobitv", "stsgl02p2.integration-msp.smf1.mobitv", "stsgl02p2.media.dev.smf1.mobitv", "stsgl02p2.prod-msp.smf1.mobitv", "stsgl02p2.staging-msp.ops.smf1.mobitv", "stsgl02p3.integration-msp.smf1.mobitv", "stsgl03p1.func-cp.qa.smf1.mobitv", "stsgl04p1.func-cp.qa.smf1.mobitv", "sttcm01p1.func-cp.qa.smf1.mobitv", "sttco01p1.func-cp.qa.smf1.mobitv", "sttst01p1.ci-cp.dev.smf1.mobitv", "sttst01p1.staging-msp.ops.smf1.mobitv", "stvar01p1.ci-cp.dev.smf1.mobitv", "stvar01p1.dev-paytv.smf1.mobitv", "stvar01p1.func-cp.qa.smf1.mobitv", "stvar01p1.media.dev.smf1.mobitv", "stvar01p2.func-cp.qa.smf1.mobitv", "stvar02p1.ci-cp.dev.smf1.mobitv", "stvar02p1.dev-paytv.smf1.mobitv", "stvar02p1.func-cp.qa.smf1.mobitv", "stvar02p1.media.dev.smf1.mobitv", "stvar02p2.func-cp.qa.smf1.mobitv", "stvav01p1.prod-msp.smf1.mobitv", "stvav01p1.staging-msp.ops.smf1.mobitv", "stvav02p1.prod-msp.smf1.mobitv", "stvav02p1.staging-msp.ops.smf1.mobitv", "svcar01p1.ci-cp.dev.smf1.mobitv", "svcar01p1.dev-paytv.smf1.mobitv", "svcar01p1.dev.smf1.mobitv", "svcar01p1.integration-msp.smf1.mobitv", "svcar01p1.prod-msp.smf1.mobitv", "svcar01p1.staging-msp.ops.smf1.mobitv", "svcar01p2.integration-msp.smf1.mobitv", "svcar01p2.staging-msp.ops.smf1.mobitv", "svcar01p3.integration-msp.smf1.mobitv", "svcar01p3.prod-msp.smf1.mobitv", "svcar01p3.staging-msp.ops.smf1.mobitv", "svcar01p4.prod-msp.smf1.mobitv", "svcar01p4.staging-msp.ops.smf1.mobitv", "svcar01p5.prod-msp.smf1.mobitv", "svcar01p5.staging-msp.ops.smf1.mobitv", "svcar02p1.ci-cp.dev.smf1.mobitv", "svcar02p1.dev-paytv.smf1.mobitv", "svcar02p1.func-cp.qa.smf1.mobitv", "svcar02p1.integration-msp.smf1.mobitv", "svcar02p1.staging-msp.ops.smf1.mobitv", "svcar02p2.integration-msp.smf1.mobitv", "svcar02p2.prod-msp.smf1.mobitv", "svcar02p2.staging-msp.ops.smf1.mobitv", "svcar02p3.integration-msp.smf1.mobitv", "svcar02p3.prod-msp.smf1.mobitv", "svcar02p3.staging-msp.ops.smf1.mobitv", "svcar02p4.integration-msp.smf1.mobitv", "svcar02p4.prod-msp.smf1.mobitv", "svcar02p4.staging-msp.ops.smf1.mobitv", "svcar02p5.prod-msp.smf1.mobitv", "svcar02p5.staging-msp.ops.smf1.mobitv", "svdtm01p1.integration-msp.smf1.mobitv", "svdtm01p1.prod-msp.smf1.mobitv", "svdtm01p1.staging-msp.ops.smf1.mobitv", "svdtm02p1.integration-msp.smf1.mobitv", "svdtm02p1.prod-msp.smf1.mobitv", "svdtm02p1.staging-msp.ops.smf1.mobitv", "svftp01p1.ci-cp.dev.smf1.mobitv", "svftp01p1.func-cp.qa.smf1.mobitv", "svftp01p1.integration-msp.smf1.mobitv", "svftp01p1.prod-msp.smf1.mobitv", "svftp01p1.staging-msp.ops.smf1.mobitv", "svftp02p1.integration-msp.smf1.mobitv", "svftp02p1.prod-msp.smf1.mobitv", "svftp02p1.staging-msp.ops.smf1.mobitv", "svmgm01p1.ci-cp.dev.smf1.mobitv", "svmgm01p1.dev-paytv.smf1.mobitv", "svmgm01p1.dev.smf1.mobitv", "svmgm01p1.func-cp.qa.smf1.mobitv", "svmgm01p1.integration-msp.smf1.mobitv", "svmgm01p1.prod-msp.smf1.mobitv", "svmgm01p1.staging-msp.ops.smf1.mobitv", "svmgm01p2.ci-cp.dev.smf1.mobitv", "svmgm01p2.func-cp.qa.smf1.mobitv", "svmgm01p2.integration-msp.smf1.mobitv", "svmgm02p1.ci-cp.dev.smf1.mobitv", "svmgm02p1.integration-msp.smf1.mobitv", "svmgm02p2.integration-msp.smf1.mobitv", "svmgt01p1.ci-cp.dev.smf1.mobitv", "svmgt01p1.dev-paytv.smf1.mobitv", "svmgt01p1.func-cp.qa.smf1.mobitv", "svmgt01p1.integration-msp.smf1.mobitv", "svmgt01p1.prod-msp.smf1.mobitv", "svmgt01p1.staging-msp.ops.smf1.mobitv", "syslogmon.infra.smf1.mobitv", "test.ci-cp.dev.smf1.mobitv", "test01p1.ci-cp.dev.smf1.mobitv", "test12media-dev200subnet.media.dev.smf1.mobitv", "tomcat01.ci-cp.dev.smf1.mobitv", "wsfbt01p1.integration-msp.smf1.mobitv", "wsprs01p1.infra.smf1.mobitv", "wsprs02p1.infra.smf1.mobitv"}

// Test with single thread
//
func Test001_SingleThread(t *testing.T) {
	testRetrieval(t,1)
}

// Test with multiple threads with thread count greater than available input hosts
//
func Test002_MultipleThread(t *testing.T) {
	testRetrieval(t,64)
}

// Performs actual test with given thread count
//
func testRetrieval(t *testing.T, threadCount int) {
	var ctx = context.Background()

	var logBuffer = NewLogBuffer(ctx, "")

	// Make asynchronous call to get NFS configuration
	resultsChannel, err := RetrieveNfsConfigs(ctx, hosts, -1, logBuffer, threadCount)
	assert.Nil(t, err)
	assert.NotNil(t, resultsChannel)

	var vcenterName = testutil.GetEnvValueWithDefault(VCENTER_ENV_LOOKUP, DEFAULT_VCENTER)
	logger.Log.Info("Using VCenter: " + vcenterName)

	vcConnection, errAddressBookLookup := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vcenterName)
	assert.Nil(t, errAddressBookLookup)
	assert.NotNil(t, vcConnection)

	vcenterSync, errVcenter := vcenter.New(vcConnection)

	assert.Nil(t, errVcenter)
	assert.NotNil(t, vcenterSync)


	// Process all results until end-of-channel nil element received
	var resultsCount = 0    // Results count regardless of success or failure
	for {
		var result = <-resultsChannel

		if result != nil {
			if util.IsBlank(result.Error) {
				// Successful NFS configuration retrieval
				if !util.IsBlank(result.FstabLine) {
					fmt.Printf("%v\n", *result)
				} else {
					fmt.Printf("Host %s has no NFS mounts\n", result.Hostname)
				}
			} else {
				// Some error reported retrieving this result
				fmt.Printf("NFS configuration failed for host \"%s\" because of error: %s\n", result.Hostname, result.Error)
			}

			// Results count increment regardless of success or failure
			resultsCount++
		} else {
			// All results processed
			break
		}
	}

	// DB connection only needed to write logs to database
	var connection = vcenterSync.GetConnectionPool().Get(ctx)
	defer vcenterSync.GetConnectionPool().Put(connection)
	logBuffer.WriteAllLogs(-1, connection)

	// There is at least one return result per host
	assert.True(t, len(hosts) <= resultsCount)
}

// Test retrieval with manual host list which allows precise testing of hosts
//
func Test003_SyncTest(t *testing.T) {
	var ctx = context.Background()
	var configErr = config.SetPath("../../dora-config-overrides.json")
	assert.Nil(t, configErr)
	config.Config.SQLiteDataFolder = "/opt/mobi-dora/sqlite"

	// Force config load
	config.Config.ReadConfig(true)

	// Need to initialize the global addressbook as NFS needs it
	var errAddressBook = addressbook.InitDefaultAddressBook("../../address-book.json")
	assert.Nil(t, errAddressBook)

	var vcenterName = testutil.GetEnvValueWithDefault(VCENTER_ENV_LOOKUP, DEFAULT_VCENTER)
	logger.Log.Info("Using VCenter: " + vcenterName)

	vcConnection, errAddressBookLookup := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vcenterName)
	assert.Nil(t, errAddressBookLookup)
	assert.NotNil(t, vcConnection)

	vcenterSync, errVcenter := vcenter.New(vcConnection)

	assert.Nil(t, errVcenter)
	assert.NotNil(t, vcenterSync)

	// NFS sync returns warnings back through channel for client processing
	var warningsFeedback = make(chan string)
	go func() {
		// Process warnings asynchronously
		for {
			warning, more := <- warningsFeedback

			if
			more {
				logger.Log.Info(fmt.Sprintf("SSH retrieval warning: %s", warning))
			} else {
				logger.Log.Info("NFS warning channel closed")
				break
			}
		}
	}()

	var connection = vcenterSync.GetConnectionPool().Get(ctx)
	defer vcenterSync.GetConnectionPool().Put(connection)

	// Set the context with values to enable NFS logging otherwise NFS sync logging fails
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_CONNECTION, connection)
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_SYNC_HISTORY_ID, int64(-1))
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_VCENTER_SYNC_REC, vcenterSync)

	var logBuffer = NewLogBuffer(ctx, "")

	//errSync := vcenterSync.SyncNfsMounts(ctx, connection, 20, warningsFeedback, hosts...)
	//errSync := vcenterSync.SyncNfsMounts(ctx, connection, 16, warningsFeedback, "stmmx03p1.media.dev.smf1.mobitv", "psr-ms-frn-01.qa.smf1.mobitv")
	//errSync := vcenterSync.SyncNfsMounts(ctx, connection, 16, warningsFeedback, "msprf02p1.func-cp.qa.smf1.mobitv")
	errSync := SyncNfsMounts(ctx, connection, -1,16, logBuffer, warningsFeedback, "msprf01p1.func-cp.qa.smf1.mobitv")
	//errSync := SyncNfsMounts(ctx, vcenterSync, 1, "indor01p1.infra.smf1.mobitv", "insre01p1.infra.smf1.mobitv", "judrm02p1.integration-msp.smf1.mobitv")
	//errSync := SyncNfsMounts(ctx, vcenterSync, 1, "birpt01p1.integration-msp.smf1.mobitv", "birpt01p2.integration-msp.smf1.mobitv", "birpt02p2.integration-msp.smf1.mobitv", "dbmgt01p1.integration-msp.smf1.mobitv", "inepd01p1.integration-msp.smf1.mobitv", "inepd02p1.integration-msp.smf1.mobitv", "inepd03p1.integration-msp.smf1.mobitv", "ingan01p2.integration-msp.smf1.mobitv", "ingra01p1.integration-msp.smf1.mobitv", "inicg01p2.integration-msp.smf1.mobitv", "inicg02p1.integration-msp.smf1.mobitv", "inkba01p1.integration-msp.smf1.mobitv", "inldp01p1.integration-msp.smf1.mobitv", "inldp02p1.integration-msp.smf1.mobitv", "inlpd01p1.integration-msp.smf1.mobitv", "inlpd02p1.integration-msp.smf1.mobitv", "inpro01p1.integration-msp.smf1.mobitv", "juamq01p1.integration-msp.smf1.mobitv", "juamq02p1.integration-msp.smf1.mobitv", "juarn01p1.integration-msp.smf1.mobitv", "juarn02p1.integration-msp.smf1.mobitv", "juasp01p1.integration-msp.smf1.mobitv", "juasp02p1.integration-msp.smf1.mobitv")
	assert.Nil(t, errSync)

	logBuffer.WriteAllLogs(-1, connection)
}

// Test retrieval automatic host list from all poweredOn host in the referenced VCenter
//
// USAGE: Use this to test NFS sync for an entire VCenter.  See other tests to target specific VMs.
//
func Test004_SyncTest(t *testing.T) {
	var ctx = context.Background()
	config.Config.SQLiteDataFolder = "/opt/mobi-dora/sqlite"

	assert.NotPanics(t, func() {
		var configErr = config.SetPath("../../dora-config-overrides.json")
		assert.Nil(t, configErr)

		// Force config load
		config.Config.ReadConfig(true)

		// Need to initialize the global addressbook as NFS needs it
		var errAddressBook = addressbook.InitDefaultAddressBook("../../address-book.json")
		assert.Nil(t, errAddressBook)

		var vcenterName = testutil.GetEnvValueWithDefault(VCENTER_ENV_LOOKUP, DEFAULT_VCENTER)
		logger.Log.Info("Using VCenter: " + vcenterName)

		vcConnection, errAddressBookLookup := addressbook.GlobalAddressBook.GetSelectedVSphereConnection(vcenterName)

		assert.Nil(t, errAddressBookLookup)
		assert.NotNil(t, vcConnection)

		vcenterSync, errVcenter := vcenter.New(vcConnection)

		assert.Nil(t, errVcenter)
		assert.NotNil(t, vcenterSync)

		// NFS sync returns warnings back through channel for client processing
		var warningsFeedback = make(chan string)
		go func() {
			// Process warnings asynchronously
			for {
				warning, more := <- warningsFeedback

				if more {
					logger.Log.Info(fmt.Sprintf("SSH retrieval warning: %s", warning))
				} else {
					logger.Log.Info("NFS warning channel closed")
					break
				}
			}
		}()

		var connection = vcenterSync.GetConnectionPool().Get(ctx)
		defer vcenterSync.GetConnectionPool().Put(connection)

		var vcPaths, errListAllVm = vcenterSync.ListAllVmPath(false)

		assert.Nil(t, errListAllVm)
		assert.NotNil(t, vcPaths)

		var logBuffer = NewLogBuffer(ctx, "")

		// Thread count can be set to 1 to disable parallel pull with slower throughput and is also good for debugging
		//
		var errSync = SyncNfsMounts(ctx, connection, -1, 32, logBuffer, warningsFeedback, vcPaths...)
		assert.Nil(t, errSync)

		logBuffer.WriteAllLogs(-1, connection)
	})
}


// Test retrieval of Isilon data by logging into an Isilon server and parsing output of "isi stat".
// NOTE: the "isi stat" command can take upwards of 6 seconds to return its results and this is normal
//
func Test005_IsilonTest(t *testing.T) {
	config.SetPath("../../dora-config-overrides.json")

	// Need to initialize the global addressbook as NFS needs it
	var errAddressBook = addressbook.InitDefaultAddressBook("../../address-book.json")
	assert.Nil(t, errAddressBook)

	assert.NotPanics(t, func() {
		// Isilon server: 10.170.111.201
		// Qumulo server: 10.173.28.125
		clusterName, errSsh, errTimeout := RetrieveIsilonName(
			"10.170.111.201",
			addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Username,
			addressbook.GlobalAddressBook.Ssh.DefaultLogin.Isilon.Password)

		assert.Nil(t, errSsh)
		assert.Nil(t, errTimeout)
		assert.False(t, util.IsBlank(clusterName))
		fmt.Printf("Received Isilon Cluster Name: %s\n", clusterName)
	})
}


// Test retrieval of Isilon data by logging into an Isilon server and parsing output of "isi stat"
//
func Test006_QumuloTest(t *testing.T) {
	config.SetPath("../../dora-config-overrides.json")

	// Need to initialize the global addressbook as NFS needs it
	var errAddressBook = addressbook.InitDefaultAddressBook("../../address-book.json")
	assert.Nil(t, errAddressBook)

	assert.NotPanics(t, func() {
		// Isilon server: 10.170.111.201
		// Qumulo server: 10.173.28.125
		clusterName, errSsh, errTimeout := RetrieveQumuloName(
			"10.173.28.125",
			addressbook.GlobalAddressBook.Ssh.DefaultLogin.Qumulo.Username,
			addressbook.GlobalAddressBook.Ssh.DefaultLogin.Qumulo.Password)

		assert.Nil(t, errSsh)
		assert.Nil(t, errTimeout)
		assert.False(t, util.IsBlank(clusterName))
		fmt.Printf("Received Qumulo Cluster Name: %s\n", clusterName)
	})
}

// Low level Test Isilon network interface data parsing
//
func Test007_IsilonNetworkInterfaceParse(t *testing.T) {
	// Test that parsing for select IP's matches the LNN in the test data
	//
	{
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "10.170.111.211")
		assert.Nil(t, err)
		assert.Equal(t, 19, *lnn)
	}
	{
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "10.170.111.208")
		assert.Nil(t, err)
		assert.Equal(t, 20, *lnn)
	}
	{
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "10.170.111.209")
		assert.Nil(t, err)
		assert.Equal(t, 20, *lnn)
	}
	{
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "10.170.111.217")
		assert.Nil(t, err)
		assert.Equal(t, 24, *lnn)
	}
	{
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "10.170.111.218")
		assert.Nil(t, err)
		assert.Equal(t, 27, *lnn)
	}
	{
		// This IP address is not in the CSV so result must be nil
		lnn, err := findMatchingIsilonLNN(ISILON_NETWORK_INTERFACE_CSV_DATA, "1.1.1.1")
		assert.Nil(t, err)
		assert.Nil(t, lnn)
	}
}


// Raw test data kept at bottom of file to reduce clutter
//
const ISILON_NETWORK_INTERFACE_CSV_DATA = `
LNN,Name,Status,Owners,"IP Addresses"
19,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.201,10.170.111.207,10.170.111.211"
19,10gige-2,Up,"",""
19,10gige-agg-1,"Not Available","",""
19,ext-1,"No Carrier","",""
19,ext-2,"No Carrier","",""
19,ext-agg,"Not Available","",""
20,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.202,10.170.111.208,10.170.111.209"
20,10gige-2,Up,"groupnet0.paytvmspshare.paytvmspshare","10.173.29.68"
20,10gige-agg-1,"Not Available","",""
20,ext-1,"No Carrier","",""
20,ext-2,"No Carrier","",""
20,ext-agg,"Not Available","",""
21,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.204,10.170.111.205"
21,10gige-2,Up,"",""
21,10gige-agg-1,"Not Available","",""
21,ext-1,"No Carrier","",""
21,ext-2,"No Carrier","",""
21,ext-agg,"Not Available","",""
22,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.206,10.170.111.212,10.170.111.215"
22,10gige-2,Up,"groupnet0.qa-media-psr.qa-psr-media","10.174.124.22"
22,10gige-agg-1,"Not Available","",""
22,ext-1,"No Carrier","",""
22,ext-2,"No Carrier","",""
22,ext-agg,"Not Available","",""
23,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.213,10.170.111.216,10.170.111.219"
23,10gige-2,Up,"groupnet0.qa-media-psr.qa-psr-media","10.174.124.23"
23,10gige-agg-1,"Not Available","",""
23,ext-1,"No Carrier","",""
23,ext-2,"No Carrier","",""
23,ext-agg,"Not Available","",""
24,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.203,10.170.111.210,10.170.111.217"
24,10gige-2,"No Carrier","",""
24,10gige-agg-1,"Not Available","",""
24,ext-1,"No Carrier","",""
24,ext-2,"No Carrier","",""
24,ext-agg,"Not Available","",""
27,10gige-1,Up,"groupnet0.subnet0.pool0","10.170.111.214,10.170.111.218"
27,10gige-2,Up,"",""
27,10gige-agg-1,"Not Available","",""
27,ext-1,"No Carrier","",""
27,ext-2,"No Carrier","",""
27,ext-agg,"Not Available","",""
28,10gige-1,Up,"groupnet0.subnet0.psr","10.170.111.221"
28,10gige-2,Up,"",""
28,10gige-agg-1,"Not Available","",""
28,ext-1,"No Carrier","",""
28,ext-2,"No Carrier","",""
28,ext-agg,"Not Available","",""
29,10gige-1,Up,"",""
29,10gige-2,Up,"groupnet0.subnet0.psr","10.170.111.222"
29,10gige-agg-1,"Not Available","",""
29,ext-1,"No Carrier","",""
29,ext-2,"No Carrier","",""
29,ext-agg,"Not Available","",""

`