package nfs

import (
	"dora/pkg/logger"
	"dora/pkg/vcenter"
	"errors"
	"fmt"
	"github.com/robfig/cron/v3"
	"sync"
	"time"
)

//
//
// This module contains the code to manage the cron configuration for each NFS environment
//
//


// A map of environments along with their associated cron configuration
//
var cronRecords = make(map[string]*NfsCronRec)

// Mutex for serializing updates to the cron map
var mutexUpdates sync.Mutex

// Puts or replaces an cron expression in the map for the given environment name
//
// PARAM: environmentName   must match the environment name in the address book
// PARAM: cronExpression    cron expression for the environment name. Ex: "32 8,14,3, * * *"
func PutSyncObject(environmentName string, cronExpression string, vcenterSyncRec *vcenter.VCenterSyncRec, cronEnabled bool) (*NfsCronRec, error) {
	defer mutexUpdates.Unlock()
	mutexUpdates.Lock()

	var nfsSyncRec = NfsCronRec {
		EnvironmentName: environmentName,
		VCenterSyncRec:  vcenterSyncRec,
		CronExpression:  cronExpression,
		enabled:         cronEnabled,
		syncCron:        cron.New(),
	}

	// Enable the cron if specified
	nfsSyncRec.SetCronEnabled(nfsSyncRec.enabled)

	// Add the new sync object to the list
	cronRecords[environmentName] = &nfsSyncRec

	return &nfsSyncRec, nil
}

// Gets the cron expression for the given environment name
//
// PARAM: environmentName   must match the environment name in the address book
func GetNfsCronObject(environmentName string) *NfsCronRec {
	defer mutexUpdates.Unlock()
	mutexUpdates.Lock()

	return cronRecords[environmentName]
}

// Represents the NFS cron record for a given environment
//
type NfsCronRec struct {
	EnvironmentName     string        // Must match the environment's name as it is in the address book
	VCenterSyncRec      *vcenter.VCenterSyncRec // VCenter configuration record (contains DB pool and config information for the current environment -- NFS data retrieval needs list of VM's from the VCenter data cache)
	DbFilePath          string        // current path to the SQLite database that will be updated with newly retrieved NFS data
	enabled             bool          // cron currently enabled/disabled.  State of cron will match.  NOTE: Use getter/setter methods to access only.
	syncCron            *cron.Cron    // current active cron object (initialized to nil)
	CronExpression      string        // current cron schedule expression string
	ActiveSyncHistoryId *int64        // The current syncHistoryId otherwise is nil
}

/**
 * Gets the next time this cron will run at
 *
 * NOTE: null value is returned if the cron is not enabled or not initialized
 *
 * RETURN: time object representing when this cron will run next or a null value is returned if the cron is not enabled
 * or if not initialized
 */
func (o *NfsCronRec) CronNextScheduleRunTime() time.Time {
	if o.syncCron != nil {
		if o.syncCron.Entries() != nil && len(o.syncCron.Entries()) > 0 {
			// There should only be one entry
			return o.syncCron.Entries()[0].Next
		} else {
			// Cron job object exists but has no entries
			return time.Unix(0, 0) // 0 means cron not running
		}
	} else {
		// No such cron object currently so create one for next time
		o.syncCron = cron.New()

		return time.Unix(0, 0) // 0 means cron not running
	}
}

// Initializes this cron with the cron expression
//
// PARAM: cronExpression    the cron expression for this cron to run with
func (o *NfsCronRec) CronInit(cronExpression string) error {
	// Stop all active jobs
	if o.syncCron != nil {
		o.syncCron.Stop()
	}

	// Save the cron expression for viewing later by UI if required
	o.CronExpression = cronExpression

	// Recreate with new job with the given cron expression.  This is only one cron entry per environment!
	o.syncCron = cron.New()
	if _, err := o.syncCron.AddFunc(cronExpression, func() {
		logger.Log.Info(fmt.Sprintf("Starting cron scheduled NFS data sync run for environment: %s", o.EnvironmentName))

		//
		// Invoke this logic when cron starts
		//
		StartSyncInBackground("<cron>", o.VCenterSyncRec)
	}); err != nil {
		return errors.New(fmt.Sprintf("Could not create cron with given cron expression '%s' because of error: %v", cronExpression, err))
	} else {
		return nil
	}
}

// Starts the NFS sync run for this environment
//
// NOTE: This should only be called by the cron scheduler
//
// PARAM: ctx   standard GoLang context
//func (o *NfsCronRec) cronStart(ctx context.Context) {
//
//}

func (o *NfsCronRec) IsCronEnabled() bool {
	return o.enabled
}

// Sets this environment's cron to enable or disable
//
// NOTE: Normally would be set from UI when user decides to override default
//
// PARAM: enabled    set to true to enable and false to disable.  If enabled then will continue with the current cron schedule.
func (o *NfsCronRec) SetCronEnabled(enabled bool) {
	// Save new current state
	o.enabled = enabled

	// Update state based on current state
	if enabled {
		o.CronInit(o.CronExpression)
		o.syncCron.Start()
	} else {
		if o.syncCron != nil {
			// Stop the current cron job then nil it out so it gets recreated next time with latest cron expression
			// should it be restarted
			o.syncCron.Stop()
			o.syncCron = nil
		}
	}
}




