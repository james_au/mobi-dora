package nfs

import (
	"context"
	"crawshaw.io/sqlite"
	"crawshaw.io/sqlite/sqlitex"
	"dora/pkg/api"
	"dora/pkg/config"
	"dora/pkg/constants"
	"dora/pkg/dbutil"
	"dora/pkg/logger"
	"dora/pkg/syncrefreshlock"
	"dora/pkg/util"
	"dora/pkg/vcenter"
	"errors"
	"fmt"
	"sync"
	"time"
)

//
//
// This module contains the code to initiate an NFS data sync run.  It would be used for either a cron activated run or
// a manual user activated run.
//
//

// This map is used to prevent concurrent sync of the same environment by more than one process
//
var syncLockMap = syncrefreshlock.New()

// Starts the NFS data synchronization process in a background process
//
// NOTE: this method returns immediately while background threads process the NFS configuration retrieval
//
var startLock sync.Mutex
func StartSyncInBackground(username string, vcenterSyncRec *vcenter.VCenterSyncRec) (*api.NfsSyncHistoryRec, error) {
	//logger.Log.Info(o.GenerateSyncLogMsg(fmt.Sprintf("Re-syncing Hypervisor records into SQLite for VCenter connection \"%s\"", o.GetName())))
	//
	// Lock the entire sync startup to prevent race condition if two or more go routines try to start sync on the same
	// environment at the same time but only one go routine will be able to start
	defer startLock.Unlock()
	startLock.Lock()

	//
	// Must use own context.
	//
	// A context from the main thread cannot be used due to problem with crawshaw SQLite will see main context get
	// closed due to the main thread completing since this method runs asynchronously from the caller.  Then crawshaw
	// SQLite library generates fatal runtime error: SQLITE_INTERRUPT whenever any database operation is performed.
	var ctx = context.WithValue(context.Background(), constants.CTX_KEY_AUTH_USERNAME, username)

	// Get database connection from the pool
	var connection = vcenterSyncRec.GetConnectionPool().Get(ctx)

	// Always return connection to the pool at end of processing
	defer vcenterSyncRec.GetConnectionPool().Put(connection)

	// The log buffer that the rest of the sync processes should log to
	var logBuffer = NewLogBuffer(ctx, "New log buffer created.  Logs will be buffered and only saved to DB after transaction commit.")

	if lockRecord := syncLockMap.Exists(vcenterSyncRec.GetName()); lockRecord != nil {
		// Some other go-routine process is currently syncing this environment
		var errMessage = fmt.Sprintf("Cannot start sync because another process with environment lock ID '%s' is currently syncing this environment", lockRecord.GenerateID())
		logBuffer.logNfsSyncHistoryError(ctx, errMessage)

		return nil, errors.New(errMessage)
	} else {
		// No other go-routine process is syncing so lock this environment
		var lockStruct = syncLockMap.AddLock(vcenterSyncRec.GetName(), username)

		if syncHistoryRec, errHistoryRec := createNfsSyncRecordEntry(ctx, connection, vcenterSyncRec.GetName()); errHistoryRec != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Could not start sync because could not create sync history record in database because of error: %v", errHistoryRec))
		} else {
			// Run logic in background.  Caller can use the syncHistoryId to check on progress messages directly in
			// the database.
			go Run(ctx, syncHistoryRec.GetSyncHistoryId(), logBuffer, lockStruct, vcenterSyncRec)

			// No unexpected hard errors if this point is reached. Give the ID of the sync history DB record which will
			// have messages and sync status attached to it
			return syncHistoryRec, nil
		}
	}
}

// This initiates a sync run.  Can be triggered from UI, cron, or test.
//
// PARAM: ctx             standard Go context
// PARAM: syncHistoryId   the sync history ID which should be a new record created for this sync
// PARAM: logBuffer       log buffer to save any log messages to for write out later
// PARAM: lockStruct      locking record for this sync and should be removed at the end of the sync even if sync failed
// PARAM: vcenterSyncRec  vcenter cache object matching the database and is needed to query the VM list for the environment which drives the NFS retrieval process
//
func Run(ctx context.Context, syncHistoryId int64, logBuffer *LogBuffer, lockStruct *syncrefreshlock.SyncLockStruct, vcenterSyncRec *vcenter.VCenterSyncRec) {
	// Sync NFS mount data (SSH remote exec to all known VMs in the environment).  This must be run only *after* the
	// latest VM list is updated because it uses the VM list to scan the environment.
	//
	// NOTE: This *must* use the connection instance above in order to participate in the transaction save point.
	//       Do not use a separate connection instance or data will not be saved and also SQLite may fail out for
	//       odd reasons because it doesn't like concurrent changes coming from different connection objects.
	//
	logger.Log.Info(fmt.Sprintf("Syncing SSH mounts for environment: %s", vcenterSyncRec.GetName()))

	// Get database connection
	var connection = vcenterSyncRec.GetConnectionPool().Get(ctx)

	// Always return connection to the pool at end of processing
	defer vcenterSyncRec.GetConnectionPool().Put(connection)

	// Set the context with values to enable NFS logging otherwise NFS sync logging fails
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_CONNECTION, connection)
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_SYNC_HISTORY_ID, syncHistoryId)
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_VCENTER_SYNC_REC, vcenterSyncRec)
	ctx = context.WithValue(ctx, constants.CTX_KEY_VCENTER_ENV_NAME, vcenterSyncRec.GetName())

	// Save built up DB logs saved to the log buffer
	//
	// NOTE: This should be run last since it is the first defer() and GoLang runs deferrals in LIFO order
	//
	defer func() {
		// DB transaction committed to database. Now can save the buffered logs outside of the transaction.
		//
		if errLogWrite := logBuffer.WriteAllLogs(syncHistoryId, connection); errLogWrite != nil {
			// Warn
			logger.Log.Error(errLogWrite, fmt.Sprintf("Could not save logs from previous NFS sync run ID: %d", syncHistoryId))
		}
	}()

	defer func() {
		// Remove lock entry for this environment regardless of sync success/failure so other processes can sync
		// this environment afterwards.  Otherwise there'd be a stale lock that would require restarting the Go server
		// process to clear.
		var logMessage = fmt.Sprintf("Removing '%s' sync lock record: %s", vcenterSyncRec.GetName(), lockStruct.GenerateID())
		logger.Log.Info(logMessage)  // Low level log
		logBuffer.logNfsSyncHistoryInfo(ctx, logMessage)  // Log to sync history record too

		syncLockMap.DeleteLock(vcenterSyncRec.GetName())
	}()

	// Initialize Crawshaw SQLite "SavePoint" and defer its completion function.  The completion function will
	// COMMIT or ROLLBACK the transaction depending on if the error is null or not at function execution time.
	logBuffer.logNfsSyncHistoryInfo(ctx, "Starting SQLite database transaction save point")
	var errRollback error = nil
	defer sqlitex.Save(connection)(&errRollback)

	// Panic handler -- rolls back the transaction before returning from this function should a panic occur
	defer func() {
		if r := recover(); r != nil {
			// Panic occurred -- log it and rollback DB transaction
			var errMsg = ""
			errMsg = fmt.Sprintf("NFS Sync process generated a panic with message: %v", r)
			errRollback = errors.New(errMsg)
			logBuffer.logNfsSyncHistoryError(ctx, fmt.Sprintf("Fatal panic error during DB Sync process.  All DB statements in transaction will be rolled back.  Error was: %s", errRollback.Error()))
			updateSyncCompletion(ctx, connection, syncHistoryId, constants.DATA_SYNC_STATUS_INCOMPLETE,  -1)  // TODO: record processed count not available here so consider putting that value in the context
		} else {
			logger.Log.Info(fmt.Sprintf("NFS sync ID %d completed successfully and DB transaction committed to database", syncHistoryId))
		}
	}()

	// Clear relevant tables within this transaction.  This ensures that all old data is removed.  Users will still see
	// the old data while NFS sync is in progress because deletions are hidden with the connection's transaction until
	// they changes committed at the end with a quick all-or-none transaction commit which will then reveal all of the
	// newly updated data.
	var TABLE_NAMES = []string{"NfsMount"}
	var logMsg = fmt.Sprintf("Clearing tables within transaction: %v", TABLE_NAMES)
	logger.Log.Info(logMsg)
	logBuffer.logNfsSyncHistoryInfo(ctx, logMsg)
	if errClearTables := dbutil.ClearTables(connection, TABLE_NAMES...); errClearTables != nil {
		// Fatal
		errMsg := fmt.Sprintf("Could not clear table '%s' because of error: %v", TABLE_NAMES, errClearTables)
		logger.Log.Info(errMsg)
		logBuffer.logNfsSyncHistoryError(ctx, errMsg)

		// Causes SQLite rollback via its completion function
		errRollback = errClearTables
	} else {
		if vmList, errVmList := vcenterSyncRec.ListAllVmPath(false); errVmList != nil {
			// Not fatal log this as an error
			var infoMsg = fmt.Sprintf("Could not retrieve list of VMs from SQLite because of error: %v", errVmList)
			logger.Log.Info(vcenterSyncRec.GenerateSyncLogMsg(infoMsg))
			logBuffer.logNfsSyncHistoryInfo(ctx, infoMsg)
		} else {
			// NFS sync returns warnings back through channel for client processing.  Can save these warnings
			// on to the sync log as a sync log entry.
			var warningsFeedback = make(chan string, 256)
			go func() {
				// Process warnings asynchronously
				for {
					warning, more := <- warningsFeedback

					if more {
						var logMsg = fmt.Sprintf("SSH retrieval warning: %s", warning)
						logger.Log.Info(logMsg)

						// Save to sync log also
						logBuffer.logNfsSyncHistoryWarn(ctx, logMsg)
					} else {
						logger.Log.Info("NFS warning channel closed")
						break
					}
				}
			}()

			//
			// Perform NFS sync here
			//
			if errSyncStart := SyncNfsMounts(ctx, connection, syncHistoryId, int(config.Config.GetNfsConfigRetrievalMaxThreads()), logBuffer, warningsFeedback, vmList...); errSyncStart != nil {
				// Fatal problem starting the NFS sync in the background

				// Causes SQLite rollback via its completion function
				errRollback = errSyncStart
			}
		}
	}
}

// Create a new sync record in the SyncHistory SQLite database table.  The ID of the new record is returned.
// Use this method when starting a new sync task whether it be for auto or manual sync run.
//
func createNfsSyncRecordEntry(ctx context.Context, connection  *sqlite.Conn, environmentName string) (*api.NfsSyncHistoryRec, error) {
	if stmt, errStmt := connection.Prepare("INSERT INTO NfsSyncHistory (environment_name, status, initiated_by, start_time_ms, end_time_ms, records_processed) VALUES (?, ?, ?, ?, ?, ?)"); errStmt != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not create NfsSyncHistory record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()
		var status = constants.DATA_SYNC_STATUS_IN_PROGRESS
		var username = util.ExtractUser(ctx)
		var currentTimeMillis = util.CurrentTimeMillis()


		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindText(bindIncrementor(), environmentName)
		stmt.BindText(bindIncrementor(), status)
		stmt.BindText(bindIncrementor(), username)
		stmt.BindInt64(bindIncrementor(), currentTimeMillis)
		stmt.BindNull(bindIncrementor())     // The sync "end time " is null which indicates it is still in progress -- it will be populated with the actual end date when the sync completes
		stmt.BindInt64(bindIncrementor(), 0) // Initially, 0 records processed

		if _, errStep := stmt.Step(); errStep != nil {
			// Fatal
			return nil, errors.New(fmt.Sprintf("Could not execute SQL statement because of error: %v", errStep))
		} else {
			// Expecting exactly one row to be returned
			// INSERT completed successfully
			var syncHistoryId = connection.LastInsertRowID()

			return &api.NfsSyncHistoryRec{
				SyncHistoryId:    syncHistoryId,
				EnvironmentName:  environmentName,
				Status:           status,
				InitiatedBy:      username,
				StartTimeMs:      currentTimeMillis,
				EndTimeMs:        -1,
				RecordsProcessed: 0,
			}, nil
		}
	}
}

// Call this to update and "finalize" an NfsSyncHistory record's completion time and completion values
//
func updateSyncCompletion(ctx context.Context, connection  *sqlite.Conn, syncHistoryId int64, status string, nbrRecordsSync int64) error {
	if stmt, errStmt := connection.Prepare("UPDATE NfsSyncHistory SET status = ?, end_time_ms = ?, records_processed = ? WHERE id = ?"); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not update NfsSyncHistory record because of DB statement creation error: %v", errStmt))
	} else {
		// Close up statement at the end other wise the connection cannot be returned to the pool
		defer stmt.Finalize()

		var bindIncrementor = sqlite.BindIncrementor()
		stmt.BindText(bindIncrementor(), status)
		stmt.BindInt64(bindIncrementor(), util.CurrentTimeMillis()) // Set completion time to UTC time right now on app server
		stmt.BindInt64(bindIncrementor(), nbrRecordsSync)
		stmt.BindInt64(bindIncrementor(), syncHistoryId)

		if _, errStmt := stmt.Step(); errStmt != nil {
			// Fatal
			return errors.New(fmt.Sprintf("Could not update NfsSyncHistory record ID %d because of database statement execution error: %v", syncHistoryId, errStmt))
		} else {
			logger.Log.Info(fmt.Sprintf("NfsSyncHistory record ID: %d updated successfully", syncHistoryId))
		}
	}

	return nil
}

// Updates SQLite NFS cache data after making SSH calls to the provided list of hosts
//
// NOTE: This method blocks caller but it will execute with parallel go routines in background
//
// PARAM: ctx  standard go context
// PARAM: connection   active and open SQLite DB connection object
// PARAM: syncHistoryId  the ID of the sync history record -- needing for logging events to the proper sync event
// PARAM: threadCount  max threads run for parallel retrieval which greatly improves NFS configuration retrieval across SSH hosts.  NOTE: can set to 1 which can good for thread debugging purposes but will be very slow for a large number of SSH hosts to check.
// PARAM: warningsResponseChan  a channel where string warning messages will be sent for caller to process & store as required
// PARAM: vcPaths   1 or more full VCenter paths to VMs to check.  NOTE: can also just use FQDN but not recommended as the path is needed for full report generation.  Simpler FQDN can be useful for testing purposes though.
//
func SyncNfsMounts(ctx context.Context, connection *sqlite.Conn, syncHistoryId int64, threadCount int, logBuffer *LogBuffer, warningsResponseChan chan string, vcPaths ...string) error {
	// Pass in the database connection in the context to enable NFS retrieval code to check e-mail if run is taking
	// longer than the average of the past retrievals.  The connection is required to do this.  If connection is not
	// passed then this feature will be disabled.
	ctx = context.WithValue(ctx, constants.CTX_KEY_NFS_SYNC_CONNECTION, connection)

	//
	// Execute NFS retrieval process and will receive results asynchronously on the returned channel.  NOTE: the
	// NFS config retrieval process uses parallel threads to improve SSH execution throughput greatly.
	//
	if resultsChannel, errRetrieve := RetrieveNfsConfigs(ctx, vcPaths, syncHistoryId, logBuffer, threadCount); errRetrieve != nil {
		// Fatal
		return errRetrieve
	} else {
		// Process all results until end-of-channel nil element received
		var mountCount = 0   // Count actual mounts found (not all host have NFS mounts and some hosts may have more than 1)

		// Mark start time for stats at the end
		var startTime = time.Now()

		defer func() {
			if warningsResponseChan != nil {
				// Send empty string to warnings channel at end of function to indicate completion
				logger.Log.Info("Closing NFS retrieval warnings channel")
				close(warningsResponseChan)
			}
		}()

	outer:
		for {
			var nfsRecord = <-resultsChannel

			if nfsRecord != nil {
				if util.IsBlank(nfsRecord.Error) {
					// Successful NFS configuration retrieval
					if !util.IsBlank(nfsRecord.FstabLine) {
						// Upsert NFS data for host

						if errUpsert := UpsertNFSRecord(connection, nfsRecord); errUpsert != nil {
							// Not fatal but log to the caller
							if warningsResponseChan != nil {
								warningsResponseChan <- fmt.Sprintf("Failed to upsert NFS record because of error: %v", errUpsert)
							}
						} else {
							logger.Log.Info(fmt.Sprintf("Successful upsert of NfsRecord: %v", nfsRecord))
						}

						// Results count increment regardless of success or failure
						mountCount++
					} else {
						logger.Log.Info(fmt.Sprintf("Host %s has no NFS mounts\n", nfsRecord.Hostname))
					}
				} else {
					// Some error reported retrieving this result.  Do not save.  Print error instead
					if warningsResponseChan != nil {
						warningsResponseChan <- (fmt.Sprintf("NFS configuration failed for host \"%s\" because of error: %s\n", nfsRecord.Hostname, nfsRecord.Error))
					}
				}
			} else {
				// All results processed
				break outer
			}
		}

		//
		// NFS retrieval is complete when the thread breaks out of the infinite loop above.  Generate final stats.
		//
		var finishedTime = time.Now().Unix()-startTime.Unix()
		var finishedMessage = fmt.Sprintf("NFS data sync retrieval completed for %d VMs and %d NFS hosts in %.2f minutes", len(vcPaths), mountCount, float64(finishedTime)/60.0)

		logger.Log.Info(finishedMessage)
		logBuffer.logNfsSyncHistoryInfo(ctx, finishedMessage)

		// Close up retrieval record with final completion status
		updateSyncCompletion(ctx, connection, syncHistoryId, constants.DATA_SYNC_STATUS_COMPLETE, int64(mountCount))
	}

	// All background worker threads completed and no errors if this point is reached so can return control to caller
	return nil
}

// Finds available
//
func updateStaleSyncLogs(latestSyncHistoryId int64, connection sqlite.Conn) (int, error) {
	if stmtUpdate, errUpdate := connection.Prepare("UPDATE NfsSyncHistory SET status = $newStatus WHERE status = $currentStatus AND start_time_ms < (SELECT start_time_ms FROM NfsSyncHistory WHERE id = $syncHistoryId)"); errUpdate != nil {
		// Fatal - unlikely
		return 0, errors.New(fmt.Sprintf("Could not update stale sync history records because of database statment error: %v", errUpdate))
	} else {
		stmtUpdate.SetText("newStatus", constants.DATA_SYNC_STATUS_ABORTED)
		stmtUpdate.SetText("currentStatus", constants.DATA_SYNC_STATUS_IN_PROGRESS)
		stmtUpdate.SetInt64("syncHistoryId", latestSyncHistoryId)

		stmtUpdate.Step()
		stmtUpdate.Reset()
		stmtUpdate.Finalize()
	}

	return 0, nil
}

// Upserts an NFS record into the database
//
// PARAM: connection      live connection to the database
// PARAM: nfsResultStruct the NFS data record to upsert
//
func UpsertNFSRecord(connection *sqlite.Conn, nfsResultStruct *ResultStruct) error {
	//
	// Delete any existing record first where "hostname" and "mount" are the composite primary keys
	//
	if stmtDelete, errStmt := connection.Prepare(`DELETE FROM NfsMount WHERE hostname = $hostname AND mount = $mount`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not delete existing NfsMount record %v because of database execution error: %v", nfsResultStruct, errStmt))
	} else {
		// Translate data record into SQLite row
		//
		stmtDelete.SetText("$hostname", nfsResultStruct.Hostname)
		stmtDelete.SetText("$mount", nfsResultStruct.HostMount)
		stmtDelete.Step()
		stmtDelete.Reset()
		stmtDelete.Finalize()
	}

	// Insert the row as replacement
	//
	if stmtInsert, errStmt := connection.Prepare(`INSERT INTO NfsMount(hostname, hostpath, mount, mount_user, mount_group, mount_perms, nfs_host, nfs_path, nfs_type, nfs_cluster_name, nfs_isilon_lnn, nfs_capacity_bytes, nfs_used_bytes, fstab_line, last_sync_ms) VALUES($hostname, $hostpath, $mount, $mount_user, $mount_group, $mount_perms, $nfs_host, $nfs_path, $nfs_type, $nfs_cluster_name, $nfs_isilon_lnn, $nfs_capacity_bytes, $nfs_used_bytes, $fstab_line, $last_sync_ms)`); errStmt != nil {
		// Fatal
		return errors.New(fmt.Sprintf("Could not insert NfsMount record %v because of database execution error: %v", nfsResultStruct, errStmt))
	} else {
		// Translate data record into SQLite row
		//

		// Isilon LNN might be null.  Therefore process accordingly.
		if nfsResultStruct.NfsIsilonLNN == nil {
			stmtInsert.SetNull("$nfs_isilon_lnn")
		} else {
			stmtInsert.SetInt64("$nfs_isilon_lnn", *nfsResultStruct.NfsIsilonLNN)
		}

		stmtInsert.SetText("$hostname", nfsResultStruct.Hostname)
		stmtInsert.SetText("$hostpath", nfsResultStruct.HostVcPath)
		stmtInsert.SetText("$mount", nfsResultStruct.HostMount)
		stmtInsert.SetText("$mount_user", nfsResultStruct.HostMountUsername)
		stmtInsert.SetText("$mount_group", nfsResultStruct.HostMountGroupname)
		stmtInsert.SetText("$mount_perms", nfsResultStruct.HostMountPerms)
		stmtInsert.SetText("$nfs_host", nfsResultStruct.NfsHost)
		stmtInsert.SetText("$nfs_path", nfsResultStruct.NfsPath)
		stmtInsert.SetText("$nfs_type", nfsResultStruct.NfsType)
		stmtInsert.SetText("$nfs_cluster_name", nfsResultStruct.NfsClusterName)
		stmtInsert.SetInt64("$nfs_capacity_bytes", nfsResultStruct.CapacityBytes)
		stmtInsert.SetInt64("$nfs_used_bytes", nfsResultStruct.CapacityUsedBytes)
		stmtInsert.SetText("$fstab_line", nfsResultStruct.FstabLine)
		stmtInsert.SetInt64("$last_sync_ms", util.CurrentTimeMillis())
		stmtInsert.Step()
		stmtInsert.Reset()
		stmtInsert.Finalize()
	}

	// No error if this point is reached
	return nil
}
