package balancingutil

import (
	"dora/pkg/logger"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Single regex object is thread-safe and can be shared
var reNodeType = regexp.MustCompile(`([^\d]+)(\d+)p(\d+).*`)

// Parses a given vm hostname or FQDN into the component mobi parts: (1) nodetype, (2) leg, and (3) cluster.
//
// Ex: stsgl.paytv.smf1.mobitv => (name = "stsgl", Leg = 3, Cluster = 100)
// Ex: msfrn01p1 => (name = "msfrn", Leg = 3, Cluster = 1)
// Ex: dbtest => nil
//
// If the given input does not match expected format then a nil reference is returned instead!  This would be
// VM names that do not follow MobiTV naming conventions.  The balance algorithm can then ignore these nodes or
// perform some other action with them.
//
func ParseNodeType(nodeName string) *NodeTypeStruct {
	var matches = reNodeType.FindAllStringSubmatch(nodeName, -1)

	if len(matches) == 1 {
		return &NodeTypeStruct{
			NodeType: matches[0][1],
			Leg:      _parseNumber(matches[0][2]), // this will convert to int no problem because regex has verified it
			Cluster:  _parseNumber(matches[0][3]), // this will convert to int no problem because regex has verified it
		}
	} else {
		return nil
	}
}

// Converts a string to a number.
//
// Used internally only.  Input must already be pre-validated as a valid integer.
func _parseNumber(s string) int32 {
	if num, err := strconv.Atoi(s); err != nil {
		// Should not happen but log it
		logger.Log.Info(fmt.Sprintf("Could not parse string '%s' to a number", s))
		return -1
	} else {
		return int32(num)
	}
}

// Contains the parsed out section of a node name.  Example:
// stsgl03p1 => (name = "stsgl", Leg = 3, Cluster = 1)
//
// This pre-parsed value increase rule verification runtime and easier to code
//
type NodeTypeStruct struct {
	NodeType string
	Leg      int32
	Cluster  int32
}

// Defines the VMType values
//
// IMPORTANT: the client-side will use these values as well any changes here must also be made
// manually to the client-side code!
const (
	VMTYPE_SENSITIVE     = 3
	VMTYPE_HEAVY_WRITERS = 2
	VMTYPE_HEAVY_READERS = 1
	VMTYPE_NORMAL        = 0
)

// Returns true if the given match regexp matches the node name of the given host.  This is used for matching
// vmTypes that are expressed as a regular expression like "nvrcw.*b"
//
// Example: a given regex "nvrcw.*b" will match nvrcw01p1b.paytv.smf1.mobitv but not nvrcw01p1a.paytv.smf1.mobitv
func HostRegexMatch(matchString string, hostName string) (bool, error) {
	// NOTE: the given regex is modified a little by adding a "catchall" pattern to it.
	if regexNodeType, errRegex := regexp.Compile(strings.TrimSpace(matchString) + "(.*)"); errRegex != nil {
		// Fatal - Regex syntax error
		return false, errors.New(fmt.Sprintf("Regex parse error: %v", errRegex))
	} else {
		var matches = regexNodeType.FindAllStringSubmatch(hostName, 1)

		return matches != nil && len(matches) != 0, nil
	}
}
