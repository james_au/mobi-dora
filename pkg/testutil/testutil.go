package testutil

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

//
// These are utilities that are useful for writing test code
//

// OS environment variable the user needs to set to enable test auth
const TEST_AUTH_ENV_KEY = "TEST_VC_AUTH"

// Login data will be parsed from OS environment variable into this data structure
//
type TestVCLogin struct {
	Name     string
	URL      string
	Username string
	Password string
}

// Returns a standard status string
func (o *TestVCLogin) GetStatus() string {
	// Return login information but with password redacted
	return fmt.Sprintf("Using environment auth login (%s::%s::%s::****)", o.Name, o.URL, o.Username)
}

// Gets the environment variables used for VCenter E2E testing
//
// Ref: https://gobyexample.com/environment-variables
func GetVCAuthInfoFromOSEnv() (*TestVCLogin, error) {
	var loginTest = os.Getenv(TEST_AUTH_ENV_KEY)

	if len(strings.TrimSpace(loginTest)) > 0 {
		var split = strings.Split(loginTest, "|")

		return &TestVCLogin{
			Name:     split[0],
			URL:      split[1],
			Username: split[2],
			Password: split[3],
		}, nil
	} else {
		return nil, errors.New(fmt.Sprintf("Required shell environment variable %s required for testing authentication is missing.  Example: NewPayTV|invct02p1.infra.smf1.mobitv:80|sre@infra.smf1.mobitv|mypassword", TEST_AUTH_ENV_KEY))
	}
}

// Attempts to look up the environment variable value at key and returns its value.  If not found then the default is
// returned instead
//
// PARAM: key     name of environment variable to look up
// PARAM: default default value if environment variable is not available
func GetEnvValueWithDefault(key string, defaultValue string) string {
	if value, found := os.LookupEnv(key); !found {
		// Environment variable with give key name not available in shell so return default instead
		return defaultValue
	} else {
		return value
	}
}
