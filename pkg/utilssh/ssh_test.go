package utilssh

import (
	"dora/pkg/config"
	"dora/pkg/util"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// Tests actual SSH connection with configured log in to known mobitv host with password credentials
//
// NOTE: this is not a unit test and requires adjusting hostname and credentials appropriate to your environment
//
func Test001_SSH_password(t *testing.T) {
	var sshExec = SshExec{
		TimeoutConnectMs:   3000,
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	assert.NotPanics(t, func() {
		// Test configuration read
		var errConfig = config.SetPath("../../dora-config-overrides.json")
		assert.Nil(t, errConfig)

		var username = config.Config.GetNfsConfigRetrieveUsername()
		var password = config.Config.GetNfsConfigRetrievePassword()
		assert.False(t, util.IsBlank(username))
		assert.False(t, util.IsBlank(password))

		sshClient, errClient := OpenSSHClient(sshExec, "insre01p1.infra.smf1.mobitv", username, password)
		assert.Nil(t, errClient)
		defer sshClient.Close()

		output, errExec := sshExec.RemoteRun(sshClient, "df -h", "", nil);

		assert.Nil(t, errExec)
		fmt.Println(output)
		assert.False(t, util.IsBlank(output))
	})
}

// Tests actual SSH connection with configured log in to known mobitv host with SSH key pass file credentials
//
// NOTE: this is not a unit test and requires adjusting hostname and credentials appropriate to your environment
//       such as public keys installed and corresponding user accounts created
//
func Test001_SSH_ssh_keypass(t *testing.T) {
	var sshExec = SshExec{
		TimeoutConnectMs:   3000,
		PrivateKeyFilePath: "../../dora-ssh/id_rsa",
		StdOut:             "",
		StdErr:             "",
	}

	assert.NotPanics(t, func() {
		// Test configuration read
		var errConfig = config.SetPath("../../dora-config-overrides.json")
		assert.Nil(t, errConfig)

		var username = config.Config.GetNfsConfigRetrieveUsername()
		var password = config.Config.GetNfsConfigRetrievePassword()
		assert.False(t, util.IsBlank(username))
		assert.False(t, util.IsBlank(password))

		// Create SSH client with blank password to use keyfile but still need login username
		sshClient, errClient := OpenSSHClient(sshExec, "insre01p1.infra.smf1.mobitv", username, "")
		assert.Nil(t, errClient)
		defer sshClient.Close()

		output, errExec := sshExec.RemoteRun(sshClient, "df -h", "", nil);

		assert.Nil(t, errExec)
		fmt.Println(output)
		assert.False(t, util.IsBlank(output))
	})
}


// Tests actual SSH connection with configured log in to known mobitv host with password credentials and run a command
// that does not quite
//
// NOTE: this is not a unit test and requires adjusting hostname and credentials appropriate to your environment
//
func Test003_SSH_password_timeout(t *testing.T) {
	var sshExec = SshExec{
		TimeoutConnectMs:   3000,
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	assert.NotPanics(t, func() {
		// Test configuration read
		var errConfig = config.SetPath("../../dora-config-overrides.json")
		assert.Nil(t, errConfig)

		// Force config load
		config.Config.ReadConfig(true)

		var username = config.Config.GetNfsConfigRetrieveUsername()
		var password = config.Config.GetNfsConfigRetrievePassword()
		assert.False(t, util.IsBlank(username))
		assert.False(t, util.IsBlank(password))
		sshClient, errClient := OpenSSHClient(sshExec, "insre01p1.infra.smf1.mobitv", username, password)
		assert.Nil(t, errClient)
		defer sshClient.Close()

		// Run a command that does not quit.  Use a simple Bash script like:
		// while :
		// do
		//   sleep 5
		// done
		//
		// NOTE: you may need to manually kill the process even after test is complete and SSH connection is closed
		// NOTE: can find process ID with pgrep command: pgrep --full -u dorasys process_hang_simulate
		const HANGING_COMMAND = "/home/dorasys/df"
		_, errExec, errTimeout := sshExec.RemoteRunWithTimeout(sshClient, HANGING_COMMAND, "", 5000 * time.Millisecond, nil)

		assert.Nil(t, errExec)
		assert.NotNil(t, errTimeout)

		// Check for at least one hanging process by the same command
		pids, errExisting := GetExistingProcessesOnHost(sshExec, sshClient, username, HANGING_COMMAND)
		assert.Nil(t, errExisting)
		assert.NotNil(t, pids)
		assert.True(t, len(pids) > 0)

		// Kill the hanging process
		fmt.Printf("Killing hung processes: %v\n", pids)
		KillProcessesOnHost(sshExec, sshClient, pids...)

		// Check for zero hanging process by the same command since they should be manually killed
		pids, errExisting = GetExistingProcessesOnHost(sshExec, sshClient, username, HANGING_COMMAND)
		assert.Nil(t, errExisting)
		assert.NotNil(t, pids)
		assert.Equal(t, 0, len(pids))

		// Should not reach this line until timeout is elapsed above
		sshClient.Close()
	})
}


// Tests killing of "isi stat" command an isilon NFS server with the dora login
//
// Procedure:
// (1) Choose the IP of one Isilon that is accessible to SSH login
// (2) Log onto 1 or more with "dorasys" user
// (3) Log onto 1 or more with "root" user (optional but allows further test)
// (4) Quickly run "isi stat" on all open terminals above while also running this test.  The "isi stat" run by the
//     "dorasys" users should be killed shortly but the "isi stat" will continue to run until completion on terminals
//     that logged in with the "root" user
//
// NOTE: this test works because "isi stat" takes 5 to 8 seconds to complete which allows enough time for manual work above
// NOTE: this is not a unit test and requires adjusting hostname and credentials appropriate to your environment
//
func Test004_isilon_process_kill(t *testing.T) {
	var sshExec = SshExec{
		TimeoutConnectMs:   3000,
		PrivateKeyFilePath: "",
		StdOut:             "",
		StdErr:             "",
	}

	assert.NotPanics(t, func() {
		// Test configuration read
		var errConfig = config.SetPath("../../dora-config-overrides.json")
		assert.Nil(t, errConfig)

		// Force config load
		config.Config.ReadConfig(true)

		var username = config.Config.GetNfsConfigRetrieveUsername()
		var password = config.Config.GetNfsConfigRetrievePassword()
		assert.False(t, util.IsBlank(username))
		assert.False(t, util.IsBlank(password))
		sshClient, errClient := OpenSSHClient(sshExec, "10.170.111.201", username, password)
		assert.Nil(t, errClient)
		defer sshClient.Close()

		// Run
		// NOTE: you may need to manually kill the process even after test is complete and SSH connection is closed
		// NOTE: can find process ID with pgrep command: pgrep --full -u dorasys process_hang_simulate
		const HANGING_COMMAND = "isi stat"
		_, errExec, errTimeout := sshExec.RemoteRunWithTimeout(sshClient, HANGING_COMMAND, "", 5000 * time.Millisecond, nil)

		assert.Nil(t, errExec)
		assert.NotNil(t, errTimeout)

		// Check for at least one hanging process by the same command
		pids, errExisting := GetExistingProcessesOnHost(sshExec, sshClient, username, HANGING_COMMAND)
		assert.Nil(t, errExisting)
		assert.NotNil(t, pids)
		assert.True(t, len(pids) > 0)

		// Kill the hanging process
		fmt.Printf("Killing hung processes: %v\n", pids)
		KillProcessesOnHost(sshExec, sshClient, pids...)

		// Check for zero hanging process by the same command since they should be manually killed
		pids, errExisting = GetExistingProcessesOnHost(sshExec, sshClient, username, HANGING_COMMAND)
		assert.Nil(t, errExisting)
		assert.NotNil(t, pids)
		assert.Equal(t, 0, len(pids))

		// Should not reach this line until timeout is elapsed above
		sshClient.Close()
	})
}