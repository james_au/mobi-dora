package utilssh

import (
	"bytes"
	"dora/pkg/logger"
	"dora/pkg/util"
	"dora/pkg/util/file"
	"errors"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"regexp"
	"strings"
	"time"
)

// Default number of seconds to wait between kill deferrals
const KILL_DEFER_WAIT_SECS = 10 * time.Second

// Unix nice lowest priority.  Must not be constant as it requires address.
//
var UNIX_PRIORITY_MIN = 19

type SshExec struct {
	TimeoutConnectMs   time.Duration // SSH connect timeout in milliseconds
	PrivateKeyFilePath string // Path to file containging private key data
	PrivateKeyString   string // Direct ssh private key string
	StdOut             string // Will always be nil until execution completes
	StdErr             string // Will always be nil until execution completes
}

// Will return an authenticated SSH client struct object based on given auth options.
//
// Params:
// server - a map with keys "user", "host", "password"  where "password" is optional if you are using other auth methods
// interactiveAuth - a loaded "ssh keyboard" auth with the password (optional if you are using other auth methods)
//
// NOTE:  There can be multiple auth methods.  This method will accept a password in either the
// server hash map for "SSH password" auth.  If no passwords are provided, this function will
// default to using the SSH private key file auth method provided in the struct constructor method
//
func (o *SshExec) NewClient(server map[string]interface{}, interactiveAuth ssh.KeyboardInteractiveChallenge) (*ssh.Client, error) {
	var host string
	if value, isFound := server["host"]; isFound {
		host = value.(string)
	} else {
		// Fatal -- SSH url is required regardless of auth method
		return nil, errors.New(fmt.Sprintf("Cannot connect to SSH server.  No SSH host url was specified."))
	}

	var user string
	if value, isFound := server["user"]; isFound {
		user = value.(string)
	} else {
		// Fatal -- user is required regardless of auth method even for SSH key file auth
		return nil, errors.New(fmt.Sprintf("Cannot connect to SSH server '%s'.  No login user was specified.", host))
	}

	var password string
	// Password is optional as there are other auth methods that the user can set
	if value, isFound := server["password"]; isFound {
		password = value.(string)
	}

	// Read in private key if specified
	//
	// Can specify private key either by direct byte string or via file
	// reference.  The direct string value has precedence if both string
	// and file reference are specified.
	//
	var signer ssh.Signer
	if len(o.PrivateKeyString) > 0 {
		// Create the Signer for this private key.
		_signer, err := ssh.ParsePrivateKey([]byte(o.PrivateKeyString))
		if err != nil {
			return nil, errors.New(fmt.Sprintf("unable to parse private key: %v", err))
		} else {
			signer = _signer
		}
	} else if file.FileExists(o.PrivateKeyFilePath) {
		key, err := ioutil.ReadFile(o.PrivateKeyFilePath)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Unable to read private key: %v", err))
		}

		// Create the Signer for this private key.
		_signer, err := ssh.ParsePrivateKey(key)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("unable to parse private key: %v", err))
		} else {
			signer = _signer
		}
	}

	// Authentication methods -- will be populated by provided auth data below
	var authMethods []ssh.AuthMethod

	// Add password only if specified
	if len(password) > 0 {
		authMethods = append(authMethods, ssh.Password(password))
	}

	// Add "keyboard interactive" auth only if specified
	if interactiveAuth != nil {
		authMethods = append(authMethods, ssh.KeyboardInteractive(interactiveAuth))
	}

	// Add SSH key file auth only if specified
	if signer != nil {
		authMethods = append(authMethods, ssh.PublicKeys(signer))
	}

	// Verify that at least once auth method was specified and error out otherwise
	if len(authMethods) > 0 {
		var config = &ssh.ClientConfig{
			User:            user,
			Auth:            authMethods,
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
			Timeout:         o.TimeoutConnectMs * time.Millisecond,
		}
		// Connect
		var client, err = ssh.Dial("tcp", host + ":22", config)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("SSH DIAL Error: %v", err))
		}

		return client, nil
	} else {
		// Fatal - No auth methods were given!
		return nil, errors.New(fmt.Sprintf("Cannot connect to SSH server '%s'.  No auth methods were given.", host))
	}
}

// Runs the SSH remote command
//
// WARNING: this method will wait indefinitely if the remote command does not exit.
//
// PARAM: client         configured SSH client
// PARAM: cmd            shell command for the host to execute
// PARAM: prefixCommands optional commands to run first (usually "set" environment commands).  These are run *before* the command
// PARAM: unixPriority  standard unix priority.  Allowed range -20 (highest priority) .. 19 (lowest priority).  Or use nil to use default priority.
// RETURN: string   stdout results of the command
// RETURN: error    on an any SSH error (nil if no error)
func (o *SshExec) RemoteRun(client *ssh.Client, cmd, prefixCommands string, unixPriority *int) (string, error) {
	// Create a session. It is one session per command.
	session, err := client.NewSession()
	if err != nil {
		return "", errors.New(fmt.Sprintf("SSH Session Error: %v", err))
	}
	defer session.Close()
	var bufStdOut bytes.Buffer
	var bufStdErr bytes.Buffer
	session.Stdout = &bufStdOut
	session.Stderr = &bufStdErr

	// Append "nice" command modifier to run as specified user priority if specified
	if unixPriority != nil {
		cmd = fmt.Sprintf("nice -n %d %s", *unixPriority, cmd)
	}

	// Append prefix commands (if any)
	if util.IsNotBlank(prefixCommands) {
		cmd = prefixCommands + "; " + cmd
	}

	// Finally, run the command
	err = session.Run(cmd)

	o.StdOut = bufStdOut.String()
	o.StdErr = bufStdErr.String()

	if err != nil {
		return bufStdErr.String() + " " + bufStdOut.String(), errors.New(fmt.Sprintf("SSH Session Run Error: %v", err))
	}

	return fmt.Sprint(bufStdOut.String()), nil
}

// Runs the SSH remote command but with a specified forced timout where results are abandoned if timeout reached.
//
// Its recommended to use this timeout method over the non-timeout method
//
// PARAM: client         configured SSH client
// PARAM: cmd            shell command for the host to execute
// PARAM: prefixCommands optional commands to run first (usually "set" environment commands).  These are run *before* the command
// PARAM: timeout        A duration timeout where this method no longer waits for results and returns a timeout error
// PARAM: unixPriority  standard unix priority.  Allowed range -20 (highest priority) .. 19 (lowest priority).  Or use nil to use default priority.
// RETURN: string   stdout results of the command
// RETURN: error    time out error when timeout period has elapsed (nil if no timeout)
// RETURN: error    on an any SSH error (nil if no error)
func (o *SshExec) RemoteRunWithTimeout(client *ssh.Client, cmd, prefixCommands string, timeout time.Duration, unixPriority *int) (string, error, error) {
	var resultsChan = make(chan interface{})

	// Run SSH remote run in asynchronous go routine and return results to channel when available
	go func() {
		if stringResults, errExec := o.RemoteRun(client, cmd, prefixCommands, unixPriority); errExec != nil {
			resultsChan <- errExec
		} else {
			resultsChan <- stringResults
		}
	}()

	// Get the latest worker result (wait with timeout)
	var result interface{}
	select {
	case result = <-resultsChan:
		// Successful return.  Check type of return for error.
		//
		switch r := result.(type) {
		case string:
			return r, nil, nil
		case error:
			return "", nil, r
		default:
			// Shouldn't be reachable
			return "", nil, errors.New(fmt.Sprintf("Unexpected result type: %v", result))
		}
	case <-time.After(timeout):
		// Timeout reached.  Return the timeout error.
		//
		return "", nil, errors.New(fmt.Sprintf("Timeout after time: %v", timeout))
	}
}

// Execute a remote SSH command with given parameters and returns the command output
//
// PARAMS
// cmd  command to execute (e.g. "uptime")
// PARAM: unixPriority  standard unix priority.  Allowed range -20 (highest priority) .. 19 (lowest priority).  Or use nil to use default priority.
// host hostname to execute on, preferrably an FQDN  (e.g. "stsgl01p1.integration-paytv.smf1.mobitv") -- must not be blank otherwise an error is returned
// username  login username
// password  login password
//
// RETURNS
// (1) command output(s) wrapped in a struct
// (2) error if any otherwise nil
//
// NOTES
// (1) If you need further control of the SSH client, then use the OpenSSHClient() method instead
// (2) The "hostname" parameter must not be blank otherwise an error is returned
func ExecuteSSH(sshExec SshExec, hostname string, username string, password string, unixPriority *int, cmds ...string) ([]SshOutputStruct, error) {
	// Trim given host name whitespace of extraneous leading & trailing whitespace may produce weird effects
	var actualHost = strings.TrimSpace(hostname)

	if len(actualHost) > 0 {
		var sshOutputs []SshOutputStruct
		// Connect to SSH host with defined auth parameters
		sshClient, errConnection := OpenSSHClient(sshExec, actualHost, username, password)
		if errConnection != nil {
			// Fatal - can't connect at all to execute any commands
			return nil, errors.New("SSH Client Creation fatal error.  Nested error = " + errConnection.Error())
		} else {
			defer sshClient.Close()
		}

		// Execute each of the user's commands and save the results accordingly
		//
		for _, command := range cmds {
			if output, err := sshExec.RemoteRun(sshClient, command, "", unixPriority); err != nil {
				sshOutputs = append(sshOutputs, SshOutputStruct{output, errors.New("Execute SSH fatal error.  Nested error = " + err.Error())})
			} else {
				sshOutputs = append(sshOutputs, SshOutputStruct{output, nil})
			}
		}

		return sshOutputs, nil
	} else {
		// Empty hostname may produce unexpected results so prevent this condition
		return nil, errors.New(fmt.Sprintf("SSH Exec: Cannot execute commands: '%v' because given SSH remote hostname is blank", cmds))
	}
}

// Opens a new authenticated SSH connection with the defined connection & authentication parameters.
// NOTES:
// (1) users of this method are responsible for closing the SSH connection when they are done
// with it
// (2) this method allows further use of the SSH client as code needs the connection open.  But if
//  you simply want to execute commands all at once then its best to use ExecuteSSH() instead.
//
// PARAM: sshExec    SSH runtime parameters map (timeout, optional private key)
// PARAM: host       hostname to connect to (must be accessible from current host via network and DNS name)
// PARAM: username   required regardless of password or keyfile authentication
// PARAM: password   leave blank if using SSH key auth where keyfile is specifed in SshExec configuration
//
func OpenSSHClient(sshExec SshExec, host string, username string, password string) (*ssh.Client, error) {
	// Keyboard interactive auth is 'nil' by default and is only set if the user provides a password
	var keyboardInteractiveFunc func(user, instruction string, questions []string, echos []bool) (answers []string, err error)
	if !util.IsBlank(password) {
		keyboardInteractiveFunc = func(user, instruction string, questions []string, echos []bool) (answers []string, err error) {
			answers = make([]string, len(questions))
			// The second parameter is unused
			for n, _ := range questions {
				answers[n] = password
			}

			return answers, nil
		}
	}

	// Populate connection parameters as given by user
	//
	var mapConnectionParams = map[string]interface{}{
		"host":      host,
		"user":     username,
		"password": password,
	}

	// Create the SSH connection
	sshClient, errConnection := sshExec.NewClient(
		mapConnectionParams,
		keyboardInteractiveFunc, // NOTE: keyboardInteractive can be a "nil"  if user didn't supply a password (such as for private key access auth)
	)

	if errConnection == nil {
		return sshClient, nil
	} else {
		return nil, errConnection
	}
}

// Regexp to split on new lines for either UNIX or Windows line ending markers
var splitLines = regexp.MustCompile("[\n\r]+")

// Regexp to get PID part of pgrep -a output line
var regexPgrepParse = regexp.MustCompile("([^\\s]+)\\s+")

// Connects through given SSH connection and returns a list of zero or more PIDs that match the given process pattern
//
// PARAM:  sshExec        live SSH connection context
// PARAM:  sshClient      live SSH connection context
// PARAM:  username       the username to check
// PARAM:  processPattern a pattern that matches the process to be identified (by default will search for any command with this as a substring)
// RETURN: an array of zero or more matching PID in *string* type as returned from shell command
// ERROR:  on any runtime SSH error
//
func GetExistingProcessesOnHost(sshExec SshExec, sshClient *ssh.Client, username string, processPattern string) (pids []string, err error) {
	// Use "pgrep" which quickly isolates process IDs for given username with the command line
	//
	// NOTE: pgrep match is a case-sensitive substring match (any part of string can match)
	// NOTE: Alternatively can use command: ps -alf -C <username> but requires complex parsing of multicolumn output that may be slightly different column format between various OS types
	// NOTE: the "|| true" is needed because pgrep returns error code if no results found but that is not considered an error in this case
	// NOTE: the --full option is needed otherwise pgrep only lists the first 15 characters of a process name which could cause erroneous mismatches
	const COMMAND_TEMPLATE = "pgrep -f -a -l -u %s '%s' || true"
	var sshCommand = fmt.Sprintf(COMMAND_TEMPLATE, username, processPattern)

	//
	// Execute the remote SSH command in lowest priority
	//
	result, errSsh := sshExec.RemoteRun(sshClient, sshCommand, "", &UNIX_PRIORITY_MIN)

	if errSsh != nil {
		// Fatal
		return nil, errors.New(fmt.Sprintf("Could not count processes with command '%s' because of error: %v", sshCommand, errSsh))
	} else {
		// Successful command execution
		// Now convert to string array while ignoring blank lines

		pids = []string{}

		// Add to string array but ignore any empty lines and ignore the actual pgrep command itself
		for _, line := range splitLines.Split(result, -1) {
			if util.IsNotBlank(line) {
				if !strings.Contains(line, sshCommand) {
					var matches = regexPgrepParse.FindAllStringSubmatch(line, 1)

					if matches != nil {
						pids = append(pids, strings.TrimSpace(matches[0][1]))
					}
				}
			}
		}

		return pids, nil
	}
}

// Checks the host for existing command on the given user
//
const SSH_EXISTING_COMMAND_CHECK_LIMIT = 5;
const SSH_EXISTING_COMMAND_CHECK_WAIT_SEC = 6;
func CheckOnExistingProcessOnHost(sshExec SshExec, sshClient *ssh.Client, username string, processPattern string) string {
	var errors []string

	// MULTIPLE CHECKS: Check until process is clear up to the given check count limit
	for checkCount := 1; checkCount <= SSH_EXISTING_COMMAND_CHECK_LIMIT; checkCount++ {
		pids, errPidCheck := GetExistingProcessesOnHost(sshExec, sshClient, username, processPattern)

		if errPidCheck != nil {
			errors = append(errors, fmt.Sprintf("PID check returned error: %s", errPidCheck.Error()))
			logger.Log.Info(fmt.Sprintf("Received error: '%s' while checking for existing commands.  Waiting %d secs before checking again.  Check count: #%d of %d", errPidCheck.Error(), SSH_EXISTING_COMMAND_CHECK_WAIT_SEC, checkCount, SSH_EXISTING_COMMAND_CHECK_LIMIT))
		} else if pids == nil || len(pids) > 0 {
			errors = append(errors, fmt.Sprintf("Existing PIDs are still running: %v", pids))
			logger.Log.Info(fmt.Sprintf("Existing commands with PIDs: %v were found.  Waiting %d secs before checking again.  Check count: #%d of %d", pids, SSH_EXISTING_COMMAND_CHECK_WAIT_SEC, checkCount, SSH_EXISTING_COMMAND_CHECK_LIMIT))
		} else {
			// No existing PIDs found for given command and user.  Empty string return indicates to caller to proceed
			// with their command.
			return ""
		}

		// Wait pre-defined seconds between check attempts
		time.Sleep(SSH_EXISTING_COMMAND_CHECK_WAIT_SEC * time.Second)
	}

	// Check limit exceeded.  Return final stats whcih indicates an error condition to the client.
	//
	return fmt.Sprintf("Check limit of %d exceeded with wait interval %d secs but existing command did not complete.  Log: %v", SSH_EXISTING_COMMAND_CHECK_LIMIT, SSH_EXISTING_COMMAND_CHECK_WAIT_SEC, errors)
}

// Used for outputs for multi command execution
//
type SshOutputStruct struct {
	Output string
	Err    error
}


// Connects through given SSH connection and hard kill (kill -9) the processes identified by the given PIDs.
//
// NOTE: Use extra caution with this command and ensure PIDs are correctly matching expected procsses to be killed.
//
// PARAM:  sshExec    live SSH connection context
// PARAM:  sshClient  live SSH connection context
// PARAM:  pids       1 or more PIDs to kill
// RETURN: warnings   0 or more warnings associated with kill process
//
func KillProcessesOnHost(sshExec SshExec, sshClient *ssh.Client, pids... string) (warnings []string) {
	const DEFAULT_TIMEOUT = 10 * time.Second
	const CMD_TEMPLATE = "kill -9 %s"

	if pids != nil {
		for _, pid := range pids {
			var command = fmt.Sprintf(CMD_TEMPLATE, pid)

			//
			// Remote execute kill command on the PID.  Result not needed but will track errors as warnings.
			//
			_, errSsh, errTimeout := sshExec.RemoteRunWithTimeout(sshClient, command, "", DEFAULT_TIMEOUT, nil)

			if errSsh != nil {
				// Not fatal but add to warnings
				warnings = append(warnings, fmt.Sprintf("Error killing PID %s because of error: %v", pid, errSsh.Error()))
			}
			if errTimeout != nil {
				// Not fatal but add to warnings
				warnings = append(warnings, fmt.Sprintf("Error killing PID %s because of timeout: %v", pid, errTimeout.Error()))
			}
		}
	}

	return
}

// KillProcesses on SSH host for given username with PIDs matching the given command string
//
// NOTE: may or may not kill any processes if no matching command for given username is found at the time.
// NOTE: "kill deferral" allows waiting a set number of times (with fixed delay) to give any matching processes the
//       opportunity to terminate normally (recommend 3+ times).  Otherwise, use value of 1 to kill immediately.
//
// PARAM: username       username on host (only processes owned by this username will be killed)
// PARAM: command        a portion of the command string to match. IMPORTANT: do *not* include prefix commands like 'nice' or 'set', etc as they will *never* match!
// PARAM: killDeferLimit number of times to check before actually performing kill command and allows a "grace" period for process to complete normally (must be 1 or greater)
//
func KillProcessesOnHostByCommand(sshExec SshExec, sshClient *ssh.Client, username string, command string, killDeferLimit int) (warnings []string) {
	// Ensure defer limit always at least 1
	killDeferLimit = util.Max(1, killDeferLimit)

	for checkCount := 0; checkCount <= killDeferLimit; checkCount++ {
		if pids, errGetPids := GetExistingProcessesOnHost(sshExec, sshClient, username, command); errGetPids != nil {
			// FATAL - lookup failed for some reason
			warnings = append(warnings, errGetPids.Error())

			return
		} else {
			if pids != nil && len(pids) > 0 {
				if checkCount >= killDeferLimit {
					// Check limit reached.  Waited long enough so now process must be killed.
					var killWarnings = KillProcessesOnHost(sshExec, sshClient, pids...)
					logger.Log.Info(fmt.Sprintf("Killed PIDs %v matching command string '%s'", pids, command))
					warnings = append(warnings, killWarnings...)

					return
				} else {
					// Check limit not reached.  Wait a moment and check again which gives opportunity for process to
					// terminate normally without a hard kill (preferred).
					logger.Log.Info(fmt.Sprintf("Found running PIDs %v for command name %s but limit %d not reached yet.  Will recheck.", pids, command, killDeferLimit))
					time.Sleep(KILL_DEFER_WAIT_SECS)
				}
			} else {
				// No matching PIDs found.  Do not continue checking.
				return
			}
		}
	}

	return
}