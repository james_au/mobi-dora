package syncrefreshlock

import (
	"fmt"
	"github.com/google/uuid"
	"sync"
)

// Define a locked map to contain runtime locking table implemented internally as a map
//
type SyncLockMap struct {
	mutex    sync.Mutex    // Access to methods are locked to avoid possible race conditions between multiple asynchronous go routines
	mapLock  map[string]*SyncLockStruct
}

// Creates a new blank sync lock map instance
//
func New() *SyncLockMap {
	return &SyncLockMap{
		mutex:  sync.Mutex{},
		mapLock: make(map[string]*SyncLockStruct),
	}
}

// Adds a lock for the given environment
//
// PARAM:  environmentName     name of the environment to lock
// PARAM:  username            name of the user
func (o *SyncLockMap) AddLock(environmentName string, username string) *SyncLockStruct {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	var syncLock = &SyncLockStruct{
		EnvironmentName: environmentName,
		Username: username,
		UUID: uuid.New(), // UUID ensure's lock is unique because the same user can initiate more than one sync
	}

	o.mapLock[environmentName] = syncLock

	return syncLock
}

// Delete the lock when complete otherwise other processes can never sync the environment
//
// PARAM: environmentName    name of the environment for the original lock
// RETURN:    true   if found and deleted and false otherwise
func (o *SyncLockMap) DeleteLock(environmentName string) bool {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	if _, keyExists := o.mapLock[environmentName]; keyExists {
		delete(o.mapLock, environmentName)

		return true
	} else {
		return false
	}
}

// Determines if the lock referenced by ID exists in the map
//
// RETURN:   reference to lock if it exists and nil otherwise
//
func (o *SyncLockMap) Exists(ID string) *SyncLockStruct {
	defer o.mutex.Unlock()
	o.mutex.Lock()

	if lock, keyExists := o.mapLock[ID]; keyExists {
		return lock
	} else {
		return nil
	}
}

// Tuple for actual sync lock record
type SyncLockStruct struct {
	EnvironmentName string
	Username        string
	UUID            uuid.UUID
}

// Generates the unique ID of the lock record which is a composite of its keys
//
// RETURN:  string form of the lock record's ID
func (o *SyncLockStruct) GenerateID() string {
	return fmt.Sprintf("%s-%s-%s", o.EnvironmentName, o.Username, o.UUID.String())
}