#!/bin/bash
########################################################################################
# This script generates the stub code for
# (1) the GoLang server-side library
# (2) the Javascript client-side library
# (3) the Envoy .pb definition format for JSON transcoding
########################################################################################

# Change working directory to script's location otherwise script fails.  Will be restored to
# user's original directory at the end of the script.
pushd "$(dirname "$0")"

#
# Run code generation.  NOTE: Both "protoc" and "protoc-gen-go" must be on the shell's PATH.
#

CLIENT_OUTPUT=../client

# Generate server GoLang library code
protoc api/dora.proto --go_out=plugins=grpc:../pkg

# Generate client-side JavaScript stub code for Javascript to process gRPC protocol
protoc -I=. \
       --proto_path=google/api/ \
       --js_out=import_style=commonjs:${CLIENT_OUTPUT} \
       --grpc-web_out=import_style=commonjs,mode=grpcwebtext:${CLIENT_OUTPUT} \
       api/dora.proto

# Fix output reference to the annotations is incorrect by one level in both the
# dora_grpc_web.pb.js and the dora_pb.js generated output files
sed -i -e 's|../google/api/annotations_pb.js|./google/api/annotations_pb.js|' $CLIENT_OUTPUT/api/dora_grpc_web_pb.js $CLIENT_OUTPUT/api/dora_pb.js

# Generate Envoy definition
protoc -I=. --include_imports --include_source_info --descriptor_set_out=../envoy/dora_service_definition.pb api/dora.proto

# Return to original directory
popd