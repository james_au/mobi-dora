#!/bin/bash

JS=../client

protoc -I=. --js_out=import_style=commonjs,binary:$JS/api google/api/annotations.proto google/api/http.proto

#sed -i '.old' 's/\.\/google\/api\/http_pb\.js/\.\/http_pb\.js/g' $JS/google/apiannotations_pb.js

#rm $JS/google/apiannotations_pb.js.old
