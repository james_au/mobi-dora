#!/usr/bin/python
#
#
# Starts a Python HTTP web server on given port.  This is used only for
# test purposes
#
# $ ./flask_server.py
#   * Serving Flask app "flask_server" (lazy loading)
#   * Environment: production
#     WARNING: This is a development server. Do not use it in a production deployment.
#     Use a production WSGI server instead.
#   * Debug mode: off
#   * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
#

from flask import Flask, request, send_from_directory
from flask_cors import CORS

# Default server listening TCP port
DEFAULT_PORT = 5000

# set the project root directory as the static folder, you can set others.
# app = Flask(__name__, static_url_path='')
app = Flask(__name__, '')
CORS(app, resources={r"/api/*": {"origins": "*"}})

@app.route('/html/<path:path>')
def send_js(path):
	return send_from_directory('./', path)

if __name__ == "__main__":
    app.run("0.0.0.0", DEFAULT_PORT)