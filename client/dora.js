const grpc = {};
grpc.web = require('grpc-web');
const doraService = require('./api/dora_grpc_web_pb.js');

//
// Usage:
// This manually editted file provides an interface for frontend Javascript code to
// access the grpc-web and webpacked Javascript.
//
// Makefile:
// Any changes to the proto definitions will likely require a full proto regen and
// webpack which can be performed via the project's Makefile
//
module.exports = {
    connectGrpcServer: function(httpUrl) {
        return new doraService.DoraClient(httpUrl, null, null);
    },

    login: function(doraService, username, password, callback) {
        var loginRequest = new proto.api.LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);

        var metadata = {};
        doraService.getLoginToken(loginRequest, metadata, callback);
    },

    keepAlive: function(doraService, token, callback) {
        var keepAliveRequest = new proto.api.KeepAliveRequest();
        keepAliveRequest.setAuthToken(token);

        var metadata = {};
        doraService.keepAlive(keepAliveRequest, metadata, callback);
    },

    logout: function(doraService, token, callback) {
        var logoutRequest = new proto.api.LogoutRequest();
        logoutRequest.setAuthToken(token);

        var metadata = {};
        doraService.logout(logoutRequest, metadata, callback);
    },

    getHypervisors: function(doraService, authToken, vcenterName, pathRegExp, hostRegExp, callback) {
        var hvQueryRequest = new proto.api.HVQueryRequest();
        hvQueryRequest.setAuthToken(authToken);
        hvQueryRequest.setVcenterName(vcenterName);
        hvQueryRequest.setPathFilter(pathRegExp);
        hvQueryRequest.setHostFilter(hostRegExp);

        return doraService.getHypervisors(hvQueryRequest, {}, callback);
    },

    // Streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getHypervisorsStreamed: function(doraService, authToken, vcenterName, pathRegExp, hostRegExp, listingOnly) {
        var hvQueryRequest = new proto.api.HVQueryRequest();
        hvQueryRequest.setAuthToken(authToken);
        hvQueryRequest.setVcenterName(vcenterName);
        hvQueryRequest.setPathFilter(pathRegExp);
        hvQueryRequest.setHostFilter(hostRegExp);
        hvQueryRequest.setListingOnly(listingOnly);

        return doraService.getHypervisorsStreamed(hvQueryRequest, {});
    },

    // Get Hypervisor Loadout (used start environment Balance operations)
    getHypervisorLoadOutList: function(doraService, authToken, vcenterName, pathRegExp, rulesText, callback) {
        var hypervisorLoadOutRequest = new proto.api.HypervisorLoadOutRequest();
        hypervisorLoadOutRequest.setAuthToken(authToken);
        hypervisorLoadOutRequest.setVcenterName(vcenterName);
        hypervisorLoadOutRequest.setPathRegexp(pathRegExp);
        hypervisorLoadOutRequest.setRulesText(rulesText);

        return doraService.getHypervisorLoadOutList(hypervisorLoadOutRequest, {}, callback);
    },

    // Performs the balance execution operation with new "proposal" and "unplaced"
    balanceExec: function(doraService, hypervisorLoadOutDTO, callback) {
        return doraService.balanceExec(hypervisorLoadOutDTO, {}, callback);
    },

    // Performs the balance "check"" operation.  Will read the "proposal" and "unplaced" lists but will not update them
    // Client code UI can view the "rules violations" on each hypervisor and get updated CPU/MEM counts also.
    balanceCheck: function(doraService, hypervisorLoadOutDTO, callback) {
        return doraService.balanceCheck(hypervisorLoadOutDTO, {}, callback);
    },

    // Performs force VM distribute with updated rules
    balanceForceDistribute: function(doraService, hypervisorLoadOutDTO, callback) {
        return doraService.balanceForceDistribute(hypervisorLoadOutDTO, {}, callback);
    },

    // Streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getVirtualMachinesStreamed: function(doraService, authToken, vcenterName, pathRegexp, hostRegexp, listingOnly) {
        var vmQueryRequest = new proto.api.VMQueryRequest();
        vmQueryRequest.setAuthToken(authToken);
        vmQueryRequest.setVcenterName(vcenterName);
        vmQueryRequest.setPathFilter(pathRegexp);
        vmQueryRequest.setHostFilter(hostRegexp);
        vmQueryRequest.setListingOnly(listingOnly);

        return doraService.getVirtualMachinesStreamed(vmQueryRequest, {});
    },

    // Streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getRoomStreamed: function(doraService, authToken, connectionName, searchPattern) {
        var queryRequest = new proto.api.QueryRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setConnectionName(connectionName);
        queryRequest.setSearchPattern(searchPattern);

        return doraService.getRoomStreamed(queryRequest, {});
    },

    // Gets the Shared Storage report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getStorageCombinedReportStreamed: function(doraService, authToken, vcenterName, datastoreName, hypervisorName, vmName) {
        var queryRequest = new proto.api.StorageCombinedReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setDatastoreNameFilter(datastoreName);
        queryRequest.setHypervisorNameFilter(hypervisorName);
        queryRequest.setVmNameFilter(vmName);

        return doraService.getStorageCombinedReportStreamed(queryRequest, {});
    },

    // Gets the Shared Storage report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getSharedStorageReportStreamed: function(doraService, authToken, vcenterName, storageType, storageName) {
        var queryRequest = new proto.api.SharedStorageReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setStorageType(storageType);
        queryRequest.setStorageName(storageName);

        return doraService.getSharedStorageReportStreamed(queryRequest, {});
    },

    // Gets th datastore report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getDatastoreReportStreamed: function(doraService, authToken, vcenterName, hypervisorPathFilter, datastorePathFilter) {
        var queryRequest = new proto.api.DatastoreReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setHypervisorPathFilter(hypervisorPathFilter);
        queryRequest.setDatastorePathFilter(datastorePathFilter);

        return doraService.getDatastoreReportStreamed(queryRequest, {});
    },

    // Gets the NSF report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getNfsReportStreamed: function(doraService, authToken, vcenterName, nfsHost, nfsPath, vmHost, vmPath, vmMountPoint) {
        var queryRequest = new proto.api.NfsReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setNfsHost(nfsHost);
        queryRequest.setNfsPath(nfsPath);
        queryRequest.setVmHost(vmHost);
        queryRequest.setVmPath(vmPath);
        queryRequest.setVmMountPoint(vmMountPoint);

        return doraService.getNfsReportStreamed(queryRequest, {});
    },

    // Gets the VM Storage report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getVmStorageReportStreamed: function(doraService, authToken, vcenterName) {
        const queryRequest = new proto.api.VmStorageReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);

        return doraService.getVmStorageReportStreamed(queryRequest, {});
    },

    // Uploads VM Storage records to the backend datastore (SQLite)
    uploadVmStorageData: function(doraService, authToken, vcenterName, records, appendMode = false, callback) {
        const queryRequest = new proto.api.VmStorageUploadRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setRecordsList(records);
        queryRequest.setAppendMode(appendMode)

        return doraService.uploadVmStorageData(queryRequest, {}, callback);
    },

    // Gets the IP Range report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getIpRangeReportStreamed: function(doraService, authToken, vcenterName) {
        const queryRequest = new proto.api.IpRangeReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);

        return doraService.getIpRangeReportStreamed(queryRequest, {});
    },

    // Uploads IP Range records to the backend datastore (SQLite)
    uploadIpRangeData: function(doraService, authToken, vcenterName, records, appendMode = false, callback) {
        const queryRequest = new proto.api.IpRangeUploadRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setRecordsList(records);
        queryRequest.setAppendMode(appendMode)

        return doraService.uploadIpRangeData(queryRequest, {}, callback);
    },

    // Gets the Storage Summary report in streaming mode.  See for more details: https://github.com/grpc/grpc-web
    getStorageSummaryReportStreamed: function(doraService, authToken, vcenterName) {
        const queryRequest = new proto.api.StorageSummaryReportRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);

        return doraService.getStorageSummaryReportStreamed(queryRequest, {});
    },

    // Uploads Storage Summary records to the backend datastore (SQLite)
    uploadStorageSummaryData: function(doraService, authToken, vcenterName, records, appendMode = false, callback) {
        const queryRequest = new proto.api.StorageSummaryUploadRequest();
        queryRequest.setAuthToken(authToken);
        queryRequest.setVcenterName(vcenterName);
        queryRequest.setRecordsList(records);
        queryRequest.setAppendMode(appendMode)

        return doraService.uploadStorageSummaryData(queryRequest, {}, callback);
    },

    // Start background run of VCenter SQLite synchronization on the specified environment.  An "ID" of the sync job is
    // returned right away while the process continues on the server asynchronously.
    startVCenterSync: function(doraService, authToken, environmentName, callback) {
        var resyncRequest = new proto.api.ResyncVCenterRequest();
        resyncRequest.setAuthToken(authToken);
        resyncRequest.setEnvironmentName(environmentName);

        return doraService.resyncVCenter(resyncRequest, {}, callback);
    },

    // Start background run of NFS synchronization on the specified environment.  An "ID" of the sync job is
    // returned right away while the process continues on the server asynchronously.
    startNfsSync: function(doraService, authToken, environmentName, callback) {
        var resyncRequest = new proto.api.ResyncNfsRequest();
        resyncRequest.setAuthToken(authToken);
        resyncRequest.setEnvironmentName(environmentName);

        return doraService.resyncNfs(resyncRequest, {}, callback);
    },

    // Gets the last 'n' number of most recent sync history records for the given environment
    getVCenterSyncHistory: function(doraService, authToken, environmentName, numRecords, callback) {
        var syncHistoryRequest = new proto.api.SyncHistoryRequest();
        syncHistoryRequest.setAuthToken(authToken);
        syncHistoryRequest.setEnvironmentName(environmentName);
        syncHistoryRequest.setNumRecords(numRecords);

        return doraService.getVCenterSyncHistory(syncHistoryRequest, {}, callback);
    },

    // Gets the last 'n' number of most recent NFS sync history records for the given environment
    getNfsSyncHistory: function(doraService, authToken, environmentName, numRecords, callback) {
        var syncHistoryRequest = new proto.api.SyncHistoryRequest();
        syncHistoryRequest.setAuthToken(authToken);
        syncHistoryRequest.setEnvironmentName(environmentName);
        syncHistoryRequest.setNumRecords(numRecords);

        return doraService.getNfsSyncHistory(syncHistoryRequest, {}, callback);
    },

    // Gets all the sync log entries for the referenced parent SyncHistory rec for the given environment
    getVCenterLogEntries: function(doraService, authToken, environmentName, parentSyncHistoryId, callback) {
        var syncLogEntryRequest = new proto.api.SyncLogRecRequest();
        syncLogEntryRequest.setAuthToken(authToken);
        syncLogEntryRequest.setEnvironmentName(environmentName);
        syncLogEntryRequest.setParentSyncHistoryId(parentSyncHistoryId);

        return doraService.getVCenterSyncLogs(syncLogEntryRequest, {}, callback);
    },

    // Gets all the NFS sync log entries for the referenced parent SyncHistory rec for the given environment
    getNfsLogEntries: function(doraService, authToken, environmentName, parentSyncHistoryId, callback) {
        var syncLogEntryRequest = new proto.api.SyncLogRecRequest();
        syncLogEntryRequest.setAuthToken(authToken);
        syncLogEntryRequest.setEnvironmentName(environmentName);
        syncLogEntryRequest.setParentSyncHistoryId(parentSyncHistoryId);

        return doraService.getNfsSyncLogs(syncLogEntryRequest, {}, callback);
    },

    // Gets VCenter sync cron runtime information (next run, cron expression) for given environment
    getCronVCenterInfo: function(doraService, environmentName, callback) {
        var cronInfoRequest = new proto.api.CronVCenterRequest();
        cronInfoRequest.setEnvironmentName(environmentName);

        return doraService.getCronVCenterInfo(cronInfoRequest, {}, callback);
    },

    // Sets the cron enabled or disabled for the VCenter referenced by tbe environment name
    setCronVCenterEnabled: function(doraService, authToken, environmentName, enabled, callback) {
        var cronEnabledRequest = new proto.api.CronEnabledRequest();
        cronEnabledRequest.setAuthToken(authToken);
        cronEnabledRequest.setEnvironmentName(environmentName);
        cronEnabledRequest.setEnabled(enabled);

        return doraService.setCronVCenterEnabled(cronEnabledRequest, {}, callback);
    },

    // Gets NFS sync cron runtime information (next run, cron expression) for given environment
    getCronNfsInfo: function(doraService, environmentName, callback) {
        var cronInfoRequest = new proto.api.CronNfsRequest();
        cronInfoRequest.setEnvironmentName(environmentName);

        return doraService.getCronNfsInfo(cronInfoRequest, {}, callback);
    },

    // Sets the cron enabled or disabled for the NFS environment referenced by tbe environment name
    setCronNfsEnabled: function(doraService, authToken, environmentName, enabled, callback) {
        var cronEnabledRequest = new proto.api.CronEnabledRequest();
        cronEnabledRequest.setAuthToken(authToken);
        cronEnabledRequest.setEnvironmentName(environmentName);
        cronEnabledRequest.setEnabled(enabled);

        return doraService.setCronNfsEnabled(cronEnabledRequest, {}, callback);
    },

    // Gets the environment label as configured on the backend
    getAppEnvironmentLabel: function(doraService, callback) {
        return doraService.getAppEnvironmentLabel(new proto.google.protobuf.Empty(), {}, callback);
    },

    ready: function(doraService, callback) {
        var metadata = {};
        return doraService.ready(new proto.google.protobuf.Empty(), metadata, callback)
    },

    echo: function(doraService, msg, callback) {
        var metadata = {};
        var echoRequest = new proto.api.EchoStruct();
        echoRequest.setMessage(msg);

        return doraService.echo(echoRequest, metadata, callback);
    },

    getVSphereEnvironments: function(doraService, callback) {
        return doraService.getVsphereEnvironments(new proto.google.protobuf.Empty(), {}, callback);
    },

    getForemanEnvironments: function(doraService, callback) {
        return doraService.getForemanEnvironments(new proto.google.protobuf.Empty(), {}, callback);
    },

    vmMove: function(doraService, vmMoveRequest, callback) {
        return doraService.vmMove(vmMoveRequest, {}, callback);
    },

    vmPowerOn: function(doraService, authToken, vcenterName, vmPath, callback) {
        const vmPowerChangeRequest = new proto.api.VmPowerChangeRequest();
        vmPowerChangeRequest.setAuthToken(authToken);
        vmPowerChangeRequest.setVcenterName(vcenterName);
        vmPowerChangeRequest.setVmPath(vmPath);

        return doraService.vmPowerOn(vmPowerChangeRequest, {}, callback);
    },

    vmPowerOff: function(doraService, authToken, vcenterName, vmPath, callback) {
        const vmPowerChangeRequest = new proto.api.VmPowerChangeRequest();
        vmPowerChangeRequest.setAuthToken(authToken);
        vmPowerChangeRequest.setVcenterName(vcenterName);
        vmPowerChangeRequest.setVmPath(vmPath);

        return doraService.vmPowerOff(vmPowerChangeRequest, {}, callback);
    },

    balanceInvalidCheck: function(doraService, vcenterName, callback) {
        const balanceLockCheckRequest = new proto.api.BalanceInvalidCheckRequest();
        balanceLockCheckRequest.setVcenterName(vcenterName);

        return doraService.balanceInvalidCheck(balanceLockCheckRequest, {}, callback);
    },

    balanceLock: function(doraService, balanceLockRequest, callback) {
        return doraService.balanceLock(balanceLockRequest, {}, callback);
    },

    balanceUnlock: function(doraService, balanceUnlockRequest, callback) {
        return doraService.balanceUnlock(balanceUnlockRequest, {}, callback);
    },
};
