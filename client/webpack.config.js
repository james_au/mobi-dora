// * USAGE: *
// Run command "npx webpack" to generate all javascript/html resources
//
// * DEPENDENCIES: *
// Requires the "node_modules" library cache to be updated with all require dependencies.  The npx tool
// should run npm as necessary.  But if the webpack operation fails, you can try running  "npm install"
// manually once before running the webpack operation again.
//
//
module.exports = {
  mode: "production",
  entry: "./dora.js",
  output: {
    // filename: "dora-main.js",
    library: "dora",
    path: __dirname,
    filename: 'js_generated/doralib.js',
    // libraryTarget: "window",
    // libraryExport: "default"
  },
  optimization: {
    minimize: false    // Dev environment disable to allow easier debugging in browser
  },
};
