//
// This library helps handle Dora cookie based auth tokens
//


// Successful login will set auth token with this method
//
function setAuthToken(authToken) {
    _setCookie("authToken", authToken);
}

// Runtime pages that need auth will call this method
function getAuthToken() {
    return _getCookie("authToken");
}

// Use this to complete logout
function clearAuthToken() {
    _setCookie("authToken", "");
}

// Private method.  Should not use directly.
function _setCookie(cname, cvalue, exdays=365) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Private method. Should not use directly.
function _getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}