var mobiDoraDeepLink;

mobiDoraDeepLink = {
    // Class provides utilities and data to keep Dora's SPA state in-line with URL state which then provides "deep
    // linking" capability for Dora.
    //
    DeepLinkState: class {
        constructor() {
            // Stores the last URI for the tab so it can be restored if the user clicks between tabs w/o a form update
            this.mapLastUri = new Map();
        }

        // Changes window state to allow deep linking
        //
        // Ref: https://smartlazycoding.com/javascript-tutorial/change-the-url-without-reloading-the-page
        // Ref: https://stackoverflow.com/questions/21426140/getting-backbutton-to-work-in-single-page-website-and-implementing-speaking-ur
        //
        // Param1: tabId - anchor ID of the tab
        // Param2: query - any query part of the URL (search parameters, etc). Defaults to none which will not include any params.
        deepLinkUpdate(tabId, mapQueryValues = null) {
            if (this.mapLastUri.has(tabId)) {
                // A previous URI is available so just restore it to the browser location
                //
                let lastUri = this.mapLastUri.get(tabId);
                console.info(`Tab switch.  Restoring tab URL: ${lastUri}`)
                window.history.pushState(new Date(), `Dora: ${tabId}`, lastUri);
                return lastUri;
            } else {
                // No previous URI exists for this tab or it was cleared out.  So regenerate a new location from the given
                // map of parameter values.
                //

                // May need to use the current window location to compute new location
                console.info(`Current window location: ${window.location}`);

                var paramsList = [];
                if (mapQueryValues !== null) {
                    for (let [key, value] of mapQueryValues) {
                        paramsList.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
                    }
                }
                var paramsString = (paramsList.length > 0) ? "?" + paramsList.join("&") : "";

                let uri = `${currentUrlNoParams()}${paramsString}#${tabId}`;
                window.history.pushState(new Date(), `Dora: ${tabId}`, uri);
                return uri
            }
        }

        // Set the URI to preserve the URI for restore if the user clicks between taabs
        setTabLastUri(tabId, uri) {
            this.mapLastUri.set(tabId, uri);
        }

        // Deletes the URI for a tab if the user has submitted a new form entry which will replace the existing
        // tab's URI
        deleteTabLastUri(tabId) {
            this.mapLastUri.delete(tabId);
        }

        // Gets the tab's last URI stored which allows restoring the tab's URI if the user clicks through the tab's GUI
        getTabLastUri(tabId) {
            return this.mapLastUri.get(tabId);
        }
    }
}
