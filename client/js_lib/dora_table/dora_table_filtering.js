if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

mobiDoraTable.TAG_ATTRIB_BOOLEAN_COMBOBOX = "DORA_TABLE_BOOLEAN_COMBOBOX";
mobiDoraTable.TAG_ATTRIB_COMPARATOR = "FILTER_COMBOBOX_COMPARATOR";

/**
 * This encapsulate the GUI, data, and control logic for the filter drop down box.
 *
 * @see  {getElementRef()}  for the root HTML element
 * @type {mobiDoraTable.FilterDropdownBox}
 */
mobiDoraTable.FilterDropdownBox = class {
    /**
     * Sole constructor
     *
     * @param tableView  reference to the parent table view
     */
    constructor(tableView) {
        this.tableView = tableView

        // Create and style the DIV used as the root UI element
        this.element = document.createElement("div");
        this.element.className = "dropDownMenu1";
        this.element.tabIndex = -1;  // Required in order ot have key listener on DIV
        this.element.style.visibility = "hidden";
        this.element.style.alignContent = "left";
        this.element.style.textAlign = "left";
        this.element.style.height = "150px";
        this.element.style.width = "475";
        this.element.addEventListener("keydown", function(event) {
           if (event.key === "Escape") {
               outerThis.element.style.visibility = "hidden";
           }
        });

        // Will store filter row records containing UI and data element neatly stored in array for
        // convenient & fast lookup later
        this.filterRows = new Array();

        // Allows anonymous function access
        const outerThis = this;

        // Create button for applying the filter
        this.btnApply = document.createElement("button");
        this.btnApply.innerHTML = "Apply";
        this.btnApply.style.fontSize = "10pt";
        this.btnApply.style.float = "right";
        this.btnApply.style.marginTop = "6pt";
        this.btnApply.addEventListener("click", function(event) {
            // Only need to make visible the filter span element because downstream code will detect this and
            // perform filtering if
            outerThis.tableView.spanFilteredOut.style.visibility = "visible";

            // Apply pivoting only if pivoting was previously active/visible.  This will also run the row filtering
            // if pivoting is run.
            const pivotingApplied = outerThis.tableView.groupingDropdownBox.applyPivotIfVisible(true);

            // If pivoting wasn't applied then apply filtering directly
            if (!pivotingApplied) {
                outerThis.applyFilter();
            }
        });

        const popInternalTable = document.createElement("table");
        popInternalTable.setAttribute("border", "0");
        popInternalTable.setAttribute("width", "100%");
        this.filterTableBody = popInternalTable.createTBody();
        this.getElementRef().appendChild(this.createFilterAddRow());
        this.getElementRef().appendChild(this.createFilterCloseIcon());
        this.getElementRef().appendChild(popInternalTable);
        this.getElementRef().appendChild(this.btnApply);
    }

    /**
     * Executes and applies the filter results to the UI.
     *
     * This should be called by the high-level button user event to apply user's filtering logic
     *
     * @see applyFilterIfVisible  if needing to refresh the current filter view only if filter is currently active
     */
    applyFilter() {
        console.info("Applying table filter...");

        // Reset the table view to full unfiltered rows since the subsequent filtering always uses the
        // *current* rows as input.
        this.tableView.restoreOriginalRows();

        // Execute filter logic on the current table rows and get results for display
        const filterResults = this.executeFilterLogic();

        // Apply the matching rows to the table view.  This only affects the UI view while the underlying table
        // model and original rows stay the same
        this.tableView.setRows(filterResults.matchingRows);

        const outerThis = this;

        // Hide & setup other UI elements accordingly post-filter operation
        // NOTE: Firefox browser needs a short setTimeout() delay otherwise otherwise the dialog re-appears on its
        // its own some reason
        setTimeout(function() {
            outerThis.element.style.visibility = "hidden";
            outerThis.tableView.spanFilteredOut.innerHTML = `<span style="opacity: 50%">x</span> &nbsp; showing ${filterResults.matchingRows.length} rows (${filterResults.nonMatchingRows.length} rows filtered out)`;
            outerThis.tableView.spanFilteredOut.style.visibility = "visible";
        }, 128);
    }

    /**
     * Applies the filter logic only if filtering is currently active.
     *
     * This method should be preferred if the page is <i>refreshing</i> itself for the same view and needs to re-apply
     * the same filters but only if filtering is currently active.  Because there could be entries in the UI filtering
     * dialog box but filtering is not active and those entries should not be in effect.
     *
     * @see applyFilter   to force applying the filter regardless if filtering is currently enabled.
     * @return {boolean}  <code>true</code> if filtering was re-applied and <code>false</code> otherwise
     */
    applyFilterIfVisible() {
        if (this.tableView.isFilteringActive()) {
            this.applyFilter();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Executes the current filtering logic to the table view.
     *
     * NOTE: this method does <b>not</b> update the UI.  Use {@see applyFilter()} to actually execute and update the UI
     * with latest filtering results.
     *
     * @returns {mobiDoraTable.FilterResults}  object containing matching rows and non-matching rows
     * @see applyFilter
     */
    executeFilterLogic() {
        const matchingRows = new Array();
        const nonMatchingRows = new Array();
        const filterValues = this.getFilterValues();

        // Iterate through each HTML table row and filter in/out as determined by the filter value
        for (const tr of this.tableView.tableBody.children) {
            const booleanResults = new Array();

            for (const filterValue of filterValues) {
                const tdMatching = searchForDomElements(tr, function(element) {
                    return element.tagName === "TD" &&
                        typeof(element.getAttribute) === "function" &&
                        element.getAttribute(DORA_TABLE_CELL_COLUMN_NAME) === filterValue.getFieldName();
                });

                if (!isArrayEmpty(tdMatching)) {
                    // Only one element is returned upon match and it contains the lOperand
                    const lOperand = tdMatching[0].innerText;

                    booleanResults.push(new mobiDoraTable.BooleanEntry(filterValue.getBooleanOperator(), filterValue.getComparator().evaluate(lOperand, trimToBlank(filterValue.getArgument()))));
                }
            }

            let currentResult = true;
            for (const boolEntry of booleanResults) {
                currentResult = boolEntry.boolOperator.evaluate(currentResult, boolEntry.value);
            }

            if (currentResult) {
                matchingRows.push(tr);
            } else {
                nonMatchingRows.push(tr);
            }
        }

        return new mobiDoraTable.FilterResults(matchingRows, nonMatchingRows);
    }

    /**
     * Adds a new blank/default filter row
     *
     * @param isFirstRow set to <code>true</code> if this is the first row and <code>false</code> otherwise
     */
    addFilterRow(isFirstRow) {
        const rowView = new mobiDoraTable.FilterRowView(this.tableView);

        if (isFirstRow) {
            rowView.disableBooleanOperator();
        }

        // Add to internal array
        this.filterRows.push(rowView);

        // Add to UI
        this.filterTableBody.appendChild(rowView.getTrRef());

        // Adjust the dialog height to accommodate the new row as necessary
        this.adjustDialogHeight();
    }

    renderFilterRows() {
        removeAllChildNodes(this.filterTableBody);

        let i = 0;
        for (const filterRow of this.filterRows) {
            if (i === 0) {
                filterRow.disableBooleanOperator();
            }

            this.filterTableBody.appendChild(filterRow.getTrRef());

            i++;
        }

        // Adjust the dialog height to accommodate the row removal as necessary
        this.adjustDialogHeight();
    }

    /**
     * Removes the referenced filter row
     *
     * @param trRef  a reference to the DOM TR row to remove
     */
    removeFilterRow(trRef) {
        // Remove row from internal array by row with the matching trRef
        _.remove(this.filterRows, function(ele) {
            // Match on TR reference ID
            return ele.getTrRef() === trRef;
        });

        // Remove row from UI
        trRef.remove();

        // Adjust the dialog height to accommodate the row removal as necessary
        this.adjustDialogHeight();

        // Re-render filter rows
        this.renderFilterRows();
    }

    /**
     * Returns current count of filter rows
     *
     * @returns {number}  current count of filter rows
     */
    getFilterRowCount() {
        return this.filterTableBody.childElementCount - 1;
    }

    /**
     * Creates and return an array of the current logical {@link FilterRowValues} records extracted from the UI.  This
     * method should be called once at the start of filter execution.
     *
     * @returns {FilterRowValues[]}
     */
    getFilterValues() {
        const filterRows = new Array();

        // Iterate over internal list and extract actual combobox values from the UI
        for (const filterRowViewRec of this.filterRows) {
            const boolOperator = filterRowViewRec.getBooleanOperatorComboBoxSelectedLogic();
            const fieldName = filterRowViewRec.getFieldNameValue();
            const comparator = filterRowViewRec.getComparatorComboBoxSelectedLogic();
            const argument = filterRowViewRec.getArgumentTextBox().value;

            filterRows.push(new mobiDoraTable.FilterRowValues(
                boolOperator,
                fieldName,
                comparator,
                argument
            ));
        }

        return filterRows;
    }

    /**
     * Adjust the dialog's height based on the current count of filter rows
     */
    adjustDialogHeight() {
        const tableHeight = this.filterTableBody.offsetHeight;
        const buttonHeight = this.btnApply.offsetHeight;

        this.getElementRef().style.height = (25 + buttonHeight +  tableHeight) + "px";
    }

    /**
     * Use this to get reference to the DIV element that is the parent HTML object of this dialog box.  This will
     * need to be placed into the page's DOM at least once at some point.
     *
     * @returns {HTMLDivElement}  root element of this dialog
     */
    getElementRef() {
        return this.element;
    }

    /**
     * Creates the "+ Add row" link
     *
     * @returns {HTMLSpanElement}  the "+ Add row" field ready for insertion into the DOM
     */
    createFilterAddRow() {
        const outerThis = this;
        const ele = document.createElement("span");

        ele.innerHTML = "+ Add row";
        ele.style.textAlign = "left";
        ele.style.fontSize = "7pt";
        ele.style.paddingRight = "200pt";

        ele.addEventListener("mouseover", function (event) {
            ele.style.cursor = "pointer";
        });
        ele.addEventListener("click", function(event) {
            console.info("Add more clicked");
            outerThis.addFilterRow(isArrayEmpty(outerThis.filterRows));
        });

        return ele
    }

    /**
     * Creates the fully formatted and event enabled close icon for the dialog
     *
     * @returns {HTMLSpanElement}
     */
    createFilterCloseIcon() {
        const outerThis = this;
        const spanCloseIcon = document.createElement("span");

        spanCloseIcon.innerHTML = "&#x2715";
        spanCloseIcon.style.float = "right";
        spanCloseIcon.style.border = "1px solid rgba(0,0,0,0.5)";
        spanCloseIcon.style.fontSize = "133%";
        spanCloseIcon.style.color = "gray";
        spanCloseIcon.addEventListener("mouseover", function(event) {
            spanCloseIcon.style.color = "black";
        });
        spanCloseIcon.addEventListener("mouseout", function(event) {
            spanCloseIcon.style.color = "gray";
        });
        spanCloseIcon.addEventListener("click", function(event) {
            outerThis.element.style.visibility = "hidden";
        });

        return spanCloseIcon;
    }
}

/**
 * Stores the UI view of each filter record for easy & fast lookup later.
 *
 * NOTE: Not to be confused with {@link mobiDoraTable.FilterRowValues} which is used for boolean evaluation later.
 *
 * @type {mobiDoraTable.FilterRowView}
 */
mobiDoraTable.FilterRowView = class {
    /**
     * Sole constructor.  Creates a new instance of a "filter row" with blank values ready for insertion into a filter
     * list UI.  Use the {@link getTrRef()} to get UI element.  Other methods available to query the state of the filter
     * row during filter execution.
     *
     * @see {@link getTrRef()} to get the placeable UI element
     */
    constructor(tableView) {
        this.tableView = tableView;
        this.tr = document.createElement("tr");
        this.tr.setAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW, "");

        this.tr.appendChild(createDataTableCell(this.createFilterRemoveRowIcon()));

        // Boolean operator
        this.cboBooleanOperator = this.createFilterBooleanComboBox();
        const logicOperatorCell = createDataTableCell(this.cboBooleanOperator);
        logicOperatorCell.setAttribute("align", "right");
        this.tr.appendChild(logicOperatorCell);

        // Field name
        this.cboFieldName = this.createFilterFieldNameComboBox(tableView.tableModel.getColumns());
        this.tr.appendChild(createDataTableCell(this.cboFieldName));

        // Comparator
        this.cboComparator = this.createFilterComparatorComboBox();
        this.tr.appendChild(createDataTableCell(this.cboComparator));

        // Argument
        this.txtArgument = this.createFilterOperandTextBox();
        this.tr.appendChild(createDataTableCell(this.txtArgument));
    }

    /**
     * Returns the prepared TR element ready for insertion into the DOM
     *
     * @returns {HTMLTableRowElement}
     */
    getTrRef() {
        return this.tr;
    }

    /**
     * Gets the combo box for the boolean operator
     *
     * @returns {*}
     */
    getBooleanOperatorComboBox() {
        return this.cboBooleanOperator;
    }

    /**
     * Gets the selected entry in the combobox of the boolean operator
     *
     * @returns {*}
     */
    getBooleanOperatorComboBoxSelectedEntry() {
        return this.cboBooleanOperator.options[this.cboBooleanOperator.selectedIndex];
    }

    /**
     * Gets the equivalent matching logic operator for the selected entry of the boolean combobox
     *
     * @returns {null|*}
     */
    getBooleanOperatorComboBoxSelectedLogic() {
        const selectedEntry = this.getBooleanOperatorComboBoxSelectedEntry();

        for (const rec of LOGIC_OPERATORS) {
            // Linear search.  Return on first match.
            if (rec.parseMatches(selectedEntry.text)) {
                return rec;
            }
        }

        // No match found if this point is reached
        return null;
    }

    /**
     * Returns the "field name" combobox
     *
     * @returns {*}
     */
    getFieldNameComboBox() {
        return this.cboFieldName;
    }

    /**
     * Returns the selected text of the "field name" combobox
     * @returns {*}
     */
    getFieldNameValue() {
        return this.getFieldNameComboBox().options[this.getFieldNameComboBox().selectedIndex].text;
    }

    /**
     * Gets the Comparator combobox
     *
     * @returns {*}
     */
    getComparatorComboBox() {
        return this.cboComparator;
    }

    /**
     * Gets the selected entry of the Comparator combobox
     *
     * @returns {*}
     */
    getComparatorComboBoxSelectedEntry() {
        return this.getComparatorComboBox().options[this.getComparatorComboBox().selectedIndex];
    }

    /**
     * Gets the equivalent matching logic operator for the selected entry of the Comparator combobox
     *
     * @returns {null|*}
     */
    getComparatorComboBoxSelectedLogic() {
        const selectedEntry = this.getComparatorComboBoxSelectedEntry();

        for (const rec of LOGIC_OPERATORS) {
            // Linear search.  Return on first match.
            if (rec.parseMatches(selectedEntry.text)) {
                return rec;
            }
        }

        // No match found if this point is reached
        return null;
    }

    /**
     * Gets the textbox for the argument value
     *
     * @returns {*}
     */
    getArgumentTextBox() {
        return this.txtArgument;
    }

    /**
     * Disables the boolean operator combobox such as when it is the first entry and the boolean operator is
     * not applicable
     */
    disableBooleanOperator() {
        this.cboBooleanOperator.style.visibility = "hidden";
    }

    /**
     * Builds a formatted and code-enabled close/remove icon
     *
     * @returns {HTMLSpanElement}  a formatted and code-enabled
     */
    createFilterRemoveRowIcon() {
        const outerThis = this;
        const span = document.createElement("span");
        span.style.color = "rgba(0,0,0,0.75)";
        span.innerHTML = "&#x2715";
        span.style.border = "1px solid rgba(0,0,0,0.25)";
        span.addEventListener("mouseover", function(event) {
            span.style.fontWeight = "bolder";
            span.style.fontSize = "120%";
            span.style.border = "";
        });
        span.addEventListener("mouseout", function(event) {
            span.style.fontWeight = "";  // Reset
            span.style.fontSize = "";    // Reset
            span.style.border = "1px solid rgba(0,0,0,0.25)";
        });
        span.addEventListener("click", function(event) {
            // Search upwards for parent table row <tr> element
            let elementIndex = span;

            while (elementIndex !== null && elementIndex.tagName !== "TR" && !elementIndex.hasAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW)) {
                elementIndex = elementIndex.parentElement;
            }

            if (elementIndex !== null) {
                outerThis.tableView.filterDropdownBox.removeFilterRow(elementIndex);
            }
        });

        return span;
    }

    /**
     * Create the combobox containing the logical operators for filtering
     *
     * @param first  set to  <code>true</code> if this is the first entry and <code>false</code> otherwise
     * @returns {HTMLSelectElement}  combobox with boolean operators
     */
    createFilterBooleanComboBox() {
        const combobox = document.createElement("select");

        // Mark this tag with an attribute class for easier DOM lookup later
        combobox.setAttribute(mobiDoraTable.TAG_ATTRIB_BOOLEAN_COMBOBOX, "");
        combobox.options.add(new Option("AND", "AND"));
        combobox.options.add(new Option("OR", "OR"));

        return combobox
    }

    /**
     * Creates the combobox for user to select table columns for filtering
     *
     * @param columnHeaders    the array of {@see #Column} objects from the table data model
     * @returns {HTMLSelectElement}  a populated combobox ready for insertion into the DOM
     */
    createFilterFieldNameComboBox(columnHeaders) {
        const outerThis = this;
        const combobox = document.createElement("select");

        if (columnHeaders !== null) {
            for (const column of columnHeaders) {
                combobox.options.add(new Option(escapeHtml(column.name), escapeHtml(column.type)))
            }
        }

        combobox.addEventListener("change", function(event) {
            const columnTypeId = combobox.options[combobox.selectedIndex].value;
            const comparatorComboBox = findElementThatHasAttribute(combobox.parentElement.parentElement, mobiDoraTable.TAG_ATTRIB_COMPARATOR);

            switch (columnTypeId) {
                case mobiDoraTable.ColumnType.FLOAT.toString():
                case mobiDoraTable.ColumnType.INTEGER.toString():
                    // Disable string comparators as they are not applicable for numeric values
                    outerThis.disableStringComparators(comparatorComboBox, true);

                    break;
                default:
                    // Re-enable all comparators
                    outerThis.disableStringComparators(comparatorComboBox, false);
            }
        });

        return combobox;
    }

    /**
     * Disables the given operator combobox options that are "string" related: CONTAINS, STARTSWITH
     *
     * @param combobox  reference to the matching comparator combobox for the filter row
     * @param disabled  set to <code>true</code> to disable string operator related options or <code>false</code> to re-enable all options
     */
    disableStringComparators(combobox, disabled=true) {
        if (combobox !== null) {
            // Disable combobox items that are for string based operations
            if (disabled) {
                for (const option of combobox.options) {
                    if (mobiDoraTable.FILTER_COMP_CONTAINS.parseMatches(option.value) || mobiDoraTable.FILTER_COMP_STARTSWITH.parseMatches(option.value)) {
                        option.disabled = true;
                    }
                }

                // If current option became disabled then change the current selection to a non-disabled option
                if (combobox.options[combobox.selectedIndex].disabled) {
                    let i = 0;
                    for (const option of combobox.options) {
                        // Linear search.  Set on first match.
                        if (!option.disabled) {
                            const currentOptionName = combobox.options[combobox.selectedIndex].label;
                            combobox.selectedIndex = i;
                            console.info(`Current operator "${currentOptionName}" is no longer valid.  Changing to "${combobox.options[combobox.selectedIndex].label}" instead.`);

                            break;
                        }
                        i++;
                    }
                }
            } else {
                // Re-enable all comparators
                for (const option of combobox.options) {
                    option.disabled = "";
                }
            }

        } else {
            console.warn("Cannot disable comparators because reference to combobox is null");
        }
    }

    /**
     * Creates the populated operator combo box
     *
     * @returns {HTMLSelectElement}  the populated operator combox box ready for insertion into the DOM
     */
    createFilterComparatorComboBox() {
        const combobox = document.createElement("select");
        combobox.setAttribute(mobiDoraTable.TAG_ATTRIB_COMPARATOR, "");

        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_EQUALS.firstToken(), mobiDoraTable.FILTER_COMP_EQUALS.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_NOT_EQUALS.firstToken(), mobiDoraTable.FILTER_COMP_NOT_EQUALS.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_CONTAINS.firstToken(), mobiDoraTable.FILTER_COMP_CONTAINS.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_STARTSWITH.firstToken(), mobiDoraTable.FILTER_COMP_STARTSWITH.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_LESS_THAN.firstToken(), mobiDoraTable.FILTER_COMP_LESS_THAN.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_LESS_THAN_EQUAL.firstToken(), mobiDoraTable.FILTER_COMP_LESS_THAN_EQUAL.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_GREATER_THAN.firstToken(), mobiDoraTable.FILTER_COMP_GREATER_THAN.firstToken()));
        combobox.options.add(new Option(mobiDoraTable.FILTER_COMP_GREATER_THAN_EQUAL.firstToken(), mobiDoraTable.FILTER_COMP_GREATER_THAN_EQUAL.firstToken()));

        return combobox;
    }

    /**
     * Creates the input text box to take the filter value from the uesr
     *
     * @returns {HTMLInputElement}
     */
    createFilterOperandTextBox() {
        const outerThis = this;
        const textbox = document.createElement("input");
        textbox.setAttribute("type", "text");
        textbox.setAttribute("size", "20");
        textbox.cancelBubble = true;
        textbox.onclick = function(event) {
            event.stopPropagation();
            event.stopImmediatePropagation();
            event.cancelBubble = true;

            return false;
        };
        textbox.addEventListener("keydown", function(event) {
            // Process special control/action keys
            //
            switch (event.key) {
                case "Enter":
                    // Apply the filter
                    outerThis.tableView.filterDropdownBox.btnApply.click();
                    break;
                case "Escape":
                    // Cancel
                    outerThis.tableView.filterDropdownBox.element.style.visibility = "hidden";
                    break;
            }

            return false;
        });

        return textbox;
    }
}

/**
 * Represents the filter data record extracted from the UI elements.  This class makes the actual filtering logic
 * processing easier by storing filter row components directly in a format suitable for evaluation.
 *
 * NOTE: Not to be confused with {@link mobiDoraTable.FilterRowView} which is used for UI processing.
 *
 * @type {mobiDoraTable.FilterRowValues}
 */
mobiDoraTable.FilterRowValues = class {
    /**
     * Sole constructor
     *
     * @param booleanOperator
     * @param fieldName
     * @param comparator
     * @param argument
     */
    constructor(booleanOperator, fieldName, comparator, argument) {
        this.booleanOperator = booleanOperator;
        this.fieldName = fieldName;
        this.comparator = comparator;
        this.argument = argument;
    }

    getBooleanOperator() {
        return this.booleanOperator;
    }

    getFieldName() {
        return this.fieldName;
    }

    getComparator() {
        return this.comparator;
    }

    getArgument() {
        return this.argument;
    }
}

/**
 * Contains logic for matching during parsing and execution during filter evaluation.  Create one instance of this
 * for each boolean logic operator available for the user to select.
 *
 * @type {mobiDoraTable.BoolLogic}
 */
mobiDoraTable.BoolLogic = class {
    /**
     * Sole constructor
     *
     * @param evaluationFunction  boolean function to run during evaluation {@see evaluate}
     * @param tokens  at least one token (symbol) that represents this operator's parse token
     */
    constructor(evaluationFunction, ...tokens) {
        if (!isArrayEmpty(tokens)) {
            this.tokens = tokens;
        } else {
            throw "At least one token is required";
        }

        if (evaluationFunction !== null) {
            this.evaluationFunction = evaluationFunction;
        } else {
            throw "Evaluation function is required";
        }
    }

    /**
     * Returns the first symbol of this operator
     *
     * @returns {string}  first symbol of this operator
     */
    firstToken() {
        return this.tokens[0];
    }

    toString() {
        return this.tokens.toString();
    }

    /**
     * Determines if this logic object matches the given token string.  Use this during filter processing to select
     * the correct logic
     *
     * @param token  string
     * @returns {boolean}
     */
    parseMatches(token) {
        return this.tokens.includes(token);
    }

    /**
     * Evaluates the two operands under this logic objects evaluation function
     *
     * @param leftOperand
     * @param rightOperand
     * @returns {boolean}
     */
    evaluate(leftOperand, rightOperand) {
        return this.evaluationFunction(leftOperand, rightOperand);
    }
}

/**
 * Used to assist in evaluating of boolean definitions by referencing the boolean operator (AND/OR) and the
 * boolean value from a boolean evaluation.  When chained together, the result will be the final boolean value
 * for a chain of boolean values.
 *
 * @type {mobiDoraTable.BooleanEntry}
 */
mobiDoraTable.BooleanEntry = class {
    /**
     * Sole constructor
     *
     * @param boolOperator  the boolean operator {@link mobiDoraTable.BoolLogic}
     * @param booleanValue  <code>true</code> or <code>false</code> from the result from some boolean sub-evaluation
     */
    constructor(boolOperator, booleanValue) {
        this.boolOperator = boolOperator;
        this.value = booleanValue;
    }
}

/**
 * Simple tuple to return both filtered-in & filtered-out results together
 *
 * @type {mobiDoraTable.FilterResults}
 */
mobiDoraTable.FilterResults = class {
    /**
     * Sole constructor
     *
     * @param matchingRows
     * @param nonMatchingRows
     */
    constructor(matchingRows, nonMatchingRows) {
        this.matchingRows = matchingRows;
        this.nonMatchingRows = nonMatchingRows;
    }
}

mobiDoraTable.FILTER_OP_BOOL_AND = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        return leftOperand && rightOperand;
    },
    "AND");
mobiDoraTable.FILTER_OP_BOOL_OR = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        return leftOperand || rightOperand;
    },
    "OR");
mobiDoraTable.FILTER_COMP_CONTAINS = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        return leftOperand.includes(rightOperand);
    },
    "CONTAINS");
mobiDoraTable.FILTER_COMP_STARTSWITH = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        return leftOperand.startsWith(rightOperand);
    },
    "STARTSWITH");
mobiDoraTable.FILTER_COMP_EQUALS = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) === Number.parseFloat(rightOperand);
        } else {
            return leftOperand === rightOperand;
        }
    },
    "=");
mobiDoraTable.FILTER_COMP_NOT_EQUALS = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) !== Number.parseFloat(rightOperand);
        } else {
            return leftOperand !== rightOperand;
        }
    },
    "≠", "!=", "!==");
mobiDoraTable.FILTER_COMP_LESS_THAN = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) < Number.parseFloat(rightOperand);
        } else {
            return leftOperand < rightOperand;
        }
    },
    "<");
mobiDoraTable.FILTER_COMP_LESS_THAN_EQUAL = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) <= Number.parseFloat(rightOperand);
        } else {
            return leftOperand <= rightOperand;
        }
    },
    "≤", "<=");
mobiDoraTable.FILTER_COMP_GREATER_THAN = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) > Number.parseFloat(rightOperand);
        } else {
            return leftOperand > rightOperand;
        }
    },
    ">");
mobiDoraTable.FILTER_COMP_GREATER_THAN_EQUAL = new mobiDoraTable.BoolLogic(
    function(leftOperand, rightOperand) {
        if (!isNaN(leftOperand)) {
            return Number.parseFloat(leftOperand) >= Number.parseFloat(rightOperand);
        } else {
            return leftOperand >= rightOperand;
        }
    },
    "≥", ">=");

/**
 * Operators places into array for runtime lookup
 *
 * @type {*[]}
 */
const LOGIC_OPERATORS = [
    mobiDoraTable.FILTER_OP_BOOL_AND,
    mobiDoraTable.FILTER_OP_BOOL_OR,
    mobiDoraTable.FILTER_COMP_CONTAINS,
    mobiDoraTable.FILTER_COMP_STARTSWITH,
    mobiDoraTable.FILTER_COMP_EQUALS,
    mobiDoraTable.FILTER_COMP_NOT_EQUALS,
    mobiDoraTable.FILTER_COMP_LESS_THAN,
    mobiDoraTable.FILTER_COMP_LESS_THAN_EQUAL,
    mobiDoraTable.FILTER_COMP_GREATER_THAN,
    mobiDoraTable.FILTER_COMP_GREATER_THAN_EQUAL
];

Object.freeze(mobiDoraTable.FILTER_OP_BOOL_AND);
Object.freeze(mobiDoraTable.FILTER_OP_BOOL_OR);
Object.freeze(mobiDoraTable.FILTER_COMP_CONTAINS);
Object.freeze(mobiDoraTable.FILTER_COMP_STARTSWITH);
Object.freeze(mobiDoraTable.FILTER_COMP_EQUALS);
Object.freeze(mobiDoraTable.FILTER_COMP_NOT_EQUALS);
Object.freeze(mobiDoraTable.FILTER_COMP_LESS_THAN);
Object.freeze(mobiDoraTable.FILTER_COMP_LESS_THAN_EQUAL);
Object.freeze(mobiDoraTable.FILTER_COMP_GREATER_THAN);
Object.freeze(mobiDoraTable.FILTER_COMP_GREATER_THAN_EQUAL);
