if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

/**
 * Drag data transfer key for table column name
 * @type {string}
 */
const DRAG_COLUMN_NAME = "DRAG_COLUMN_NAME";

/**
 * Drag attribute key to identify a blank row
 *
 * @type {string}
 */
const DRAG_ROW_BLANK = "DRAG_ROW_BLANK";

/**
 * This encapsulates the GUI, data, and control logic for the table grouping feature which provides basic table
 * pivoting features
 *
 * @see  {getElementRef()}  for the root HTML element
 * @type {mobiDoraTable.ColumnHidingDropdownBox}
 */
mobiDoraTable.GroupingDropdownBox = class {
    /**
     * Sole constructor
     *
     * @param tableView  reference to the parent table view
     * @param {function} fnPostPivot  optional callback function to run after pivot is run by user.  This can be used to
     *                                set UI state according to the context of the caller.
     */
    constructor(tableView, fnPostPivot) {
        // Save reference to the parent table
        this.tableView = tableView;

        // Create and style the DIV used as the root UI element
        this.element = document.createElement("div");
        this.element.className = "dropDownMenu1";
        this.element.tabIndex = -1;  // Required in order to have key listener on DIV
        this.element.style.visibility = "hidden";
        this.element.style.alignContent = "left";
        this.element.style.textAlign = "left";
        this.element.style.height = "150px";
        this.element.style.width = "300px";
        this.element.addEventListener("keydown", function(event) {
            if (event.key === "Escape") {
                outerThis.element.style.visibility = "hidden";
            }
        });

        // Allows anonymous function access this instance
        const outerThis = this;

        // Create shortcut button for inverting the current selection state of all checkboxes
        this.btnApply = document.createElement("button");
        this.btnApply.innerHTML = "Apply";
        this.btnApply.style.fontSize = "10pt";
        this.btnApply.style.marginTop = "6pt";
        this.btnApply.addEventListener("click", function(event) {
            console.info("Applying row pivoting...");

            // Apply the table pivoting
            outerThis.applyPivot();

            // Run the post-pivot callback if provided
            if (fnPostPivot !== null) {
                fnPostPivot();
            }
        });

        // Setup this popup dialog's main layout
        const tblSourceList = document.createElement("table");
        tblSourceList.style.width = "100%";
        const tblPivotList = document.createElement("table");
        tblPivotList.style.width = "100%";
        tblSourceList.style.border = "1px solid rgba(0,0,0,0.4)";
        tblSourceList.style.float = "left";
        tblSourceList.style.marginLeft = "5pt";
        tblSourceList.cellSpacing = "0";
        tblPivotList.style.border = "1px solid rgba(0,0,0,0.4)";
        tblPivotList.style.float = "left";
        tblPivotList.style.marginRight = "5pt";
        tblPivotList.cellSpacing = "0";
        const table = document.createElement("table");
        table.style.width = "100%";
        table.border = "0";
        const tbody = table.createTBody();
        const tr1 = document.createElement("tr");
        const tr2 = document.createElement("tr");
        const tr3 = document.createElement("tr");
        const td1 = document.createElement("td");
        const td2 = document.createElement("td");
        const td3 = document.createElement("td");
        const td4 = document.createElement("td");
        const td5 = document.createElement("td");
        const td6 = document.createElement("td");
        tr2.style.verticalAlign = "top";
        td6.style.textAlign = "right";
        td1.align = "center";
        td1.innerHTML = "<span class='groupItemHeader'>Pivot Columns</span>";
        td2.style.textAlign = "center";
        td2.innerHTML = "<span class='groupItemHeader'>Source Columns</span>";
        td3.append(tblPivotList);
        td4.append(tblSourceList);
        td5.append(this.createCloseIcon());
        td6.append(this.btnApply);
        td6.colSpan = 3;
        tr1.appendChild(td1);
        tr1.appendChild(td2);
        tr1.appendChild(td5);
        tr2.appendChild(td3);
        tr2.appendChild(td4);
        tr3.appendChild(td6);
        tbody.appendChild(tr1);
        tbody.appendChild(tr2);
        tbody.appendChild(tr3);
        this.element.appendChild(table);

        // Create TBODY which will be used to add/remove rows for DnD operations
        this.tbodySourceList = tblSourceList.createTBody();
        this.tbodyPivotList = tblPivotList.createTBody();

        // Create list of available columns to drag from
        //
        for (const columnHeader of tableView.tableModel.getColumns()) {
            const tr = document.createElement("tr");
            const td = document.createElement("td");

            // Append the table row
            td.append(trimToBlank(columnHeader.name));
            td.setAttribute(DRAG_COLUMN_NAME, trimToBlank(columnHeader.name));   // Set actual column name so drag target will use to determine which column was dragged
            td.className = "groupListItem";
            td.draggable = true;
            td.ondragstart = function(dragEvent) {
                console.info(dragEvent);
                dragEvent.dataTransfer.setData(DRAG_COLUMN_NAME, td.getAttribute(DRAG_COLUMN_NAME));
            };
            td.ondragenter = function(dragEvent) {
                // Need to disable this so otherwise the "ondrop" event is blocked
                dragEvent.preventDefault();
                if (td.parentElement.parentElement === outerThis.tbodyPivotList) {
                    td.style.borderTop = "1px dashed rgba(0,0,0,0.66)";
                }
            };
            td.ondragover = function(dragEvent) {
                // Need to disable this so otherwise the "ondrop" event is blocked
                dragEvent.preventDefault();
            };
            td.ondragleave = function(dragEvent) {
                td.style.borderTop = "";
            };
            td.ondrop = function(dragEvent) {
                // This handles *both* source and destination tables and will be based on which of the two tables
                // the row is currently child of
                //
                if (td.parentNode.parentNode === outerThis.tbodyPivotList) {
                    // Get the reference to the TD node that was dropped by reverse-lookup on the data transfer column
                    // name.
                    // NOTE: Only *one* of these will be not-null depending on where the dropped cell came from.  This
                    // then determines whether the drop is for a "move" operation or "re-ordering" operation.
                    const tdDroppedFromSourceList = outerThis.findRowWithTableColName(dragEvent.dataTransfer.getData(DRAG_COLUMN_NAME), outerThis.tbodySourceList);
                    const tdDroppedFromPivotList = outerThis.findRowWithTableColName(dragEvent.dataTransfer.getData(DRAG_COLUMN_NAME), outerThis.tbodyPivotList);

                    // Index position of which element was dropped
                    const indexPosition = Array.from(td.parentNode.parentNode.children).indexOf(td.parentNode);

                    if (tdDroppedFromSourceList !== null) {
                        // MOVE OPERATION
                        //
                        console.info(`Drag dropped onto non-blank row in table "pivot" list`);
                        outerThis.moveToPivotList(tdDroppedFromSourceList, indexPosition);
                    } else {
                        // RE-ORDERING OPERATION
                        console.info(`Drag dropped onto non-blank row for "re-ordering" in table "pivot" list`);
                        outerThis.reorderPivotList(tdDroppedFromPivotList, indexPosition);
                    }
                } else {
                    console.info(`Drag dropped onto non-blank row in table "source" list`);

                    // Find the dropped TD's reference and move it to the target list
                    const tdDropped = outerThis.findRowWithTableColName(dragEvent.dataTransfer.getData(DRAG_COLUMN_NAME), outerThis.tbodyPivotList);
                    if (tdDropped !== null) {
                        outerThis.moveToSourceList(tdDropped);
                    }
                }

                // Always clear transient move indicators
                for (const tr of outerThis.tbodyPivotList.children) {
                    tr.firstChild.style.borderTop = "";
                    tr.firstChild.style.background = "";
                }
                for (const tr of outerThis.tbodySourceList.children) {
                    tr.firstChild.style.borderTop = "";
                    tr.firstChild.style.background = "";
                }
            };
            tr.append(td);

            // Append to the main table body
            this.tbodySourceList.append(tr);
        }

        // Create initially blank table to drag columns into
        //
        for (const column of tableView.tableModel.getColumns()) {
            const trEmpty = document.createElement("tr");
            const tdEmpty = document.createElement("td");

            // Append the table row
            tdEmpty.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            trEmpty.setAttribute(DRAG_ROW_BLANK, "");   // Mark this is a blank spacer row
            tdEmpty.ondragstart = function(dragEvent) {
                console.info(dragEvent);
            };
            tdEmpty.ondragenter = function(dragEvent) {
                // Need to disable this so otherwise the "ondrop" event is blocked
                dragEvent.preventDefault();

                const firstBlankRow = outerThis.findFirstBlankRow(tdEmpty.parentElement.parentElement);
                // firstBlankRow.firstChild.style.borderTop = "1px dashed rgba(0,0,0,0.66)";
                firstBlankRow.firstChild.style.background = "rgba(128,0,128,0.25)";
            };
            tdEmpty.ondragover = function(dragEvent) {
                // Need to disable this so otherwise the "ondrop" event is blocked
                dragEvent.preventDefault();
            };
            tdEmpty.ondragleave = function(dragEvent) {
                // Clear move indicator as hover changes
                tdEmpty.style.borderTop = "";
                tdEmpty.style.background = "";
            };
            tdEmpty.ondrop = function(dragEvent) {
                // This handles *both* source and destination tables and will be based on which of the two tables
                // the row is currently child of
                if (tdEmpty.parentNode.parentNode === outerThis.tbodyPivotList) {
                    // Get the reference to the TD node that was dropped by reverse lookup on the data transfer column
                    // name
                    const dragColumnName = dragEvent.dataTransfer.getData(DRAG_COLUMN_NAME);
                    const tdDroppedFromSource = outerThis.findRowWithTableColName(dragColumnName, outerThis.tbodySourceList);
                    const tdDroppedFromPivot = outerThis.findRowWithTableColName(dragColumnName, outerThis.tbodyPivotList);

                    if (tdDroppedFromSource !== null) {
                        // INSERT NEW ROW
                        // Handles when a column is dropped into "pivot list" from the "source list" (a move)
                        //
                        console.info(`Dropped onto blank row in "pivot list": ${tdDroppedFromSource.getAttribute(DRAG_COLUMN_NAME)}`);
                        outerThis.moveToPivotList(tdDroppedFromSource, -1);
                    } else {
                        // RE-ORDERING EXISTING ROW
                        //
                        const row = outerThis.findFirstBlankRow(outerThis.tbodyPivotList, outerThis.tbodyPivotList.lastChild);
                        const indexPosition = Array.from(outerThis.tbodyPivotList.children).indexOf(row);

                        outerThis.reorderPivotList(tdDroppedFromPivot, indexPosition - 1);
                    }
                } else {
                    // Handles when a column is dropped back to the original source list
                    //
                    const dragColumnName = dragEvent.dataTransfer.getData(DRAG_COLUMN_NAME);
                    const tdDropped = outerThis.findRowWithTableColName(dragColumnName, outerThis.tbodyPivotList);

                    if (tdDropped !== null) {
                        console.info(`Dropped onto blank row in "source list": ${tdDropped.getAttribute(DRAG_COLUMN_NAME)}`);
                        outerThis.moveToSourceList(tdDropped);
                    }
                }

                // Always clear transient move indicators
                for (const tr of outerThis.tbodyPivotList.children) {
                    tr.firstChild.style.borderTop = "";
                    tr.firstChild.style.background = "";
                }
                for (const tr of outerThis.tbodySourceList.children) {
                    tr.firstChild.style.borderTop = "";
                    tr.firstChild.style.background = "";
                }
            };
            trEmpty.append(tdEmpty);

            // Append to the main table body
            this.tbodyPivotList.append(trEmpty);
        }
    }

    /**
     * Finds the row with the given column name within the given table body
     *
     * @param columnName   the name of the column to search for
     * @param tbody     the TBODY that contains the rows to search through
     * @returns {null|HTMLElement}
     */
    findRowWithTableColName(columnName, tbody) {
        for (const tr of tbody.children) {
            // Linear search.  Return on first match.
            if (tr.childElementCount > 0) {
                const td = tr.childNodes[0];
                if (td.getAttribute(DRAG_COLUMN_NAME) === columnName) {
                    return td;
                }
            }
        }

        // No match found if this point is reached.  Null will indicate to client to skip.
        return null;
    }

    /**
     * Re-orders a row in the "pivot list" by removing it from its original position and inserting it before
     * the row referenced by the index position
     *
     * @param tdSrc          reference to the TD to move
     * @param indexPosition  index of the position to insert at
     * @see moveToSourceList
     */
    reorderPivotList(tdSrc, indexPosition) {
        console.log(`Re-ordering pivot column in table before existing row index: ${indexPosition}`);

        // Remove TD element from source table body...
        //
        this.tbodyPivotList.removeChild(tdSrc.parentElement);

        // Re-insert TD element to the pivot table body at the new position
        //
        const trInsertBefore = this.tbodyPivotList.childNodes[indexPosition];
        this.tbodyPivotList.insertBefore(tdSrc.parentElement, trInsertBefore);
    }

    /**
     * Moves a row to the pivot selections list
     *
     * @param {HTMLTableCellElement} tdSrc  the cell containing the column header to move
     * @param {number} indexPosition  the index to insert the row into.  Ex 0 for at head of the list or can use -1 of the list
     * @see moveToSourceList
     * @see reorderPivotList
     */
    moveToPivotList(tdSrc, indexPosition=0) {
        console.log(`Inserting pivot column in table before existing row index: ${indexPosition}`);

        // Remove TD element from source table body
        //
        this.tbodySourceList.removeChild(tdSrc.parentElement);

        // Add TD element to the pivot table body
        //
        if (indexPosition >= 0) {
            const trInsertBefore = this.tbodyPivotList.childNodes[indexPosition];
            this.tbodyPivotList.insertBefore(tdSrc.parentElement, trInsertBefore);
        } else {
            const trInsertBefore = this.findFirstBlankRow(this.tbodyPivotList, this.tbodyPivotList.firstChild);
            this.tbodyPivotList.insertBefore(tdSrc.parentElement, trInsertBefore);
        }

        // Move a blank row to the pivot list to replace the moved row.  This maintains both lists with exactly the
        // same sum of row and blank rows as when they were first initialized.
        for (const tr of this.tbodyPivotList.children) {
            if (tr.hasAttribute(DRAG_ROW_BLANK)) {
                this.tbodyPivotList.removeChild(tr);
                this.tbodySourceList.append(tr);

                // Only need to move one blank row over
                break;
            }
        }
    }

    /**
     * Moves a row back to the source list.  The row is always inserted to the end of the list because
     * order does not matter in this list.
     *
     * @param tdSrc
     * @see moveToPivotList
     */
    moveToSourceList(tdSrc) {
        console.log("Inserting row back to the source list");

        // Remove TD element from pivot list's table body
        //
        this.tbodyPivotList.removeChild(tdSrc.parentElement);

        // Add TD element to the source list's table body
        //
        const trInsertBefore = this.findFirstBlankRow(this.tbodySourceList, this.tbodySourceList.lastChild);
        this.tbodySourceList.insertBefore(tdSrc.parentElement, trInsertBefore);

        // Move a blank row to the pivot list to replace the moved row.  This maintains both lists with exactly the
        // same sum of row and blank rows as when they were first initialized.
        for (const tr of this.tbodySourceList.children) {
            if (tr.hasAttribute(DRAG_ROW_BLANK)) {
                this.tbodySourceList.removeChild(tr);
                this.tbodyPivotList.append(tr);

                // Only need to move one blank row over
                break;
            }
        }
    }

    /**
     * Finds the row in the column list with name matching the given colun
     *
     * @param {string} columnName
     * @param {HTMLTableSectionElement} tableBody
     * @return {HTMLTableRowElement}
     */
    findColumnNameInList(columnName, tableBody) {
        for (const tr of tableBody.rows) {
            const td = tr.firstChild;

            if (td.getAttribute(DRAG_COLUMN_NAME) === columnName) {
                return tr;
            }
        }

        // No matching row found
        return null;
    }

    /**
     * Finds the first blank TR row in the given TBODY otherwise returns the alternative if no blank rows found
     *
     * @param tbody
     * @returns {null|HTMLElement}
     */
    findFirstBlankRow(tbody, alternative = null) {
        for (const row of tbody.childNodes) {
            // Linear search.  Return on first match.
            if (row.hasAttribute(DRAG_ROW_BLANK)) {
                return row;
            }
        }

        // No blank rows found so return the alternative
        return alternative;
    }

    /**
     * Use this to get reference to the DIV element that is the parent HTML object of this dialog box.  This will
     * need to be placed into the page's DOM at least once at some point.
     *
     * @returns {HTMLDivElement}  root element of this dialog
     */
    getElementRef() {
        return this.element;
    }

    /**
     * Applies the table pivoting based on current selections
     *
     * @param {boolean} [filterFirst = true]  set to <code>true</code> to applying row filtering first or
     *                     <code>false</code> to disable filtering first which is necessary if table filtering is
     *                     itself requesting pivot operation to avoid infinite recursion.
     * @param {number} [overrideSortOrder = null] optional override all sort columns to this sort order {@see SORT_ORDER_ASCENDING} {@see SORT_ORDER_DESCENDING}
     * @param {string} [overrideColumns=null]  Specify a pivot column list programmatically
     */
    applyPivot(filterFirst=true, overrideSortOrder = null, ...overrideColumns) {
        // Reset pivoting columns as they will be updated below with user's current pivot column selections
        this.pivotColumnHeaders = new Array();

        console.info("Showing pivot spinner");
        this.tableView.setPivotSpinnerVisible(true);

        // Need to run the pivoting code with slight delay otherwise the spinner UI made visible above above does *not*
        // show up on screen.  The pivoting code is CPU-heavy so probably stalls out processing all Javascript event
        // queues.
        setTimeout(() => {
            try {
                if (!isArrayEmpty(overrideColumns)) {
                    // Programmatically set the pivot columns into the GUI as if they were dragged-in by the user
                    this.resetPivotRowsUi();
                    this.setPivotRowsUi(...overrideColumns);
                }

                // Activate pivot mode now because downstream code can refer to this to know that pivot sort is currently active
                this.tableView.spanPivotStatus.style.visibility = "visible";

                // Show all columns if they have been hidden otherwise some columns requested to be viewed in pivot will be
                // missing
                this.tableView.columnHidingDropdownBox.showAllColumns(false);

                // Iterate through pivot list and get the column names to pivot on.  Ordering matters.
                let i = 0;
                for (const tr of this.tbodyPivotList.children) {
                    const td = tr.firstChild;

                    // Get pivot row names in user's specified order (and ignore blank rows)
                    if (!tr.hasAttribute(DRAG_ROW_BLANK)) {
                        const columnName = td.getAttribute(DRAG_COLUMN_NAME);

                        console.info("Starting table row pivoting the following columns:");
                        console.info(`Column name ${i}: ${columnName}`);

                        for (const columnHeader of this.tableView.columnHeaders) {
                            if (columnName === columnHeader.modelColumn.name) {
                                this.pivotColumnHeaders.push(columnHeader);
                                if (overrideSortOrder !== null) {
                                    columnHeader.setCurrentSortOrder(overrideSortOrder);
                                }
                                break;
                            }
                        }

                        i++;
                    }
                }

                // Close the popup dialog
                this.element.style.visibility = "hidden";

                // Get just the name (string value) of the user selected pivot column
                const columnNames = this.pivotColumnHeaders.map(columnHeader => columnHeader.modelColumn.name);

                // Detach columns from the report that are *not* included in the pivot
                this.tableView.columnsAttachOnlyByName(...columnNames);
                this.tableView.reorderColumnHeaders(...columnNames);

                // Re-apply filtering if requested
                //
                if (filterFirst) {
                    console.info("Applying row pivot with row filtering first...");

                    // Apply the filtering if it is enabled
                    //
                    // NOTE: this single call also handles all pivot column arrangements
                    //
                    const filteringApplied = this.tableView.filterDropdownBox.applyFilterIfVisible();

                    // If filtering wasn't enabled/applied, then must manually apply pivot column arrangements
                    if (!filteringApplied) {
                        // Restore original rows from data model
                        //
                        // NOTE: This also creates the new <TR> rows in the given pivot row!  Which is a critical part of
                        // the pivot feature.
                        this.tableView.restoreOriginalRows();

                        // Set new formatted/re-ordered rows into the table view directly since there is no row filtering that
                        // is active
                        this.tableView.setRows(new Array(...this.tableView.tableBody.rows));  // NOTE: a shallow copy of the original array is required since original array gets cleared out in the call
                    }
                } else {
                    console.info("Applying row pivot with no filtering...");
                }

                // Table headers will be re-rendered according to pivot column ordering and selections.  Columns *not* selected
                // for pivoting will also *not* be rendered here.
                //
                this.tableView.renderTableHeaders();

                //
                // Call table view to execute the row pivot on the column list retrieved above
                //
                // NOTE: the core pivot logic of partitioning the rows actually occurs in this sorting method
                //
                this.tableView.tableSorter.sortByHeaderNames(false, ...this.pivotColumnHeaders);

                // Group UI table cells to complete pivot
                this.pivotRenderCells();

                // Update status indicator with pivoting column counts
                this.tableView.spanPivotStatus.innerHTML = `<span style="opacity: 50%">x</span> &nbsp; Pivoting on ${this.pivotColumnHeaders.length} columns`;

                // Disable the show/hide button because that interferes with grouping since grouping has its own column set
                this.tableView.btnHideColumns.disabled = true;
            } finally {
                console.info("Hiding pivot spinner");
                this.tableView.setPivotSpinnerVisible(false);
            }
        }, 64);
    }

    /**
     * Applies the current pivot but only if pivot function is currently enabled
     *
     * @param {boolean} [filterFirst = true]  set to <code>true</code> to applying row filtering first or
     *                     <code>false</code> to disable filtering first which is necessary if table filtering is
     *                     itself requesting pivot operation to avoid infinite recursion.
     *
     * @return {boolean}   <code>true</code if pivot was re-applied and <code>false</code> if not
     */
    applyPivotIfVisible(filterFirst=true) {
        if (this.tableView.isPivotingActive()) {
            this.applyPivot(filterFirst);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates the visual look of the grouped rows in the table to create visible partitions in the view.
     *
     * NOTE: Requires that {@see applyPivot} is executed first because this uses the results of the grouping and
     *       partitions created there
     * NOTE: This does <b>not</b> apply the pivoting operation itself -- it only renders the edges visually. So this
     *       method should not be called directly.  Use {@see applyPivot} instead to actually perform pivoting on the
     *       dataset.
     * NOTE: Rows that become completely blank (all cells empty) are removed from view
     *
     * @see applyPivot
     */
    pivotRenderCells() {
        const tableBody = this.tableView.tableBody;

        // DRAW PARTITION EDGES & BLANK OUT REPEATS
        //
        // Use the results of the partitioning above to (1) Draw edges at ends of partitions and (2) Blank out cells in
        // a partition group *except* for the first cell which creates the "visual grouping" look.
        //
        for (const rowPartitions of this.tableView.tableSorter.rowPartitionsList) {
            const rowGroupRanges = this.groupRowIndexes(rowPartitions.partitions);

            for (const rowGroupRange of rowGroupRanges) {
                // Draw row bottom border on partition boundaries
                // NOTE: the final partition edge at bottom of the table is *not* drawn for aesthetic reasons
                //
                const bottomEdgeIdx = rowGroupRange.getEnd() - 1;
                if (bottomEdgeIdx < tableBody.rows.length - 1) {
                    tableBody.rows[bottomEdgeIdx].childNodes[rowPartitions.colIndex].style.borderBottom = "2px solid rgba(0,0,0,0.25)";
                }

                // Blank all cell values in the group *except* for first which creates a "merged cell" appearance
                //
                for (let i = rowGroupRange.getStart(); i < rowGroupRange.getEnd(); i++) {
                    const td = tableBody.rows[i].childNodes[rowPartitions.colIndex];

                    if (i === rowGroupRange.getStart()) {
                        if (rowGroupRange.getEnd() - rowGroupRange.getStart() > 1) {
                            // First row in partition keeps the cell value but background becomes white
                            td.style.background = "white";
                        }
                    } else {
                        // Remaining cells in the partition get blanked out
                        td.innerHTML = "";
                        td.style.background = "white";
                    }
                }
            }
        }

        // ONE COLUMN SPECIAL FORMATTING
        //
        // Special processing for when only "one column" is in the pivot
        //
        // Remove edge partitions since they are not necessary for single column view
        if (this.tableView.tableRef.tHead.firstChild.childElementCount === 1) {
            console.info("Single column mode detected.  Clearing out partition edges from view.");

            for (const tr of tableBody.childNodes) {
                for (const td of tr.children) {
                    // Edge partitions (if any) not needed
                    td.style.borderBottom = "";

                    // Background inherited any previous pivot not needed
                    td.style.background = "";
                }
            }
        }

        // DELETE EMPTY ROWS
        //
        // Iterate through rows backwards (bottom up) and remove rows that are completely empty -- have all cells blank.
        //
        // NOTE: Blank rows occur usually when too few columns are included in the pivot where normally a "child" cell
        // would occupy the row but are not included in the pivot.  This often happens when only columnn is included in
        // the pivot list.
        if (!isArrayEmpty(tableBody.childNodes)) {
            for (let i = tableBody.childElementCount - 1; i >= 0; i--) {
                if (isTableRowEmpty(tableBody.childNodes[i])) {
                    const trBlank = tableBody.childNodes[i];

                    tableBody.removeChild(trBlank);

                    // INHERIT PARTITION EDGES
                    //
                    // The previous row above the deleted row needs to inherit the partition edges otherwise some
                    // partition edge markers could disappear from view and break the correct pivot view.
                    if (i > 0) {
                        // NOTE: Deletion is easiest to perform *backwards* bottom up
                        const trPrevious = tableBody.childNodes[i - 1];
                        for (let j = 0; j < trBlank.childElementCount; j++) {
                            trPrevious.childNodes[j].style.borderBottom = trBlank.childNodes[j].style.borderBottom;
                        }
                    }
                }
            }
        }
    }

    /**
     * Translates the partition positions to table row indexes
     *
     * @param {mobiDoraTable.RowPartitions[]} partitions
     * @returns {number[]}
     */
    groupRowIndexes(partitions) {
        const groupRowIndexes = new Array();

        let i = 0;
        for (const partition of partitions) {
            i += partition.length;

            groupRowIndexes.push(new mobiDoraTable.TableRowGroupRange(i - partition.length, i));
        }

        return groupRowIndexes;
    }

    /**
     * Moves all pivot list non-blank rows back to the source list.  At the end, the pivot list will contain
     * all blank rows
     */
    resetPivotRowsUi() {
        // Implementation:  The list always has TR rows in it which the top part contains 0 or more non-empty rows
        // while the rest contains empty rows.  Move all top non-empty rows out until an empty row appears then stop.
        //
        if (this.tbodyPivotList !== null && !isArrayEmpty(this.tbodyPivotList.rows)) {
            while (!this.tbodyPivotList.rows[0].hasAttribute(DRAG_ROW_BLANK)) {
                this.moveToSourceList(this.tbodyPivotList.rows[0].firstChild);
            }
        }
    }

    /**
     * Forces the pivot list to be configured to represent the pivot on the given column names
     *
     * NOTE: Any existing pivot columns are <i>reset</i> first before applying new pivot columns
     * NOTE: This does <b>not</b> actually perform the pivot option.  It only sets the GUI state.
     *
     * @param {string} columnNames
     */
    setPivotRowsUi(...columnNames) {
        this.resetPivotRowsUi();
        this.addPivotRowsUi(...columnNames);
    }

    /**
     * Adds to the pivot list the given columns referenced by name.
     *
     * NOTE: Any existing pivot columns are left untouched and new columns are appended at the end
     * NOTE: This does <b>not</b> actually perform the pivot option.  It only sets the GUI state.
     *
     * @param {string} columnNames
     */
    addPivotRowsUi(...columnNames) {
        if (!isArrayEmpty(columnNames)) {
            for (const columnName of columnNames) {
                // Find matching column name item from the available "source" list
                const tr = this.findColumnNameInList(columnName, this.tbodySourceList);

                // Add to the "pivot" list
                if (tr !== null) {
                    this.moveToPivotList(tr.firstChild, -1);
                }
            }
        }
    }

    /**
     * Creates the fully formatted and event enabled close icon for the dialog
     *
     * @returns {HTMLSpanElement}
     */
    createCloseIcon() {
        const outerThis = this;
        const spanCloseIcon = document.createElement("span");

        spanCloseIcon.innerHTML = "&#x2715";
        spanCloseIcon.style.float = "right";
        spanCloseIcon.style.border = "1px solid rgba(0,0,0,0.15)";
        spanCloseIcon.style.marginLeft = "8pt";
        spanCloseIcon.style.fontSize = "133%";
        spanCloseIcon.style.color = "gray";
        spanCloseIcon.addEventListener("mouseover", function(event) {
            spanCloseIcon.style.color = "black";
        });
        spanCloseIcon.addEventListener("mouseout", function(event) {
            spanCloseIcon.style.color = "gray";
        });
        spanCloseIcon.addEventListener("click", function(event) {
            outerThis.element.style.visibility = "hidden";
        });

        return spanCloseIcon;
    }

    /**
     * Adjust the dialog's height based on the current count of filter rows
     */
    adjustDialogHeight() {
        this.getElementRef().style.height = (36 + this.btnApply.offsetHeight + Math.max(this.tbodyPivotList.offsetHeight, this.tbodySourceList.offsetHeight)) + "px";
    }
}

/**
 * Represents a partition translated to direct table index which assists with UI pivot rendering
 *
 * @type {mobiDoraTable.TableRowGroupRange}
 */
mobiDoraTable.TableRowGroupRange = class {
    /**
     * Sole constructor.  Indexes are 0-based with 0 being the first row.
     *
     * @param start  start of range
     * @param end    end of range
     */
    constructor(start, end) {
        this._start = start;
        this._end = end;
    }

    getStart() {
        return this._start;
    }

    getEnd() {
        return this._end;
    }
}