if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

//
// Defines data structures and code for sorting HTML tables dynamically
//
// NOTE: the HTML <table> must have a <thead> section with <th> rows which is standard HTML to define a table's header

// Define class and methods in a namespace
// Ref: https://stackoverflow.com/questions/881515/how-do-i-declare-a-namespace-in-javascript

// HTML attribute that identifies an element that can have a sort indicator (ascending/descending)
const SORT_ORDER_ATTRIBUTE = "sort-order-indicator";

// HTML attribute that identifies an element for the sortable indicator
const SORTABLE_ATTRIBUTE = "sortable-indicator";

/**
 * Sort order ascending
 *
 * @type {number}
 */
const SORT_ORDER_ASCENDING = 1;

/**
 * Sort order original
 *
 * @type {number}
 */
const SORT_ORDER_ORIGINAL = 0;

/**
 * Sort order descending
 *
 * @type {number}
 */
const SORT_ORDER_DESCENDING = -1;

/**
 * Provides sorting state and functions after being attached to a table
 */
mobiDoraTable.TableSorter = class {
    /**
     * Sole constructor
     *
     * @param {mobiDoraTable.TableView}  [tableView=null]                 reference to table view which contains the table to attach to.  Can be null and use attachToTable() to attach later
     * @param {boolean} [enableOriginalSortOrder=false]    if true then enables 3-state sorting (Asc/Original/Desc) otherwise defaults to 2-state sorting (Asc/Desc)
     */
    constructor(tableView = null, enableOriginalSortOrder=false) {
        this.reset();
        this.tableView = tableView;
        this.enableOriginalSortOrder = enableOriginalSortOrder;

        /**
         * Stores partitions used for pivot cell grouping.
         *
         * Will be populated after a {@link sortByHeaderNames} operation.
         *
         * @type {mobiDoraTable.RowPartitions[]}
         * @see sortByHeaderNames
         */
        this.rowPartitionsList = new Array();
    }

    /**
     * Sorts the loaded table on the given column or columns in order.
     *
     * NOTE: Multicolumn sorting is supported and column order is important.
     * NOTE: Column order dictates sorting order and will affect resulting output.  First column will be sorted as
     * "primary sort", 2nd column as "secondary sort" and so on.
     *
     * @param {boolean} [cycleFirst=false]   set to <code>true</code> to cycle to next sort order first for the given column before sorting otherwise set to <code>false</code> to sort with column's current sort order
     * @param {mobiDoraTable.TableView.ColumnHeader} columnViewHeaders  1 or more {@see mobiDoraTable.TableView.ColumnHeader} objects representing columns to be sorted.  Sort order is important and 1st column will be sorted as primary followed by others in order.
     * @see reSort
     * @see mobiDoraTable.TableView.ColumnHeader  The constructor contains event handler which initiates sorting after user clicks on a TD table header cell
     */
    sortByHeaderNames(cycleFirst=false, ...columnViewHeaders) {
        // Sanity check
        //
        if (isArrayEmpty(columnViewHeaders)) {
            console.log("Cannot sort.  No columns specified.");
            return;
        }

        // The first sort will save the original sort order to an internal array which allows this TableSorter to
        // sort to original row order if requested.
        if (this.sortCount === 0) {
            this.copyCurrentRowsInto(this.originalSortRows);
        }

        // Save the sort columns list in case re-sorting is requested later
        this.sortColumns = new Array(...columnViewHeaders);

        // Increment the sort count
        this.sortCount++;

        // Clear final partitions list as it will be re-generated
        this.rowPartitionsList.length = 0;

        // Clear all visible sort order indicators and set new indicator on the given cell
        this.tableView.clearAllSortIndicators();

        // Create a working array copy of the original rows so not to affect original row order
        let sortingArray = this.copyCurrentRowsInto();

        // Sort the primary column directly because the primary column is *not* constrained by any previous
        // column's sort order
        this.sort(
            sortingArray,
            cycleFirst ? columnViewHeaders[0].cycleSortOrder() : columnViewHeaders[0].getCurrentSortOrder(),
            columnViewHeaders[0].currentPositionInTable());
        columnViewHeaders[0].showSortIndicator(columnViewHeaders[0].getCurrentSortOrder());

        // Sort remaining (non-primary) columns in given order using previous column's row partitions to sort w.r.t.
        // to previous column's sorting (see algorithm description below)
        //
        for (const columnViewHeader of columnViewHeaders) {
            let previousPartitions = (!isArrayEmpty(this.rowPartitionsList)) ? this.rowPartitionsList[this.rowPartitionsList.length - 1] : null;
            if (previousPartitions !== null) {
                // NOTE: Each column keeps its own state on its current/next sort order
                const sortOrder = cycleFirst ? columnViewHeader.cycleSortOrder() : columnViewHeader.getCurrentSortOrder();

                // Make visible the sort indicator for this column
                columnViewHeader.showSortIndicator(sortOrder);

                // ALGORITHM:
                // Sort the rows of the current column but under the partitions defined in the *previous* parent col-
                // umn. That is, each column is sorted but constrained to partitions groups defined in previous column.
                //
                // INDUCTION PROOF:
                // BASE CASE (colIndex = 0): First (primary) column is sorted entirely across all its rows because the
                // primary column does not have a parent column to inherit partitions. NOTE: sorting of primary column
                // already occurred above and outside of loop since it is the one-time base case.
                //
                // INDUCTION (colIndex > 0): Columns after the primary inherit partitions from previous column and sorts
                // itself within those partitions.  Furthermore, after its own sorting is completed, the current column
                // would introduce 0 or more of its own partitions depending on its individual values.  So the current
                // column's partitions become the next column's partitions and so on.
                //
                for (const previousPartition of previousPartitions.getPartitionsAll()) {
                    // Optimization: no need to sort if previous partition has been reduced to just a single row!  This
                    // can happen frequently during multicolumn sorts
                    if (!isArrayEmpty(previousPartition) && previousPartition.length > 1) {
                        // Sort current column's values within the previous (parent) column's partitions
                        this.sort(
                            previousPartition,  // partitions defined in *previous* column dictate sorted groups in current column
                            sortOrder,
                            columnViewHeader.currentPositionInTable()  // Sort in *current* column which is column n+1 where n is previous column's index
                        );
                    }
                }

                // Concatenated all the sub-sorted partition lists back to original list for cumulative sorting to be
                // saved between each column sort!
                sortingArray = previousPartitions.concatenate();
            }

            // Create the partitions used for sub-sorting from the sorted array
            // NOTE: the array *must* be sorted at this point in order for the partitions for the current column are a
            // union of:
            // (1) detected boundary between groups of cell values in current column
            // (2) inherited from previous column
            const partitions = this.partitionRows(sortingArray, previousPartitions, columnViewHeader.currentPositionInTable());

            // Store partitions as they are created because they are used (in order) for further column sorts (multi-
            // column sorting and later for UI pivot group generation
            this.rowPartitionsList.push(partitions);
        }

        console.log("Sorting completed on table column index: " + columnViewHeaders);
        // Uncomment to enable debugging console output
        // sortingArray.forEach((element) => {
        //     console.log(element.cells[columnName].innerHTML);
        // });

        // Remove TR records from the DOM and re-insert in new order
        //
        // NOTE: the existing TBODY elements are left intact as to not interfere with other table operations and
        // features that may hold references to them
        //
        $(this.tableView.tableRef).children("tbody").children("tr").remove();
        let tableBody = this.tableView.tableRef.tBodies[0];
        for (let i = 0; i < sortingArray.length; i++) {
            tableBody.appendChild(sortingArray[i]);
        }
    }

    /**
     * Performs the actual sort on the given rows.
     *
     * NOTE: This is an internal method and should not be called from outside this package
     *
     * @param rows           rows to be sorted
     * @param sortOrder      sort order  {see mobiDoraTable.}
     * @param columnIndex
     * @protected
     */
    sort(rows, sortOrder, columnIndex) {
        // Determine if this column has all numeric data or is a mix (alphanumeric) as this controls sort
        // comparison to produce natural sorting results
        const isNumeric = this.columnIsNumeric(columnIndex);

        // Sort using the Javascript Array object's built-in sort method with a custom comparator that will induce
        // the intended sorting order
        if (sortOrder !== SORT_ORDER_ORIGINAL) {
            // Sort based on "numeric" vs "alphanumeric" and sort according to the detected type
            if (!isNumeric) {
                //
                // Alphanumeric sort
                //
                rows.sort(function (a, b) {
                    if (sortOrder > SORT_ORDER_ORIGINAL) {
                        //
                        // Ascending sort
                        //
                        if (a.cells[columnIndex].innerHTML < b.cells[columnIndex].innerHTML) {
                            return -1;
                        } else if (a.cells[columnIndex].innerHTML > b.cells[columnIndex].innerHTML) {
                            return 1;
                        } else {
                            // Values are equal
                            return 0;
                        }
                    } else if (sortOrder < SORT_ORDER_ORIGINAL) {
                        //
                        // Descending sort
                        //
                        if (a.cells[columnIndex].innerHTML > b.cells[columnIndex].innerHTML) {
                            return -1;
                        } else if (a.cells[columnIndex].innerHTML < b.cells[columnIndex].innerHTML) {
                            return 1;
                        } else {
                            // Values are equal
                            return 0;
                        }
                    }
                });
            } else {
                //
                // Numeric sort
                //
                rows.sort(function (a, b) {
                    if (sortOrder > SORT_ORDER_ORIGINAL) {
                        //
                        // Ascending sort
                        //
                        if (Number(a.cells[columnIndex].innerHTML) < Number(b.cells[columnIndex].innerHTML)) {
                            return -1;
                        } else if (Number(a.cells[columnIndex].innerHTML) > Number(b.cells[columnIndex].innerHTML)) {
                            return 1;
                        } else {
                            // Values are equal
                            return 0;
                        }
                    } else if (sortOrder < SORT_ORDER_ORIGINAL) {
                        //
                        // Descending sort
                        //
                        if (Number(a.cells[columnIndex].innerHTML) > Number(b.cells[columnIndex].innerHTML)) {
                            return -1;
                        } else if (Number(a.cells[columnIndex].innerHTML) < Number(b.cells[columnIndex].innerHTML)) {
                            return 1;
                        } else {
                            // Values are equal
                            return 0;
                        }
                    }
                });
            }
        } else {
            // TODO: Implementation needs to be updated for new design
            // NO SORT: The original sort order was specified so use the contents of the original array
            this.copyArray(this.originalSortRows, rows);
        }
    }

    /**
     * Partitions the given rows based on constraints of the previous partition and the grouping of rows
     *
     * @param {HTMLElement[]} sourceRows   the source TR rows
     * @param {mobiDoraTable.RowPartitions} previousPartitions   the array of TR rows to use as source rows
     * @param {number} columnIndexToSort  zero-index of the column to group on or use -1 to perform no grouping which is used for primary sort
     * @returns {mobiDoraTable.RowPartitions}
     * @see concatenatePartitions
     */
    partitionRows(sourceRows, previousPartitions, columnIndexToSort) {
        // Initialize the list of partitions which will accumulate new partitions
        // NOTE: The last element is always the "current" partition.
        // NOTE: This is an "array of arrays"
        const partitions = new mobiDoraTable.RowPartitions(columnIndexToSort);
        const partitionEdges = (!isArrayEmpty(previousPartitions)) ? previousPartitions.calcPartitionEdges() : new Array();

        let currentGroupValue = null;
        let rowIndex = 0;
        for (const tr of sourceRows) {
            const td = tr.childNodes[columnIndexToSort];

            if (td.innerText !== currentGroupValue || partitionEdges.includes(rowIndex)) {
                // Change in value detected or reached a partition edge. So start a new current partition.
                partitions.startNewPartition();
                currentGroupValue = td.innerText;
            }

            // Add row to the current partition
            partitions.getCurrentPartition().push(tr);

            rowIndex++;
        }

        // The rows are now partitioned by constraints of previous partitions and current values
        return partitions;
    }

    /**
     * Returns true if the column's data contains only blank or numeric data and false if it contains alphanumeric
     * data.  This determines proper sort order for the given data as they both sort differently.
     *
     * @param {number} columnIndex  index of column to check
     * @returns {boolean}
     */
    columnIsNumeric(columnIndex) {
        // Linear search.  Return on first match.
        let tbody = this.tableView.tableRef.tBodies[this.tableView.tableRef.tBodies.length - 1];
        for (let i = 0; i < tbody.rows.length; i++) {
            if (isNaN(tbody.rows[i].cells[columnIndex].innerHTML)) {
                // Non numeric value found so return non-numeric result imediately.
                return false;
            }
        }

        // If this point is reached, ony numeric or blank data was found with the specified table column index
        return true;
    }

    /**
     * Re-sorts the table on the column's current sort settings.
     *
     * PIVOT PRIORITY: If pivoting is enabled then this will sort with pivot columns.  Otherwise it falls
     * back to normal non-pivot sorting.
     *
     * @see sortByHeaderNames
     */
    reSort() {
        // Determine which sorting columns to use depending on if pivot mode is enabled
        if (this.tableView.isPivotingActive()) {
            this.sortByHeaderNames(false, ...this.tableView.groupingDropdownBox.pivotColumnHeaders);
        } else {
            // TODO: Coordinate with multi-column non-pivot mode sorting here when that feature becomes available
            this.sortByHeaderNames(false, ...this.tableView.columnHeaders);
        }
    }

    /**
     * Saves the table's rows to the given array
     *
     * @param {HTMLElement[]} [array=null] (optional) array to populate. If not specified then a new array is created and returned
     */
    copyCurrentRowsInto(array = null) {
        let tableRows = this.tableView.tableRef.tBodies[this.tableView.tableRef.tBodies.length - 1].rows;
        if (!isArrayEmpty(array)) {
            this.copyArray(tableRows, array);

            return array;
        } else {
            const newArray = new Array();
            this.copyArray(tableRows, newArray);

            return newArray;
        }
    }

    /**
     * Shallow copies the contents of array a1 to a2.  The element references in each array are the same but
     * changing the order of a1 will will not affect a2 and vice-versa since they are listed in separate arrays.
     *
     * @param {Object} a1  source array
     * @param {Object} a2  destination array
     */
    copyArray(a1, a2) {
        for (let i = 0; i < a1.length; i++) {
            a2.push(a1[i]);
        }
    }

    /**
     * Resets the sort state
     */
    reset() {
        // Clear search header annotations otherwise user can get double "up arrow" or "down arrow" annoations while
        // performing sorts
        if (typeof this.mapSortColumns !== "undefined" && this.mapSortColumns !== null && this.tableView.tableRef !== null) {
            for (const [key, value] of this.mapSortColumns) {
                if (value !== null) {
                    value.nextSortOrder = 1;

                    this.tableView.tableRef.tHead.rows[0].cells[key].innerHTML = value.headerLabel;
                }
            }
        }

        this.mapSortColumns = new Map();
        this.sortCount = 0;
        this.originalSortRows = new Array();
        this.sortColumns = new Array();    // Stores the current column names under sort
        this.enableOriginalSortOrder = false;
    }
}

/**
 * Maintains an <b>ordered</b> list of row "partitions" that are used to calculate multi-column sorting and used with
 * table row pivots.
 *
 * Use {@see mobiDoraTable.RowPartitions.startNewPartition} to create a new partition as necessary then use
 * {@see mobiDoraTable.RowPartitions.getCurrentPartition} and push new entries in.
 *
 * @type {mobiDoraTable.RowPartitions}
 */
mobiDoraTable.RowPartitions = class {
    /**
     * Creates a new blank partition list
     *
     * @param {number} colIndex   the table's view column index this partition will be generated from
     */
    constructor(colIndex) {
        // Initial partitions list is an empty array that will be populated by partitions which are sub-arrays
        //
        // NOTE: this is an "array of arrays" which maintains a list of "partitions" which itself is an array
        this.partitions = new Array();

        // Save the column index which partitioning edge detection generated from
        this.colIndex = colIndex;
    }

    /**
     * Returns all partitions and their rows currently in this list
     *
     * @returns {HTMLElement[][]}
     */
    getPartitionsAll() {
        return this.partitions;
    }

    /**
     * Calculates a linear list of row edges of each partition where the end of each partition forms an "edge".
     *
     * This allows sub-sorting to know the boundaries of the current sort based on these positions of these edges.
     *
     * @returns {number[]}  list of 0-index based edges based on the original source table
     */
    calcPartitionEdges() {
        const partitionEdges = new Array();

        let rowIndex = 0;
        if (!isArrayEmpty(this.partitions)) {
            let currentGroupValue = null;
            for (const partition of this.partitions) {
                for (const tr of partition) {
                    const td = tr.childNodes[this.colIndex];

                    if (td.innerText !== currentGroupValue) {
                        // Edge detected
                        partitionEdges.push(rowIndex);
                        currentGroupValue = td.innerText;
                    }

                    // Continue iterating through the rows as a continuous index through all partitions since the
                    // index is in the domain of the source table rows
                    rowIndex++;
                }

                // End of a partition is also an partition edge
                partitionEdges.push(rowIndex);
            }
        }

        // Remove first edge since it is the top of the table and is not an edge that affects grouping within the
        // table
        partitionEdges.shift();
        partitionEdges.pop();

        // Return unique list of edges (remove duplicates)
        // NOTE: the list could be empty if there is only a single partition
        return _.uniq(partitionEdges);
    }

    /**
     * Internally creates a new blank partition in the list
     *
     * This method is critical to proper sorting/pivoting and must be called whenever a row boundary is encountered
     * during sorting
     *
     * @param {HTMLElement[]} [rows = null] (optional) initialize the new partition with rows otherwise new partition is empty
     * @see getCurrentPartition
     */
    startNewPartition(rows=null) {
        if (isArrayEmpty(rows)) {
            // Push a new empty partition to the end of the list
            this.partitions.push(new Array());
        } else {
            // Push given rows to initialize new partition before adding partition to the end of the list
            this.partitions.push(new Array(...rows));
        }
    }

    /**
     * Returns the current row partition
     *
     * @returns {HTMLElement[]}
     * @see startNewPartition
     */
    getCurrentPartition() {
        return this.partitions[this.partitions.length - 1];
    }

    /**
     * Concatenates all current partitions and their TR data into in order into a single list. This is used to complete
     * a sorting operation and generate the resulting ordered row
     *
     * @returns {HtmlElement[]}
     */
    concatenate() {
        const concatenatedList = new Array();

        // Iterate through each *array* element
        for (const partition of this.partitions) {
            // Flatten each array out before pushing into the concatenated list
            concatenatedList.push(...partition);
        }

        return concatenatedList;
    }
}