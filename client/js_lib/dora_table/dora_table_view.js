if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

/**
 * Draws the UI for user to see and interact with the model data
 */
mobiDoraTable.TableView = class {
    /**
     * Sole constructor
     *
     * @param {mobiDoraTable.TableModel}   tableModel  the table model containing describing & containing data to be displayed
     * @param {HTMLElement} parentRef      the parent element the table component should be displayed under (usually a <div> on the page)
     * @param {boolean} [enableSort=true]  enable/disable table sorting feature
     * @param {boolean} [enableFiltering=true]
     * @param {boolean} [enableColumnHiding=true]
     * @param {boolean} [enablePivoting=true]
     * @param {boolean} [enableExporting=true]
     * @param {function} [fnPostPivot=null]   an optional function callback to run after user initiated pivot
     */
    constructor(tableModel, parentRef, enableSort=true, enableFiltering=true, enableColumnHiding=true, enablePivoting=true, enableExporting=true, fnPostPivot = null) {
        const outerThis = this;
        this.tableModel = tableModel;
        this.parentRef = parentRef;

        /**
         * Stores the current view-level columns & and ordering with {@see mobiDoraTable.ColumnHeader} records
         *
         * NOTE:
         * This is an important field used throughout the rest of the table module code
         *
         * NOTE:
         * The physical ordering of the column objects in this array also controls the ordering in the page view.
         * That is, to chance the column order, first change the ordering in this array and re-render the headers
         * and re-render the table and both header and table body will use this array to render themselves in the
         * given order.
         */
        this.columnHeaders = new Array();
        // Default column order to original table model column order
        for (const columnHeader of this.tableModel.getColumns()) {
            this.columnHeaders.push(new mobiDoraTable.ColumnHeader(this, columnHeader));
        }

        // This is the one single instance of the filter dropdown box for this table.  No other instances per
        // table should be used.
        //
        // NOTE: the popup must be attached to its parent button below with appendChild()
        //
        this.filterDropdownBox = new mobiDoraTable.FilterDropdownBox(this);

        // This is the one single instance of the column hiding dropdown box for this table.  No other instances per
        // table should be used.
        //
        // NOTE: the popup must be attached to its parent button below with appendChild()
        //
        this.columnHidingDropdownBox = new mobiDoraTable.ColumnHidingDropdownBox(this);

        // This is the one single instance of the table grouping dropdown box for this table.  No other instances per
        // table should be used.
        //
        // NOTE: the popup must be attached to its parent button below with appendChild()
        //
        this.groupingDropdownBox = new mobiDoraTable.GroupingDropdownBox(this, fnPostPivot);

        // This contains the spinner that can be made visible for when long-running table pivot operation is underway
        // See initialization and usage information described later.
        this.spanPivotSpinner = document.createElement("span");

        // Automatically attach this view as an observer of the table model
        tableModel.addObserver(this);

        // Create HTML table view elements
        //

        // Filter button configuration
        const imgFilter = document.createElement("img");
        imgFilter.src = "images/filter.svg";
        imgFilter.setAttribute("height", "10pt");
        this.btnFilter = document.createElement("button");
        this.btnFilter.innerHTML = "Filter&nbsp;";
        this.btnFilter.style.fontSize = "8pt";
        this.btnFilter.appendChild(imgFilter);
        this.btnFilter.addEventListener(
            "click",
            function(event) {
                if (event.target === outerThis.btnFilter || event.target === imgFilter) {
                    if (outerThis.filterDropdownBox.getElementRef().style.visibility === "hidden") {
                        console.info("Showing table filter popup...");
                        outerThis.filterDropdownBox.getElementRef().style.visibility = "visible";
                        outerThis.filterDropdownBox.adjustDialogHeight();

                        // Close the column hiding dialog since only one dialog at max should be showing at one time
                        outerThis.columnHidingDropdownBox.getElementRef().style.visibility = "hidden";
                        outerThis.groupingDropdownBox.getElementRef().style.visibility = "hidden";
                    } else {
                        // COMMENTING OUT BECAUSE EVENT BUBBLING FROM CHILD ELEMENTS IS ACTIVATING THIS CODE
                        // console.info("Hiding table filter popup...");
                        // outerThis.filterDropdownBox.getElementRef().style.visibility = "hidden";
                    }
                }
            });
        this.btnFilter.addEventListener("keydown", function(event) {
            if (event.key === "Escape") {
                outerThis.filterDropdownBox.element.style.visibility = "hidden";
            }
        });

        // Column "Hide" button configuration
        this.spanHiddenColumns = document.createElement("span");
        this.spanHiddenColumns.className = "tableStatusIndicator01";
        this.spanHiddenColumns.style.visibility = "hidden";
        this.spanHiddenColumns.addEventListener("click", function(event) {
            outerThis.columnHidingDropdownBox.btnShowAll.click();
        });
        this.spanHiddenColumns.addEventListener("mouseover", function(event) {
            outerThis.spanHiddenColumns.style.cursor = "pointer";
            outerThis.spanHiddenColumns.style.textDecoration = "underline";
        });
        this.spanHiddenColumns.addEventListener("mouseout", function(event) {
            outerThis.spanHiddenColumns.style.cursor = "";
            outerThis.spanHiddenColumns.style.textDecoration = "";
        });
        this.btnHideColumns = document.createElement("button");
        this.btnHideColumns.innerHTML = "Hide Columns";
        this.btnHideColumns.style.fontSize = "8pt";
        this.btnHideColumns.style.marginLeft = "8pt";
        this.btnHideColumns.addEventListener(
            "click",
            function(event) {
                if (event.target === outerThis.btnHideColumns) {
                    if (outerThis.columnHidingDropdownBox.getElementRef().style.visibility === "hidden") {
                        console.info("Showing table column hiding popup...");
                        outerThis.columnHidingDropdownBox.getElementRef().style.visibility = "visible";
                        outerThis.columnHidingDropdownBox.adjustDialogHeight();

                        // Close the filter dialog since only one dialog at max should be showing at one time
                        outerThis.filterDropdownBox.getElementRef().style.visibility = "hidden";
                        outerThis.groupingDropdownBox.getElementRef().style.visibility = "hidden";
                    } else {
                        // COMMENTING OUT BECAUSE EVENT BUBBLING FROM CHILD ELEMENTS IS ACTIVATING THIS CODE
                        // console.info("Hiding table filter popup...");
                        // outerThis.filterDropdownBox.getElementRef().style.visibility = "hidden";
                    }
                }
            });
        this.btnHideColumns.addEventListener("keydown", function(event) {
            if (event.key === "Escape") {
                outerThis.columnHidingDropdownBox.element.style.visibility = "hidden";
            }
        });

        // Table grouping & pivoting
        this.btnPivot = document.createElement("button");
        this.btnPivot.innerHTML = "Pivot Rows";
        this.btnPivot.style.fontSize = "8pt";
        this.btnPivot.style.marginLeft = "8pt";
        this.btnPivot.addEventListener(
            "click",
            function(event) {
                if (event.target === outerThis.btnPivot) {
                    if (outerThis.groupingDropdownBox.getElementRef().style.visibility === "hidden") {
                        outerThis.groupingDropdownBox.getElementRef().style.visibility = "visible";
                        outerThis.groupingDropdownBox.adjustDialogHeight();

                        // Close the filter dialog since only one dialog at max should be showing at one time
                        outerThis.filterDropdownBox.getElementRef().style.visibility = "hidden";
                        outerThis.columnHidingDropdownBox.getElementRef().style.visibility = "hidden";
                    } else {
                        // COMMENTING OUT BECAUSE EVENT BUBBLING FROM CHILD ELEMENTS IS ACTIVATING THIS CODE
                        // console.info("Hiding table filter popup...");
                        // outerThis.filterDropdownBox.getElementRef().style.visibility = "hidden";
                    }
                }
            });
        this.btnPivot.addEventListener("keydown", function(event) {
            if (event.key === "Escape") {
                outerThis.groupingDropdownBox.element.style.visibility = "hidden";
            }
        });

        // Export data button
        const btnExport = document.createElement("button");
        btnExport.style.float = "right";
        btnExport.style.fontSize = "9pt";
        btnExport.innerHTML = "Export Rows";
        btnExport.addEventListener("click", function(event) {
            exportDataTSV($(outerThis.tableRef));
        });

        this.spanFilteredOut = document.createElement("span");
        this.spanFilteredOut.className = "tableStatusIndicator01";
        this.spanFilteredOut.style.visibility = "collapse";
        this.spanFilteredOut.addEventListener("click", function(event) {
           outerThis.resetTableRows(true);
           outerThis.spanFilteredOut.style.visibility = "collapse";

           // Apply pivoting only if it is currently active
           outerThis.groupingDropdownBox.applyPivotIfVisible(false);
        });
        this.spanFilteredOut.addEventListener("mouseover", function(event) {
            outerThis.spanFilteredOut.style.cursor = "pointer";
            outerThis.spanFilteredOut.style.textDecoration = "underline";
        });
        this.spanFilteredOut.addEventListener("mouseout", function(event) {
            outerThis.spanFilteredOut.style.cursor = "";
            outerThis.spanFilteredOut.style.textDecoration = "";
        });

        this.spanPivotStatus = document.createElement("span");
        this.spanPivotStatus.className = "tableStatusIndicator01";
        this.spanPivotStatus.style.visibility = "collapse";
        this.spanPivotStatus.addEventListener("click", function(event) {
            //
            // Turn off row pivoting
            //

            // Make visible and re-attach all columns
            for (const header of outerThis.columnHeaders) {
                header.visible = true;
                header.detached = false;
            }

            // Reset column ordering to original order and make all columns visible & attached again
            outerThis.columnsResetOrder(true);

            // Reset row data from model
            outerThis.resetTableRows(false);

            // Re-apply row filtering if it is currently enabled
            outerThis.filterDropdownBox.applyFilterIfVisible();

            // Show/hide column can be re-enabled
            outerThis.btnHideColumns.disabled = false;

            // Hide the pivot status indicator
            outerThis.spanPivotStatus.style.visibility = "collapse";

            // Column sort indicators are no longer valid so clear them
            outerThis.clearAllSortIndicators();
        });
        this.spanPivotStatus.addEventListener("mouseover", function(event) {
            outerThis.spanPivotStatus.style.cursor = "pointer";
            outerThis.spanPivotStatus.style.textDecoration = "underline";
        });
        this.spanPivotStatus.addEventListener("mouseout", function(event) {
            outerThis.spanPivotStatus.style.cursor = "";
            outerThis.spanPivotStatus.style.textDecoration = "";
        });

        this.tableRef = document.createElement("table");
        this.borderRef = document.createElement("div");
        this.borderRef.style.border = "1px solid rgba(0,0,0,0.25)";
        this.borderRef.style.borderRadius = "2px";
        this.borderRef.style.verticalAlign = "middle";
        // Add table toolbar features but only if specified.
        //
        // NOTE: Some smaller tables like the Balance popup search do not need these features and their buttons are
        // visual clutter for a physically small table
        //
        if (enableFiltering) {
            this.borderRef.appendChild(this.btnFilter);

            this.btnFilter.appendChild(this.filterDropdownBox.getElementRef());
        } else {
            console.info("Not enabling table filtering");
        }
        if (enableColumnHiding) {
            this.borderRef.appendChild(this.btnHideColumns);
            this.btnHideColumns.appendChild(this.columnHidingDropdownBox.getElementRef());
        } else {
            console.info("Not enabling column hiding");
        }
        if (enablePivoting) {
            this.borderRef.appendChild(this.btnPivot);
            this.btnPivot.appendChild(this.groupingDropdownBox.getElementRef());
        } else {
            console.info("Not enabling pivot grouping");
        }
        if (enableExporting) {
            this.borderRef.appendChild(btnExport);
        } else {
            console.info("Not enabling pivot grouping");
        }

        // Add status indicators
        //
        // NOTE: These indicators are initially "invisible" until their associated feature is activated
        //
        if (this.btnFilter.parentElement !== null) {
            // Append associated indicator tag only if parent button is attached to the DOM
            this.borderRef.appendChild(this.spanFilteredOut);
        }
        if (this.btnHideColumns.parentElement !== null) {
            // Append associated indicator tag only if parent button is attached to the DOM
            this.borderRef.appendChild(this.spanHiddenColumns);
        }
        if (this.btnPivot.parentElement !== null) {
            // Append associated indicator tag only if parent button is attached to the DOM
            this.borderRef.appendChild(this.spanPivotStatus);
        }

        // Layout and initialize pivot activity spinner -- initially is hidden
        this.borderRef.appendChild(this.spanPivotSpinner);
        this.spanPivotSpinner.display = "collapse";
        this.spanPivotSpinner.style.visibility = "hidden";
        const imgPivotSpinner = document.createElement("img");
        imgPivotSpinner.src = "images/spinner-squares-anim.svg";
        imgPivotSpinner.style.height = "16pt";
        imgPivotSpinner.style.marginLeft = "8pt";
        imgPivotSpinner.style.marginRight = "8pt";
        imgPivotSpinner.style.verticalAlign = "middle";
        this.spanPivotSpinner.appendChild(imgPivotSpinner);
        const spanPivotingStatus = document.createElement("span");
        spanPivotingStatus.className = "blinkPulsating";
        spanPivotingStatus.innerHTML = "Applying pivot...";
        this.spanPivotSpinner.appendChild(spanPivotingStatus);

        this.borderRef.appendChild(this.tableRef);
        this.parentRef.appendChild(this.borderRef);

        // Render the table column headers based on the table model
        this.renderTableHeaders();

        // Attach sorter if requested to.  Attach only *after* the table's headers are defined because the
        // sorter uses the table's headers to self-configure.
        if (enableSort) {
            this.tableSorter = new mobiDoraTable.TableSorter(this);
        } else {
            this.tableSorter = null;
        }

        this.tableRef.style.border = "1";
        this.tableRef.className = "outputTable";
        this.tableBody = this.tableRef.createTBody();
    }

    /**
     * Returns reference to the wrapped underlying {@see HTMLTableElement} element
     *
     * @returns {HTMLTableElement}
     */
    getTable() {
        return this.tableRef;
    }

    /**
     * Returns an array of TR references to the current filter rows
     *
     * @returns {HTMLTableRowElement[]}  a zero or more size array containing TR references to the filter rows
     */
    getFilterRows() {
        return searchForDomElements(
            this.filterDropdownBox.getElementRef(),
            function (currentElement) {
                return currentElement.tagName === "TR" && typeof(currentElement.hasAttribute) === "function" && currentElement.hasAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW)
            }
        );
    }

    // Creates the visible table header's based on columned defined in the table's model
    // NOTE: Any existing headers are replaced
    //
    renderTableHeaders() {
        if (this.tableModel !== null) {
            // Delete table's current THEAD and header row in preparation to re-create
            this.tableRef.deleteTHead();
            const rowHeader = this.tableRef.createTHead().insertRow(-1);

            // Attach the individual TH header cells if they are visible and not detached
            for (const columnView of this.columnHeaders) {
                if (columnView !== null) {
                    if (columnView.visible && !columnView.detached) {
                        rowHeader.appendChild(columnView.headerThCell);
                    }
                } else {
                    console.warn("Got null column in column list");
                }
            }
        } else {
            console.warn("No TableModel: Cannot render table column headers.");
        }
    }

    /**
     * Renders a row of data to the HTML view
     *
     * @param {mobiDoraTable.Cell[]} data  a row record array consistent with the columns
     * @param {boolean} [applyColumnFilter=false] <code>true</code> will draw with column filtering applied and <code>false</code> disables
     *                          column filtering.  Column filter should be enabled except when the raw full table with
     *                          all columns is required such as when re-applying a column filter from scratch because all
     *                          the original columns are needed to column filter correctly.
     * @return the newly inserted <TR> element
     */
    addRow(data, applyColumnFilter = false) {
        // Create the new TR record for the given data
        const tr = this.createTableDataRow(data, applyColumnFilter);

        // Add own index to the row's attribute which allows referencing this row eventhough the
        // rows may later be shuffled by a table sorter
        tr.setAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW_ORIG_IDX, this.tableBody.childElementCount.toString());

        // Add the row to the DOM which immediately makes it viewable to the user
        this.tableBody.appendChild(tr);

        return tr;
    }

    /**
     * Clears the table UI of the rows by detaching from the DOM
     *
     * NOTE: external references to TR rows are still valid and can be processed & re-attached later to update the table
     * with new sorted, updated, or filtered row data.
     */
    clearUiRows() {
        // NOTE: This can also be accomplished with a JQuery expression: $(this.tableBody).empty() -- OR -- $(this.tableBody).find("tr").remove();

        // Redraw table headers which also clears any existing header cells
        this.renderTableHeaders()

        // Remove body rows
        removeAllChildNodes(this.tableBody);
    }

    applyAll() {
        this.columnHidingDropdownBox.applyColumnHiding();

        if (this.isFilteringActive()) {
            this.filterDropdownBox.btnApply.click();
        }
    }

    /**
     * <b>Replaces</b> the table's current rows with an arbitrary set of TR rows
     *
     * NOTE: All current rows are first removed before setting in new TR rows
     * NOTE: Column hiding for pivoting is performed here so the column ordering definition must be set <b>first</b>
     *       prior to calling this method otherwise
     *
     * @param {HTMLTableRowElement[]} rows  array of table rows to overwrite the table view with
     * @param {boolean} [reapplySorting=true]  optionally re-apply any sorting
     */
    setRows(rows, reapplySorting=true) {
        // Clear viewed rows in preparation for replacement
        this.clearUiRows();

        // Update UI table with the new rows
        for (const row of rows) {
            // Show/hide pivot-specified or column hiding dialog-specified columns to the row
            this.columnHidingDropdownBox.applyColumnHidingToRow(row);

            // Append the new row
            this.tableBody.appendChild(row);
        }

        // Reapply current sorting to maintain consistent view but only if pivoting not enabled because pivoting per-
        // forms its own more complicated multi-column sorting
        if (reapplySorting) {
            if (typeof (this.tableSorter) !== "undefined" && this.tableSorter !== null) {
                this.tableSorter.reSort();
            }
        }
    }

    /**
     * Restores the original unfiltered rows to the table UI view
     */
    restoreOriginalRows() {
        this.clearUiRows();

        // Restore view data directly from the backing model
        this.resetTableRows(false);
    }

    /**
     * Removes all table filtering and history
     */
    clearFilter() {
        this.spanFilteredOut.style.visibility = "collapse"; // hide UI filter indicator
    }

    /**
     * Creates the actual TR HTML row element from the given table data cell array
     *
     * NOTE: This method also contains the function that <i>re-orders</i> the cells in the row to match the
     * current pivot column order
     *
     * @param {mobiDoraTable.Cell[]} dataCells  an array of {@link mobiDoraTable.Cell} objects representing the table row data
     * @param {boolean} [applyColumnFilter=true] <code>true</code> will draw with column filtering applied and <code>false</code> disables
     *                          column filtering.  Column filter should be enabled except when the raw full table with
     *                          all columns is required such as when re-applying a column filter from scratch because all
     *                          the original columns are needed to column filter correctly.
     * @returns {HTMLTableRowElement}   fully constructed TR row with TD cells ready for insertion into DOM
     */
    createTableDataRow(dataCells, applyColumnFilter = false) {
        const tr = document.createElement("tr");

        // Cells must be re-ordered to match the current column order
        const reorderedCells = this.reorderCellsForCurrentColumnOrder(dataCells);

        let i = 0;
        for (const cell of reorderedCells) {
            if (typeof cell !== "undefined") {
                // Default cell text alignment unless overridden below
                let textAlign = "left"

                // Get the column view at the index which allows the code to get the original column even if the actual
                // columns are shuffled different than original column order
                const columnView = this.columnHeaders[i];
                const originalIndex = columnView.modelColumn.originalIndex;  // Get original index which allows retrieving the actual column even through shuffled column order
                if (!applyColumnFilter || columnView.visible && !columnView.detached) {
                    let label = "";
                    switch (this.tableModel.getColumnType(originalIndex).type) {
                        case mobiDoraTable.ColumnType.FLOAT:
                            label = floatRound(cell.data, 2);
                            textAlign = "right";  // Align numbers to right alignment to align on decimal
                            break;
                        case mobiDoraTable.ColumnType.INTEGER:
                            label = cell.data;
                            textAlign = "right";  // Align numbers to right alignment to align on decimal
                            break;
                        default:
                            label = cell.data;
                            break;
                    }

                    // Add data to the table which will make it visible to user after the row is attached to the table.
                    //
                    // NOTE: The data is *not* automatically escaped for HTML which allows the caller to control if HTML
                    // should be displayed or not.  Callers must escape the data as needed.
                    tr.appendChild(createDataTableCell(label, `${label}`, cell.tooltip, this.tableModel.getColumnType(originalIndex).name, textAlign));
                } else {
                    // Guard against fatal Javascript runtime error
                    console.warn("CRITICAL WARNING: Cell was undefined.  Client code must create exactly 1 cell object for each client defined table column.");
                }
            }

            i++;
        }

        return tr;
    }

    /**
     * Re-orders that data cell for the current column ordering
     *
     * @param {mobiDoraTable.Cell[]} dataCells
     * @returns {mobiDoraTable.Cell[]}
     */
    reorderCellsForCurrentColumnOrder(dataCells) {
        const updatedDataCellOrdering = new Array();

        // Iterate through the current user described column ordering and place the cells in the user's order
        for (const columnHeader of this.columnHeaders) {
            updatedDataCellOrdering.push(dataCells[columnHeader.modelColumn.originalIndex]);
        }

        return updatedDataCellOrdering;
    }

    /**
     * Sorts the current table column header list in the order to match the ordering of the given column names.
     *
     * NOTE: this mutates the original column header list
     * NOTE: if the input column list does not specify all columns then those unspecified column remain at the end the
     * header list in their original order.
     *
     * @param {string} columnNames  columns will be ordered to the order of the names listed here
     */
    reorderColumnHeaders(...columnNames) {
        // Temporary array to store sorted columns
        const reorderedPrefixArray = new Array();

        console.info(`Sorting column headers to order specified by: ${columnNames}`)

        // Move the matching columns one-by-one by name and in order to the temporary header array
        //
        for (const colName of columnNames) {
            try {
                const columnHeader = this.findColumnHeaderByName(colName, true);

                if (columnHeader !== null) {
                    reorderedPrefixArray.push(columnHeader);
                } else {
                    // Shouldn't happen but very bad if it does
                    console.error(`FATAL: Column header for given name "${colName} was null.  Cannot process."`);
                }
            } catch (e) {
                // Fatal.  Should put debug breakpoint is the find column method to check why no match as this should
                // match everytime.
                console.error(`FATAL: Could not find column by name "${colName} because of error: ${e}"`);
            }
        }

        // Prepend the ordered header array to the front of the source array completing the sort by name
        // operation
        //
        this.columnHeaders.unshift(...reorderedPrefixArray);

        console.info("Sorted column headers to the following order: ");
        let i = 0;
        for (const column of this.columnHeaders) {
            if (column !== null) {
                console.info(column.modelColumn.name);
            } else {
                console.warn(`WARN: Got empty column object in column list at index: ${i}`);
            }

            i++;
        }
    }

    /**
     * Finds the {@see ViewColumn} header object that matches the given name
     *
     * @param {string} columnName
     * @param {boolean} remove the found header object also (optional) (default = false)
     * @returns {null|ViewColumn}
     */
    findColumnHeaderByName(columnName, remove = false) {
        // Linear search.  Return on first match.
        let i = 0;
        for (const columnViewHeader of this.columnHeaders) {
            if (columnViewHeader.modelColumn.name === columnName) {
                if (remove) {
                    this.columnHeaders.splice(i , 1);
                }

                return columnViewHeader;
            }

            i++;
        }

        // No match found if this point is reached.  Throw exception because this is fatal and cannot continue. Do not
        // return a null as that will cause major problems later.
        throw new Error(`Cannot find column by given name "${columnName}"`);
    }

    /**
     * Get a list of all {@see mobiDoraTable.ColumnHeader} objects matching the given names
     *
     * NOTE: If the specified name does <i>not</i> match any column record then it will not be included in the list and
     *       no error is generated.
     *
     * @param {string} columnNames  1 or more column names
     * @return {string[]}  a string array of zero or more matching
     */
    findColumnHeadersAll(...columnNames) {
        const results = new Array();

        if (!isArrayEmpty(columnNames)) {
            // Find matching column header records and push (in order) to results array
            for (const columnName of columnNames) {
                // Linear search.  Break on first match.
                for (const columnHeader of this.columnHeaders) {
                    if (columnHeader.modelColumn.name === columnName) {
                        results.push(columnHeader);
                        break;
                    }
                }
            }
        }

        // Provide warning if results array length does not match selected column count
        if (columnNames !== null && results.length < columnNames.length) {
            console.warn(`Only found ${results.length} out of the requested ${columnNames.length} header names`);
        }

        return results;
    }

    /**
     * Re-reads the data model and updates the view row with the data model row at the same index.
     *
     * Can use this to update the view with a single row update change for efficient UI processing.  Alternatively,
     * you can always re-draw but that can be slow for a large table.
     *
     * @param {number} modelIndex  refers to the model's original row index
     */
    replaceRow(modelIndex) {
        // IMPLEMENTATION NOTE: This will work even if the UI rows are sorted because the TR element was originally
        // tagged with their original model index position in the element's attribute.
        //
        for (const row of this.tableBody.childNodes) {
            // Linear search. Break on first match.
            //
            if (row.hasAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW_ORIG_IDX)) {
                if (row.getAttribute(mobiDoraTable.TAG_ATTRIB_FILTER_ROW_ORIG_IDX).toString() === modelIndex.toString()) {
                    // Insert replacement table row TR before current one just prior to removing the old row -- which
                    // is equivalent to updating the existing row.
                    this.tableBody.insertBefore(this.createTableDataRow(this.tableModel.getRow(modelIndex)), row);
                    row.remove();

                    // No further processing needed
                    return;
                }
            }
        }
    }

    /**
     * Resets the table row data back to the original rows of the model.
     *
     * Typically, this is useful to clear the temporary view rows that are a result of a filter operation.  This is
     * especially needed when applying a new filtering operation since each filtering operation is not a cumulative op-
     * eration.
     *
     * @param {boolean} [applyColumnFilter=true] <code>true</code> will draw with column filtering applied and <code>false</code> disables
     *                          column filtering.  Column filter should be enabled except when the raw full table with
     *                          all columns is required such as when re-applying a column filter from scratch because all
     *                          the original columns are needed to column filter correctly.
     */
    resetTableRows(applyColumnFilter = false) {
        // Clear existing data rows in the TBODY only in preparation for redraw.  The table headers cells
        // are untouched because those rows are in the THEAD.
        this.clearUiRows();

        // Render all data model rows if any
        if (!isArrayEmpty(this.tableModel.getRows())) {
            for (const data of this.tableModel.getRows()) {
                this.addRow(data, applyColumnFilter);
            }
        }
    }

    /**
     * Detaches the column by given name
     *
     * @param {string} columnName  name of column to detach
     * @see columnsAttachByName
     */
    columnsDetachByName(columnName) {
        for (const columnView of this.columnHeaders) {
            if (columnView.name === columnName) {
                columnView.detached = true;
            }
        }
    }

    /**
     * Attaches the column by given name
     *
     * @param {string} columnName  name of column to attach
     * @see columnsDetachByName
     */
    columnsAttachByName(columnName) {
        for (const columnView of this.columnHeaders) {
            if (columnView.name === columnName) {
                columnView.detached = false;
            }
        }
    }

    /**
     * Detaches all columns <i>except</i> for the columns referenced by the given column names
     *
     * @param {string} columnNames  1 or more names of the columns to attach
     */
    columnsAttachOnlyByName(...columnNames) {
        if (!isArrayEmpty(columnNames)) {
            for (const columnView of this.columnHeaders) {
                if (columnNames.includes(columnView.modelColumn.name)) {
                    // Found column to remain attached
                    columnView.detached = false;
                } else {
                    // Detach all other columns
                    columnView.detached = true;
                }
            }
        }
    }

    /**
     * Re-attaches all columns back to the table
     */
    columnsAttachAll() {
        for (const columnView of this.columnHeaders) {
            columnView.detached = false;
        }
    }

    /**
     * Resets the column order to the original order when page was first loaded and the table initialized
     *
     * Example use cases:
     * <li>Pivoting is de-activated and table needs to return columns to original order
     *
     * @param {boolean} [makeColumnsVisible=false] Optionally make all columns visible and attached again
     */
    columnsResetOrder(makeColumnsVisible=false) {
        // IMPLEMENTATION NOTE: Just sort the column headers list to the order of the model originalIndex
        this.columnHeaders.sort(function (a, b) {
            return a.modelColumn.originalIndex - b.modelColumn.originalIndex;
        });

        if (makeColumnsVisible) {
            for (const column of this.columnHeaders) {
                column.visible = true;
                column.detached = false;
            }
        }
    }

    /**
     * Restores all columns that are hidden to viewable again
     *
     * @param {boolean} [applyColumnFilter=true]  controls whether filtering should be applied
     * @see columnsShow()
     * @see columnsHide()
     */
    columnsShowAll(applyColumnFilter=true) {
        // Set all columns to be visible again
        for (const columnHeaderView of this.columnHeaders) {
            columnHeaderView.visibility = true;
        }

        this.resetTableRows(applyColumnFilter);
    }

    /**
     * Hides (make not visible) the column given by name
     *
     * NOTE: All other columns show/hide status are untouched and remain the same
     *
     * @param {string}  columnName  name column name to hide
     * @param {boolean} redraw      redraw entire table.  Default = false for performance reasons to allow client to redraw once at the end
     * @see columnsShowByName
     */
    columnsHideByName(columnName, redraw= false) {
        for (const columnHeaderView of this.columnHeaders) {
            if (columnHeaderView.modelColumn.name === columnName) {
                columnHeaderView.visible = false;
            }
        }

        if (redraw) {
            this.resetTableRows(true);
        }
    }

    /**
     * Hides all columns except the ones names in the list
     *
     * @param {string} columnNames  1 or more names of columns to <b>not</b> hide.  All other columns will be hidden
     * @param {boolean} [redraw=true]  immediate redraw of table or not
     */
    columnsHideAllExcept(columnNames, redraw=true) {
        for (const columnHeaderView of this.columnHeaders) {
            if (columnNames != null && columnNames.includes(columnHeaderView.modelColumn.name)) {
                columnHeaderView.visible = true;
            } else {
                columnHeaderView.visible = false;
            }
        }

        if (redraw) {
            this.resetTableRows(true);
        }
    }

    /**
     * Shows (make visible) the columns given by name.
     *
     * NOTE: All other columns show/hide status are untouched and remain the same
     *
     * @param {string}  columnName  1 or more column name to hide
     * @param {boolean} redraw      redraw entire table.  Default = false for performance reasons to allow client to redraw once at the end
     * @see columnsHideByName
     */
    columnsShowByName(columnName, redraw = false) {
        for (const columnHeaderView of this.columnHeaders) {
            if (columnHeaderView !== null && columnHeaderView.modelColumn.name === columnName) {
                columnHeaderView.visible = true;
            }
        }

        if (redraw) {
            this.resetTableRows(true);
        }
    }

    /**
     * Clears all sort indicators across all column header views.
     *
     * This should be called before applying new sort indicators to avoid incorrect sort indications.
     */
    clearAllSortIndicators() {
        for (const columnViewHeader of this.columnHeaders) {
            const indicatorElement = findElementThatHasAttribute(columnViewHeader.headerThCell, SORT_ORDER_ATTRIBUTE);
            if (typeof(indicatorElement) !== "undefined" && indicatorElement !== null) {
                indicatorElement.innerHTML = "";
            }
        }
    }

    /**
     * Check is filtering is currently active
     *
     * @returns {boolean} <code>true</code> if filtering is active and <code>false</code> otherwise
     */
    isFilteringActive() {
        return this.spanFilteredOut.style.visibility === "visible";
    }

    /**
     * Check is column hiding is currently active
     *
     * @returns {boolean} <code>true</code> if column hiding is active and <code>false</code> otherwise
     */
    isColumnHidingActive() {
        return this.spanHiddenColumns.style.visibility === "visible";
    }

    /**
     * Check is pivoting is currently active
     *
     * @returns {boolean} <code>true</code> if pivoting is active and <code>false</code> otherwise
     */
    isPivotingActive() {
        return this.spanPivotStatus.style.visibility === "visible";
    }

    /**
     * Sets the spinner for pivoting to be visible or not
     *
     * @param visible {boolean}  <code>true</code> to make spinner visible <code>false</code> to hide
     */
    setPivotSpinnerVisible(visible) {
        if (visible) {
            this.spanPivotSpinner.style.visibility = "visible";
            this.spanPivotSpinner.display = "block";
        } else {
            this.spanPivotSpinner.display = "none";
            this.spanPivotSpinner.style.visibility = "collapse";
        }
    }

    /**
     * Initiated by model when model data changes so that view may update accordingly
     *
     * @param {mobiDoraTable.ObserverAction} action enumerated action {@link #mobiDoraTable.ObserverAction}
     * @param {*} data  associated data specific to the action
     * @see #mobiDoraTable.ObserverAction
     */
    notify(action, data) {
        switch (action) {
            case mobiDoraTable.ObserverAction.NEW_ROW:
                // console.info(`Got new model row data`);
                const tr = this.addRow(data, true);

                break;
            case mobiDoraTable.ObserverAction.UPDATED_ROW:
                // console.info(`Got row update notification for row at index: ${data}`);

                this.replaceRow(data);

                break;
            case mobiDoraTable.ObserverAction.CLEAR:
                // Clear UI
                this.clearUiRows();

                // Reset column ordering to table model's original column ordering
                this.columnHeaders.length = 0;
                for (const columnHeader of this.tableModel.getColumns()) {
                    this.columnHeaders.push(new mobiDoraTable.ColumnHeader(this, columnHeader));
                }

                break;
            default:
            case mobiDoraTable.ObserverAction.REDRAW:
                // Default action is to redraw the entire table
                console.info(`Redrawing table...`);

                this.resetTableRows(true);

                break;
        }
    }
}

/**
 * This is a composite class that wraps a model column object {@see mobiDoraTable.Column} while adding view level
 * information like if column is visible, detached, and current/next sort order.
 *
 * @type {mobiDoraTable.ColumnHeader}
 * @see mobiDoraTable.Column
 */
mobiDoraTable.ColumnHeader = class {
    /**
     * Sole constructor
     *
     * @param {mobiDoraTable.TableView} tableView    reference to parent {@see mobiDoraTable.TableView} instance
     * @param {mobiDoraTable.Column} modelColumn  required - reference the wrapped actual data model header {@see mobiDoraTable.Column}
     * @param {boolean} [visible=true]       <code>true</code> if column should be visible in UI and <code>false</code> otherwise
     * @param {boolean} [detached=false] set to <code>true</code> to force column to be removed from the view where it cannot be
     *                     referenced unless placed back.  This is for reports that effectively remove columns from
     *                     the report such as for "pivot" operations.
     */
    constructor(tableView, modelColumn, visible=true, detached=false, sortOrder = 1, enableOriginalSortOrder=false) {
        // Reference to parent object
        this.tableView = tableView;

        // Allow anonymous class access
        const outerThis = this;

        // Reference to core model data cell
        this.modelColumn = modelColumn;

        //
        // Visibility related fields
        //
        this.visible = visible;
        this.detached = detached;

        //
        // Sorting related fields
        //
        this.nextSortOrder = sortOrder;
        this.enableOriginalSortOrder = enableOriginalSortOrder;

        /**
         * Create the detached TH cell poppulated with the model's name and tooltip.  This TH element can be attached to
         * the DOM when its time to render the table's header row.
         *
         * @type {HTMLElement}
         * @see mobiDoraTable.TableView.renderTableHeaders
         */
        this.headerThCell = createHeaderCell(this.modelColumn.name, this.modelColumn.tooltip);

        // Attach event handler that handles table header click to perform the table sort
        this.headerThCell.onclick = function() {
            if (!outerThis.tableView.isPivotingActive()) {
                // REGULAR SORT MODE - (non-pivot) - Sort on this column on user click
                outerThis.tableView.tableSorter.sortByHeaderNames(true, outerThis);
            } else {
                // PIVOT SORT MODE - just cycle sort order of current column and re-run the pivot operation
                outerThis.cycleSortOrder();
                outerThis.tableView.groupingDropdownBox.applyPivot();
            }
        }

        // Reformat the header cell in a segmented table that allows for table sort UI components to
        // be placed
        // TODO: this HTML layout can be created by default w/o this post step in the new design
        this.headerThCell.innerHTML = `
            <table border="0" width="100%">
                <tr style="background: rgba(0,0,0,0.0)">
                    <td style="border-style: hidden">${this.headerThCell.innerHTML}</td>
                    <td style="border-style: hidden" valign="top" ${SORT_ORDER_ATTRIBUTE}><!-- Placeholder for the sort indicator (asc/desc) --></td>
                    <td align="right" valign="top" style="opacity: 0.33; border-style: hidden"><img src="images/sortable-indicator.svg" ${SORTABLE_ATTRIBUTE} height="11pt" style="visibility: hidden" /></td>
                </tr>
            </table>
        `;

        // Show/Hide the sortable indicators only when the mouse cursor enters any of the table headers and
        // then hide the sortable indicators if the mouse cursor leaves any table cell header
        //
        // *SHOW INDICATORS*
        this.headerThCell.addEventListener("mouseover", function() {
            // TODO: Do NOT execute if table sorting is not enabled
            // Iterate over all TH headers
            for (const columnView of outerThis.tableView.columnHeaders) {
                const indicatorElement = findElementThatHasAttribute(columnView.headerThCell, SORTABLE_ATTRIBUTE);
                if (typeof(indicatorElement) !== "undefined" && indicatorElement !== null) {
                    indicatorElement.style.visibility = "visible";
                }
            }
        });
        // *HIDE INDICATORS*
        this.headerThCell.addEventListener("mouseout", function() {
            // TODO: Do NOT execute if table sorting is not enabled
            // Iterate over all TH headers
            for (const columnView of outerThis.tableView.columnHeaders) {
                const indicatorElement = findElementThatHasAttribute(columnView.headerThCell, SORTABLE_ATTRIBUTE);
                if (typeof(indicatorElement) !== "undefined" && indicatorElement !== null) {
                    indicatorElement.style.visibility = "hidden";
                }
            }
        });
    }

    /**
     * Returns the current index of this column as it is <i>presently</i> in the table if the column both <b>visible</b>
     * and <b>attached</b>.  This value can change if user has re-ordered the columns manually or via pivot operation.
     *
     * @returns {number}   0-index value of this column as it is currently in the table
     */
    currentPositionInTable() {
        let index = 0;
        for (const columnViewHeader of this.tableView.columnHeaders) {
            if (columnViewHeader.visible && !columnViewHeader.detached) {
                if (columnViewHeader.modelColumn.name === this.modelColumn.name) {
                    // Linear search.  Stop on first match.
                    return index;
                } else {
                    index++;
                }
            }
        }

        // Not found in table or is not visible
        return -1;
    }

    /**
     * Returns the current sort order
     *
     * @returns {number}
     * @see setcurrent
     */
    getCurrentSortOrder() {
        return this.calcPrevSortOrder(this.nextSortOrder);
    }

    /**
     * Sets the current sort order
     *
     * @param {number} sortOrder
     * @see getCurrentSortOrder
     */
    setCurrentSortOrder(sortOrder) {
        this.nextSortOrder = this.calcNextSortOrder(sortOrder);
    }

    /**
     * Returns the current sort order and, internally, cycles to the next sort order
     *
     * @returns {number}
     */
    cycleSortOrder() {
        // Save the current sort order before cycling through
        let currentSortOrder = this.nextSortOrder;

        // Cycle sort order for subsequent call
        this.nextSortOrder = this.calcNextSortOrder(currentSortOrder);

        return currentSortOrder;
    }

    /**
     * Calculates the next sort order for any given current sort order
     *
     * @param currSortOrder
     * @returns {number|*}
     */
    calcNextSortOrder(currSortOrder) {
        if (currSortOrder === 1) {
            // Wrapped around
            return -1;
        } else {
            if (this.enableOriginalSortOrder) {
                // Advance to next sort order
                return currSortOrder + 1;
            } else {
                // Original sort order not enabled so just flip between ascending and descending sort modes
                if (currSortOrder !== 0) {
                    return -1 * currSortOrder;
                } else {
                    return -1;
                }
            }
        }
    }

    /**
     * Calculates the previous sort order for any given current sort order
     *
     * @param {number} currSortOrder
     * @returns {number}
     */
    calcPrevSortOrder(currSortOrder) {
        if (currSortOrder === -1) {
            // Wrap around
            return 1;
        } else {
            if (this.enableOriginalSortOrder) {
                // Decrement to previous sort order
                return currSortOrder - 1;
            } else {
                // Original sort order not enabled so just flip between ascending and descending sort modes
                if (currSortOrder !== 0) {
                    return -1 * currSortOrder;
                } else {
                    return -1;
                }
            }
        }
    }

    /**
     * Show the sort indicator for the given direction for this table header cell
     *
     * @param {number} direction   Set -1 => Descending; +1 => Ascending; 0 => original sort order
     */
    showSortIndicator(direction) {
        const indicatorElement = findElementThatHasAttribute(this.headerThCell, SORT_ORDER_ATTRIBUTE);

        switch (direction) {
            case -1:
                // Descending
                //
                indicatorElement.innerHTML = "<span style='font-size: 133%; color: rgba(255,255,255,0.55)'>&#x2b07</span>";
                break;
            case 1:
                // Ascending
                //
                indicatorElement.innerHTML = "<span style='font-size: 133%; color: rgba(255,255,255,0.55)'>&#x2b06</span>";
                break;
            default:
                // Original sort order (clear indicator)
                indicatorElement.innerHTML = "";
                break;
        }
    }
}

