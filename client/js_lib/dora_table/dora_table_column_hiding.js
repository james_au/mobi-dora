if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

/**
 * This encapsulates the GUI, data, and control logic for the column hiding feature drop down box.
 *
 * @see  {getElementRef()}  for the root HTML element
 * @type {mobiDoraTable.ColumnHidingDropdownBox}
 */
mobiDoraTable.ColumnHidingDropdownBox = class {
    /**
     * Sole constructor
     *
     * @param tableView  reference to the parent table view
     */
    constructor(tableView) {
        this.tableView = tableView

        this.checkBoxes = new Array();

        // Create and style the DIV used as the root UI element
        this.element = document.createElement("div");
        this.element.className = "dropDownMenu1";
        this.element.tabIndex = -1;  // Required in order to have key listener on DIV
        this.element.style.visibility = "hidden";
        this.element.style.alignContent = "left";
        this.element.style.textAlign = "left";
        this.element.style.height = "150px";
        this.element.style.width = "375px";
        this.element.addEventListener("keydown", function(event) {
            if (event.key === "Escape") {
                outerThis.element.style.visibility = "hidden";
            }
        });

        // Allows anonymous function access
        const outerThis = this;

        // Create shortcut button for selecting all checkboxes
        this.btnShowAll = document.createElement("button");
        this.btnShowAll.innerHTML = "Show All";
        this.btnShowAll.style.fontSize = "10pt";
        this.btnShowAll.style.marginTop = "6pt";
        this.btnShowAll.style.marginRight = "3pt";
        this.btnShowAll.addEventListener("click", function(event) {
            outerThis.showAllColumns();
        });

        // Create shortcut button for de-selecting all checkboxes
        this.btnHideAll = document.createElement("button");
        this.btnHideAll.innerHTML = "Hide All";
        this.btnHideAll.style.fontSize = "10pt";
        this.btnHideAll.style.marginTop = "6pt";
        this.btnHideAll.style.marginRight = "3pt";
        this.btnHideAll.addEventListener("click", function(event) {
            console.info("Hiding all columns");

            if (outerThis.checkBoxTuples !== "undefined" && outerThis.checkBoxTuples !== null) {
                for (const checkboxTuple of outerThis.checkBoxTuples) {
                    if (checkboxTuple.checkbox !== null) {
                        checkboxTuple.checkbox.checked = false;
                    }
                }
            }

            // Apply the column hiding based on user selected checkbox state
            outerThis.applyColumnHiding();
        });

        // Create shortcut button for inverting the current selection state of all checkboxes
        this.btnInvert = document.createElement("button");
        this.btnInvert.innerHTML = "Invert";
        this.btnInvert.style.fontSize = "10pt";
        this.btnInvert.style.marginTop = "6pt";
        this.btnInvert.style.marginRight = "3pt";
        this.btnInvert.addEventListener("click", function(event) {
            console.info("Inverting all columns");

            if (outerThis.checkBoxTuples !== "undefined" && outerThis.checkBoxTuples !== null) {
                for (const checkboxTuple of outerThis.checkBoxTuples) {
                    if (checkboxTuple.checkbox !== null) {
                        checkboxTuple.checkbox.checked = !checkboxTuple.checkbox.checked;
                    }
                }
            }

            // Apply the column hiding based on user selected checkbox state
            outerThis.applyColumnHiding();
        });

        const popInternalTable = document.createElement("table");
        popInternalTable.setAttribute("border", "0");
        // popInternalTable.setAttribute("width", "100%");
        this.fieldSelectionTableBody = popInternalTable.createTBody();
        this.element.appendChild(this.createCloseIcon());
        this.element.appendChild(popInternalTable);
        this.element.appendChild(this.btnShowAll);
        this.element.appendChild(this.btnHideAll);
        this.element.appendChild(this.btnInvert);

        // Populate columns from table model
        this.checkBoxTuples = this.createColumnCheckBoxes();

        // Create the actual formatted checkbox table rows
        //
        for (const record of this.checkBoxTuples) {
            const tr = document.createElement("tr");
            const td = document.createElement("td");
            const label = document.createElement("label");

            label.className = "checkBoxSmall";

            // Label element wraps the checkbox with its label and both are clickable
            label.append(record.checkbox);
            label.append(record.label);

            // Append the table row
            td.append(label);
            tr.append(td);

            // Append to the main table body
            this.fieldSelectionTableBody.append(tr);
        }
    }

    /**
     * Shows all columns with optional row filtering applied
     *
     * @param {boolean} filter=true   optional disable filtering which is necessary to prevent infinite recursion if filtering is already being performed
     */
    showAllColumns(filter=true) {
        console.info("Showing all columns");

        // Check all UI field checkboxes
        //
        if (this.checkBoxTuples !== "undefined" && this.checkBoxTuples !== null) {
            for (const checkboxTuple of this.checkBoxTuples) {
                if (checkboxTuple.checkbox !== null) {
                    checkboxTuple.checkbox.checked = true;
                }
            }
        }

        // Apply the column hiding based on user selected checkbox state
        this.applyColumnHiding(filter);
    }

    /**
     * Use this to get reference to the DIV element that is the parent HTML object of this dialog box.  This will
     * need to be placed into the page's DOM at least once at some point.
     *
     * @returns {HTMLDivElement}  root element of this dialog
     */
    getElementRef() {
        return this.element;
    }

    /**
     * Applies the column hiding operation by iterating through the user's column checkbox selections and hiding or
     * showing columns as necessary.
     *
     * @param {boolean} filter=true
     */
    applyColumnHiding(filter=true) {
        let colIndex = 0;
        let hiddenCount = 0;
        for (const checkboxTuple of this.checkBoxTuples) {
            if (checkboxTuple.checkbox.checked) {
                this.tableView.columnsShowByName(checkboxTuple.label);
            } else {
                this.tableView.columnsHideByName(checkboxTuple.label);
                hiddenCount++;
            }

            colIndex++;
        }

        // Activate the UI tag if any columns were hidden
        if (hiddenCount > 0) {
            this.tableView.spanHiddenColumns.innerHTML = `<span style="opacity: 50%">x</span> &nbsp;${hiddenCount} columns hidden`;
            this.tableView.spanHiddenColumns.style.visibility = "visible";
        } else {
            // No columns were hidden to hide the tag
            this.tableView.spanHiddenColumns.style.visibility = "collapse";
        }

        // Also re-apply filtering (if any) since the table rows above get replaced with original unfiltered rows
        if (filter) {
            if (this.tableView.isFilteringActive()) {
                // Re-apply filtering
                this.tableView.filterDropdownBox.btnApply.click();
            } else {
                // No filtering was active so just re-draw the table
                this.tableView.resetTableRows(true);
            }
        }
    }

    /**
     * Mutates the given row by removing child TD columns that set to be hidden by user selection.  Use this method
     * if you already have TR records and which to apply the column hiding to them.
     *
     * @param row  row to hide columns based on user's current column hiding specifications
     */
    applyColumnHidingToRow(row) {
        let i = 0;
        // Uncomment for extra debug:
        // for (const cell of row.children) {
        //     console.info(`Cell index: ${i} => ${cell.getAttribute("dora_table_column_name")}`);
        //     i++;
        // }
        // console.info("==");

        if (row !== null && !isArrayEmpty(row.children)) {
            // Iterate the row's cells in reverse order and remove cells are are currently
            // hidden.  Reverse iteration is required to keep list consistent after removal.
            for (let columnIndex = row.children.length - 1; columnIndex >= 0; columnIndex--) {
                // Get the column view at the index which allows the code to get the original column even if the actual
                // columns are shuffled different than original column order
                const columnView = this.tableView.columnHeaders[columnIndex];

                if (!columnView.visible || columnView.detached) {
                    row.removeChild(row.children[columnIndex]);
                }
            }

            // Uncomment for extra debug
            // i = 0;
            // for (const cell of row.children) {
            //     console.info(`Cell index: ${i} => ${cell.getAttribute("dora_table_column_name")}`);
            //     i++;
            // }
            // console.info("**");
        }
    }

    /**
     * Creates the checkbox elements for the table's columns based the current table model definition
     *
     * @returns {mobiDoraTable.CheckboxTuple[]}
     */
    createColumnCheckBoxes() {
        const outerThis = this;
        const checkBoxRecords = new Array();
        const uniqueId = uniqueNumber();

        if (!isArrayEmpty(this.tableView.columnHeaders)) {
            for (const columnView of this.tableView.columnHeaders) {
                if (!columnView.detached) {
                    const checkbox = document.createElement("input");
                    checkbox.setAttribute("id", `chk_${uniqueId}_${makeVariableName(escapeHtml(columnView.modelColumn.name))}`); // NOTE: generated ID needs to be unique across page which has multiple Dora table instances
                    checkbox.setAttribute("type", "checkbox");
                    checkbox.checked = true;
                    checkbox.addEventListener("click", function () {
                        outerThis.applyColumnHiding();
                    });

                    checkBoxRecords.push(new mobiDoraTable.CheckboxTuple(columnView.modelColumn.name, checkbox));
                }
            }
        }

        return checkBoxRecords;
    }

    /**
     * Creates the fully formatted and event enabled close icon for the dialog
     *
     * @returns {HTMLSpanElement}
     */
    createCloseIcon() {
        const outerThis = this;
        const spanCloseIcon = document.createElement("span");

        spanCloseIcon.innerHTML = "&#x2715";
        spanCloseIcon.style.float = "right";
        spanCloseIcon.style.border = "1px solid rgba(0,0,0,0.5)";
        spanCloseIcon.style.fontSize = "133%";
        spanCloseIcon.style.color = "gray";
        spanCloseIcon.addEventListener("mouseover", function(event) {
            spanCloseIcon.style.color = "black";
        });
        spanCloseIcon.addEventListener("mouseout", function(event) {
            spanCloseIcon.style.color = "gray";
        });
        spanCloseIcon.addEventListener("click", function(event) {
            outerThis.element.style.visibility = "hidden";
        });

        return spanCloseIcon;
    }

    /**
     * Adjust the dialog's height based on the current count of filter rows
     */
    adjustDialogHeight() {
        const tableHeight = this.fieldSelectionTableBody.offsetHeight;
        const buttonHeight = this.btnShowAll.offsetHeight;

        this.getElementRef().style.height = (12 + buttonHeight + tableHeight) + "px";
    }
}

/**
 * Simple tuple to return both filtered-in & filtered-out results together
 */
mobiDoraTable.CheckboxTuple = class {
    constructor(label, checkbox) {
        this.label = label;
        this.checkbox = checkbox;
    }
}