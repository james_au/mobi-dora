if (mobiDoraTable === undefined) {
    var mobiDoraTable = {};
}

/**
 * Constant for attribute key to identify a table row as a "filter" table row used in the popup filter dialog
 */
mobiDoraTable.TAG_ATTRIB_FILTER_ROW = "DORA_TABLE_FILTER_ROW";

/**
 * Constant for attribute key to identify a table row's original position in the data model.  This attribute will
 * "stick" with row even if its position is shuffled by a table sorter in the UI.
 */
mobiDoraTable.TAG_ATTRIB_FILTER_ROW_ORIG_IDX = "DORA_TABLE_ROW_ORIGINAL_IDX";

/**
 * Constant enum to describe action event for any notifications
 */
mobiDoraTable.ObserverAction = {
    NEW_ROW: 1,  // Signals view that a new row was added
    UPDATED_ROW: 2,    // Signals view to do an update of the row (upsert update)
    REDRAW: 3,    // Signals view to do a complete redraw
    CLEAR: 4      // Must be called at start at least once to reset the table's state properly
}

/**
 * Constant enum for column data type which allows proper sorting and filtering operations
 */
mobiDoraTable.ColumnType = {
    STRING : 1,
    INTEGER : 2,
    FLOAT : 3,
    DATE : 4
}

// Freeze enumerations so they cannot be changed later
Object.freeze(mobiDoraTable.ColumnType);
Object.freeze(mobiDoraTable.ObserverAction);

/**
 * Provides the data model which Dora table instances get their data
 */
mobiDoraTable.TableModel = class {
    /**
     * Sole constructor
     *
     * @param {Cell[]}  data     (optional) data array -- can be set later with {@link upsertRow} or {@link addRow}
     * @param columns  (required) data array containing {@link #Column} objects
     * @see #Column
     * @see upsertRow
     * @see addRow
     */
    constructor(data, columns) {
        this.observers = new Array();
        this.data = new Array();
        this.columns = new Array();

        if (columns !== null) {
            this.columns = columns;

            for (let i = 0; i < columns.length; i++) {
                columns[i].originalIndex = i;
            }
        }

        if (data !== null) {
            this.data = data;
        }
    }

    /**
     * Adds a data row to the model
     *
     * @param {Cell[]} row  an array containing row data matching original column definition
     * @param {boolean} [notifyObservers=true] set to <code>true</code> to notify observers right away or not.  It can be more efficient to wait to to notify later after all data is added.
     */
    addRow(row, notifyObservers=true) {
        this.data.push(row);

        if (notifyObservers) {
            this.notifyObservers(mobiDoraTable.ObserverAction.NEW_ROW, row);
        }
    }

    /**
     * Similiar to {@link #addRow} but uses a comparison function to determine if row is a brand new row or an
     * <i>update</i> to an existing row.
     *
     * @param {Cell[]} row    an array of {@see Cell} objects which represents the row to upsert
     * @param {function} isEquivalent    client supplied predicate function determines when two rows are deemed equivalent based on client logic
     * @param {boolean} [notifyObservers=true] set to <code>true</code> to notify observers right away or not.  It can be more efficient to wait to to notify later after all data is upserted.
     * @return {number} 0 if this was an existing row (update) or 1 if this was a brand new row which allows client to know which operation occurred
     */
    upsertRow(row, isEquivalent, notifyObservers = true) {
        for (let i = 0; i < this.getRowCount(); i++) {
            if (isEquivalent(row, this.data[i])) {
                // Linear search.  Exit on first match
                // Found existing row, so just update it
                this.data[i] = row;

                // Notify observer with the index of the row that was updated so they can update their UI row
                // in the table view accordingly
                this.notifyObservers(mobiDoraTable.ObserverAction.UPDATED_ROW, i);

                // Found match, so exit loop.
                return 0;
            }
        }

        // If this point is reached the no match found so its a brand new row so add it to the end of
        // the data list
        this.addRow(row, notifyObservers);

        return 1;
    }

    /**
     * Clears all data in the model except for header definitions.  Must be called if table model being re-populated
     * with all new data.
     *
     * @param notifyObservers  (optional) defaults to true
     */
    clearAllRows(notifyObservers=true) {
        this.data = new Array();

        if (notifyObservers) {
            this.notifyObservers(mobiDoraTable.ObserverAction.CLEAR);
            this.notifyObservers(mobiDoraTable.ObserverAction.REDRAW, "Model data cleared");
        }
    }

    /**
     * Adds one or more observers
     *
     * @param {*} observers  1 or more observers that implement the <code>notify</code> method
     */
    addObserver(...observers) {
        if (observers !== null) {
            for (const observer of observers) {
                // Ignore observers that are already added
                if (isArrayEmpty(this.observers.filter(n => n === observer))) {
                    this.observers.push(observer);
                } else {
                    console.warn(`Not adding observer ${observer} because it is already added.`);
                }
            }
        }
    }

    /**
     * Notifies all observers that an action event has occurred
     *
     * @param {ObserverAction}  action
     * @param {string} data  data associated with the action
     */
    notifyObservers(action, data) {
        if (this.observers !== null) {
            for (const observer of this.observers) {
                observer.notify(action, data);
            }
        }
    }

    /**
     * Returns column object at given index
     *
     * @param  {number} i   the index value 0 or more
     */
    getColumnType(i) {
        return this.columns[i];
    }

    /**
     * Returns number of columns
     *
     * @return {number}
     */
    getColumnCount() {
        return this.columns !== null ? this.columns.length : 0;
    }

    /**
     * Directly returns the iterable column array
     *
     * @return {Column[]}
     */
    getColumns() {
        return this.columns;
    }

    /**
     * Removes all columns from the model except for the given names
     *
     * @param {string} name  1 or more column names to keep
     */
    removeColumnsExcept(...names) {
        const tempList = new Array();

        // Find matching columns and save to a temp list
        for (const column of this.columns) {
            if (names.includes(column.name)) {
                tempList.push(column);
            }
        }

        // Update objects column list with just the matched names found above
        this.columns.length = 0;
        this.columns.push(...tempList);
    }

    /**
     * Returns the row at referenced index
     *
     * @param {number}  i    index must be 0 or greater
     * @returns {string}  data row at index
     */
    getRow(i) {
        return this.data[i];
    }

    /**
     * Returns current count of data rows
     *
     * @returns {number}  current count of data rows (0+)
     */
    getRowCount() {
        return this.data !== null ? this.data.length : 0;
    }

    /**
     * Returns the iterable data rows
     *
     * @returns {[]}
     */
    getRows() {
        return this.data;
    }

    /**
     * Gets the cell value at row and col positions
     *
     * @param {number} row    row coordinate
     * @param {number} col    column coordinate
     * @returns {string}  cell object value
     */
    getValueAt(row, col) {
        return this.data[row][col];
    }
}

/**
 * Represents a column in the model
 */
mobiDoraTable.Column = class {
    /**
     * Sole constructor
     *
     * @param {string} name
     * @param {ColumnType} type  enumerated type describing column's data type {@link #ColumnType}
     * @param {string} tooltip  (optional) short tooltip verbiage
     * @see #ColumnType
     */
    constructor(name, type, tooltip="") {
        this.name = name;
        this.type = type;
        if (isNotBlank(tooltip)) {
            this.tooltip = tooltip;
        }

        // This will be automatically set according to by the table model during when Column records are inserted into
        // the table model
        this.originalIndex = -1;
    }
}

/**
 * Represents a table cell in the model
 *
 * @type {mobiDoraTable.Cell}
 */
mobiDoraTable.Cell = class {
    /**
     * Sole constructor
     *
     * @param {string} data     the data to show the HTML output
     * @param {string} [tooltip=""]  (optional) short tooltip verbiage
     * @param {string} [dataAttribute=""]  (optional) optional attribute that can be applied to the TD or any other HTML element
     */
    constructor(data, tooltip="", dataAttribute="") {
        this.data = data;
        if (isNotBlank(tooltip)) {
            this.tooltip = tooltip;
        }
        if (isNotBlank(dataAttribute)) {
            this.dataAttribute = dataAttribute;
        }
    }
}

