var mobiDoraBalanceUtil;

mobiDoraBalanceUtil = {
    // Encapsulates a hypervisor and provides the Model & Control in MVC design pattern.  A balance tab will use
    // one these objects as the data model to represent a VCenter hypervisor for balancing operations in the UI view.
    //
    // Ref: https://www.w3schools.com/js/js_array_methods.asp
    HypervisorDataModel: class {
        _rulesParsed = null;

        constructor(observer = null) {
            this.hypervisorDataRecords = new Array();
            this.environmentStats = new mobiDoraBalanceUtil.EnvironmentStats();
            this.observers = new Array();
            this.unplacedVMs = new Array();
            this.vcenterName = ""
            this.rackMapping = null; // Needs to be set in setter after first hypervisor load out retrieval
            this.rulesText;
            this.balanceSessionTag
            this._balanceSessionVCenter
            this._balanceSessionUUID
            this._balanceSessionTimeMs

            if (observer !== null) {
                this.addObserver(observer);
            }
        }

        // Sets an update rules text YAML that was used for this model data. This mus be updated whenever the "balance
        // exec" or "balance check" operations are performed.
        setRulesText(rulesText) {
            this.rulesText = rulesText;

            // YAML has changed -- so reset etBalanceSessionId()parsed object so it will be re-generated on first use in getRulesParsed()
            this._rulesParsed = null;
        }

        // Gets the raw YAML text that was used for this model data
        getRulesText() {
            return this.rulesText;
        }

        // Returns the current parsed data structure.  May return null if no YAML data was set or is incorrect.
        getRulesParsed() {
            if (!isBlank(this.getRulesText())) {
                if (this._rulesParsed == null) {
                    // Lazy instantiate the parsed YAML data structure
                    this._rulesParsed = jsyaml.load(this.getRulesText());
                }
            }

            // NOTE: this may be null if no rules text was set by the user
            return this._rulesParsed;
        }

        setVCenterName(vcenterName) {
            this.vcenterName = vcenterName;
        }

        getVCenterName() {
            return this.vcenterName;
        }

        setRackMapping(rackMapping) {
            this.rackMapping = rackMapping;
        }

        getRackMapping() {
            return this.rackMapping;
        }

        setBalanceSessionTag(balanceSessionTag) {
            this.balanceSessionTag = balanceSessionTag;

            if (!isBlank(this.balanceSessionTag)) {
                try {
                    // Parse the raw string into the parts for easy access later
                    const split = this.balanceSessionTag.split(":::");
                    this._balanceSessionVCenter = split[0];
                    this._balanceSessionUUID = split[1];
                    this._balanceSessionTimeMs = split[2];
                } catch (e) {
                    // Not fatal but definitely a problem because balance session locking will not function without a
                    // proper balance session ID.
                    console.error(`Could not parse balance session ID "${this.balanceSessionTag}" because of error ${e}`);
                }
            }
        }

        getBalanceSessionTag() {
            return this.balanceSessionTag;
        }

        // This is only available after setting the balance session tag (see #setBalanceSessionIdRaw)
        getBalanceSessionVCenter() {
            return this._balanceSessionVCenter;
        }

        // This is only available after setting the balance session tag (see #setBalanceSessionIdRaw)
        getBalanceSessionUUID() {
            return this._balanceSessionUUID;
        }

        // This is only available after setting the balance session tag (see #setBalanceSessionIdRaw)
        getBalanceSessionTimeMs() {
            return this._balanceSessionTimeMs;
        }

        // Adds a complete set of Hypervisor load records at once in on bulk operation.  Good for first time
        // initialization and causes just a single observer call for the bulk list.
        // NOTE: You may need to clearAll() if intending to reset the data
        addAll(hypervisorDataRecords, notify=true) {
            this.hypervisorDataRecords = hypervisorDataRecords;

            // Update the environment stats by feeding each hypervisor record into the stats object to get
            // the overall environment stats
            for (const hypervisorRec of this.hypervisorDataRecords) {
                this.environmentStats.add(hypervisorRec);
            }

            this.notifyObservers(notify);
        }

        add(hypervisorRecord) {
            this.hypervisorDataRecords.push(hypervisorRecord);
            this.environmentStats.add(hypervisorRecord);
            this.notifyObservers();
        }

        // Adds all specified VMs to the unplaced list replacing any existing list prior. Note, if the given list
        // is null then the internal list also becomes null.
        addAllUnplacedVMs(unplacedVms, notify=true) {
            this.unplacedVMs = unplacedVms;

            if (notify) {
                this.notifyObservers();
            }
        }

        getAllUnplacedVMs() {
            return this.unplacedVMs;
        }

        getCountUnplacedVMs() {
            if (this.unplacedVMs != null) {
                return this.unplacedVMs.length;
            } else {
                return 0;
            }
        }

        // Adds a single VM record to the unplaced list.  Call this for DnD operation where use moves a VM back to
        // the unplaced basket.  This requires the full VM Record and not just the VM hostname.
        addUnplacedVM(vmRec) {
            if (this.unplacedVMs == null) {
                // Lazy instantiation if Array object not already created
                this.unplacedVMs = new Array();
            }

            this.unplacedVMs.push(vmRec);
        }

        // Clears the entire VM unplaced list such as when resetting the layout
        clearUnplacedVMList() {
            if (this.getAllUnplacedVMs() !== null) {
                this.unplacedVMs = new Array();
            }
        }

        // Call this after actually "placing" a VM so it can be removed from the "unplaced" list.
        // If found, the VM record is returned.  If not found, then a null reference is returned.
        removeUnplacedVM(vmHostName, notify=true) {
            // Linear search, stop on first match
            for (var i = 0; i < this.unplacedVMs.length; i++) {
                let vmRec = this.unplacedVMs[i];

                if (vmRec.getHostname() == vmHostName) {
                    // Remove found VM object from the array
                    this.unplacedVMs = removeArrayElementIndex(this.unplacedVMs, i);

                    this.notifyObservers(notify);

                    // Return found VM object which can then be used to transfer to another list or hypervisor
                    return vmRec;
                }
            }

            this.notifyObservers(notify);

            // Not found
            return null;
        }

        removeVmFromHypervisor(vmRec, hvManagedObjRef, notify=true) {
            let hypervisorRec = this.getHypervisorByMoRef(hvManagedObjRef);

            if (hypervisorRec !== null) {
                // Linear search.  Remove and break on first match.
                for (let i = 0; i < hypervisorRec.getVmProposedList().length; i++) {
                    if (vmRec.getHostname() === hypervisorRec.getVmProposedList()[i].getHostname()) {
                        // Found matching index so can remove
                        hypervisorRec.setVmProposedList(removeArrayElementIndex(hypervisorRec.getVmProposedList(), i));

                        break;
                    }
                }
            }

            this.notifyObservers(notify);
        }

        getHypervisorRecords() {
            return this.hypervisorDataRecords;
        }

        // Returns the Hypervisor record matching the given managed Object ID (e.g. "host-2920") or null if
        // no match found
        //
        getHypervisorByMoRef(managedObjRef) {
            for (const hypervisor of this.hypervisorDataRecords) {
                // Linear search return on first match
                if (hypervisor.getManagedObjRef() === managedObjRef) {
                    return hypervisor;
                }
            }

            // If this point is reached then no match was found
            return null;
        }

        // Returns the Hypervisor record matching the given path or null if no match found
        //
        getHypervisorByPath(path) {
            for (const hypervisor of this.hypervisorDataRecords) {
                // Linear search return on first match
                if (hypervisor.getPath() === path) {
                    return hypervisor;
                }
            }

            // If this point is reached then no match was found
            return null;
        }

        getEnvironmentStats() {
            return this.environmentStats;
        }

        clearAll(notify=true) {
            this.hypervisorDataRecords = new Array();
            this.environmentStats.clearAll();

            this.notifyObservers(notify);
        }

        // Searches the "proposed" list of all hypervisors for the VM matching the given VCenter path
        //
        findVMInProposedListByPath(vmPath) {
            // Nested linear search.  Return on first match.
            for (const hypervisorRec of this.getHypervisorRecords()) {
                for (const vmRec of hypervisorRec.getVmProposedList()) {
                    if (vmRec.getPath() === vmPath) {
                        // Found
                        return vmRec;
                    }
                }
            }

            // Not found
            return null;
        }

        // Returns the hypervisor where the given VirtualMachineLoad record is currently located in.  Otherwise, returns
        // null if not found.  This method is useful for finding where a VM moved to while generating the "move list".
        findHypervisorWithProposedVM(vmLoadRec) {
            // Linear search.  Return on first match.
            for (const hypervisorRec of this.getHypervisorRecords()) {
                for (const vmRec of hypervisorRec.getVmProposedList()) {
                    if (vmRec.getHostname() === vmLoadRec.getHostname()) {
                        // Found
                        return hypervisorRec;
                    }
                }
            }

            // Not found
            return null;
        }

        // Adds a new proposed VM to this hypervisor.
        // This is used for drag-n-drop manual user change event from the UI
        addProposed(vmRecord) {
            this.hypervisorDataRecords.getVmProposedList().push(vmRecord);
            this.notifyObservers();
        }

        // Returns a list of all VMs that have moved from one hypervisor to another hypervisor
        //
        getAllMovedVMs() {
            const allMovedVMs = new Array();

            // All moved VMs are the array "difference" in all Hypervisor's "proposed" minus "current" lists
            for (const hypervisorRec of this.getHypervisorRecords()) {
                allMovedVMs.push(..._.differenceWith(
                    hypervisorRec.getVmProposedList(),
                    hypervisorRec.getVmCurrentList(), (a, b) => {
                        // Defines hypervisor equality as hostname field equility
                        return a.getHostname() === b.getHostname()
                    }));
            }

            return allMovedVMs;
        }

        // Creates a new data model object with the "proposed" VM list forced into the "current" VM list which allows
        // a "preview" of what the model would look like after actual VCenter moves.  It basically accepts the proposed
        // VM list for all hypervisors and returns a model representing such.
        //
        // NOTE: the current data model is *not* modified and it can be saved to a stack for history/undo options
        //
        previewProposed() {
            if (!isArrayEmpty(this.getAllUnplacedVMs())) {
                throw `Cannot preview because there are still ${this.getAllUnplacedVMs().length} unplaced VMs`;
            } else {
                // Make a "deep copy" of current data model
                const updatedModel = _.cloneDeep(this);

                // Iterate through each hypervisor and replace its "current" list with its "proposed" list -- this is
                // the key to "preview" feature
                for (const hypervisorRec of updatedModel.getHypervisorRecords()) {
                    // Copy "proposed" as "current" which forces the model to accept the proposed changes
                    hypervisorRec.setVmCurrentList(_.cloneDeep(hypervisorRec.getVmProposedList()));

                    // Update "current" side CPU & MEM totals be the same "proposed" since the proposal was accepted
                    hypervisorRec.setCurrentRoomCpu(_.cloneDeep(hypervisorRec.getProposedRoomCpu()));
                    hypervisorRec.setCurrentRoomMemGb(_.cloneDeep(hypervisorRec.getProposedRoomMemGb()));
                }

                return updatedModel;
            }
        }

        // Add 1 or more observers.  Observers get notified of changes and can update their respective views accordingly.
        //
        addObserver(...observers) {
            if (observers !== null && observers.length > 0) {
                for (const observer of observers) {
                    this.observers.push(observer);
                }
            }
        }

        // Return current list of zero or more observers
        //
        getObservers() {
            return this.observers;
        }

        // Detaches all observers
        //
        clearObservers() {
            this.observers.length = 0;
        }

        //  Notifies all attached observers that data has changed and they should update their view accordingly
        //
        notifyObservers(notify=true) {
            // Notify each observer by calling its registered function
            if (notify) {
                if (this.observers !== null) {
                    for (const observer of this.observers) {
                        if (observer !== null) {
                            observer();
                        }
                    }
                }
            }
        }
    },

    // Tracks the environment stats after loading all hypervisors for a given environment
    // TODO: Remove this and associated code after it is moved to server-side
    EnvironmentStats: class {
        constructor() {
            this.clearAll();
        }

        // Resets stats
        clearAll() {
            this.hypervisors = 0;
            this.hypervisorsTotalCpu = 0;
            this.hypervisorsTotalMemGB = 0.0;
            this.virtualMachines = 0;
            this.vmTotalCpu = 0;
            this.vmTotalMemGB = 0.0;
        }

        // Adds a new hypervisor which also updates (increments) the stats
        add(hypervisorLoadOutRecord) {
            this.hypervisors++;
            this.hypervisorsTotalCpu += hypervisorLoadOutRecord.getCpu();
            this.hypervisorsTotalMemGB += hypervisorLoadOutRecord.getMemGb();

            // Sum up child virtual machine stats
            for (const vmRec of hypervisorLoadOutRecord.getVmCurrentList()) {
                this.virtualMachines++;
                this.vmTotalCpu += vmRec.getCpu()
                this.vmTotalMemGB += vmRec.getMemGb();
            }
        }
    },

    // This class assists with computing the "diff" between the "proposed" VM list and the "current" VM list.
    //
    VmInventoryDiff : class {
        // Loads up the class with the list of VMs.  Note the given array is cloned internally so the given array is
        // not modified.
        constructor(proposedList) {
            // Make a *copy* of the given list so that the original list is not modified
            this.proposedList = [...proposedList];  // Uses ES6 array clone method (https://www.samanthaming.com/tidbits/35-es6-way-to-clone-an-array/)
        }

        // Returns true if the hostname is in the list and false otherwise.  Also, if found, the item is removed
        // from the list as to keep track of remaining VMs.
        contains(hostname) {
            // Linear search
            for (let i = 0; i < this.proposedList.length; i++) {
                if (this.proposedList[i] !== null) {
                    if (this.proposedList[i].getHostname() === hostname) {
                        // Remove the element since it was found
                        this.proposedList.splice(i, 1);

                        // Match
                        return true;
                    }
                }
            }

            // If this point is reached then no match was found
            return false;
        }

        getRemaining() {
            return this.proposedList;
        }

        hasRemaining() {
            return this.proposedList.length > 0
        }
    },

    // Represents an entry for VM move list.  Contains the actual reference to the Javascript data objects.
    //
    MoveEntry : class {
        constructor(vmRec, sourceHypervisorRec, targetHypervisorRec, incompatibleDatastoreMessage = null, incompatibleNetworkMessage = null) {
            this.vmRec = vmRec;
            this.sourceHypervisorRec = sourceHypervisorRec;
            this.targetHypervisorRec = targetHypervisorRec;
            this.incompatibleDatastoreMessage = incompatibleDatastoreMessage;     // Use null/blank for no warning
            this.incompatibleNetworkMessage = incompatibleNetworkMessage;         // Use null/blank for no warning
        }
    },

    // Helps create a "reverse view" mapping of VM-to-parent Hypervisor
    VmReverseMap : class {
        _vmRec;
        _parentHypervisorRec;

        constructor(vmRec, parentHypervisorRec) {
            this._vmRec = vmRec;
            this._parentHypervisorRec = parentHypervisorRec;
        }

        getVmRec() {
            return this._vmRec;
        }

        getParentHypervisorRec() {
            return this._parentHypervisorRec;
        }
    }
}

// Balance rules help text stored separately.  NOTE: Backslash characters (\) need to be escaped!
const BALANCE_RULES_HELP_TEXT = `
        <h2>Balance Rules</h2>
            <p>The rules YAML allows you to control aspects of the balancing algorithm:</p>
            <table cellpadding="5"">
                <tr>
                    <td align="right" valign="top"><b>definitions / vmType</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">
                        Allows categorizing VM node types as (a) <b>Heavy Writer</b>, (b) <b>Heavy Reader</b> or (c) <b>Sensitive</b>.  You can also use regexp to match like <code>nvrcw.*b</code> to match only the b-cluster of recording writers.  Other regexp examples:<br/>
                        <table border="1" cellspacing="0" cellpadding="3" style="border: 1px solid white">
                            <tr>
                                <td>
                                    <code>nvrcw01p[3-6]b.*</code>
                                </td>
                                <td>
                                    Match nvrcw nodes on cluster 3 thru 6 on leg 1 on "b" shards
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <code>stsgl0[1-2]p.*</code>
                                </td>
                                <td>
                                    Match stsgl nodes on legs 1 thru 2 only any cluster
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <code>stsgl01p[1-9]</code>
                                </td>
                                <td>
                                    Match stsgl nodes on cluster 1 thru 9 on leg 1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <code>stsgl0\\dp([2-9]|1[0-6])</code>
                                </td>
                                <td>
                                    Match stsgl nodes on cluster 2 thru 16 on any leg
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>definitions / vmTypes / hypervisor:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">Limits the vmType to be placed <i>only</i> on those Hypervisors tagged (with VCenter inventory tag) with the same tag label.  <b>Warning:</b> if your tag has a typo or no hypervisors in this environment have been tagged, then all matching vmTypes will all end up as an "unplaced" VM!</td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>definitions / vmTypes / order:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">The "order" value provides a balancing optimization. It controls the order in which the vmTypes are picked by the balancing algorithm. Each vmType must have its own order of [1, 2, 3] where 1 is first.  Balance order can have profound effect on the balance results because VM's balanced <i>earlier</i> tend to get placed easier versus VM's placed <i>later</i> which may end up in "unplaced" when insufficient room is available.</td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>cpu & mem:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">Balancing algorithm will not load up a hypervisor beyond these CPU and MEM percentage limits</td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>restrictions / affinity:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">Limits placement of nodeTypes with other nodeTypes within a hypervisor</td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>restrictions / rackRestriction:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">A boolean value [ <code>true</code> | <code>false</code> ] to indicate if balancer should restrict VM rack placement to prevent same node leg on same rack.  That is, the algorithm will assign VMs to hypervisors such that 01, 02, 03 legs are on <i>separate</i> racks.  If the vmType has other legs (04, 05, etc) then they will be placed in sequential order
                        <b>Note:</b> "Sensitive" nodes are exempt from this restriction and are allowed to accumulate on hypervisors in the same rack.</td>
                </tr>
                <tr>
                    <td align="right" valign="top"><b>constraints / rackLimit:</b></td>
                    <td valign="top" style="border-bottom: 1px solid rgba(0,0,0,0.16);">
                        Limits available racks for the purposes of balancing.  Typical valid values are 1, 2, or 3.  Specifically,<br/>
                        <table border="1" cellspacing="0" cellpadding="3" style="border: 1px solid white">
                            <tr><td><code>=</code></td><td><b>1</b></td><td>Force <b>Primary rack only</b>.  All hypervisors (including untagged) are considered to be in the Primary rack for balancing.</td></tr>
                            <tr><td><code>=</code></td><td><b>2</b></td><td>Force <b>Primary & Secondary rack only</b>.  Any hypervisors that are on Quorum rack or untagged will be alternatively assigned to Primary & Secondary racks for balancing.</td></tr>
                            <tr><td><code>≥</code></td><td><b>3</b></td><td>Default 3-rack view.  No hypervisor rack re-assignments are made.</td></tr>
                        </table>
                        Also, if the constraint is omitted or the value is ≤ 0, then the default 3-rack view is used.
                    </td>
                </tr>
            </table>
        `;
