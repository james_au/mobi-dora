//
// Utility Javascript function and classes
//

// Use this key for session storage to store original URL so that login page can redirect user back to originally
// intended destination page
const SESSION_KEY_DORA_URL = "DORA_DEST_URL";

// Handles progressive percentage calculations which are useful for controlling progress bar UIs
//
class ProgressStats {
    constructor(total = 0) {
        this.total = total;
        this.completed = 0;
        this.startTimeMillis = Date.now();
    }

    reset() {
        this.total = total;
        this.completed = 0;
        this.startTimeMillis = Date.now();
    }

    // Set a new total
    setTotal(total) {
        this.total = total;
    }

    // Set a new completed count
    setCompleted(completed) {
        this.completed = completed;
    }

    // Gets current completion count
    getCompletedCount() {
        return this.completed;
    }

    // Gets the current duration time since start (in seconds)
    getCurrentDurationSecs() {
        return (Date.now() - this.startTimeMillis) / 1000;
    }

    // Increment the total such as in in case total is only known by streaming preview records sequentially
    incrementTotal(incr = 1) {
        this.total += incr;
    }

    // Get the new percentage after incrementing completed count by one where percentage = 100 * completed / total.
    // The optional "precision" value can be given for how many decimal places to show in the result and defaults
    // to 0.
    //
    // NOTE: if the total is 0 then this returns string "--" otherwise this would result in Infinity
    incrementPercentageCompleted(precision = 0) {
        this.completed++;

        if (this.total !== 0) {
            let percent100 = 100 * this.completed / this.total;

            return percent100.toFixed(precision);
        } else {
            return "--";
        }
    }
}

// Used to display balancer log messages to the GUI

class LogEntry {
    _recordName;
    _recordType;
    _message;
    _solution;

    constructor(recordName, recordType, message, solution) {
        this._recordName = recordName;
        this._recordType = recordType;
        this._message = message;
        this._solution = solution;
    }

    getRecordName() {
        return this._recordName;
    }

    getRecordType() {
        return this._recordType;
    }

    getMessage() {
        return this._message;
    }

    getSolution() {
        return this._solution;
    }
}

// Simple 2-tuple to pair a HTML view and data typically used for setAttribute() and later getAttribute() to keep
// setter and getter consistent
class AttribTuple {
    constructor(html, value) {
        this.html = html;
        this.data = value;
    }
}

// Deletes all cells of the referenced table row
//
function deleteTableRowCells(row) {
    for (i = row.cells.length - 1; i >= 0; i--) {
        row.removeChild(row.cells[i]);
    }
}

// Key for setting & retrieving element attribute from table cells
const DORA_TABLE_CELL_DATA = "dora_cell_data";

// Attribute key for marking a table cell with its column name
const DORA_TABLE_CELL_COLUMN_NAME = "dora_table_column_name";

// Simple macro to help ease the creation of table header columns
//
function createHeaderCell(label, infoMessage = "") {
    let headerCell = document.createElement("TH");
    headerCell.innerHTML = label;
    headerCell.setAttribute(DORA_TABLE_CELL_DATA, label)

    // Add tooltip only if specified
    if (trimToBlank(infoMessage).length > 0) {
        headerCell.innerHTML += "&nbsp;&nbsp;<div class='tooltip'><img src='images/question.svg' height='10pt' alt='?'><span class='tooltiptext'>" + escapeHtml(infoMessage) + "</span></div>"
    }

    return headerCell;
}


/**
 * Use this to create a standard Dora table data cell with a given label (required) and optional "data" value and
 * "tooltip" values
 *
 * @param label      required label visible on the UI and can contain HTML formatting
 * @param data       (optional) to set a non-HTML version of the label which the TSV exporter will use if available.  If not specified, the exporters will use the label directly
 * @param tooltip    (optional) to set a tooltip mouseover on the cell
 * @param columnName (optional) set data attribute for column's name for selection later
 * @param align      (optional) set the text alignment.  Ex: "left", "right", "center"
 * @returns {HTMLElement}  Formatted TD cell ready for appending to a table TR
 */
function createDataTableCell(label, data = null, tooltip = null, columnName = null, align = "") {
    const cell = document.createElement("TD");

    // Text alignment within the cell.  Defaults to "" which is left alignment.
    cell.style.textAlign = align;

    if (typeof(label) === "string") {
        // Label is a regular string
        if (isBlank(tooltip)) {
            cell.innerHTML = label;
        } else {
            cell.innerHTML = `<span class="tooltip">${label}<span class="tooltiptext">${tooltip}</span></span>`;
        }
    } else {
        // Label is a complex type so add as a full child element
        cell.append(label);
    }

    // Set data if specified
    if (!isBlank(data)) {
        cell.setAttribute(DORA_TABLE_CELL_DATA, data);
    }

    if (columnName !== null) {
        cell.setAttribute(DORA_TABLE_CELL_COLUMN_NAME, columnName);
    }

    return cell;
}

// Retrieves Dora table cell attribute if available from the given tablecell.  If not data attribute was set then the
// cell's label textContent is returned instead.
//
function getCellData(tablecell) {
    const data = tablecell.getAttribute(DORA_TABLE_CELL_DATA);

    if (!isBlank(data)) {
        return data;
    } else {
        return tablecell.textContent;
    }
}

// Searches the DOM subtree starting at the specified root element for DOM element that has the specified attribute
// name.  Will return the *first* element found in a breadth-first search.
//
// Uses: This function is good for finding a previously marked tag with given attribute.
//
// NOTE: for performance, the startElement should be closest to the target element as possible to reduce search size
// NOTE: this function could be updated later to return a list of *all* matching nodes not just the first one
//
// PARAM: rootElement     reference to the DOM element to start search from
// PARAM: attributeName   the attribute name to look for
function findElementThatHasAttribute(startElement, attributeName) {
    //
    // IMPLEMENTATION NOTE: Searches breadth-first non-recursive
    //
    const queue = new Array(startElement);

    while (!isArrayEmpty(queue)) {
        // Take first element from queue (FIFO order)
        const currentElement = queue.shift();

        if (currentElement !== null) {
            // Process the node for matching attribute (if it an attributable node)
            if (typeof(currentElement.hasAttribute) === "function" && currentElement.hasAttribute(attributeName)) {
                // Found the matching element -- return immediately
                return currentElement;
            }

            // Add child nodes (if any) and process those at the top of the loop
            if (currentElement.hasChildNodes()) {
                queue.push(...currentElement.childNodes);
            }
        }
    }

    // No matching element found
    return null;
}


/**
 * Returns all elements under the given root element that match the given condition function.
 *
 * NOTE: this search space should be as restrictive as possible by passing a root element that is <i>closest</i> to
 * the child elements.  Otherwise this utility still works but will search an unnecessarily large tree space.
 *
 * @param rootElement          DOM element to start searching from
 * @param conditionFunction    anonymous predicate function that selects which elements to return
 * @return {HTMLBaseElement[]} an array of DOM element objects matching the condition function
 */
 function searchForDomElements(rootElement, conditionFunction) {
    //
    // IMPLEMENTATION NOTE: Searches breadth-first non-recursive
    //
    const queue = new Array(rootElement);
    const results = new Array();

    while (!isArrayEmpty(queue)) {
        // Take first element from queue (FIFO order)
        const currentElement = queue.shift();

        if (currentElement !== null) {
            // Process the node
            if (conditionFunction(currentElement)) {
                results.push(currentElement);
            }

            // Add child nodes (if any) and process those at the top of the loop (FIFO order)
            if (typeof(currentElement.hasChildNodes) === "function" && currentElement.hasChildNodes()) {
                queue.push(...currentElement.childNodes);
            }
        }
    }

    return results;
}

/**
 * Removes all the child nodes from the given DOM reference
 */
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

// Extracts the hostname from a MobiTV VCenter path.
// Ex: /PAYTV/host/Rack-C6-SRV/c-06-33.infra.smf1.mobitv returns c-06-33.infra.smf1.mobitv
//
function extractVCenterPathHost(vcenterPath) {
    if (!isBlank(vcenterPath)) {
        let regexp = /^.*?\/([^\/]+)$/;   // This is compiled only once by browser even though it is local
        let matchGroups = vcenterPath.trim().match(regexp);

        if (matchGroups.length > 0) {
            return matchGroups[1];
        } else {
            return "";
        }
    } else {
        return "";
    }
}

function trimToBlank(s) {
    if (typeof(s) === "undefined" || s === null) {
        return "";
    } else {
        return s.trim();
    }
}

function isBlank(s) {
    return trimToBlank(s) === "";
}

function isNotBlank(s) {
    return !isBlank(s);
}

// Returns true if the array is undefined or null or empty size and false otherwise
//
function isArrayEmpty(array) {
    return typeof array === "undefined" || array === null || array.length === 0;
}

// Escapes strings for displaying directly in HTML page.  This escape *MUST* be used for any data
// written out to the page to prevent rendering errors should the data have any HTMl control characters
// especially the < and > characters.
function escapeHtml(str) {
    if (str !== null) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    } else {
        return "";
    }
}

// Runs the usual join() on an array but guards against null pointer if array reference is null or undefined
//
function safeJoin(array, separator) {
    if (!isArrayEmpty(array)) {
        return array.join(separator);
    } else {
        return "";
    }
}

// Returns given string surrounded by <nobr>...</nobr> tag
function nobr(string) {
    if (!isBlank(string)) {
        return `<nobr>${string}</nobr>`;
    } else {
        // Nothing to do so just return string as-is
        return string
    }
}

function isUiMsg(doraGrpcRecord) {
    return !isBlank(doraGrpcRecord.getUiMsgInfo()) || !isBlank(doraGrpcRecord.getUiMsgWarning()) || !isBlank(doraGrpcRecord.getUiMsgError());
}

// Performs "standard" float rounding.  This should be used by all UI to keep rounding consistent.
//
function floatRound(f, precision=2) {
    if (f !== null) {
        return f.toFixed(precision);
    } else {
        return "0";
    }
}

// Finds the preview record which then allows an updated fully detailed record to replace it.  The match criteria
// is any text cell that matches so this utility method can be generically used from multiple situations.
function findRecordInTableBody(tableBody, text) {
    // Linear search.  Return on first match.
    for (i = 0; i < tableBody.rows.length; i++) {
        for (j = 0; j < tableBody.rows[i].cells.length; j++) {
            if (tableBody.rows[i].cells[j].innerHTML === text) {
                return tableBody.rows[i];
            }
        }
    }

    // Not match found if this point is reached
    return null;
}

// Exports the referenced table's data as a TSV document to the OS clipboard
//
// NOTES:
// (1) the table's first <THEAD> will be the header
// (2) the table's rows in its main will be the TSV data
//
function exportDataTSV(tableRef) {
    var delimiter = "\t";
    var text = "";

    if (tableRef !== null && tableRef.length > 0 && tableRef[0].rows !== null && tableRef[0].rows.length > 0) {
        // Iterate through table's row data
        for (var i = 0; i < tableRef[0].rows.length; i++) {
            var row = tableRef[0].rows[i];
            for (var j = 0; j < row.cells.length; j++) {
                var data = getCellData(row.cells[j]);
                text += data + delimiter;
            }
            text += "\n";
        }

        copyToClipboard(text);

        window.alert("Copied " + (tableRef[0].rows.length - 1) + " data rows to clipboard");
    } else {
        window.alert("No table data to copy to clipboard");
    }
}

// Copies the given text to the desktop's clipboard.
//
// Ref: https://stackoverflow.com/a/46118025
function copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

// Standardized error message when a gRPC error object is obtained.  Only call this if you have an error message other-
// wise you'll get a null return.
function grpcErrorMessage(messagePrefix, err) {
    if (err !== null) {
        return messagePrefix + JSON.stringify(err);
    } else {
        return null;
    }
}

// Use this to convert a database millis time to human readable time consistently across the Dora UI
function convertMillisToTimestamp(millis) {
    if (millis !== null && millis > 0) {
        // Use local browser timezone output using the "Moment" library
        let date = moment(millis);
        return date.format("YYYY-MM-DD HH:mm:ss (ZZ)");
    } else {
        return "";
    }
}

// Use this to extract the datasource metadata from a grpc record with "UI info" attached to it
function parseDataSource(datasourceRawString) {
    let json = datasourceRawString.replace(/^DATASOURCE:/, "");

    return JSON.parse(json);
}

// Opens side hamburger menu
//
//
function openNav() {
    document.getElementById("doraUserSideNav").style.width = "250px";  // Expand out the menu to this width
}
function closeNav() {
    document.getElementById("doraUserSideNav").style.width = "0";  // Shrink menu width to 0 to hide
}

// Determines if the user has "dora-admins" role.  This is *not* a secure method and should only be used for
// informational purposes on the UI.  This method is not secure because the auth signature cannot be verified
// on the frontend without access to the secret key that was used to generate the signature originally.
function isUserAdmin(jwtTokenString) {
    if (jwtTokenString !== null) {
        let tokenBody = parseAuthToken(jwtTokenString);

        if (tokenBody !== null) {
            if (tokenBody.groups !== null && tokenBody.groups.length > 0) {
                // Linear search return on first match
                for (const group of tokenBody.groups) {
                    if (group.includes("cn=dora-admins")) {
                        return true;
                    }
                }
            }
        }
    }

    // If this point is reached then the user does not have the dora-admins group attached to LDAP account
    return false;
}


var keepAliveLastCheckTimestamp = new Date();
const keepAlivePollIntervalSec = 60;

/**
 * Call on any page interaction (mouse movement, key strokes, etc) and will maintain keep alive with the server.  This
 * should be referenced in a mouse/keyboard handler from all auth protected Dora HTML pages.
 *
 * @param {Event} e  the event object direct from the page
 */
function keepAliveCheck(e) {
    // NOTE: this log output produces a lot of logs but can be useful for debugging that activity events are logging
    //console.info(`Activity detected: ${e.originalEvent}`);

    // Only send keep-alive if interval time has elapsed which *prevents flooding* the server with excessive
    // keep-alive calls
    //
    const currentTimestamp = new Date();
    if (keepAliveLastCheckTimestamp.getTime() + keepAlivePollIntervalSec*1000 < currentTimestamp.getTime()) {
        console.info(`Requesting auth token "keep alive" extension from server...`);

        dora.keepAlive(client, getAuthToken(), (err, keepAliveResponse) => {
            if (err !== null) {
                // Keep alive request results in low-level fatal error.  Likely culprit is network failure
                // like user's network or VPN layer is down
                // window.alert(grpcErrorMessage("Could not request keep alive for session from server", err))
                console.error(`Keep-alive server call FAILED.  Suggestions: check auth token not expired; check network connectivity. Error: ${grpcErrorMessage("GRPC Error Message:", err)}`);
            } else {
                // Keep alive request successful
                console.info("Keep-alive server call SUCCESSFUL");

                // Check if server issued a replacement auth token and use the new token going forward
                if (isNotBlank(keepAliveResponse.getAuthTokenReplacement())) {
                    // Server issued a replacement auth token which client must update with to keep login going
                    setAuthToken(keepAliveResponse.getAuthTokenReplacement());
                    console.info(`Received replacement keep-alive auth token: ${keepAliveResponse.getAuthTokenReplacement()}`);
                }
            }
        });

        keepAliveLastCheckTimestamp = currentTimestamp;
    }
}

// Decodes the JSON body from the given JWT token and returns as a JSON object.  The JWT's header and signature
// sections are not needed at the UI level.
//
function parseAuthToken(jwtToken) {
    if (jwtToken !== null && jwtToken.length > 0) {
        let [header, body, signature] = jwtToken.split(".");
        let tokenBody = JSON.parse(atob(body));

        return tokenBody;
    } else {
        return null;
    }
}

// Safely returns the username extracted from the given auth token.  If cannot be parsed then a empty string "" is
// returned instead
function extractUserName(jwtToken) {
    const token = parseAuthToken(jwtToken)
    if (token !== null) {
        return token.username;
    } else {
        return "";
    }
}

// Popups up a browser window alert with information on the currently logged in user's given auth token JWT string
//
function showUserInfo(jwtTokenString) {
    let authToken = parseAuthToken(jwtTokenString);

    // TODO: Parse out the user's LDAP groups and show them as well

    if (authToken !== null) {
        window.alert("Username: " + authToken.username + "\nAdminLevel: " + isUserAdmin(jwtTokenString) + "\nAuth expires: " + new Date(authToken.exp * 1000));
    } else {
        window.alert("No auth token found");
    }
}

// Creates a key/value HashMap object of the given form fields.  This map can then be used to generate
// a URL parameter list which can be used for updating the window location URL to enable deep-linking.
//
// Input: list of parameter names/IDs
// Output: map of parameter names with values fetched via DOM lookup on param ID
function createMapFromParams(...paramNames) {
    const mapParams = new Map();

    for (const param of paramNames) {
        const element = document.getElementById(param);

        if (element !== null) {
            if (element.type === "select-one") {
                // Combo Box - Use the selection's "text label" and not the "value"
                const selectedOption = element.options[element.selectedIndex];
                if (typeof (selectedOption) !== "undefined" && selectedOption !== null) {
                    mapParams.set(param, selectedOption.text);
                } else {
                    console.warn('Specified combobox ${param} does not have matching value or combobox is empty');
                }
            } else if (element.type === "checkbox") {
                // Check Box - convert to boolean
                mapParams.set(param, element.checked);
            } else {
                mapParams.set(param, element.value);
            }
        } else {
            // The param was not found as and ID.  It likely is a multi-parameter radiobutton

            // Try as radio button with only one item out of multiple checked
            const radioboxes = $(`input[name="${param}"]`);
            if (!isArrayEmpty(radioboxes)) {
                // Save the checked radiobox's value.  Only one of the radio box values is checked.
                mapParams.set(param, $(`input[name="${param}"]:checked`).val());
            }
        }
    }

    return mapParams;
}

// Returns the current URL *without* any parameters or anchors.  This is needed in order to update the URL with
// new parameters.
//
// Ref: https://www.w3schools.com/jsref/obj_location.asp
function currentUrlNoParams() {
    return `${location.protocol}//${location.host}${location.pathname}`;
}

// Removes the specified index element from the array.  An updated array is returned.
function removeArrayElementIndex(arr, index) {
    let output = new Array();

    for (let i = 0; i < arr.length; i++) {
        if (i !== index) {
            output.push(arr[i]);
        }
    }

    return output;
}

var reNodeType = /([^\d]+).*/gm;
function parseNodeType(vmHostname) {
    if (typeof vmHostname !== "undefined") {
        let matchAll = trimToBlank(vmHostname).matchAll(reNodeType);
        matchAll = Array.from(matchAll);

        if (matchAll !== null && matchAll.length >= 1) {
            if (matchAll[0].length >= 2) {
                return matchAll[0][1];
            }
        }
    }

    return null;
}

// Extracts that hypervisor's Cluster from the given VCenter path.  NOTE: cluster might be "" if the hypervisor is
// not assigned to a cluster.
//
// Example input Hypervisor path with both cluster and hostname:
//     /PAYTV-SEGMENTERS/host/PAYTV-SEGMENTERS/c-04-16.infra.smf1.mobitv will return cluster = "PAYTV-SEGMENTERS"
//
// Example input Hypervisor path with both no cluster and hostname.  Cluster is "" because of duplicate host
//     /PAYTV-SEGMENTERS/host/c-04-16.infra.smf1.mobitv/c-04-16.infra.smf1.mobitv will return cluster = ""
//
function extractHypervisorCluster(hypervisorPath) {
    let hostname = "";

    if (!isBlank(hypervisorPath)) {
        const elements = trimToBlank(hypervisorPath).split("/");

        // Hostname is always the last element.  Hostname is needed to check cluster is empty or not.
        if (elements.length > 0) {
            hostname = elements[elements.length - 1];
        }

        // Look for the cluster in the expected position after the "host" element and only if the hostname
        // is *not* duplicated in the path.
        for (var i = 0; i < elements.length; i++) {
            const ele = elements[i];
            if (ele === "host") {
                if (i < elements.length - 1) {
                    if (elements[i + 1] !== hostname) {
                        return elements[i + 1];
                    } else {
                        return "";
                    }
                }
            }
        }
    }

    // No cluster found if this point is reached
    return "";
}

// Runs any given function asynchronously
function executeAsync(func) {
    setTimeout(func, 0);
}

// Sleep a given millis at which point the the promise is resolved.  Client code can await the promise to effectively
// wait the given milliseconds.
//
// NOTE: a Promise object is returned right away and client code must "await" the promise
// NOTE: client must be an 'async' function and this has other ramifications to cient coding.  Use this utility method
//       with caution.
async function sleep(millis) {
    // Resolve the promise when the millis has expired
    // setTimeout(() => {Promise.resolve()}, millis);
    return new Promise(resolve => setTimeout(resolve, millis));
}


// "Toggles" the referenced JQuery combobox which triggers its event handlers.  This is useful for initializing
// the page state by re-using the combobox's already defined event handling code.
//
// PARAM: comboboxJqueryRef  the JQuery reference to the combobox to toggle
// PARAM: prefix             optional prefix set the combobox to the *first* matching option (by name) matching
//                           the prefix.  If no match then the combobox remainson  whatever option was selected.
function toggleComboBox(comboBoxJQueryRef, prefix="") {
    // Default the selected index to the currently selected whatever it happens to be...
    let currentIndex = $(comboBoxJQueryRef).find("option:selected").index();

    // ...but if a prefix is provided then override to the currently selected index to the first matching option
    if (!isBlank(prefix)) {
        const matching = findOptionsWithMatchingPrefix(comboBoxJQueryRef, prefix);
        if (!isArrayEmpty(matching)) {
            currentIndex = matching[0].index;
        }
    }

    const length = $(comboBoxJQueryRef).find("option").length;
    console.info(`Current environment combobox index: ${currentIndex} and size: ${length}`);

    if (length > 0) {
        $(comboBoxJQueryRef).prop("selectedIndex", Math.min(currentIndex + 1, length - 1));
        $(comboBoxJQueryRef).change();

        // Slight delay to allow selection code to "settle" in.  Maybe try to synchronize this?
        setTimeout(function() {
            $(comboBoxJQueryRef).prop("selectedIndex", currentIndex);
            $(comboBoxJQueryRef).change();
        }, 64);
    }
}

// Searches the JQuery referenced combobox and returns *all* options with the name that starts with the given
// key prefix.
//
// RETURNS: 0 or more matching combo box objects. REMINDER: use the "index" field if you want to know the option's
//          original position in its combobox list
// PARAM: comboboxJqueryRef  the JQuery reference to the combobox to toggle
// PARAM: keyPrefix          prefix to search for
function findOptionsWithMatchingPrefix(comboBoxJQueryRef, keyPrefix) {
    return $(comboBoxJQueryRef).find("option").filter(function() {
        // Include option records only if their name starts with the given prefix
        return ($(this)[0].text.startsWith(keyPrefix));
    });
}

// Sets the referenced comboboxId (ID string) currently selected option to the first opion that that matches the given
// value string
//
// PARAM: comboboxId  the string ID of the combobox to set
// PARAM: value       set the first found option that matches this value string
function setComboBoxSelectionByValue(comboboxId, value) {
    $(`#${comboboxId} option`)
        .filter(function() {
            return ($(this).text() === value);
        })
        .prop("selected", true);
}

/**
 * A simple macro factory method for creating a FileReader object and setting its onload handler in one line
 *
 * @param onload     the callback function when reading the file
 * @returns {FileReader}
 */
function createFileReader(onload) {
    let fileReader = new FileReader();

    fileReader.onload = onload;

    return fileReader;
}

/**
 * Generates a unique number
 *
 * @returns {number}
 */
function uniqueNumber() {
    // TODO: Might not be unique if two threads call this exactly at the same time
    return new Date().getTime() ^ Math.floor(10000000 * Math.random());
}

/**
 * Shuffles the given array randomly in-place
 *
 * @param array  array to sort and will be mutated
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

/**
 * Processes given string and ensures it is a legit HTML ID be removing invalid characters like spaces
 *
 * @param s  string
 * @returns {string}
 */
function makeVariableName(s) {
    if (isNotBlank(s)) {
        return s.replace(/\s*\t*-*\+*/gi, "");
    } else {
        return "";
    }
}

/**
 * Determines if the given TR row has all empty cells
 *
 * @param {HTMLTableRowElement} row
 * @return {boolean}  <code>true</code> if TR contains all blank TD cells and <code>false</code> otherwise
 */
function isTableRowEmpty(row) {
    for (const td of row.childNodes) {
        if (isNotBlank(td.innerText)) {
            return false;
        }
    }

    // If this point is reached then all TD cells are blank
    return true;
}

// A simple stack
//
class Stack {
    constructor(...items){
        this.data = items;
    }

    push(item) {
        this.data.push(item);
    }

    length() {
        return this.data.length;
    }

    peek() {
        return this.data[this.data.length - 1];
    }

    clear() {
        this.data.length = 0;
    }

    isEmpty() {
        return this.length() === 0;
    }

    pop() {
        if( this.isEmpty() === false ) {
            return this.data.pop(); // removes the last element
        }
    }

    print() {
        let idx = this.data.length - 1;
        while(idx >= 0) { // print upto 0th index
            console.log(this.data[idx]);
            idx--;
        }
    }

    reverse() {
        this._reverse(this.data.length - 1 );
    }

    _reverse(index) {
        if(index != 0) {
            this._reverse(index-1);
        }
        console.log(this.data[index]);
    }
}

// This is a stack with "indexing" abilities useful for helping to implement "undo" history or a history of any
// other changes of objects.  Maintains an index that can be moved incrementally.
//
class IndexedStack extends Stack {
    constructor(...items) {
        super(...items);

        this.index = 0;
    }

    // Gets the current index number
    getCurrentIndex() {
        return this.index;
    }

    // Gets the current indexed object
    getCurrentItem() {
        return this.data[this.getCurrentIndex()];
    }

    // Gets the object in the stack at any given index
    getIndexItem(i) {
        return this.data[i];
    }

    // Clears entire data and resets index to 0
    clear() {
        super.clear();
        this.index = 0;
    }

    // A quick way to determine if the index is pointing at the last element or not.  Will return false if index
    // is pointing at the last element and true otherwise.
    hasNext() {
        return this.index < this.data.length - 1;
    }

    // Moves to next index and returns the object at index.
    // NOTE: Will *not* advance past the last object in the stack
    next() {
        if (this.index < this.data.length - 1) {
            this.index++;
        }
        return this.getIndexItem(this.index);
    }

    // A quick way to determine if the index is pointing at the first element or not.  Will return false if index
    // is pointing at the first element and true otherwise.
    hasPrevious() {
        return this.index > 0;
    }

    // Moves to the previous index and returns the object at index.
    // NOTE: Will *not* move past the first index
    previous() {
        if (this.index > 0) {
            this.index--;
        }

        return this.getIndexItem(this.index);
    }

    // Moves index to the first item and returns the object there
    first() {
        this.index = 0;
        return this.getIndexItem(this.index);
    }

    // Moves index to the last items and returns the object there
    last() {
        this.index = this.data.length - 1;
        return this.getIndexItem(this.index);
    }

    // Pushes an item onto the Indexed Stack but will automatically pop off any items that are above the index.
    //
    // This allows "history replacement" if user indexed back in the history and then decides to push on a change.
    //
    pushOut(item) {
        const distanceFromTopOfStack = this.length() - this.index - 1;
        const itemsPopped = new Array();

        // Pop off any and all items above the current index
        for (var i = 0; i < distanceFromTopOfStack; i++) {
            // Pop item the stack but save to separate array
            itemsPopped.push(this.pop());
        }

        this.push(item);  // Now push on the item that replaces any popped out entries
        this.last();      // Index to the last element

        // Return list of zero or more items that were popped off for reference
        return itemsPopped;
    }
}

const STORAGE_UPLOAD_BLURB = `
<span class="subtitle1">Uploading Replacement Data</span>
<table class="uiInfo uiInfoSmall">
    <tr><td>
    You can upload a data file to <i>replace</i> the existing data.  Recommended procedure:
    <ol>
        <li>Export the existing data (even if blank) to a local file</li>
        <li>Update the local file with your new data. <i>Be sure to preserve <b>headers</b> & <b>tabs</b>!</i></li>
        <li>Upload the new data</li>
    </ol>
    </td></tr>
</table><br/>
`;