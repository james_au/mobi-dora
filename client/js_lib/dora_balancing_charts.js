// This is the default "base" config which the charts below will use and override as necessary
const baseColor = "200,37,37";
const defaultConfig = {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [],          // Will be populated by data generation below
            backgroundColor: [],   // Colors will be populated by data generation below
            label: 'Node Types'
        }],
        labels: []   // will be populated at runtime
    },
    options: {
        cutoutPercentage: 59,
        responsive: true,
        legend: {
            position: 'left',
            labels: {
                fontSize: 10
            }
        },
        title: {
            display: true,
            text: "Default Title",    // This should be overriden
            fontSize: 16
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
};

// Generates data for the "node type by count" chart
//
function generateTopNodesChartData(dataModel, limit, initialColor=baseColor) {
    // Count the node types
    let mapVmNodeTypeCount = new Map();
    let totalCount = 0;
    for (const hypervisorRec of dataModel.getHypervisorRecords()) {
        for (const vmRec of hypervisorRec.getVmCurrentList()) {
            let nodeType = parseNodeType(vmRec.getHostname());
            // Lazy initialize
            if (!mapVmNodeTypeCount.has(nodeType)) {
                mapVmNodeTypeCount.set(nodeType, 0);
            }

            mapVmNodeTypeCount.set(nodeType, mapVmNodeTypeCount.get(nodeType) + 1);
            totalCount++;
        }
    }

    console.info(`Node type stats generated with ${mapVmNodeTypeCount.size} entries`);

    let sortedNodeTypeCounts = Array.from(mapVmNodeTypeCount.entries());
    sortedNodeTypeCounts.sort(sortFunction);

    console.info("Sorting completed");

    // Make a *copy* of the default Chart config and override as necessary
    let chartDataTopNodes = JSON.parse(JSON.stringify(defaultConfig));
    chartDataTopNodes.options.title.text = `Top ${(sortedNodeTypeCounts.length > limit ? limit : "")} Node Types (by count)`;
    chartDataTopNodes.data.datasets[0].backgroundColor.push(...generateGradation(initialColor, 1.00, 0.15, Math.min(limit, sortedNodeTypeCounts.length - 1)));

    let idx = 1;
    for (const ele of sortedNodeTypeCounts) {
        chartDataTopNodes.data.labels.push(`${ele[0]} (${floatRound(100 * ele[1] / totalCount, 1)}%)`);
        chartDataTopNodes.data.datasets[0].data.push(ele[1]);
        if (idx++ >= limit) {
            break;
        }
    }

    if (sortedNodeTypeCounts.length >= limit) {
        // Calculate "others" as a sum of all remaining
        let othersCount = 0;

        for (var i = limit; i < sortedNodeTypeCounts.length; i++) {
            othersCount += sortedNodeTypeCounts[i][1];
        }

        chartDataTopNodes.data.labels.push(`Others (${floatRound(100 * othersCount / totalCount, 1)}%)`);
        chartDataTopNodes.data.datasets[0].data.push(othersCount);
    }

    return chartDataTopNodes;
}

// Generates data for the "node type by CPU" chart
//
function generateTopCpuNodesChartData(dataModel, limit, initialColor=baseColor) {
    // Count the node types CPU usage
    let mapCpuGroupByNodeType = new Map();
    let hypervisorsTotalCPU = 0;
    let vmTotalAllocCpu = 0;
    for (const hypervisorRec of dataModel.getHypervisorRecords()) {
        for (const vmRec of hypervisorRec.getVmCurrentList()) {
            let nodeType = parseNodeType(vmRec.getHostname());
            // Lazy initialize
            if (!mapCpuGroupByNodeType.has(nodeType)) {
                mapCpuGroupByNodeType.set(nodeType, 0);
            }

            mapCpuGroupByNodeType.set(nodeType, mapCpuGroupByNodeType.get(nodeType) + vmRec.getCpu());
            vmTotalAllocCpu += vmRec.getCpu();
        }

        hypervisorsTotalCPU += hypervisorRec.getCpu();
    }

    let sortedNodeTypeCounts = Array.from(mapCpuGroupByNodeType.entries());
    sortedNodeTypeCounts.sort(sortFunction);

    console.info("Sorting completed");

    // Make a *copy* of the default Chart config and override as necessary
    let chartDataTopCpuNodes = JSON.parse(JSON.stringify(defaultConfig));
    chartDataTopCpuNodes.options.title.text = `Top ${(sortedNodeTypeCounts.length > limit ? limit : "")} Node Types (by CPU)`;
    chartDataTopCpuNodes.data.datasets[0].backgroundColor.push(...generateGradation(initialColor,1.00, 0.15, Math.min(limit, sortedNodeTypeCounts.length - 1)));

    let idx = 1;
    for (const ele of sortedNodeTypeCounts) {
        chartDataTopCpuNodes.data.labels.push(`${ele[0]} (${floatRound(100 * ele[1] / hypervisorsTotalCPU, 1)}%)`);
        chartDataTopCpuNodes.data.datasets[0].data.push(ele[1]);
        if (idx++ >= limit) {
            break;
        }
    }

    if (sortedNodeTypeCounts.length >= limit) {
        // Calculate "others" as a sum of all remaining
        let othersCount = 0;

        for (var i = limit; i < sortedNodeTypeCounts.length; i++) {
            othersCount += sortedNodeTypeCounts[i][1];
        }

        chartDataTopCpuNodes.data.labels.push(`Others (${floatRound(100 * othersCount / hypervisorsTotalCPU, 1)}%)`);
        chartDataTopCpuNodes.data.datasets[0].data.push(othersCount);
    }

    // Add available CPU remaining (but not if negative)
    let availableCPU = hypervisorsTotalCPU - vmTotalAllocCpu;
    if (availableCPU > 0) {
        chartDataTopCpuNodes.data.labels.push(`Avail CPU (${floatRound(100 * availableCPU / hypervisorsTotalCPU, 1)}%)`);
        chartDataTopCpuNodes.data.datasets[0].data.push(floatRound(availableCPU, 1));
    }

    console.info(`CPU nodetype stats generated with ${mapCpuGroupByNodeType.size} entries`);

    return chartDataTopCpuNodes;
}

// Generates data for the "node type by MEM" chart
//
function generateTopMemNodesChartData(dataModel, limit, initialColor=baseColor) {
    // Count the node types CPU usage
    let mapMemGroupByNodeType = new Map();
    let hypervisorsTotalMem = 0;
    let vmTotalAllocMem = 0;
    for (const hypervisorRec of dataModel.getHypervisorRecords()) {
        for (const vmRec of hypervisorRec.getVmCurrentList()) {
            let nodeType = parseNodeType(vmRec.getHostname());
            // Lazy initialize
            if (!mapMemGroupByNodeType.has(nodeType)) {
                mapMemGroupByNodeType.set(nodeType, 0);
            }

            mapMemGroupByNodeType.set(nodeType, mapMemGroupByNodeType.get(nodeType) + vmRec.getMemGb());
            vmTotalAllocMem += vmRec.getMemGb();
        }

        hypervisorsTotalMem += hypervisorRec.getMemGb();
    }

    let sortedNodeTypeCounts = Array.from(mapMemGroupByNodeType.entries());
    sortedNodeTypeCounts.sort(sortFunction);

    console.info("Sorting completed");

    // Make a *copy* of the default Chart config and override as necessary
    let chartDataTopMemNodes = JSON.parse(JSON.stringify(defaultConfig));
    chartDataTopMemNodes.options.title.text = `Top ${(sortedNodeTypeCounts.length > limit ? limit : "")} Node Types (by MEM GB)`;
    chartDataTopMemNodes.data.datasets[0].backgroundColor.push(...generateGradation(initialColor,1.00, 0.15, Math.min(limit, sortedNodeTypeCounts.length - 1)));

    let idx = 1;
    for (const ele of sortedNodeTypeCounts) {
        chartDataTopMemNodes.data.labels.push(`${ele[0]} (${floatRound(100 * ele[1] / hypervisorsTotalMem, 1)}%)`);
        chartDataTopMemNodes.data.datasets[0].data.push(ele[1]);
        if (idx++ >= limit) {
            break;
        }
    }

    if (sortedNodeTypeCounts.length >= limit) {
        // Calculate "others" as a sum of all remaining
        let othersCount = 0;

        for (var i = limit; i < sortedNodeTypeCounts.length; i++) {
            othersCount += sortedNodeTypeCounts[i][1];
        }

        chartDataTopMemNodes.data.labels.push(`Others (${floatRound(100 * othersCount / hypervisorsTotalMem, 1)}%)`);
        chartDataTopMemNodes.data.datasets[0].data.push(othersCount);
    }

    // Add available MEM remaining (but not if negative)
    let availableMem = hypervisorsTotalMem - vmTotalAllocMem;
    if (availableMem > 0) {
        chartDataTopMemNodes.data.labels.push(`Avail MEM (${floatRound(100 * availableMem / hypervisorsTotalMem, 1)}%)`);
        chartDataTopMemNodes.data.datasets[0].data.push(floatRound(availableMem, 1));
    }

    console.info(`MEM nodetype stats generated with ${mapMemGroupByNodeType.size} entries`);

    return chartDataTopMemNodes;
}

// Sorts by the value field which is the second field
//
function sortFunction (a, b) {
    if (a[1] > b[1]) {
        return -1;
    } else if (a[1] < b[1]) {
        return 1;
    } else {
        return 0;
    }
}

// Generates compatible JSChart "rgba" gradation colors from base color from startAlpha (1.00) to endAlpha with the given number of "steps"
//
// Param notes:
// baseColor is a comma separated rgb value like "123,13,29"
function generateGradation(baseColor, startAlpha, endAlpha, steps) {
    let colors = [];
    let decrement = (startAlpha - endAlpha) / (steps + 1);

    colors.push(`rgba(${baseColor}, ${startAlpha})`);
    for (var i = 1; i < steps - 1; i++) {
        colors.push(`rgba(${baseColor}, ${startAlpha - floatRound(i*decrement)})`);
    }
    colors.push(`rgba(${baseColor}, ${endAlpha})`);
    colors.push(`rgba(0, 0, 0, 0.07)`);       // Color for "Others" (light gray when over white)
    colors.push(`rgba(${baseColor}, 0.00)`);  // Transparent color MEM/CPU available (completely transparent will show circle incomplete which visually shows available space available)

    return colors;
}