/**
 * @fileoverview gRPC-Web generated client stub for api
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('./google/api/annotations_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.api = require('./dora_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.api.DoraClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.api.DoraPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.KeepAliveRequest,
 *   !proto.api.KeepAliveResponse>}
 */
const methodDescriptor_Dora_KeepAlive = new grpc.web.MethodDescriptor(
  '/api.Dora/KeepAlive',
  grpc.web.MethodType.UNARY,
  proto.api.KeepAliveRequest,
  proto.api.KeepAliveResponse,
  /**
   * @param {!proto.api.KeepAliveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.KeepAliveResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.KeepAliveRequest,
 *   !proto.api.KeepAliveResponse>}
 */
const methodInfo_Dora_KeepAlive = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.KeepAliveResponse,
  /**
   * @param {!proto.api.KeepAliveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.KeepAliveResponse.deserializeBinary
);


/**
 * @param {!proto.api.KeepAliveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.KeepAliveResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.KeepAliveResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.keepAlive =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/KeepAlive',
      request,
      metadata || {},
      methodDescriptor_Dora_KeepAlive,
      callback);
};


/**
 * @param {!proto.api.KeepAliveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.KeepAliveResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.keepAlive =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/KeepAlive',
      request,
      metadata || {},
      methodDescriptor_Dora_KeepAlive);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.LogoutRequest,
 *   !proto.api.LogoutResponse>}
 */
const methodDescriptor_Dora_Logout = new grpc.web.MethodDescriptor(
  '/api.Dora/Logout',
  grpc.web.MethodType.UNARY,
  proto.api.LogoutRequest,
  proto.api.LogoutResponse,
  /**
   * @param {!proto.api.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.LogoutResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.LogoutRequest,
 *   !proto.api.LogoutResponse>}
 */
const methodInfo_Dora_Logout = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.LogoutResponse,
  /**
   * @param {!proto.api.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.LogoutResponse.deserializeBinary
);


/**
 * @param {!proto.api.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.LogoutResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.LogoutResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.logout =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/Logout',
      request,
      metadata || {},
      methodDescriptor_Dora_Logout,
      callback);
};


/**
 * @param {!proto.api.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.LogoutResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.logout =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/Logout',
      request,
      metadata || {},
      methodDescriptor_Dora_Logout);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HVQueryRequest,
 *   !proto.api.HypervisorResponseArray>}
 */
const methodDescriptor_Dora_GetHypervisors = new grpc.web.MethodDescriptor(
  '/api.Dora/GetHypervisors',
  grpc.web.MethodType.UNARY,
  proto.api.HVQueryRequest,
  proto.api.HypervisorResponseArray,
  /**
   * @param {!proto.api.HVQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HVQueryRequest,
 *   !proto.api.HypervisorResponseArray>}
 */
const methodInfo_Dora_GetHypervisors = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorResponseArray,
  /**
   * @param {!proto.api.HVQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorResponseArray.deserializeBinary
);


/**
 * @param {!proto.api.HVQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.HypervisorResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getHypervisors =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetHypervisors',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisors,
      callback);
};


/**
 * @param {!proto.api.HVQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.HypervisorResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getHypervisors =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetHypervisors',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisors);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HVQueryRequest,
 *   !proto.api.HypervisorResponse>}
 */
const methodDescriptor_Dora_GetHypervisorsStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetHypervisorsStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.HVQueryRequest,
  proto.api.HypervisorResponse,
  /**
   * @param {!proto.api.HVQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HVQueryRequest,
 *   !proto.api.HypervisorResponse>}
 */
const methodInfo_Dora_GetHypervisorsStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorResponse,
  /**
   * @param {!proto.api.HVQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorResponse.deserializeBinary
);


/**
 * @param {!proto.api.HVQueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getHypervisorsStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetHypervisorsStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisorsStreamed);
};


/**
 * @param {!proto.api.HVQueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getHypervisorsStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetHypervisorsStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisorsStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VMQueryRequest,
 *   !proto.api.VirtualMachineResponseArray>}
 */
const methodDescriptor_Dora_GetVirtualMachines = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVirtualMachines',
  grpc.web.MethodType.UNARY,
  proto.api.VMQueryRequest,
  proto.api.VirtualMachineResponseArray,
  /**
   * @param {!proto.api.VMQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VirtualMachineResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VMQueryRequest,
 *   !proto.api.VirtualMachineResponseArray>}
 */
const methodInfo_Dora_GetVirtualMachines = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VirtualMachineResponseArray,
  /**
   * @param {!proto.api.VMQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VirtualMachineResponseArray.deserializeBinary
);


/**
 * @param {!proto.api.VMQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.VirtualMachineResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.VirtualMachineResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVirtualMachines =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetVirtualMachines',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVirtualMachines,
      callback);
};


/**
 * @param {!proto.api.VMQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.VirtualMachineResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getVirtualMachines =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetVirtualMachines',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVirtualMachines);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VMQueryRequest,
 *   !proto.api.VirtualMachineResponse>}
 */
const methodDescriptor_Dora_GetVirtualMachinesStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVirtualMachinesStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.VMQueryRequest,
  proto.api.VirtualMachineResponse,
  /**
   * @param {!proto.api.VMQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VirtualMachineResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VMQueryRequest,
 *   !proto.api.VirtualMachineResponse>}
 */
const methodInfo_Dora_GetVirtualMachinesStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VirtualMachineResponse,
  /**
   * @param {!proto.api.VMQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VirtualMachineResponse.deserializeBinary
);


/**
 * @param {!proto.api.VMQueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.VirtualMachineResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVirtualMachinesStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetVirtualMachinesStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVirtualMachinesStreamed);
};


/**
 * @param {!proto.api.VMQueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.VirtualMachineResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getVirtualMachinesStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetVirtualMachinesStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVirtualMachinesStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.QueryRequest,
 *   !proto.api.RoomResponseArray>}
 */
const methodDescriptor_Dora_GetRoom = new grpc.web.MethodDescriptor(
  '/api.Dora/GetRoom',
  grpc.web.MethodType.UNARY,
  proto.api.QueryRequest,
  proto.api.RoomResponseArray,
  /**
   * @param {!proto.api.QueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.RoomResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.QueryRequest,
 *   !proto.api.RoomResponseArray>}
 */
const methodInfo_Dora_GetRoom = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.RoomResponseArray,
  /**
   * @param {!proto.api.QueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.RoomResponseArray.deserializeBinary
);


/**
 * @param {!proto.api.QueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.RoomResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.RoomResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getRoom =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetRoom',
      request,
      metadata || {},
      methodDescriptor_Dora_GetRoom,
      callback);
};


/**
 * @param {!proto.api.QueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.RoomResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getRoom =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetRoom',
      request,
      metadata || {},
      methodDescriptor_Dora_GetRoom);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.QueryRequest,
 *   !proto.api.RoomResponse>}
 */
const methodDescriptor_Dora_GetRoomStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetRoomStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.QueryRequest,
  proto.api.RoomResponse,
  /**
   * @param {!proto.api.QueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.RoomResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.QueryRequest,
 *   !proto.api.RoomResponse>}
 */
const methodInfo_Dora_GetRoomStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.RoomResponse,
  /**
   * @param {!proto.api.QueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.RoomResponse.deserializeBinary
);


/**
 * @param {!proto.api.QueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.RoomResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getRoomStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetRoomStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetRoomStreamed);
};


/**
 * @param {!proto.api.QueryRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.RoomResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getRoomStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetRoomStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetRoomStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.SharedStorageReportRequest,
 *   !proto.api.SharedStorageReportResponse>}
 */
const methodDescriptor_Dora_GetSharedStorageReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetSharedStorageReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.SharedStorageReportRequest,
  proto.api.SharedStorageReportResponse,
  /**
   * @param {!proto.api.SharedStorageReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SharedStorageReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.SharedStorageReportRequest,
 *   !proto.api.SharedStorageReportResponse>}
 */
const methodInfo_Dora_GetSharedStorageReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.SharedStorageReportResponse,
  /**
   * @param {!proto.api.SharedStorageReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SharedStorageReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.SharedStorageReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.SharedStorageReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getSharedStorageReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetSharedStorageReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetSharedStorageReportStreamed);
};


/**
 * @param {!proto.api.SharedStorageReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.SharedStorageReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getSharedStorageReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetSharedStorageReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetSharedStorageReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.DatastoreReportRequest,
 *   !proto.api.DatastoreReportResponse>}
 */
const methodDescriptor_Dora_GetDatastoreReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetDatastoreReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.DatastoreReportRequest,
  proto.api.DatastoreReportResponse,
  /**
   * @param {!proto.api.DatastoreReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.DatastoreReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.DatastoreReportRequest,
 *   !proto.api.DatastoreReportResponse>}
 */
const methodInfo_Dora_GetDatastoreReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.DatastoreReportResponse,
  /**
   * @param {!proto.api.DatastoreReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.DatastoreReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.DatastoreReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.DatastoreReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getDatastoreReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetDatastoreReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetDatastoreReportStreamed);
};


/**
 * @param {!proto.api.DatastoreReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.DatastoreReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getDatastoreReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetDatastoreReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetDatastoreReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.NfsReportRequest,
 *   !proto.api.NfsReportResponse>}
 */
const methodDescriptor_Dora_GetNfsReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetNfsReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.NfsReportRequest,
  proto.api.NfsReportResponse,
  /**
   * @param {!proto.api.NfsReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.NfsReportRequest,
 *   !proto.api.NfsReportResponse>}
 */
const methodInfo_Dora_GetNfsReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.NfsReportResponse,
  /**
   * @param {!proto.api.NfsReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.NfsReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.NfsReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getNfsReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetNfsReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsReportStreamed);
};


/**
 * @param {!proto.api.NfsReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.NfsReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getNfsReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetNfsReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VmStorageReportRequest,
 *   !proto.api.VmStorageReportResponse>}
 */
const methodDescriptor_Dora_GetVmStorageReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVmStorageReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.VmStorageReportRequest,
  proto.api.VmStorageReportResponse,
  /**
   * @param {!proto.api.VmStorageReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmStorageReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VmStorageReportRequest,
 *   !proto.api.VmStorageReportResponse>}
 */
const methodInfo_Dora_GetVmStorageReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VmStorageReportResponse,
  /**
   * @param {!proto.api.VmStorageReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmStorageReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.VmStorageReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmStorageReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVmStorageReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetVmStorageReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVmStorageReportStreamed);
};


/**
 * @param {!proto.api.VmStorageReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmStorageReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getVmStorageReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetVmStorageReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVmStorageReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VmStorageUploadRequest,
 *   !proto.api.VmStorageUploadResponse>}
 */
const methodDescriptor_Dora_UploadVmStorageData = new grpc.web.MethodDescriptor(
  '/api.Dora/UploadVmStorageData',
  grpc.web.MethodType.UNARY,
  proto.api.VmStorageUploadRequest,
  proto.api.VmStorageUploadResponse,
  /**
   * @param {!proto.api.VmStorageUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmStorageUploadResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VmStorageUploadRequest,
 *   !proto.api.VmStorageUploadResponse>}
 */
const methodInfo_Dora_UploadVmStorageData = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VmStorageUploadResponse,
  /**
   * @param {!proto.api.VmStorageUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmStorageUploadResponse.deserializeBinary
);


/**
 * @param {!proto.api.VmStorageUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.VmStorageUploadResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmStorageUploadResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.uploadVmStorageData =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/UploadVmStorageData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadVmStorageData,
      callback);
};


/**
 * @param {!proto.api.VmStorageUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.VmStorageUploadResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.uploadVmStorageData =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/UploadVmStorageData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadVmStorageData);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.IpRangeReportRequest,
 *   !proto.api.IpRangeReportResponse>}
 */
const methodDescriptor_Dora_GetIpRangeReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetIpRangeReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.IpRangeReportRequest,
  proto.api.IpRangeReportResponse,
  /**
   * @param {!proto.api.IpRangeReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.IpRangeReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.IpRangeReportRequest,
 *   !proto.api.IpRangeReportResponse>}
 */
const methodInfo_Dora_GetIpRangeReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.IpRangeReportResponse,
  /**
   * @param {!proto.api.IpRangeReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.IpRangeReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.IpRangeReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.IpRangeReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getIpRangeReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetIpRangeReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetIpRangeReportStreamed);
};


/**
 * @param {!proto.api.IpRangeReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.IpRangeReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getIpRangeReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetIpRangeReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetIpRangeReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.IpRangeUploadRequest,
 *   !proto.api.IpRangeUploadResponse>}
 */
const methodDescriptor_Dora_UploadIpRangeData = new grpc.web.MethodDescriptor(
  '/api.Dora/UploadIpRangeData',
  grpc.web.MethodType.UNARY,
  proto.api.IpRangeUploadRequest,
  proto.api.IpRangeUploadResponse,
  /**
   * @param {!proto.api.IpRangeUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.IpRangeUploadResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.IpRangeUploadRequest,
 *   !proto.api.IpRangeUploadResponse>}
 */
const methodInfo_Dora_UploadIpRangeData = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.IpRangeUploadResponse,
  /**
   * @param {!proto.api.IpRangeUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.IpRangeUploadResponse.deserializeBinary
);


/**
 * @param {!proto.api.IpRangeUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.IpRangeUploadResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.IpRangeUploadResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.uploadIpRangeData =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/UploadIpRangeData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadIpRangeData,
      callback);
};


/**
 * @param {!proto.api.IpRangeUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.IpRangeUploadResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.uploadIpRangeData =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/UploadIpRangeData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadIpRangeData);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.StorageSummaryReportRequest,
 *   !proto.api.StorageSummaryReportResponse>}
 */
const methodDescriptor_Dora_GetStorageSummaryReportStreamed = new grpc.web.MethodDescriptor(
  '/api.Dora/GetStorageSummaryReportStreamed',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.api.StorageSummaryReportRequest,
  proto.api.StorageSummaryReportResponse,
  /**
   * @param {!proto.api.StorageSummaryReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.StorageSummaryReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.StorageSummaryReportRequest,
 *   !proto.api.StorageSummaryReportResponse>}
 */
const methodInfo_Dora_GetStorageSummaryReportStreamed = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.StorageSummaryReportResponse,
  /**
   * @param {!proto.api.StorageSummaryReportRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.StorageSummaryReportResponse.deserializeBinary
);


/**
 * @param {!proto.api.StorageSummaryReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.StorageSummaryReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getStorageSummaryReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetStorageSummaryReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetStorageSummaryReportStreamed);
};


/**
 * @param {!proto.api.StorageSummaryReportRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.api.StorageSummaryReportResponse>}
 *     The XHR Node Readable Stream
 */
proto.api.DoraPromiseClient.prototype.getStorageSummaryReportStreamed =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/api.Dora/GetStorageSummaryReportStreamed',
      request,
      metadata || {},
      methodDescriptor_Dora_GetStorageSummaryReportStreamed);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.StorageSummaryUploadRequest,
 *   !proto.api.StorageSummaryUploadResponse>}
 */
const methodDescriptor_Dora_UploadStorageSummaryData = new grpc.web.MethodDescriptor(
  '/api.Dora/UploadStorageSummaryData',
  grpc.web.MethodType.UNARY,
  proto.api.StorageSummaryUploadRequest,
  proto.api.StorageSummaryUploadResponse,
  /**
   * @param {!proto.api.StorageSummaryUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.StorageSummaryUploadResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.StorageSummaryUploadRequest,
 *   !proto.api.StorageSummaryUploadResponse>}
 */
const methodInfo_Dora_UploadStorageSummaryData = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.StorageSummaryUploadResponse,
  /**
   * @param {!proto.api.StorageSummaryUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.StorageSummaryUploadResponse.deserializeBinary
);


/**
 * @param {!proto.api.StorageSummaryUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.StorageSummaryUploadResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.StorageSummaryUploadResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.uploadStorageSummaryData =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/UploadStorageSummaryData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadStorageSummaryData,
      callback);
};


/**
 * @param {!proto.api.StorageSummaryUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.StorageSummaryUploadResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.uploadStorageSummaryData =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/UploadStorageSummaryData',
      request,
      metadata || {},
      methodDescriptor_Dora_UploadStorageSummaryData);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.LoginRequest,
 *   !proto.api.LoginResponse>}
 */
const methodDescriptor_Dora_GetLoginToken = new grpc.web.MethodDescriptor(
  '/api.Dora/GetLoginToken',
  grpc.web.MethodType.UNARY,
  proto.api.LoginRequest,
  proto.api.LoginResponse,
  /**
   * @param {!proto.api.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.LoginResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.LoginRequest,
 *   !proto.api.LoginResponse>}
 */
const methodInfo_Dora_GetLoginToken = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.LoginResponse,
  /**
   * @param {!proto.api.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.LoginResponse.deserializeBinary
);


/**
 * @param {!proto.api.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.LoginResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.LoginResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getLoginToken =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetLoginToken',
      request,
      metadata || {},
      methodDescriptor_Dora_GetLoginToken,
      callback);
};


/**
 * @param {!proto.api.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.LoginResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getLoginToken =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetLoginToken',
      request,
      metadata || {},
      methodDescriptor_Dora_GetLoginToken);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.ReadyResponse>}
 */
const methodDescriptor_Dora_Ready = new grpc.web.MethodDescriptor(
  '/api.Dora/Ready',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.api.ReadyResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.ReadyResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.ReadyResponse>}
 */
const methodInfo_Dora_Ready = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.ReadyResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.ReadyResponse.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.ReadyResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.ReadyResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.ready =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/Ready',
      request,
      metadata || {},
      methodDescriptor_Dora_Ready,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.ReadyResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.ready =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/Ready',
      request,
      metadata || {},
      methodDescriptor_Dora_Ready);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.EchoStruct,
 *   !proto.api.EchoStruct>}
 */
const methodDescriptor_Dora_Echo = new grpc.web.MethodDescriptor(
  '/api.Dora/Echo',
  grpc.web.MethodType.UNARY,
  proto.api.EchoStruct,
  proto.api.EchoStruct,
  /**
   * @param {!proto.api.EchoStruct} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EchoStruct.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.EchoStruct,
 *   !proto.api.EchoStruct>}
 */
const methodInfo_Dora_Echo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.EchoStruct,
  /**
   * @param {!proto.api.EchoStruct} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EchoStruct.deserializeBinary
);


/**
 * @param {!proto.api.EchoStruct} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.EchoStruct)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.EchoStruct>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.echo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/Echo',
      request,
      metadata || {},
      methodDescriptor_Dora_Echo,
      callback);
};


/**
 * @param {!proto.api.EchoStruct} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.EchoStruct>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.echo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/Echo',
      request,
      metadata || {},
      methodDescriptor_Dora_Echo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentNameResponseArray>}
 */
const methodDescriptor_Dora_GetVsphereEnvironments = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVsphereEnvironments',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.api.EnvironmentNameResponseArray,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentNameResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentNameResponseArray>}
 */
const methodInfo_Dora_GetVsphereEnvironments = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.EnvironmentNameResponseArray,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentNameResponseArray.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.EnvironmentNameResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.EnvironmentNameResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVsphereEnvironments =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetVsphereEnvironments',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVsphereEnvironments,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.EnvironmentNameResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getVsphereEnvironments =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetVsphereEnvironments',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVsphereEnvironments);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentNameResponseArray>}
 */
const methodDescriptor_Dora_GetForemanEnvironments = new grpc.web.MethodDescriptor(
  '/api.Dora/GetForemanEnvironments',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.api.EnvironmentNameResponseArray,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentNameResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentNameResponseArray>}
 */
const methodInfo_Dora_GetForemanEnvironments = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.EnvironmentNameResponseArray,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentNameResponseArray.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.EnvironmentNameResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.EnvironmentNameResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getForemanEnvironments =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetForemanEnvironments',
      request,
      metadata || {},
      methodDescriptor_Dora_GetForemanEnvironments,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.EnvironmentNameResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getForemanEnvironments =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetForemanEnvironments',
      request,
      metadata || {},
      methodDescriptor_Dora_GetForemanEnvironments);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.ResyncVCenterRequest,
 *   !proto.api.SyncHistoryRec>}
 */
const methodDescriptor_Dora_ResyncVCenter = new grpc.web.MethodDescriptor(
  '/api.Dora/ResyncVCenter',
  grpc.web.MethodType.UNARY,
  proto.api.ResyncVCenterRequest,
  proto.api.SyncHistoryRec,
  /**
   * @param {!proto.api.ResyncVCenterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncHistoryRec.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.ResyncVCenterRequest,
 *   !proto.api.SyncHistoryRec>}
 */
const methodInfo_Dora_ResyncVCenter = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.SyncHistoryRec,
  /**
   * @param {!proto.api.ResyncVCenterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncHistoryRec.deserializeBinary
);


/**
 * @param {!proto.api.ResyncVCenterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.SyncHistoryRec)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.SyncHistoryRec>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.resyncVCenter =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/ResyncVCenter',
      request,
      metadata || {},
      methodDescriptor_Dora_ResyncVCenter,
      callback);
};


/**
 * @param {!proto.api.ResyncVCenterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.SyncHistoryRec>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.resyncVCenter =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/ResyncVCenter',
      request,
      metadata || {},
      methodDescriptor_Dora_ResyncVCenter);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.ResyncNfsRequest,
 *   !proto.api.NfsSyncHistoryRec>}
 */
const methodDescriptor_Dora_ResyncNfs = new grpc.web.MethodDescriptor(
  '/api.Dora/ResyncNfs',
  grpc.web.MethodType.UNARY,
  proto.api.ResyncNfsRequest,
  proto.api.NfsSyncHistoryRec,
  /**
   * @param {!proto.api.ResyncNfsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncHistoryRec.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.ResyncNfsRequest,
 *   !proto.api.NfsSyncHistoryRec>}
 */
const methodInfo_Dora_ResyncNfs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.NfsSyncHistoryRec,
  /**
   * @param {!proto.api.ResyncNfsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncHistoryRec.deserializeBinary
);


/**
 * @param {!proto.api.ResyncNfsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.NfsSyncHistoryRec)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.NfsSyncHistoryRec>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.resyncNfs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/ResyncNfs',
      request,
      metadata || {},
      methodDescriptor_Dora_ResyncNfs,
      callback);
};


/**
 * @param {!proto.api.ResyncNfsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.NfsSyncHistoryRec>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.resyncNfs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/ResyncNfs',
      request,
      metadata || {},
      methodDescriptor_Dora_ResyncNfs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.SyncHistoryRequest,
 *   !proto.api.SyncHistoryRecArray>}
 */
const methodDescriptor_Dora_GetVCenterSyncHistory = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVCenterSyncHistory',
  grpc.web.MethodType.UNARY,
  proto.api.SyncHistoryRequest,
  proto.api.SyncHistoryRecArray,
  /**
   * @param {!proto.api.SyncHistoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncHistoryRecArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.SyncHistoryRequest,
 *   !proto.api.SyncHistoryRecArray>}
 */
const methodInfo_Dora_GetVCenterSyncHistory = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.SyncHistoryRecArray,
  /**
   * @param {!proto.api.SyncHistoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncHistoryRecArray.deserializeBinary
);


/**
 * @param {!proto.api.SyncHistoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.SyncHistoryRecArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.SyncHistoryRecArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVCenterSyncHistory =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetVCenterSyncHistory',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVCenterSyncHistory,
      callback);
};


/**
 * @param {!proto.api.SyncHistoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.SyncHistoryRecArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getVCenterSyncHistory =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetVCenterSyncHistory',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVCenterSyncHistory);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.SyncLogRecRequest,
 *   !proto.api.SyncLogRecResponseArray>}
 */
const methodDescriptor_Dora_GetVCenterSyncLogs = new grpc.web.MethodDescriptor(
  '/api.Dora/GetVCenterSyncLogs',
  grpc.web.MethodType.UNARY,
  proto.api.SyncLogRecRequest,
  proto.api.SyncLogRecResponseArray,
  /**
   * @param {!proto.api.SyncLogRecRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncLogRecResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.SyncLogRecRequest,
 *   !proto.api.SyncLogRecResponseArray>}
 */
const methodInfo_Dora_GetVCenterSyncLogs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.SyncLogRecResponseArray,
  /**
   * @param {!proto.api.SyncLogRecRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.SyncLogRecResponseArray.deserializeBinary
);


/**
 * @param {!proto.api.SyncLogRecRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.SyncLogRecResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.SyncLogRecResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getVCenterSyncLogs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetVCenterSyncLogs',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVCenterSyncLogs,
      callback);
};


/**
 * @param {!proto.api.SyncLogRecRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.SyncLogRecResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getVCenterSyncLogs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetVCenterSyncLogs',
      request,
      metadata || {},
      methodDescriptor_Dora_GetVCenterSyncLogs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.CronVCenterRequest,
 *   !proto.api.CronVCenterResponse>}
 */
const methodDescriptor_Dora_GetCronVCenterInfo = new grpc.web.MethodDescriptor(
  '/api.Dora/GetCronVCenterInfo',
  grpc.web.MethodType.UNARY,
  proto.api.CronVCenterRequest,
  proto.api.CronVCenterResponse,
  /**
   * @param {!proto.api.CronVCenterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronVCenterResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.CronVCenterRequest,
 *   !proto.api.CronVCenterResponse>}
 */
const methodInfo_Dora_GetCronVCenterInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.CronVCenterResponse,
  /**
   * @param {!proto.api.CronVCenterRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronVCenterResponse.deserializeBinary
);


/**
 * @param {!proto.api.CronVCenterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.CronVCenterResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.CronVCenterResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getCronVCenterInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetCronVCenterInfo',
      request,
      metadata || {},
      methodDescriptor_Dora_GetCronVCenterInfo,
      callback);
};


/**
 * @param {!proto.api.CronVCenterRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.CronVCenterResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getCronVCenterInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetCronVCenterInfo',
      request,
      metadata || {},
      methodDescriptor_Dora_GetCronVCenterInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.CronEnabledRequest,
 *   !proto.api.CronVCenterResponse>}
 */
const methodDescriptor_Dora_SetCronVCenterEnabled = new grpc.web.MethodDescriptor(
  '/api.Dora/SetCronVCenterEnabled',
  grpc.web.MethodType.UNARY,
  proto.api.CronEnabledRequest,
  proto.api.CronVCenterResponse,
  /**
   * @param {!proto.api.CronEnabledRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronVCenterResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.CronEnabledRequest,
 *   !proto.api.CronVCenterResponse>}
 */
const methodInfo_Dora_SetCronVCenterEnabled = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.CronVCenterResponse,
  /**
   * @param {!proto.api.CronEnabledRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronVCenterResponse.deserializeBinary
);


/**
 * @param {!proto.api.CronEnabledRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.CronVCenterResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.CronVCenterResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.setCronVCenterEnabled =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/SetCronVCenterEnabled',
      request,
      metadata || {},
      methodDescriptor_Dora_SetCronVCenterEnabled,
      callback);
};


/**
 * @param {!proto.api.CronEnabledRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.CronVCenterResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.setCronVCenterEnabled =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/SetCronVCenterEnabled',
      request,
      metadata || {},
      methodDescriptor_Dora_SetCronVCenterEnabled);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.CronEnabledRequest,
 *   !proto.api.CronNfsResponse>}
 */
const methodDescriptor_Dora_SetCronNfsEnabled = new grpc.web.MethodDescriptor(
  '/api.Dora/SetCronNfsEnabled',
  grpc.web.MethodType.UNARY,
  proto.api.CronEnabledRequest,
  proto.api.CronNfsResponse,
  /**
   * @param {!proto.api.CronEnabledRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronNfsResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.CronEnabledRequest,
 *   !proto.api.CronNfsResponse>}
 */
const methodInfo_Dora_SetCronNfsEnabled = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.CronNfsResponse,
  /**
   * @param {!proto.api.CronEnabledRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronNfsResponse.deserializeBinary
);


/**
 * @param {!proto.api.CronEnabledRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.CronNfsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.CronNfsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.setCronNfsEnabled =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/SetCronNfsEnabled',
      request,
      metadata || {},
      methodDescriptor_Dora_SetCronNfsEnabled,
      callback);
};


/**
 * @param {!proto.api.CronEnabledRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.CronNfsResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.setCronNfsEnabled =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/SetCronNfsEnabled',
      request,
      metadata || {},
      methodDescriptor_Dora_SetCronNfsEnabled);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.CronNfsRequest,
 *   !proto.api.CronNfsResponse>}
 */
const methodDescriptor_Dora_GetCronNfsInfo = new grpc.web.MethodDescriptor(
  '/api.Dora/GetCronNfsInfo',
  grpc.web.MethodType.UNARY,
  proto.api.CronNfsRequest,
  proto.api.CronNfsResponse,
  /**
   * @param {!proto.api.CronNfsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronNfsResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.CronNfsRequest,
 *   !proto.api.CronNfsResponse>}
 */
const methodInfo_Dora_GetCronNfsInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.CronNfsResponse,
  /**
   * @param {!proto.api.CronNfsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.CronNfsResponse.deserializeBinary
);


/**
 * @param {!proto.api.CronNfsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.CronNfsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.CronNfsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getCronNfsInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetCronNfsInfo',
      request,
      metadata || {},
      methodDescriptor_Dora_GetCronNfsInfo,
      callback);
};


/**
 * @param {!proto.api.CronNfsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.CronNfsResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getCronNfsInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetCronNfsInfo',
      request,
      metadata || {},
      methodDescriptor_Dora_GetCronNfsInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.SyncHistoryRequest,
 *   !proto.api.NfsSyncHistoryRecArray>}
 */
const methodDescriptor_Dora_GetNfsSyncHistory = new grpc.web.MethodDescriptor(
  '/api.Dora/GetNfsSyncHistory',
  grpc.web.MethodType.UNARY,
  proto.api.SyncHistoryRequest,
  proto.api.NfsSyncHistoryRecArray,
  /**
   * @param {!proto.api.SyncHistoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncHistoryRecArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.SyncHistoryRequest,
 *   !proto.api.NfsSyncHistoryRecArray>}
 */
const methodInfo_Dora_GetNfsSyncHistory = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.NfsSyncHistoryRecArray,
  /**
   * @param {!proto.api.SyncHistoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncHistoryRecArray.deserializeBinary
);


/**
 * @param {!proto.api.SyncHistoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.NfsSyncHistoryRecArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.NfsSyncHistoryRecArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getNfsSyncHistory =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetNfsSyncHistory',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsSyncHistory,
      callback);
};


/**
 * @param {!proto.api.SyncHistoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.NfsSyncHistoryRecArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getNfsSyncHistory =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetNfsSyncHistory',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsSyncHistory);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.SyncLogRecRequest,
 *   !proto.api.NfsSyncLogRecResponseArray>}
 */
const methodDescriptor_Dora_GetNfsSyncLogs = new grpc.web.MethodDescriptor(
  '/api.Dora/GetNfsSyncLogs',
  grpc.web.MethodType.UNARY,
  proto.api.SyncLogRecRequest,
  proto.api.NfsSyncLogRecResponseArray,
  /**
   * @param {!proto.api.SyncLogRecRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncLogRecResponseArray.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.SyncLogRecRequest,
 *   !proto.api.NfsSyncLogRecResponseArray>}
 */
const methodInfo_Dora_GetNfsSyncLogs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.NfsSyncLogRecResponseArray,
  /**
   * @param {!proto.api.SyncLogRecRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.NfsSyncLogRecResponseArray.deserializeBinary
);


/**
 * @param {!proto.api.SyncLogRecRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.NfsSyncLogRecResponseArray)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.NfsSyncLogRecResponseArray>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getNfsSyncLogs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetNfsSyncLogs',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsSyncLogs,
      callback);
};


/**
 * @param {!proto.api.SyncLogRecRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.NfsSyncLogRecResponseArray>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getNfsSyncLogs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetNfsSyncLogs',
      request,
      metadata || {},
      methodDescriptor_Dora_GetNfsSyncLogs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HypervisorLoadOutRequest,
 *   !proto.api.HypervisorLoadOutList>}
 */
const methodDescriptor_Dora_GetHypervisorLoadOutList = new grpc.web.MethodDescriptor(
  '/api.Dora/GetHypervisorLoadOutList',
  grpc.web.MethodType.UNARY,
  proto.api.HypervisorLoadOutRequest,
  proto.api.HypervisorLoadOutList,
  /**
   * @param {!proto.api.HypervisorLoadOutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutList.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HypervisorLoadOutRequest,
 *   !proto.api.HypervisorLoadOutList>}
 */
const methodInfo_Dora_GetHypervisorLoadOutList = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorLoadOutList,
  /**
   * @param {!proto.api.HypervisorLoadOutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutList.deserializeBinary
);


/**
 * @param {!proto.api.HypervisorLoadOutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.HypervisorLoadOutList)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorLoadOutList>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getHypervisorLoadOutList =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetHypervisorLoadOutList',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisorLoadOutList,
      callback);
};


/**
 * @param {!proto.api.HypervisorLoadOutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.HypervisorLoadOutList>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getHypervisorLoadOutList =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetHypervisorLoadOutList',
      request,
      metadata || {},
      methodDescriptor_Dora_GetHypervisorLoadOutList);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodDescriptor_Dora_BalanceExec = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceExec',
  grpc.web.MethodType.UNARY,
  proto.api.HypervisorLoadOutDTO,
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodInfo_Dora_BalanceExec = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.HypervisorLoadOutDTO)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorLoadOutDTO>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceExec =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceExec',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceExec,
      callback);
};


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.HypervisorLoadOutDTO>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceExec =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceExec',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceExec);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodDescriptor_Dora_BalanceCheck = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceCheck',
  grpc.web.MethodType.UNARY,
  proto.api.HypervisorLoadOutDTO,
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodInfo_Dora_BalanceCheck = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.HypervisorLoadOutDTO)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorLoadOutDTO>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceCheck =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceCheck',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceCheck,
      callback);
};


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.HypervisorLoadOutDTO>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceCheck =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceCheck',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceCheck);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodDescriptor_Dora_BalanceForceDistribute = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceForceDistribute',
  grpc.web.MethodType.UNARY,
  proto.api.HypervisorLoadOutDTO,
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.HypervisorLoadOutDTO,
 *   !proto.api.HypervisorLoadOutDTO>}
 */
const methodInfo_Dora_BalanceForceDistribute = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.HypervisorLoadOutDTO,
  /**
   * @param {!proto.api.HypervisorLoadOutDTO} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.HypervisorLoadOutDTO.deserializeBinary
);


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.HypervisorLoadOutDTO)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.HypervisorLoadOutDTO>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceForceDistribute =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceForceDistribute',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceForceDistribute,
      callback);
};


/**
 * @param {!proto.api.HypervisorLoadOutDTO} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.HypervisorLoadOutDTO>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceForceDistribute =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceForceDistribute',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceForceDistribute);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VmMoveRequest,
 *   !proto.api.VmMoveResponse>}
 */
const methodDescriptor_Dora_VmMove = new grpc.web.MethodDescriptor(
  '/api.Dora/VmMove',
  grpc.web.MethodType.UNARY,
  proto.api.VmMoveRequest,
  proto.api.VmMoveResponse,
  /**
   * @param {!proto.api.VmMoveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmMoveResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VmMoveRequest,
 *   !proto.api.VmMoveResponse>}
 */
const methodInfo_Dora_VmMove = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VmMoveResponse,
  /**
   * @param {!proto.api.VmMoveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmMoveResponse.deserializeBinary
);


/**
 * @param {!proto.api.VmMoveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.VmMoveResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmMoveResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.vmMove =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/VmMove',
      request,
      metadata || {},
      methodDescriptor_Dora_VmMove,
      callback);
};


/**
 * @param {!proto.api.VmMoveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.VmMoveResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.vmMove =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/VmMove',
      request,
      metadata || {},
      methodDescriptor_Dora_VmMove);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VmPowerChangeRequest,
 *   !proto.api.VmPowerChangeResponse>}
 */
const methodDescriptor_Dora_VmPowerOff = new grpc.web.MethodDescriptor(
  '/api.Dora/VmPowerOff',
  grpc.web.MethodType.UNARY,
  proto.api.VmPowerChangeRequest,
  proto.api.VmPowerChangeResponse,
  /**
   * @param {!proto.api.VmPowerChangeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmPowerChangeResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VmPowerChangeRequest,
 *   !proto.api.VmPowerChangeResponse>}
 */
const methodInfo_Dora_VmPowerOff = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VmPowerChangeResponse,
  /**
   * @param {!proto.api.VmPowerChangeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmPowerChangeResponse.deserializeBinary
);


/**
 * @param {!proto.api.VmPowerChangeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.VmPowerChangeResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmPowerChangeResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.vmPowerOff =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/VmPowerOff',
      request,
      metadata || {},
      methodDescriptor_Dora_VmPowerOff,
      callback);
};


/**
 * @param {!proto.api.VmPowerChangeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.VmPowerChangeResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.vmPowerOff =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/VmPowerOff',
      request,
      metadata || {},
      methodDescriptor_Dora_VmPowerOff);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.VmPowerChangeRequest,
 *   !proto.api.VmPowerChangeResponse>}
 */
const methodDescriptor_Dora_VmPowerOn = new grpc.web.MethodDescriptor(
  '/api.Dora/VmPowerOn',
  grpc.web.MethodType.UNARY,
  proto.api.VmPowerChangeRequest,
  proto.api.VmPowerChangeResponse,
  /**
   * @param {!proto.api.VmPowerChangeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmPowerChangeResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.VmPowerChangeRequest,
 *   !proto.api.VmPowerChangeResponse>}
 */
const methodInfo_Dora_VmPowerOn = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.VmPowerChangeResponse,
  /**
   * @param {!proto.api.VmPowerChangeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.VmPowerChangeResponse.deserializeBinary
);


/**
 * @param {!proto.api.VmPowerChangeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.VmPowerChangeResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.VmPowerChangeResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.vmPowerOn =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/VmPowerOn',
      request,
      metadata || {},
      methodDescriptor_Dora_VmPowerOn,
      callback);
};


/**
 * @param {!proto.api.VmPowerChangeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.VmPowerChangeResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.vmPowerOn =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/VmPowerOn',
      request,
      metadata || {},
      methodDescriptor_Dora_VmPowerOn);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.BalanceInvalidCheckRequest,
 *   !proto.api.BalanceInvalidCheckResponse>}
 */
const methodDescriptor_Dora_BalanceInvalidCheck = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceInvalidCheck',
  grpc.web.MethodType.UNARY,
  proto.api.BalanceInvalidCheckRequest,
  proto.api.BalanceInvalidCheckResponse,
  /**
   * @param {!proto.api.BalanceInvalidCheckRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceInvalidCheckResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.BalanceInvalidCheckRequest,
 *   !proto.api.BalanceInvalidCheckResponse>}
 */
const methodInfo_Dora_BalanceInvalidCheck = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.BalanceInvalidCheckResponse,
  /**
   * @param {!proto.api.BalanceInvalidCheckRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceInvalidCheckResponse.deserializeBinary
);


/**
 * @param {!proto.api.BalanceInvalidCheckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.BalanceInvalidCheckResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.BalanceInvalidCheckResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceInvalidCheck =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceInvalidCheck',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceInvalidCheck,
      callback);
};


/**
 * @param {!proto.api.BalanceInvalidCheckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.BalanceInvalidCheckResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceInvalidCheck =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceInvalidCheck',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceInvalidCheck);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.BalanceLockRequest,
 *   !proto.api.BalanceLockResponse>}
 */
const methodDescriptor_Dora_BalanceLock = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceLock',
  grpc.web.MethodType.UNARY,
  proto.api.BalanceLockRequest,
  proto.api.BalanceLockResponse,
  /**
   * @param {!proto.api.BalanceLockRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceLockResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.BalanceLockRequest,
 *   !proto.api.BalanceLockResponse>}
 */
const methodInfo_Dora_BalanceLock = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.BalanceLockResponse,
  /**
   * @param {!proto.api.BalanceLockRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceLockResponse.deserializeBinary
);


/**
 * @param {!proto.api.BalanceLockRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.BalanceLockResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.BalanceLockResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceLock =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceLock',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceLock,
      callback);
};


/**
 * @param {!proto.api.BalanceLockRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.BalanceLockResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceLock =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceLock',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceLock);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.api.BalanceUnlockRequest,
 *   !proto.api.BalanceUnlockResponse>}
 */
const methodDescriptor_Dora_BalanceUnlock = new grpc.web.MethodDescriptor(
  '/api.Dora/BalanceUnlock',
  grpc.web.MethodType.UNARY,
  proto.api.BalanceUnlockRequest,
  proto.api.BalanceUnlockResponse,
  /**
   * @param {!proto.api.BalanceUnlockRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceUnlockResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.api.BalanceUnlockRequest,
 *   !proto.api.BalanceUnlockResponse>}
 */
const methodInfo_Dora_BalanceUnlock = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.BalanceUnlockResponse,
  /**
   * @param {!proto.api.BalanceUnlockRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.BalanceUnlockResponse.deserializeBinary
);


/**
 * @param {!proto.api.BalanceUnlockRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.BalanceUnlockResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.BalanceUnlockResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.balanceUnlock =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/BalanceUnlock',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceUnlock,
      callback);
};


/**
 * @param {!proto.api.BalanceUnlockRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.BalanceUnlockResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.balanceUnlock =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/BalanceUnlock',
      request,
      metadata || {},
      methodDescriptor_Dora_BalanceUnlock);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentLabelResponse>}
 */
const methodDescriptor_Dora_GetAppEnvironmentLabel = new grpc.web.MethodDescriptor(
  '/api.Dora/GetAppEnvironmentLabel',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.api.EnvironmentLabelResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentLabelResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.EnvironmentLabelResponse>}
 */
const methodInfo_Dora_GetAppEnvironmentLabel = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.EnvironmentLabelResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.EnvironmentLabelResponse.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.EnvironmentLabelResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.EnvironmentLabelResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getAppEnvironmentLabel =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetAppEnvironmentLabel',
      request,
      metadata || {},
      methodDescriptor_Dora_GetAppEnvironmentLabel,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.EnvironmentLabelResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getAppEnvironmentLabel =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetAppEnvironmentLabel',
      request,
      metadata || {},
      methodDescriptor_Dora_GetAppEnvironmentLabel);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.MetricsReportResponse>}
 */
const methodDescriptor_Dora_GetMetricsReport = new grpc.web.MethodDescriptor(
  '/api.Dora/GetMetricsReport',
  grpc.web.MethodType.UNARY,
  google_protobuf_empty_pb.Empty,
  proto.api.MetricsReportResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.MetricsReportResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.api.MetricsReportResponse>}
 */
const methodInfo_Dora_GetMetricsReport = new grpc.web.AbstractClientBase.MethodInfo(
  proto.api.MetricsReportResponse,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.api.MetricsReportResponse.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.api.MetricsReportResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.api.MetricsReportResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.api.DoraClient.prototype.getMetricsReport =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/api.Dora/GetMetricsReport',
      request,
      metadata || {},
      methodDescriptor_Dora_GetMetricsReport,
      callback);
};


/**
 * @param {!proto.google.protobuf.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.api.MetricsReportResponse>}
 *     A native promise that resolves to the response
 */
proto.api.DoraPromiseClient.prototype.getMetricsReport =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/api.Dora/GetMetricsReport',
      request,
      metadata || {},
      methodDescriptor_Dora_GetMetricsReport);
};


module.exports = proto.api;

