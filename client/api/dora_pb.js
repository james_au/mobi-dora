// source: api/dora.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_api_annotations_pb = require('./google/api/annotations_pb.js');
goog.object.extend(proto, google_api_annotations_pb);
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');
goog.object.extend(proto, google_protobuf_empty_pb);
goog.exportSymbol('proto.api.BalanceInvalidCheckRequest', null, global);
goog.exportSymbol('proto.api.BalanceInvalidCheckResponse', null, global);
goog.exportSymbol('proto.api.BalanceLockRequest', null, global);
goog.exportSymbol('proto.api.BalanceLockResponse', null, global);
goog.exportSymbol('proto.api.BalanceRuleViolation', null, global);
goog.exportSymbol('proto.api.BalanceUnlockRequest', null, global);
goog.exportSymbol('proto.api.BalanceUnlockResponse', null, global);
goog.exportSymbol('proto.api.BalanceWarnErrorResponse', null, global);
goog.exportSymbol('proto.api.CronEnabledRequest', null, global);
goog.exportSymbol('proto.api.CronNfsRequest', null, global);
goog.exportSymbol('proto.api.CronNfsResponse', null, global);
goog.exportSymbol('proto.api.CronVCenterRequest', null, global);
goog.exportSymbol('proto.api.CronVCenterResponse', null, global);
goog.exportSymbol('proto.api.Datastore', null, global);
goog.exportSymbol('proto.api.DatastoreHost', null, global);
goog.exportSymbol('proto.api.DatastoreReportRequest', null, global);
goog.exportSymbol('proto.api.DatastoreReportResponse', null, global);
goog.exportSymbol('proto.api.DatastoreResponse', null, global);
goog.exportSymbol('proto.api.DatastoreResponseArray', null, global);
goog.exportSymbol('proto.api.EchoStruct', null, global);
goog.exportSymbol('proto.api.EnvironmentLabelResponse', null, global);
goog.exportSymbol('proto.api.EnvironmentNameResponse', null, global);
goog.exportSymbol('proto.api.EnvironmentNameResponseArray', null, global);
goog.exportSymbol('proto.api.HVQueryRequest', null, global);
goog.exportSymbol('proto.api.HypervisorLoadOut', null, global);
goog.exportSymbol('proto.api.HypervisorLoadOutDTO', null, global);
goog.exportSymbol('proto.api.HypervisorLoadOutList', null, global);
goog.exportSymbol('proto.api.HypervisorLoadOutRequest', null, global);
goog.exportSymbol('proto.api.HypervisorResponse', null, global);
goog.exportSymbol('proto.api.HypervisorResponseArray', null, global);
goog.exportSymbol('proto.api.IpRangeRec', null, global);
goog.exportSymbol('proto.api.IpRangeReportRequest', null, global);
goog.exportSymbol('proto.api.IpRangeReportResponse', null, global);
goog.exportSymbol('proto.api.IpRangeUploadRequest', null, global);
goog.exportSymbol('proto.api.IpRangeUploadResponse', null, global);
goog.exportSymbol('proto.api.KeepAliveRequest', null, global);
goog.exportSymbol('proto.api.KeepAliveResponse', null, global);
goog.exportSymbol('proto.api.LoginRequest', null, global);
goog.exportSymbol('proto.api.LoginResponse', null, global);
goog.exportSymbol('proto.api.LogoutRequest', null, global);
goog.exportSymbol('proto.api.LogoutResponse', null, global);
goog.exportSymbol('proto.api.MetricUsageResponse', null, global);
goog.exportSymbol('proto.api.MetricsReportResponse', null, global);
goog.exportSymbol('proto.api.NfsReportRequest', null, global);
goog.exportSymbol('proto.api.NfsReportResponse', null, global);
goog.exportSymbol('proto.api.NfsSyncHistoryRec', null, global);
goog.exportSymbol('proto.api.NfsSyncHistoryRecArray', null, global);
goog.exportSymbol('proto.api.NfsSyncLogRecResponse', null, global);
goog.exportSymbol('proto.api.NfsSyncLogRecResponseArray', null, global);
goog.exportSymbol('proto.api.QueryRequest', null, global);
goog.exportSymbol('proto.api.RackMapping', null, global);
goog.exportSymbol('proto.api.ReadyResponse', null, global);
goog.exportSymbol('proto.api.ResyncNfsRequest', null, global);
goog.exportSymbol('proto.api.ResyncVCenterRequest', null, global);
goog.exportSymbol('proto.api.RoomCpu', null, global);
goog.exportSymbol('proto.api.RoomMemGB', null, global);
goog.exportSymbol('proto.api.RoomResponse', null, global);
goog.exportSymbol('proto.api.RoomResponseArray', null, global);
goog.exportSymbol('proto.api.SharedStorageReportRequest', null, global);
goog.exportSymbol('proto.api.SharedStorageReportResponse', null, global);
goog.exportSymbol('proto.api.StorageSummaryRec', null, global);
goog.exportSymbol('proto.api.StorageSummaryReportRequest', null, global);
goog.exportSymbol('proto.api.StorageSummaryReportResponse', null, global);
goog.exportSymbol('proto.api.StorageSummaryUploadRequest', null, global);
goog.exportSymbol('proto.api.StorageSummaryUploadResponse', null, global);
goog.exportSymbol('proto.api.SyncHistoryRec', null, global);
goog.exportSymbol('proto.api.SyncHistoryRecArray', null, global);
goog.exportSymbol('proto.api.SyncHistoryRequest', null, global);
goog.exportSymbol('proto.api.SyncLogRecRequest', null, global);
goog.exportSymbol('proto.api.SyncLogRecResponse', null, global);
goog.exportSymbol('proto.api.SyncLogRecResponseArray', null, global);
goog.exportSymbol('proto.api.VMQueryRequest', null, global);
goog.exportSymbol('proto.api.VirtualMachineLoad', null, global);
goog.exportSymbol('proto.api.VirtualMachineNetwork', null, global);
goog.exportSymbol('proto.api.VirtualMachineResponse', null, global);
goog.exportSymbol('proto.api.VirtualMachineResponseArray', null, global);
goog.exportSymbol('proto.api.VmMoveRequest', null, global);
goog.exportSymbol('proto.api.VmMoveResponse', null, global);
goog.exportSymbol('proto.api.VmPowerChangeRequest', null, global);
goog.exportSymbol('proto.api.VmPowerChangeResponse', null, global);
goog.exportSymbol('proto.api.VmStorageReportRequest', null, global);
goog.exportSymbol('proto.api.VmStorageReportResponse', null, global);
goog.exportSymbol('proto.api.VmStorageUploadRec', null, global);
goog.exportSymbol('proto.api.VmStorageUploadRequest', null, global);
goog.exportSymbol('proto.api.VmStorageUploadResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.KeepAliveRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.KeepAliveRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.KeepAliveRequest.displayName = 'proto.api.KeepAliveRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.KeepAliveResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.KeepAliveResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.KeepAliveResponse.displayName = 'proto.api.KeepAliveResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.LogoutRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.LogoutRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.LogoutRequest.displayName = 'proto.api.LogoutRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.LogoutResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.LogoutResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.LogoutResponse.displayName = 'proto.api.LogoutResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.EnvironmentLabelResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.EnvironmentLabelResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.EnvironmentLabelResponse.displayName = 'proto.api.EnvironmentLabelResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CronEnabledRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CronEnabledRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.CronEnabledRequest.displayName = 'proto.api.CronEnabledRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CronVCenterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CronVCenterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.CronVCenterRequest.displayName = 'proto.api.CronVCenterRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CronVCenterResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CronVCenterResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.CronVCenterResponse.displayName = 'proto.api.CronVCenterResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CronNfsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CronNfsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.CronNfsRequest.displayName = 'proto.api.CronNfsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.CronNfsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.CronNfsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.CronNfsResponse.displayName = 'proto.api.CronNfsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.ResyncVCenterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.ResyncVCenterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.ResyncVCenterRequest.displayName = 'proto.api.ResyncVCenterRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.ResyncNfsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.ResyncNfsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.ResyncNfsRequest.displayName = 'proto.api.ResyncNfsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncHistoryRec = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SyncHistoryRec, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncHistoryRec.displayName = 'proto.api.SyncHistoryRec';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncHistoryRecArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.SyncHistoryRecArray.repeatedFields_, null);
};
goog.inherits(proto.api.SyncHistoryRecArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncHistoryRecArray.displayName = 'proto.api.SyncHistoryRecArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsSyncHistoryRec = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NfsSyncHistoryRec, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsSyncHistoryRec.displayName = 'proto.api.NfsSyncHistoryRec';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsSyncHistoryRecArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.NfsSyncHistoryRecArray.repeatedFields_, null);
};
goog.inherits(proto.api.NfsSyncHistoryRecArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsSyncHistoryRecArray.displayName = 'proto.api.NfsSyncHistoryRecArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncHistoryRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SyncHistoryRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncHistoryRequest.displayName = 'proto.api.SyncHistoryRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncLogRecRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SyncLogRecRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncLogRecRequest.displayName = 'proto.api.SyncLogRecRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncLogRecResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SyncLogRecResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncLogRecResponse.displayName = 'proto.api.SyncLogRecResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SyncLogRecResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.SyncLogRecResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.SyncLogRecResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SyncLogRecResponseArray.displayName = 'proto.api.SyncLogRecResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsSyncLogRecResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NfsSyncLogRecResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsSyncLogRecResponse.displayName = 'proto.api.NfsSyncLogRecResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsSyncLogRecResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.NfsSyncLogRecResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.NfsSyncLogRecResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsSyncLogRecResponseArray.displayName = 'proto.api.NfsSyncLogRecResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.HypervisorResponse.repeatedFields_, null);
};
goog.inherits(proto.api.HypervisorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorResponse.displayName = 'proto.api.HypervisorResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.HypervisorResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.HypervisorResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorResponseArray.displayName = 'proto.api.HypervisorResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VirtualMachineResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VirtualMachineResponse.repeatedFields_, null);
};
goog.inherits(proto.api.VirtualMachineResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VirtualMachineResponse.displayName = 'proto.api.VirtualMachineResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VirtualMachineNetwork = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VirtualMachineNetwork.repeatedFields_, null);
};
goog.inherits(proto.api.VirtualMachineNetwork, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VirtualMachineNetwork.displayName = 'proto.api.VirtualMachineNetwork';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VirtualMachineResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VirtualMachineResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.VirtualMachineResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VirtualMachineResponseArray.displayName = 'proto.api.VirtualMachineResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DatastoreResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.DatastoreResponse.repeatedFields_, null);
};
goog.inherits(proto.api.DatastoreResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.DatastoreResponse.displayName = 'proto.api.DatastoreResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DatastoreHost = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DatastoreHost, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.DatastoreHost.displayName = 'proto.api.DatastoreHost';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DatastoreResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.DatastoreResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.DatastoreResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.DatastoreResponseArray.displayName = 'proto.api.DatastoreResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.RoomResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.RoomResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.RoomResponse.displayName = 'proto.api.RoomResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.RoomResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.RoomResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.RoomResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.RoomResponseArray.displayName = 'proto.api.RoomResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.QueryRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.QueryRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.QueryRequest.displayName = 'proto.api.QueryRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VMQueryRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VMQueryRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VMQueryRequest.displayName = 'proto.api.VMQueryRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HVQueryRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.HVQueryRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HVQueryRequest.displayName = 'proto.api.HVQueryRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DatastoreReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DatastoreReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.DatastoreReportRequest.displayName = 'proto.api.DatastoreReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.DatastoreReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.DatastoreReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.DatastoreReportResponse.displayName = 'proto.api.DatastoreReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NfsReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsReportRequest.displayName = 'proto.api.NfsReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.NfsReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.NfsReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.NfsReportResponse.displayName = 'proto.api.NfsReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmStorageReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VmStorageReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmStorageReportRequest.displayName = 'proto.api.VmStorageReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmStorageReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VmStorageReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmStorageReportResponse.displayName = 'proto.api.VmStorageReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmStorageUploadRec = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VmStorageUploadRec, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmStorageUploadRec.displayName = 'proto.api.VmStorageUploadRec';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmStorageUploadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VmStorageUploadRequest.repeatedFields_, null);
};
goog.inherits(proto.api.VmStorageUploadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmStorageUploadRequest.displayName = 'proto.api.VmStorageUploadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmStorageUploadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VmStorageUploadResponse.repeatedFields_, null);
};
goog.inherits(proto.api.VmStorageUploadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmStorageUploadResponse.displayName = 'proto.api.VmStorageUploadResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.IpRangeReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.IpRangeReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.IpRangeReportRequest.displayName = 'proto.api.IpRangeReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.IpRangeReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.IpRangeReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.IpRangeReportResponse.displayName = 'proto.api.IpRangeReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.IpRangeRec = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.IpRangeRec, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.IpRangeRec.displayName = 'proto.api.IpRangeRec';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.IpRangeUploadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.IpRangeUploadRequest.repeatedFields_, null);
};
goog.inherits(proto.api.IpRangeUploadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.IpRangeUploadRequest.displayName = 'proto.api.IpRangeUploadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.IpRangeUploadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.IpRangeUploadResponse.repeatedFields_, null);
};
goog.inherits(proto.api.IpRangeUploadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.IpRangeUploadResponse.displayName = 'proto.api.IpRangeUploadResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StorageSummaryReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StorageSummaryReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.StorageSummaryReportRequest.displayName = 'proto.api.StorageSummaryReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StorageSummaryReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StorageSummaryReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.StorageSummaryReportResponse.displayName = 'proto.api.StorageSummaryReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StorageSummaryRec = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.StorageSummaryRec, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.StorageSummaryRec.displayName = 'proto.api.StorageSummaryRec';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StorageSummaryUploadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.StorageSummaryUploadRequest.repeatedFields_, null);
};
goog.inherits(proto.api.StorageSummaryUploadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.StorageSummaryUploadRequest.displayName = 'proto.api.StorageSummaryUploadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.StorageSummaryUploadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.StorageSummaryUploadResponse.repeatedFields_, null);
};
goog.inherits(proto.api.StorageSummaryUploadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.StorageSummaryUploadResponse.displayName = 'proto.api.StorageSummaryUploadResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SharedStorageReportRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SharedStorageReportRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SharedStorageReportRequest.displayName = 'proto.api.SharedStorageReportRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.SharedStorageReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.SharedStorageReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.SharedStorageReportResponse.displayName = 'proto.api.SharedStorageReportResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.LoginRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.LoginRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.LoginRequest.displayName = 'proto.api.LoginRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.LoginResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.LoginResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.LoginResponse.displayName = 'proto.api.LoginResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.ReadyResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.ReadyResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.ReadyResponse.displayName = 'proto.api.ReadyResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.EchoStruct = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.EchoStruct, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.EchoStruct.displayName = 'proto.api.EchoStruct';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.EnvironmentNameResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.EnvironmentNameResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.EnvironmentNameResponse.displayName = 'proto.api.EnvironmentNameResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.EnvironmentNameResponseArray = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.EnvironmentNameResponseArray.repeatedFields_, null);
};
goog.inherits(proto.api.EnvironmentNameResponseArray, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.EnvironmentNameResponseArray.displayName = 'proto.api.EnvironmentNameResponseArray';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.Datastore = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.Datastore, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.Datastore.displayName = 'proto.api.Datastore';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VirtualMachineLoad = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VirtualMachineLoad.repeatedFields_, null);
};
goog.inherits(proto.api.VirtualMachineLoad, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VirtualMachineLoad.displayName = 'proto.api.VirtualMachineLoad';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.RoomCpu = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.RoomCpu, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.RoomCpu.displayName = 'proto.api.RoomCpu';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.RoomMemGB = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.RoomMemGB, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.RoomMemGB.displayName = 'proto.api.RoomMemGB';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorLoadOut = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.HypervisorLoadOut.repeatedFields_, null);
};
goog.inherits(proto.api.HypervisorLoadOut, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorLoadOut.displayName = 'proto.api.HypervisorLoadOut';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorLoadOutDTO = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.HypervisorLoadOutDTO.repeatedFields_, null);
};
goog.inherits(proto.api.HypervisorLoadOutDTO, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorLoadOutDTO.displayName = 'proto.api.HypervisorLoadOutDTO';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceWarnErrorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.BalanceWarnErrorResponse.repeatedFields_, null);
};
goog.inherits(proto.api.BalanceWarnErrorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceWarnErrorResponse.displayName = 'proto.api.BalanceWarnErrorResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.RackMapping = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.RackMapping, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.RackMapping.displayName = 'proto.api.RackMapping';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceRuleViolation = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceRuleViolation, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceRuleViolation.displayName = 'proto.api.BalanceRuleViolation';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorLoadOutList = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.HypervisorLoadOutList.repeatedFields_, null);
};
goog.inherits(proto.api.HypervisorLoadOutList, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorLoadOutList.displayName = 'proto.api.HypervisorLoadOutList';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.HypervisorLoadOutRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.HypervisorLoadOutRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.HypervisorLoadOutRequest.displayName = 'proto.api.HypervisorLoadOutRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmMoveRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VmMoveRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmMoveRequest.displayName = 'proto.api.VmMoveRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmMoveResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VmMoveResponse.repeatedFields_, null);
};
goog.inherits(proto.api.VmMoveResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmMoveResponse.displayName = 'proto.api.VmMoveResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmPowerChangeRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.VmPowerChangeRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmPowerChangeRequest.displayName = 'proto.api.VmPowerChangeRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.VmPowerChangeResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.api.VmPowerChangeResponse.repeatedFields_, null);
};
goog.inherits(proto.api.VmPowerChangeResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.VmPowerChangeResponse.displayName = 'proto.api.VmPowerChangeResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceInvalidCheckRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceInvalidCheckRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceInvalidCheckRequest.displayName = 'proto.api.BalanceInvalidCheckRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceInvalidCheckResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceInvalidCheckResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceInvalidCheckResponse.displayName = 'proto.api.BalanceInvalidCheckResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceLockRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceLockRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceLockRequest.displayName = 'proto.api.BalanceLockRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceLockResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceLockResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceLockResponse.displayName = 'proto.api.BalanceLockResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceUnlockRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceUnlockRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceUnlockRequest.displayName = 'proto.api.BalanceUnlockRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.BalanceUnlockResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.BalanceUnlockResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.BalanceUnlockResponse.displayName = 'proto.api.BalanceUnlockResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.MetricUsageResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.MetricUsageResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.MetricUsageResponse.displayName = 'proto.api.MetricUsageResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.api.MetricsReportResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.api.MetricsReportResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.api.MetricsReportResponse.displayName = 'proto.api.MetricsReportResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.KeepAliveRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.KeepAliveRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.KeepAliveRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.KeepAliveRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.KeepAliveRequest}
 */
proto.api.KeepAliveRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.KeepAliveRequest;
  return proto.api.KeepAliveRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.KeepAliveRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.KeepAliveRequest}
 */
proto.api.KeepAliveRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.KeepAliveRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.KeepAliveRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.KeepAliveRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.KeepAliveRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.KeepAliveRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.KeepAliveRequest} returns this
 */
proto.api.KeepAliveRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.KeepAliveResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.KeepAliveResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.KeepAliveResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.KeepAliveResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    authTokenReplacement: jspb.Message.getFieldWithDefault(msg, 1, ""),
    doraVersion: jspb.Message.getFieldWithDefault(msg, 2, ""),
    error: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.KeepAliveResponse}
 */
proto.api.KeepAliveResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.KeepAliveResponse;
  return proto.api.KeepAliveResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.KeepAliveResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.KeepAliveResponse}
 */
proto.api.KeepAliveResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthTokenReplacement(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDoraVersion(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setError(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.KeepAliveResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.KeepAliveResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.KeepAliveResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.KeepAliveResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthTokenReplacement();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDoraVersion();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getError();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string auth_token_replacement = 1;
 * @return {string}
 */
proto.api.KeepAliveResponse.prototype.getAuthTokenReplacement = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.KeepAliveResponse} returns this
 */
proto.api.KeepAliveResponse.prototype.setAuthTokenReplacement = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string dora_version = 2;
 * @return {string}
 */
proto.api.KeepAliveResponse.prototype.getDoraVersion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.KeepAliveResponse} returns this
 */
proto.api.KeepAliveResponse.prototype.setDoraVersion = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string error = 3;
 * @return {string}
 */
proto.api.KeepAliveResponse.prototype.getError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.KeepAliveResponse} returns this
 */
proto.api.KeepAliveResponse.prototype.setError = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.LogoutRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.LogoutRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.LogoutRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LogoutRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.LogoutRequest}
 */
proto.api.LogoutRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.LogoutRequest;
  return proto.api.LogoutRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.LogoutRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.LogoutRequest}
 */
proto.api.LogoutRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.LogoutRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.LogoutRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.LogoutRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LogoutRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.LogoutRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.LogoutRequest} returns this
 */
proto.api.LogoutRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.LogoutResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.LogoutResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.LogoutResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LogoutResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    success: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.LogoutResponse}
 */
proto.api.LogoutResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.LogoutResponse;
  return proto.api.LogoutResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.LogoutResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.LogoutResponse}
 */
proto.api.LogoutResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSuccess(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.LogoutResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.LogoutResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.LogoutResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LogoutResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSuccess();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool success = 1;
 * @return {boolean}
 */
proto.api.LogoutResponse.prototype.getSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.LogoutResponse} returns this
 */
proto.api.LogoutResponse.prototype.setSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.EnvironmentLabelResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.EnvironmentLabelResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.EnvironmentLabelResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentLabelResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    environmentLabel: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.EnvironmentLabelResponse}
 */
proto.api.EnvironmentLabelResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.EnvironmentLabelResponse;
  return proto.api.EnvironmentLabelResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.EnvironmentLabelResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.EnvironmentLabelResponse}
 */
proto.api.EnvironmentLabelResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentLabel(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.EnvironmentLabelResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.EnvironmentLabelResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.EnvironmentLabelResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentLabelResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEnvironmentLabel();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string environment_label = 1;
 * @return {string}
 */
proto.api.EnvironmentLabelResponse.prototype.getEnvironmentLabel = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.EnvironmentLabelResponse} returns this
 */
proto.api.EnvironmentLabelResponse.prototype.setEnvironmentLabel = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CronEnabledRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CronEnabledRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CronEnabledRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronEnabledRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    enabled: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CronEnabledRequest}
 */
proto.api.CronEnabledRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CronEnabledRequest;
  return proto.api.CronEnabledRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CronEnabledRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CronEnabledRequest}
 */
proto.api.CronEnabledRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnabled(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CronEnabledRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.CronEnabledRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.CronEnabledRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronEnabledRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getEnabled();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.CronEnabledRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronEnabledRequest} returns this
 */
proto.api.CronEnabledRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.CronEnabledRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronEnabledRequest} returns this
 */
proto.api.CronEnabledRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool enabled = 3;
 * @return {boolean}
 */
proto.api.CronEnabledRequest.prototype.getEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.CronEnabledRequest} returns this
 */
proto.api.CronEnabledRequest.prototype.setEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CronVCenterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CronVCenterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CronVCenterRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronVCenterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    environmentName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CronVCenterRequest}
 */
proto.api.CronVCenterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CronVCenterRequest;
  return proto.api.CronVCenterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CronVCenterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CronVCenterRequest}
 */
proto.api.CronVCenterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CronVCenterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.CronVCenterRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.CronVCenterRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronVCenterRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string environment_name = 1;
 * @return {string}
 */
proto.api.CronVCenterRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronVCenterRequest} returns this
 */
proto.api.CronVCenterRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CronVCenterResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CronVCenterResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CronVCenterResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronVCenterResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    nextRunTimeMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    cronExpression: jspb.Message.getFieldWithDefault(msg, 2, ""),
    enabled: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CronVCenterResponse}
 */
proto.api.CronVCenterResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CronVCenterResponse;
  return proto.api.CronVCenterResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CronVCenterResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CronVCenterResponse}
 */
proto.api.CronVCenterResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNextRunTimeMs(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCronExpression(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnabled(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CronVCenterResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.CronVCenterResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.CronVCenterResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronVCenterResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNextRunTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getCronExpression();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getEnabled();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional int64 next_run_time_ms = 1;
 * @return {number}
 */
proto.api.CronVCenterResponse.prototype.getNextRunTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.CronVCenterResponse} returns this
 */
proto.api.CronVCenterResponse.prototype.setNextRunTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string cron_expression = 2;
 * @return {string}
 */
proto.api.CronVCenterResponse.prototype.getCronExpression = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronVCenterResponse} returns this
 */
proto.api.CronVCenterResponse.prototype.setCronExpression = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool enabled = 3;
 * @return {boolean}
 */
proto.api.CronVCenterResponse.prototype.getEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.CronVCenterResponse} returns this
 */
proto.api.CronVCenterResponse.prototype.setEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CronNfsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CronNfsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CronNfsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronNfsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    environmentName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CronNfsRequest}
 */
proto.api.CronNfsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CronNfsRequest;
  return proto.api.CronNfsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CronNfsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CronNfsRequest}
 */
proto.api.CronNfsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CronNfsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.CronNfsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.CronNfsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronNfsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string environment_name = 1;
 * @return {string}
 */
proto.api.CronNfsRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronNfsRequest} returns this
 */
proto.api.CronNfsRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.CronNfsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.CronNfsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.CronNfsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronNfsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    nextRunTimeMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    cronExpression: jspb.Message.getFieldWithDefault(msg, 2, ""),
    enabled: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.CronNfsResponse}
 */
proto.api.CronNfsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.CronNfsResponse;
  return proto.api.CronNfsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.CronNfsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.CronNfsResponse}
 */
proto.api.CronNfsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNextRunTimeMs(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCronExpression(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnabled(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.CronNfsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.CronNfsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.CronNfsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.CronNfsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNextRunTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getCronExpression();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getEnabled();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional int64 next_run_time_ms = 1;
 * @return {number}
 */
proto.api.CronNfsResponse.prototype.getNextRunTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.CronNfsResponse} returns this
 */
proto.api.CronNfsResponse.prototype.setNextRunTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string cron_expression = 2;
 * @return {string}
 */
proto.api.CronNfsResponse.prototype.getCronExpression = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.CronNfsResponse} returns this
 */
proto.api.CronNfsResponse.prototype.setCronExpression = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool enabled = 3;
 * @return {boolean}
 */
proto.api.CronNfsResponse.prototype.getEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.CronNfsResponse} returns this
 */
proto.api.CronNfsResponse.prototype.setEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.ResyncVCenterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.ResyncVCenterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.ResyncVCenterRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ResyncVCenterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.ResyncVCenterRequest}
 */
proto.api.ResyncVCenterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.ResyncVCenterRequest;
  return proto.api.ResyncVCenterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.ResyncVCenterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.ResyncVCenterRequest}
 */
proto.api.ResyncVCenterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.ResyncVCenterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.ResyncVCenterRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.ResyncVCenterRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ResyncVCenterRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.ResyncVCenterRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.ResyncVCenterRequest} returns this
 */
proto.api.ResyncVCenterRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.ResyncVCenterRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.ResyncVCenterRequest} returns this
 */
proto.api.ResyncVCenterRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.ResyncNfsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.ResyncNfsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.ResyncNfsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ResyncNfsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.ResyncNfsRequest}
 */
proto.api.ResyncNfsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.ResyncNfsRequest;
  return proto.api.ResyncNfsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.ResyncNfsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.ResyncNfsRequest}
 */
proto.api.ResyncNfsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.ResyncNfsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.ResyncNfsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.ResyncNfsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ResyncNfsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.ResyncNfsRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.ResyncNfsRequest} returns this
 */
proto.api.ResyncNfsRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.ResyncNfsRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.ResyncNfsRequest} returns this
 */
proto.api.ResyncNfsRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncHistoryRec.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncHistoryRec.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncHistoryRec} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRec.toObject = function(includeInstance, msg) {
  var f, obj = {
    syncHistoryId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    status: jspb.Message.getFieldWithDefault(msg, 3, ""),
    initiatedBy: jspb.Message.getFieldWithDefault(msg, 4, ""),
    startTimeMs: jspb.Message.getFieldWithDefault(msg, 5, 0),
    endTimeMs: jspb.Message.getFieldWithDefault(msg, 6, 0),
    recordsProcessed: jspb.Message.getFieldWithDefault(msg, 7, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncHistoryRec}
 */
proto.api.SyncHistoryRec.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncHistoryRec;
  return proto.api.SyncHistoryRec.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncHistoryRec} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncHistoryRec}
 */
proto.api.SyncHistoryRec.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSyncHistoryId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setInitiatedBy(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStartTimeMs(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setEndTimeMs(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setRecordsProcessed(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncHistoryRec.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncHistoryRec.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncHistoryRec} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRec.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSyncHistoryId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getInitiatedBy();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getStartTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getEndTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getRecordsProcessed();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
};


/**
 * optional int64 sync_history_id = 1;
 * @return {number}
 */
proto.api.SyncHistoryRec.prototype.getSyncHistoryId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setSyncHistoryId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.SyncHistoryRec.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string status = 3;
 * @return {string}
 */
proto.api.SyncHistoryRec.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string initiated_by = 4;
 * @return {string}
 */
proto.api.SyncHistoryRec.prototype.getInitiatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setInitiatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int64 start_time_ms = 5;
 * @return {number}
 */
proto.api.SyncHistoryRec.prototype.getStartTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setStartTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int64 end_time_ms = 6;
 * @return {number}
 */
proto.api.SyncHistoryRec.prototype.getEndTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setEndTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 records_processed = 7;
 * @return {number}
 */
proto.api.SyncHistoryRec.prototype.getRecordsProcessed = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncHistoryRec} returns this
 */
proto.api.SyncHistoryRec.prototype.setRecordsProcessed = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.SyncHistoryRecArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncHistoryRecArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncHistoryRecArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncHistoryRecArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRecArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.SyncHistoryRec.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncHistoryRecArray}
 */
proto.api.SyncHistoryRecArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncHistoryRecArray;
  return proto.api.SyncHistoryRecArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncHistoryRecArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncHistoryRecArray}
 */
proto.api.SyncHistoryRecArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.SyncHistoryRec;
      reader.readMessage(value,proto.api.SyncHistoryRec.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncHistoryRecArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncHistoryRecArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncHistoryRecArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRecArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.SyncHistoryRec.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SyncHistoryRec items = 1;
 * @return {!Array<!proto.api.SyncHistoryRec>}
 */
proto.api.SyncHistoryRecArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.SyncHistoryRec>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.SyncHistoryRec, 1));
};


/**
 * @param {!Array<!proto.api.SyncHistoryRec>} value
 * @return {!proto.api.SyncHistoryRecArray} returns this
*/
proto.api.SyncHistoryRecArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.SyncHistoryRec=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.SyncHistoryRec}
 */
proto.api.SyncHistoryRecArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.SyncHistoryRec, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.SyncHistoryRecArray} returns this
 */
proto.api.SyncHistoryRecArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsSyncHistoryRec.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsSyncHistoryRec.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsSyncHistoryRec} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncHistoryRec.toObject = function(includeInstance, msg) {
  var f, obj = {
    syncHistoryId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    status: jspb.Message.getFieldWithDefault(msg, 3, ""),
    initiatedBy: jspb.Message.getFieldWithDefault(msg, 4, ""),
    startTimeMs: jspb.Message.getFieldWithDefault(msg, 5, 0),
    endTimeMs: jspb.Message.getFieldWithDefault(msg, 6, 0),
    recordsProcessed: jspb.Message.getFieldWithDefault(msg, 7, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsSyncHistoryRec}
 */
proto.api.NfsSyncHistoryRec.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsSyncHistoryRec;
  return proto.api.NfsSyncHistoryRec.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsSyncHistoryRec} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsSyncHistoryRec}
 */
proto.api.NfsSyncHistoryRec.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSyncHistoryId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setInitiatedBy(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStartTimeMs(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setEndTimeMs(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setRecordsProcessed(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsSyncHistoryRec.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsSyncHistoryRec.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsSyncHistoryRec} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncHistoryRec.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSyncHistoryId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getInitiatedBy();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getStartTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getEndTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getRecordsProcessed();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
};


/**
 * optional int64 sync_history_id = 1;
 * @return {number}
 */
proto.api.NfsSyncHistoryRec.prototype.getSyncHistoryId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setSyncHistoryId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.NfsSyncHistoryRec.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string status = 3;
 * @return {string}
 */
proto.api.NfsSyncHistoryRec.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string initiated_by = 4;
 * @return {string}
 */
proto.api.NfsSyncHistoryRec.prototype.getInitiatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setInitiatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int64 start_time_ms = 5;
 * @return {number}
 */
proto.api.NfsSyncHistoryRec.prototype.getStartTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setStartTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int64 end_time_ms = 6;
 * @return {number}
 */
proto.api.NfsSyncHistoryRec.prototype.getEndTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setEndTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 records_processed = 7;
 * @return {number}
 */
proto.api.NfsSyncHistoryRec.prototype.getRecordsProcessed = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncHistoryRec} returns this
 */
proto.api.NfsSyncHistoryRec.prototype.setRecordsProcessed = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.NfsSyncHistoryRecArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsSyncHistoryRecArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsSyncHistoryRecArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsSyncHistoryRecArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncHistoryRecArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.NfsSyncHistoryRec.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsSyncHistoryRecArray}
 */
proto.api.NfsSyncHistoryRecArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsSyncHistoryRecArray;
  return proto.api.NfsSyncHistoryRecArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsSyncHistoryRecArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsSyncHistoryRecArray}
 */
proto.api.NfsSyncHistoryRecArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.NfsSyncHistoryRec;
      reader.readMessage(value,proto.api.NfsSyncHistoryRec.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsSyncHistoryRecArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsSyncHistoryRecArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsSyncHistoryRecArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncHistoryRecArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.NfsSyncHistoryRec.serializeBinaryToWriter
    );
  }
};


/**
 * repeated NfsSyncHistoryRec items = 1;
 * @return {!Array<!proto.api.NfsSyncHistoryRec>}
 */
proto.api.NfsSyncHistoryRecArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.NfsSyncHistoryRec>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.NfsSyncHistoryRec, 1));
};


/**
 * @param {!Array<!proto.api.NfsSyncHistoryRec>} value
 * @return {!proto.api.NfsSyncHistoryRecArray} returns this
*/
proto.api.NfsSyncHistoryRecArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.NfsSyncHistoryRec=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.NfsSyncHistoryRec}
 */
proto.api.NfsSyncHistoryRecArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.NfsSyncHistoryRec, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.NfsSyncHistoryRecArray} returns this
 */
proto.api.NfsSyncHistoryRecArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncHistoryRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncHistoryRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncHistoryRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    numRecords: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncHistoryRequest}
 */
proto.api.SyncHistoryRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncHistoryRequest;
  return proto.api.SyncHistoryRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncHistoryRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncHistoryRequest}
 */
proto.api.SyncHistoryRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumRecords(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncHistoryRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncHistoryRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncHistoryRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncHistoryRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getNumRecords();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.SyncHistoryRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncHistoryRequest} returns this
 */
proto.api.SyncHistoryRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.SyncHistoryRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncHistoryRequest} returns this
 */
proto.api.SyncHistoryRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int32 num_records = 3;
 * @return {number}
 */
proto.api.SyncHistoryRequest.prototype.getNumRecords = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncHistoryRequest} returns this
 */
proto.api.SyncHistoryRequest.prototype.setNumRecords = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncLogRecRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncLogRecRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncLogRecRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    environmentName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    parentSyncHistoryId: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncLogRecRequest}
 */
proto.api.SyncLogRecRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncLogRecRequest;
  return proto.api.SyncLogRecRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncLogRecRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncLogRecRequest}
 */
proto.api.SyncLogRecRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setParentSyncHistoryId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncLogRecRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncLogRecRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncLogRecRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnvironmentName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getParentSyncHistoryId();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.SyncLogRecRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncLogRecRequest} returns this
 */
proto.api.SyncLogRecRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string environment_name = 2;
 * @return {string}
 */
proto.api.SyncLogRecRequest.prototype.getEnvironmentName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncLogRecRequest} returns this
 */
proto.api.SyncLogRecRequest.prototype.setEnvironmentName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int64 parent_sync_history_id = 3;
 * @return {number}
 */
proto.api.SyncLogRecRequest.prototype.getParentSyncHistoryId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncLogRecRequest} returns this
 */
proto.api.SyncLogRecRequest.prototype.setParentSyncHistoryId = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncLogRecResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncLogRecResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncLogRecResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    parentId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    timeMs: jspb.Message.getFieldWithDefault(msg, 2, 0),
    messageError: jspb.Message.getFieldWithDefault(msg, 3, ""),
    messageWarn: jspb.Message.getFieldWithDefault(msg, 4, ""),
    messageInfo: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncLogRecResponse}
 */
proto.api.SyncLogRecResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncLogRecResponse;
  return proto.api.SyncLogRecResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncLogRecResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncLogRecResponse}
 */
proto.api.SyncLogRecResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setParentId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimeMs(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageError(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageWarn(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageInfo(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncLogRecResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncLogRecResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncLogRecResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getParentId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getMessageError();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMessageWarn();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getMessageInfo();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional int64 parent_id = 1;
 * @return {number}
 */
proto.api.SyncLogRecResponse.prototype.getParentId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncLogRecResponse} returns this
 */
proto.api.SyncLogRecResponse.prototype.setParentId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 time_ms = 2;
 * @return {number}
 */
proto.api.SyncLogRecResponse.prototype.getTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SyncLogRecResponse} returns this
 */
proto.api.SyncLogRecResponse.prototype.setTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string message_error = 3;
 * @return {string}
 */
proto.api.SyncLogRecResponse.prototype.getMessageError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncLogRecResponse} returns this
 */
proto.api.SyncLogRecResponse.prototype.setMessageError = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string message_warn = 4;
 * @return {string}
 */
proto.api.SyncLogRecResponse.prototype.getMessageWarn = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncLogRecResponse} returns this
 */
proto.api.SyncLogRecResponse.prototype.setMessageWarn = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string message_info = 5;
 * @return {string}
 */
proto.api.SyncLogRecResponse.prototype.getMessageInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SyncLogRecResponse} returns this
 */
proto.api.SyncLogRecResponse.prototype.setMessageInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.SyncLogRecResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SyncLogRecResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SyncLogRecResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SyncLogRecResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.SyncLogRecResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SyncLogRecResponseArray}
 */
proto.api.SyncLogRecResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SyncLogRecResponseArray;
  return proto.api.SyncLogRecResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SyncLogRecResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SyncLogRecResponseArray}
 */
proto.api.SyncLogRecResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.SyncLogRecResponse;
      reader.readMessage(value,proto.api.SyncLogRecResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SyncLogRecResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SyncLogRecResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SyncLogRecResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SyncLogRecResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.SyncLogRecResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SyncLogRecResponse items = 1;
 * @return {!Array<!proto.api.SyncLogRecResponse>}
 */
proto.api.SyncLogRecResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.SyncLogRecResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.SyncLogRecResponse, 1));
};


/**
 * @param {!Array<!proto.api.SyncLogRecResponse>} value
 * @return {!proto.api.SyncLogRecResponseArray} returns this
*/
proto.api.SyncLogRecResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.SyncLogRecResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.SyncLogRecResponse}
 */
proto.api.SyncLogRecResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.SyncLogRecResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.SyncLogRecResponseArray} returns this
 */
proto.api.SyncLogRecResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsSyncLogRecResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsSyncLogRecResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsSyncLogRecResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncLogRecResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    parentId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    timeMs: jspb.Message.getFieldWithDefault(msg, 2, 0),
    messageError: jspb.Message.getFieldWithDefault(msg, 3, ""),
    messageWarn: jspb.Message.getFieldWithDefault(msg, 4, ""),
    messageInfo: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsSyncLogRecResponse}
 */
proto.api.NfsSyncLogRecResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsSyncLogRecResponse;
  return proto.api.NfsSyncLogRecResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsSyncLogRecResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsSyncLogRecResponse}
 */
proto.api.NfsSyncLogRecResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setParentId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimeMs(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageError(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageWarn(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessageInfo(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsSyncLogRecResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsSyncLogRecResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsSyncLogRecResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncLogRecResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getParentId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getTimeMs();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getMessageError();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMessageWarn();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getMessageInfo();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional int64 parent_id = 1;
 * @return {number}
 */
proto.api.NfsSyncLogRecResponse.prototype.getParentId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncLogRecResponse} returns this
 */
proto.api.NfsSyncLogRecResponse.prototype.setParentId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 time_ms = 2;
 * @return {number}
 */
proto.api.NfsSyncLogRecResponse.prototype.getTimeMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsSyncLogRecResponse} returns this
 */
proto.api.NfsSyncLogRecResponse.prototype.setTimeMs = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string message_error = 3;
 * @return {string}
 */
proto.api.NfsSyncLogRecResponse.prototype.getMessageError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncLogRecResponse} returns this
 */
proto.api.NfsSyncLogRecResponse.prototype.setMessageError = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string message_warn = 4;
 * @return {string}
 */
proto.api.NfsSyncLogRecResponse.prototype.getMessageWarn = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncLogRecResponse} returns this
 */
proto.api.NfsSyncLogRecResponse.prototype.setMessageWarn = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string message_info = 5;
 * @return {string}
 */
proto.api.NfsSyncLogRecResponse.prototype.getMessageInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsSyncLogRecResponse} returns this
 */
proto.api.NfsSyncLogRecResponse.prototype.setMessageInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.NfsSyncLogRecResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsSyncLogRecResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsSyncLogRecResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsSyncLogRecResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncLogRecResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.NfsSyncLogRecResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsSyncLogRecResponseArray}
 */
proto.api.NfsSyncLogRecResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsSyncLogRecResponseArray;
  return proto.api.NfsSyncLogRecResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsSyncLogRecResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsSyncLogRecResponseArray}
 */
proto.api.NfsSyncLogRecResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.NfsSyncLogRecResponse;
      reader.readMessage(value,proto.api.NfsSyncLogRecResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsSyncLogRecResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsSyncLogRecResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsSyncLogRecResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsSyncLogRecResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.NfsSyncLogRecResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated NfsSyncLogRecResponse items = 1;
 * @return {!Array<!proto.api.NfsSyncLogRecResponse>}
 */
proto.api.NfsSyncLogRecResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.NfsSyncLogRecResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.NfsSyncLogRecResponse, 1));
};


/**
 * @param {!Array<!proto.api.NfsSyncLogRecResponse>} value
 * @return {!proto.api.NfsSyncLogRecResponseArray} returns this
*/
proto.api.NfsSyncLogRecResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.NfsSyncLogRecResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.NfsSyncLogRecResponse}
 */
proto.api.NfsSyncLogRecResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.NfsSyncLogRecResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.NfsSyncLogRecResponseArray} returns this
 */
proto.api.NfsSyncLogRecResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.HypervisorResponse.repeatedFields_ = [18,20,21];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vcenter: jspb.Message.getFieldWithDefault(msg, 3, ""),
    cluster: jspb.Message.getFieldWithDefault(msg, 4, ""),
    cpuCount: jspb.Message.getFieldWithDefault(msg, 5, 0),
    memoryMb: jspb.Message.getFieldWithDefault(msg, 6, 0),
    hddGb: jspb.Message.getFieldWithDefault(msg, 7, 0),
    vmCount: jspb.Message.getFieldWithDefault(msg, 8, 0),
    usagemb: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    usagecpu: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    uptime: jspb.Message.getFieldWithDefault(msg, 11, ""),
    state: jspb.Message.getFieldWithDefault(msg, 12, ""),
    status: jspb.Message.getFieldWithDefault(msg, 13, ""),
    vmotion: jspb.Message.getBooleanFieldWithDefault(msg, 14, false),
    netPortId: jspb.Message.getFieldWithDefault(msg, 15, ""),
    netSystemName: jspb.Message.getFieldWithDefault(msg, 16, ""),
    rack: jspb.Message.getFieldWithDefault(msg, 17, ""),
    tagsList: (f = jspb.Message.getRepeatedField(msg, 18)) == null ? undefined : f,
    managedobjref: jspb.Message.getFieldWithDefault(msg, 19, ""),
    networksList: (f = jspb.Message.getRepeatedField(msg, 20)) == null ? undefined : f,
    datastoresList: jspb.Message.toObjectList(msg.getDatastoresList(),
    proto.api.Datastore.toObject, includeInstance),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 22, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 23, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 24, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 25, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorResponse}
 */
proto.api.HypervisorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorResponse;
  return proto.api.HypervisorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorResponse}
 */
proto.api.HypervisorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenter(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setCluster(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCpuCount(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMemoryMb(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHddGb(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setVmCount(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setUsagemb(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setUsagecpu(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setUptime(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setState(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 14:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setVmotion(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setNetPortId(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setNetSystemName(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setRack(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.addTags(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setManagedobjref(value);
      break;
    case 20:
      var value = /** @type {string} */ (reader.readString());
      msg.addNetworks(value);
      break;
    case 21:
      var value = new proto.api.Datastore;
      reader.readMessage(value,proto.api.Datastore.deserializeBinaryFromReader);
      msg.addDatastores(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 23:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 24:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 25:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVcenter();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getCluster();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCpuCount();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getMemoryMb();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getHddGb();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getVmCount();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getUsagemb();
  if (f !== 0.0) {
    writer.writeFloat(
      9,
      f
    );
  }
  f = message.getUsagecpu();
  if (f !== 0.0) {
    writer.writeFloat(
      10,
      f
    );
  }
  f = message.getUptime();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getState();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getVmotion();
  if (f) {
    writer.writeBool(
      14,
      f
    );
  }
  f = message.getNetPortId();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getNetSystemName();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getRack();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getTagsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      18,
      f
    );
  }
  f = message.getManagedobjref();
  if (f.length > 0) {
    writer.writeString(
      19,
      f
    );
  }
  f = message.getNetworksList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      20,
      f
    );
  }
  f = message.getDatastoresList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      21,
      f,
      proto.api.Datastore.serializeBinaryToWriter
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      23,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      24,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      25,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vcenter = 3;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getVcenter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setVcenter = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string cluster = 4;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getCluster = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setCluster = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int32 cpu_count = 5;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getCpuCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setCpuCount = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int64 memory_MB = 6;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getMemoryMb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setMemoryMb = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 hdd_GB = 7;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getHddGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setHddGb = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 vm_count = 8;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getVmCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setVmCount = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional float usageMB = 9;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getUsagemb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUsagemb = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional float usageCPU = 10;
 * @return {number}
 */
proto.api.HypervisorResponse.prototype.getUsagecpu = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUsagecpu = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional string uptime = 11;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getUptime = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUptime = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string state = 12;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getState = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setState = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string status = 13;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional bool vmotion = 14;
 * @return {boolean}
 */
proto.api.HypervisorResponse.prototype.getVmotion = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 14, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setVmotion = function(value) {
  return jspb.Message.setProto3BooleanField(this, 14, value);
};


/**
 * optional string net_port_id = 15;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getNetPortId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setNetPortId = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string net_system_name = 16;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getNetSystemName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setNetSystemName = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string rack = 17;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getRack = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setRack = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * repeated string tags = 18;
 * @return {!Array<string>}
 */
proto.api.HypervisorResponse.prototype.getTagsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 18));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setTagsList = function(value) {
  return jspb.Message.setField(this, 18, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.addTags = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 18, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.clearTagsList = function() {
  return this.setTagsList([]);
};


/**
 * optional string managedObjRef = 19;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getManagedobjref = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setManagedobjref = function(value) {
  return jspb.Message.setProto3StringField(this, 19, value);
};


/**
 * repeated string networks = 20;
 * @return {!Array<string>}
 */
proto.api.HypervisorResponse.prototype.getNetworksList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 20));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setNetworksList = function(value) {
  return jspb.Message.setField(this, 20, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.addNetworks = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 20, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.clearNetworksList = function() {
  return this.setNetworksList([]);
};


/**
 * repeated Datastore datastores = 21;
 * @return {!Array<!proto.api.Datastore>}
 */
proto.api.HypervisorResponse.prototype.getDatastoresList = function() {
  return /** @type{!Array<!proto.api.Datastore>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.Datastore, 21));
};


/**
 * @param {!Array<!proto.api.Datastore>} value
 * @return {!proto.api.HypervisorResponse} returns this
*/
proto.api.HypervisorResponse.prototype.setDatastoresList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 21, value);
};


/**
 * @param {!proto.api.Datastore=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.Datastore}
 */
proto.api.HypervisorResponse.prototype.addDatastores = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 21, opt_value, proto.api.Datastore, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.clearDatastoresList = function() {
  return this.setDatastoresList([]);
};


/**
 * optional string ui_msg_info = 22;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional string ui_msg_warning = 23;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 23, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 23, value);
};


/**
 * optional string ui_msg_error = 24;
 * @return {string}
 */
proto.api.HypervisorResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 24, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 24, value);
};


/**
 * optional bool ui_control_is_preview = 25;
 * @return {boolean}
 */
proto.api.HypervisorResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 25, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.HypervisorResponse} returns this
 */
proto.api.HypervisorResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 25, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.HypervisorResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.HypervisorResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorResponseArray}
 */
proto.api.HypervisorResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorResponseArray;
  return proto.api.HypervisorResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorResponseArray}
 */
proto.api.HypervisorResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.HypervisorResponse;
      reader.readMessage(value,proto.api.HypervisorResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.HypervisorResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated HypervisorResponse items = 1;
 * @return {!Array<!proto.api.HypervisorResponse>}
 */
proto.api.HypervisorResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.HypervisorResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.HypervisorResponse, 1));
};


/**
 * @param {!Array<!proto.api.HypervisorResponse>} value
 * @return {!proto.api.HypervisorResponseArray} returns this
*/
proto.api.HypervisorResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.HypervisorResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorResponse}
 */
proto.api.HypervisorResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.HypervisorResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorResponseArray} returns this
 */
proto.api.HypervisorResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VirtualMachineResponse.repeatedFields_ = [10,11];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VirtualMachineResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VirtualMachineResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VirtualMachineResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vcenter: jspb.Message.getFieldWithDefault(msg, 3, ""),
    host: jspb.Message.getFieldWithDefault(msg, 4, ""),
    os: jspb.Message.getFieldWithDefault(msg, 5, ""),
    cpus: jspb.Message.getFieldWithDefault(msg, 6, 0),
    totalCpuMhz: jspb.Message.getFieldWithDefault(msg, 7, 0),
    hddAllocatedGb: jspb.Message.getFieldWithDefault(msg, 8, 0),
    memMb: jspb.Message.getFieldWithDefault(msg, 9, 0),
    networksList: jspb.Message.toObjectList(msg.getNetworksList(),
    proto.api.VirtualMachineNetwork.toObject, includeInstance),
    datastoresList: jspb.Message.toObjectList(msg.getDatastoresList(),
    proto.api.Datastore.toObject, includeInstance),
    cpuHotAddEnabled: jspb.Message.getBooleanFieldWithDefault(msg, 12, false),
    memHotAddEnabled: jspb.Message.getBooleanFieldWithDefault(msg, 13, false),
    uptimeSec: jspb.Message.getFieldWithDefault(msg, 14, 0),
    memUsagePerc: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0),
    cpuUsagePerc: jspb.Message.getFloatingPointFieldWithDefault(msg, 16, 0.0),
    state: jspb.Message.getFieldWithDefault(msg, 17, ""),
    overallStatus: jspb.Message.getFieldWithDefault(msg, 18, ""),
    rack: jspb.Message.getFieldWithDefault(msg, 19, ""),
    managedObjRef: jspb.Message.getFieldWithDefault(msg, 20, ""),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 21, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 22, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 23, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 24, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VirtualMachineResponse}
 */
proto.api.VirtualMachineResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VirtualMachineResponse;
  return proto.api.VirtualMachineResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VirtualMachineResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VirtualMachineResponse}
 */
proto.api.VirtualMachineResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenter(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setHost(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setOs(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCpus(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalCpuMhz(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHddAllocatedGb(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMemMb(value);
      break;
    case 10:
      var value = new proto.api.VirtualMachineNetwork;
      reader.readMessage(value,proto.api.VirtualMachineNetwork.deserializeBinaryFromReader);
      msg.addNetworks(value);
      break;
    case 11:
      var value = new proto.api.Datastore;
      reader.readMessage(value,proto.api.Datastore.deserializeBinaryFromReader);
      msg.addDatastores(value);
      break;
    case 12:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCpuHotAddEnabled(value);
      break;
    case 13:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setMemHotAddEnabled(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setUptimeSec(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMemUsagePerc(value);
      break;
    case 16:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setCpuUsagePerc(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setState(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.setOverallStatus(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setRack(value);
      break;
    case 20:
      var value = /** @type {string} */ (reader.readString());
      msg.setManagedObjRef(value);
      break;
    case 21:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 23:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 24:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VirtualMachineResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VirtualMachineResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VirtualMachineResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVcenter();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getHost();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getOs();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getCpus();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getTotalCpuMhz();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getHddAllocatedGb();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getMemMb();
  if (f !== 0) {
    writer.writeInt32(
      9,
      f
    );
  }
  f = message.getNetworksList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      proto.api.VirtualMachineNetwork.serializeBinaryToWriter
    );
  }
  f = message.getDatastoresList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      proto.api.Datastore.serializeBinaryToWriter
    );
  }
  f = message.getCpuHotAddEnabled();
  if (f) {
    writer.writeBool(
      12,
      f
    );
  }
  f = message.getMemHotAddEnabled();
  if (f) {
    writer.writeBool(
      13,
      f
    );
  }
  f = message.getUptimeSec();
  if (f !== 0) {
    writer.writeInt32(
      14,
      f
    );
  }
  f = message.getMemUsagePerc();
  if (f !== 0.0) {
    writer.writeDouble(
      15,
      f
    );
  }
  f = message.getCpuUsagePerc();
  if (f !== 0.0) {
    writer.writeDouble(
      16,
      f
    );
  }
  f = message.getState();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getOverallStatus();
  if (f.length > 0) {
    writer.writeString(
      18,
      f
    );
  }
  f = message.getRack();
  if (f.length > 0) {
    writer.writeString(
      19,
      f
    );
  }
  f = message.getManagedObjRef();
  if (f.length > 0) {
    writer.writeString(
      20,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      21,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      23,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      24,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vcenter = 3;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getVcenter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setVcenter = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string host = 4;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getHost = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setHost = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string os = 5;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getOs = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setOs = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int32 cpus = 6;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getCpus = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setCpus = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 total_cpu_mhz = 7;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getTotalCpuMhz = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setTotalCpuMhz = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 hdd_allocated_gb = 8;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getHddAllocatedGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setHddAllocatedGb = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional int32 mem_mb = 9;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getMemMb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setMemMb = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * repeated VirtualMachineNetwork networks = 10;
 * @return {!Array<!proto.api.VirtualMachineNetwork>}
 */
proto.api.VirtualMachineResponse.prototype.getNetworksList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineNetwork>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineNetwork, 10));
};


/**
 * @param {!Array<!proto.api.VirtualMachineNetwork>} value
 * @return {!proto.api.VirtualMachineResponse} returns this
*/
proto.api.VirtualMachineResponse.prototype.setNetworksList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.api.VirtualMachineNetwork=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineNetwork}
 */
proto.api.VirtualMachineResponse.prototype.addNetworks = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.api.VirtualMachineNetwork, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.clearNetworksList = function() {
  return this.setNetworksList([]);
};


/**
 * repeated Datastore datastores = 11;
 * @return {!Array<!proto.api.Datastore>}
 */
proto.api.VirtualMachineResponse.prototype.getDatastoresList = function() {
  return /** @type{!Array<!proto.api.Datastore>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.Datastore, 11));
};


/**
 * @param {!Array<!proto.api.Datastore>} value
 * @return {!proto.api.VirtualMachineResponse} returns this
*/
proto.api.VirtualMachineResponse.prototype.setDatastoresList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.api.Datastore=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.Datastore}
 */
proto.api.VirtualMachineResponse.prototype.addDatastores = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.api.Datastore, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.clearDatastoresList = function() {
  return this.setDatastoresList([]);
};


/**
 * optional bool cpu_hot_add_enabled = 12;
 * @return {boolean}
 */
proto.api.VirtualMachineResponse.prototype.getCpuHotAddEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 12, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setCpuHotAddEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 12, value);
};


/**
 * optional bool mem_hot_add_enabled = 13;
 * @return {boolean}
 */
proto.api.VirtualMachineResponse.prototype.getMemHotAddEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 13, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setMemHotAddEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 13, value);
};


/**
 * optional int32 uptime_sec = 14;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getUptimeSec = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 14, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setUptimeSec = function(value) {
  return jspb.Message.setProto3IntField(this, 14, value);
};


/**
 * optional double mem_usage_perc = 15;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getMemUsagePerc = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setMemUsagePerc = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};


/**
 * optional double cpu_usage_perc = 16;
 * @return {number}
 */
proto.api.VirtualMachineResponse.prototype.getCpuUsagePerc = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 16, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setCpuUsagePerc = function(value) {
  return jspb.Message.setProto3FloatField(this, 16, value);
};


/**
 * optional string state = 17;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getState = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setState = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * optional string overall_status = 18;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getOverallStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 18, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setOverallStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 18, value);
};


/**
 * optional string rack = 19;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getRack = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setRack = function(value) {
  return jspb.Message.setProto3StringField(this, 19, value);
};


/**
 * optional string managed_obj_ref = 20;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getManagedObjRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 20, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setManagedObjRef = function(value) {
  return jspb.Message.setProto3StringField(this, 20, value);
};


/**
 * optional string ui_msg_info = 21;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 21, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 21, value);
};


/**
 * optional string ui_msg_warning = 22;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional string ui_msg_error = 23;
 * @return {string}
 */
proto.api.VirtualMachineResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 23, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 23, value);
};


/**
 * optional bool ui_control_is_preview = 24;
 * @return {boolean}
 */
proto.api.VirtualMachineResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 24, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VirtualMachineResponse} returns this
 */
proto.api.VirtualMachineResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 24, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VirtualMachineNetwork.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VirtualMachineNetwork.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VirtualMachineNetwork.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VirtualMachineNetwork} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineNetwork.toObject = function(includeInstance, msg) {
  var f, obj = {
    networkPath: jspb.Message.getFieldWithDefault(msg, 1, ""),
    networkName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    adapterType: jspb.Message.getFieldWithDefault(msg, 3, ""),
    macAddress: jspb.Message.getFieldWithDefault(msg, 4, ""),
    ipAddressesList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VirtualMachineNetwork}
 */
proto.api.VirtualMachineNetwork.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VirtualMachineNetwork;
  return proto.api.VirtualMachineNetwork.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VirtualMachineNetwork} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VirtualMachineNetwork}
 */
proto.api.VirtualMachineNetwork.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setNetworkPath(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNetworkName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setAdapterType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMacAddress(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.addIpAddresses(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VirtualMachineNetwork.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VirtualMachineNetwork.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VirtualMachineNetwork} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineNetwork.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getNetworkPath();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getNetworkName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getAdapterType();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMacAddress();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getIpAddressesList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
};


/**
 * optional string network_path = 1;
 * @return {string}
 */
proto.api.VirtualMachineNetwork.prototype.getNetworkPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.setNetworkPath = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string network_name = 2;
 * @return {string}
 */
proto.api.VirtualMachineNetwork.prototype.getNetworkName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.setNetworkName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string adapter_type = 3;
 * @return {string}
 */
proto.api.VirtualMachineNetwork.prototype.getAdapterType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.setAdapterType = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string mac_address = 4;
 * @return {string}
 */
proto.api.VirtualMachineNetwork.prototype.getMacAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.setMacAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * repeated string ip_addresses = 5;
 * @return {!Array<string>}
 */
proto.api.VirtualMachineNetwork.prototype.getIpAddressesList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.setIpAddressesList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.addIpAddresses = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineNetwork} returns this
 */
proto.api.VirtualMachineNetwork.prototype.clearIpAddressesList = function() {
  return this.setIpAddressesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VirtualMachineResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VirtualMachineResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VirtualMachineResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VirtualMachineResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.VirtualMachineResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VirtualMachineResponseArray}
 */
proto.api.VirtualMachineResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VirtualMachineResponseArray;
  return proto.api.VirtualMachineResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VirtualMachineResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VirtualMachineResponseArray}
 */
proto.api.VirtualMachineResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.VirtualMachineResponse;
      reader.readMessage(value,proto.api.VirtualMachineResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VirtualMachineResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VirtualMachineResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VirtualMachineResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.VirtualMachineResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated VirtualMachineResponse items = 1;
 * @return {!Array<!proto.api.VirtualMachineResponse>}
 */
proto.api.VirtualMachineResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineResponse, 1));
};


/**
 * @param {!Array<!proto.api.VirtualMachineResponse>} value
 * @return {!proto.api.VirtualMachineResponseArray} returns this
*/
proto.api.VirtualMachineResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.VirtualMachineResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineResponse}
 */
proto.api.VirtualMachineResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.VirtualMachineResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineResponseArray} returns this
 */
proto.api.VirtualMachineResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.DatastoreResponse.repeatedFields_ = [8,9];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DatastoreResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DatastoreResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DatastoreResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    url: jspb.Message.getFieldWithDefault(msg, 2, ""),
    path: jspb.Message.getFieldWithDefault(msg, 3, ""),
    managedObjRef: jspb.Message.getFieldWithDefault(msg, 4, ""),
    capacityBytes: jspb.Message.getFieldWithDefault(msg, 5, 0),
    capacityRemainingBytes: jspb.Message.getFieldWithDefault(msg, 6, 0),
    type: jspb.Message.getFieldWithDefault(msg, 7, ""),
    hypervisorsRefsList: jspb.Message.toObjectList(msg.getHypervisorsRefsList(),
    proto.api.DatastoreHost.toObject, includeInstance),
    vmRefsList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 10, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 11, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 12, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 13, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DatastoreResponse}
 */
proto.api.DatastoreResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DatastoreResponse;
  return proto.api.DatastoreResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DatastoreResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DatastoreResponse}
 */
proto.api.DatastoreResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setManagedObjRef(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCapacityBytes(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCapacityRemainingBytes(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 8:
      var value = new proto.api.DatastoreHost;
      reader.readMessage(value,proto.api.DatastoreHost.deserializeBinaryFromReader);
      msg.addHypervisorsRefs(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.addVmRefs(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 13:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DatastoreResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.DatastoreResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.DatastoreResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUrl();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getManagedObjRef();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCapacityBytes();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getCapacityRemainingBytes();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getHypervisorsRefsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      proto.api.DatastoreHost.serializeBinaryToWriter
    );
  }
  f = message.getVmRefsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      9,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      13,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string url = 2;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string path = 3;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string managed_obj_ref = 4;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getManagedObjRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setManagedObjRef = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int64 capacity_bytes = 5;
 * @return {number}
 */
proto.api.DatastoreResponse.prototype.getCapacityBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setCapacityBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int64 capacity_remaining_bytes = 6;
 * @return {number}
 */
proto.api.DatastoreResponse.prototype.getCapacityRemainingBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setCapacityRemainingBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional string type = 7;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * repeated DatastoreHost hypervisors_refs = 8;
 * @return {!Array<!proto.api.DatastoreHost>}
 */
proto.api.DatastoreResponse.prototype.getHypervisorsRefsList = function() {
  return /** @type{!Array<!proto.api.DatastoreHost>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.DatastoreHost, 8));
};


/**
 * @param {!Array<!proto.api.DatastoreHost>} value
 * @return {!proto.api.DatastoreResponse} returns this
*/
proto.api.DatastoreResponse.prototype.setHypervisorsRefsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.api.DatastoreHost=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.DatastoreHost}
 */
proto.api.DatastoreResponse.prototype.addHypervisorsRefs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.api.DatastoreHost, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.clearHypervisorsRefsList = function() {
  return this.setHypervisorsRefsList([]);
};


/**
 * repeated string vm_refs = 9;
 * @return {!Array<string>}
 */
proto.api.DatastoreResponse.prototype.getVmRefsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setVmRefsList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.addVmRefs = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.clearVmRefsList = function() {
  return this.setVmRefsList([]);
};


/**
 * optional string ui_msg_info = 10;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string ui_msg_warning = 11;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string ui_msg_error = 12;
 * @return {string}
 */
proto.api.DatastoreResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional bool ui_control_is_preview = 13;
 * @return {boolean}
 */
proto.api.DatastoreResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 13, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.DatastoreResponse} returns this
 */
proto.api.DatastoreResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 13, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DatastoreHost.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DatastoreHost.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DatastoreHost} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreHost.toObject = function(includeInstance, msg) {
  var f, obj = {
    datastoreRef: jspb.Message.getFieldWithDefault(msg, 1, ""),
    hypervisorRef: jspb.Message.getFieldWithDefault(msg, 2, ""),
    mountPoint: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DatastoreHost}
 */
proto.api.DatastoreHost.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DatastoreHost;
  return proto.api.DatastoreHost.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DatastoreHost} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DatastoreHost}
 */
proto.api.DatastoreHost.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastoreRef(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorRef(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMountPoint(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DatastoreHost.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.DatastoreHost.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.DatastoreHost} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreHost.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDatastoreRef();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getHypervisorRef();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getMountPoint();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string datastore_ref = 1;
 * @return {string}
 */
proto.api.DatastoreHost.prototype.getDatastoreRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreHost} returns this
 */
proto.api.DatastoreHost.prototype.setDatastoreRef = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string hypervisor_ref = 2;
 * @return {string}
 */
proto.api.DatastoreHost.prototype.getHypervisorRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreHost} returns this
 */
proto.api.DatastoreHost.prototype.setHypervisorRef = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string mount_point = 3;
 * @return {string}
 */
proto.api.DatastoreHost.prototype.getMountPoint = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreHost} returns this
 */
proto.api.DatastoreHost.prototype.setMountPoint = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.DatastoreResponseArray.repeatedFields_ = [11];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DatastoreResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DatastoreResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DatastoreResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.DatastoreResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DatastoreResponseArray}
 */
proto.api.DatastoreResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DatastoreResponseArray;
  return proto.api.DatastoreResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DatastoreResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DatastoreResponseArray}
 */
proto.api.DatastoreResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 11:
      var value = new proto.api.DatastoreResponse;
      reader.readMessage(value,proto.api.DatastoreResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DatastoreResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.DatastoreResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.DatastoreResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      proto.api.DatastoreResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated DatastoreResponse items = 11;
 * @return {!Array<!proto.api.DatastoreResponse>}
 */
proto.api.DatastoreResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.DatastoreResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.DatastoreResponse, 11));
};


/**
 * @param {!Array<!proto.api.DatastoreResponse>} value
 * @return {!proto.api.DatastoreResponseArray} returns this
*/
proto.api.DatastoreResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.api.DatastoreResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.DatastoreResponse}
 */
proto.api.DatastoreResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.api.DatastoreResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.DatastoreResponseArray} returns this
 */
proto.api.DatastoreResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.RoomResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.RoomResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.RoomResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    hypervisorPath: jspb.Message.getFieldWithDefault(msg, 1, ""),
    severity: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vmCount: jspb.Message.getFieldWithDefault(msg, 3, 0),
    totalHddGb: jspb.Message.getFieldWithDefault(msg, 4, 0),
    totalCpu: jspb.Message.getFieldWithDefault(msg, 5, 0),
    totalMemGb: jspb.Message.getFieldWithDefault(msg, 6, 0),
    headroomCpu: jspb.Message.getFieldWithDefault(msg, 7, 0),
    headroomMemGb: jspb.Message.getFieldWithDefault(msg, 8, 0),
    headroomHddGb: jspb.Message.getFieldWithDefault(msg, 9, 0),
    numStsgl: jspb.Message.getFieldWithDefault(msg, 10, 0),
    numStvar: jspb.Message.getFieldWithDefault(msg, 11, 0),
    numNvrcw: jspb.Message.getFieldWithDefault(msg, 12, 0),
    numStmmx: jspb.Message.getFieldWithDefault(msg, 13, 0),
    cpuBalanceScore: jspb.Message.getFloatingPointFieldWithDefault(msg, 14, 0.0),
    memBalanceScore: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0),
    guestVms: jspb.Message.getFieldWithDefault(msg, 16, ""),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 17, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 18, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 19, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 20, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.RoomResponse}
 */
proto.api.RoomResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.RoomResponse;
  return proto.api.RoomResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.RoomResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.RoomResponse}
 */
proto.api.RoomResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorPath(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSeverity(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setVmCount(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalHddGb(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalCpu(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotalMemGb(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHeadroomCpu(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHeadroomMemGb(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHeadroomHddGb(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumStsgl(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumStvar(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumNvrcw(value);
      break;
    case 13:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNumStmmx(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setCpuBalanceScore(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMemBalanceScore(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setGuestVms(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 20:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.RoomResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.RoomResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.RoomResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHypervisorPath();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSeverity();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVmCount();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getTotalHddGb();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getTotalCpu();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getTotalMemGb();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getHeadroomCpu();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getHeadroomMemGb();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getHeadroomHddGb();
  if (f !== 0) {
    writer.writeInt32(
      9,
      f
    );
  }
  f = message.getNumStsgl();
  if (f !== 0) {
    writer.writeInt32(
      10,
      f
    );
  }
  f = message.getNumStvar();
  if (f !== 0) {
    writer.writeInt32(
      11,
      f
    );
  }
  f = message.getNumNvrcw();
  if (f !== 0) {
    writer.writeInt32(
      12,
      f
    );
  }
  f = message.getNumStmmx();
  if (f !== 0) {
    writer.writeInt32(
      13,
      f
    );
  }
  f = message.getCpuBalanceScore();
  if (f !== 0.0) {
    writer.writeFloat(
      14,
      f
    );
  }
  f = message.getMemBalanceScore();
  if (f !== 0.0) {
    writer.writeFloat(
      15,
      f
    );
  }
  f = message.getGuestVms();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      18,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      19,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      20,
      f
    );
  }
};


/**
 * optional string hypervisor_path = 1;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getHypervisorPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setHypervisorPath = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string severity = 2;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getSeverity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setSeverity = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int32 vm_count = 3;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getVmCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setVmCount = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 total_hdd_gb = 4;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getTotalHddGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setTotalHddGb = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int32 total_cpu = 5;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getTotalCpu = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setTotalCpu = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int32 total_mem_gb = 6;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getTotalMemGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setTotalMemGb = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 headroom_cpu = 7;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getHeadroomCpu = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setHeadroomCpu = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 headroom_mem_gb = 8;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getHeadroomMemGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setHeadroomMemGb = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional int32 headroom_hdd_gb = 9;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getHeadroomHddGb = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setHeadroomHddGb = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * optional int32 num_stsgl = 10;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getNumStsgl = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 10, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setNumStsgl = function(value) {
  return jspb.Message.setProto3IntField(this, 10, value);
};


/**
 * optional int32 num_stvar = 11;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getNumStvar = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setNumStvar = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional int32 num_nvrcw = 12;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getNumNvrcw = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setNumNvrcw = function(value) {
  return jspb.Message.setProto3IntField(this, 12, value);
};


/**
 * optional int32 num_stmmx = 13;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getNumStmmx = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 13, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setNumStmmx = function(value) {
  return jspb.Message.setProto3IntField(this, 13, value);
};


/**
 * optional float cpu_balance_score = 14;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getCpuBalanceScore = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 14, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setCpuBalanceScore = function(value) {
  return jspb.Message.setProto3FloatField(this, 14, value);
};


/**
 * optional float mem_balance_score = 15;
 * @return {number}
 */
proto.api.RoomResponse.prototype.getMemBalanceScore = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setMemBalanceScore = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};


/**
 * optional string guest_vms = 16;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getGuestVms = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setGuestVms = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string ui_msg_info = 17;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * optional string ui_msg_warning = 18;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 18, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 18, value);
};


/**
 * optional string ui_msg_error = 19;
 * @return {string}
 */
proto.api.RoomResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 19, value);
};


/**
 * optional bool ui_control_is_preview = 20;
 * @return {boolean}
 */
proto.api.RoomResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 20, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.RoomResponse} returns this
 */
proto.api.RoomResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 20, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.RoomResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.RoomResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.RoomResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.RoomResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.RoomResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.RoomResponseArray}
 */
proto.api.RoomResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.RoomResponseArray;
  return proto.api.RoomResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.RoomResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.RoomResponseArray}
 */
proto.api.RoomResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.RoomResponse;
      reader.readMessage(value,proto.api.RoomResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.RoomResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.RoomResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.RoomResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.RoomResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated RoomResponse items = 1;
 * @return {!Array<!proto.api.RoomResponse>}
 */
proto.api.RoomResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.RoomResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.RoomResponse, 1));
};


/**
 * @param {!Array<!proto.api.RoomResponse>} value
 * @return {!proto.api.RoomResponseArray} returns this
*/
proto.api.RoomResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.RoomResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.RoomResponse}
 */
proto.api.RoomResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.RoomResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.RoomResponseArray} returns this
 */
proto.api.RoomResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.QueryRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.QueryRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.QueryRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.QueryRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    connectionName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    searchPattern: jspb.Message.getFieldWithDefault(msg, 3, ""),
    listingOnly: jspb.Message.getBooleanFieldWithDefault(msg, 4, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.QueryRequest}
 */
proto.api.QueryRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.QueryRequest;
  return proto.api.QueryRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.QueryRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.QueryRequest}
 */
proto.api.QueryRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setConnectionName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setSearchPattern(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setListingOnly(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.QueryRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.QueryRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.QueryRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.QueryRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getConnectionName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getSearchPattern();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getListingOnly();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.QueryRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.QueryRequest} returns this
 */
proto.api.QueryRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string connection_name = 2;
 * @return {string}
 */
proto.api.QueryRequest.prototype.getConnectionName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.QueryRequest} returns this
 */
proto.api.QueryRequest.prototype.setConnectionName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string search_pattern = 3;
 * @return {string}
 */
proto.api.QueryRequest.prototype.getSearchPattern = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.QueryRequest} returns this
 */
proto.api.QueryRequest.prototype.setSearchPattern = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional bool listing_only = 4;
 * @return {boolean}
 */
proto.api.QueryRequest.prototype.getListingOnly = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.QueryRequest} returns this
 */
proto.api.QueryRequest.prototype.setListingOnly = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VMQueryRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VMQueryRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VMQueryRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VMQueryRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    pathFilter: jspb.Message.getFieldWithDefault(msg, 3, ""),
    hostFilter: jspb.Message.getFieldWithDefault(msg, 4, ""),
    listingOnly: jspb.Message.getBooleanFieldWithDefault(msg, 5, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VMQueryRequest}
 */
proto.api.VMQueryRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VMQueryRequest;
  return proto.api.VMQueryRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VMQueryRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VMQueryRequest}
 */
proto.api.VMQueryRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPathFilter(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostFilter(value);
      break;
    case 5:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setListingOnly(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VMQueryRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VMQueryRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VMQueryRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VMQueryRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPathFilter();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getHostFilter();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getListingOnly();
  if (f) {
    writer.writeBool(
      5,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.VMQueryRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VMQueryRequest} returns this
 */
proto.api.VMQueryRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.VMQueryRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VMQueryRequest} returns this
 */
proto.api.VMQueryRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string path_filter = 3;
 * @return {string}
 */
proto.api.VMQueryRequest.prototype.getPathFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VMQueryRequest} returns this
 */
proto.api.VMQueryRequest.prototype.setPathFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string host_filter = 4;
 * @return {string}
 */
proto.api.VMQueryRequest.prototype.getHostFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VMQueryRequest} returns this
 */
proto.api.VMQueryRequest.prototype.setHostFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional bool listing_only = 5;
 * @return {boolean}
 */
proto.api.VMQueryRequest.prototype.getListingOnly = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 5, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VMQueryRequest} returns this
 */
proto.api.VMQueryRequest.prototype.setListingOnly = function(value) {
  return jspb.Message.setProto3BooleanField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HVQueryRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HVQueryRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HVQueryRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HVQueryRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    pathFilter: jspb.Message.getFieldWithDefault(msg, 3, ""),
    hostFilter: jspb.Message.getFieldWithDefault(msg, 4, ""),
    listingOnly: jspb.Message.getBooleanFieldWithDefault(msg, 5, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HVQueryRequest}
 */
proto.api.HVQueryRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HVQueryRequest;
  return proto.api.HVQueryRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HVQueryRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HVQueryRequest}
 */
proto.api.HVQueryRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPathFilter(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostFilter(value);
      break;
    case 5:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setListingOnly(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HVQueryRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HVQueryRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HVQueryRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HVQueryRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPathFilter();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getHostFilter();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getListingOnly();
  if (f) {
    writer.writeBool(
      5,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.HVQueryRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HVQueryRequest} returns this
 */
proto.api.HVQueryRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.HVQueryRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HVQueryRequest} returns this
 */
proto.api.HVQueryRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string path_filter = 3;
 * @return {string}
 */
proto.api.HVQueryRequest.prototype.getPathFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HVQueryRequest} returns this
 */
proto.api.HVQueryRequest.prototype.setPathFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string host_filter = 4;
 * @return {string}
 */
proto.api.HVQueryRequest.prototype.getHostFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HVQueryRequest} returns this
 */
proto.api.HVQueryRequest.prototype.setHostFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional bool listing_only = 5;
 * @return {boolean}
 */
proto.api.HVQueryRequest.prototype.getListingOnly = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 5, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.HVQueryRequest} returns this
 */
proto.api.HVQueryRequest.prototype.setListingOnly = function(value) {
  return jspb.Message.setProto3BooleanField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DatastoreReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DatastoreReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DatastoreReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    hypervisorPathFilter: jspb.Message.getFieldWithDefault(msg, 3, ""),
    datastorePathFilter: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DatastoreReportRequest}
 */
proto.api.DatastoreReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DatastoreReportRequest;
  return proto.api.DatastoreReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DatastoreReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DatastoreReportRequest}
 */
proto.api.DatastoreReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorPathFilter(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastorePathFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DatastoreReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.DatastoreReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.DatastoreReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getHypervisorPathFilter();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDatastorePathFilter();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.DatastoreReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportRequest} returns this
 */
proto.api.DatastoreReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.DatastoreReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportRequest} returns this
 */
proto.api.DatastoreReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string hypervisor_path_filter = 3;
 * @return {string}
 */
proto.api.DatastoreReportRequest.prototype.getHypervisorPathFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportRequest} returns this
 */
proto.api.DatastoreReportRequest.prototype.setHypervisorPathFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string datastore_path_filter = 4;
 * @return {string}
 */
proto.api.DatastoreReportRequest.prototype.getDatastorePathFilter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportRequest} returns this
 */
proto.api.DatastoreReportRequest.prototype.setDatastorePathFilter = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.DatastoreReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.DatastoreReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.DatastoreReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    url: jspb.Message.getFieldWithDefault(msg, 2, ""),
    datastoreName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    datastorePath: jspb.Message.getFieldWithDefault(msg, 4, ""),
    datastoreMoRef: jspb.Message.getFieldWithDefault(msg, 5, ""),
    capacityBytes: jspb.Message.getFieldWithDefault(msg, 6, 0),
    capacityRemainingBytes: jspb.Message.getFieldWithDefault(msg, 7, 0),
    type: jspb.Message.getFieldWithDefault(msg, 8, ""),
    hypervisorPath: jspb.Message.getFieldWithDefault(msg, 9, ""),
    hypervisorMoRef: jspb.Message.getFieldWithDefault(msg, 10, ""),
    hypervisorMount: jspb.Message.getFieldWithDefault(msg, 11, ""),
    vmHostName: jspb.Message.getFieldWithDefault(msg, 12, ""),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 13, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 14, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 15, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 16, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.DatastoreReportResponse}
 */
proto.api.DatastoreReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.DatastoreReportResponse;
  return proto.api.DatastoreReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.DatastoreReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.DatastoreReportResponse}
 */
proto.api.DatastoreReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastoreName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastorePath(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastoreMoRef(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCapacityBytes(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCapacityRemainingBytes(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorPath(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorMoRef(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorMount(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmHostName(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 16:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.DatastoreReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.DatastoreReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.DatastoreReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.DatastoreReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUrl();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDatastoreName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDatastorePath();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getDatastoreMoRef();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getCapacityBytes();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getCapacityRemainingBytes();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getHypervisorPath();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getHypervisorMoRef();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getHypervisorMount();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getVmHostName();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      16,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string url = 2;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string datastore_name = 3;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getDatastoreName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setDatastoreName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string datastore_path = 4;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getDatastorePath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setDatastorePath = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string datastore_mo_ref = 5;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getDatastoreMoRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setDatastoreMoRef = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int64 capacity_bytes = 6;
 * @return {number}
 */
proto.api.DatastoreReportResponse.prototype.getCapacityBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setCapacityBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 capacity_remaining_bytes = 7;
 * @return {number}
 */
proto.api.DatastoreReportResponse.prototype.getCapacityRemainingBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setCapacityRemainingBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional string type = 8;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string hypervisor_path = 9;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getHypervisorPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setHypervisorPath = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string hypervisor_mo_ref = 10;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getHypervisorMoRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setHypervisorMoRef = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string hypervisor_mount = 11;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getHypervisorMount = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setHypervisorMount = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string vm_host_name = 12;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getVmHostName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setVmHostName = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string ui_msg_info = 13;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string ui_msg_warning = 14;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string ui_msg_error = 15;
 * @return {string}
 */
proto.api.DatastoreReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional bool ui_control_is_preview = 16;
 * @return {boolean}
 */
proto.api.DatastoreReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 16, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.DatastoreReportResponse} returns this
 */
proto.api.DatastoreReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 16, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    nfsHost: jspb.Message.getFieldWithDefault(msg, 3, ""),
    nfsPath: jspb.Message.getFieldWithDefault(msg, 4, ""),
    vmHost: jspb.Message.getFieldWithDefault(msg, 5, ""),
    vmPath: jspb.Message.getFieldWithDefault(msg, 6, ""),
    vmMountPoint: jspb.Message.getFieldWithDefault(msg, 7, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsReportRequest}
 */
proto.api.NfsReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsReportRequest;
  return proto.api.NfsReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsReportRequest}
 */
proto.api.NfsReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsHost(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsPath(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmHost(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmPath(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmMountPoint(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getNfsHost();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getNfsPath();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getVmHost();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getVmPath();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getVmMountPoint();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string nfs_host = 3;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getNfsHost = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setNfsHost = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string nfs_path = 4;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getNfsPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setNfsPath = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string vm_host = 5;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getVmHost = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setVmHost = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string vm_path = 6;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getVmPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setVmPath = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string vm_mount_point = 7;
 * @return {string}
 */
proto.api.NfsReportRequest.prototype.getVmMountPoint = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportRequest} returns this
 */
proto.api.NfsReportRequest.prototype.setVmMountPoint = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.NfsReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.NfsReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.NfsReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    nfsHost: jspb.Message.getFieldWithDefault(msg, 2, ""),
    nfsPath: jspb.Message.getFieldWithDefault(msg, 3, ""),
    nfsType: jspb.Message.getFieldWithDefault(msg, 4, ""),
    nfsClusterName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    nfsIsilonLnn: jspb.Message.getFieldWithDefault(msg, 6, 0),
    nfsCapacityBytes: jspb.Message.getFieldWithDefault(msg, 7, 0),
    nfsUsedBytes: jspb.Message.getFieldWithDefault(msg, 8, 0),
    vmHost: jspb.Message.getFieldWithDefault(msg, 9, ""),
    vmPath: jspb.Message.getFieldWithDefault(msg, 10, ""),
    vmMountPoint: jspb.Message.getFieldWithDefault(msg, 11, ""),
    vmMountUser: jspb.Message.getFieldWithDefault(msg, 12, ""),
    vmMountGroup: jspb.Message.getFieldWithDefault(msg, 13, ""),
    vmMountPermissions: jspb.Message.getFieldWithDefault(msg, 14, ""),
    fstab: jspb.Message.getFieldWithDefault(msg, 15, ""),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 16, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 17, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 18, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 19, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.NfsReportResponse}
 */
proto.api.NfsReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.NfsReportResponse;
  return proto.api.NfsReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.NfsReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.NfsReportResponse}
 */
proto.api.NfsReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsHost(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsPath(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsType(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setNfsClusterName(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNfsIsilonLnn(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNfsCapacityBytes(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setNfsUsedBytes(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmHost(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmPath(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmMountPoint(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmMountUser(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmMountGroup(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmMountPermissions(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setFstab(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 19:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.NfsReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.NfsReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.NfsReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.NfsReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getNfsHost();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getNfsPath();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getNfsType();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getNfsClusterName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getNfsIsilonLnn();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getNfsCapacityBytes();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getNfsUsedBytes();
  if (f !== 0) {
    writer.writeInt64(
      8,
      f
    );
  }
  f = message.getVmHost();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getVmPath();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getVmMountPoint();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getVmMountUser();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getVmMountGroup();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getVmMountPermissions();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getFstab();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      18,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      19,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string nfs_host = 2;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getNfsHost = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsHost = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string nfs_path = 3;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getNfsPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsPath = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string nfs_type = 4;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getNfsType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsType = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string nfs_cluster_name = 5;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getNfsClusterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsClusterName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int64 nfs_isilon_lnn = 6;
 * @return {number}
 */
proto.api.NfsReportResponse.prototype.getNfsIsilonLnn = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsIsilonLnn = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 nfs_capacity_bytes = 7;
 * @return {number}
 */
proto.api.NfsReportResponse.prototype.getNfsCapacityBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsCapacityBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int64 nfs_used_bytes = 8;
 * @return {number}
 */
proto.api.NfsReportResponse.prototype.getNfsUsedBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setNfsUsedBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional string vm_host = 9;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmHost = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmHost = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string vm_path = 10;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmPath = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string vm_mount_point = 11;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmMountPoint = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmMountPoint = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string vm_mount_user = 12;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmMountUser = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmMountUser = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string vm_mount_group = 13;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmMountGroup = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmMountGroup = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string vm_mount_permissions = 14;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getVmMountPermissions = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setVmMountPermissions = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string fstab = 15;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getFstab = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setFstab = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string ui_msg_info = 16;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string ui_msg_warning = 17;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * optional string ui_msg_error = 18;
 * @return {string}
 */
proto.api.NfsReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 18, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 18, value);
};


/**
 * optional bool ui_control_is_preview = 19;
 * @return {boolean}
 */
proto.api.NfsReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 19, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.NfsReportResponse} returns this
 */
proto.api.NfsReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 19, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmStorageReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmStorageReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmStorageReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmStorageReportRequest}
 */
proto.api.VmStorageReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmStorageReportRequest;
  return proto.api.VmStorageReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmStorageReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmStorageReportRequest}
 */
proto.api.VmStorageReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmStorageReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmStorageReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmStorageReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.VmStorageReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportRequest} returns this
 */
proto.api.VmStorageReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.VmStorageReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportRequest} returns this
 */
proto.api.VmStorageReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmStorageReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmStorageReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmStorageReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    sizeTb: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 4, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 5, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 6, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 7, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmStorageReportResponse}
 */
proto.api.VmStorageReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmStorageReportResponse;
  return proto.api.VmStorageReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmStorageReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmStorageReportResponse}
 */
proto.api.VmStorageReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSizeTb(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 7:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmStorageReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmStorageReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmStorageReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getSizeTb();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      7,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.VmStorageReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.api.VmStorageReportResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional double size_tb = 3;
 * @return {number}
 */
proto.api.VmStorageReportResponse.prototype.getSizeTb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setSizeTb = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * optional string ui_msg_info = 4;
 * @return {string}
 */
proto.api.VmStorageReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string ui_msg_warning = 5;
 * @return {string}
 */
proto.api.VmStorageReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string ui_msg_error = 6;
 * @return {string}
 */
proto.api.VmStorageReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional bool ui_control_is_preview = 7;
 * @return {boolean}
 */
proto.api.VmStorageReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 7, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VmStorageReportResponse} returns this
 */
proto.api.VmStorageReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 7, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmStorageUploadRec.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmStorageUploadRec.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmStorageUploadRec} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadRec.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    sizeTb: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmStorageUploadRec}
 */
proto.api.VmStorageUploadRec.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmStorageUploadRec;
  return proto.api.VmStorageUploadRec.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmStorageUploadRec} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmStorageUploadRec}
 */
proto.api.VmStorageUploadRec.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSizeTb(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmStorageUploadRec.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmStorageUploadRec.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmStorageUploadRec} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadRec.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSizeTb();
  if (f !== 0.0) {
    writer.writeDouble(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.VmStorageUploadRec.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageUploadRec} returns this
 */
proto.api.VmStorageUploadRec.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional double size_tb = 2;
 * @return {number}
 */
proto.api.VmStorageUploadRec.prototype.getSizeTb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.VmStorageUploadRec} returns this
 */
proto.api.VmStorageUploadRec.prototype.setSizeTb = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VmStorageUploadRequest.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmStorageUploadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmStorageUploadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmStorageUploadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    appendMode: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    recordsList: jspb.Message.toObjectList(msg.getRecordsList(),
    proto.api.VmStorageUploadRec.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmStorageUploadRequest}
 */
proto.api.VmStorageUploadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmStorageUploadRequest;
  return proto.api.VmStorageUploadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmStorageUploadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmStorageUploadRequest}
 */
proto.api.VmStorageUploadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setAppendMode(value);
      break;
    case 4:
      var value = new proto.api.VmStorageUploadRec;
      reader.readMessage(value,proto.api.VmStorageUploadRec.deserializeBinaryFromReader);
      msg.addRecords(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmStorageUploadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmStorageUploadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmStorageUploadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getAppendMode();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getRecordsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.api.VmStorageUploadRec.serializeBinaryToWriter
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.VmStorageUploadRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageUploadRequest} returns this
 */
proto.api.VmStorageUploadRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.VmStorageUploadRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageUploadRequest} returns this
 */
proto.api.VmStorageUploadRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool append_mode = 3;
 * @return {boolean}
 */
proto.api.VmStorageUploadRequest.prototype.getAppendMode = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.VmStorageUploadRequest} returns this
 */
proto.api.VmStorageUploadRequest.prototype.setAppendMode = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * repeated VmStorageUploadRec records = 4;
 * @return {!Array<!proto.api.VmStorageUploadRec>}
 */
proto.api.VmStorageUploadRequest.prototype.getRecordsList = function() {
  return /** @type{!Array<!proto.api.VmStorageUploadRec>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VmStorageUploadRec, 4));
};


/**
 * @param {!Array<!proto.api.VmStorageUploadRec>} value
 * @return {!proto.api.VmStorageUploadRequest} returns this
*/
proto.api.VmStorageUploadRequest.prototype.setRecordsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.api.VmStorageUploadRec=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VmStorageUploadRec}
 */
proto.api.VmStorageUploadRequest.prototype.addRecords = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.api.VmStorageUploadRec, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmStorageUploadRequest} returns this
 */
proto.api.VmStorageUploadRequest.prototype.clearRecordsList = function() {
  return this.setRecordsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VmStorageUploadResponse.repeatedFields_ = [3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmStorageUploadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmStorageUploadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmStorageUploadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, ""),
    recordsUploaded: jspb.Message.getFieldWithDefault(msg, 2, 0),
    warningsList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    errorsList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmStorageUploadResponse}
 */
proto.api.VmStorageUploadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmStorageUploadResponse;
  return proto.api.VmStorageUploadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmStorageUploadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmStorageUploadResponse}
 */
proto.api.VmStorageUploadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRecordsUploaded(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarnings(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addErrors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmStorageUploadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmStorageUploadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmStorageUploadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmStorageUploadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRecordsUploaded();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.VmStorageUploadResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 records_uploaded = 2;
 * @return {number}
 */
proto.api.VmStorageUploadResponse.prototype.getRecordsUploaded = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.setRecordsUploaded = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * repeated string warnings = 3;
 * @return {!Array<string>}
 */
proto.api.VmStorageUploadResponse.prototype.getWarningsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.addWarnings = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated string errors = 4;
 * @return {!Array<string>}
 */
proto.api.VmStorageUploadResponse.prototype.getErrorsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.addErrors = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmStorageUploadResponse} returns this
 */
proto.api.VmStorageUploadResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.IpRangeReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.IpRangeReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.IpRangeReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.IpRangeReportRequest}
 */
proto.api.IpRangeReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.IpRangeReportRequest;
  return proto.api.IpRangeReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.IpRangeReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.IpRangeReportRequest}
 */
proto.api.IpRangeReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.IpRangeReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.IpRangeReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.IpRangeReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.IpRangeReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportRequest} returns this
 */
proto.api.IpRangeReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.IpRangeReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportRequest} returns this
 */
proto.api.IpRangeReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.IpRangeReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.IpRangeReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.IpRangeReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    ipRangeRecord: (f = msg.getIpRangeRecord()) && proto.api.IpRangeRec.toObject(includeInstance, f),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 3, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 4, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 5, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 6, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.IpRangeReportResponse}
 */
proto.api.IpRangeReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.IpRangeReportResponse;
  return proto.api.IpRangeReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.IpRangeReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.IpRangeReportResponse}
 */
proto.api.IpRangeReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = new proto.api.IpRangeRec;
      reader.readMessage(value,proto.api.IpRangeRec.deserializeBinaryFromReader);
      msg.setIpRangeRecord(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.IpRangeReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.IpRangeReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.IpRangeReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getIpRangeRecord();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.IpRangeRec.serializeBinaryToWriter
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.IpRangeReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional IpRangeRec ip_range_record = 2;
 * @return {?proto.api.IpRangeRec}
 */
proto.api.IpRangeReportResponse.prototype.getIpRangeRecord = function() {
  return /** @type{?proto.api.IpRangeRec} */ (
    jspb.Message.getWrapperField(this, proto.api.IpRangeRec, 2));
};


/**
 * @param {?proto.api.IpRangeRec|undefined} value
 * @return {!proto.api.IpRangeReportResponse} returns this
*/
proto.api.IpRangeReportResponse.prototype.setIpRangeRecord = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.clearIpRangeRecord = function() {
  return this.setIpRangeRecord(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.IpRangeReportResponse.prototype.hasIpRangeRecord = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string ui_msg_info = 3;
 * @return {string}
 */
proto.api.IpRangeReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string ui_msg_warning = 4;
 * @return {string}
 */
proto.api.IpRangeReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string ui_msg_error = 5;
 * @return {string}
 */
proto.api.IpRangeReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional bool ui_control_is_preview = 6;
 * @return {boolean}
 */
proto.api.IpRangeReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.IpRangeReportResponse} returns this
 */
proto.api.IpRangeReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.IpRangeRec.prototype.toObject = function(opt_includeInstance) {
  return proto.api.IpRangeRec.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.IpRangeRec} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeRec.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    ipRange: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vlan: jspb.Message.getFieldWithDefault(msg, 3, ""),
    notes: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.IpRangeRec}
 */
proto.api.IpRangeRec.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.IpRangeRec;
  return proto.api.IpRangeRec.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.IpRangeRec} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.IpRangeRec}
 */
proto.api.IpRangeRec.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setIpRange(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVlan(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNotes(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.IpRangeRec.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.IpRangeRec.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.IpRangeRec} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeRec.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getIpRange();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVlan();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getNotes();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.IpRangeRec.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeRec} returns this
 */
proto.api.IpRangeRec.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string ip_range = 2;
 * @return {string}
 */
proto.api.IpRangeRec.prototype.getIpRange = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeRec} returns this
 */
proto.api.IpRangeRec.prototype.setIpRange = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vlan = 3;
 * @return {string}
 */
proto.api.IpRangeRec.prototype.getVlan = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeRec} returns this
 */
proto.api.IpRangeRec.prototype.setVlan = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string notes = 4;
 * @return {string}
 */
proto.api.IpRangeRec.prototype.getNotes = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeRec} returns this
 */
proto.api.IpRangeRec.prototype.setNotes = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.IpRangeUploadRequest.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.IpRangeUploadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.IpRangeUploadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.IpRangeUploadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeUploadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    appendMode: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    recordsList: jspb.Message.toObjectList(msg.getRecordsList(),
    proto.api.IpRangeRec.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.IpRangeUploadRequest}
 */
proto.api.IpRangeUploadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.IpRangeUploadRequest;
  return proto.api.IpRangeUploadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.IpRangeUploadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.IpRangeUploadRequest}
 */
proto.api.IpRangeUploadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setAppendMode(value);
      break;
    case 4:
      var value = new proto.api.IpRangeRec;
      reader.readMessage(value,proto.api.IpRangeRec.deserializeBinaryFromReader);
      msg.addRecords(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.IpRangeUploadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.IpRangeUploadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.IpRangeUploadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeUploadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getAppendMode();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getRecordsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.api.IpRangeRec.serializeBinaryToWriter
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.IpRangeUploadRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeUploadRequest} returns this
 */
proto.api.IpRangeUploadRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.IpRangeUploadRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeUploadRequest} returns this
 */
proto.api.IpRangeUploadRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool append_mode = 3;
 * @return {boolean}
 */
proto.api.IpRangeUploadRequest.prototype.getAppendMode = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.IpRangeUploadRequest} returns this
 */
proto.api.IpRangeUploadRequest.prototype.setAppendMode = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * repeated IpRangeRec records = 4;
 * @return {!Array<!proto.api.IpRangeRec>}
 */
proto.api.IpRangeUploadRequest.prototype.getRecordsList = function() {
  return /** @type{!Array<!proto.api.IpRangeRec>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.IpRangeRec, 4));
};


/**
 * @param {!Array<!proto.api.IpRangeRec>} value
 * @return {!proto.api.IpRangeUploadRequest} returns this
*/
proto.api.IpRangeUploadRequest.prototype.setRecordsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.api.IpRangeRec=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.IpRangeRec}
 */
proto.api.IpRangeUploadRequest.prototype.addRecords = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.api.IpRangeRec, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.IpRangeUploadRequest} returns this
 */
proto.api.IpRangeUploadRequest.prototype.clearRecordsList = function() {
  return this.setRecordsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.IpRangeUploadResponse.repeatedFields_ = [3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.IpRangeUploadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.IpRangeUploadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.IpRangeUploadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeUploadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, ""),
    recordsUploaded: jspb.Message.getFieldWithDefault(msg, 2, 0),
    warningsList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    errorsList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.IpRangeUploadResponse}
 */
proto.api.IpRangeUploadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.IpRangeUploadResponse;
  return proto.api.IpRangeUploadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.IpRangeUploadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.IpRangeUploadResponse}
 */
proto.api.IpRangeUploadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRecordsUploaded(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarnings(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addErrors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.IpRangeUploadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.IpRangeUploadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.IpRangeUploadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.IpRangeUploadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRecordsUploaded();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.IpRangeUploadResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 records_uploaded = 2;
 * @return {number}
 */
proto.api.IpRangeUploadResponse.prototype.getRecordsUploaded = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.setRecordsUploaded = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * repeated string warnings = 3;
 * @return {!Array<string>}
 */
proto.api.IpRangeUploadResponse.prototype.getWarningsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.addWarnings = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated string errors = 4;
 * @return {!Array<string>}
 */
proto.api.IpRangeUploadResponse.prototype.getErrorsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.addErrors = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.IpRangeUploadResponse} returns this
 */
proto.api.IpRangeUploadResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StorageSummaryReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StorageSummaryReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StorageSummaryReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StorageSummaryReportRequest}
 */
proto.api.StorageSummaryReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StorageSummaryReportRequest;
  return proto.api.StorageSummaryReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StorageSummaryReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StorageSummaryReportRequest}
 */
proto.api.StorageSummaryReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StorageSummaryReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.StorageSummaryReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.StorageSummaryReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.StorageSummaryReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportRequest} returns this
 */
proto.api.StorageSummaryReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.StorageSummaryReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportRequest} returns this
 */
proto.api.StorageSummaryReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StorageSummaryReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StorageSummaryReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StorageSummaryReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    storageSummaryRecord: (f = msg.getStorageSummaryRecord()) && proto.api.StorageSummaryRec.toObject(includeInstance, f),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 3, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 4, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 5, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 6, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StorageSummaryReportResponse}
 */
proto.api.StorageSummaryReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StorageSummaryReportResponse;
  return proto.api.StorageSummaryReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StorageSummaryReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StorageSummaryReportResponse}
 */
proto.api.StorageSummaryReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = new proto.api.StorageSummaryRec;
      reader.readMessage(value,proto.api.StorageSummaryRec.deserializeBinaryFromReader);
      msg.setStorageSummaryRecord(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StorageSummaryReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.StorageSummaryReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.StorageSummaryReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStorageSummaryRecord();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.StorageSummaryRec.serializeBinaryToWriter
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.StorageSummaryReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional StorageSummaryRec storage_summary_record = 2;
 * @return {?proto.api.StorageSummaryRec}
 */
proto.api.StorageSummaryReportResponse.prototype.getStorageSummaryRecord = function() {
  return /** @type{?proto.api.StorageSummaryRec} */ (
    jspb.Message.getWrapperField(this, proto.api.StorageSummaryRec, 2));
};


/**
 * @param {?proto.api.StorageSummaryRec|undefined} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
*/
proto.api.StorageSummaryReportResponse.prototype.setStorageSummaryRecord = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.clearStorageSummaryRecord = function() {
  return this.setStorageSummaryRecord(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.StorageSummaryReportResponse.prototype.hasStorageSummaryRecord = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string ui_msg_info = 3;
 * @return {string}
 */
proto.api.StorageSummaryReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string ui_msg_warning = 4;
 * @return {string}
 */
proto.api.StorageSummaryReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string ui_msg_error = 5;
 * @return {string}
 */
proto.api.StorageSummaryReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional bool ui_control_is_preview = 6;
 * @return {boolean}
 */
proto.api.StorageSummaryReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.StorageSummaryReportResponse} returns this
 */
proto.api.StorageSummaryReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StorageSummaryRec.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StorageSummaryRec.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StorageSummaryRec} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryRec.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    cidr: jspb.Message.getFieldWithDefault(msg, 2, ""),
    ports: jspb.Message.getFieldWithDefault(msg, 3, ""),
    username: jspb.Message.getFieldWithDefault(msg, 4, ""),
    password: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StorageSummaryRec}
 */
proto.api.StorageSummaryRec.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StorageSummaryRec;
  return proto.api.StorageSummaryRec.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StorageSummaryRec} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StorageSummaryRec}
 */
proto.api.StorageSummaryRec.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCidr(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPorts(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StorageSummaryRec.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.StorageSummaryRec.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.StorageSummaryRec} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryRec.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCidr();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPorts();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.StorageSummaryRec.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryRec} returns this
 */
proto.api.StorageSummaryRec.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string cidr = 2;
 * @return {string}
 */
proto.api.StorageSummaryRec.prototype.getCidr = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryRec} returns this
 */
proto.api.StorageSummaryRec.prototype.setCidr = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string ports = 3;
 * @return {string}
 */
proto.api.StorageSummaryRec.prototype.getPorts = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryRec} returns this
 */
proto.api.StorageSummaryRec.prototype.setPorts = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string username = 4;
 * @return {string}
 */
proto.api.StorageSummaryRec.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryRec} returns this
 */
proto.api.StorageSummaryRec.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string password = 5;
 * @return {string}
 */
proto.api.StorageSummaryRec.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryRec} returns this
 */
proto.api.StorageSummaryRec.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.StorageSummaryUploadRequest.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StorageSummaryUploadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StorageSummaryUploadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StorageSummaryUploadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryUploadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    appendMode: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    recordsList: jspb.Message.toObjectList(msg.getRecordsList(),
    proto.api.StorageSummaryRec.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StorageSummaryUploadRequest}
 */
proto.api.StorageSummaryUploadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StorageSummaryUploadRequest;
  return proto.api.StorageSummaryUploadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StorageSummaryUploadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StorageSummaryUploadRequest}
 */
proto.api.StorageSummaryUploadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setAppendMode(value);
      break;
    case 4:
      var value = new proto.api.StorageSummaryRec;
      reader.readMessage(value,proto.api.StorageSummaryRec.deserializeBinaryFromReader);
      msg.addRecords(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StorageSummaryUploadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.StorageSummaryUploadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.StorageSummaryUploadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryUploadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getAppendMode();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getRecordsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.api.StorageSummaryRec.serializeBinaryToWriter
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.StorageSummaryUploadRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryUploadRequest} returns this
 */
proto.api.StorageSummaryUploadRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.StorageSummaryUploadRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryUploadRequest} returns this
 */
proto.api.StorageSummaryUploadRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool append_mode = 3;
 * @return {boolean}
 */
proto.api.StorageSummaryUploadRequest.prototype.getAppendMode = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.StorageSummaryUploadRequest} returns this
 */
proto.api.StorageSummaryUploadRequest.prototype.setAppendMode = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * repeated StorageSummaryRec records = 4;
 * @return {!Array<!proto.api.StorageSummaryRec>}
 */
proto.api.StorageSummaryUploadRequest.prototype.getRecordsList = function() {
  return /** @type{!Array<!proto.api.StorageSummaryRec>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.StorageSummaryRec, 4));
};


/**
 * @param {!Array<!proto.api.StorageSummaryRec>} value
 * @return {!proto.api.StorageSummaryUploadRequest} returns this
*/
proto.api.StorageSummaryUploadRequest.prototype.setRecordsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.api.StorageSummaryRec=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.StorageSummaryRec}
 */
proto.api.StorageSummaryUploadRequest.prototype.addRecords = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.api.StorageSummaryRec, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.StorageSummaryUploadRequest} returns this
 */
proto.api.StorageSummaryUploadRequest.prototype.clearRecordsList = function() {
  return this.setRecordsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.StorageSummaryUploadResponse.repeatedFields_ = [3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.StorageSummaryUploadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.StorageSummaryUploadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.StorageSummaryUploadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryUploadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, ""),
    recordsUploaded: jspb.Message.getFieldWithDefault(msg, 2, 0),
    warningsList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    errorsList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.StorageSummaryUploadResponse}
 */
proto.api.StorageSummaryUploadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.StorageSummaryUploadResponse;
  return proto.api.StorageSummaryUploadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.StorageSummaryUploadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.StorageSummaryUploadResponse}
 */
proto.api.StorageSummaryUploadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRecordsUploaded(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarnings(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addErrors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.StorageSummaryUploadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.StorageSummaryUploadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.StorageSummaryUploadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.StorageSummaryUploadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRecordsUploaded();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.api.StorageSummaryUploadResponse.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 records_uploaded = 2;
 * @return {number}
 */
proto.api.StorageSummaryUploadResponse.prototype.getRecordsUploaded = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.setRecordsUploaded = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * repeated string warnings = 3;
 * @return {!Array<string>}
 */
proto.api.StorageSummaryUploadResponse.prototype.getWarningsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.addWarnings = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated string errors = 4;
 * @return {!Array<string>}
 */
proto.api.StorageSummaryUploadResponse.prototype.getErrorsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.addErrors = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.StorageSummaryUploadResponse} returns this
 */
proto.api.StorageSummaryUploadResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SharedStorageReportRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SharedStorageReportRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SharedStorageReportRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SharedStorageReportRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    storageType: jspb.Message.getFieldWithDefault(msg, 3, ""),
    storageName: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SharedStorageReportRequest}
 */
proto.api.SharedStorageReportRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SharedStorageReportRequest;
  return proto.api.SharedStorageReportRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SharedStorageReportRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SharedStorageReportRequest}
 */
proto.api.SharedStorageReportRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SharedStorageReportRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SharedStorageReportRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SharedStorageReportRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SharedStorageReportRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStorageType();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getStorageName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.SharedStorageReportRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportRequest} returns this
 */
proto.api.SharedStorageReportRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.SharedStorageReportRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportRequest} returns this
 */
proto.api.SharedStorageReportRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string storage_type = 3;
 * @return {string}
 */
proto.api.SharedStorageReportRequest.prototype.getStorageType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportRequest} returns this
 */
proto.api.SharedStorageReportRequest.prototype.setStorageType = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string storage_name = 4;
 * @return {string}
 */
proto.api.SharedStorageReportRequest.prototype.getStorageName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportRequest} returns this
 */
proto.api.SharedStorageReportRequest.prototype.setStorageName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.SharedStorageReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.SharedStorageReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.SharedStorageReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SharedStorageReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    storageId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    storageName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    storageType: jspb.Message.getFieldWithDefault(msg, 4, ""),
    storageClusterName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    storageCapacityBytes: jspb.Message.getFieldWithDefault(msg, 6, 0),
    storageUsedBytes: jspb.Message.getFieldWithDefault(msg, 7, 0),
    uiMsgInfo: jspb.Message.getFieldWithDefault(msg, 8, ""),
    uiMsgWarning: jspb.Message.getFieldWithDefault(msg, 9, ""),
    uiMsgError: jspb.Message.getFieldWithDefault(msg, 10, ""),
    uiControlIsPreview: jspb.Message.getBooleanFieldWithDefault(msg, 11, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.SharedStorageReportResponse}
 */
proto.api.SharedStorageReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.SharedStorageReportResponse;
  return proto.api.SharedStorageReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.SharedStorageReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.SharedStorageReportResponse}
 */
proto.api.SharedStorageReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageId(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageType(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setStorageClusterName(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStorageCapacityBytes(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setStorageUsedBytes(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgInfo(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgWarning(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setUiMsgError(value);
      break;
    case 11:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUiControlIsPreview(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.SharedStorageReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.SharedStorageReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.SharedStorageReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.SharedStorageReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStorageId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStorageName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getStorageType();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getStorageClusterName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getStorageCapacityBytes();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getStorageUsedBytes();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getUiMsgInfo();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getUiMsgWarning();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getUiMsgError();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getUiControlIsPreview();
  if (f) {
    writer.writeBool(
      11,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string storage_id = 2;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string storage_name = 3;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string storage_type = 4;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageType = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string storage_cluster_name = 5;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageClusterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageClusterName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int64 storage_capacity_bytes = 6;
 * @return {number}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageCapacityBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageCapacityBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int64 storage_used_bytes = 7;
 * @return {number}
 */
proto.api.SharedStorageReportResponse.prototype.getStorageUsedBytes = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setStorageUsedBytes = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional string ui_msg_info = 8;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getUiMsgInfo = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setUiMsgInfo = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string ui_msg_warning = 9;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getUiMsgWarning = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setUiMsgWarning = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string ui_msg_error = 10;
 * @return {string}
 */
proto.api.SharedStorageReportResponse.prototype.getUiMsgError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setUiMsgError = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional bool ui_control_is_preview = 11;
 * @return {boolean}
 */
proto.api.SharedStorageReportResponse.prototype.getUiControlIsPreview = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 11, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.SharedStorageReportResponse} returns this
 */
proto.api.SharedStorageReportResponse.prototype.setUiControlIsPreview = function(value) {
  return jspb.Message.setProto3BooleanField(this, 11, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.LoginRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.LoginRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.LoginRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LoginRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    username: jspb.Message.getFieldWithDefault(msg, 1, ""),
    password: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.LoginRequest}
 */
proto.api.LoginRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.LoginRequest;
  return proto.api.LoginRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.LoginRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.LoginRequest}
 */
proto.api.LoginRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.LoginRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.LoginRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.LoginRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LoginRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string username = 1;
 * @return {string}
 */
proto.api.LoginRequest.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.LoginRequest} returns this
 */
proto.api.LoginRequest.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string password = 2;
 * @return {string}
 */
proto.api.LoginRequest.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.LoginRequest} returns this
 */
proto.api.LoginRequest.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.LoginResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.LoginResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.LoginResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LoginResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    token: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.LoginResponse}
 */
proto.api.LoginResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.LoginResponse;
  return proto.api.LoginResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.LoginResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.LoginResponse}
 */
proto.api.LoginResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.LoginResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.LoginResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.LoginResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.LoginResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string token = 1;
 * @return {string}
 */
proto.api.LoginResponse.prototype.getToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.LoginResponse} returns this
 */
proto.api.LoginResponse.prototype.setToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.ReadyResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.ReadyResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.ReadyResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ReadyResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    readyState: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.ReadyResponse}
 */
proto.api.ReadyResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.ReadyResponse;
  return proto.api.ReadyResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.ReadyResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.ReadyResponse}
 */
proto.api.ReadyResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setReadyState(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.ReadyResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.ReadyResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.ReadyResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.ReadyResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getReadyState();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string ready_state = 1;
 * @return {string}
 */
proto.api.ReadyResponse.prototype.getReadyState = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.ReadyResponse} returns this
 */
proto.api.ReadyResponse.prototype.setReadyState = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.EchoStruct.prototype.toObject = function(opt_includeInstance) {
  return proto.api.EchoStruct.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.EchoStruct} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EchoStruct.toObject = function(includeInstance, msg) {
  var f, obj = {
    message: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.EchoStruct}
 */
proto.api.EchoStruct.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.EchoStruct;
  return proto.api.EchoStruct.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.EchoStruct} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.EchoStruct}
 */
proto.api.EchoStruct.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.EchoStruct.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.EchoStruct.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.EchoStruct} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EchoStruct.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string message = 1;
 * @return {string}
 */
proto.api.EchoStruct.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.EchoStruct} returns this
 */
proto.api.EchoStruct.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.EnvironmentNameResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.EnvironmentNameResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.EnvironmentNameResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentNameResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    environmentname: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.EnvironmentNameResponse}
 */
proto.api.EnvironmentNameResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.EnvironmentNameResponse;
  return proto.api.EnvironmentNameResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.EnvironmentNameResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.EnvironmentNameResponse}
 */
proto.api.EnvironmentNameResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEnvironmentname(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.EnvironmentNameResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.EnvironmentNameResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.EnvironmentNameResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentNameResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEnvironmentname();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string environmentName = 1;
 * @return {string}
 */
proto.api.EnvironmentNameResponse.prototype.getEnvironmentname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.EnvironmentNameResponse} returns this
 */
proto.api.EnvironmentNameResponse.prototype.setEnvironmentname = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.EnvironmentNameResponseArray.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.EnvironmentNameResponseArray.prototype.toObject = function(opt_includeInstance) {
  return proto.api.EnvironmentNameResponseArray.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.EnvironmentNameResponseArray} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentNameResponseArray.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.api.EnvironmentNameResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.EnvironmentNameResponseArray}
 */
proto.api.EnvironmentNameResponseArray.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.EnvironmentNameResponseArray;
  return proto.api.EnvironmentNameResponseArray.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.EnvironmentNameResponseArray} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.EnvironmentNameResponseArray}
 */
proto.api.EnvironmentNameResponseArray.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.EnvironmentNameResponse;
      reader.readMessage(value,proto.api.EnvironmentNameResponse.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.EnvironmentNameResponseArray.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.EnvironmentNameResponseArray.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.EnvironmentNameResponseArray} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.EnvironmentNameResponseArray.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.EnvironmentNameResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated EnvironmentNameResponse items = 1;
 * @return {!Array<!proto.api.EnvironmentNameResponse>}
 */
proto.api.EnvironmentNameResponseArray.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.api.EnvironmentNameResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.EnvironmentNameResponse, 1));
};


/**
 * @param {!Array<!proto.api.EnvironmentNameResponse>} value
 * @return {!proto.api.EnvironmentNameResponseArray} returns this
*/
proto.api.EnvironmentNameResponseArray.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.EnvironmentNameResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.EnvironmentNameResponse}
 */
proto.api.EnvironmentNameResponseArray.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.EnvironmentNameResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.EnvironmentNameResponseArray} returns this
 */
proto.api.EnvironmentNameResponseArray.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.Datastore.prototype.toObject = function(opt_includeInstance) {
  return proto.api.Datastore.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.Datastore} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.Datastore.toObject = function(includeInstance, msg) {
  var f, obj = {
    datastorePath: jspb.Message.getFieldWithDefault(msg, 1, ""),
    datastoreUrl: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.Datastore}
 */
proto.api.Datastore.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.Datastore;
  return proto.api.Datastore.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.Datastore} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.Datastore}
 */
proto.api.Datastore.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastorePath(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatastoreUrl(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.Datastore.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.Datastore.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.Datastore} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.Datastore.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDatastorePath();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDatastoreUrl();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string datastore_path = 1;
 * @return {string}
 */
proto.api.Datastore.prototype.getDatastorePath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.Datastore} returns this
 */
proto.api.Datastore.prototype.setDatastorePath = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string datastore_url = 2;
 * @return {string}
 */
proto.api.Datastore.prototype.getDatastoreUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.Datastore} returns this
 */
proto.api.Datastore.prototype.setDatastoreUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VirtualMachineLoad.repeatedFields_ = [10,11,12];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VirtualMachineLoad.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VirtualMachineLoad.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VirtualMachineLoad} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineLoad.toObject = function(includeInstance, msg) {
  var f, obj = {
    hostname: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    cpu: jspb.Message.getFieldWithDefault(msg, 3, 0),
    memGb: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0),
    nodetype: jspb.Message.getFieldWithDefault(msg, 5, ""),
    leg: jspb.Message.getFieldWithDefault(msg, 6, 0),
    cluster: jspb.Message.getFieldWithDefault(msg, 7, 0),
    vmType: jspb.Message.getFieldWithDefault(msg, 8, 0),
    balanceSortOrder: jspb.Message.getFieldWithDefault(msg, 9, 0),
    networksList: (f = jspb.Message.getRepeatedField(msg, 10)) == null ? undefined : f,
    datastoresList: jspb.Message.toObjectList(msg.getDatastoresList(),
    proto.api.Datastore.toObject, includeInstance),
    tagsList: (f = jspb.Message.getRepeatedField(msg, 12)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.VirtualMachineLoad.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VirtualMachineLoad;
  return proto.api.VirtualMachineLoad.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VirtualMachineLoad} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.VirtualMachineLoad.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostname(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCpu(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMemGb(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setNodetype(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setLeg(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCluster(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setVmType(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setBalanceSortOrder(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.addNetworks(value);
      break;
    case 11:
      var value = new proto.api.Datastore;
      reader.readMessage(value,proto.api.Datastore.deserializeBinaryFromReader);
      msg.addDatastores(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.addTags(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VirtualMachineLoad.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VirtualMachineLoad.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VirtualMachineLoad} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VirtualMachineLoad.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHostname();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getCpu();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getMemGb();
  if (f !== 0.0) {
    writer.writeFloat(
      4,
      f
    );
  }
  f = message.getNodetype();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getLeg();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getCluster();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getVmType();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getBalanceSortOrder();
  if (f !== 0) {
    writer.writeInt32(
      9,
      f
    );
  }
  f = message.getNetworksList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      10,
      f
    );
  }
  f = message.getDatastoresList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      proto.api.Datastore.serializeBinaryToWriter
    );
  }
  f = message.getTagsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      12,
      f
    );
  }
};


/**
 * optional string hostname = 1;
 * @return {string}
 */
proto.api.VirtualMachineLoad.prototype.getHostname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setHostname = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.api.VirtualMachineLoad.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int32 cpu = 3;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getCpu = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setCpu = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional float mem_gb = 4;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getMemGb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setMemGb = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};


/**
 * optional string nodeType = 5;
 * @return {string}
 */
proto.api.VirtualMachineLoad.prototype.getNodetype = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setNodetype = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int32 leg = 6;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getLeg = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setLeg = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 cluster = 7;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getCluster = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setCluster = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 vm_type = 8;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getVmType = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setVmType = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional int32 balance_sort_order = 9;
 * @return {number}
 */
proto.api.VirtualMachineLoad.prototype.getBalanceSortOrder = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setBalanceSortOrder = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * repeated string networks = 10;
 * @return {!Array<string>}
 */
proto.api.VirtualMachineLoad.prototype.getNetworksList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 10));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setNetworksList = function(value) {
  return jspb.Message.setField(this, 10, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.addNetworks = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 10, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.clearNetworksList = function() {
  return this.setNetworksList([]);
};


/**
 * repeated Datastore datastores = 11;
 * @return {!Array<!proto.api.Datastore>}
 */
proto.api.VirtualMachineLoad.prototype.getDatastoresList = function() {
  return /** @type{!Array<!proto.api.Datastore>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.Datastore, 11));
};


/**
 * @param {!Array<!proto.api.Datastore>} value
 * @return {!proto.api.VirtualMachineLoad} returns this
*/
proto.api.VirtualMachineLoad.prototype.setDatastoresList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.api.Datastore=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.Datastore}
 */
proto.api.VirtualMachineLoad.prototype.addDatastores = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.api.Datastore, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.clearDatastoresList = function() {
  return this.setDatastoresList([]);
};


/**
 * repeated string tags = 12;
 * @return {!Array<string>}
 */
proto.api.VirtualMachineLoad.prototype.getTagsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 12));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.setTagsList = function(value) {
  return jspb.Message.setField(this, 12, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.addTags = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 12, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VirtualMachineLoad} returns this
 */
proto.api.VirtualMachineLoad.prototype.clearTagsList = function() {
  return this.setTagsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.RoomCpu.prototype.toObject = function(opt_includeInstance) {
  return proto.api.RoomCpu.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.RoomCpu} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomCpu.toObject = function(includeInstance, msg) {
  var f, obj = {
    vmTotal: jspb.Message.getFieldWithDefault(msg, 1, 0),
    roomAvailable: jspb.Message.getFieldWithDefault(msg, 2, 0),
    severity: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.RoomCpu}
 */
proto.api.RoomCpu.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.RoomCpu;
  return proto.api.RoomCpu.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.RoomCpu} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.RoomCpu}
 */
proto.api.RoomCpu.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setVmTotal(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setRoomAvailable(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setSeverity(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.RoomCpu.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.RoomCpu.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.RoomCpu} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomCpu.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVmTotal();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getRoomAvailable();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getSeverity();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional int32 vm_total = 1;
 * @return {number}
 */
proto.api.RoomCpu.prototype.getVmTotal = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomCpu} returns this
 */
proto.api.RoomCpu.prototype.setVmTotal = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 room_available = 2;
 * @return {number}
 */
proto.api.RoomCpu.prototype.getRoomAvailable = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomCpu} returns this
 */
proto.api.RoomCpu.prototype.setRoomAvailable = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string severity = 3;
 * @return {string}
 */
proto.api.RoomCpu.prototype.getSeverity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomCpu} returns this
 */
proto.api.RoomCpu.prototype.setSeverity = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.RoomMemGB.prototype.toObject = function(opt_includeInstance) {
  return proto.api.RoomMemGB.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.RoomMemGB} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomMemGB.toObject = function(includeInstance, msg) {
  var f, obj = {
    vmTotal: jspb.Message.getFloatingPointFieldWithDefault(msg, 1, 0.0),
    roomAvailable: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    severity: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.RoomMemGB}
 */
proto.api.RoomMemGB.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.RoomMemGB;
  return proto.api.RoomMemGB.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.RoomMemGB} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.RoomMemGB}
 */
proto.api.RoomMemGB.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setVmTotal(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setRoomAvailable(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setSeverity(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.RoomMemGB.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.RoomMemGB.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.RoomMemGB} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RoomMemGB.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVmTotal();
  if (f !== 0.0) {
    writer.writeFloat(
      1,
      f
    );
  }
  f = message.getRoomAvailable();
  if (f !== 0.0) {
    writer.writeFloat(
      2,
      f
    );
  }
  f = message.getSeverity();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional float vm_total = 1;
 * @return {number}
 */
proto.api.RoomMemGB.prototype.getVmTotal = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 1, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomMemGB} returns this
 */
proto.api.RoomMemGB.prototype.setVmTotal = function(value) {
  return jspb.Message.setProto3FloatField(this, 1, value);
};


/**
 * optional float room_available = 2;
 * @return {number}
 */
proto.api.RoomMemGB.prototype.getRoomAvailable = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.RoomMemGB} returns this
 */
proto.api.RoomMemGB.prototype.setRoomAvailable = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional string severity = 3;
 * @return {string}
 */
proto.api.RoomMemGB.prototype.getSeverity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RoomMemGB} returns this
 */
proto.api.RoomMemGB.prototype.setSeverity = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.HypervisorLoadOut.repeatedFields_ = [7,8,9,14,15,16,17,18];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorLoadOut.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorLoadOut.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorLoadOut} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOut.toObject = function(includeInstance, msg) {
  var f, obj = {
    hostname: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    managedObjRef: jspb.Message.getFieldWithDefault(msg, 3, ""),
    cpu: jspb.Message.getFieldWithDefault(msg, 4, 0),
    memGb: jspb.Message.getFloatingPointFieldWithDefault(msg, 5, 0.0),
    rack: jspb.Message.getFieldWithDefault(msg, 6, ""),
    tagsList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f,
    vmCurrentList: jspb.Message.toObjectList(msg.getVmCurrentList(),
    proto.api.VirtualMachineLoad.toObject, includeInstance),
    vmProposedList: jspb.Message.toObjectList(msg.getVmProposedList(),
    proto.api.VirtualMachineLoad.toObject, includeInstance),
    currentRoomCpu: (f = msg.getCurrentRoomCpu()) && proto.api.RoomCpu.toObject(includeInstance, f),
    currentRoomMemGb: (f = msg.getCurrentRoomMemGb()) && proto.api.RoomMemGB.toObject(includeInstance, f),
    proposedRoomCpu: (f = msg.getProposedRoomCpu()) && proto.api.RoomCpu.toObject(includeInstance, f),
    proposedRoomMemGb: (f = msg.getProposedRoomMemGb()) && proto.api.RoomMemGB.toObject(includeInstance, f),
    rulesViolationsList: jspb.Message.toObjectList(msg.getRulesViolationsList(),
    proto.api.BalanceRuleViolation.toObject, includeInstance),
    networksList: (f = jspb.Message.getRepeatedField(msg, 15)) == null ? undefined : f,
    datastoresList: jspb.Message.toObjectList(msg.getDatastoresList(),
    proto.api.Datastore.toObject, includeInstance),
    moveWarnDatastoreList: jspb.Message.toObjectList(msg.getMoveWarnDatastoreList(),
    proto.api.VirtualMachineLoad.toObject, includeInstance),
    moveWarnNetworkList: jspb.Message.toObjectList(msg.getMoveWarnNetworkList(),
    proto.api.VirtualMachineLoad.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorLoadOut}
 */
proto.api.HypervisorLoadOut.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorLoadOut;
  return proto.api.HypervisorLoadOut.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorLoadOut} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorLoadOut}
 */
proto.api.HypervisorLoadOut.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostname(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setManagedObjRef(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCpu(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMemGb(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setRack(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.addTags(value);
      break;
    case 8:
      var value = new proto.api.VirtualMachineLoad;
      reader.readMessage(value,proto.api.VirtualMachineLoad.deserializeBinaryFromReader);
      msg.addVmCurrent(value);
      break;
    case 9:
      var value = new proto.api.VirtualMachineLoad;
      reader.readMessage(value,proto.api.VirtualMachineLoad.deserializeBinaryFromReader);
      msg.addVmProposed(value);
      break;
    case 10:
      var value = new proto.api.RoomCpu;
      reader.readMessage(value,proto.api.RoomCpu.deserializeBinaryFromReader);
      msg.setCurrentRoomCpu(value);
      break;
    case 11:
      var value = new proto.api.RoomMemGB;
      reader.readMessage(value,proto.api.RoomMemGB.deserializeBinaryFromReader);
      msg.setCurrentRoomMemGb(value);
      break;
    case 12:
      var value = new proto.api.RoomCpu;
      reader.readMessage(value,proto.api.RoomCpu.deserializeBinaryFromReader);
      msg.setProposedRoomCpu(value);
      break;
    case 13:
      var value = new proto.api.RoomMemGB;
      reader.readMessage(value,proto.api.RoomMemGB.deserializeBinaryFromReader);
      msg.setProposedRoomMemGb(value);
      break;
    case 14:
      var value = new proto.api.BalanceRuleViolation;
      reader.readMessage(value,proto.api.BalanceRuleViolation.deserializeBinaryFromReader);
      msg.addRulesViolations(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.addNetworks(value);
      break;
    case 16:
      var value = new proto.api.Datastore;
      reader.readMessage(value,proto.api.Datastore.deserializeBinaryFromReader);
      msg.addDatastores(value);
      break;
    case 17:
      var value = new proto.api.VirtualMachineLoad;
      reader.readMessage(value,proto.api.VirtualMachineLoad.deserializeBinaryFromReader);
      msg.addMoveWarnDatastore(value);
      break;
    case 18:
      var value = new proto.api.VirtualMachineLoad;
      reader.readMessage(value,proto.api.VirtualMachineLoad.deserializeBinaryFromReader);
      msg.addMoveWarnNetwork(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorLoadOut.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorLoadOut.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorLoadOut} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOut.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHostname();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getManagedObjRef();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getCpu();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getMemGb();
  if (f !== 0.0) {
    writer.writeFloat(
      5,
      f
    );
  }
  f = message.getRack();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getTagsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      7,
      f
    );
  }
  f = message.getVmCurrentList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      proto.api.VirtualMachineLoad.serializeBinaryToWriter
    );
  }
  f = message.getVmProposedList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      proto.api.VirtualMachineLoad.serializeBinaryToWriter
    );
  }
  f = message.getCurrentRoomCpu();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.api.RoomCpu.serializeBinaryToWriter
    );
  }
  f = message.getCurrentRoomMemGb();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.api.RoomMemGB.serializeBinaryToWriter
    );
  }
  f = message.getProposedRoomCpu();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      proto.api.RoomCpu.serializeBinaryToWriter
    );
  }
  f = message.getProposedRoomMemGb();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.api.RoomMemGB.serializeBinaryToWriter
    );
  }
  f = message.getRulesViolationsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      14,
      f,
      proto.api.BalanceRuleViolation.serializeBinaryToWriter
    );
  }
  f = message.getNetworksList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      15,
      f
    );
  }
  f = message.getDatastoresList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      16,
      f,
      proto.api.Datastore.serializeBinaryToWriter
    );
  }
  f = message.getMoveWarnDatastoreList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      17,
      f,
      proto.api.VirtualMachineLoad.serializeBinaryToWriter
    );
  }
  f = message.getMoveWarnNetworkList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      18,
      f,
      proto.api.VirtualMachineLoad.serializeBinaryToWriter
    );
  }
};


/**
 * optional string hostname = 1;
 * @return {string}
 */
proto.api.HypervisorLoadOut.prototype.getHostname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setHostname = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.api.HypervisorLoadOut.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string managed_obj_ref = 3;
 * @return {string}
 */
proto.api.HypervisorLoadOut.prototype.getManagedObjRef = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setManagedObjRef = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional int32 cpu = 4;
 * @return {number}
 */
proto.api.HypervisorLoadOut.prototype.getCpu = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setCpu = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional float mem_gb = 5;
 * @return {number}
 */
proto.api.HypervisorLoadOut.prototype.getMemGb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 5, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setMemGb = function(value) {
  return jspb.Message.setProto3FloatField(this, 5, value);
};


/**
 * optional string rack = 6;
 * @return {string}
 */
proto.api.HypervisorLoadOut.prototype.getRack = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setRack = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * repeated string tags = 7;
 * @return {!Array<string>}
 */
proto.api.HypervisorLoadOut.prototype.getTagsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setTagsList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.addTags = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearTagsList = function() {
  return this.setTagsList([]);
};


/**
 * repeated VirtualMachineLoad vm_current = 8;
 * @return {!Array<!proto.api.VirtualMachineLoad>}
 */
proto.api.HypervisorLoadOut.prototype.getVmCurrentList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineLoad>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineLoad, 8));
};


/**
 * @param {!Array<!proto.api.VirtualMachineLoad>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setVmCurrentList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.api.VirtualMachineLoad=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.HypervisorLoadOut.prototype.addVmCurrent = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.api.VirtualMachineLoad, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearVmCurrentList = function() {
  return this.setVmCurrentList([]);
};


/**
 * repeated VirtualMachineLoad vm_proposed = 9;
 * @return {!Array<!proto.api.VirtualMachineLoad>}
 */
proto.api.HypervisorLoadOut.prototype.getVmProposedList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineLoad>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineLoad, 9));
};


/**
 * @param {!Array<!proto.api.VirtualMachineLoad>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setVmProposedList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.api.VirtualMachineLoad=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.HypervisorLoadOut.prototype.addVmProposed = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.api.VirtualMachineLoad, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearVmProposedList = function() {
  return this.setVmProposedList([]);
};


/**
 * optional RoomCpu current_room_cpu = 10;
 * @return {?proto.api.RoomCpu}
 */
proto.api.HypervisorLoadOut.prototype.getCurrentRoomCpu = function() {
  return /** @type{?proto.api.RoomCpu} */ (
    jspb.Message.getWrapperField(this, proto.api.RoomCpu, 10));
};


/**
 * @param {?proto.api.RoomCpu|undefined} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setCurrentRoomCpu = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearCurrentRoomCpu = function() {
  return this.setCurrentRoomCpu(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.HypervisorLoadOut.prototype.hasCurrentRoomCpu = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional RoomMemGB current_room_mem_gb = 11;
 * @return {?proto.api.RoomMemGB}
 */
proto.api.HypervisorLoadOut.prototype.getCurrentRoomMemGb = function() {
  return /** @type{?proto.api.RoomMemGB} */ (
    jspb.Message.getWrapperField(this, proto.api.RoomMemGB, 11));
};


/**
 * @param {?proto.api.RoomMemGB|undefined} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setCurrentRoomMemGb = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearCurrentRoomMemGb = function() {
  return this.setCurrentRoomMemGb(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.HypervisorLoadOut.prototype.hasCurrentRoomMemGb = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional RoomCpu proposed_room_cpu = 12;
 * @return {?proto.api.RoomCpu}
 */
proto.api.HypervisorLoadOut.prototype.getProposedRoomCpu = function() {
  return /** @type{?proto.api.RoomCpu} */ (
    jspb.Message.getWrapperField(this, proto.api.RoomCpu, 12));
};


/**
 * @param {?proto.api.RoomCpu|undefined} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setProposedRoomCpu = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearProposedRoomCpu = function() {
  return this.setProposedRoomCpu(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.HypervisorLoadOut.prototype.hasProposedRoomCpu = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional RoomMemGB proposed_room_mem_gb = 13;
 * @return {?proto.api.RoomMemGB}
 */
proto.api.HypervisorLoadOut.prototype.getProposedRoomMemGb = function() {
  return /** @type{?proto.api.RoomMemGB} */ (
    jspb.Message.getWrapperField(this, proto.api.RoomMemGB, 13));
};


/**
 * @param {?proto.api.RoomMemGB|undefined} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setProposedRoomMemGb = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearProposedRoomMemGb = function() {
  return this.setProposedRoomMemGb(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.HypervisorLoadOut.prototype.hasProposedRoomMemGb = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * repeated BalanceRuleViolation rules_violations = 14;
 * @return {!Array<!proto.api.BalanceRuleViolation>}
 */
proto.api.HypervisorLoadOut.prototype.getRulesViolationsList = function() {
  return /** @type{!Array<!proto.api.BalanceRuleViolation>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.BalanceRuleViolation, 14));
};


/**
 * @param {!Array<!proto.api.BalanceRuleViolation>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setRulesViolationsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 14, value);
};


/**
 * @param {!proto.api.BalanceRuleViolation=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.BalanceRuleViolation}
 */
proto.api.HypervisorLoadOut.prototype.addRulesViolations = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 14, opt_value, proto.api.BalanceRuleViolation, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearRulesViolationsList = function() {
  return this.setRulesViolationsList([]);
};


/**
 * repeated string networks = 15;
 * @return {!Array<string>}
 */
proto.api.HypervisorLoadOut.prototype.getNetworksList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 15));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.setNetworksList = function(value) {
  return jspb.Message.setField(this, 15, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.addNetworks = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 15, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearNetworksList = function() {
  return this.setNetworksList([]);
};


/**
 * repeated Datastore datastores = 16;
 * @return {!Array<!proto.api.Datastore>}
 */
proto.api.HypervisorLoadOut.prototype.getDatastoresList = function() {
  return /** @type{!Array<!proto.api.Datastore>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.Datastore, 16));
};


/**
 * @param {!Array<!proto.api.Datastore>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setDatastoresList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 16, value);
};


/**
 * @param {!proto.api.Datastore=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.Datastore}
 */
proto.api.HypervisorLoadOut.prototype.addDatastores = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 16, opt_value, proto.api.Datastore, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearDatastoresList = function() {
  return this.setDatastoresList([]);
};


/**
 * repeated VirtualMachineLoad move_warn_datastore = 17;
 * @return {!Array<!proto.api.VirtualMachineLoad>}
 */
proto.api.HypervisorLoadOut.prototype.getMoveWarnDatastoreList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineLoad>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineLoad, 17));
};


/**
 * @param {!Array<!proto.api.VirtualMachineLoad>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setMoveWarnDatastoreList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 17, value);
};


/**
 * @param {!proto.api.VirtualMachineLoad=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.HypervisorLoadOut.prototype.addMoveWarnDatastore = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 17, opt_value, proto.api.VirtualMachineLoad, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearMoveWarnDatastoreList = function() {
  return this.setMoveWarnDatastoreList([]);
};


/**
 * repeated VirtualMachineLoad move_warn_network = 18;
 * @return {!Array<!proto.api.VirtualMachineLoad>}
 */
proto.api.HypervisorLoadOut.prototype.getMoveWarnNetworkList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineLoad>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineLoad, 18));
};


/**
 * @param {!Array<!proto.api.VirtualMachineLoad>} value
 * @return {!proto.api.HypervisorLoadOut} returns this
*/
proto.api.HypervisorLoadOut.prototype.setMoveWarnNetworkList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 18, value);
};


/**
 * @param {!proto.api.VirtualMachineLoad=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.HypervisorLoadOut.prototype.addMoveWarnNetwork = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 18, opt_value, proto.api.VirtualMachineLoad, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOut} returns this
 */
proto.api.HypervisorLoadOut.prototype.clearMoveWarnNetworkList = function() {
  return this.setMoveWarnNetworkList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.HypervisorLoadOutDTO.repeatedFields_ = [5,6,7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorLoadOutDTO.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorLoadOutDTO.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorLoadOutDTO} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutDTO.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    rulesText: jspb.Message.getFieldWithDefault(msg, 2, ""),
    reduceMoves: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    hypervisorLoadOutsList: jspb.Message.toObjectList(msg.getHypervisorLoadOutsList(),
    proto.api.HypervisorLoadOut.toObject, includeInstance),
    vmUnplacedList: jspb.Message.toObjectList(msg.getVmUnplacedList(),
    proto.api.VirtualMachineLoad.toObject, includeInstance),
    errorsWarningsList: jspb.Message.toObjectList(msg.getErrorsWarningsList(),
    proto.api.BalanceWarnErrorResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorLoadOutDTO}
 */
proto.api.HypervisorLoadOutDTO.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorLoadOutDTO;
  return proto.api.HypervisorLoadOutDTO.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorLoadOutDTO} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorLoadOutDTO}
 */
proto.api.HypervisorLoadOutDTO.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setRulesText(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setReduceMoves(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 5:
      var value = new proto.api.HypervisorLoadOut;
      reader.readMessage(value,proto.api.HypervisorLoadOut.deserializeBinaryFromReader);
      msg.addHypervisorLoadOuts(value);
      break;
    case 6:
      var value = new proto.api.VirtualMachineLoad;
      reader.readMessage(value,proto.api.VirtualMachineLoad.deserializeBinaryFromReader);
      msg.addVmUnplaced(value);
      break;
    case 7:
      var value = new proto.api.BalanceWarnErrorResponse;
      reader.readMessage(value,proto.api.BalanceWarnErrorResponse.deserializeBinaryFromReader);
      msg.addErrorsWarnings(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorLoadOutDTO.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorLoadOutDTO.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorLoadOutDTO} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutDTO.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRulesText();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getReduceMoves();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getHypervisorLoadOutsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.api.HypervisorLoadOut.serializeBinaryToWriter
    );
  }
  f = message.getVmUnplacedList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.api.VirtualMachineLoad.serializeBinaryToWriter
    );
  }
  f = message.getErrorsWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      proto.api.BalanceWarnErrorResponse.serializeBinaryToWriter
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.HypervisorLoadOutDTO.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string rules_text = 2;
 * @return {string}
 */
proto.api.HypervisorLoadOutDTO.prototype.getRulesText = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.setRulesText = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool reduce_moves = 3;
 * @return {boolean}
 */
proto.api.HypervisorLoadOutDTO.prototype.getReduceMoves = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.setReduceMoves = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional string vcenter_name = 4;
 * @return {string}
 */
proto.api.HypervisorLoadOutDTO.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * repeated HypervisorLoadOut hypervisor_load_outs = 5;
 * @return {!Array<!proto.api.HypervisorLoadOut>}
 */
proto.api.HypervisorLoadOutDTO.prototype.getHypervisorLoadOutsList = function() {
  return /** @type{!Array<!proto.api.HypervisorLoadOut>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.HypervisorLoadOut, 5));
};


/**
 * @param {!Array<!proto.api.HypervisorLoadOut>} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
*/
proto.api.HypervisorLoadOutDTO.prototype.setHypervisorLoadOutsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.api.HypervisorLoadOut=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorLoadOut}
 */
proto.api.HypervisorLoadOutDTO.prototype.addHypervisorLoadOuts = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.api.HypervisorLoadOut, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.clearHypervisorLoadOutsList = function() {
  return this.setHypervisorLoadOutsList([]);
};


/**
 * repeated VirtualMachineLoad vm_unplaced = 6;
 * @return {!Array<!proto.api.VirtualMachineLoad>}
 */
proto.api.HypervisorLoadOutDTO.prototype.getVmUnplacedList = function() {
  return /** @type{!Array<!proto.api.VirtualMachineLoad>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.VirtualMachineLoad, 6));
};


/**
 * @param {!Array<!proto.api.VirtualMachineLoad>} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
*/
proto.api.HypervisorLoadOutDTO.prototype.setVmUnplacedList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.api.VirtualMachineLoad=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.VirtualMachineLoad}
 */
proto.api.HypervisorLoadOutDTO.prototype.addVmUnplaced = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.api.VirtualMachineLoad, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.clearVmUnplacedList = function() {
  return this.setVmUnplacedList([]);
};


/**
 * repeated BalanceWarnErrorResponse errors_warnings = 7;
 * @return {!Array<!proto.api.BalanceWarnErrorResponse>}
 */
proto.api.HypervisorLoadOutDTO.prototype.getErrorsWarningsList = function() {
  return /** @type{!Array<!proto.api.BalanceWarnErrorResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.BalanceWarnErrorResponse, 7));
};


/**
 * @param {!Array<!proto.api.BalanceWarnErrorResponse>} value
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
*/
proto.api.HypervisorLoadOutDTO.prototype.setErrorsWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.api.BalanceWarnErrorResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.BalanceWarnErrorResponse}
 */
proto.api.HypervisorLoadOutDTO.prototype.addErrorsWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.api.BalanceWarnErrorResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOutDTO} returns this
 */
proto.api.HypervisorLoadOutDTO.prototype.clearErrorsWarningsList = function() {
  return this.setErrorsWarningsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.BalanceWarnErrorResponse.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceWarnErrorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceWarnErrorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceWarnErrorResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceWarnErrorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    type: jspb.Message.getFieldWithDefault(msg, 1, ""),
    message: jspb.Message.getFieldWithDefault(msg, 2, ""),
    solutionsList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceWarnErrorResponse}
 */
proto.api.BalanceWarnErrorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceWarnErrorResponse;
  return proto.api.BalanceWarnErrorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceWarnErrorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceWarnErrorResponse}
 */
proto.api.BalanceWarnErrorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addSolutions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceWarnErrorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceWarnErrorResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceWarnErrorResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceWarnErrorResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getSolutionsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
};


/**
 * optional string type = 1;
 * @return {string}
 */
proto.api.BalanceWarnErrorResponse.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceWarnErrorResponse} returns this
 */
proto.api.BalanceWarnErrorResponse.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string message = 2;
 * @return {string}
 */
proto.api.BalanceWarnErrorResponse.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceWarnErrorResponse} returns this
 */
proto.api.BalanceWarnErrorResponse.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated string solutions = 3;
 * @return {!Array<string>}
 */
proto.api.BalanceWarnErrorResponse.prototype.getSolutionsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.BalanceWarnErrorResponse} returns this
 */
proto.api.BalanceWarnErrorResponse.prototype.setSolutionsList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.BalanceWarnErrorResponse} returns this
 */
proto.api.BalanceWarnErrorResponse.prototype.addSolutions = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.BalanceWarnErrorResponse} returns this
 */
proto.api.BalanceWarnErrorResponse.prototype.clearSolutionsList = function() {
  return this.setSolutionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.RackMapping.prototype.toObject = function(opt_includeInstance) {
  return proto.api.RackMapping.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.RackMapping} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RackMapping.toObject = function(includeInstance, msg) {
  var f, obj = {
    primary: jspb.Message.getFieldWithDefault(msg, 1, ""),
    secondary: jspb.Message.getFieldWithDefault(msg, 2, ""),
    quorum: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.RackMapping}
 */
proto.api.RackMapping.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.RackMapping;
  return proto.api.RackMapping.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.RackMapping} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.RackMapping}
 */
proto.api.RackMapping.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPrimary(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSecondary(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setQuorum(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.RackMapping.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.RackMapping.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.RackMapping} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.RackMapping.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPrimary();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSecondary();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getQuorum();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string primary = 1;
 * @return {string}
 */
proto.api.RackMapping.prototype.getPrimary = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RackMapping} returns this
 */
proto.api.RackMapping.prototype.setPrimary = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string secondary = 2;
 * @return {string}
 */
proto.api.RackMapping.prototype.getSecondary = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RackMapping} returns this
 */
proto.api.RackMapping.prototype.setSecondary = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string quorum = 3;
 * @return {string}
 */
proto.api.RackMapping.prototype.getQuorum = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.RackMapping} returns this
 */
proto.api.RackMapping.prototype.setQuorum = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceRuleViolation.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceRuleViolation.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceRuleViolation} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceRuleViolation.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceRuleViolation}
 */
proto.api.BalanceRuleViolation.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceRuleViolation;
  return proto.api.BalanceRuleViolation.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceRuleViolation} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceRuleViolation}
 */
proto.api.BalanceRuleViolation.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceRuleViolation.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceRuleViolation.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceRuleViolation} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceRuleViolation.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.api.BalanceRuleViolation.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceRuleViolation} returns this
 */
proto.api.BalanceRuleViolation.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.api.BalanceRuleViolation.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceRuleViolation} returns this
 */
proto.api.BalanceRuleViolation.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.HypervisorLoadOutList.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorLoadOutList.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorLoadOutList.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorLoadOutList} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutList.toObject = function(includeInstance, msg) {
  var f, obj = {
    loadList: jspb.Message.toObjectList(msg.getLoadList(),
    proto.api.HypervisorLoadOut.toObject, includeInstance),
    rackMapping: (f = msg.getRackMapping()) && proto.api.RackMapping.toObject(includeInstance, f),
    balanceSessionId: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorLoadOutList}
 */
proto.api.HypervisorLoadOutList.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorLoadOutList;
  return proto.api.HypervisorLoadOutList.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorLoadOutList} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorLoadOutList}
 */
proto.api.HypervisorLoadOutList.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.api.HypervisorLoadOut;
      reader.readMessage(value,proto.api.HypervisorLoadOut.deserializeBinaryFromReader);
      msg.addLoad(value);
      break;
    case 2:
      var value = new proto.api.RackMapping;
      reader.readMessage(value,proto.api.RackMapping.deserializeBinaryFromReader);
      msg.setRackMapping(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setBalanceSessionId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorLoadOutList.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorLoadOutList.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorLoadOutList} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutList.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLoadList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.api.HypervisorLoadOut.serializeBinaryToWriter
    );
  }
  f = message.getRackMapping();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.api.RackMapping.serializeBinaryToWriter
    );
  }
  f = message.getBalanceSessionId();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * repeated HypervisorLoadOut load = 1;
 * @return {!Array<!proto.api.HypervisorLoadOut>}
 */
proto.api.HypervisorLoadOutList.prototype.getLoadList = function() {
  return /** @type{!Array<!proto.api.HypervisorLoadOut>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.api.HypervisorLoadOut, 1));
};


/**
 * @param {!Array<!proto.api.HypervisorLoadOut>} value
 * @return {!proto.api.HypervisorLoadOutList} returns this
*/
proto.api.HypervisorLoadOutList.prototype.setLoadList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.api.HypervisorLoadOut=} opt_value
 * @param {number=} opt_index
 * @return {!proto.api.HypervisorLoadOut}
 */
proto.api.HypervisorLoadOutList.prototype.addLoad = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.api.HypervisorLoadOut, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.HypervisorLoadOutList} returns this
 */
proto.api.HypervisorLoadOutList.prototype.clearLoadList = function() {
  return this.setLoadList([]);
};


/**
 * optional RackMapping rack_mapping = 2;
 * @return {?proto.api.RackMapping}
 */
proto.api.HypervisorLoadOutList.prototype.getRackMapping = function() {
  return /** @type{?proto.api.RackMapping} */ (
    jspb.Message.getWrapperField(this, proto.api.RackMapping, 2));
};


/**
 * @param {?proto.api.RackMapping|undefined} value
 * @return {!proto.api.HypervisorLoadOutList} returns this
*/
proto.api.HypervisorLoadOutList.prototype.setRackMapping = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.api.HypervisorLoadOutList} returns this
 */
proto.api.HypervisorLoadOutList.prototype.clearRackMapping = function() {
  return this.setRackMapping(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.api.HypervisorLoadOutList.prototype.hasRackMapping = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string balance_session_id = 3;
 * @return {string}
 */
proto.api.HypervisorLoadOutList.prototype.getBalanceSessionId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutList} returns this
 */
proto.api.HypervisorLoadOutList.prototype.setBalanceSessionId = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.HypervisorLoadOutRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.HypervisorLoadOutRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.HypervisorLoadOutRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    rulesText: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    pathRegexp: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.HypervisorLoadOutRequest}
 */
proto.api.HypervisorLoadOutRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.HypervisorLoadOutRequest;
  return proto.api.HypervisorLoadOutRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.HypervisorLoadOutRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.HypervisorLoadOutRequest}
 */
proto.api.HypervisorLoadOutRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setRulesText(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setPathRegexp(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.HypervisorLoadOutRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.HypervisorLoadOutRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.HypervisorLoadOutRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.HypervisorLoadOutRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRulesText();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getPathRegexp();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.HypervisorLoadOutRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutRequest} returns this
 */
proto.api.HypervisorLoadOutRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string rules_text = 2;
 * @return {string}
 */
proto.api.HypervisorLoadOutRequest.prototype.getRulesText = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutRequest} returns this
 */
proto.api.HypervisorLoadOutRequest.prototype.setRulesText = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vcenter_name = 3;
 * @return {string}
 */
proto.api.HypervisorLoadOutRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutRequest} returns this
 */
proto.api.HypervisorLoadOutRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string path_regexp = 4;
 * @return {string}
 */
proto.api.HypervisorLoadOutRequest.prototype.getPathRegexp = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.HypervisorLoadOutRequest} returns this
 */
proto.api.HypervisorLoadOutRequest.prototype.setPathRegexp = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmMoveRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmMoveRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmMoveRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmMoveRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    username: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    balanceSessionUuid: jspb.Message.getFieldWithDefault(msg, 4, ""),
    balanceSessionTimestampMs: jspb.Message.getFieldWithDefault(msg, 5, 0),
    vmPath: jspb.Message.getFieldWithDefault(msg, 6, ""),
    hypervisorSrcPath: jspb.Message.getFieldWithDefault(msg, 7, ""),
    hypervisorDestPath: jspb.Message.getFieldWithDefault(msg, 8, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmMoveRequest}
 */
proto.api.VmMoveRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmMoveRequest;
  return proto.api.VmMoveRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmMoveRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmMoveRequest}
 */
proto.api.VmMoveRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setBalanceSessionUuid(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setBalanceSessionTimestampMs(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmPath(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorSrcPath(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setHypervisorDestPath(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmMoveRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmMoveRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmMoveRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmMoveRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getBalanceSessionUuid();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getBalanceSessionTimestampMs();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getVmPath();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getHypervisorSrcPath();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getHypervisorDestPath();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string username = 2;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vcenter_name = 3;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string balance_session_uuid = 4;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getBalanceSessionUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setBalanceSessionUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int64 balance_session_timestamp_ms = 5;
 * @return {number}
 */
proto.api.VmMoveRequest.prototype.getBalanceSessionTimestampMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setBalanceSessionTimestampMs = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional string vm_path = 6;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getVmPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setVmPath = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string hypervisor_src_path = 7;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getHypervisorSrcPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setHypervisorSrcPath = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string hypervisor_dest_path = 8;
 * @return {string}
 */
proto.api.VmMoveRequest.prototype.getHypervisorDestPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmMoveRequest} returns this
 */
proto.api.VmMoveRequest.prototype.setHypervisorDestPath = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VmMoveResponse.repeatedFields_ = [1,2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmMoveResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmMoveResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmMoveResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmMoveResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    warningsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    errorsList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmMoveResponse}
 */
proto.api.VmMoveResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmMoveResponse;
  return proto.api.VmMoveResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmMoveResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmMoveResponse}
 */
proto.api.VmMoveResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarnings(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.addErrors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmMoveResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmMoveResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmMoveResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmMoveResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      2,
      f
    );
  }
};


/**
 * repeated string warnings = 1;
 * @return {!Array<string>}
 */
proto.api.VmMoveResponse.prototype.getWarningsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.addWarnings = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated string errors = 2;
 * @return {!Array<string>}
 */
proto.api.VmMoveResponse.prototype.getErrorsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.addErrors = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmMoveResponse} returns this
 */
proto.api.VmMoveResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmPowerChangeRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmPowerChangeRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmPowerChangeRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmPowerChangeRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    authToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    vmPath: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmPowerChangeRequest}
 */
proto.api.VmPowerChangeRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmPowerChangeRequest;
  return proto.api.VmPowerChangeRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmPowerChangeRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmPowerChangeRequest}
 */
proto.api.VmPowerChangeRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setVmPath(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmPowerChangeRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmPowerChangeRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmPowerChangeRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmPowerChangeRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getVmPath();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string auth_token = 1;
 * @return {string}
 */
proto.api.VmPowerChangeRequest.prototype.getAuthToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmPowerChangeRequest} returns this
 */
proto.api.VmPowerChangeRequest.prototype.setAuthToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.VmPowerChangeRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmPowerChangeRequest} returns this
 */
proto.api.VmPowerChangeRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string vm_path = 3;
 * @return {string}
 */
proto.api.VmPowerChangeRequest.prototype.getVmPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.VmPowerChangeRequest} returns this
 */
proto.api.VmPowerChangeRequest.prototype.setVmPath = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.api.VmPowerChangeResponse.repeatedFields_ = [1,2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.VmPowerChangeResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.VmPowerChangeResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.VmPowerChangeResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmPowerChangeResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    warningsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    errorsList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.VmPowerChangeResponse}
 */
proto.api.VmPowerChangeResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.VmPowerChangeResponse;
  return proto.api.VmPowerChangeResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.VmPowerChangeResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.VmPowerChangeResponse}
 */
proto.api.VmPowerChangeResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addWarnings(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.addErrors(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.VmPowerChangeResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.VmPowerChangeResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.VmPowerChangeResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.VmPowerChangeResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      2,
      f
    );
  }
};


/**
 * repeated string warnings = 1;
 * @return {!Array<string>}
 */
proto.api.VmPowerChangeResponse.prototype.getWarningsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.addWarnings = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated string errors = 2;
 * @return {!Array<string>}
 */
proto.api.VmPowerChangeResponse.prototype.getErrorsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.addErrors = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.api.VmPowerChangeResponse} returns this
 */
proto.api.VmPowerChangeResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceInvalidCheckRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceInvalidCheckRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceInvalidCheckRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceInvalidCheckRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceInvalidCheckRequest}
 */
proto.api.BalanceInvalidCheckRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceInvalidCheckRequest;
  return proto.api.BalanceInvalidCheckRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceInvalidCheckRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceInvalidCheckRequest}
 */
proto.api.BalanceInvalidCheckRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceInvalidCheckRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceInvalidCheckRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceInvalidCheckRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceInvalidCheckRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.BalanceInvalidCheckRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceInvalidCheckRequest} returns this
 */
proto.api.BalanceInvalidCheckRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceInvalidCheckResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceInvalidCheckResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceInvalidCheckResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceInvalidCheckResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    username: jspb.Message.getFieldWithDefault(msg, 2, ""),
    timestampMs: jspb.Message.getFieldWithDefault(msg, 3, 0),
    sessionUuid: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceInvalidCheckResponse}
 */
proto.api.BalanceInvalidCheckResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceInvalidCheckResponse;
  return proto.api.BalanceInvalidCheckResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceInvalidCheckResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceInvalidCheckResponse}
 */
proto.api.BalanceInvalidCheckResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimestampMs(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceInvalidCheckResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceInvalidCheckResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceInvalidCheckResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceInvalidCheckResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTimestampMs();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getSessionUuid();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.BalanceInvalidCheckResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceInvalidCheckResponse} returns this
 */
proto.api.BalanceInvalidCheckResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string username = 2;
 * @return {string}
 */
proto.api.BalanceInvalidCheckResponse.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceInvalidCheckResponse} returns this
 */
proto.api.BalanceInvalidCheckResponse.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int64 timestamp_ms = 3;
 * @return {number}
 */
proto.api.BalanceInvalidCheckResponse.prototype.getTimestampMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.BalanceInvalidCheckResponse} returns this
 */
proto.api.BalanceInvalidCheckResponse.prototype.setTimestampMs = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional string session_uuid = 4;
 * @return {string}
 */
proto.api.BalanceInvalidCheckResponse.prototype.getSessionUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceInvalidCheckResponse} returns this
 */
proto.api.BalanceInvalidCheckResponse.prototype.setSessionUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceLockRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceLockRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceLockRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceLockRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    username: jspb.Message.getFieldWithDefault(msg, 1, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    sessionUuid: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceLockRequest}
 */
proto.api.BalanceLockRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceLockRequest;
  return proto.api.BalanceLockRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceLockRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceLockRequest}
 */
proto.api.BalanceLockRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceLockRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceLockRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceLockRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceLockRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getSessionUuid();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string username = 1;
 * @return {string}
 */
proto.api.BalanceLockRequest.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockRequest} returns this
 */
proto.api.BalanceLockRequest.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string vcenter_name = 2;
 * @return {string}
 */
proto.api.BalanceLockRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockRequest} returns this
 */
proto.api.BalanceLockRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string session_uuid = 3;
 * @return {string}
 */
proto.api.BalanceLockRequest.prototype.getSessionUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockRequest} returns this
 */
proto.api.BalanceLockRequest.prototype.setSessionUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceLockResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceLockResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceLockResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceLockResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    timestampMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    sessionUuid: jspb.Message.getFieldWithDefault(msg, 2, ""),
    username: jspb.Message.getFieldWithDefault(msg, 3, ""),
    vcenterName: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceLockResponse}
 */
proto.api.BalanceLockResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceLockResponse;
  return proto.api.BalanceLockResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceLockResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceLockResponse}
 */
proto.api.BalanceLockResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimestampMs(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionUuid(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceLockResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceLockResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceLockResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceLockResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTimestampMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getSessionUuid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional int64 timestamp_ms = 1;
 * @return {number}
 */
proto.api.BalanceLockResponse.prototype.getTimestampMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.BalanceLockResponse} returns this
 */
proto.api.BalanceLockResponse.prototype.setTimestampMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string session_uuid = 2;
 * @return {string}
 */
proto.api.BalanceLockResponse.prototype.getSessionUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockResponse} returns this
 */
proto.api.BalanceLockResponse.prototype.setSessionUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string username = 3;
 * @return {string}
 */
proto.api.BalanceLockResponse.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockResponse} returns this
 */
proto.api.BalanceLockResponse.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string vcenter_name = 4;
 * @return {string}
 */
proto.api.BalanceLockResponse.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceLockResponse} returns this
 */
proto.api.BalanceLockResponse.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceUnlockRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceUnlockRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceUnlockRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceUnlockRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    vcenterName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    sessionUuid: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceUnlockRequest}
 */
proto.api.BalanceUnlockRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceUnlockRequest;
  return proto.api.BalanceUnlockRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceUnlockRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceUnlockRequest}
 */
proto.api.BalanceUnlockRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setVcenterName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceUnlockRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceUnlockRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceUnlockRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceUnlockRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVcenterName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSessionUuid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string vcenter_name = 1;
 * @return {string}
 */
proto.api.BalanceUnlockRequest.prototype.getVcenterName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceUnlockRequest} returns this
 */
proto.api.BalanceUnlockRequest.prototype.setVcenterName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string session_uuid = 2;
 * @return {string}
 */
proto.api.BalanceUnlockRequest.prototype.getSessionUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.BalanceUnlockRequest} returns this
 */
proto.api.BalanceUnlockRequest.prototype.setSessionUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.BalanceUnlockResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.BalanceUnlockResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.BalanceUnlockResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceUnlockResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    unlockCount: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.BalanceUnlockResponse}
 */
proto.api.BalanceUnlockResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.BalanceUnlockResponse;
  return proto.api.BalanceUnlockResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.BalanceUnlockResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.BalanceUnlockResponse}
 */
proto.api.BalanceUnlockResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setUnlockCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.BalanceUnlockResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.BalanceUnlockResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.BalanceUnlockResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.BalanceUnlockResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUnlockCount();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 unlock_count = 1;
 * @return {number}
 */
proto.api.BalanceUnlockResponse.prototype.getUnlockCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.BalanceUnlockResponse} returns this
 */
proto.api.BalanceUnlockResponse.prototype.setUnlockCount = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.MetricUsageResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.MetricUsageResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.MetricUsageResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.MetricUsageResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    action: jspb.Message.getFieldWithDefault(msg, 1, ""),
    timestampMs: jspb.Message.getFieldWithDefault(msg, 2, 0),
    data01: jspb.Message.getFieldWithDefault(msg, 3, ""),
    data02: jspb.Message.getFieldWithDefault(msg, 4, ""),
    data03: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.MetricUsageResponse}
 */
proto.api.MetricUsageResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.MetricUsageResponse;
  return proto.api.MetricUsageResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.MetricUsageResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.MetricUsageResponse}
 */
proto.api.MetricUsageResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAction(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimestampMs(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setData01(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setData02(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setData03(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.MetricUsageResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.MetricUsageResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.MetricUsageResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.MetricUsageResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAction();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getTimestampMs();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getData01();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getData02();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getData03();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional string action = 1;
 * @return {string}
 */
proto.api.MetricUsageResponse.prototype.getAction = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.MetricUsageResponse} returns this
 */
proto.api.MetricUsageResponse.prototype.setAction = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 timestamp_ms = 2;
 * @return {number}
 */
proto.api.MetricUsageResponse.prototype.getTimestampMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.MetricUsageResponse} returns this
 */
proto.api.MetricUsageResponse.prototype.setTimestampMs = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string data01 = 3;
 * @return {string}
 */
proto.api.MetricUsageResponse.prototype.getData01 = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.MetricUsageResponse} returns this
 */
proto.api.MetricUsageResponse.prototype.setData01 = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string data02 = 4;
 * @return {string}
 */
proto.api.MetricUsageResponse.prototype.getData02 = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.MetricUsageResponse} returns this
 */
proto.api.MetricUsageResponse.prototype.setData02 = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string data03 = 5;
 * @return {string}
 */
proto.api.MetricUsageResponse.prototype.getData03 = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.api.MetricUsageResponse} returns this
 */
proto.api.MetricUsageResponse.prototype.setData03 = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.api.MetricsReportResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.api.MetricsReportResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.api.MetricsReportResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.MetricsReportResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    timestampMs: jspb.Message.getFieldWithDefault(msg, 1, 0),
    totalLogins: jspb.Message.getFieldWithDefault(msg, 2, 0),
    activeLogins: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.api.MetricsReportResponse}
 */
proto.api.MetricsReportResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.api.MetricsReportResponse;
  return proto.api.MetricsReportResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.api.MetricsReportResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.api.MetricsReportResponse}
 */
proto.api.MetricsReportResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTimestampMs(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTotalLogins(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setActiveLogins(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.api.MetricsReportResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.api.MetricsReportResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.api.MetricsReportResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.api.MetricsReportResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTimestampMs();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getTotalLogins();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getActiveLogins();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
};


/**
 * optional int64 timestamp_ms = 1;
 * @return {number}
 */
proto.api.MetricsReportResponse.prototype.getTimestampMs = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.MetricsReportResponse} returns this
 */
proto.api.MetricsReportResponse.prototype.setTimestampMs = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 total_logins = 2;
 * @return {number}
 */
proto.api.MetricsReportResponse.prototype.getTotalLogins = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.MetricsReportResponse} returns this
 */
proto.api.MetricsReportResponse.prototype.setTotalLogins = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 active_logins = 3;
 * @return {number}
 */
proto.api.MetricsReportResponse.prototype.getActiveLogins = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.api.MetricsReportResponse} returns this
 */
proto.api.MetricsReportResponse.prototype.setActiveLogins = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


goog.object.extend(exports, proto.api);
