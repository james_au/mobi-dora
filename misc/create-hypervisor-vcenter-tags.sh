#!/bin/bash

#
# Creates the tag categories and tag values used by the Dora Balancing algorithm
#

# Pre-requisites:
# 1) BASH shell (or Windows Cygwin Bash emulator)
# 2) The "govc" utility on the shell search bath as "govc"
# 3) Set the govc login shell environment variables described below:
#     GOVC_URL
#     GOVC_USERNAME
#     GOVC_PASSWORD
# 4) Set govc "insecure" mode to allow unsigned SSH certificates otherwise govc errors out with SSL error
#     GOVC_INSECURE=true

#
# Setup the Dora racks tag category & values
#
rack_category="dora-balance-racks"

# category
govc tags.category.create -k -d "Used for MobiTV Balancing for racks" -m=true $rack_category
# values
govc tags.create -k -d "For tagging hypervisor into rack1" -c "${rack_category}" rack1
govc tags.create -k -d "For tagging hypervisor into rack2" -c "${rack_category}" rack2
govc tags.create -k -d "For tagging hypervisor into rack3" -c "${rack_category}" rack3

#
# Set up the hypervisor balance target tag category & values
#
hv_target_category="dora-balance-hv-tags"

# Create the tag category (applicable to "host" records and multiple tags allowed per host)
govc tags.category.create -k -d "Used for MobiTV Balancing functions" -m=true $hv_target_category

# Create the hypervisor target tags
govc tags.create -k -d "For tagging hypervisor as a media 'segmenter farm'" -c "${hv_target_category}" segmenterfarm
govc tags.create -k -d "For tagging hypervisor as a media recording shard" -c "${hv_target_category}" recordingshard