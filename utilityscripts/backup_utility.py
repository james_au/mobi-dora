#!/bin/python3
"""
Generic Backup Utility

PURPOSE
Backups up entire contents of a given source folder to a timestamped tarball in a given destination folder

FEATURES
Archive Rotation
    - Will keep a set number of recent files archive files (default 10) and older archives are deleted
"""

import os
import re
import subprocess
import time
import sys
import argparse
from datetime import datetime


# The dateformat used for backup file creation
#
DEFAULT_DATE_FORMAT = "%Y%m%d-%H%M%S"

# Default rotation count
DEFAULT_ROTATION_COUNT = 10


def timecode(time):
    '''
    Returns time format string (in server timezone) suitable as part of file archive name.  Example: 20200827-011920

    :param time: time to format
    :return: time format string (in server timezone) suitable as part of file archive name.  Example: 20200827-011920
    '''
    return time.strftime(DEFAULT_DATE_FORMAT)


def parse_timestamp(timestamp):
    '''
    Reads timestamp string back to time object
    :param timestamp:  example expected format: 20200827-114246
    :return:  time object parsed from timestamp string
    '''
    return time.mktime(datetime.strptime(timestamp, DEFAULT_DATE_FORMAT).timetuple())


def create_archive_name(source_path, time):
    '''
    Returns a string that is an standardized filename based on the basepath and given time
    
    :param source_path: the source path where the trailing name will be used as the archive's prefix name
    :param time:  time object representing time (usually current time)
    :return:  string that is an standardized filename based on the basepath and given time
    '''
    prefix = os.path.basename(os.path.normpath(source_path))
    return "%s-%s.tbz" % (prefix, timecode(time))


def compress(source_path, destination_path):
    '''
    Compresses the source directory path and all sub-folders to a compressed tarball at the given destination path.
    The archive file will be bzip2 compressed and named with trailing name of source path and timecode with extension
    .tbz.

    :param source_path   a full path to the directory to compress
    :param destination_path  the path where to write the compressed tarball to (should be the standard backup location)
    :return: 0 if success and non-zero otherwise
    '''

    # Example tar command:
    # tar -jcf sqlite-20200827-0100000.tbz sqlite

    archive_name = create_archive_name(source_path, datetime.now())
    output_path = os.path.join(destination_path, archive_name)

    print("Compressing: {0} to {1}".format(source_path, output_path))
    process = subprocess.run(["tar", "-jcf", output_path, source_path],
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    # Print error to console
    if process.returncode != 0:
        print(process.stderr, file=sys.stderr)

    # Return code will be 0 if success.  Caller should check and update user as necessary.
    return process.returncode


def rotate_archives(n, directory_path, basename):
    '''
    Deletes archives that are older than a given time value

    :param n   number of most recent files to keep and the remaining are deleted
    :param directory_path:  full path to the directory containing the files to be rotated
    :param basename   the file base name/prefix
    :return:
    '''

    # Matching file records will be populated into this array
    files = []

    # Get all files in output folder matching expected filename format and the basename prefix
    # NOTE os.scandir() requires Python 3.5+
    for dir_entry in os.scandir(directory_path):
        match = re.search(r'(.+)-(\d+-\d+)\.tbz', dir_entry.name)

        # Collect all matching file entries to an array (not yet sorted or filtered)
        if match and match[1] == basename:
            files.append(dir_entry)

    # Sort file entries descending by filename (newest file first)
    files = sorted(files, key=lambda item: item.name, reverse=True)

    # Keep the first 'n' most recent files but delete the rest if any.
    if len(files) > n:
        print("FILE ROTATION: Keeping only the most recent %d files." % n)
        expired_files = files[n:]  # Select sublist of all files after the n'th element in the original list
        for dir_entry in expired_files:
            print("Deleting expired backup file: %s" % dir_entry.path)
            os.remove(dir_entry.path)

    return 0


def ensure_path_writable(path):
    # Use shell call to make all parent paths.  If the path exists then this effectively does nothing.
    process = subprocess.run(["mkdir", "--parents", path],
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    if process.returncode != 0:
        return process.returncode
    else:
        # Ensure path is writeable
        if not os.access(path, os.W_OK):
            print("Cannot write to specified destination path: %s" % path)
            return 1

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Backs up a given folder to a compressed timestamped archive file and rotates out older backup files")
    parser.add_argument("source_path", help="Path to the folder containing the data to backup")
    parser.add_argument("dest_path", help="Path to folder to write compressed backup to")
    parser.add_argument("--files_to_keep", "-n", type=int, default=DEFAULT_ROTATION_COUNT)
    args = parser.parse_args()

    # Get the basename of the source path to be used as the prefix for the backup files
    # Ex"/opt/mobi-dora/sqlite" => "sqlite"
    basename = os.path.basename(os.path.normpath(args.source_path))

    # Ensure destination path exists and is writable.
    # Error out if not able to ensure this.
    if ensure_path_writable(args.dest_path) != 0:
        # FATAL: problem with destination path so cannot continue processing
        sys.exit("Destination path %s does not exist or is not writeable" % args.dest_path)

    # SANITY CHECK: the given source path
    #
    if not os.path.exists(args.source_path):
        # FATAL
        sys.exit("Source path %s does not exist." % args.source_path)
    elif not os.path.isdir(args.source_path):
        # FATAL
        sys.exit("Source path %s is not a directory.  Source must be a directory." % args.source_path)
    elif not os.access(args.source_path, os.R_OK):
        # FATAL: Nothing to read
        sys.exit("Source path %s is not readable. Check file permissions." % args.source_path)

    # ARCHIVE: Perform the current backup
    if compress(args.source_path, args.dest_path) != 0:
        # Fatal
        sys.exit("Backup failed.  Refer to error messages above.")

    # ROTATE: Clean up backup folder by removing old expired backup files
    if rotate_archives(args.files_to_keep, args.dest_path, basename) != 0:
        # WARN: Not fatal but serious system malfunction that could lead to running out of disk space
        print("WARN: Archive rotation failed.  Proceeding with backup but you may run out of disk space eventually.")

    print("Rotation & Compression complete for: %s" % args.source_path)
    print("See above for logs")

    # SUCCESS: Successful program complete.  Exit with status code 0.
    sys.exit()