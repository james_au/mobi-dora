#!/bin/bash
# This script will run a given command in "watchdog" mode which will restart the command
# if it errors out for any reason.  This is intended to safe guard server processes that may
# ocassionally exit because of some runtime panic error.  It is not intended for any other use.
#
if [[ $# -eq 0 ]]; then
  echo "Must supply a command to run in watchdog mode"
  echo "Usage: $0 commands..."
  echo "Example: $0 start-my-server.sh --port=9000 --debug"
  exit 1
fi

# Save the name of the program to run
declare command="$@"

# Redirect outputs to both stdout and a log file.  This is the only way
# to redirect docker CMD process's output.
# TODO: Log file path should be specified by some command switch later
exec &> >(tee -a /var/log/mobi-dora/dora.out)
exec& 2> >(tee -a /var/log/mobi-dora/dora.out)

# Start Dora server in a continous "watch dog" loop which will just restart Dora server if it
# exits for any reason.  This is useful GoLang executables which can exit if any code panics.
declare count=0
while true
do
  (( count++ ))

  eval "${command}"
  status=$?

  if (( $status != 0 )); then
    echo "The command \"${command}\" crashed.  Restarting... (restart count = ${count})"
  else
    echo "The command \"${command}\" exited normally.  Quitting watchdog."
    break
  fi
done
