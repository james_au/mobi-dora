#!/bin/python3

"""
This is a utility script to be run in Dora gRPC Docker container only to setup its SQLite cron expression
based on JSON configuration parameter
"""

import json
import os
import sys

# Parse the cron expression from this path
CONFIG_FILE = "./dora-config-overrides.json"

# The cron expression will be output to this cron file
CRON_FILE = "/var/spool/cron/rtv"

if __name__ == '__main__':
    if not os.path.exists(CONFIG_FILE):
        sys.exit("Configuration file not found at expected location: %s" % CONFIG_FILE)
    if not os.access(CONFIG_FILE, os.R_OK):
        sys.exit("Configuration file found at %s but cannot be read.  Check file permissions." % CONFIG_FILE)

    # Read JSON text from file
    jsonText = open(CONFIG_FILE, "r").read()

    # Parse the JSON data and get the cron expression out
    configData = json.loads(jsonText)
    cron_expression = configData["sqlite_backup_cron_expression"]

    if cron_expression is not None and cron_expression.strip() != "":
        print("Adding cron expression '%s' to %s" % (cron_expression, CRON_FILE))
        open(CRON_FILE, "w+").write(cron_expression + "\n")
    else:
        print("Not setting up cron.  No cron expression found.")

    # SUCCESS: Successful program complete.  Exit with status code 0.
    sys.exit()
