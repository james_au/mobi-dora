# DEVOPS RESEARCH & ASSESSMENT (DORA)

A website to replace sitebook.

## Infrastructure

* Envoy on port 8000
    - gRPC-Web Go-RPC-Server on port 9090
    - clientjs - Go-HTTP-Server on port 8081 to host webpack generated static files
    - lib - https://github.com/gorilla/mux
* GRPC Middleware:
    - TLS:
        - version: 1.2+ 
        - symmetric/asymmetric algorithm: ECDHE-ECDSA-AES128-GCM-SHA256
        - lib - https://blog.gopheracademy.com/advent-2019/go-grps-and-tls/
    - Deadlines:
        - lib - https://grpc.io/blog/deadlines/
    - Auth:
        - lib - https://github.com/grpc/grpc-go/blob/master/Documentation/grpc-auth-support.md#jwt
* Auth: LDAP Integration
    - lib - https://github.com/go-ldap/ldap
* Session: JSON Web Tokens
    - lib - https://github.com/dgrijalva/jwt-go 
* Logging:
    - lib - klogr https://git.k8s.io/klog/klogr
* Redis: For caching and queuing
    - lib - github.com/mediocregopher/radix/v3
* Sqlite3:
    - lib - https://github.com/crawshaw/sqlite
* Profiling:
    - lib - https://github.com/sourcegraph/appdash
    - grpctrace middleware lib - https://github.com/grpc-ecosystem/grpc-opentracing/tree/master/go/otgrpc
    - opentracing interface lib - github.com/opentracing/opentracing-go

## Components

* Router:
    - Handle RPC calls from clients.
    - Cache and write to respective Redis-Queues.
* Cache:
    - Store relevant data in Redis-KV for immediate response.
* Queue:
    - Redis queue.
    - Aggregate requests using Redis-LUA scripting. [Optional]
* Reader:
    - Routine to read from Redis-Queues and send to appropriate Pools.
* Pool:
    - Workers grouped together to perform a particular task.
    - lib - https://github.com/oklog/run
* Worker:
    - Goroutine to perform a particular task.

## Project Structure
```
├ api = proto code
├ client = javascript code
├ auth = external packages
├ cmd = cli code for all components
├ pkg = server code
├ go.mod = go module file
```

## Functionalities:

* dork ls - show all vms in a current environment (with filters)
* dork room:
    - nfs-balancing for nvrcws - https://docs.google.com/spreadsheets/d/1hstMec5lDmHkvUV66TxhGFHsTKgF5_OblQkny_h7y2o/edit#gid=1759614056
    - vm-balancing
* dork info:
    - esxi vm switch/port info
    - segmenter channel-info (also compare data with DPS)
    - consolidate traffic-server stats (parent, mid-tier & child ecats)
* dork tracer: given a url, show the entire network request/response timing all the way to the backend

## Testing

* curl testing - https://github.com/fullstorydev/grpcurl

## Frontend

* UI lib - https://getbootstrap.com/

## Notes

* RPC Design Guide: https://cloud.google.com/apis/design/
* gRPC Transcoding:
    - https://cloud.google.com/endpoints/docs/grpc/transcoding
    - https://github.com/googleapis/googleapis/blob/master/google/api/http.proto
* cmd Package
    - mobi ref - https://bitbucket.org/mobitv/mobi-recording-amq-splitter/src/master/amq_splitter/main.go

## Getting Started

You will need the following components to get started with the utility:

1. go
    - go1.14 - https://golang.org/doc/install
2. vscode - recommended IDE
    - https://code.visualstudio.com/Download
    - [go plugin](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Go)
